# Contributing to Eclipse StatET

Thanks for your interest in this project.

Eclipse StatET is an Eclipse-based IDE for R.

  * Project home: https://projects.eclipse.org/projects/science.statet


## Developer Resources

Please check the project page for information regarding source repositories, bug tracker and other
developer resources:
  * https://projects.eclipse.org/projects/science.statet/developer

This project uses GitLab as development platform:
  * https://gitlab.eclipse.org/eclipse/statet

#### Source Repositories

The source code is hosted in the GIT repository:
  * https://gitlab.eclipse.org/eclipse/statet/statet.git

#### Issues

The ongoing development, bugs, ideas and other issues are tracked in:
  * https://gitlab.eclipse.org/eclipse/statet/statet/-/issues

Be sure to search for existing issues before you create another one.

#### Development Environment

The default development environment is the Eclipse IDE with JDT, PDE and M2E. For additional
information see also:
  * https://gitlab.eclipse.org/groups/eclipse/statet/-/wikis/Development-Environment


## Contribute Source

This project accepts source contributions via merge request in GitLab. Please note the requirements
described in the following section.


## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team contributors must electronically sign
the Eclipse Contributor Agreement (ECA).

  * https://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in the footer indicating
that the author is aware of the terms by which the contribution has been provided to the project.
The non-committer must additionally have an Eclipse Foundation account and must have a signed
Eclipse Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook
https://www.eclipse.org/projects/handbook/#resources-commit


## Build

The build of StatET is based on Apache Maven (https://maven.apache.org/) and Eclipse Tycho
(https://www.eclipse.org/tycho/). 

### Prerequisite

  * Maven
  * The content of the StatET main repository `statet`.

### Additional Plug-in Dependencies

To aggregate all additional plug-in dependencies as Eclipse (OSGI) plug-ins to a P2 repository,
run:

```
mvn -f 3rdparty/bundle-recipes-pom.xml install
mvn -f 3rdparty/pom.xml package
```

### StatET

The build requires the additional plug-in dependencies described above. Instead of using the locally
build repository, it is also possible to use an existing P2 repository with the required plug-ins by
specifying the location with the property `statet.dependencies.url`.

To build StatET with all components, run:

```
mvn -f statet-pom.xml package
```


## Contact

Contact the project developers via the project’s Developer Mailing List `statet-dev`:
  * https://projects.eclipse.org/projects/science.statet/contact
