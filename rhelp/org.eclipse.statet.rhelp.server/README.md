# Eclipse StatET - R Help Server

## Requirements

  * Java
  * RJSrv

## Configuration

Edit the configuration(s) for the R environments in the folder `renvs`:
  - One configuration file per R environment
  - The name of the file is the id of the R environment

The server is based on Spring Boot; see documentation of Spring Boot e.g. how
to configure the webserver.

## Run the Server

To run the server just start the jar:

    java -jar org.eclipse.statet.rhelp.server.jar &

### Special modes
  * `-index-and-exit` - creates or updates the help index only

## Access the Server

### URI for API
The URI to access the help of an R environment via IDE is specified as follows:

    http://<name or ip>:<port>/<env id>

For example: `http://serverwithhelp:8080/r35`

The following defaults can be optionally omitted:
  - `<port>` = 80
  - `<env id>` = default

So `http://serverwithhelp/` is equal to `http://serverwithhelp:80/default`.

### Web
The URL to access the help using a webbrowser is:

    http://<name or ip>:<port>/rhelp/browse/<env id>

