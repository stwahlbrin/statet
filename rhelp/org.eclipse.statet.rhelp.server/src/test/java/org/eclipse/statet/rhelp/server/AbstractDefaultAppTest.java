/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.server;

import static java.util.concurrent.TimeUnit.SECONDS;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.rhelp.core.REnvHelp;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvManager;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= WebEnvironment.RANDOM_PORT)
@Import(ContextConfiguration.class)
@NonNullByDefault
public abstract class AbstractDefaultAppTest {
	
	
	@Autowired
	protected @Nullable REnvManager rEnvManager;
	
	@Autowired
	protected @Nullable RHelpManager rHelpManager;
	
	
	public AbstractDefaultAppTest() {
	}
	
	
	@SuppressWarnings("null")
	public static <T> T assumeNotNull(final @Nullable T value) {
		assumeTrue(value != null);
		return value;
	}
	
	@SuppressWarnings("null")
	public static REnv assumeREnvValid(final @Nullable REnv rEnv) {
		assumeTrue(rEnv != null);
		final REnvConfiguration rEnvConfiguration= rEnv.get(REnvConfiguration.class);
		assumeTrue(rEnvConfiguration != null
				&& rEnvConfiguration.getValidationStatus().getSeverity() == Status.OK );
		return rEnv;
	}
	
	protected void assumeRHelpAvailable(final String envId) throws InterruptedException {
		final REnvManager rEnvManager= assumeNotNull(this.rEnvManager);
		final REnv rEnv= assumeREnvValid(rEnvManager.get(envId, null));
		
		final RHelpManager rHelpManager= assumeNotNull(this.rHelpManager);
		final long tStart= System.nanoTime();
		final long tEnd= tStart + SECONDS.toNanos(5);
		while (tStart < tEnd) {
			final REnvHelp rEnvHelp= rHelpManager.getHelp(rEnv);
			if (rEnvHelp != null) {
				rEnvHelp.unlock();
				return;
			}
			Thread.sleep(500);
		}
		assumeTrue(false, String.format("Help for R env '%1$s' available.", envId));
	}
	
	
	@SuppressWarnings("null")
	public static <T> T assertNotNull(final @Nullable T value) {
		Assertions.assertNotNull(value);
		return value;
	}
	
}
