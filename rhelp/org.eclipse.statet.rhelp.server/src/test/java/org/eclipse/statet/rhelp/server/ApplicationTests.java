/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.time.Duration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.AppEnvironment;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.rhelp.core.REnvHelp;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvManager;


@NonNullByDefault
public class ApplicationTests extends AbstractDefaultAppTest {
	
	
	@Test
	public void AppEnv_available() {
		final AppEnvironment environment= assertNotNull(
				CommonsRuntime.getEnvironment() );
		Assertions.assertEquals(RHelpServerApplication.BUNDLE_ID, environment.getEnvId());
	}
	
	@Test
	public void REnvManager_available() {
		final REnvManager rEnvManager= assertNotNull(this.rEnvManager);
		final REnv rEnv= assertNotNull(
				rEnvManager.get("default", null) ); //$NON-NLS-1$
		rEnv.getId();
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named = "STATET_TEST_FILES", matches = ".+")
	public void REnvManager_Config_loaded() {
		final REnvManager rEnvManager= assumeNotNull(this.rEnvManager);
		final REnv rEnv= assertNotNull(
				rEnvManager.get("default", null) ); //$NON-NLS-1$
		final REnvConfiguration rEnvConfig= assertNotNull(
				rEnv.get(REnvConfiguration.class) );
		final Status status= rEnvConfig.getValidationStatus();
		assertEquals(Status.OK, status.getSeverity());
	}
	
	
	@Test
	public void RHelpManager_available() {
		final RHelpManager rHelpManager= this.rHelpManager;
		assertNotNull(rHelpManager);
	}
	
	
	@Test
	@EnabledIfEnvironmentVariable(named = "STATET_TEST_FILES", matches = ".+")
	public void RHelp_loadAvailable() {
		final REnvManager rEnvManager= assumeNotNull(this.rEnvManager);
		final REnv rEnv= assumeREnvValid(rEnvManager.get("default", null));
		
		final RHelpManager rHelpManager= assumeNotNull(this.rHelpManager);
		
		assertTimeoutPreemptively(Duration.ofSeconds(10),
				() -> {
					while (true) {
						final REnvHelp rEnvHelp= rHelpManager.getHelp(rEnv);
						if (rEnvHelp != null) {
							rEnvHelp.unlock();
							return;
						}
					}
				});
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named = "STATET_TEST_FILES", matches = ".+")
	public void RHelp_createNew() {
		final REnvManager rEnvManager= assumeNotNull(this.rEnvManager);
		final REnv rEnv= assumeREnvValid(rEnvManager.get("new", null));
		
		final RHelpManager rHelpManager= assumeNotNull(this.rHelpManager);
		
		assertTimeoutPreemptively(Duration.ofMinutes(10),
				() -> {
					while (true) {
						final REnvHelp rEnvHelp= rHelpManager.getHelp(rEnv);
						if (rEnvHelp != null) {
							rEnvHelp.unlock();
							return;
						}
					}
				});
	}
	
	
}
