/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class CustomMediaTypeProvider implements MediaTypeProvider {
	
	
	private final Map<String, String> nameTypes= new HashMap<>();
	private final Map<String, String> extTypes= new HashMap<>();
	
	
	public CustomMediaTypeProvider() {
	}
	
	
	public void addName(final String fileName, final String mediaTypeString) {
		this.nameTypes.put(fileName, mediaTypeString);
	}
	
	public void addExt(final String fileExt, final String mediaTypeString) {
		this.extTypes.put(fileExt, mediaTypeString);
	}
	
	@Override
	public @Nullable String getMediaTypeString(final String fileName) {
		String type= this.nameTypes.get(fileName);
		if (type == null) {
			final int idx= fileName.indexOf('.');
			if (idx >= 0) {
				type= this.extTypes.get(fileName.substring(idx + 1));
			}
		}
		return type;
	}
	
}
