/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http;

import javax.servlet.ServletContext;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ServletMediaTypeProvider implements MediaTypeProvider {
	
	
	private final ServletContext servletContext;
	
	
	public ServletMediaTypeProvider(final ServletContext servletContext) {
		this.servletContext= servletContext;
	}
	
	
	@Override
	public @Nullable String getMediaTypeString(final String fileName) {
		return this.servletContext.getMimeType(fileName);
	}
	
}
