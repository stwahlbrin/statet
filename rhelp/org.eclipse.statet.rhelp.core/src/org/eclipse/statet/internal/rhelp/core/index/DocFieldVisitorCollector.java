/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import java.io.IOException;
import java.util.Set;

import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.StoredFieldVisitor;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.SimpleCollector;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class DocFieldVisitorCollector extends SimpleCollector {
	
	
	public abstract static class Visitor extends StoredFieldVisitor {
		
		
		private final Set<String> fieldNames;
		
		
		public Visitor(final Set<String> fieldNames) {
			this.fieldNames= fieldNames;
		}
		
		
		@Override
		public Status needsField(final FieldInfo fieldInfo) throws IOException {
			return this.fieldNames.contains(fieldInfo.name) ? Status.YES : Status.NO;
		}
		
		public void setReader(final LeafReader reader) throws IOException {
		}
		
		public abstract void newDocMatch(int doc, float score) throws IOException;
		
		public abstract void finalizeDocMatch() throws IOException;
		
	}
	
	
	private final Visitor visitor;
	
	private Scorer scorer;
	
	private LeafReader reader;
	private LeafReader readerSet;
	
	
	public DocFieldVisitorCollector(final Visitor visitor) {
		this.visitor= visitor;
	}
	
	
	@Override
	public boolean needsScores() {
		return true;
	}
	
	@Override
	protected void doSetNextReader(final LeafReaderContext context) throws IOException {
		this.reader= context.reader();
	}
	
	@Override
	public void setScorer(final Scorer scorer) throws IOException {
		this.scorer= scorer;
	}
	
	@Override
	public void collect(final int doc) throws IOException {
		final float score= this.scorer.score();
		if (score > 0.0f) {
			if (this.readerSet != this.reader) {
				this.visitor.setReader(this.readerSet= this.reader);
			}
			this.visitor.newDocMatch(doc, score);
			this.reader.document(doc, this.visitor);
			this.visitor.finalizeDocMatch();
		}
	}
	
}
