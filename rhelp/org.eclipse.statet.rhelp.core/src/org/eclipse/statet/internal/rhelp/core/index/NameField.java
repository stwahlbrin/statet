/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
final class NameField extends StringDataField {
	
	
	public static final FieldType TYPE;
	
	static {
		TYPE= new FieldType(StringField.TYPE_STORED);
		TYPE.setOmitNorms(false);
		TYPE.freeze();
	}
	
	
	/**
	 * Creates a new field.
	 * 
	 * @param name field name
	 * @throws IllegalArgumentException if the field name.
	 */
	public NameField(final String name) {
		super(name, TYPE);
	}
	
}
