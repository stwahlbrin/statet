/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core.input;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Text parser input for source string.
 */
@NonNullByDefault
public final class StringParserInput extends TextParserInput implements CharSequence {
	
	
	private String source;
	
	
	public StringParserInput(final int defaultBufferSize) {
		super(defaultBufferSize);
	}
	
	public StringParserInput() {
		this(DEFAULT_BUFFER_SIZE);
	}
	
	public StringParserInput(final String source) {
		this(Math.min(source.length(), DEFAULT_BUFFER_SIZE));
		
		this.source= source;
	}
	
	
	public StringParserInput reset(final String source) {
		this.source= source;
		
		super.reset();
		
		return this;
	}
	
	@Override
	public StringParserInput init() {
		super.init();
		
		return this;
	}
	
	@Override
	public StringParserInput init(final int startIndex, final int stopIndex) {
		super.init(startIndex, stopIndex);
		
		return this;
	}
	
	
	@Override
	protected int getSourceLength() {
		return this.source.length();
	}
	
	@Override
	protected @Nullable String getSourceString() {
		return this.source;
	}
	
	
	@Override
	protected void doUpdateBuffer(final int index, final char[] buffer,
			final int requiredLength, final int recommendLength) {
		final int length= Math.min(recommendLength, getStopIndex() - index);
		this.source.getChars(index, index + length, buffer, 0);
		setBuffer(buffer, 0, length);
	}
	
	
/*- CharSequence ---------------------------------------------------------------------------------*/
	
	@Override
	public int length() {
		return this.source.length();
	}
	
	@Override
	public char charAt(final int index) {
		return this.source.charAt(index);
	}
	
	@Override
	public String subSequence(final int start, final int end) {
		return this.source.substring(start, end);
	}
	
}
