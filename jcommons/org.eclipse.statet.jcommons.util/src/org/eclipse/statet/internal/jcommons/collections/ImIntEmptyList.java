/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImIntEmptyList extends AbstractImIntList {
	
	
	public static final ImIntEmptyList INSTANCE= new ImIntEmptyList();
	
	
	public ImIntEmptyList() {
	}
	
	
	@Override
	public int size() {
		return 0;
	}
	
	@Override
	public boolean isEmpty() {
		return true;
	}
	
	@Override
	public boolean contains(final int element) {
		return false;
	}
	
	@Override
	public int getAt(final int index) {
		throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
	}
	
	@Override
	public int indexOf(final int element) {
		return -1;
	}
	
	@Override
	public int lastIndexOf(final int element) {
		return -1;
	}
	
	
	@Override
	public IntStream stream() {
		return StreamSupport.intStream(Spliterators.emptyIntSpliterator(), false);
	}
	
	
	@Override
	public int[] toArray() {
		return ArrayUtils.EMPTY_INT;
	}
	
	
	@Override
	public String getString() {
		return ""; //$NON-NLS-1$
	}
	
	
	@Override
	public int hashCode() {
		return 9;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof IntList) {
			final IntList other= (IntList)obj;
			return (other.isEmpty());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "[]"; //$NON-NLS-1$
	}
	
}
