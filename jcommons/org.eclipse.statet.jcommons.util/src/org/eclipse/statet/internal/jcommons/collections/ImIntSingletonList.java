/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.stream.IntStream;

import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImIntSingletonList extends AbstractImIntList {
	
	
	private final int e;
	
	
	public ImIntSingletonList(final int e) {
		this.e= e;
	}
	
	
	@Override
	public int size() {
		return 1;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	
	@Override
	public boolean contains(final int element) {
		return (this.e == element);
	}
	
	@Override
	public int getAt(final int index) {
		if (index != 0) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.e;
	}
	
	@Override
	public int indexOf(final int element) {
		return (this.e == element) ? 0 : -1;
	}
	
	@Override
	public int lastIndexOf(final int element) {
		return (this.e == element) ? 0 : -1;
	}
	
	
	@Override
	public IntStream stream() {
		return IntStream.of(this.e);
	}
	
	
	@Override
	public int[] toArray() {
		return new int[] { this.e };
	}
	
	
	@Override
	public String getString() {
		return Character.toString(this.e);
	}
	
	
	@Override
	public int hashCode() {
		int hashCode= 9;
		hashCode= 31 * hashCode + this.e;
		return hashCode;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof IntList) {
			final IntList other= (IntList)obj;
			return (1 == other.size()
					&& this.e == other.getAt(0) );
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "[" + this.e + ']';
	}
	
}
