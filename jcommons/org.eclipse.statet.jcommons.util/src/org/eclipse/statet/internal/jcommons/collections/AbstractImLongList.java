/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import org.eclipse.statet.jcommons.collections.ImLongList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AbstractImLongList implements ImLongList {
	
	
	public AbstractImLongList() {
	}
	
	
	@Override
	public boolean add(final long element) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void addAt(final int index, final long element) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public long setAt(final int index, final long element) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean remove(final long element) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public long removeAt(final int index) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}
	
}
