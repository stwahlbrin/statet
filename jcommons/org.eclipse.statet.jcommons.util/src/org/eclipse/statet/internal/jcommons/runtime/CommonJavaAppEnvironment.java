/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.runtime;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.BasicAppEnvironment;
import org.eclipse.statet.jcommons.runtime.bundle.Bundles;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.util.JUtilLoggingStatusLogger;


@NonNullByDefault
public class CommonJavaAppEnvironment extends BasicAppEnvironment {
	
	
	public static final String ENV_ID= "org.eclipse.statet.jcommons.runtime.appEnvironments.CommonJava"; //$NON-NLS-1$
	
	
	public CommonJavaAppEnvironment() throws StatusException {
		super(ENV_ID, new JUtilLoggingStatusLogger(), Bundles.createResolver());
		initJRuntimeStoppingHandler();
	}
	
	
}
