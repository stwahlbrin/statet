/*=============================================================================#
 # Copyright (c) 2019, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Spliterator;
import java.util.function.Consumer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImSingletonSpliterator<E> implements Spliterator<E> {
	
	
	private final E e;
	
	private int estimateSize = 1;
	
	
	public ImSingletonSpliterator(final E e) {
		this.e = e;
	}
	
	
	@Override
	public int characteristics() {
		return (Spliterator.ORDERED | Spliterator.DISTINCT | Spliterator.IMMUTABLE |
				Spliterator.SIZED | Spliterator.SUBSIZED);
	}
	
	@Override
	public long estimateSize() {
		return this.estimateSize;
	}
	
	@Override
	public @Nullable Spliterator<E> trySplit() {
		return null;
	}
	
	@Override
	public boolean tryAdvance(final Consumer<? super E> consumer) {
		nonNullAssert(consumer);
		if (this.estimateSize > 0) {
			this.estimateSize--;
			consumer.accept(this.e);
			return true;
		}
		return false;
	}
	
	@Override
	public void forEachRemaining(final Consumer<? super E> consumer) {
		tryAdvance(consumer);
	}
	
}
