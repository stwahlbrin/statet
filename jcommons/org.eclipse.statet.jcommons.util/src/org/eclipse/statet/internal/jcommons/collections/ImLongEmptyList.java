/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.Spliterators;
import java.util.stream.LongStream;
import java.util.stream.StreamSupport;

import org.eclipse.statet.jcommons.collections.LongList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImLongEmptyList extends AbstractImLongList {
	
	
	public static final ImLongEmptyList INSTANCE= new ImLongEmptyList();
	
	
	public ImLongEmptyList() {
	}
	
	
	@Override
	public int size() {
		return 0;
	}
	
	@Override
	public boolean isEmpty() {
		return true;
	}
	
	@Override
	public boolean contains(final long element) {
		return false;
	}
	
	@Override
	public long getAt(final int index) {
		throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
	}
	
	@Override
	public int indexOf(final long element) {
		return -1;
	}
	
	@Override
	public int lastIndexOf(final long element) {
		return -1;
	}
	
	
	@Override
	public LongStream stream() {
		return StreamSupport.longStream(Spliterators.emptyLongSpliterator(), false);
	}
	
	
	@Override
	public long[] toArray() {
		return ArrayUtils.EMPTY_LONG;
	}
	
	
	@Override
	public int hashCode() {
		return 7;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof LongList) {
			final LongList other= (LongList)obj;
			return (other.isEmpty());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "[]"; //$NON-NLS-1$
	}
	
}
