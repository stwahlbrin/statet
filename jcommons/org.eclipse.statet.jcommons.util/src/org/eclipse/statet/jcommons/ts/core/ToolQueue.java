/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.Status;


/**
 * Queue of a {@link Tool} to schedule {@link ToolRunnable}s.
 */
@NonNullByDefault
public interface ToolQueue {
	
	
	/**
	 * Schedules the runnable in the regular queue.
	 * <p>
	 * The method returns directly after the queue operation is done. The runnable is run when it is
	 * its turn.</p>
	 * <p>
	 * A runnable can be added multiple times to schedule it multiple times.</p>
	 * 
	 * @param runnable the runnable to add to the queue
	 * @return status of the queue operation.
	 */
	Status add(final ToolRunnable runnable);
	
	/**
	 * Removes the runnable from the queue.
	 * <p>
	 * If the runnable exists multiple times in the queue, the first runnable (in order they would
	 * be run) will be removed. If the runnable is not in the queue, nothing is done.</p>
	 * <p>
	 * If a runnable is removed, the operation trigger a event of type {@link ToolRunnable#REMOVING_FROM}.
	 * </p>
	 * 
	 * @param runnable the runnable to remove from the queue
	 */
	void remove(final ToolRunnable runnable);
	
	
	/**
	 * Returns if the instance supports real hot scheduling of runnables.
	 * 
	 * @return <code>true</code> if it is supported, otherwise false
	 * @see #addHot(ToolRunnable)
	 */
	boolean isHotSupported();
	
	/**
	 * Schedules the runnable in the hot queue.
	 * <p>
	 * Runnables in the hot queue are run as soon as possible, also before and within runnables
	 * of the regular queue.  It should be used only  for short task and not changing global data.
	 * A typical use case are minor updates in the GUI.</p>
	 * <p>
	 * A runnable can be added multiple times to schedule it multiple times.</p>
	 * 
	 * @param runnable the runnable to add to the queue
	 * @return status of the queue operation.
	 */
	Status addHot(final ToolRunnable runnable);
	
	/**
	 * Removes the runnable from the queue.
	 * <p>
	 * If the runnable exists multiple times in the queue, the first runnable (in order they would
	 * be run) will be removed. If the runnable is not in the queue, nothing is done.</p>
	 * <p>
	 * If a runnable is removed, the operation trigger a event of type {@link ToolRunnable#REMOVING_FROM}.
	 * </p>
	 * 
	 * @param runnable the runnable to remove from the queue
	 */
	void removeHot(final ToolRunnable runnable);
	
	
}
