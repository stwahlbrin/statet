/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Objects accepts tool instances. Can be connected to a {@link ToolProvider}.
 */
@NonNullByDefault
public interface ActiveToolListener {
	
	
	class ActiveToolEvent {
		
		public static final byte TOOL_ACTIVATED= 1;
		public static final byte TOOL_TERMINATED= 2;
		
		
		private final byte type;
		
		private final @Nullable Tool tool;
		
		
		public ActiveToolEvent(final byte type, final @Nullable Tool tool) {
			this.type= type;
			this.tool= tool;
		}
		
		
		public byte getType() {
			return this.type;
		}
		
		public final @Nullable Tool getTool() {
			return this.tool;
		}
		
	}
	
	
	/**
	 * Is called when the tool changed
	 * 
	 * @param tool the new tool
	 */
	void onToolChanged(final ActiveToolEvent event);
	
}
