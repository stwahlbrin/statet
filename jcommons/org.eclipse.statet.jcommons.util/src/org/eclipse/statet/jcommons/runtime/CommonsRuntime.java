/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime;

import static org.eclipse.statet.internal.jcommons.runtime.CommonsRuntimeInternals.BUNDLE_ID;

import java.io.PrintStream;

import org.eclipse.statet.internal.jcommons.runtime.CommonJavaAppEnvironment;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public class CommonsRuntime {
	
	
	private static @Nullable AppEnvironment environment;
	
	
	/**
	 * Initializes this runtime with specified app environment.
	 * 
	 * @param environment the app environment to use.
	 * @throws IllegalStateException if the runtime is already initialized.
	 */
	public static synchronized void init(final AppEnvironment environment) {
		if (environment == null) {
			throw new NullPointerException("environment"); //$NON-NLS-1$
		}
		final AppEnvironment current= CommonsRuntime.environment;
		if (current != null) {
			throw new IllegalStateException(
					String.format("Already initialized by '%1$s'.",
							current.getEnvId()) );
		}
		CommonsRuntime.environment= environment;
		log(new InfoStatus(BUNDLE_ID, "CommonsRuntime initialized with: " + environment));
	}
	
	/**
	 * Checks if this runtime is initialized.
	 * 
	 * @param autoInit <code>true</code> to detect the app environment automatically and initializes
	 *     the runtime, if the runtime is not yet initialized.
	 * @throws StatusException autoInit is <code>false</code> and the runtime is not initialized,
	 *     or if the auto initialization fails.
	 */
	public static synchronized void check(final boolean autoInit) throws StatusException {
		final AppEnvironment current= CommonsRuntime.environment;
		if (current != null) {
			return;
		}
		if (autoInit) {
			init(new CommonJavaAppEnvironment());
		}
		else {
			throw new StatusException(new ErrorStatus(BUNDLE_ID, "CommonsRuntime is not initialized."));
		}
	}
	
	
	public static AppEnvironment getEnvironment() {
		return CommonsRuntime.environment;
	}
	
	
	public static void log(final Status status) {
		final AppEnvironment environment= CommonsRuntime.environment;
		if (environment != null) {
			environment.log(status);
		}
		else {
			final PrintStream out;
			switch (status.getSeverity()) {
			case Status.OK:
			case Status.INFO:
				out= System.out;
				break;
			default:
				out= System.err;
				break;
			}
			out.print(status.toString());
		}
	}
	
	
	private CommonsRuntime() {}
	
}
