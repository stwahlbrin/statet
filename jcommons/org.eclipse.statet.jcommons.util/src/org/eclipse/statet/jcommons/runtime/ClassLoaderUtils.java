/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.security.CodeSource;

import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ClassLoaderUtils {
	
	
	private static final String JAR_START= UriUtils.JAR_SCHEME + ':';
	private static final String FILE_START= UriUtils.FILE_SCHEME + ':';
	private static final String JAR_SEPARATOR= UriUtils.JAR_SEPARATOR;
	
	
	public static String getClassLocationUrlString(final Class<?> refClass) {
		String urlString;
		urlString= getUrlStringByClassloader(refClass);
//		if (urlString.startsWith("bundleresource://")) { // in osgi //$NON-NLS-1$
//			urlString= getUrlStringByDomain(refClass);
//		}
		return urlString;
	}
	
	private static String getUrlStringByDomain(final Class<?> refClass) {
		try {
			final CodeSource source= refClass.getProtectionDomain().getCodeSource();
			if (source == null) {
				throw new UnsupportedOperationException("CodeSource not available"); //$NON-NLS-1$
			}
			final URI url= source.getLocation().toURI();
			final String s= url.toString();
			if (s.endsWith(".jar")) { //$NON-NLS-1$
				final StringBuilder sb= new StringBuilder(s.length());
				if (!s.startsWith(JAR_START)) {
					sb.append(JAR_START);
				}
				sb.append(s);
				sb.append(JAR_SEPARATOR);
				return sb.toString();
			}
			return s;
		}
		catch (final Exception e) {
			throw new UnsupportedOperationException(
					String.format("getUrlStringByDomain failed (refClass= %1$s)", refClass.getName()), //$NON-NLS-1$
					e );
		}
	}
	
	private static String getUrlStringByClassloader(final Class<?> refClass) {
		try {
			final String resourceName= refClass.getName().replace('.', '/') + ".class"; //$NON-NLS-1$
			final URI url= refClass.getClassLoader().getResource(resourceName).toURI();
			final String s= url.toString();
			if (s.endsWith(resourceName)) {
				return s.substring(0, s.length() - resourceName.length());
			}
			throw new UnsupportedOperationException("url= " + url); //$NON-NLS-1$
		}
		catch (final Exception e) {
			throw new UnsupportedOperationException(
					String.format("getUrlStringByClassloader failed (refClass= %1$s)", refClass.getName()), //$NON-NLS-1$
					e );
		}
	}
	
	
	public static String toJClassPathEntryString(final String url) {
		String fileUrl= url;
		if (url.startsWith(JAR_START)) {
			final int separatorIdx= url.indexOf(JAR_SEPARATOR);
			if (separatorIdx != url.length() - 2) {
				throw new UnsupportedOperationException("url= " + url); //$NON-NLS-1$
			}
			fileUrl= url.substring(JAR_START.length(), separatorIdx);
		}
		if (!fileUrl.startsWith(FILE_START)) {
			throw new UnsupportedOperationException("url= " + url); //$NON-NLS-1$
		}
		try {
			final URI uri= new URI(fileUrl);
			final Path path= Path.of(uri);
			return path.toString();
		}
		catch (final URISyntaxException e) {
			throw new UnsupportedOperationException("url= " + url, e); //$NON-NLS-1$
		}
	}
	
	
	private ClassLoaderUtils() {}
	
}
