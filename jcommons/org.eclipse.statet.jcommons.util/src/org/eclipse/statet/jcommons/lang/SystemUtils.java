/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.lang;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import java.util.Locale;


@NonNullByDefault
public class SystemUtils {
	
	
	public static final String OS_NAME_KEY= "os.name"; //$NON-NLS-1$
	public static final String OS_ARCH_KEY= "os.arch"; //$NON-NLS-1$
	public static final String OS_VERSION_KEY= "os.version"; //$NON-NLS-1$
	
	public static final String FILE_SEPARATOR_KEY= "file.separator"; //$NON-NLS-1$
	public static final String PATH_SEPARATOR_KEY= "path.separator"; //$NON-NLS-1$
	public static final String LINE_SEPARATOR_KEY= "line.separator"; //$NON-NLS-1$
	
	public static final String USER_NAME_KEY= "user.name"; //$NON-NLS-1$
	public static final String USER_HOME_KEY= "user.home"; //$NON-NLS-1$
	
	
	public static final byte OS_WIN= 1;
	public static final byte OS_MAC= 2;
	
	public static final String ARCH_X86_64= "x86_64"; //$NON-NLS-1$
	public static final String ARCH_X86_32= "x86_32"; //$NON-NLS-1$
	public static final String ARCH_ARM_64= "arm_64"; //$NON-NLS-1$
	
	
	private static final byte LOCAL_OS;
	private static final String LOCAL_ARCH;
	static {
		LOCAL_OS= getOs(nonNullAssert(System.getProperty(OS_NAME_KEY)));
		LOCAL_ARCH= nonNullElse(
				getArch(nonNullAssert(System.getProperty(OS_ARCH_KEY))),
				"other" );
	}
	
	public static byte getLocalOs() {
		return LOCAL_OS;
	}
	
	public static String getLocalArch() {
		return LOCAL_ARCH;
	}
	
	public static byte getOs(String osName) {
		osName= osName.toLowerCase(Locale.ROOT);
		if (osName.startsWith("windows", 0)) { //$NON-NLS-1$
			return OS_WIN;
		}
		if (osName.startsWith("mac os", 0)) { //$NON-NLS-1$
			return OS_MAC;
		}
		return 0;
	}
	
	public static @Nullable String getArch(String osArch) {
		osArch= osArch.toLowerCase(Locale.ROOT);
		
		switch (osArch) {
		case ARCH_X86_64:
		case "amd64": //$NON-NLS-1$
		case "x64": //$NON-NLS-1$
			return ARCH_X86_64;
		case ARCH_X86_32:
		case "x86": //$NON-NLS-1$
		case "i386": //$NON-NLS-1$
		case "i486": //$NON-NLS-1$
		case "i586": //$NON-NLS-1$
		case "i686": //$NON-NLS-1$
			return ARCH_X86_32;
		case "aarch64": //$NON-NLS-1$
			return ARCH_ARM_64;
		default:
			return null;
		}
	}
	
}
