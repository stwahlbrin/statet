/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.stream.LongStream;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface LongList {
	
	
	int size();
	
	boolean isEmpty();
	
	
	int indexOf(long element);
	int lastIndexOf(long element);
	
	boolean contains(long element);
	
	long getAt(int index);
	
	
	boolean add(long element);
	void addAt(int index, long element);
	
	long setAt(int index, long element);
	
	boolean remove(long element);
	long removeAt(int index);
	
	void clear();
	
	
	LongStream stream();
	
	
	long[] toArray();
	
}
