/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.stream.IntStream;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface IntList {
	
	
	int size();
	
	boolean isEmpty();
	
	boolean contains(int element);
	
	int getAt(int index);
	
	int indexOf(int element);
	int lastIndexOf(int element);
	
	
	boolean add(int element);
	void addAt(int index, int element);
	
	int setAt(int index, int element);
	
	boolean remove(int element);
	int removeAt(int index);
	
	void clear();
	
	
	IntStream stream();
	
	
	int[] toArray();
	
	
	String getString();
	
}
