/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ByteArrayList implements ByteList {
	
	
	private static final byte[] EMPTY_ARRAY= new byte[0];
	
	
	private byte[] array;
	
	private int size;
	
	
	public ByteArrayList() {
		this.array= EMPTY_ARRAY;
	}
	
	public ByteArrayList(final int initialCapacity) {
		this.array= new byte[initialCapacity];
	}
	
	
	@Override
	public int size() {
		return this.size;
	}
	
	@Override
	public boolean isEmpty() {
		return (this.size == 0);
	}
	
	
	@Override
	public int indexOf(final byte element) {
		final byte[] array= this.array;
		final int size= this.size;
		for (int i= 0; i < size; i++) {
			if (array[i] == element) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public int lastIndexOf(final byte element) {
		final byte[] array= this.array;
		for (int i= this.size - 1; i >= 0; i--) {
			if (array[i] == element) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public boolean contains(final byte element) {
		return (indexOf(element) >= 0);
	}
	
	
	@Override
	public byte getAt(final int index) {
		if (index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.array[index];
	}
	
	
	protected final void ensureCapacity(int min) {
		if (min > this.array.length) {
			if (this.array == EMPTY_ARRAY) {
				min= 8;
			}
			int newCapacity= Math.max(this.array.length + (this.array.length >> 1), min);
			if (newCapacity < 0) {
				newCapacity= Integer.MAX_VALUE;
			}
			final byte[] newArray= new byte[newCapacity];
			System.arraycopy(this.array, 0, newArray, 0, this.size);
			this.array= newArray;
		}
	}
	
	@Override
	public boolean add(final byte element) {
		ensureCapacity(this.size + 1);
		this.array[this.size++]= element;
		return true;
	}
	
	@Override
	public void addAt(final int index, final byte element) {
		if (index < 0 || index > this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		ensureCapacity(this.size + 1);
		if (index < this.size) {
			System.arraycopy(this.array, index, this.array, index + 1, this.size - index);
		}
		this.array[index]= element;
		this.size++;
	}
	
	@Override
	public boolean add(final byte[] element, final int start, final int end) {
		final int l= end - start;
		ensureCapacity(this.size + l);
		System.arraycopy(element, start, this.array, this.size, l);
		this.size+= l;
		return true;
	}
	
	@Override
	public byte setAt(final int index, final byte element) {
		if (index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		final byte oldElement= this.array[index];
		this.array[index]= element;
		return oldElement;
	}
	
	private void doRemoveElementBefore(final int index) {
		if (index != this.size) {
			System.arraycopy(this.array, index, this.array, index - 1, this.size - index);
		}
	}
	
	@Override
	public boolean remove(final byte element) {
		final int index= indexOf(element);
		if (index >= 0) {
			doRemoveElementBefore(index + 1);
			this.size--;
			return true;
		}
		return false;
	}
	
	@Override
	public int removeAt(final int index) {
		if (index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		final int oldElement= this.array[index];
		doRemoveElementBefore(index + 1);
		this.size--;
		return oldElement;
	}
	
	@Override
	public void clear() {
		this.size= 0;
	}
	
	
	@Override
	public byte[] toArray() {
		return Arrays.copyOf(this.array, this.size);
	}
	
	
	@Override
	public String getString(final Charset charset) {
		return new String(this.array, 0, this.size, charset);
	}
	
}
