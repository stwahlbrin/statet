/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.nio.charset.Charset;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface ByteList {
	
	
	int size();
	
	boolean isEmpty();
	
	boolean contains(byte element);
	
	byte getAt(int index);
	
	int indexOf(byte element);
	int lastIndexOf(byte element);
	
	
	boolean add(byte element);
	void addAt(int index, byte element);
	
	boolean add(byte[] element, int start, int end);
	
	byte setAt(int index, byte element);
	
	boolean remove(byte element);
	int removeAt(int index);
	
	void clear();
	
	
	byte[] toArray();
	
	
	String getString(Charset charset);
	
}
