/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class BasicStringFactory implements StringFactory {
	
	
	public static final BasicStringFactory INSTANCE= new BasicStringFactory();
	
	
	static final int CHARTABLE_SIZE= 0x100;
	static final @NonNull String[] CHARTABLE;
	static {
		CHARTABLE= new @NonNull String[CHARTABLE_SIZE];
		for (char i= 0; i < CHARTABLE_SIZE; i++) {
			CHARTABLE[i]= String.valueOf(i).intern();
		}
	}
	
	
	public BasicStringFactory() {
	}
	
	
	@Override
	public String get(final CharSequence s) {
		switch (s.length()) {
		case 0:
			return ""; //$NON-NLS-1$
		case 1:
			return get(s.charAt(0));
		default:
			return s.toString();
		}
	}
	
	@Override
	public String get(final CharArrayString s) {
		switch (s.length()) {
		case 0:
			return ""; //$NON-NLS-1$
		case 1:
			return get(s.charAt(0));
		default:
			return s.toString();
		}
	}
	
	@Override
	public String get(final String s) {
		switch (s.length()) {
		case 0:
			return ""; //$NON-NLS-1$
		case 1:
			return get(s.charAt(0), s);
		default:
			return s;
		}
	}
	
	@Override
	public String get(final char c) {
		if (c >= 0 && c < CHARTABLE_SIZE) {
			return CHARTABLE[c];
		}
		return String.valueOf(c);
	}
	
	@Override
	public String get(final int codepoint) {
		if (codepoint >= 0 && codepoint < CHARTABLE_SIZE) {
			return CHARTABLE[codepoint];
		}
		return Character.toString(codepoint);
	}
	
	private String get(final char c, final String s) {
		if (c >= 0 && c < CHARTABLE_SIZE) {
			return CHARTABLE[c];
		}
		return s;
	}
	
}
