/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Implement to provide a strategy to create or share string instances.
 */
@NonNullByDefault
public interface StringFactory {
	
	
	String get(CharSequence s);
	String get(CharArrayString s);
	String get(String s);
	
	String get(char c);
	String get(int codepoint);
	
}
