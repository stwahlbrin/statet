/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.concurrent;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class DaemonThreadFactory extends BasicThreadFactory {
	
	
	public DaemonThreadFactory(final ThreadGroup threadGroup,
			final String threadNamePrefix, final String threadNameSuffix) {
		super(threadGroup, threadNamePrefix, threadNameSuffix);
	}
	
	public DaemonThreadFactory(final String threadBaseName) {
		super(threadBaseName);
	}
	
	public DaemonThreadFactory(final ThreadGroup threadGroup,
			final @Nullable String threadSubGroupName) {
		super(threadGroup, threadSubGroupName);
	}
	
	public DaemonThreadFactory(final ThreadGroup threadGroup) {
		super(threadGroup);
	}
	
	
	@Override
	protected boolean getDaemon() {
		return true;
	}
	
	
}
