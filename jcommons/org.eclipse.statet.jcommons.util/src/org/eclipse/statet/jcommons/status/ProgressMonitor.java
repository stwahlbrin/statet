/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface ProgressMonitor {
	
	
	/** Constant indicating an unknown amount of work. */
	int UNKNOWN= -1;
	
	
	/** Flag indicating that the name passed into {@link #beginTask(String, int)} should be ignored. */
	int SUPPRESS_BEGINTASK_NAME= 1 << 1;
	
	/** Flag indicating that {@link #isCanceled()} should always return <code>false</code>. */
	int SUPPRESS_ISCANCELED= 1 << 4;
	
	int SUPPRESS_NONE= 0;
	
	
	/**
	 * Notifies that the main task is beginning.
	 * 
	 * @param name The label of the main task
	 * @param totalWork The number of work units into which the main task is been subdivided; or
	 *     <code>UNKNOWN</code>.
	 */
	void beginTask(final String name, final int totalWork);
	
	/**
	 * Notifies that a subtask of the main task is beginning.
	 * 
	 * Subtasks are optional.
	 *
	 * @param name The label of the subtask, or <code>null</code> to clear
	 */
	void beginSubTask(final @Nullable String name);
	
	/**
	 * Sets the work remaining. The remaining space on the progress monitor is redistributed into
	 * the given number of work units.
	 * 
	 * @param work The number of work units into which the work remaining is been subdivided
	 */
	ProgressMonitor setWorkRemaining(final int work);
	
	/**
	 * Notifies that a given number of work unit task has been completed.
	 * 
	 * @param work The number of work units completed
	 */
	void addWorked(final int work);
	
	/**
	 * Creates a new nested progress monitor that will consume the given number of work units from
	 * this monitor.
	 * 
	 * <p>Shorthand for calling {@link #newSubMonitor(int, int)} with (totalWork, #SUPPRESS_BEGINTASK_NAME).</p>
	 * 
	 * @param work The number of work unit to consume from the receiver
	 * @return new progress monitor
	 */
	default ProgressMonitor newSubMonitor(final int work) {
		return newSubMonitor(work, SUPPRESS_BEGINTASK_NAME);
	}
	
	/**
	 * Creates a new nested progress monitor that will consume the given number of work units from
	 * this monitor.
	 *  
	 * @param work The number of work unit to consume from the receiver
	 * @param flags Flags to configure the behavior
	 * @return new progress monitor
	 */
	ProgressMonitor newSubMonitor(final int work, final int flags);
	
	
	/**
	 * Returns whether cancelation of current operation has been requested.
	 *
	 * @return <code>true</code> if cancellation has been requested, <code>false</code> otherwise
	 * @see #setCanceled(boolean)
	 */
	boolean isCanceled();
	
	/**
	 * Sets the cancel state to the given value.
	 * 
	 * @param state <code>true</code> to indicate that cancelation has been requested,
	 *     or <code>false</code> to clear this flag
	 * @see #isCanceled()
	 */
	void setCanceled(final boolean state);
	
	default void checkCanceled() throws StatusException {
		if (isCanceled()) {
			throw new StatusException(Status.CANCEL_STATUS);
		}
	}
	
	
	default void setBlocked(final Status reason) {
	}
	
	default void clearBlocked() {
	}
	
}
