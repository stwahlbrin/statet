/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusLogger;
import org.eclipse.statet.jcommons.status.Statuses;


@NonNullByDefault
public class ACommonsLoggingStatusLogger implements StatusLogger {
	
	
	private static final Function<String, Log> CREATE_LOG_FUNCTION= LogFactory::getLog;
	
	
	private final ConcurrentHashMap<String, Log> logs= new ConcurrentHashMap<>();
	
	private final StatusPrinter logStatusPrinter= new StatusPrinter();
	
	
	public ACommonsLoggingStatusLogger() {
	}
	
	
	@Override
	public void log(final Status status) {
		final Log log= this.logs.computeIfAbsent(status.getBundleId(), CREATE_LOG_FUNCTION);
		
		switch (status.getSeverity()) {
		case Status.ERROR:
			if (log.isErrorEnabled()) {
				log.error(createMessage(status), status.getException());
			}
			break;
		case Status.WARNING:
			if (log.isWarnEnabled()) {
				log.warn(createMessage(status), status.getException());
			}
			break;
		default:
			if (log.isInfoEnabled()) {
				log.info(createMessage(status), status.getException());
			}
			break;
		}
	}
	
	protected String createMessage(final Status status) {
		final ToStringBuilder sb= new ToStringBuilder();
		sb.append('[', status.getCode(), ']');
		switch (status.getSeverity()) {
		case Status.OK:
		case Status.CANCEL:
			sb.append(' ', Statuses.getSeverityString(status.getSeverity()));
			break;
		default:
			break;
		}
		sb.appendLines(' ', status.getMessage());
		if (status.isMultiStatus()) {
			final ImList<Status> children= status.getChildren();
			if (children != null && !children.isEmpty()) {
				final StringBuilder sb0= new StringBuilder();
				sb0.append("Status:\n");
				this.logStatusPrinter.print(children, sb0);
				sb.addProp("children", sb0.toString()); //$NON-NLS-1$
			}
			else {
				sb.addProp("children", "<none>"); //$NON-NLS-1$
			}
		}
		return sb.toString();
	}
	
}
