/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Assertions;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CollectionTests {
	
	
	public static final String A_notIdentical= new String("A");
	public static final String B_notIdentical= new String("B");
	
	
	public static final <E> void assertCollectionEmpty(final Collection<E> c) {
		assertTrue(c.isEmpty());
		Assertions.assertEquals(0, c.size());
	}
	
	public static final <E> void assertListEquals(final String[] expected, final List<E> c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int i= 0; i < expected.length; i++) {
			assertEquals(new String(expected[i]), c.get(i), String.format("[%1$s]", i));
		}
	}
	
	public static final <E> void assertListEqualsIdentical(final String[] expected, final IdentityList<E> c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int i= 0; i < expected.length; i++) {
			assertSame(expected[i], c.get(i), String.format("[%1$s]", i));
		}
	}
	
	public static final <E> void assertSetEquals(final String[] expected, final Set<E> c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int i= 0; i < expected.length; i++) {
			assertTrue(c.contains(expected[i]), String.format("[%1$s]", i));
		}
	}
	
}
