/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import org.eclipse.statet.internal.jcommons.collections.ImLongArrayList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ImLongArrayListTest extends AbstractImLongListTest {
	
	
	public ImLongArrayListTest() {
	}
	
	
	@Override
	protected ImLongList createList(final long value0) {
		return new ImLongArrayList(new long[] { value0, Long.MAX_VALUE, -1 });
	}
	
	@Override
	protected int getExpectedSize() {
		return 3;
	}
	
	
}
