/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CopyOnWriteListTest extends AbstractMutableListTest<CopyOnWriteList<String>> {
	
	
	public CopyOnWriteListTest() {
		super(CopyOnWriteList.class);
	}
	
	
	@Override
	protected CopyOnWriteList<String> createEmptyCollection() {
		return new CopyOnWriteList<>();
	}
	
	@Override
	protected CopyOnWriteList<String> createCollection(final String... elements) {
		return new CopyOnWriteList<>(Arrays.asList(elements));
	}
	
	
}
