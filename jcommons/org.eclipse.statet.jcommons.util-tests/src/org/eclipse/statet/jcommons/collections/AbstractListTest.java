/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AbstractListTest<T extends List<String>> {
	
	
	protected final boolean isIdentityCollection;
	
	protected T c;
	
	
	@SuppressWarnings("null")
	public AbstractListTest(final Class<?> type) {
		this.isIdentityCollection= IdentityList.class.isAssignableFrom(type);
	}
	
	
	protected abstract T createEmptyCollection();
	
	protected abstract T createCollection(final String... elements);
	
	
	protected void assertCollectionEmpty() {
		CollectionTests.assertCollectionEmpty(this.c);
	}
	
	protected void assertCollectionEquals(final String... expected) {
		CollectionTests.assertListEquals(expected, this.c);
	}
	
	
}
