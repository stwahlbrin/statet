/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.collections.CollectionTests.A_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.B_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.assertListEquals;
import static org.eclipse.statet.jcommons.collections.CollectionTests.assertListEqualsIdentical;
import static org.eclipse.statet.jcommons.collections.ImCollections.emptyIdentityList;
import static org.eclipse.statet.jcommons.collections.ImCollections.emptyList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newIdentityList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newList;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ImCollectionsOperationsTest {
	
	
	public ImCollectionsOperationsTest() {
	}
	
	
	private static ArrayList<String> newArrayList(final @NonNull String... elements) {
		return new ArrayList<>(Arrays.asList(elements));
	}
	
	
	@Test
	public void List_concatList_2ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list3= newList("A", "B", "C");
		
		assertListEquals(
				new String[] { },
				ImCollections.concatList(list0, emptyList()) );
		assertListEquals(
				new String[] { "A" },
				ImCollections.concatList(list1, emptyList()) );
		assertListEquals(
				new String[] { "E" },
				ImCollections.concatList(list0, newList("E")) );
		assertListEquals(
				new String[] { "A", "E" },
				ImCollections.concatList(list1, newList("E")) );
		assertListEquals(
				new String[] { "A", "B", "C" },
				ImCollections.concatList(list3, emptyList()) );
		assertListEquals(
				new String[] { "E", "F", "G" },
				ImCollections.concatList(list0, newList("E", "F", "G")) );
		assertListEquals(
				new String[] { "A", "B", "C", "E", "F", "G" },
				ImCollections.concatList(list3, newList("E", "F", "G")) );
		
		assertTrue(list1 == ImCollections.concatList(list1, emptyList()));
		assertTrue(list1 == ImCollections.concatList(emptyList(), list1));
		assertTrue(list3 == ImCollections.concatList(list3, emptyList()));
		assertTrue(list3 == ImCollections.concatList(emptyList(), list3));
	}
	
	@Test
	public void List_addElement_ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		
		assertListEquals(
				new String[] { "E" },
				ImCollections.addElement(list0, "E") );
		assertListEquals(
				new String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertListEquals(
				new String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertListEquals(
				new String[] { "A", "B", "B" },
				ImCollections.addElement(list2, "B") );
	}
	
	@Test
	public void List_addElement_ArrayList() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		
		assertListEquals(
				new String[] { "E" },
				ImCollections.addElement(list0, "E") );
		assertListEquals(
				new String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertListEquals(
				new String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertListEquals(
				new String[] { "A", "B", "B" },
				ImCollections.addElement(list2, "B") );
	}
	
	@Test
	public void List_addElement_ImList_atIndex() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		
		assertListEquals(
				new String[] { "E" },
				ImCollections.addElement(list0, 0, "E") );
		assertListEquals(
				new String[] { "E", "A" },
				ImCollections.addElement(list1, 0, "E") );
		assertListEquals(
				new String[] { "A", "E" },
				ImCollections.addElement(list1, 1, "E") );
		assertListEquals(
				new String[] { "E", "A", "B" },
				ImCollections.addElement(list2, 0, "E") );
		assertListEquals(
				new String[] { "A", "E", "B" },
				ImCollections.addElement(list2, 1, "E") );
		assertListEquals(
				new String[] { "A", "B", "E" },
				ImCollections.addElement(list2, 2, "E") );
	}
	
	@Test
	public void List_addElement_ArrayList_atIndex() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		
		assertListEquals(
				new String[] { "E" },
				ImCollections.addElement(list0, 0, "E") );
		assertListEquals(
				new String[] { "E", "A" },
				ImCollections.addElement(list1, 0, "E") );
		assertListEquals(
				new String[] { "A", "E" },
				ImCollections.addElement(list1, 1, "E") );
		assertListEquals(
				new String[] { "E", "A", "B" },
				ImCollections.addElement(list2, 0, "E") );
		assertListEquals(
				new String[] { "A", "E", "B" },
				ImCollections.addElement(list2, 1, "E") );
		assertListEquals(
				new String[] { "A", "B", "E" },
				ImCollections.addElement(list2, 2, "E") );
	}
	
	@Test
	public void List_addElementIfAbsent_ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		
		assertListEquals(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list0, "A") );
		assertListEquals(
				new String[] { "A", "E" },
				ImCollections.addElementIfAbsent(list1, "E") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list1, "A") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list1, A_notIdentical) );
		assertListEquals(
				new String[] { "A", "B", "E" },
				ImCollections.addElementIfAbsent(list2, "E") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, "B") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, B_notIdentical) );
		
		assertTrue(list1 == ImCollections.addElementIfAbsent(list1, "A") );
		assertTrue(list2 == ImCollections.addElementIfAbsent(list2, "A") );
	}
	
	@Test
	public void List_addElementIfAbsent_ArrayList() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		
		assertListEquals(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list0, "A") );
		assertListEquals(
				new String[] { "A", "E" },
				ImCollections.addElementIfAbsent(list1, "E") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list1, "A") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list1, A_notIdentical) );
		assertListEquals(
				new String[] { "A", "B", "E" },
				ImCollections.addElementIfAbsent(list2, "E") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, "B") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, B_notIdentical) );
	}
	
	@Test
	public void List_removeElement_ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		final ImList<String> list3= newList("A", "B", "C");
		
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list0, "A") );
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list1, "A") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.removeElement(list1, "X") );
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list1, A_notIdentical) );
		assertListEquals(
				new String[] { "B" },
				ImCollections.removeElement(list2, "A") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.removeElement(list2, "B") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.removeElement(list2, "X") );
		assertListEquals(
				new String[] { "A", },
				ImCollections.removeElement(list2, B_notIdentical) );
		assertListEquals(
				new String[] { "B", "C" },
				ImCollections.removeElement(list3, "A") );
		assertListEquals(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, "B") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.removeElement(list3, "C") );
		assertListEquals(
				new String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, "X") );
		assertListEquals(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, B_notIdentical) );
	}
	
	@Test
	public void List_removeElement_ArrayList() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		final ArrayList<String> list3= newArrayList("A", "B", "C");
		
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list0, "A") );
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list1, "A") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.removeElement(list1, "X") );
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list1, A_notIdentical) );
		assertListEquals(
				new String[] { "B" },
				ImCollections.removeElement(list2, "A") );
		assertListEquals(
				new String[] { "A" },
				ImCollections.removeElement(list2, "B") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.removeElement(list2, "X") );
		assertListEquals(
				new String[] { "A", },
				ImCollections.removeElement(list2, B_notIdentical) );
		assertListEquals(
				new String[] { "B", "C" },
				ImCollections.removeElement(list3, "A") );
		assertListEquals(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, "B") );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.removeElement(list3, "C") );
		assertListEquals(
				new String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, "X") );
		assertListEquals(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, B_notIdentical) );
	}
	
	@Test
	public void List_removeElement_ImList_atIndex() {
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		final ImList<String> list3= newList("A", "B", "C");
		
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list1, 0) );
		assertListEquals(
				new String[] { "B" },
				ImCollections.removeElement(list2, 0) );
		assertListEquals(
				new String[] { "A" },
				ImCollections.removeElement(list2, 1) );
		assertListEquals(
				new String[] { "B", "C" },
				ImCollections.removeElement(list3, 0) );
		assertListEquals(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, 1) );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.removeElement(list3, 2) );
	}
	
	@Test
	public void List_removeElement_ArrayList_atIndex() {
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		final ArrayList<String> list3= newArrayList("A", "B", "C");
		
		assertListEquals(
				new String[] { },
				ImCollections.removeElement(list1, 0) );
		assertListEquals(
				new String[] { "B" },
				ImCollections.removeElement(list2, 0) );
		assertListEquals(
				new String[] { "A" },
				ImCollections.removeElement(list2, 1) );
		assertListEquals(
				new String[] { "B", "C" },
				ImCollections.removeElement(list3, 0) );
		assertListEquals(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, 1) );
		assertListEquals(
				new String[] { "A", "B" },
				ImCollections.removeElement(list3, 2) );
	}
	
	
	@Test
	public void IdentityList_concatList_2ImList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsIdentical(
				new String[] { },
				ImCollections.concatList(list0, emptyIdentityList()) );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.concatList(list1, emptyIdentityList()) );
		assertListEqualsIdentical(
				new String[] { "E" },
				ImCollections.concatList(list0, newIdentityList("E")) );
		assertListEqualsIdentical(
				new String[] { "A", "E" },
				ImCollections.concatList(list1, newIdentityList("E")) );
		assertListEqualsIdentical(
				new String[] { "A", "B", "C" },
				ImCollections.concatList(list3, emptyIdentityList()) );
		assertListEqualsIdentical(
				new String[] { "E", "F", "G" },
				ImCollections.concatList(list0, newIdentityList("E", "F", "G")) );
		assertListEqualsIdentical(
				new String[] { "A", "B", "C", "E", "F", "G" },
				ImCollections.concatList(list3, newIdentityList("E", "F", "G")) );
		
		assertTrue(list1 == ImCollections.concatList(list1, list0));
		assertTrue(list1 == ImCollections.concatList(list0, list1));
		assertTrue(list3 == ImCollections.concatList(list3, list0));
		assertTrue(list3 == ImCollections.concatList(list0, list3));
	}
	
	@Test
	public void IdentityList_addElement_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		
		assertListEqualsIdentical(
				new String[] { "E" },
				ImCollections.addElement(list0, "E") );
		assertListEqualsIdentical(
				new String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertListEqualsIdentical(
				new String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertListEqualsIdentical(
				new String[] { "A", "B", "B" },
				ImCollections.addElement(list2, "B") );
	}
	
	@Test
	public void IdentityList_addElement_ImIdentityList_atIndex() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		
		assertListEqualsIdentical(
				new String[] { "E" },
				ImCollections.addElement(list0, 0, "E") );
		assertListEqualsIdentical(
				new String[] { "E", "A" },
				ImCollections.addElement(list1, 0, "E") );
		assertListEqualsIdentical(
				new String[] { "A", "E" },
				ImCollections.addElement(list1, 1, "E") );
		assertListEqualsIdentical(
				new String[] { "E", "A", "B" },
				ImCollections.addElement(list2, 0, "E") );
		assertListEqualsIdentical(
				new String[] { "A", "E", "B" },
				ImCollections.addElement(list2, 1, "E") );
		assertListEqualsIdentical(
				new String[] { "A", "B", "E" },
				ImCollections.addElement(list2, 2, "E") );
	}
	
	@Test
	public void IdentityList_addElementIfAbsent_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list0, "A") );
		assertListEqualsIdentical(
				new String[] { "A", "E" },
				ImCollections.addElementIfAbsent(list1, "E") );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.addElementIfAbsent(list1, "A") );
		assertListEqualsIdentical(
				new String[] { "A", A_notIdentical },
				ImCollections.addElementIfAbsent(list1, A_notIdentical) );
		assertListEqualsIdentical(
				new String[] { "A", "B", "E" },
				ImCollections.addElementIfAbsent(list2, "E") );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, "B") );
		assertListEqualsIdentical(
				new String[] { "A", "B", B_notIdentical },
				ImCollections.addElementIfAbsent(list2, B_notIdentical) );
		
		assertTrue(list1 == ImCollections.addElementIfAbsent(list1, "A") );
		assertTrue(list2 == ImCollections.addElementIfAbsent(list2, "A") );
	}
	
	@Test
	public void IdentityList_removeElement_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsIdentical(
				new String[] { },
				ImCollections.removeElement(list0, "A") );
		assertListEqualsIdentical(
				new String[] { },
				ImCollections.removeElement(list1, "A") );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.removeElement(list1, "X") );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.removeElement(list1, A_notIdentical) );
		assertListEqualsIdentical(
				new String[] { "B" },
				ImCollections.removeElement(list2, "A") );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.removeElement(list2, "B") );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeElement(list2, "X") );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeElement(list2, B_notIdentical) );
		assertListEqualsIdentical(
				new String[] { "B", "C" },
				ImCollections.removeElement(list3, "A") );
		assertListEqualsIdentical(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, "B") );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeElement(list3, "C") );
		assertListEqualsIdentical(
				new String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, "X") );
		assertListEqualsIdentical(
				new String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, B_notIdentical) );
		assertListEqualsIdentical(
				new String[] { "B", "A" },
				ImCollections.removeElement(newIdentityList("A", "B", "A"), "A") );
		
		assertTrue(list1 == ImCollections.removeElement(list1, "X") );
		assertTrue(list2 == ImCollections.removeElement(list2, "X") );
		assertTrue(list3 == ImCollections.removeElement(list3, "X") );
	}
	
	@Test
	public void IdentityList_removeLastElement_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsIdentical(
				new String[] { },
				ImCollections.removeLastElement(list0, "A") );
		assertListEqualsIdentical(
				new String[] { },
				ImCollections.removeLastElement(list1, "A") );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.removeLastElement(list1, "X") );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.removeLastElement(list1, A_notIdentical) );
		assertListEqualsIdentical(
				new String[] { "B" },
				ImCollections.removeLastElement(list2, "A") );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.removeLastElement(list2, "B") );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeLastElement(list2, "X") );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeLastElement(list2, B_notIdentical) );
		assertListEqualsIdentical(
				new String[] { "B", "C" },
				ImCollections.removeLastElement(list3, "A") );
		assertListEqualsIdentical(
				new String[] { "A", "C" },
				ImCollections.removeLastElement(list3, "B") );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeLastElement(list3, "C") );
		assertListEqualsIdentical(
				new String[] { "A", "B", "C" },
				ImCollections.removeLastElement(list3, "X") );
		assertListEqualsIdentical(
				new String[] { "A", "B", "C" },
				ImCollections.removeLastElement(list3, B_notIdentical) );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeLastElement(newIdentityList("A", "B", "A"), "A") );
		
		assertTrue(list1 == ImCollections.removeLastElement(list1, "X") );
		assertTrue(list2 == ImCollections.removeLastElement(list2, "X") );
		assertTrue(list3 == ImCollections.removeLastElement(list3, "X") );
	}
	
	@Test
	public void IdentityList_removeElement_ImIdentityList_atIndex() {
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsIdentical(
				new String[] { },
				ImCollections.removeElement(list1, 0) );
		assertListEqualsIdentical(
				new String[] { "B" },
				ImCollections.removeElement(list2, 0) );
		assertListEqualsIdentical(
				new String[] { "A" },
				ImCollections.removeElement(list2, 1) );
		assertListEqualsIdentical(
				new String[] { "B", "C" },
				ImCollections.removeElement(list3, 0) );
		assertListEqualsIdentical(
				new String[] { "A", "C" },
				ImCollections.removeElement(list3, 1) );
		assertListEqualsIdentical(
				new String[] { "A", "B" },
				ImCollections.removeElement(list3, 2) );
	}
	
}
