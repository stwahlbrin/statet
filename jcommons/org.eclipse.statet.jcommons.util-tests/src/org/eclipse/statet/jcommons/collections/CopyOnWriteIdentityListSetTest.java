/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CopyOnWriteIdentityListSetTest extends AbstractMutableSetTest<CopyOnWriteIdentityListSet<String>> {
	
	
	public CopyOnWriteIdentityListSetTest() {
		super(CopyOnWriteIdentityListSet.class);
	}
	
	
	@Override
	protected CopyOnWriteIdentityListSet<String> createEmptyCollection() {
		return new CopyOnWriteIdentityListSet<>();
	}
	
	@Override
	protected CopyOnWriteIdentityListSet<String> createCollection(final String... elements) {
		final Set<String> s= new HashSet<>(Arrays.asList(elements));
		return new CopyOnWriteIdentityListSet<>(s);
	}
	
	
	@Override
	protected void assertCollectionEquals(final String... expected) {
		CollectionTests.assertListEqualsIdentical(expected, this.c.toList());
	}
	
	
	@Test
	public void clearToList() {
		ImIdentityList<String> r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.clearToList();
		assertCollectionEmpty();
		CollectionTests.assertListEqualsIdentical(new String[] { "A", "B", "C", "D", "E", "F" }, r);
		
		r= this.c.clearToList();
		assertCollectionEmpty();
		CollectionTests.assertListEqualsIdentical(new String[] {}, r);
	}
	
}
