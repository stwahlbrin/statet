/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AbstractImIntListTest {
	
	
	protected static final int VALUE_0= 1654;
	
	
	protected ImIntList list= nonNullLateInit();
	
	
	public AbstractImIntListTest() {
	}
	
	
	protected abstract ImIntList createList(int value0);
	
	protected abstract int getExpectedSize();
	
	@BeforeEach
	public void init() {
		this.list= createList(VALUE_0);
	}
	
	
	@Test
	public void size() {
		assertEquals(getExpectedSize(), this.list.size());
	}
	
	@Test
	public void isEmpty() {
		if (getExpectedSize() == 0) {
			assertTrue(this.list.isEmpty());
		}
		else {
			assertFalse(this.list.isEmpty());
		}
	}
	
	@Test
	public void contains() {
		if (getExpectedSize() > 0) {
			assertTrue(this.list.contains(VALUE_0));
		}
		assertFalse(this.list.contains(0));
	}
	
	@Test
	public void getAt() {
		if (getExpectedSize() > 0) {
			assertEquals(VALUE_0, this.list.getAt(0));
		}
		
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.list.getAt(getExpectedSize());
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.list.getAt(-1);
		});
	}
	
	@Test
	public void indexOf() {
		if (getExpectedSize() > 0) {
			assertEquals(0, this.list.indexOf(VALUE_0));
		}
		assertEquals(-1, this.list.indexOf(0));
	}
	
	@Test
	public void lastIndexOf() {
		if (getExpectedSize() > 0) {
			assertEquals(0, this.list.lastIndexOf(VALUE_0));
		}
		assertEquals(-1, this.list.lastIndexOf(0));
	}
	
	
	protected void assertNotModified() {
		assertEquals(getExpectedSize(), this.list.size());
		if (getExpectedSize() > 0) {
			assertEquals(VALUE_0, this.list.getAt(0));
		}
	}
	
	@Test
	public void add() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.add(2);
		});
		assertNotModified();
	}
	
	@Test
	public void addAt() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.addAt(0, 2);
		});
		assertNotModified();
	}
	
	@Test
	public void setAt() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.setAt(0, 2);
		});
		assertNotModified();
	}
	
	@Test
	public void remove() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.remove(VALUE_0);
		});
		assertNotModified();
	}
	
	@Test
	public void removeAt() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.removeAt(0);
		});
		assertNotModified();
	}
	
	@Test
	public void clear() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.clear();
		});
		assertNotModified();
	}
	
	
	@Test
	public void toArray() {
		final int[] array= this.list.toArray();
		assertEquals(getExpectedSize(), array.length);
		if (getExpectedSize() > 0) {
			assertEquals(VALUE_0, array[0]);
		}
	}
	
	
	@Test
	public void equals() {
		assertTrue(this.list.equals(createList(VALUE_0)));
		if (getExpectedSize() > 0) {
			assertFalse(this.list.equals(createList(0)));
			assertFalse(this.list.equals(ImCollections.emptyIntList()));
		}
		assertFalse(this.list.equals(ImCollections.newIntList(VALUE_0, VALUE_0)));
	}
	
	
	@Test
	public void toArrayEquals() {
		assertTrue(this.list.equals(ImCollections.newIntList(this.list.toArray())));
	}
	
}
