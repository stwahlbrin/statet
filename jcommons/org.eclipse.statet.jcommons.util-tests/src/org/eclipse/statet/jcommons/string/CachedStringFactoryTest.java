/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CachedStringFactoryTest extends BasicStringFactoryTest {
	
	
	@Override
	protected StringFactory create() {
		return new CacheStringFactory(10);
	}
	
	
	@Test
	public void get_CharSequence_cached() {
		this.factory= create();
		final String s= this.factory.get(new StringBuilder("ab"));
		assertEquals("ab", s);
		assertTrue(s == this.factory.get(new StringBuilder("ab")));
	}
	
	@Test
	public void get_CharArrayString_cached() {
		this.factory= create();
		final String s= this.factory.get(new CharArrayString("ab"));
		assertEquals("ab", s);
		assertTrue(s == this.factory.get(new CharArrayString("ab")));
	}
	
	@Test
	public void get_String_cached() {
		this.factory= create();
		final String s= this.factory.get(new String("ab"));
		assertEquals("ab", s);
		assertTrue(s == this.factory.get(new String("ab")));
	}
	
}
