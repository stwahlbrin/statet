/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class BasicStringFactoryTest {
	
	
	protected StringFactory factory= nonNullLateInit();
	
	
	public BasicStringFactoryTest() {
	}
	
	
	protected StringFactory create() {
		return BasicStringFactory.INSTANCE;
	}
	
	
	@Test
	public void get_CharSequence() {
		this.factory= create();
		final String s= this.factory.get(new StringBuilder("ab"));
		assertEquals("ab", s);
	}
	
	@Test
	public void get_CharSequence_emtpy() {
		this.factory= create();
		final String s= this.factory.get(new StringBuilder(""));
		assertTrue("" == s);
	}
	
	@Test
	public void get_CharSequence_asciiChar() {
		this.factory= create();
		final String s= this.factory.get(new StringBuilder("a"));
		assertTrue("a" == s);
	}
	
	
	@Test
	public void get_CharArrayString() {
		this.factory= create();
		final String s= this.factory.get(new CharArrayString("ab"));
		assertEquals("ab", s);
	}
	
	@Test
	public void get_CharArrayString_emtpy() {
		this.factory= create();
		final String s= this.factory.get(new CharArrayString(""));
		assertTrue("" == s);
	}
	
	@Test
	public void get_CharArrayString_asciiChar() {
		this.factory= create();
		final String s= this.factory.get(new CharArrayString("a"));
		assertTrue("a" == s);
	}
	
	
	@Test
	public void get_String() {
		this.factory= create();
		final String s= this.factory.get(new String("ab"));
		assertEquals("ab", s);
	}
	
	@Test
	public void get_String_emtpy() {
		this.factory= create();
		final String s= this.factory.get(new String(""));
		assertTrue("" == s);
	}
	
	@Test
	public void get_String_asciiChar() {
		this.factory= create();
		final String s= this.factory.get(new String("a"));
		assertTrue("a" == s);
	}
	
	
	@Test
	public void get_Char_asciiChar() {
		this.factory= create();
		final String s= this.factory.get('a');
		assertTrue("a" == s);
	}
	
}
