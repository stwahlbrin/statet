/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class UriUtilsTest {
	
	
	public UriUtilsTest() {
	}
	
	
	@Test
	public void isFileUrl() throws URISyntaxException {
		assertEquals(true, UriUtils.isFileUrl(new URI("file", "/test/file.jar", null)));
		assertEquals(true, UriUtils.isFileUrl(new URI("FILE", "/test/file.jar", null)));
		assertEquals(true, UriUtils.isFileUrl(new URI("File", "/test/file.jar", null)));
		
		assertEquals(false, UriUtils.isFileUrl(new URI(null, "", null)));
		assertEquals(false, UriUtils.isFileUrl(new URI(null, "file", null)));
		assertEquals(false, UriUtils.isFileUrl(new URI(null, "file/test/file.jar!/", null)));
		assertEquals(false, UriUtils.isFileUrl(new URI("filx", "/test/file.jar!/", null)));
		assertEquals(false, UriUtils.isFileUrl(new URI("filesystem", "/test/file.jar", null)));
		assertEquals(false, UriUtils.isFileUrl(new URI("jar", "/test/file.jar", null)));
	}
	
	@Test
	public void isFileUrlString() {
		assertEquals(true, UriUtils.isFileUrl("file:/test/file.jar"));
		assertEquals(true, UriUtils.isFileUrl("FILE:/test/file.jar"));
		assertEquals(true, UriUtils.isFileUrl("File:/test/file.jar"));
		
		assertEquals(false, UriUtils.isFileUrl(""));
		assertEquals(false, UriUtils.isFileUrl("file"));
		assertEquals(false, UriUtils.isFileUrl("file/test/file.jar"));
		assertEquals(false, UriUtils.isFileUrl("filÉ:/test/file.jar"));
		assertEquals(false, UriUtils.isFileUrl("filesystem:/test/file.jar"));
		assertEquals(false, UriUtils.isFileUrl("jar:/test/file.jar"));
	}
	
	
	@Test
	public void isJarUrl() throws URISyntaxException {
		assertEquals(true, UriUtils.isJarUrl(new URI("jar", "file:/test/file.jar!/", null)));
		assertEquals(true, UriUtils.isJarUrl(new URI("JAR", "file:/test/file.jar!/", null)));
		assertEquals(true, UriUtils.isJarUrl(new URI("Jar", "file:/test/file.jar!/", null)));
		
		assertEquals(false, UriUtils.isJarUrl(new URI(null, "", null)));
		assertEquals(false, UriUtils.isJarUrl(new URI(null, "jar", null)));
		assertEquals(false, UriUtils.isJarUrl(new URI(null, "jar/test/file.jar!/", null)));
		assertEquals(false, UriUtils.isJarUrl(new URI("jarfile", "/test/file.jar!/", null)));
		assertEquals(false, UriUtils.isJarUrl(new URI("file", "/test/file.jar!/", null)));
	}
	
	@Test
	public void isJarUrlString() {
		assertEquals(true, UriUtils.isJarUrl("jar:file:/test/file.jar!/"));
		assertEquals(true, UriUtils.isJarUrl("JAR:file:/test/file.jar!/"));
		assertEquals(true, UriUtils.isJarUrl("Jar:file:/test/file.jar!/"));
		
		assertEquals(false, UriUtils.isJarUrl(""));
		assertEquals(false, UriUtils.isJarUrl("jar"));
		assertEquals(false, UriUtils.isJarUrl("jar/test/file.jar!/"));
		assertEquals(false, UriUtils.isJarUrl("jax:/test/file.jar!/"));
		assertEquals(false, UriUtils.isJarUrl("jarfile:/test/file.jar!/"));
		assertEquals(false, UriUtils.isJarUrl("file:/test/file.jar!/"));
	}
	
	@Test
	public void getJarFileUrl_requireJarUrl() throws URISyntaxException {
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.getJarFileUrl(new URI("file:/test/file.jar"));
		});
		assertThrows(URISyntaxException.class, () -> {
			UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar"));
		});
	}
	
	@Test
	public void getJarFileUrl() throws URISyntaxException {
		assertEquals(new URI("file:/test/file.jar"),
				UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar!/")) );
		
		assertEquals(new URI("file:/test/file.jar"),
				UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar!/path/file.txt")) );
		
		assertEquals(new URI("jar:file:/test/file.jar!/path/nested.jar"),
				UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar!/path/nested.jar!/")) );
	}
	
	@Test
	public void getJarEntryPath_requireJarUrl() throws URISyntaxException {
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.getJarEntryPath(new URI("file:/test/file.jar"));
		});
		assertThrows(URISyntaxException.class, () -> {
			UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar"));
		});
	}
	
	@Test
	public void getJarEntryPath() throws URISyntaxException {
		assertEquals("",
				UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar!/")) );
		
		assertEquals("path/file.txt",
				UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar!/path/file.txt")) );
		
		assertEquals("",
				UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar!/path/nested.jar!/")) );
	}
	
	@Test
	public void toJarUrl() throws URISyntaxException {
		assertEquals(new URI("jar:file:/test/file.jar!/"),
				UriUtils.toJarUrl("file:/test/file.jar") );
		
		assertEquals(new URI("jar:file:/test/file.jar!/path/nested.jar!/"),
				UriUtils.toJarUrl("jar:file:/test/file.jar!/path/nested.jar") );
	}
	
	@Test
	public void toJarUrlString() throws URISyntaxException {
		assertEquals("jar:file:/test/file.jar!/",
				UriUtils.toJarUrlString("file:/test/file.jar") );
		
		assertEquals("jar:file:/test/file.jar!/path/nested.jar!/",
				UriUtils.toJarUrlString("jar:file:/test/file.jar!/path/nested.jar") );
	}
	
	
	@Test
	public void encodePathSegment() {
		assertEquals("path%3C%3Esegment",
				UriUtils.encodePathSegment("path<>segment") );
		
		assertEquals("%3C%3E",
				UriUtils.encodePathSegment("<>") );
		assertEquals("path%3C%3E",
				UriUtils.encodePathSegment("path<>") );
		assertEquals("%3C%3Esegment",
				UriUtils.encodePathSegment("<>segment") );
		assertEquals("pathsegment",
				UriUtils.encodePathSegment("pathsegment") );
		
		assertEquals("%25",
				UriUtils.encodePathSegment("%") );
		assertEquals("%2F",
				UriUtils.encodePathSegment("/") );
		assertEquals("%3F",
				UriUtils.encodePathSegment("?") );
		assertEquals("%23",
				UriUtils.encodePathSegment("#") );
		assertEquals("~",
				UriUtils.encodePathSegment("~") );
		assertEquals("%C3%A1%C4%81",
				UriUtils.encodePathSegment("áā") );
		assertEquals("%F0%90%86%90",
				UriUtils.encodePathSegment("\uD800\uDD90") );
	}
	
	@Test
	public void encodeFragment() {
		assertEquals("fragment%3C%3E123",
				UriUtils.encodeFragment("fragment<>123") );
		
		assertEquals("%3C%3E",
				UriUtils.encodeFragment("<>") );
		assertEquals("fragment%3C%3E",
				UriUtils.encodeFragment("fragment<>") );
		assertEquals("%3C%3E123",
				UriUtils.encodeFragment("<>123") );
		assertEquals("fragment123",
				UriUtils.encodeFragment("fragment123") );
		
		assertEquals("%25",
				UriUtils.encodeFragment("%") );
		assertEquals("/",
				UriUtils.encodeFragment("/") );
		assertEquals("?",
				UriUtils.encodeFragment("?") );
		assertEquals("%23",
				UriUtils.encodeFragment("#") );
		assertEquals("~",
				UriUtils.encodeFragment("~") );
		assertEquals("%C3%A1%C4%81",
				UriUtils.encodeFragment("áā") );
		assertEquals("%F0%90%86%90",
				UriUtils.encodeFragment("\uD800\uDD90") );
	}
	
}
