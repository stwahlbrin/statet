/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.texteditor.IUpdate;
import org.eclipse.ui.texteditor.IWorkbenchActionDefinitionIds;

import org.eclipse.statet.ecommons.ui.SharedMessages;


/**
 * Common function for actions that operate on a text viewer.
 * <p>
 * Clients may subclass this class.</p>
 * 
 * TODO replace with handler implementation
 */
public class TextViewerAction extends Action implements IUpdate {
	
	
	private final ITextOperationTarget fOperationTarget;
	private final int fOperationCode;
	
	
	/**
	 * Constructs a new action in the given text viewer with
	 * the specified operation code.
	 * 
	 * @param viewer
	 * @param operationCode
	 */
	public TextViewerAction(final ITextViewer viewer, final int operationCode) {
		assert (viewer != null);
		assert (operationCode != -1);
		
		this.fOperationCode= operationCode;
		this.fOperationTarget= viewer.getTextOperationTarget();
		update();
	}
	
	
	@Override
	public void update() {
		setEnabled(this.fOperationTarget.canDoOperation(this.fOperationCode));
	}
	
	@Override
	public void run() {
		if (this.fOperationTarget.canDoOperation(this.fOperationCode)) {
			this.fOperationTarget.doOperation(this.fOperationCode);
		}
	}
	
	
	public static TextViewerAction createDeleteAction(final ITextViewer viewer) {
		final TextViewerAction action= new TextViewerAction(viewer, ITextOperationTarget.DELETE);
		action.setId(ActionFactory.DELETE.getId());
		action.setActionDefinitionId(IWorkbenchActionDefinitionIds.DELETE);
		action.setText(SharedMessages.DeleteAction_name);
		action.setToolTipText(SharedMessages.DeleteAction_tooltip);
		return action;
	}
	
	public static TextViewerAction createCutAction(final ITextViewer viewer) {
		final TextViewerAction action= new TextViewerAction(viewer, ITextOperationTarget.CUT);
		action.setId(ActionFactory.CUT.getId());
		action.setActionDefinitionId(IWorkbenchActionDefinitionIds.CUT);
		action.setText(SharedMessages.CutAction_name);
		action.setToolTipText(SharedMessages.CutAction_tooltip);
		return action;
	}
	
	public static TextViewerAction createCopyAction(final ITextViewer viewer) {
		final TextViewerAction action= new TextViewerAction(viewer, ITextOperationTarget.COPY);
		action.setId(ActionFactory.COPY.getId());
		action.setActionDefinitionId(IWorkbenchActionDefinitionIds.COPY);
		action.setText(SharedMessages.CopyAction_name);
		action.setToolTipText(SharedMessages.CopyAction_tooltip);
		return action;
	}
	
	public static TextViewerAction createPasteAction(final ITextViewer viewer) {
		final TextViewerAction action= new TextViewerAction(viewer, ITextOperationTarget.PASTE);
		action.setId(ActionFactory.PASTE.getId());
		action.setActionDefinitionId(IWorkbenchActionDefinitionIds.PASTE);
		action.setText(SharedMessages.PasteAction_name);
		action.setToolTipText(SharedMessages.PasteAction_tooltip);
		return action;
	}
	
	public static TextViewerAction createSelectAllAction(final ITextViewer viewer) {
		final TextViewerAction action= new TextViewerAction(viewer, ITextOperationTarget.SELECT_ALL);
		action.setId(ActionFactory.SELECT_ALL.getId());
		action.setActionDefinitionId(IWorkbenchActionDefinitionIds.SELECT_ALL);
		action.setText(SharedMessages.SelectAllAction_name);
		action.setToolTipText(SharedMessages.SelectAllAction_tooltip);
		return action;
	}
	
	public static TextViewerAction createUndoAction(final ITextViewer viewer) {
		final TextViewerAction action= new TextViewerAction(viewer, ITextOperationTarget.UNDO);
		action.setId(ActionFactory.UNDO.getId());
		action.setActionDefinitionId(IWorkbenchActionDefinitionIds.UNDO);
		action.setText(SharedMessages.UndoAction_name);
		action.setToolTipText(SharedMessages.UndoAction_tooltip);
		return action;
	}
	
	public static TextViewerAction createRedoAction(final ITextViewer viewer) {
		final TextViewerAction action= new TextViewerAction(viewer, ITextOperationTarget.REDO);
		action.setId(ActionFactory.REDO.getId());
		action.setActionDefinitionId(IWorkbenchActionDefinitionIds.REDO);
		action.setText(SharedMessages.RedoAction_name);
		action.setToolTipText(SharedMessages.RedoAction_tooltip);
		return action;
	}
	
}
