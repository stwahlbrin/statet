/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.presentation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.rules.BufferedRuleBasedScanner;
import org.eclipse.jface.text.rules.IRule;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager.TextStyleToken;


/**
 * BufferedRuleBasedScanner with managed text styles/tokens.
 */
@NonNullByDefault
public abstract class AbstractRuleBasedScanner extends BufferedRuleBasedScanner {
	
	
	private final TextStyleManager<?> textStyles;
	
	
	public AbstractRuleBasedScanner(final TextStyleManager<?> textStyles) {
		this.textStyles= textStyles;
	}
	
	
	protected void initRules() {
		final List<IRule> rules= new ArrayList<>();
		createRules(rules);
		setRules(rules.toArray(new IRule[rules.size()]));
	}
	
	protected abstract void createRules(final List<IRule> rules);
	
	
	protected TextStyleToken<?> getToken(final String key) {
		return this.textStyles.getToken(key);
	}
	
}
