/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates;

import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateVariableResolver;

import org.eclipse.statet.internal.ltk.ui.TemplatesMessages;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


public abstract class SourceUnitVariableResolver extends TemplateVariableResolver {
	
	
	public static final String FILENAME_TYPE= "file_name"; //$NON-NLS-1$
	
	
	public static class FileName extends SourceUnitVariableResolver {
		
		public FileName() {
			super(FILENAME_TYPE, TemplatesMessages.Templates_Variable_File_description);
		}
		
		@Override
		protected String resolve(final TemplateContext context) {
			final SourceUnit su= ((IWorkbenchTemplateContext) context).getSourceUnit();
			if (su != null) {
				return su.getElementName().getDisplayName();
			}
			return null;
		}
		
	}
	
	
	protected SourceUnitVariableResolver(final String type, final String description) {
		super(type, description);
	}
	
	@Override
	protected boolean isUnambiguous(final TemplateContext context) {
		return (((IWorkbenchTemplateContext) context).getSourceUnit() != null);
	}
	
}
