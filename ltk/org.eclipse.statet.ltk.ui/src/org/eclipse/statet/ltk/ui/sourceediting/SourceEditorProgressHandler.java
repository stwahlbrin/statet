/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ltk.ui.EditingMessages;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public abstract class SourceEditorProgressHandler extends AbstractHandler {
	
	
	private final SourceEditor editor;
	
	
	public SourceEditorProgressHandler(final SourceEditor editor) {
		this.editor= editor;
	}
	
	
	protected abstract String getTaskLabel();
	
	protected abstract boolean isEditTask();
	
	
	protected @Nullable SourceEditor getEditor(final @Nullable Object context) {
		return this.editor;
	}
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		final SourceEditor editor= getEditor(evaluationContext);
		setBaseEnabled(editor != null
				&& (!isEditTask() || editor.isEditable(false)) );
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final SourceEditor editor= getEditor(event.getApplicationContext());
		if (editor == null) {
			return null;
		}
		if (!editor.isEditable(true)) {
			return null;
		}
		final SourceUnit su= editor.getSourceUnit();
		final ITextSelection selection= (ITextSelection) editor.getViewer().getSelection();
		if (su == null || selection == null) {
			return null;
		}
		try {
			PlatformUI.getWorkbench().getProgressService().busyCursorWhile(new IRunnableWithProgress() {
				@Override
				public void run(final IProgressMonitor monitor)
						throws InvocationTargetException, InterruptedException {
					try {
						doExecute(editor, su, selection, monitor);
					}
					catch (final Exception e) {
						throw new InvocationTargetException(e);
					}
				}
			});
		}
		catch (final InvocationTargetException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, BUNDLE_ID, -1,
					NLS.bind(EditingMessages.GenericAction_error_message, getTaskLabel()),
					e.getTargetException() ));
		}
		catch (final InterruptedException e) {}
		return null;
	}
	
	
	protected abstract void doExecute(SourceEditor editor, SourceUnit su,
			ITextSelection selection, IProgressMonitor monitor) throws Exception;
	
}
