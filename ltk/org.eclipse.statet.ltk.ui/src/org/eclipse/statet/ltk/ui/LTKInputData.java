/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.part.ShowInContext;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


/**
 * Data/state of LTK based input of a view/editor.
 * 
 * E.g. used for {@link SelectionWithElementInfoListener} or {@link ShowInContext}
 */
@NonNullByDefault
public class LTKInputData implements ISelection {
	
	
	protected final SourceUnit inputElement;
	protected @Nullable SourceUnitModelInfo inputInfo;
	
	protected final @Nullable ISelectionProvider selectionProvider;
	protected ISelection selection;
	protected @Nullable AstSelection astSelection;
	protected @Nullable SourceStructElement<?, ?> modelSelection;
	
	
	public LTKInputData(final SourceUnit inputElement, final ISelection selection) {
		this.inputElement= inputElement;
		this.selectionProvider= null;
		this.selection= selection;
	}
	
	public LTKInputData(final SourceUnit inputElement, final ISelectionProvider selectionProvider) {
		this.inputElement= inputElement;
		this.selectionProvider= selectionProvider;
		this.selection= selectionProvider.getSelection();
	}
	
	
	public boolean update() {
		if (this.selectionProvider != null) {
			final ISelection selection= this.selectionProvider.getSelection();
			if (!selection.equals(this.selection)) {
				this.astSelection= null;
				this.modelSelection= null;
			}
			return true;
		}
		return false;
	}
	
	public LtkModelElement<?> getInputElement() {
		return this.inputElement;
	}
	
	public @Nullable SourceUnitModelInfo getInputInfo() {
		SourceUnitModelInfo inputInfo= this.inputInfo;
		if (inputInfo == null) {
			inputInfo= this.inputElement.getModelInfo(null, ModelManager.NONE, new NullProgressMonitor());
			this.inputInfo= inputInfo;
		}
		return inputInfo;
	}
	
	
	@Override
	public boolean isEmpty() {
		return this.selection.isEmpty();
	}
	
	public ISelection getSelection() {
		return this.selection;
	}
	
	public @Nullable AstSelection getAstSelection() {
		AstSelection astSelection= this.astSelection;
		if (astSelection == null) {
			final SourceUnitModelInfo modelInfo;
			if (this.selection instanceof ITextSelection && (modelInfo= getInputInfo()) != null) {
				final ITextSelection textSelection= (ITextSelection)this.selection;
				astSelection= AstSelection.search(modelInfo.getAst().getRoot(),
						textSelection.getOffset(), textSelection.getOffset()+textSelection.getLength(),
						AstSelection.MODE_COVERING_SAME_LAST );
				this.astSelection= astSelection;
			}
		}
		return astSelection;
	}
	
	public @Nullable SourceStructElement<?, ?> getModelSelection() {
		SourceStructElement<?, ?> modelSelection= this.modelSelection;
		if (modelSelection == null) {
			final SourceUnitModelInfo modelInfo;
			if (this.selection instanceof ITextSelection && (modelInfo= getInputInfo()) != null) {
				final ITextSelection textSelection= (ITextSelection)this.selection;
				modelSelection= LtkModelUtils.getCoveringSourceElement(modelInfo.getSourceElement(),
						textSelection.getOffset(), textSelection.getOffset() + textSelection.getLength() );
				this.modelSelection= modelSelection;
			}
		}
		return modelSelection;
	}
	
	
	public boolean isStillValid() {
		return true;
	}
	
}
