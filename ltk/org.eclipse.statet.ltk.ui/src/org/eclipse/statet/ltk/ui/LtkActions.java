/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;


/**
 * 
 * @see org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds
 */
public class LtkActions {
	
	
//--Edit--
	
	/**
	 * ID of command [Edit] 'Copy (Qualified) Element Name'.
	 * 
	 * Value: @value
	 */
	public static final String COPY_ELEMENT_NAME_COMMAND_ID=
			"org.eclipse.statet.workbench.commands.CopyElementName"; //$NON-NLS-1$
	
	/**
	 * ID of command [Edit] 'Raw Paste'.
	 * 
	 * Value: @value
	 */
	public static final String PASTE_RAW_COMMAND_ID=
			"org.eclipse.statet.workbench.commands.PasteRaw"; //$NON-NLS-1$
	
	/**
	 * ID of command [Edit > Expand Selection] 'Select Enclosing Element'.
	 * 
	 * Value: @value
	 */
	public static final String SELECT_ENCLOSING_COMMAND_ID=
			"org.eclipse.statet.workbench.commands.SelectExpandEnclosing"; //$NON-NLS-1$
	
	/**
	 * ID of command [Edit > Expand Selection] 'Select Next Element'.
	 * 
	 * Value: @value
	 */
	public static final String SELECT_NEXT_COMMAND_ID=
			"org.eclipse.statet.workbench.commands.SelectExpandNext"; //$NON-NLS-1$
	
	/**
	 * ID of command [Edit > Expand Selection] 'Select Previous Element'.
	 * 
	 * Value: @value
	 */
	public static final String SELECT_PREVIOUS_COMMAND_ID=
			"org.eclipse.statet.workbench.commands.SelectExpandPrevious"; //$NON-NLS-1$
	
	/**
	 * ID of command [Edit > Expand Selection] 'Restore Last Selection'.
	 * 
	 * Value: @value
	 */
	public static final String SELECT_LAST_COMMAND_ID=
			"org.eclipse.statet.workbench.commands.SelectLast"; //$NON-NLS-1$
	
	
//--Navigation--
	
	/**
	 * ID of command [Navigate] 'Go to Matching Bracket'.
	 * 
	 * Value: @value
	 */
	public static final String GOTO_MATCHING_BRACKET_COMMAND_ID=
			"org.eclipse.statet.ecommons.text.commands.GotoMatchingBracket"; //$NON-NLS-1$
	
	
//--Show--
	
	/**
	 * ID of command 'Show Quick Outline'.
	 * 
	 * Value: @value
	 */
	public static final String SHOW_QUICK_SOURCE_OUTLINE_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.ShowQuickSourceOutline"; //$NON-NLS-1$
	
	/**
	 * ID of command 'Show Quick Element Structure'.
	 * 
	 * Value: @value
	 */
	public static final String SHOW_QUICK_ELEMENT_OUTLINE_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.ShowQuickElementOutline"; //$NON-NLS-1$
	
	/**
	 * ID of command 'Show Quick Element Hierarchy'.
	 * 
	 * Value: @value
	 */
	public static final String SHOW_QUICK_ELEMENT_HIERARCHY_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.ShowQuickElementHierarchy"; //$NON-NLS-1$
	
	
//--Edit Source (Assists)--
	
	/**
	 * ID for command to invoke a specific content assist (with a single content assist category).
	 * 
	 * Command required attribute {@link #SPECIFIC_CONTENT_ASSIST_CATEGORY_PARAMETER_ID}.
	 */
	public static final String SPECIFIC_CONTENT_ASSIST_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.SpecificContentAssist"; //$NON-NLS-1$
	
	/**
	 * ID for command parameter for {@link #SPECIFIC_CONTENT_ASSIST_COMMAND_ID} specifying the category to show.
	 */
	public static final String SPECIFIC_CONTENT_ASSIST_CATEGORY_PARAMETER_ID= "categoryId"; //$NON-NLS-1$
	
	/**
	 * ID of command [Source] 'Toggle Comment'.
	 * 
	 * Value: @value
	 */
	public static final String TOGGLE_COMMENT=
			"org.eclipse.statet.ltk.commands.ToggleComment"; //$NON-NLS-1$
	
	/**
	 * ID of command [Source] 'Strip Comments'.
	 * 
	 * Value: @value
	 */
	public static final String STRIP_COMMENTS_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.StripComments"; //$NON-NLS-1$
	
	/**
	 * ID of command [Source] 'Add Doc Comment'.
	 * 
	 * Value: @value
	 */
	public static final String ADD_DOC_COMMENT_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.AddDocComment"; //$NON-NLS-1$

	/**
	 * ID of command [Source] 'Generate Element Comment'.
	 * 
	 * Value: @value
	 */
	public static final String GENERATE_ELEMENT_COMMENT_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.GenerateElementComment"; //$NON-NLS-1$
	
	
	/**
	 * ID of command [Source] 'Insert Assignment'.
	 * 
	 * Value: @value
	 */
	public static final String INSERT_ASSIGNMENT_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.InsertAssignment"; //$NON-NLS-1$
	
	/**
	 * ID of command [Source] 'Correct Indentation'.
	 * 
	 * Value: @value
	 */
	public static final String CORRECT_INDENT_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.CorrectIndent"; //$NON-NLS-1$
	
	/**
	 * ID of command [Source] 'Correct Line Wrap'.
	 * 
	 * Value: @value
	 */
	public static final String CORRECT_LINE_WRAP_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.CorrectLineWrap"; //$NON-NLS-1$
	
	
	/**
	 * ID of command 'Quick Assist - Rename in File'.
	 * 
	 * Value: @value
	 */
	public static final String QUICK_ASSIST_RENAME_IN_FILE=
			"org.eclipse.statet.ltk.commands.QuickAssistRenameInFile";
	
	/**
	 * ID of command 'Quick Assist - Convert to Pipe Forward Statement'.
	 * 
	 * Value: @value
	 */
	public static final String QUICK_ASSIST_CONVERT_TO_PIPE_FORWARD=
			"org.eclipse.statet.ltk.commands.QuickAssistConvertToPipeForward";
	
	
//--Search--
	
	/**
	 * ID of command to search for access (all occurrences) of an element.
	 * 
	 * Value: @value
	 */
	public static final String SEARCH_ALL_ELEMENT_ACCESS_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.SearchAllElementAccess"; //$NON-NLS-1$
	
	/**
	 * ID of command to search for write access of an element.
	 * 
	 * Value: @value
	 */
	public static final String SEARCH_WRITE_ELEMENT_ACCESS_COMMAND_ID=
			"org.eclipse.statet.ltk.commands.SearchWriteElementAccess"; //$NON-NLS-1$
	
	/**
	 * Name of command parameter defining the scope of a search.
	 * 
	 * Value: @value
	 */
	public static final String SEARCH_SCOPE_PARAMETER_ID= "scope"; //$NON-NLS-1$
	
	public static final String SEARCH_SCOPE_WORKSPACE_PARAMETER_VALUE= "workspace"; //$NON-NLS-1$
	public static final String SEARCH_SCOPE_PROJECT_PARAMETER_VALUE= "project"; //$NON-NLS-1$
	public static final String SEARCH_SCOPE_FILE_PARAMETER_VALUE= "file"; //$NON-NLS-1$
	
	
}
