/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Computes quick fix or assist information displayed by the editor quick assistant.
 * Contributions to the extension point <code>org.eclipse.statet.ltk.advancedQuickAssist</code>
 * must implement this interface.
 */
@NonNullByDefault
public interface QuickAssistComputer {
	
	
	/**
	 * Returns a list of assist proposals valid at the given invocation context.
	 * 
	 * @param context the context of the quick assist invocation
	 * @param proposals a set collecting the completion proposals
	 * @param monitor a progress monitor to report progress. The monitor is private to this
	 *     invocation, i.e. there is no need for the receiver to spawn a sub monitor.
	 */
	void computeAssistProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor);
	
}
