/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.input;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IPersistableElement;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.input.SourceFragment;


@NonNullByDefault
public class BasicSourceFragmentEditorInput<TFragment extends SourceFragment>
		implements SourceFragmentEditorInput {
	
	
	protected final TFragment fragment;
	
	
	public BasicSourceFragmentEditorInput(final TFragment fragment) {
		this.fragment= nonNullAssert(fragment);
	}
	
	
	@Override
	public SourceFragment getSourceFragment() {
		return this.fragment;
	}
	
	
	@Override
	public boolean exists() {
		return false;
	}
	
	@Override
	public @Nullable ImageDescriptor getImageDescriptor() {
		return null;
	}
	
	@Override
	public String getName() {
		return this.fragment.getName();
	}
	
	@Override
	public String getToolTipText() {
		return this.fragment.getFullName();
	}
	
	
	@Override
	public @Nullable IPersistableElement getPersistable() {
		return null;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return this.fragment.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof BasicSourceFragmentEditorInput
				&& getClass() == obj.getClass()) {
			final BasicSourceFragmentEditorInput<?> other= (BasicSourceFragmentEditorInput<?>)obj;
			return this.fragment.equals(other.fragment);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.fragment.getFullName();
	}
	
}
