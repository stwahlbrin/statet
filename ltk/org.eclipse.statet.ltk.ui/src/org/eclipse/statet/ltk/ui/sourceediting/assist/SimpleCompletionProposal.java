/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.SharedUIResources;


/**
 * Proposal completing a given replacement string.
 */
@NonNullByDefault
public abstract class SimpleCompletionProposal<TContext extends AssistInvocationContext>
		extends SourceProposal<TContext> {
	
	
	/** The replacement string. */
	private final String replacementString;
	
	
	public SimpleCompletionProposal(final ProposalParameters<? extends TContext> parameters,
			final String replacementString) {
		super(parameters);
		
		this.replacementString= replacementString;
	}
	
	
	@Override
	protected final String getName() {
		return this.replacementString;
	}
	
	
	@Override
	public String getSortingString() {
		return this.replacementString;
	}
	
	
	@Override
	public Image getImage() {
		return SharedUIResources.getImages().get(SharedUIResources.PLACEHOLDER_IMAGE_ID);
	}
	
	
}
