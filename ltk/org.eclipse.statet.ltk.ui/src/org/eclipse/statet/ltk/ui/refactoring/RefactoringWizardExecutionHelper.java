/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.refactoring;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Shell;


/**
 * Helper to activate the UI of a refactoring.
 */
public class RefactoringWizardExecutionHelper {
	
	
	private final RefactoringWizard wizard;
	private final int saveMode;
	private final boolean build;
	
	private RefactoringStatus status;
	
	
	public RefactoringWizardExecutionHelper(final RefactoringWizard wizard, final int saveMode) {
		this(wizard, saveMode, false);
	}
	
	public RefactoringWizardExecutionHelper(final RefactoringWizard wizard, final int saveMode, final boolean build) {
		this.wizard= wizard;
		this.saveMode= saveMode;
		this.build= build;
	}
	
	
	public boolean perform(final Shell parent) {
		final RefactoringSaveHelper saveHelper= new RefactoringSaveHelper(this.saveMode);
		if (!saveHelper.saveEditors(parent)
				&& (this.saveMode & RefactoringSaveHelper.OPTIONAL) == 0) {
			return false;
		}
		try {
			if (this.build) {
				saveHelper.triggerBuild();
			}
			final RefactoringWizardOpenOperation op= new RefactoringWizardOpenOperation(this.wizard);
			final int result= op.run(parent, this.wizard.getRefactoring().getName());
			this.status= op.getInitialConditionCheckingStatus();
			if (result == IDialogConstants.CANCEL_ID || result == RefactoringWizardOpenOperation.INITIAL_CONDITION_CHECKING_FAILED) {
				return false;
			}
			else {
				return true;
			}
		}
		catch (final InterruptedException e) {
			return false; // User action got cancelled
		}
	}
	
	public RefactoringStatus getInitialConditionCheckingStatus() {
		return this.status;
	}
	
}
