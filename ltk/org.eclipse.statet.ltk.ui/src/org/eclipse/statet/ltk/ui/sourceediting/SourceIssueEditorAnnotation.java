/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.jface.text.quickassist.IQuickFixableAnnotation;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationPresentation;
import org.eclipse.jface.text.source.ImageUtilities;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ltk.ui.sourceediting.AnnotationPresentationConfig;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet.IssueCategory;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet.ProblemTypes;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.ui.LtkUIResources;


/**
 * Annotation representing an {@link Problem}.
 */
@NonNullByDefault
public class SourceIssueEditorAnnotation extends Annotation
		implements SourceIssueAnnotation<Problem>, IAnnotationPresentation, IQuickFixableAnnotation {
	
	
	static final AnnotationPresentationConfig ERROR_CONFIG= new AnnotationPresentationConfig(
			"org.eclipse.ui.workbench.texteditor.error", +1, //$NON-NLS-1$
			LtkUIResources.OBJ_ERROR_IMAGE_ID, LtkUIResources.OBJ_ERROR_AWAY_IMAGE_ID );
	static final AnnotationPresentationConfig WARNING_CONFIG= new AnnotationPresentationConfig(
			"org.eclipse.ui.workbench.texteditor.warning", +1, //$NON-NLS-1$
			LtkUIResources.OBJ_WARNING_IMAGE_ID, LtkUIResources.OBJ_WARNING_AWAY_IMAGE_ID );
	static final AnnotationPresentationConfig INFO_CONFIG= new AnnotationPresentationConfig(
			"org.eclipse.ui.workbench.texteditor.info", +1, //$NON-NLS-1$
			LtkUIResources.OBJ_INFO_IMAGE_ID, LtkUIResources.OBJ_INFO_AWAY_IMAGE_ID );
	static final AnnotationPresentationConfig FALLBACK_CONFIG = new AnnotationPresentationConfig(
			null, Integer.MIN_VALUE,
			null, null );
	
	static AnnotationPresentationConfig getProblemPresentationConfig(final int severity) {
		switch (severity) {
		case Problem.SEVERITY_ERROR:
			return ERROR_CONFIG;
		case Problem.SEVERITY_WARNING:
			return WARNING_CONFIG;
		case Problem.SEVERITY_INFO:
			return INFO_CONFIG;
		default:
			return FALLBACK_CONFIG;
		}
	}
	
	static AnnotationPresentationConfig getIssuePresentationConfig(
			final IssueCategory<?> issueCategory, final String type) {
		if (issueCategory instanceof IssueTypeSet.ProblemCategory) {
			final ProblemTypes problemTypes= ((IssueTypeSet.ProblemCategory)issueCategory).getTypes(Ltk.EDITOR_CONTEXT);
			if (problemTypes != null) {
				return getProblemPresentationConfig(problemTypes.getSeverity(type));
			}
		}
		return FALLBACK_CONFIG;
	}
	
	
	private final IssueTypeSet.IssueCategory<Problem> issueCategory;
	private final Problem issue;
	
	private boolean isQuickFixable= false;
	private boolean isQuickFixableStateSet= false;
	
	private final AnnotationPresentationConfig presentationConfig;
	
	
	public SourceIssueEditorAnnotation(final IssueTypeSet.IssueCategory<Problem> issueCategory,
			final String type, final Problem issue) {
		super(type, false, null);
		this.issueCategory= issueCategory;
		this.issue= issue;
		
		this.presentationConfig= getProblemPresentationConfig(issue.getSeverity());
	}
	
	
	@Override
	public IssueCategory<Problem> getIssueCategory() {
		return this.issueCategory;
	}
	
	@Override
	public String getText() {
		return this.issue.getMessage();
	}
	
	public Problem getIssue() {
		return this.issue;
	}
	
	
	@Override
	public int getLayer() {
		return this.presentationConfig.getLevel();
	}
	
	@Override
	public void paint(final GC gc, final Canvas canvas, final Rectangle bounds) {
		final Image image= this.presentationConfig.getImage();
		if (image != null) {
			ImageUtilities.drawImage(image, gc, canvas, bounds, SWT.CENTER, SWT.TOP);
		}
	}
	
	
	@Override
	public void setQuickFixable(final boolean state) {
		this.isQuickFixable= state;
		this.isQuickFixableStateSet= true;
	}
	
	@Override
	public boolean isQuickFixableStateSet() {
		return this.isQuickFixableStateSet;
	}
	
	@Override
	public boolean isQuickFixable() {
		return this.isQuickFixable;
	}
	
	
	private ImList<Annotation> overlaidAnnotations= ImCollections.emptyList();
	
	@Override
	public @Nullable SourceIssueAnnotation<Problem> getOverlay() {
		return null;
	}
	
	@Override
	public ImList<Annotation> getOverlaidAnnotations() {
		return this.overlaidAnnotations;
	}
	
	public void addOverlaidAnnotation(final Annotation annotation) {
		this.overlaidAnnotations= ImCollections.addElement(this.overlaidAnnotations, annotation);
	}
	
	public void removeOverlaidAnnotation(final Annotation annotation) {
		this.overlaidAnnotations= ImCollections.removeElement(this.overlaidAnnotations, annotation);
	}
	
}
