/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.folding;

import org.eclipse.jface.text.Position;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractFoldingPosition<E extends AbstractFoldingPosition<E>> extends Position {
	
	
	public AbstractFoldingPosition(final int offset, final int length) {
		super(offset, length);
	}
	
	
	protected abstract boolean update(E newPosition);
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof AbstractFoldingPosition<?>
				&& this.getClass() == obj.getClass() ) {
			final AbstractFoldingPosition<?> other= (AbstractFoldingPosition<?>)obj;
			return (this.offset == other.offset && this.length == other.length);
		}
		return false;
	}
	
}
