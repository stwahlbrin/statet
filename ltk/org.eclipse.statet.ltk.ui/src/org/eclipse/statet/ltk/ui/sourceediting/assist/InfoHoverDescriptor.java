/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ltk.ui.AdvancedExtensionsInternal;


/**
 * Describes an configured information hover type.
 */
@NonNullByDefault
public class InfoHoverDescriptor {
	
	
	private final String id;
	
	private final String name;
	
	int stateMask;
	
	boolean isEnabled;
	
	private final IConfigurationElement configurationElement;
	
	
	/**
	 * Creates a new editor text hover descriptor from the given configuration element.
	 * @param name 
	 *
	 * @param element the configuration element
	 */
	InfoHoverDescriptor(final String id, final String name,
			final IConfigurationElement configurationElement) {
		this.id= id;
		this.name= name;
		this.configurationElement= configurationElement;
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	/**
	 * Creates the editor text hover.
	 *
	 * @return the text hover
	 */
	public @Nullable InfoHover createHover() {
		try {
			return (InfoHover) this.configurationElement.createExecutableExtension(
					AdvancedExtensionsInternal.CONFIG_CLASS_ATTRIBUTE_NAME);
		}
		catch (final CoreException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, BUNDLE_ID, 0,
					"Could not create text hover '" + this.name + "'.", e ));
			return null;
		}
	}
	
	/**
	 * Returns the configured modifier getStateMask for this hover.
	 *
	 * @return the hover modifier stateMask or -1 if no hover is configured
	 */
	public int getStateMask() {
		return this.stateMask;
	}
	
	/**
	 * Returns whether this hover is enabled or not.
	 *
	 * @return <code>true</code> if enabled
	 */
	public boolean isEnabled() {
		return this.isEnabled;
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj != null && getClass().equals(obj.getClass())
				&& this.id.equals(((InfoHoverDescriptor) obj).getId()) );
	}
	
}
