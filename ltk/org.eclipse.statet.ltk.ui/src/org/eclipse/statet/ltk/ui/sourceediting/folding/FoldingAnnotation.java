/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.folding;

import org.eclipse.jface.text.source.projection.ProjectionAnnotation;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class FoldingAnnotation extends ProjectionAnnotation {
	
	
	static final int EXPANDED_STATE= 1;
	static final int COLLAPSED_STATE= 2;
	
	
	private final String type;
	
	private int initialState;
	
	private final AbstractFoldingPosition<?> position;
	
	
	public FoldingAnnotation(final String type, final boolean collapse,
			final AbstractFoldingPosition<?> position) {
		super(collapse);
		
		this.type= type;
		this.initialState= (collapse) ? COLLAPSED_STATE : EXPANDED_STATE;
		this.position= position;
	}
	
	
	int getInitialState() {
		return this.initialState;
	}
	
	int getState() {
		return (isCollapsed()) ? COLLAPSED_STATE : EXPANDED_STATE;
	}
	
	void applyState(final int state) {
		switch (state) {
		case EXPANDED_STATE:
			markExpanded();
			break;
		case COLLAPSED_STATE:
			markCollapsed();
			break;
		}
	}
	
	
	public AbstractFoldingPosition<?> getPosition() {
		return this.position;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected boolean update(final FoldingAnnotation newAnn) {
		if (this.type == newAnn.type && newAnn.getClass() == FoldingAnnotation.class
				&& this.position.getClass() == newAnn.position.getClass()
				&& ((AbstractFoldingPosition)this.position).update(newAnn.position) ) {
			this.initialState= newAnn.initialState;
			return true;
		}
		return false;
	}
	
}
