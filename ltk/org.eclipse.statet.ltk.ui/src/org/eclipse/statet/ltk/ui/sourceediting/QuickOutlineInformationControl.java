/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.ecommons.ui.dialogs.QuickTreeInformationControl;

import org.eclipse.statet.internal.ltk.ui.LtkUIPlugin;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.ui.LtkActions;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;
import org.eclipse.statet.ltk.ui.util.ExtModelContentProvider;
import org.eclipse.statet.ltk.ui.util.ExtModelLabelProvider;


/**
 * Show outline in light-weight control.
 */
@NonNullByDefault
public abstract class QuickOutlineInformationControl extends QuickTreeInformationControl {
	
	
	protected static final String INHERITED_COLOR_NAME= "org.eclipse.jdt.ui.ColoredLabels.inherited"; //$NON-NLS-1$
	
	
	protected class QuickOutlineContent implements ExtModelContentProvider.ModelContent {
		
		
		public QuickOutlineContent() {
		}
		
		
		@Override
		public @Nullable SourceUnitModelInfo getModelInfo(final Object input) {
			return QuickOutlineInformationControl.this.getModelInfo(input);
		}
		@Override
		public @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> getContentFilter() {
			return QuickOutlineInformationControl.this.getContentFilter();
		}
		
	}
	
	
	private final OpenDeclaration opener;
	
	private ITreeContentProvider contentProvider= nonNullLateInit();
	
	private boolean requireFullName;
	
	
	/**
	 * Creates a new outline information control.
	 *
	 * @param parent the parent shell
	 * @param shellStyle the additional styles for the shell
	 * @param commandId the id of the command that invoked this control or <code>null</code>
	 */
	public QuickOutlineInformationControl(final Shell parent,
			final String commandId, final int pageCount,
			final OpenDeclaration opener) {
		super(parent, SWT.RESIZE, true, commandId, pageCount);
		
		this.opener= opener;
	}
	
	
	public abstract String getModelTypeId(int page);
	
	@Override
	protected IDialogSettings getDialogSettings() {
		return DialogUtils.getDialogSettings(LtkUIPlugin.getInstance(), "EditorStructurePopup"); //$NON-NLS-1$
	}
	
	@Override
	protected String getDescription(final int page) {
		if (getCommandId() == LtkActions.SHOW_QUICK_SOURCE_OUTLINE_COMMAND_ID) {
			return "Document Outline";
		}
		if (getCommandId() == LtkActions.SHOW_QUICK_ELEMENT_OUTLINE_COMMAND_ID) {
			return "Object Outline";
		}
		return ""; //$NON-NLS-1$
	}
	
	
	@Override
	protected void setMatcherString(final String pattern, final boolean update) {
		this.requireFullName= (pattern.indexOf('*') >= 0);
		super.setMatcherString(pattern, update);
	}
	
	@Override
	protected String getElementName(final IAdaptable element) {
		if (element instanceof LtkModelElement && !this.requireFullName) {
			return ((LtkModelElement<?>)element).getElementName().getSegmentName();
		}
		return super.getElementName(element);
	}
	
	
	@Override
	protected void configureViewer(final TreeViewer viewer) {
		this.contentProvider= createContentProvider();
		viewer.setContentProvider(this.contentProvider);
		viewer.setLabelProvider(createLabelProvider());
	}
	
	protected ITreeContentProvider createContentProvider() {
		return new ExtModelContentProvider(new QuickOutlineContent());
	}
	
	protected ILabelProvider createLabelProvider() {
		return new ExtModelLabelProvider(getModelTypeId(0));
	}
	
	protected @Nullable SourceUnitModelInfo getModelInfo(final Object input) {
		if (input instanceof SourceUnit) {
			final String modelTypeId= getModelTypeId(getIterationPosition());
			return ((SourceUnit)input).getModelInfo(modelTypeId, 0, null);
		}
		return null;
	}
	
	protected @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> getContentFilter() {
		return null;
	}
	
	
	protected int getInitialIterationPage(final SourceElement<?> element) {
		return 0;
	}
	
	@Override
	public void setInput(final Object information) {
		if (information instanceof SourceElement) {
			final SourceElement<?> element= (SourceElement<?>)information;
			final SourceUnit su= element.getSourceUnit();
			if (su != null) {
				inputChanged(getInitialIterationPage(element), su, element);
				return;
			}
		}
		
		inputChanged(0, null, null);
	}
	
	@Override
	protected void openElement(final Object element) throws CoreException {
		if (element instanceof SourceElement) {
			this.opener.open((SourceElement<?>)element, true);
		}
	}
	
}
