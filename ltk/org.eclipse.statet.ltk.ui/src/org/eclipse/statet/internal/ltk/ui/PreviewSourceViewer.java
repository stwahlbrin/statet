/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.swt.EnhStyledText;
import org.eclipse.statet.ltk.ui.sourceediting.util.SourceViewerEditorPreferenceUpdater;


@NonNullByDefault
public class PreviewSourceViewer extends SourceViewer {
	
	
	public PreviewSourceViewer(final Composite parent,
			final boolean manageFont,
			final IPreferenceStore preferenceStore) {
		super(parent, null, null, false, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		
		setEditable(false);
		
		final var textWidget= nonNullAssert(getTextWidget());
		new SourceViewerEditorPreferenceUpdater(this, true, manageFont, preferenceStore);
		if (!manageFont) {
			textWidget.setFont(JFaceResources.getFont(JFaceResources.TEXT_FONT));
		}
	}
	
	public PreviewSourceViewer(final Composite parent,
			final IPreferenceStore preferenceStore) {
		this(parent, true, preferenceStore);
	}
	
	public PreviewSourceViewer(final Composite parent) {
		this(parent, true, EditorsUI.getPreferenceStore());
	}
	
	
	@Override
	protected StyledText createTextWidget(final Composite parent, final int styles) {
		return EnhStyledText.forSourceEditor(parent, styles);
	}
	
	
}
