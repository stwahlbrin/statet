/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.internal.ltk.buildpath.ui.BuildpathsUIPlugin;
import org.eclipse.statet.ltk.buildpath.ui.BuildpathListElement;
import org.eclipse.statet.ltk.buildpath.ui.BuildpathsUIDescription;


public abstract class BuildPathWizard extends Wizard {
	
	
	private final ImList<BuildpathListElement> existingEntries;
	
	private final BuildpathListElement entryToEdit;
	
	private final BuildpathsUIDescription uiDescription;
	
	
	public BuildPathWizard(final ImList<BuildpathListElement> existingEntries, final BuildpathListElement newEntry,
			final String titel, final ImageDescriptor image, final BuildpathsUIDescription uiDescription) {
		this.existingEntries= existingEntries;
		this.entryToEdit= newEntry;
		
		this.uiDescription= uiDescription;
		
		if (image != null) {
			setDefaultPageImageDescriptor(image);
		}
		setDialogSettings(BuildpathsUIPlugin.getInstance().getDialogSettings());
		setWindowTitle(titel);
	}
	
	
	public ImList<BuildpathListElement> getExistingEntries() {
		return this.existingEntries;
	}
	
	protected BuildpathListElement getEntryToEdit() {
		return this.entryToEdit;
	}
	
	
	protected BuildpathsUIDescription getUIDescription() {
		return this.uiDescription;
	}
	
	
	public List<BuildpathListElement> getInsertedElements() {
		return new ArrayList<>();
	}
	
	public List<BuildpathListElement> getRemovedElements() {
		return new ArrayList<>();
	}
	
	public List<BuildpathListElement> getModifiedElements() {
		final ArrayList<BuildpathListElement> result= new ArrayList<>(1);
		result.add(this.entryToEdit);
		return result;
	}
	
}
