/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

/**
 * Represents a context when working with LTK sources and models.
 * <p>
 * The objects can be used to identify the context (by identity). It has only the
 * key attribute and doesn't provides further functions.</p>
 * 
 * @see Ltk
 * @noextend
 */
@NonNullByDefault
public final class WorkingContext {
	
	
	private final String key;
	
	
	public WorkingContext(final String key) {
		this.key= key;
	}
	
	
	public String getKey() {
		return this.key;
	}
	
	
	@Override
	public int hashCode() {
		return this.key.hashCode();
	}
	
	@Override
	public String toString() {
		return this.key;
	}
	
}
