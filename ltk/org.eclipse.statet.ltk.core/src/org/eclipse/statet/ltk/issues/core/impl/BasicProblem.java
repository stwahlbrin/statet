/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.issues.core.Problem;


/**
 * Default implementation of {@link Problem}.
 */
@NonNullByDefault
public class BasicProblem extends BasicSourceIssue implements Problem {
	
	
	private final String categoryId;
	
	private final int severity;
	private final int code;
	private final String message;
	
	
	public BasicProblem(final String categoryId,
			final int severity, final int code, final String message,
			final int line, final int startOffset, final int endOffset) {
		super(line, startOffset, endOffset);
		this.categoryId= categoryId;
		this.severity= severity;
		this.code= code;
		this.message= message;
	}
	
	public BasicProblem(final String categoryId,
			final int severity, final int code, final String message,
			final int startOffset, final int endOffset) {
		super(-1, startOffset, endOffset);
		this.categoryId= categoryId;
		this.severity= severity;
		this.code= code;
		this.message= message;
	}
	
	
	@Override
	public String getCategoryId() {
		return this.categoryId;
	}
	
	@Override
	public int getSeverity() {
		return this.severity;
	}
	
	@Override
	public int getCode() {
		return this.code;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
}
