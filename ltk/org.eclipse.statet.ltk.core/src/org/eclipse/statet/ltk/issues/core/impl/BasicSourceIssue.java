/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.issues.core.Issue;


@NonNullByDefault
public abstract class BasicSourceIssue implements Issue {
	
	
	private final int line;
	private final int startOffset;
	private final int endOffset;
	
	
	public BasicSourceIssue(final int line, final int startOffset, final int endOffset) {
		this.line= line;
		this.startOffset= startOffset;
		this.endOffset= (endOffset - startOffset > 0) ? endOffset : startOffset + 1;
	}
	
	
	@Override
	public int getSourceLine() {
		return this.line;
	}
	
	@Override
	public int getSourceStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public int getSourceEndOffset() {
		return this.endOffset;
	}
	
	
}
