/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ast.core;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Visitor for AST of {@link AstNode}s.
 * <p>
 * A visitor can visit an AST (or a subtree of an AST) by calling
 * {@link AstNode#accept(AstVisitor)} or {@link AstNode#acceptInChildren(AstVisitor)}.
 * It can be used independent of the language (often a language specific visitor pattern is offered
 * too).</p>
 */
@NonNullByDefault
public interface AstVisitor {
	
	
	void visit(final AstNode node) throws InvocationTargetException;
	
}
