/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ast.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public class Asts {
	
	
	public static final AstNode getRoot(AstNode node) {
		AstNode parent;
		while ((parent= node.getParent()) != null) {
			node= parent;
		}
		return node;
	}
	
	
	public static boolean hasErrors(final AstNode node) {
		return ((node.getStatusCode() &
						(StatusCodes.ERROR | StatusCodes.ERROR_IN_CHILD)
				) != 0 );
	}
	
	
}
