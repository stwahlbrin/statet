/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.build;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.source.SourceConfig;


@NonNullByDefault
public class ReconcileConfig<@NonNull TSourceConfig extends SourceConfig> {
	
	
	private final TSourceConfig sourceConfig;
	
	
	public ReconcileConfig(final TSourceConfig sourceConfig) {
		this.sourceConfig= sourceConfig;
	}
	
	
	public TSourceConfig getSourceConfig() {
		return this.sourceConfig;
	}
	
}
