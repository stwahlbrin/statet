/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core;

import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementDelta;


/**
 * An element changed event describes a change to the structure or contents
 * of a tree of Java elements. The changes to the elements are described by
 * the associated delta object carried by this event.
 * <p>
 * This class is not intended to be instantiated or subclassed by clients.
 * Instances of this class are automatically created by the Java model.
 * </p>
 * 
 * @see ElementChangedListener
 * @see IJavaElementDelta
 */
public class ElementChangedEvent {
	
	
	public final WorkingContext context;
	public final LtkModelElementDelta delta;
	
	
	/**
	 * Creates an new element changed event (based on a <code>IJavaElementDelta</code>).
	 * 
	 * @param delta the Java element delta.
	 * @param type the type of delta (ADDED, REMOVED, CHANGED) this event contains
	 */
	public ElementChangedEvent(final LtkModelElementDelta delta, final WorkingContext context) {
		this.context= context;
		this.delta= delta;
	}
	
}
