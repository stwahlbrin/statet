/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


@NonNullByDefault
public abstract class BasicSourceUnitModelInfo implements SourceUnitModelInfo {
	
	
	private static final ImList<Object> NO_ATTACHMENTS= ImCollections.emptyList();
	
	
	private final AstInfo ast;
	
	private volatile ImList<Object> attachments= NO_ATTACHMENTS;
	
	
	public BasicSourceUnitModelInfo(final AstInfo ast) {
		this.ast= ast;
	}
	
	
	@Override
	public SourceModelStamp getStamp() {
		return this.ast.getStamp();
	}
	
	@Override
	public AstInfo getAst() {
		return this.ast;
	}
	
	
	@Override
	public synchronized void addAttachment(final Object data) {
		this.attachments= ImCollections.addElement(this.attachments, data);
	}
	
	@Override
	public synchronized void removeAttachment(final Object data) {
		this.attachments= ImCollections.removeElement(this.attachments, data);
	}
	
	@Override
	public ImList<Object> getAttachments() {
		return this.attachments;
	}
	
}
