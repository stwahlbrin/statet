/*=============================================================================#
 # Copyright (c) 2006, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.ChangeDescriptor;
import org.eclipse.ltk.core.refactoring.RefactoringChangeDescriptor;
import org.eclipse.ltk.core.refactoring.RefactoringDescriptor;
import org.eclipse.ltk.core.refactoring.participants.ReorgExecutionLog;


/**
 * Dynamic validation change with support for refactoring descriptors.
 */
public final class DynamicValidationRefactoringChange extends DynamicValidationChange {
	
	
	/** The refactoring descriptor */
	private final RefactoringDescriptor descriptor;
	
	private ReorgExecutionLog executionLog;
	
	
	/**
	 * Creates a new dynamic validation refactoring change.
	 * 
	 * @param descriptor
	 *     the refactoring descriptor
	 * @param name
	 *     the name of the change
	 */
	public DynamicValidationRefactoringChange(final RefactoringDescriptor descriptor, final String name) {
		super(name);
		assert (descriptor != null);
		this.descriptor= descriptor;
	}
	
	/**
	 * Creates a new dynamic validation refactoring change.
	 * 
	 * @param descriptor the refactoring descriptor
	 * @param name the name of the change
	 * @param changes the changes
	 * @param executionLog optional reorg execution log
	 */
	public DynamicValidationRefactoringChange(final RefactoringDescriptor descriptor, final String name,
			final Change[] changes, final ReorgExecutionLog executionLog) {
		super(name, changes);
		assert (descriptor != null);
		this.descriptor= descriptor;
		this.executionLog= executionLog;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChangeDescriptor getDescriptor() {
		return new RefactoringChangeDescriptor(this.descriptor);
	}
	
	
	@Override
	public Change perform(final IProgressMonitor progress) throws CoreException {
		try {
			return super.perform(progress);
		}
		catch (final OperationCanceledException e) {
			if (this.executionLog != null) {
				this.executionLog.markAsCanceled();
			}
			throw e;
		}
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Class<T> adapterType) {
		if (adapterType == ReorgExecutionLog.class) {
			return (T) this.executionLog;
		}
		return super.getAdapter(adapterType);
	}
	
}
