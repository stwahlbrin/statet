/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.CopyProcessor;
import org.eclipse.ltk.core.refactoring.participants.DeleteProcessor;
import org.eclipse.ltk.core.refactoring.participants.MoveProcessor;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

import org.eclipse.statet.ltk.model.core.ElementSet;


/**
 * Must be implemented for each language.
 */
public class CommonRefactoringFactory {
	
	
	public RefactoringAdapter createAdapter(final Object elements) {
		return null;
	}
	
	public DeleteProcessor createDeleteProcessor(final Object elementsToDelete,
			final RefactoringAdapter adapter) {
		return null;
	}
	
	public MoveProcessor createMoveProcessor(final Object elementsToMove, final RefactoringDestination destination,
			final RefactoringAdapter adapter) {
		return null;
	}
	
	public CopyProcessor createCopyProcessor(final Object elementsToCopy, final RefactoringDestination destination,
			final RefactoringAdapter adapter) {
		return null;
	}
	
	public RefactoringProcessor createPasteProcessor(final Object elementsToPaste, final RefactoringDestination destination,
			final RefactoringAdapter adapter) {
		return null;
	}
	
	
	protected ElementSet createElementSet(final Object elements) {
		if (elements instanceof ElementSet) {
			return (ElementSet)elements;
		}
		if (elements instanceof Object[]) {
			return new ElementSet((Object[]) elements);
		}
		return new ElementSet(elements);
	}
	
}
