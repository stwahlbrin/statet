/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text;



public interface IIndentSettings {
	
	
	enum IndentationType {
		TAB, SPACES,
	}
	
	
	String TAB_SIZE_PROP= "tabSize"; //$NON-NLS-1$
	
	int getTabSize();
	
	
	String INDENT_DEFAULT_TYPE_PROP= "indentDefaultType"; //$NON-NLS-1$
	
	IndentationType getIndentDefaultType();
	
	
	String INDENT_SPACES_COUNT_PROP= "indentSpacesCount"; //$NON-NLS-1$
	
	int getIndentSpacesCount();
	
	
	String REPLACE_TABS_WITH_SPACES_PROP= "replaceOtherTabsWithSpaces"; //$NON-NLS-1$
	
	boolean getReplaceOtherTabsWithSpaces();
	
	
	String REPLACE_CONSERVATIVE_PROP= "replaceConservative"; //$NON-NLS-1$
	
	boolean getReplaceConservative();
	
	
	String WRAP_LINE_WIDTH_PROP= "lineWidth"; //$NON-NLS-1$
	
	int getLineWidth();
	
	
}
