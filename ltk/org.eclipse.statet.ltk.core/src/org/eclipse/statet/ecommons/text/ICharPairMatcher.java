/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.source.ICharacterPairMatcher;


/**
 * Extended version of {@link ICharacterPairMatcher} providing
 * {@link #match(IDocument, int, boolean)}
 */
public interface ICharPairMatcher extends ICharacterPairMatcher {
	
	
	@Override
	IRegion match(IDocument document, int offset);
	
	/**
	 * Starting at the given offset, the matcher chooses a character close to this offset.
	 * The matcher then searches for the matching peer character of the chosen character
	 * and if it finds one, returns the minimal region of the document that contains both
	 * characters.
	 * 
	 * @param document the document
	 * @param offset the start offset
	 * @param auto if the character can be selected automatically
	 *     before or after the given offset
	 * @return the minimal region containing the peer characters, if a pair was found;
	 *     a region with length -1 if start character but not its peer was found;
	 *     otherwise <code>null</code>
	 */
	IRegion match(IDocument document, int offset, boolean auto);
	
	
}
