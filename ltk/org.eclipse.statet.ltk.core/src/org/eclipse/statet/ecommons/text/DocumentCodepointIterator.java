/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;


public class DocumentCodepointIterator extends CharCodepointIterator {
	
	
	private static int FRAGMENT_LENGTH= 2048;
	private static int FRAGMENT_ADDITION= 64;
	
	
	/**
	 * Creates a new iterator for a jface document.
	 * 
	 * @param document the document
	 * @param beginIndex the begin index of the iterator in the document
	 * @param endIndex the end index of the iterator in the document
	 * @throws BadLocationException if an index is not valid
	 */
	public static CharCodepointIterator create(final IDocument document,
			final int beginIndex, final int endIndex)
			throws BadLocationException {
		if (endIndex-beginIndex <= FRAGMENT_LENGTH + FRAGMENT_ADDITION) {
			return create(document.get(beginIndex, endIndex-beginIndex),
					beginIndex, beginIndex, endIndex );
		}
		else {
			return new DocumentCodepointIterator(document, beginIndex, endIndex);
		}
	}
	
	/**
	 * Creates a new iterator for a whole jface document.
	 * 
	 * @param document the document
	 * @throws BadLocationException if an index is not valid
	 */
	public static CharCodepointIterator create(final IDocument document)
			throws BadLocationException {
		return create(document, 0, document.getLength());
	}
	
	
	private final IDocument fDocument;
	
	private int fFragmentOffset;
	private String fFragment;
	
	
	protected DocumentCodepointIterator(final IDocument doc,
			final int beginIndex, final int endIndex) throws BadLocationException {
		super(beginIndex, endIndex);
		this.fDocument= doc;
		
		this.fFragmentOffset= Integer.MIN_VALUE;
	}
	
	
	@Override
	protected char getChar(final int offset, final byte prepare) {
		int offsetInFragment= offset - this.fFragmentOffset;
		if (offsetInFragment < 0 || offsetInFragment >= FRAGMENT_LENGTH) {
			if ((prepare & PREPARE_FORWARD) != 0 ||
					(prepare & PREPARE_FORWARD) == 0 && offset < this.fFragmentOffset) {
				this.fFragmentOffset= Math.max(offset - FRAGMENT_LENGTH + FRAGMENT_ADDITION,
						getBeginIndex() );
			}
			else {
				this.fFragmentOffset= Math.max(offset - FRAGMENT_ADDITION,
						getBeginIndex() );
			}
			try {
				this.fFragment= this.fDocument.get(this.fFragmentOffset, Math.min(FRAGMENT_LENGTH, 
						getEndIndex() - this.fFragmentOffset ));
				offsetInFragment= offset - this.fFragmentOffset;
			}
			catch (final BadLocationException e) {
				this.fFragmentOffset= Integer.MIN_VALUE;
				this.fFragment= null;
				return (char)EOF;
			}
		}
		return this.fFragment.charAt(offsetInFragment);
	}
	
	
	@Override
	public String toString() {
		try {
			return this.fDocument.get(getBeginIndex(), getEndIndex() - getBeginIndex());
		}
		catch (final BadLocationException e) {
			return e.getMessage();
		}
	}
	
}
