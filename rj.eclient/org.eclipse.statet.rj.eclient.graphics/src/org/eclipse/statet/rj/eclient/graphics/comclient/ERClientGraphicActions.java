/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics.comclient;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.SystemRunnable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.collections.IntArrayMap;

import org.eclipse.statet.rj.server.client.AbstractRJComClient;
import org.eclipse.statet.rj.server.client.AbstractRJComClientGraphicActions;
import org.eclipse.statet.rj.ts.core.AbstractRToolRunnable;
import org.eclipse.statet.rj.ts.core.RTool;
import org.eclipse.statet.rj.ts.core.RToolService;


public class ERClientGraphicActions extends AbstractRJComClientGraphicActions
		implements ToolRClientGraphicActions {
	
	
	private final RTool tool;
	
	private final IntArrayMap<Boolean> resizeTasks= new IntArrayMap<>();
	private final IntArrayMap<Boolean> closeTasks= new IntArrayMap<>();
	
	
	public ERClientGraphicActions(final AbstractRJComClient rjs, final RTool tool) {
		super(rjs);
		this.tool= tool;
	}
	
	
	private class ResizeRunnable extends AbstractRToolRunnable implements SystemRunnable {
		
		private final int devId;
		private final Runnable beforeResize;
		
		public ResizeRunnable(final int devId, final Runnable beforeResize) {
			super("r/rj/gd/resize", "Resize R Graphic"); //$NON-NLS-1$
			this.devId= devId;
			this.beforeResize= beforeResize;
		}
		
		@Override
		public boolean changed(final int event, final Tool process) {
			switch (event) {
			case MOVING_FROM:
			case REMOVING_FROM:
				return false;
			case BEING_ABANDONED:
				synchronized (ERClientGraphicActions.this.closeTasks) {
					ERClientGraphicActions.this.resizeTasks.put(this.devId, Boolean.FALSE);
				}
				return true;
			default:
				return true;
			}
		}
		
		@Override
		public void run(final RToolService r, final ProgressMonitor m) throws StatusException {
			synchronized (ERClientGraphicActions.this.closeTasks) {
				ERClientGraphicActions.this.resizeTasks.put(this.devId, Boolean.FALSE);
			}
			this.beforeResize.run();
			doResizeGraphic(this.devId, m);
		}
		
	}
	
	private class CloseRunnable extends AbstractRToolRunnable implements SystemRunnable {
		
		
		private final int devId;
		
		public CloseRunnable(final int devId) {
			super("r/rj/gd/close", "Close R Graphic ("+(devId+1)+")"); //$NON-NLS-1$
			this.devId= devId;
		}
		
		@Override
		public boolean changed(final int event, final Tool tool) {
			switch (event) {
			case MOVING_FROM:
				return false;
			case REMOVING_FROM:
			case BEING_ABANDONED:
			case FINISHING_OK:
			case FINISHING_ERROR:
			case FINISHING_CANCEL:
				synchronized (ERClientGraphicActions.this.closeTasks) {
					ERClientGraphicActions.this.closeTasks.put(this.devId, Boolean.FALSE);
				}
				return true;
			default:
				return true;
			}
		}
		
		@Override
		public void run(final RToolService r, final ProgressMonitor m) throws StatusException {
			doCloseGraphic(this.devId, m);
		}
		
	}
	
	
	@Override
	public RTool getRHandle() {
		return this.tool;
	}
	
	@Override
	public String getRLabel() {
		return this.tool.getLabel(RTool.DEFAULT_LABEL);
	}
	
	@Override
	public Status resizeGraphic(final int devId, final Runnable beforeResize) {
		synchronized (this.resizeTasks) {
			if (this.resizeTasks.put(devId, Boolean.TRUE) == Boolean.TRUE) {
				return Status.OK_STATUS;
			}
		}
		return this.tool.getQueue().add(new ResizeRunnable(devId, beforeResize));
	}
	
	@Override
	public Status closeGraphic(final int devId) {
		synchronized (this.closeTasks) {
			if (this.closeTasks.put(devId, Boolean.TRUE) == Boolean.TRUE) {
				return Status.OK_STATUS;
			}
		}
		return this.tool.getQueue().add(new CloseRunnable(devId));
	}
	
}
