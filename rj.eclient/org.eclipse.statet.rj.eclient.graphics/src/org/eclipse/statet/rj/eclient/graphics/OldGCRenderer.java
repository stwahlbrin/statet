/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.LineAttributes;
import org.eclipse.swt.graphics.Transform;

import org.eclipse.statet.internal.rj.eclient.graphics.CircleElement;
import org.eclipse.statet.internal.rj.eclient.graphics.ClipSetting;
import org.eclipse.statet.internal.rj.eclient.graphics.ColorSetting;
import org.eclipse.statet.internal.rj.eclient.graphics.FillSetting;
import org.eclipse.statet.internal.rj.eclient.graphics.FontSetting;
import org.eclipse.statet.internal.rj.eclient.graphics.GraphicInitialization;
import org.eclipse.statet.internal.rj.eclient.graphics.LineElement;
import org.eclipse.statet.internal.rj.eclient.graphics.LineSetting;
import org.eclipse.statet.internal.rj.eclient.graphics.PolygonElement;
import org.eclipse.statet.internal.rj.eclient.graphics.PolylineElement;
import org.eclipse.statet.internal.rj.eclient.graphics.RectElement;
import org.eclipse.statet.internal.rj.eclient.graphics.TextElement;
import org.eclipse.statet.rj.graphic.core.RGraphicInstruction;


public class OldGCRenderer {
	
	
	private float scale= 1.0f;
	
	private final LineAttributes lineAttributes= new LineAttributes(1.0f);
	private Color lineColor;
	private int lineAlpha;
	private Color fillColor;
	private int fillAlpha;
	private int xMax;
	private int yMax;
	
	
	public void clear(final float scale) {
		this.scale= scale;
		this.lineColor= null;
		this.lineAlpha= 0xff;
		this.fillColor= null;
		this.fillAlpha= 0xff;
		this.lineAttributes.style= SWT.LINE_SOLID;
		this.lineAttributes.width= scale;
		this.xMax= 0;
		this.yMax= 0;
	}
	
	public void paint(final GC gc, final List<? extends ERGraphicInstruction> instructions) {
		final Transform defaultTransform= null;
		Transform tempTransform= null;
		final float scale= this.scale;
		int currentAlpha= -1;
		int lineAlpha= this.lineAlpha;
		int fillAlpha= this.fillAlpha;
		try {
			gc.setAdvanced(true);
			gc.setAntialias(SWT.ON);
			gc.setTextAntialias(SWT.ON);
			gc.setLineAttributes(this.lineAttributes);
			gc.setTransform(defaultTransform);
			gc.setAlpha(currentAlpha);
			if (this.lineColor != null) {
				gc.setForeground(this.lineColor);
			}
			if (this.fillColor != null) {
				gc.setBackground(this.fillColor);
			}
			int ixmax= this.xMax;
			int iymax= this.yMax;
			
			for (final ERGraphicInstruction instr : instructions) {
				switch (instr.getInstructionType()) {
				case RGraphicInstruction.INIT:
					final GraphicInitialization init= (GraphicInitialization) instr;
					ixmax= (int) (((init.width) * scale) + 0.5);
					iymax= (int) (((init.height) * scale) + 0.5);
					gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
					gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_BLACK));
					gc.setAlpha(0xff);
					gc.fillRectangle(0, 0, ixmax, iymax);
					gc.setClipping(0, 0, ixmax, iymax);
					continue;
				case RGraphicInstruction.SET_CLIP: {
					final ClipSetting setting= (ClipSetting) instr;
					final int ix0= (int) ((setting.x0 * scale) + 0.5);
					final int iy0= (int) ((setting.y0 * scale) + 0.5);
					gc.setClipping(ix0, iy0,
							(int) Math.min(((setting.x1 * scale) + 0.5), ixmax) - ix0,
							(int) Math.min(((setting.y1 * scale) + 0.5), iymax) - iy0 );
					continue; }
				case RGraphicInstruction.SET_COLOR: {
					final ColorSetting setting= (ColorSetting) instr;
					lineAlpha= setting.getAlpha();
					gc.setForeground(setting.swtColor);
					continue; }
				case RGraphicInstruction.SET_FILL: {
					final FillSetting setting= (FillSetting) instr;
					fillAlpha= setting.getAlpha();
					gc.setBackground(setting.swtColor);
					continue; }
				case RGraphicInstruction.SET_LINE: {
					final LineSetting setting= (LineSetting) instr;
					switch (setting.type) {
					case 0:
						this.lineAttributes.style= SWT.LINE_SOLID;
						this.lineAttributes.width= setting.width * scale;
						gc.setLineAttributes(this.lineAttributes);
						continue;
					case -1:
						this.lineAttributes.style= SWT.LINE_SOLID;
						this.lineAttributes.width= 0.0f;
						gc.setLineAttributes(this.lineAttributes);
						continue;
	//				case 0x44:
	//					this.tempLineAttributes.style= SWT.LINE_DASH;
	//					this.tempLineAttributes.width= (float) (setting.width * scale);
	//					gc.setLineAttributes(this.tempLineAttributes);
	//					continue;
	//				case 0x13:
	//					this.tempLineAttributes.style= SWT.LINE_DOT;
	//					this.tempLineAttributes.width= (float) (setting.width * scale);
	//					gc.setLineAttributes(this.tempLineAttributes);
	//					continue;
	//				case 0x1343:
	//					this.tempLineAttributes.style= SWT.LINE_DASHDOT;
	//					this.tempLineAttributes.width= (float) (setting.width * scale);
	//					gc.setLineAttributes(this.tempLineAttributes);
	//					continue;
					}
					
					int rPattern= setting.type;
					int length= 0;
					while (rPattern != 0) {
						length++;
						rPattern >>>= 4;
					}
					final int[] dashes= new int[length];
					rPattern= setting.type;
					for (int i= 0; i < length; i++) {
						dashes[i]= (rPattern & 0xf);
						rPattern >>>= 4;
					}
					gc.setLineDash(dashes);
					gc.setLineWidth((int) (setting.width * scale + 0.5));
					continue; }
				case RGraphicInstruction.SET_FONT: {
					final FontSetting setting= (FontSetting) instr;
					gc.setFont(setting.swtFont);
					continue; }
				case RGraphicInstruction.DRAW_LINE: {
					final LineElement element= (LineElement) instr;
					if (lineAlpha != currentAlpha) {
						gc.setAlpha(currentAlpha= lineAlpha);
					}
					gc.drawLine(
							(int) (element.x0 * scale + 0.5),
							(int) (element.y0 * scale + 0.5),
							(int) (element.x1 * scale + 0.5),
							(int) (element.y1 * scale + 0.5) );
					continue; }
				case RGraphicInstruction.DRAW_RECTANGLE: {
					final RectElement element= (RectElement) instr;
					final int ix0= (int) (element.x0 * scale + 0.5);
					final int iy0= (int) (element.y0 * scale + 0.5);
					final int iw= (int) (element.x1 * scale + 0.5) - ix0;
					final int ih= (int) (element.y1 * scale + 0.5) - iy0;
					if (fillAlpha != 0) {
						if (fillAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= fillAlpha);
						}
						gc.fillRectangle(ix0, iy0, iw, ih);
					}
					if (lineAlpha != 0) {
						if (lineAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= lineAlpha);
						}
						gc.drawRectangle(ix0, iy0, iw, ih);
					}
					continue; }
				case RGraphicInstruction.DRAW_POLYLINE: {
					final PolylineElement element= (PolylineElement) instr;
					final int n= element.x.length;
					final int[] icoord= new int[n * 2];
					for (int i= 0, j= 0; j < n; j++) {
						icoord[i++]= (int) (element.x[j] * scale + 0.5);
						icoord[i++]= (int) (element.y[j] * scale + 0.5);
					}
					gc.drawPolyline(icoord);
					continue; }
				case RGraphicInstruction.DRAW_POLYGON: {
					final PolygonElement element= (PolygonElement) instr;
					final int n= element.x.length;
					final int[] icoord= new int[n * 2];
					for (int i= 0, j= 0; j < n; j++) {
						icoord[i++]= (int) (element.x[j] * scale + 0.5);
						icoord[i++]= (int) (element.y[j] * scale + 0.5);
					}
					if (fillAlpha != 0) {
						if (fillAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= fillAlpha);
						}
						gc.fillPolygon(icoord);
					}
					if (lineAlpha != 0) {
						if (lineAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= lineAlpha);
						}
						gc.drawPolygon(icoord);
					}
					continue; }
				case RGraphicInstruction.DRAW_CIRCLE: {
					final CircleElement element= (CircleElement) instr;
					final int id= (int) (element.r * scale * 2.0 + 0.5);
					final int ix0= (int) ((element.x - element.r) * scale + 0.5);
					final int iy0= (int) ((element.y - element.r) * scale + 0.5);
					if (fillAlpha != 0) {
						if (fillAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= fillAlpha);
						}
						gc.fillOval(ix0, iy0, id, id);
					}
					if (lineAlpha != 0) {
						if (lineAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= lineAlpha);
						}
						gc.drawOval(ix0, iy0, id, id);
					}
					continue; }
				case RGraphicInstruction.DRAW_TEXT: {
					final TextElement element= (TextElement) instr;
					final double hShift;
					if (element.horizontalAdjust != 0.0) {
						hShift= element.horizontalAdjust * gc.textExtent(element.text, (SWT.DRAW_DELIMITER | SWT.DRAW_TAB | SWT.DRAW_TRANSPARENT)).x;
					}
					else {
						hShift= 0.0;
					}
					if (element.rotateDegree != 0.0) {
						if (tempTransform == null) {
							tempTransform= new Transform(gc.getDevice());
						}
						tempTransform.identity();
						tempTransform.translate((float) (element.x * scale), (float) (element.y * scale));
						tempTransform.rotate((float) -element.rotateDegree);
						tempTransform.translate((float) - hShift, - gc.getFontMetrics().getAscent());
						gc.setTransform(tempTransform);
						if (lineAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= lineAlpha);
						}
						gc.drawText(element.text, 0, 0,
								(SWT.DRAW_DELIMITER | SWT.DRAW_TAB | SWT.DRAW_TRANSPARENT) );
						gc.setTransform(defaultTransform);
					}
					else {
						if (lineAlpha != currentAlpha) {
							gc.setAlpha(currentAlpha= lineAlpha);
						}
						gc.drawText(element.text,
								(int) (((element.x - hShift) * scale) + 0.5),
								(int) ((element.y * scale) + 0.5) - gc.getFontMetrics().getAscent(),
								(SWT.DRAW_DELIMITER | SWT.DRAW_TAB | SWT.DRAW_TRANSPARENT) );
					}
					continue; }
				}
			}
			
			this.lineColor= gc.getForeground();
			this.lineAlpha= lineAlpha;
			this.fillColor= gc.getBackground();
			this.fillAlpha= fillAlpha;
			this.xMax= ixmax;
			this.yMax= iymax;
		}
		finally {
			if (tempTransform != null) {
				tempTransform.dispose();
			}
		}
	}
	
}
