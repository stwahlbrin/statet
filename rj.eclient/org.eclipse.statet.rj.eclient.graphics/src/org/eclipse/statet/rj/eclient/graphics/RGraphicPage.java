/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics;

import static org.eclipse.statet.ecommons.ui.actions.UIActions.ADDITIONS_GROUP_ID;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.StatusLineContributionItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusChangeListener;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.rj.eclient.graphics.RGraphicCompositeActionSet.LocationListener;


/**
 * Single graphic page for {@link PageBookRGraphicView}.
 */
public class RGraphicPage extends Page implements StatusChangeListener {
	
	
	private final ERGraphic graphic;
	
	private RGraphicComposite control;
	
	private RGraphicCompositeActionSet actions;
	
	private StatusLineContributionItem locationStatusItem;
	private String locationStatusText;
	private LocationListener mouseLocationListener;
	
	
	public RGraphicPage(final ERGraphic graphic) {
		this.graphic= graphic;
	}
	
	
	protected ERGraphic getGraphic() {
		return this.graphic;
	}
	
	protected void init(final StatusLineContributionItem locationStatusItem) {
		this.locationStatusItem= locationStatusItem;
	}
	
	@Override
	public void createControl(final Composite parent) {
		this.control= new RGraphicComposite(parent, this.graphic);
		
		initActions(getSite(), getSite().getActionBars());
		
		this.graphic.addMessageListener(this);
		onStatusChanged(this.graphic.getMessage());
	}
	
	protected void initActions(final IServiceLocator serviceLocator, final IActionBars actionBars) {
		final IHandlerService handlerService= serviceLocator.getService(IHandlerService.class);
		
		final IHandler2 refreshHandler= new AbstractHandler() {
			@Override
			public Object execute(final ExecutionEvent event) throws ExecutionException {
				RGraphicPage.this.control.redrawGraphic();
				return null;
			}
		};
		handlerService.activateHandler(IWorkbenchCommandConstants.FILE_REFRESH, refreshHandler);
		
		final IToolBarManager toolBar= actionBars.getToolBarManager();
		toolBar.insertBefore(ADDITIONS_GROUP_ID, new Separator(RGraphicCompositeActionSet.CONTEXT_MENU_GROUP_ID));
		toolBar.insertBefore(ADDITIONS_GROUP_ID, new Separator(RGraphicCompositeActionSet.SIZE_MENU_GROUP_ID));
		
		this.actions= createActionSet();
		this.actions.setGraphic(this.graphic);
		this.actions.initActions(serviceLocator);
		this.actions.contributeToActionsBars(serviceLocator, actionBars);
		
		// Can find wrong item from other view
//		locationStatusItem= (StatusLineContributionItem) actionBars.getStatusLineManager().find(RGraphicCompositeActionSet.POSITION_STATUSLINE_ITEM_ID);
		if (this.locationStatusItem != null) {
			this.mouseLocationListener= new LocationListener() {
				
				final DecimalFormat format= new DecimalFormat("0.0####", new DecimalFormatSymbols(Locale.US)); //$NON-NLS-1$
				
				@Override
				public void loading() {
					if (RGraphicPage.this.locationStatusItem != null) {
						RGraphicPage.this.locationStatusItem.setText("..."); //$NON-NLS-1$
					}
				}
				
				@Override
				public void located(final double x, final double y) {
					if (RGraphicPage.this.locationStatusItem != null && UIAccess.isOkToUse(RGraphicPage.this.control)) {
						if (Double.isNaN(x) || Double.isInfinite(x)
								|| Double.isNaN(y) || Double.isInfinite(y) ) {
							RGraphicPage.this.locationStatusText= "NA"; //$NON-NLS-1$
						}
						else {
							final StringBuilder sb= new StringBuilder(32);
							sb.append('(');
							sb.append(this.format.format(x));
							sb.append(", "); //$NON-NLS-1$
							sb.append(this.format.format(y));
							sb.append(')');
							RGraphicPage.this.locationStatusText= sb.toString();
						}
						RGraphicPage.this.locationStatusItem.setText(RGraphicPage.this.locationStatusText);
					}
				}
			};
			this.actions.addMouseClickLocationListener(this.mouseLocationListener);
		}
	}
	
	protected RGraphicCompositeActionSet createActionSet() {
		return new RGraphicCompositeActionSet(this.control);
	}
	
	@Override
	public void onStatusChanged(final Status status) {
		final String message;
		if (status.getSeverity() > 0) {
			message= status.getMessage();
		}
		else {
			message= null;
		}
		getSite().getActionBars().getStatusLineManager().setMessage(message);
	}
	
	@Override
	public Control getControl() {
		return this.control;
	}
	
	protected RGraphicComposite getGraphicComposite() {
		return this.control;
	}
	
	@Override
	public void setFocus() {
		this.control.setFocus();
	}
	
	@Override
	public void dispose() {
		if (this.actions != null) {
			this.actions.dispose();
			this.actions= null;
		}
		if (this.graphic != null) {
			this.graphic.removeMessageListener(this);
		}
		super.dispose();
	}
	
}
