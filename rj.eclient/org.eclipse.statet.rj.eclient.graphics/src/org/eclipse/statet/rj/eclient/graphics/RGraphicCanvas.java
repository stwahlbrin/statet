/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics;

import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.internal.rj.eclient.graphics.GraphicInitialization;


/**
 * SWT control to paint an R graphic.
 * <p>
 * See {@link RGraphicComposite} for a higher API widget.</p>
 */
public class RGraphicCanvas extends Canvas implements PaintListener {
	
	
	public static final List<ERGraphicInstruction> NO_GRAPHIC= Collections.emptyList();
	
	
	private List<? extends ERGraphicInstruction> graphicInstructions;
	private final DefaultGCRenderer renderer;
	
	private GraphicInitialization init;
	
	
	public RGraphicCanvas(final Composite parent) {
		super(parent, checkStyle(0));
		
		this.graphicInstructions= Collections.emptyList();
		this.renderer= new DefaultGCRenderer();
		addPaintListener(this);
	}
	
	private static int checkStyle(int style) {
		style |= SWT.NO_REDRAW_RESIZE | SWT.NO_BACKGROUND;
		style |= SWT.DOUBLE_BUFFERED;
		return style;
	}
	
	public void setInstructions(final List<? extends ERGraphicInstruction> graphicData) {
		this.graphicInstructions= graphicData;
		if (!this.graphicInstructions.isEmpty()) {
			this.init= (GraphicInitialization) this.graphicInstructions.get(0);
		}
		else {
			this.init= null;
		}
	}
	
	@Override
	public Point computeSize(final int wHint, final int hHint, final boolean changed) {
		final GraphicInitialization init= this.init;
		if (init != null) {
			return new Point((int) (init.width + 0.5), (int) (init.height + 0.5));
		}
		else {
			return super.computeSize(wHint, hHint, changed);
		}
	}
	
	public double widget2graphicsX(final double x) {
		return x; // scale
	}
	
	public double widget2graphicY(final double y) {
		return y; // scale
	}
	
	@Override
	public void paintControl(final PaintEvent e) {
		final GC gc= e.gc;
		
		final Rectangle clientArea= getClientArea();
		gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		gc.fillRectangle(0, 0, clientArea.width, clientArea.height);
		if (this.graphicInstructions.isEmpty()) {
			return;
		}
		this.renderer.clear(1.0f); // scale
		this.renderer.paint(gc, this.graphicInstructions);
	}
	
}
