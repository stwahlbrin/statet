/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.rtm.base.ui.rexpr.RExprListProperty;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


class FacetGridDetail extends FacetDetail {
	
	
	private final RExprListProperty colVarsProperty;
	private final RExprListProperty rowVarsProperty;
	
	
	public FacetGridDetail(final DetailStack parent) {
		super(parent);
		
		final EClass eClass= Literals.GRID_FACET_LAYOUT;
		
		this.colVarsProperty= (RExprListProperty) GGPlotProperties.createProperty(
				eClass, Literals.GRID_FACET_LAYOUT__COL_VARS,
				"Column Variables:", "The variables for facets in horizontal direction");
		this.rowVarsProperty= (RExprListProperty) GGPlotProperties.createProperty(
				eClass, Literals.GRID_FACET_LAYOUT__ROW_VARS,
				"Row Variables:", "The variables for facets in vertical direction");
		
		createContent();
	}
	
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		LayoutUtils.addSmallFiller(composite, false);
		
		{	final Composite column= new Composite(composite, SWT.NONE);
			column.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			column.setLayout(LayoutUtils.newCompositeGrid(3));
			
			this.colVarsProperty.create(column, page);
		}
		
		{	final Composite column= new Composite(composite, SWT.NONE);
			column.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			column.setLayout(LayoutUtils.newCompositeGrid(3));
			
			this.rowVarsProperty.create(column, page);
		}
		LayoutUtils.addGDDummy(composite, true);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.colVarsProperty.bind(context);
		
		this.rowVarsProperty.bind(context);
	}
	
}
