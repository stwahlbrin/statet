/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.ecommons.emf.ui.forms.EFProperty;

import org.eclipse.statet.rtm.base.ui.rexpr.RExprListProperty;
import org.eclipse.statet.rtm.base.ui.rexpr.RExprValueProperty;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;


public class GGPlotProperties {
	
	
	private static GGPlotExprTypesUIProvider UI_ADAPTER_FACTORY= new GGPlotExprTypesUIProvider();
	
	
	public static EFProperty createProperty(
			final EClass eClass, final EStructuralFeature eFeature,
			final String label, final String tooltip) {
		if (!eClass.getEAllStructuralFeatures().contains(eFeature)) {
			return null;
		}
		if (eFeature == GGPlotPackage.Literals.PROP_STAT_PROVIDER__STAT) {
			return new StatProperty(label, tooltip, eClass, eFeature);
		}
		if (eFeature.getUpperBound() == 1) {
			return new RExprValueProperty(label, tooltip, eClass, eFeature,
					UI_ADAPTER_FACTORY );
		}
		return new RExprListProperty(label, tooltip, eClass, eFeature,
				UI_ADAPTER_FACTORY );
	}
	
}
