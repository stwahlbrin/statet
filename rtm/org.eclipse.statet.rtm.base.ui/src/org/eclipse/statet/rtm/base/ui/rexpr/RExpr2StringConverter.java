/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.rexpr;

import org.eclipse.core.databinding.conversion.IConverter;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rtm.rtdata.types.RExpr;


@NonNullByDefault
public class RExpr2StringConverter implements IConverter<@Nullable RExpr, String> {
	
	
	public RExpr2StringConverter() {
	}
	
	
	@Override
	public Object getFromType() {
		return RExpr.class;
	}
	
	@Override
	public Object getToType() {
		return String.class;
	}
	
	@Override
	public String convert(final @Nullable RExpr fromObject) {
		if (fromObject == null) {
			return ""; //$NON-NLS-1$
		}
		return fromObject.getExpr();
	}
	
}
