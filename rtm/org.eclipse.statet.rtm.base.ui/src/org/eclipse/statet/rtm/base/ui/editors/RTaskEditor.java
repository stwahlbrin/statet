/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.editors;

import static org.eclipse.statet.rtm.base.ui.RtModelUIPlugin.RUN_R_TASK_COMMAND_ID;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.emf.ui.forms.EFEditor;
import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;

import org.eclipse.statet.internal.rtm.base.ui.actions.RunRTaskHandler;
import org.eclipse.statet.internal.rtm.base.ui.editors.RCodeGenEditorInput;
import org.eclipse.statet.internal.rtm.base.ui.editors.RTaskGenManager;
import org.eclipse.statet.rtm.base.ui.IRtDescriptor;
import org.eclipse.statet.rtm.base.ui.RTaskSnippet;
import org.eclipse.statet.rtm.base.ui.RtModelUIPlugin;


public abstract class RTaskEditor extends EFEditor {
	
	
	public static final String MAIN_PAGE_ID= "Main"; //$NON-NLS-1$
	
	
	private final RTaskGenManager rCodeGenAdapter;
	
	
	/**
	 * This creates a model editor.
	 * 
	 * @param descriptor 
	 */
	public RTaskEditor(final IRtDescriptor descriptor) {
		super(descriptor);
		
		this.rCodeGenAdapter= new RTaskGenManager(this);
		
		getEditingDomain().getResourceSet().eAdapters().add(this.rCodeGenAdapter);
	}
	
	
	@Override
	public IRtDescriptor getModelDescriptor() {
		return (IRtDescriptor) super.getModelDescriptor();
	}
	
	@Override
	protected RtFormToolkit createToolkit(final Display display) {
		return new RtFormToolkit(RtModelUIPlugin.getInstance().getFormColors(display));
	}
	
	/**
	 * This is the method used by the framework to install your own controls.
	 * @generated
	 */
	@Override
	public void addPages() {
		// Only creates the other pages if there is something that can be edited
		//
		if (!getEditingDomain().getResourceSet().getResources().isEmpty()) {
			try {
				// Create a page for the selection tree view.
//				{	final ViewerPane viewerPane= new ViewerPane(getSite().getPage(), AbstractRTaskEditor.this) {
//						@Override
//						public Viewer createViewer(final Composite composite) {
//							final Tree tree= new Tree(composite, SWT.MULTI);
//							final TreeViewer newTreeViewer= new TreeViewer(tree);
//							return newTreeViewer;
//						}
//						@Override
//						public void requestActivation() {
//							super.requestActivation();
//							setCurrentViewerPane(this);
//						}
//					};
//					viewerPane.createControl(getContainer());
//					
//					this.selectionViewer= (TreeViewer)viewerPane.getViewer();
//					this.selectionViewer.setContentProvider(new AdapterFactoryContentProvider(this.adapterFactory));
//					
//					this.selectionViewer.setLabelProvider(new AdapterFactoryLabelProvider(this.adapterFactory));
//					this.selectionViewer.setInput(this.editingDomain.getResourceSet());
//					this.selectionViewer.setSelection(new StructuredSelection(this.editingDomain.getResourceSet().getResources().get(0)), true);
//					viewerPane.setTitle(this.editingDomain.getResourceSet());
//					
//					new AdapterFactoryTreeEditor(this.selectionViewer.getTree(), this.adapterFactory);
//					
//					createContextMenuFor(this.selectionViewer);
//					final int pageIndex= addPage(viewerPane.getControl());
//					setPageText(pageIndex, Messages.RTaskEditor_FirstPage_label);
//				}
				addFormPages();
				
				final RCodePage rCodePage= new RCodePage(this);
				
				addPage(rCodePage, new RCodeGenEditorInput(this.rCodeGenAdapter.getCodeFragment()));
			}
			catch (final PartInitException e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, RtModelUIPlugin.BUNDLE_ID, 0,
						"An error occured when creating the pages for the R task editor.", e ));
			}
			
			getSite().getShell().getDisplay().asyncExec(new Runnable() {
				@Override
				public void run() {
					if (getPageCount() > 0) {
						setActivePage(0);
					}
				}
			});
		}
	}
	
	protected abstract void addFormPages() throws PartInitException;
	
	/**
	 * This is used to track the active viewer.
	 * @generated
	 */
	@Override
	protected void pageChange(final int pageIndex) {
		super.pageChange(pageIndex);
		
//		if (this.contentOutlinePage != null) {
//			handleContentOutlineSelection(this.contentOutlinePage.getSelection());
//		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Class<T> adapterType) {
		if (adapterType == RTaskSnippet.class) {
			return (T) this.rCodeGenAdapter.getRTaskSnippet();
		}
		return super.getAdapter(adapterType);
	}
	
//	@Override
//	public IContentOutlinePage createContentOutlinePage() {
//		final EFRawOutlinePage page= new EFRawOutlinePage(this);
		
		// Listen to selection so that we can handle it is a special way
//		page.addSelectionChangedListener(new ISelectionChangedListener() {
//			// This ensures that we handle selections correctly
//			@Override
//			public void selectionChanged(final SelectionChangedEvent event) {
//				handleContentOutlineSelection(event.getSelection());
//			}
//		});
//		
//		return page;
//	}
	
	/**
	 * This deals with how we want selection in the outliner to affect the other views.
	 * @generated
	 */
//	public void handleContentOutlineSelection(final ISelection selection) {
//		if (this.currentViewerPane != null && !selection.isEmpty() && selection instanceof IStructuredSelection) {
//			final Iterator<?> selectedElements= ((IStructuredSelection)selection).iterator();
//			if (selectedElements.hasNext()) {
//				// Get the first selected element
//				final Object selectedElement= selectedElements.next();
//				
//				// If it's the selection viewer, then we want it to select the same selection as this selection
//				if (this.currentViewerPane.getViewer() == this.selectionViewer) {
//					final ArrayList<Object> selectionList= new ArrayList<Object>();
//					selectionList.add(selectedElement);
//					while (selectedElements.hasNext()) {
//						selectionList.add(selectedElements.next());
//					}
//					
//					// Set the selection to the widget
//					this.selectionViewer.setSelection(new StructuredSelection(selectionList));
//				}
//				else {
//					// Set the input to the widget
//					if (this.currentViewerPane.getViewer().getInput() != selectedElement) {
//						this.currentViewerPane.getViewer().setInput(selectedElement);
//						this.currentViewerPane.setTitle(selectedElement);
//					}
//				}
//			}
//		}
//	}
	
	@Override
	protected void operationFailed(final String operation, final Exception e) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, RtModelUIPlugin.BUNDLE_ID, 0,
				NLS.bind("An error occurred when ''{0}'' in R task editor.", operation),
				e ));
	}
	
	@Override
	protected void createActions(final IServiceLocator locator, final HandlerCollection handlers) {
		super.createActions(locator, handlers);
		
		final IContextService contextService= locator.getService(IContextService.class);
		contextService.activateContext(RtModelUIPlugin.R_TASK_EDITOR_CONTEXT_ID);
		
		final IHandlerService handlerService= locator.getService(IHandlerService.class);
		{	final IHandler2 handler= new RunRTaskHandler(this);
			handlers.add(RUN_R_TASK_COMMAND_ID, handler);
			handlerService.activateHandler(RUN_R_TASK_COMMAND_ID, handler);
		}
	}
	
	@Override
	protected void contributeToPages(final IToolBarManager manager) {
		super.contributeToPages(manager);
		
		{	final IHandler2 handler= this.handlers.get(RUN_R_TASK_COMMAND_ID);
			manager.add(new HandlerContributionItem(new CommandContributionItemParameter(getSite(),
					null, RUN_R_TASK_COMMAND_ID, HandlerContributionItem.STYLE_PUSH ),
					handler ));
		}
	}
	
}
