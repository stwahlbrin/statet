/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import org.eclipse.statet.ecommons.databinding.core.DataBindingSubContext;
import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;


/**
 * An abstract implementation of a section in a tab in the tabbed property sheet
 * page for EF.
 */
public abstract class EFPropertySection extends AbstractPropertySection
		implements IEFFormPage {
	
	
	private EFPropertySheetPage page;
	
	private EFEditor editor;
	
	private Composite composite;
	
	private DataBindingSubContext subContext;
	
	
	protected EFPropertySection() {
	}
	
	
	@Override
	public void createControls(final Composite parent, final TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		if (!(aTabbedPropertySheetPage instanceof EFPropertySheetPage)) {
			return;
		}
		
		this.page= (EFPropertySheetPage) aTabbedPropertySheetPage;
		this.editor= this.page.getEditor();
		
		this.composite= getToolkit().createComposite(parent);
		this.composite.setLayout(createContentLayout());
		
		createContent(this.composite);
		
		this.subContext= new DataBindingSubContext(this.editor.getDataBinding().getContext());
		this.subContext.run(new Runnable() {
			@Override
			public void run() {
				initBindings();
			}
		});
	}
	
	protected Layout createContentLayout() {
//		return EFLayoutUtil.createPropertiesColumnLayout();
		return EFLayoutUtil.createPropertiesTableLayout(1);
	}
	
	protected abstract void createContent(Composite parent);
	
	protected IEMFEditContext getRootContext() {
		return this.editor.getDataBinding();
	}
	
	protected void initBindings() {
	}
	
	@Override
	public void setInput(final IWorkbenchPart part, final ISelection selection) {
		super.setInput(part, selection);
	}
	
	protected EObject adapt(final Object object) {
		if (object instanceof IAdaptable) {
			return ((IAdaptable) object).getAdapter(EObject.class);
		}
		return null;
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		if (this.subContext != null) {
			this.subContext.dispose();
			this.subContext= null;
		}
		this.editor= null;
		this.page= null;
	}
	
	
	@Override
	public EFEditor getEditor() {
		return this.editor;
	}
	
	@Override
	public EFToolkit getToolkit() {
		return this.editor.getToolkit();
	}
	
	@Override
	public void reflow(final boolean flushCache) {
	}
	
}
