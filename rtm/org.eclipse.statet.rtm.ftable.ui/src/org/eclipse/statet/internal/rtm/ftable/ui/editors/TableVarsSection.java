/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ftable.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.rtm.base.ui.rexpr.RExprListProperty;
import org.eclipse.statet.rtm.ftable.FTablePackage.Literals;


public class TableVarsSection extends EFFormSection {
	
	
	private final RExprListProperty colVarsProperty;
	private final RExprListProperty rowVarsProperty;
	
	
	public TableVarsSection(final IEFFormPage page, final Composite parent) {
		super(page, parent,
				"Variables",
				null );
		
		final EClass eClass= Literals.FTABLE;
		
		this.colVarsProperty= (RExprListProperty) FTableProperties.createProperty(
				eClass, Literals.FTABLE__COL_VARS,
				"Column Variables:", "The variables for facets in horizontal direction");
		this.rowVarsProperty= (RExprListProperty) FTableProperties.createProperty(
				eClass, Literals.FTABLE__ROW_VARS,
				"Row Variables:", "The variables for facets in vertical direction");
		
		createClient();
	}
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		{	final Composite column= new Composite(composite, SWT.NONE);
			column.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			column.setLayout(LayoutUtils.newCompositeGrid(3));
			
			this.colVarsProperty.create(column, page);
		}
		
		{	final Composite column= new Composite(composite, SWT.NONE);
			column.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true));
			column.setLayout(LayoutUtils.newCompositeGrid(3));
			
			this.rowVarsProperty.create(column, page);
		}
		LayoutUtils.addGDDummy(composite, true);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.colVarsProperty.bind(context);
		
		this.rowVarsProperty.bind(context);
	}
	
}
