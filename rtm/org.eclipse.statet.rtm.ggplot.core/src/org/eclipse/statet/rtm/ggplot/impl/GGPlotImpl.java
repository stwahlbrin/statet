/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.statet.rtm.ggplot.FacetLayout;
import org.eclipse.statet.rtm.ggplot.GGPlot;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.Layer;
import org.eclipse.statet.rtm.ggplot.PropXVarProvider;
import org.eclipse.statet.rtm.ggplot.PropYVarProvider;
import org.eclipse.statet.rtm.ggplot.TextStyle;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GG Plot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getData <em>Data</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getXVar <em>XVar</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getYVar <em>YVar</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getDataFilter <em>Data Filter</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getMainTitle <em>Main Title</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getMainTitleStyle <em>Main Title Style</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getFacet <em>Facet</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxXLim <em>Ax XLim</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxYLim <em>Ax YLim</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxXLabel <em>Ax XLabel</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxYLabel <em>Ax YLabel</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxXLabelStyle <em>Ax XLabel Style</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxYLabelStyle <em>Ax YLabel Style</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxXTextStyle <em>Ax XText Style</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getAxYTextStyle <em>Ax YText Style</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GGPlotImpl#getLayers <em>Layers</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class GGPlotImpl extends EObjectImpl implements GGPlot {
	/**
	 * The default value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr DATA_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr data= DATA_EDEFAULT;

	/**
	 * The default value of the '{@link #getXVar() <em>XVar</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXVar()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr XVAR_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getXVar() <em>XVar</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXVar()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr xVar= XVAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getYVar() <em>YVar</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYVar()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr YVAR_EDEFAULT= null; 

	/**
	 * The cached value of the '{@link #getYVar() <em>YVar</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYVar()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr yVar= YVAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getDataFilter() <em>Data Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataFilter()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr DATA_FILTER_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getDataFilter() <em>Data Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataFilter()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr dataFilter= DATA_FILTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getMainTitle() <em>Main Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainTitle()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr MAIN_TITLE_EDEFAULT= null; 

	/**
	 * The cached value of the '{@link #getMainTitle() <em>Main Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainTitle()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr mainTitle= MAIN_TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMainTitleStyle() <em>Main Title Style</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainTitleStyle()
	 * @generated
	 * @ordered
	 */
	protected TextStyle mainTitleStyle;

	/**
	 * The cached value of the '{@link #getFacet() <em>Facet</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFacet()
	 * @generated
	 * @ordered
	 */
	protected FacetLayout facet;

	/**
	 * The default value of the '{@link #getAxXLim() <em>Ax XLim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxXLim()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr AX_XLIM_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getAxXLim() <em>Ax XLim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxXLim()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr axXLim= AX_XLIM_EDEFAULT;

	/**
	 * The default value of the '{@link #getAxYLim() <em>Ax YLim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxYLim()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr AX_YLIM_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getAxYLim() <em>Ax YLim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxYLim()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr axYLim= AX_YLIM_EDEFAULT;

	/**
	 * The default value of the '{@link #getAxXLabel() <em>Ax XLabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxXLabel()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr AX_XLABEL_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getAxXLabel() <em>Ax XLabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxXLabel()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr axXLabel= AX_XLABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getAxYLabel() <em>Ax YLabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxYLabel()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr AX_YLABEL_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getAxYLabel() <em>Ax YLabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxYLabel()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr axYLabel= AX_YLABEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAxXLabelStyle() <em>Ax XLabel Style</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxXLabelStyle()
	 * @generated
	 * @ordered
	 */
	protected TextStyle axXLabelStyle;

	/**
	 * The cached value of the '{@link #getAxYLabelStyle() <em>Ax YLabel Style</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxYLabelStyle()
	 * @generated
	 * @ordered
	 */
	protected TextStyle axYLabelStyle;

	/**
	 * The cached value of the '{@link #getAxXTextStyle() <em>Ax XText Style</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxXTextStyle()
	 * @generated
	 * @ordered
	 */
	protected TextStyle axXTextStyle;

	/**
	 * The cached value of the '{@link #getAxYTextStyle() <em>Ax YText Style</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxYTextStyle()
	 * @generated
	 * @ordered
	 */
	protected TextStyle axYTextStyle;

	/**
	 * The cached value of the '{@link #getLayers() <em>Layers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayers()
	 * @generated
	 * @ordered
	 */
	protected EList<Layer> layers;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected GGPlotImpl() {
		super();
		
		NotificationChain msgs= null;
		{	final TextStyle style= GGPlotPackage.eINSTANCE.getGGPlotFactory().createTextStyle();
			msgs= ((InternalEObject)style).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE, null, msgs);
			msgs= basicSetMainTitleStyle(style, msgs); 
		}
		{	final TextStyle style= GGPlotPackage.eINSTANCE.getGGPlotFactory().createTextStyle();
			msgs= ((InternalEObject)style).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE, null, msgs);
			msgs= basicSetAxXLabelStyle(style, msgs); 
		}
		{	final TextStyle style= GGPlotPackage.eINSTANCE.getGGPlotFactory().createTextStyle();
			msgs= ((InternalEObject)style).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE, null, msgs);
			msgs= basicSetAxYLabelStyle(style, msgs); 
		}
		{	final TextStyle style= GGPlotPackage.eINSTANCE.getGGPlotFactory().createTextStyle();
			msgs= ((InternalEObject)style).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE, null, msgs);
			msgs= basicSetAxXTextStyle(style, msgs); 
		}
		{	final TextStyle style= GGPlotPackage.eINSTANCE.getGGPlotFactory().createTextStyle();
			msgs= ((InternalEObject)style).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE, null, msgs);
			msgs= basicSetAxYTextStyle(style, msgs); 
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GGPlotPackage.Literals.GG_PLOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getData() {
		return this.data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setData(final RTypedExpr newData) {
		final RTypedExpr oldData= this.data;
		this.data= newData;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__DATA, oldData, this.data));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getXVar() {
		return this.xVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setXVar(final RTypedExpr newXVar) {
		final RTypedExpr oldXVar= this.xVar;
		this.xVar= newXVar;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__XVAR, oldXVar, this.xVar));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getYVar() {
		return this.yVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYVar(final RTypedExpr newYVar) {
		final RTypedExpr oldYVar= this.yVar;
		this.yVar= newYVar;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__YVAR, oldYVar, this.yVar));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getDataFilter() {
		return this.dataFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDataFilter(final RTypedExpr newDataFilter) {
		final RTypedExpr oldDataFilter= this.dataFilter;
		this.dataFilter= newDataFilter;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__DATA_FILTER, oldDataFilter, this.dataFilter));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getMainTitle() {
		return this.mainTitle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMainTitle(final RTypedExpr newMainTitle) {
		final RTypedExpr oldMainTitle= this.mainTitle;
		this.mainTitle= newMainTitle;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__MAIN_TITLE, oldMainTitle, this.mainTitle));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextStyle getMainTitleStyle() {
		return this.mainTitleStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMainTitleStyle(final TextStyle newMainTitleStyle, NotificationChain msgs) {
		final TextStyle oldMainTitleStyle= this.mainTitleStyle;
		this.mainTitleStyle= newMainTitleStyle;
		if (eNotificationRequired()) {
			final ENotificationImpl notification= new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE, oldMainTitleStyle, newMainTitleStyle);
			if (msgs == null) {
				msgs= notification;
			}
			else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMainTitleStyle(final TextStyle newMainTitleStyle) {
		if (newMainTitleStyle != this.mainTitleStyle) {
			NotificationChain msgs= null;
			if (this.mainTitleStyle != null) {
				msgs= ((InternalEObject)this.mainTitleStyle).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE, null, msgs);
			}
			if (newMainTitleStyle != null) {
				msgs= ((InternalEObject)newMainTitleStyle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE, null, msgs);
			}
			msgs= basicSetMainTitleStyle(newMainTitleStyle, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		}
		else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE, newMainTitleStyle, newMainTitleStyle));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FacetLayout getFacet() {
		return this.facet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFacet(final FacetLayout newFacet, NotificationChain msgs) {
		final FacetLayout oldFacet= this.facet;
		this.facet= newFacet;
		if (eNotificationRequired()) {
			final ENotificationImpl notification= new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__FACET, oldFacet, newFacet);
			if (msgs == null) {
				msgs= notification;
			}
			else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFacet(final FacetLayout newFacet) {
		if (newFacet != this.facet) {
			NotificationChain msgs= null;
			if (this.facet != null) {
				msgs= ((InternalEObject)this.facet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__FACET, null, msgs);
			}
			if (newFacet != null) {
				msgs= ((InternalEObject)newFacet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__FACET, null, msgs);
			}
			msgs= basicSetFacet(newFacet, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		}
		else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__FACET, newFacet, newFacet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getAxXLim() {
		return this.axXLim;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxXLim(final RTypedExpr newAxXLim) {
		final RTypedExpr oldAxXLim= this.axXLim;
		this.axXLim= newAxXLim;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_XLIM, oldAxXLim, this.axXLim));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getAxYLim() {
		return this.axYLim;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxYLim(final RTypedExpr newAxYLim) {
		final RTypedExpr oldAxYLim= this.axYLim;
		this.axYLim= newAxYLim;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_YLIM, oldAxYLim, this.axYLim));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getAxXLabel() {
		return this.axXLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxXLabel(final RTypedExpr newAxXLabel) {
		final RTypedExpr oldAxXLabel= this.axXLabel;
		this.axXLabel= newAxXLabel;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_XLABEL, oldAxXLabel, this.axXLabel));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getAxYLabel() {
		return this.axYLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxYLabel(final RTypedExpr newAxYLabel) {
		final RTypedExpr oldAxYLabel= this.axYLabel;
		this.axYLabel= newAxYLabel;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_YLABEL, oldAxYLabel, this.axYLabel));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextStyle getAxXLabelStyle() {
		return this.axXLabelStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAxXLabelStyle(final TextStyle newAxXLabelStyle, NotificationChain msgs) {
		final TextStyle oldAxXLabelStyle= this.axXLabelStyle;
		this.axXLabelStyle= newAxXLabelStyle;
		if (eNotificationRequired()) {
			final ENotificationImpl notification= new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE, oldAxXLabelStyle, newAxXLabelStyle);
			if (msgs == null) {
				msgs= notification;
			}
			else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxXLabelStyle(final TextStyle newAxXLabelStyle) {
		if (newAxXLabelStyle != this.axXLabelStyle) {
			NotificationChain msgs= null;
			if (this.axXLabelStyle != null) {
				msgs= ((InternalEObject)this.axXLabelStyle).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE, null, msgs);
			}
			if (newAxXLabelStyle != null) {
				msgs= ((InternalEObject)newAxXLabelStyle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE, null, msgs);
			}
			msgs= basicSetAxXLabelStyle(newAxXLabelStyle, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		}
		else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE, newAxXLabelStyle, newAxXLabelStyle));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextStyle getAxYLabelStyle() {
		return this.axYLabelStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAxYLabelStyle(final TextStyle newAxYLabelStyle, NotificationChain msgs) {
		final TextStyle oldAxYLabelStyle= this.axYLabelStyle;
		this.axYLabelStyle= newAxYLabelStyle;
		if (eNotificationRequired()) {
			final ENotificationImpl notification= new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE, oldAxYLabelStyle, newAxYLabelStyle);
			if (msgs == null) {
				msgs= notification;
			}
			else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxYLabelStyle(final TextStyle newAxYLabelStyle) {
		if (newAxYLabelStyle != this.axYLabelStyle) {
			NotificationChain msgs= null;
			if (this.axYLabelStyle != null) {
				msgs= ((InternalEObject)this.axYLabelStyle).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE, null, msgs);
			}
			if (newAxYLabelStyle != null) {
				msgs= ((InternalEObject)newAxYLabelStyle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE, null, msgs);
			}
			msgs= basicSetAxYLabelStyle(newAxYLabelStyle, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		}
		else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE, newAxYLabelStyle, newAxYLabelStyle));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextStyle getAxXTextStyle() {
		return this.axXTextStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAxXTextStyle(final TextStyle newAxXTextStyle, NotificationChain msgs) {
		final TextStyle oldAxXTextStyle= this.axXTextStyle;
		this.axXTextStyle= newAxXTextStyle;
		if (eNotificationRequired()) {
			final ENotificationImpl notification= new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE, oldAxXTextStyle, newAxXTextStyle);
			if (msgs == null) {
				msgs= notification;
			}
			else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxXTextStyle(final TextStyle newAxXTextStyle) {
		if (newAxXTextStyle != this.axXTextStyle) {
			NotificationChain msgs= null;
			if (this.axXTextStyle != null) {
				msgs= ((InternalEObject)this.axXTextStyle).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE, null, msgs);
			}
			if (newAxXTextStyle != null) {
				msgs= ((InternalEObject)newAxXTextStyle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE, null, msgs);
			}
			msgs= basicSetAxXTextStyle(newAxXTextStyle, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		}
		else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE, newAxXTextStyle, newAxXTextStyle));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextStyle getAxYTextStyle() {
		return this.axYTextStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAxYTextStyle(final TextStyle newAxYTextStyle, NotificationChain msgs) {
		final TextStyle oldAxYTextStyle= this.axYTextStyle;
		this.axYTextStyle= newAxYTextStyle;
		if (eNotificationRequired()) {
			final ENotificationImpl notification= new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE, oldAxYTextStyle, newAxYTextStyle);
			if (msgs == null) {
				msgs= notification;
			}
			else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAxYTextStyle(final TextStyle newAxYTextStyle) {
		if (newAxYTextStyle != this.axYTextStyle) {
			NotificationChain msgs= null;
			if (this.axYTextStyle != null) {
				msgs= ((InternalEObject)this.axYTextStyle).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE, null, msgs);
			}
			if (newAxYTextStyle != null) {
				msgs= ((InternalEObject)newAxYTextStyle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE, null, msgs);
			}
			msgs= basicSetAxYTextStyle(newAxYTextStyle, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		}
		else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE, newAxYTextStyle, newAxYTextStyle));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Layer> getLayers() {
		if (this.layers == null) {
			this.layers= new EObjectContainmentEList<>(Layer.class, this, GGPlotPackage.GG_PLOT__LAYERS);
		}
		return this.layers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(final InternalEObject otherEnd, final int featureID, final NotificationChain msgs) {
		switch (featureID) {
			case GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE:
				return basicSetMainTitleStyle(null, msgs);
			case GGPlotPackage.GG_PLOT__FACET:
				return basicSetFacet(null, msgs);
			case GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE:
				return basicSetAxXLabelStyle(null, msgs);
			case GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE:
				return basicSetAxYLabelStyle(null, msgs);
			case GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE:
				return basicSetAxXTextStyle(null, msgs);
			case GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE:
				return basicSetAxYTextStyle(null, msgs);
			case GGPlotPackage.GG_PLOT__LAYERS:
				return ((InternalEList<?>)getLayers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
		switch (featureID) {
			case GGPlotPackage.GG_PLOT__DATA:
				return getData();
			case GGPlotPackage.GG_PLOT__XVAR:
				return getXVar();
			case GGPlotPackage.GG_PLOT__YVAR:
				return getYVar();
			case GGPlotPackage.GG_PLOT__DATA_FILTER:
				return getDataFilter();
			case GGPlotPackage.GG_PLOT__MAIN_TITLE:
				return getMainTitle();
			case GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE:
				return getMainTitleStyle();
			case GGPlotPackage.GG_PLOT__FACET:
				return getFacet();
			case GGPlotPackage.GG_PLOT__AX_XLIM:
				return getAxXLim();
			case GGPlotPackage.GG_PLOT__AX_YLIM:
				return getAxYLim();
			case GGPlotPackage.GG_PLOT__AX_XLABEL:
				return getAxXLabel();
			case GGPlotPackage.GG_PLOT__AX_YLABEL:
				return getAxYLabel();
			case GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE:
				return getAxXLabelStyle();
			case GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE:
				return getAxYLabelStyle();
			case GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE:
				return getAxXTextStyle();
			case GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE:
				return getAxYTextStyle();
			case GGPlotPackage.GG_PLOT__LAYERS:
				return getLayers();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(final int featureID, final Object newValue) {
		switch (featureID) {
			case GGPlotPackage.GG_PLOT__DATA:
				setData((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__XVAR:
				setXVar((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__YVAR:
				setYVar((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__DATA_FILTER:
				setDataFilter((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__MAIN_TITLE:
				setMainTitle((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE:
				setMainTitleStyle((TextStyle)newValue);
				return;
			case GGPlotPackage.GG_PLOT__FACET:
				setFacet((FacetLayout)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_XLIM:
				setAxXLim((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_YLIM:
				setAxYLim((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_XLABEL:
				setAxXLabel((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_YLABEL:
				setAxYLabel((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE:
				setAxXLabelStyle((TextStyle)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE:
				setAxYLabelStyle((TextStyle)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE:
				setAxXTextStyle((TextStyle)newValue);
				return;
			case GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE:
				setAxYTextStyle((TextStyle)newValue);
				return;
			case GGPlotPackage.GG_PLOT__LAYERS:
				getLayers().clear();
				getLayers().addAll((Collection<? extends Layer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.GG_PLOT__DATA:
				setData(DATA_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__XVAR:
				setXVar(XVAR_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__YVAR:
				setYVar(YVAR_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__DATA_FILTER:
				setDataFilter(DATA_FILTER_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__MAIN_TITLE:
				setMainTitle(MAIN_TITLE_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE:
				setMainTitleStyle((TextStyle)null);
				return;
			case GGPlotPackage.GG_PLOT__FACET:
				setFacet((FacetLayout)null);
				return;
			case GGPlotPackage.GG_PLOT__AX_XLIM:
				setAxXLim(AX_XLIM_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__AX_YLIM:
				setAxYLim(AX_YLIM_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__AX_XLABEL:
				setAxXLabel(AX_XLABEL_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__AX_YLABEL:
				setAxYLabel(AX_YLABEL_EDEFAULT);
				return;
			case GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE:
				setAxXLabelStyle((TextStyle)null);
				return;
			case GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE:
				setAxYLabelStyle((TextStyle)null);
				return;
			case GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE:
				setAxXTextStyle((TextStyle)null);
				return;
			case GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE:
				setAxYTextStyle((TextStyle)null);
				return;
			case GGPlotPackage.GG_PLOT__LAYERS:
				getLayers().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.GG_PLOT__DATA:
				return DATA_EDEFAULT == null ? this.data != null : !DATA_EDEFAULT.equals(this.data);
			case GGPlotPackage.GG_PLOT__XVAR:
				return XVAR_EDEFAULT == null ? this.xVar != null : !XVAR_EDEFAULT.equals(this.xVar);
			case GGPlotPackage.GG_PLOT__YVAR:
				return YVAR_EDEFAULT == null ? this.yVar != null : !YVAR_EDEFAULT.equals(this.yVar);
			case GGPlotPackage.GG_PLOT__DATA_FILTER:
				return DATA_FILTER_EDEFAULT == null ? this.dataFilter != null : !DATA_FILTER_EDEFAULT.equals(this.dataFilter);
			case GGPlotPackage.GG_PLOT__MAIN_TITLE:
				return MAIN_TITLE_EDEFAULT == null ? this.mainTitle != null : !MAIN_TITLE_EDEFAULT.equals(this.mainTitle);
			case GGPlotPackage.GG_PLOT__MAIN_TITLE_STYLE:
				return this.mainTitleStyle != null;
			case GGPlotPackage.GG_PLOT__FACET:
				return this.facet != null;
			case GGPlotPackage.GG_PLOT__AX_XLIM:
				return AX_XLIM_EDEFAULT == null ? this.axXLim != null : !AX_XLIM_EDEFAULT.equals(this.axXLim);
			case GGPlotPackage.GG_PLOT__AX_YLIM:
				return AX_YLIM_EDEFAULT == null ? this.axYLim != null : !AX_YLIM_EDEFAULT.equals(this.axYLim);
			case GGPlotPackage.GG_PLOT__AX_XLABEL:
				return AX_XLABEL_EDEFAULT == null ? this.axXLabel != null : !AX_XLABEL_EDEFAULT.equals(this.axXLabel);
			case GGPlotPackage.GG_PLOT__AX_YLABEL:
				return AX_YLABEL_EDEFAULT == null ? this.axYLabel != null : !AX_YLABEL_EDEFAULT.equals(this.axYLabel);
			case GGPlotPackage.GG_PLOT__AX_XLABEL_STYLE:
				return this.axXLabelStyle != null;
			case GGPlotPackage.GG_PLOT__AX_YLABEL_STYLE:
				return this.axYLabelStyle != null;
			case GGPlotPackage.GG_PLOT__AX_XTEXT_STYLE:
				return this.axXTextStyle != null;
			case GGPlotPackage.GG_PLOT__AX_YTEXT_STYLE:
				return this.axYTextStyle != null;
			case GGPlotPackage.GG_PLOT__LAYERS:
				return this.layers != null && !this.layers.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(final int derivedFeatureID, final Class<?> baseClass) {
		if (baseClass == PropXVarProvider.class) {
			switch (derivedFeatureID) {
				case GGPlotPackage.GG_PLOT__XVAR: return GGPlotPackage.PROP_XVAR_PROVIDER__XVAR;
				default: return -1;
			}
		}
		if (baseClass == PropYVarProvider.class) {
			switch (derivedFeatureID) {
				case GGPlotPackage.GG_PLOT__YVAR: return GGPlotPackage.PROP_YVAR_PROVIDER__YVAR;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(final int baseFeatureID, final Class<?> baseClass) {
		if (baseClass == PropXVarProvider.class) {
			switch (baseFeatureID) {
				case GGPlotPackage.PROP_XVAR_PROVIDER__XVAR: return GGPlotPackage.GG_PLOT__XVAR;
				default: return -1;
			}
		}
		if (baseClass == PropYVarProvider.class) {
			switch (baseFeatureID) {
				case GGPlotPackage.PROP_YVAR_PROVIDER__YVAR: return GGPlotPackage.GG_PLOT__YVAR;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		final StringBuffer result= new StringBuffer(super.toString());
		result.append(" (data: "); //$NON-NLS-1$
		result.append(this.data);
		result.append(", xVar: "); //$NON-NLS-1$
		result.append(this.xVar);
		result.append(", yVar: "); //$NON-NLS-1$
		result.append(this.yVar);
		result.append(", dataFilter: "); //$NON-NLS-1$
		result.append(this.dataFilter);
		result.append(", mainTitle: "); //$NON-NLS-1$
		result.append(this.mainTitle);
		result.append(", axXLim: "); //$NON-NLS-1$
		result.append(this.axXLim);
		result.append(", axYLim: "); //$NON-NLS-1$
		result.append(this.axYLim);
		result.append(", axXLabel: "); //$NON-NLS-1$
		result.append(this.axXLabel);
		result.append(", axYLabel: "); //$NON-NLS-1$
		result.append(this.axYLabel);
		result.append(')');
		return result.toString();
	}

} //GGPlotImpl
