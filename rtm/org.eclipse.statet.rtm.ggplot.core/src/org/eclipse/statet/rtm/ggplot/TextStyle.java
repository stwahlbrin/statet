/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Text Style</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.TextStyle#getFontFamily <em>Font Family</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.TextStyle#getFontFace <em>Font Face</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.TextStyle#getHJust <em>HJust</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.TextStyle#getVJust <em>VJust</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.TextStyle#getAngle <em>Angle</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getTextStyle()
 * @model
 * @generated
 */
public interface TextStyle extends PropSizeProvider, PropColorProvider {
	/**
	 * Returns the value of the '<em><b>Font Family</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Font Family</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font Family</em>' attribute.
	 * @see #setFontFamily(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getTextStyle_FontFamily()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RFontFamily"
	 * @generated
	 */
	RTypedExpr getFontFamily();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.TextStyle#getFontFamily <em>Font Family</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font Family</em>' attribute.
	 * @see #getFontFamily()
	 * @generated
	 */
	void setFontFamily(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>Font Face</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Font Face</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font Face</em>' attribute.
	 * @see #setFontFace(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getTextStyle_FontFace()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RText"
	 * @generated
	 */
	RTypedExpr getFontFace();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.TextStyle#getFontFace <em>Font Face</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font Face</em>' attribute.
	 * @see #getFontFace()
	 * @generated
	 */
	void setFontFace(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>HJust</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>HJust</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>HJust</em>' attribute.
	 * @see #setHJust(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getTextStyle_HJust()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RNum"
	 * @generated
	 */
	RTypedExpr getHJust();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.TextStyle#getHJust <em>HJust</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HJust</em>' attribute.
	 * @see #getHJust()
	 * @generated
	 */
	void setHJust(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>VJust</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>VJust</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VJust</em>' attribute.
	 * @see #setVJust(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getTextStyle_VJust()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RNum"
	 * @generated
	 */
	RTypedExpr getVJust();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.TextStyle#getVJust <em>VJust</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VJust</em>' attribute.
	 * @see #getVJust()
	 * @generated
	 */
	void setVJust(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>Angle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Angle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Angle</em>' attribute.
	 * @see #setAngle(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getTextStyle_Angle()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RNum"
	 * @generated
	 */
	RTypedExpr getAngle();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.TextStyle#getAngle <em>Angle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Angle</em>' attribute.
	 * @see #getAngle()
	 * @generated
	 */
	void setAngle(RTypedExpr value);

} // TextStyle
