/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.util;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;


public class RExprTypes {
	
	
	private final ImList<RExprType> types;
	
	private final String defaultTypeKey;
	
	
	public RExprTypes(final RExprType type) {
		this(ImCollections.newList(type), type.getTypeKey());
	}
	
	public RExprTypes(final ImList<RExprType> types, final int defaultTypeIdx) {
		this.types= types;
		this.defaultTypeKey= types.get(defaultTypeIdx).getTypeKey();
	}
	
	public RExprTypes(final ImList<RExprType> types, final String defaultType) {
		this.types= types;
		this.defaultTypeKey= defaultType;
	}
	
	
	public List<RExprType> getTypes() {
		return this.types;
	}
	
	public String getDefaultTypeKey() {
		return this.defaultTypeKey;
	}
	
	public boolean contains(final String typeKey) {
		for (int i= 0; i < this.types.size(); i++) {
			if (this.types.get(i).getTypeKey() == typeKey) {
				return true;
			}
		}
		return false;
	}
	
}
