/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj;


/**
 * Exception indicating that setting the configuration failed because it is invalid
 */
public class RjInvalidConfigurationException extends RjException {
	
	
	private static final long serialVersionUID= 6709030537472523580L;
	
	
	public RjInvalidConfigurationException(final String message) {
		super(message);
	}
	
	public RjInvalidConfigurationException(final String message, final Throwable cause) {
		super(message, cause);
	}
	
}
