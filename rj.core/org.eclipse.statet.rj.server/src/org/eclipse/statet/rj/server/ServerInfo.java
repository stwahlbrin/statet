/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.Serializable;


public class ServerInfo implements Serializable {
	
	
	private static final long serialVersionUID= -5411479269748201535L;
	
	public static final String USER_OWNER= "owner";
	public static final String USER_CONSOLE= "console";
	public static final String USER_RSERVI= "rservi";
	
	
	private final String name;
	private final String[] userTypes;
	private final String[] userNames;
	private final String directory;
	private final long timestamp;
	private final int state;
	
	
	public ServerInfo(final String name, final String directory, final long timestamp,
			final String[] userTypes, final String[] userNames,
			final int state) {
		this.name= name;
		this.userTypes= userTypes;
		this.userNames= userNames;
		this.directory= directory;
		this.timestamp= timestamp;
		this.state= state;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public String getUsername(final String type) {
		for (int i= 0; i < this.userTypes.length; i++) {
			if (this.userTypes[i].equals(type)) {
				return this.userNames[i];
			}
		}
		return null;
	}
	
	public String getDirectory() {
		return this.directory;
	}
	
	public long getTimestamp() {
		return this.timestamp;
	}
	
	public int getState() {
		return this.state;
	}
	
}
