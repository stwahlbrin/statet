/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Interface of the R server visible for the remote clients.
 * 
 */
@NonNullByDefault
public interface Server extends Remote {
	
	/**
	 * Status code indicating, that the R engine is not yet started.
	 */
	int S_NOT_STARTED=        0x00011;
	
	/**
	 * Status code indicating, that the R engine is started and a/the client is connected.
	 */
	int S_CONNECTED=          0x00014;
	
	/**
	 * Status code indicating, that the R engine is started and the client is connected but
	 * not active during the last minutes.
	 */
	int S_CONNECTED_STALE=    0x00016;
	
	/**
	 * Status code indicating, that the R engine is started and the client was disconnected.
	 */
	int S_DISCONNECTED=       0x00018;
	
	/**
	 * Status code indicating, that R engine is started and the client-server connection was lost.
	 */
	int S_LOST=               0x00019;
	
	/**
	 * Status code indicating, that the server was stopped.
	 */
	int S_STOPPED= 0x0001a;
	
	
	String C_CONSOLE_START= "console.start";
	
	String C_CONSOLE_CONNECT= "console.connect";
	
	String C_RSERVI_NODECONTROL= "rservi.nodecontrol";
	
	
	/**
	 * Return the triple of API version of the this server.
	 * 
	 * @return the version number
	 * @throws RemoteException
	 */
	int[] getVersion() throws RemoteException;
	
	/**
	 * Returns the current server information.
	 * 
	 * The information represents the state this method is call and is not updated. To check for
	 * updates the method must be called again.
	 * 
	 * @return a server information object
	 * @throws RemoteException
	 */
	ServerInfo getInfo() throws RemoteException;
	
	/**
	 * Current state of this server. One of the constants with S_ prefix.
	 * 
	 * @return current state
	 * @throws RemoteException
	 */
	int getState() throws RemoteException;
	
	
	/**
	 * Creates and returns the ServerLogin with all information necessary to login
	 * ({@link #execute(String, Map, ServerLogin)}) to run the specified command.
	 * 
	 * @param the execute command constant the login will be for
	 * @return the server login for the command
	 * @throws RemoteException
	 */
	ServerLogin createLogin(String command) throws RemoteException;
	
	
	/**
	 * Executes the specified server command.
	 * 
	 * The ids of predefined commands are available as constants with C_ prefix.
	 * 
	 * @param command the id of the command to execute
	 * @param login the login credentials, an answer of {@link #createConsoleLogin()}
	 * @return the return value of the command, see command description
	 * @throws LoginException if login failed
	 * @throws RemoteException
	 */
	@Nullable Object execute(String command, Map<String, ? extends @NonNull Object> args,
			ServerLogin login) throws LoginException, RemoteException;
	
}
