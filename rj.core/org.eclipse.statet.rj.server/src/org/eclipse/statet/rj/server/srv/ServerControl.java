/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.srv;

import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.server.util.ServerUtils;


@NonNullByDefault
public abstract class ServerControl {
	
	
	public static final int EXIT_ARGS=                      130;
	
	public static final int EXIT_INIT=                      140;
	
	public static final int EXIT_INIT_LOGGING_ERROR=        EXIT_INIT | 1;
	public static final int EXIT_INIT_AUTHMETHOD_ERROR=     EXIT_INIT | 3;
	public static final int EXIT_INIT_RENGINE_ERROR=        EXIT_INIT | 5;
	
	
	private static boolean VERBOSE= false;
	private static boolean VERBOSE_ON_ERROR= true;
	
	public static void initVerbose() {
		if (VERBOSE) {
			return;
		}
		VERBOSE= true;
		
		LOGGER.setLevel(Level.ALL); // default is Level.INFO
//		System.setProperty("java.rmi.server.logCalls", "true");
//		RemoteServer.setLog(System.err);
		ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
		
		Logger logger= LOGGER;
		SEARCH_CONSOLE: while (logger != null) {
			for (final Handler handler : logger.getHandlers()) {
				if (handler instanceof ConsoleHandler) {
					handler.setLevel(Level.ALL);
					break SEARCH_CONSOLE;
				}
			}
			if (!logger.getUseParentHandlers()) {
				break SEARCH_CONSOLE;
			}
			logger= logger.getParent();
		}
		
		final StringBuilder sb= new StringBuilder(256);
		
		LOGGER.log(Level.CONFIG, "verbose mode enabled.");
		
		sb.setLength(0);
		sb.append("java properties:");
		ServerUtils.prettyPrint(System.getProperties(), sb);
		LOGGER.log(Level.CONFIG, sb.toString());
		
		sb.setLength(0);
		sb.append("env variables:");
		ServerUtils.prettyPrint(System.getenv(), sb);
		LOGGER.log(Level.CONFIG, sb.toString());
	}
	
	public static void exit(final int status) {
		try {
			if (status != 0 && VERBOSE_ON_ERROR) {
				initVerbose();
			}
			System.err.flush();
			System.out.flush();
		}
		finally {
			Runtime.getRuntime().exit(status);
		}
	}
	
	
	protected static final Logger LOGGER= Logger.getLogger("org.eclipse.statet.rj.server.srv");
	
	
	
	private final Map<String, String> options;
	
	
	public ServerControl(final Map<String, String> options) {
		this.options= options;
	}
	
	
	public Map<String, String> getOptions() {
		return this.options;
	}
	
}
