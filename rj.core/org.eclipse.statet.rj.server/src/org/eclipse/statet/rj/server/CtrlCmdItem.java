/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


/**
 * Client-to-Server list with {@link MainCmdItem}s.
 */
public final class CtrlCmdItem implements RjsComObject, RJIOExternalizable, Externalizable {
	
	
	public static final int REQUEST_CANCEL= 1;
	public static final int REQUEST_HOT_MODE= 2;
	
	
	private int ctrl;
	
	
	public CtrlCmdItem(final int ctrlId) {
		this.ctrl= ctrlId;
	}
	
	/**
	 * Constructor for automatic deserialization
	 */
	public CtrlCmdItem() {
	}
	
	/**
	 * Constructor for automatic deserialization
	 * @throws IOException 
	 */
	public CtrlCmdItem(final RJIO in) throws IOException {
		this.ctrl= in.readInt();
	}
	
	@Override
	public void readExternal(final ObjectInput in) throws IOException {
		this.ctrl= in.readInt();
	}
	
	@Override
	public void writeExternal(final RJIO out) throws IOException {
		out.writeInt(this.ctrl);
	}
	
	@Override
	public void writeExternal(final ObjectOutput out) throws IOException {
		out.writeInt(this.ctrl);
	}
	
	
	@Override
	public int getComType() {
		return RjsComObject.T_CTRL;
	}
	
	public int getCtrlId() {
		return this.ctrl;
	}
	
	
	public boolean testEquals(final CtrlCmdItem other) {
		if (this.ctrl != other.ctrl) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(64);
		sb.append("CtrlCmdItem ");
		switch (this.ctrl) {
		case REQUEST_CANCEL:
			sb.append("REQUEST_CANCEL");
			break;
		case REQUEST_HOT_MODE:
			sb.append("REQUEST_HOT_MODE");
			break;
		default:
			sb.append(this.ctrl);
			break;
		}
		return sb.toString();
	}
	
}
