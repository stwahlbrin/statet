/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
final class AutoIdMap<E> {
	
	
	private @Nullable Object[] currentArray;
	
	
	public AutoIdMap() {
		this.currentArray= new @Nullable Object[16];
	}
	
	
	public synchronized int put(final @NonNull E e) {
		final int size= this.currentArray.length;
		for (int i= 1; i < size; i++) {
			if (this.currentArray[i] == null) {
				this.currentArray[i]= e;
				return i;
			}
		}
		this.currentArray= Arrays.copyOf(this.currentArray, size + 16);
		this.currentArray[size]= e;
		return size;
	}
	
	public synchronized void remove(final int id) {
		if (id > 0 && id < this.currentArray.length) {
			this.currentArray[id]= null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public E get(final int id) {
		return (E) this.currentArray[id];
	}
	
}
