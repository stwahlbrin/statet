/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class TracepointStatesUpdate implements RJIOExternalizable {
	
	
	private final static int RESET=                         0x00000001;
	
	
	private final int properties;
	
	private final List<TracepointState> states;
	
	
	public TracepointStatesUpdate(final List<TracepointState> states,
			final boolean reset) {
		this.properties= (reset) ? RESET : 0;
		this.states= states;
	}
	
	public TracepointStatesUpdate(final RJIO io) throws IOException {
		this.properties= io.readInt();
		this.states= TracepointState.readList(io);
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeInt(this.properties);
		TracepointState.writeList(this.states, io);
	}
	
	
	public boolean getReset() {
		return ((this.properties & RESET) != 0);
	}
	
	public List<TracepointState> getStates() {
		return this.states;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(TracepointStatesUpdate.class, getClass());
		sb.addProp("reset", getReset());
		sb.addProp("states", getStates());
		return sb.toString();
	}
	
}
