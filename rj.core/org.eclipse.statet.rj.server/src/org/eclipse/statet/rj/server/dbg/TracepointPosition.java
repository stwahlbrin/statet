/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;
import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class TracepointPosition implements Tracepoint, Comparable<TracepointPosition>,
		RJIOExternalizable {
	
	
	private final int type;
	
	private final long id;
	
	private final int[] exprIndex;
	private final int[] @Nullable [] exprSrcrefs;
	
	
	public TracepointPosition(final int type, final long id,
			final int[] index) {
		this.type= type;
		this.id= id;
		this.exprIndex= nonNullAssert(index);
		this.exprSrcrefs= new int[this.exprIndex.length] @Nullable [];
	}
	
	public TracepointPosition(final int type, final long id,
			final int[] index, final int @Nullable [] srcref) {
		this.type= type;
		this.id= id;
		this.exprIndex= nonNullAssert(index);
		this.exprSrcrefs= new int[this.exprIndex.length] @Nullable [];
		this.exprSrcrefs[this.exprSrcrefs.length - 1]= srcref;
	}
	
	TracepointPosition(final RJIO io) throws IOException {
		this.type= io.readInt();
		this.id= io.readLong();
		this.exprIndex= nonNullAssert(io.readIntArray());
		this.exprSrcrefs= new int[this.exprIndex.length] @Nullable [];
		for (int i= 0; i < this.exprSrcrefs.length; i++) {
			this.exprSrcrefs[i]= io.readIntArray();
		}
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeInt(this.type);
		io.writeLong(this.id);
		io.writeIntArray(this.exprIndex, this.exprIndex.length);
		for (int i= 0; i < this.exprSrcrefs.length; i++) {
			final int[] srcref= this.exprSrcrefs[i];
			io.writeIntArray(srcref, (srcref != null) ? 6 : -1);
		}
	}
	
	
	@Override
	public int getType() {
		return this.type;
	}
	
	@Override
	public long getId() {
		return this.id;
	}
	
	public int[] getIndex() {
		return this.exprIndex;
	}
	
	public int [] @Nullable [] getSrcrefs() {
		return this.exprSrcrefs;
	}
	
	public int @Nullable [] getSrcref(final int level) {
		return this.exprSrcrefs[level];
	}
	
	public int @Nullable [] getSrcref() {
		return (this.exprSrcrefs.length > 0) ?
				this.exprSrcrefs[this.exprSrcrefs.length - 1] :
				null;
	}
	
	
	@Override
	public int compareTo(final TracepointPosition other) {
		for (int i= 0; i < this.exprIndex.length; i++) {
			if (i < other.exprIndex.length) {
				final int diff= this.exprIndex[i] - other.exprIndex[i];
				if (diff != 0) {
					return diff;
				}
				else {
					continue;
				}
			}
			return 1; // this deeper
		}
		if (this.exprIndex.length != other.exprIndex.length) {
			return -1; // other deeper
		}
		return this.type - other.type;
	}
	
	@Override
	public int hashCode() {
		int h= this.type;
		for (int i= 0; i < this.exprIndex.length; i++) {
			h= this.exprIndex[i] * 128 * (i + 1) ^ 2;
		}
		return h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof TracepointPosition) {
			final TracepointPosition other= (TracepointPosition) obj;
			return (this.type == other.type
					&& Arrays.equals(this.exprIndex, other.exprIndex) );
		}
		return false;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("TracepointPosition"); //$NON-NLS-1$
		sb.append(" (type= ").append(this.type).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append("\n\t" + "exprIndex= ").append(Arrays.toString(this.exprIndex)); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append("\n\t" + "exprSrcref= ").append(Arrays.toString(getSrcref())); //$NON-NLS-1$ //$NON-NLS-2$
		return sb.toString();
	}
	
}
