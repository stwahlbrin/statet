/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface Tracepoint {
	
	/** all breakpoint types */
	byte TYPE_BREAKPOINT=                                   0x0_0000_000F;
	/** function breakpoint */
	byte TYPE_FB=                                           0x0_0000_0001;
	/** line breakpoint */
	byte TYPE_LB=                                           0x0_0000_0002;
	/** toplevel line breakpoint */
	byte TYPE_TB=                                           0x0_0000_0003;
	/** exception breakpoint */
	byte TYPE_EB=                                           0x0_0000_0005;
	
	int TYPE_DELETED=                                       0x0_0100_0000;
	
	
	int getType();
	
	long getId();
	
}
