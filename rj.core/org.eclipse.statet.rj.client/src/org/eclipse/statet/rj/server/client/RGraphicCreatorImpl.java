/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.graphic.core.RGraphic;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RGraphicCreator;
import org.eclipse.statet.rj.services.RService;


/**
 * Default implementation for {@link RGraphicCreator}.
 * <p>
 * Requires R package <code>rj</code>.</p>
 */
public class RGraphicCreatorImpl implements RGraphicCreator {
	
	
	private final RService service;
	private final AbstractRJComClient rjs;
	
	private int options;
	
	private double width= -1;
	private double height= -1;
	
	
	public RGraphicCreatorImpl(final RService service, final AbstractRJComClient rjs, final int options) {
		this.service= service;
		this.rjs= rjs;
	}
	
	
	@Override
	public void setSize(final double width, final double height) {
		if (width == -1 && height == -1) {
			this.width= -1;
			this.height= -1;
			return;
		}
		if (width <= 0 || height <= 0) {
			throw new IllegalArgumentException();
		}
		this.width= width;
		this.height= height;
	}
	
	@Override
	public RGraphic create(final String expression,
			final ProgressMonitor m) throws StatusException {
		return create(expression, null, m);
	}
	
	@Override
	public RGraphic create(final FunctionCall fcall,
			final ProgressMonitor m) throws StatusException {
		return create(null, fcall, m);
	}
	
	private RGraphic create(final String expression, final FunctionCall fcall,
			final ProgressMonitor m) throws StatusException {
		final int savedOptions= this.rjs.getGraphicOptions();
		int graphicOptions= this.options;
		if ((this.options & RClientGraphicFactory.MANAGED_ON) == 0) {
			graphicOptions |= RClientGraphicFactory.MANAGED_OFF;
		}
		if ((this.options & RClientGraphicFactory.R_CLOSE_ON) == 0) {
			graphicOptions |= RClientGraphicFactory.R_CLOSE_OFF;
		}
		this.rjs.setGraphicOptions(graphicOptions);
		final RClientGraphic graphic;
		try {
			if (this.width < 0) {
				this.service.evalVoid("rj::rj.GD()", m);
			}
			else {
				this.service.evalVoid("rj::rj.GD(" +
						"width= " + this.width + ", height= "+this.height+", size.unit= \"px\"" +
						")", m );
			}
			if (expression != null) {
				this.service.evalData(expression, m);
			}
			else {
				fcall.evalVoid(m);
			}
			graphic= this.rjs.getLastGraphic();
		}
		finally {
			this.rjs.setGraphicOptions(savedOptions);
		}
		return ((graphic instanceof RGraphic) ? ((RGraphic) graphic) : null);
	}
	
}
