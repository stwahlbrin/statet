/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;
import java.util.HashMap;

import org.eclipse.statet.rj.data.RJIO;


public class RUniqueCharacterHash32Store extends RUniqueCharacter32Store {
	
	
	private final HashMap<String, Integer> map;
	
	
	public RUniqueCharacterHash32Store(final String names[]) {
		super(names);
		this.map= new HashMap<>();
		initMap();
	}
	
	RUniqueCharacterHash32Store(final RCharacter32Store source, final boolean reuse) {
		super(source, reuse);
		this.map= new HashMap<>();
		initMap();
	}
	
	public RUniqueCharacterHash32Store(final RJIO io, final int length) throws IOException {
		super(io, length);
		this.map= new HashMap<>();
		initMap();
	}
	
	
	protected void initMap() {
		final int length= (int)getLength();
		for (int idx= 0; idx < length; idx++) {
			if (this.charValues[idx] != null) {
				this.map.put(this.charValues[idx], idx);
			}
		}
	}
	
	
	@Override
	public void setChar(final int idx, final String value) {
		final String previous= getChar(idx);
		super.setChar(idx, value);
		this.map.remove(previous);
		this.map.put(value, idx);
	}
	
	@Override
	public void setChar(final long idx, final String value) {
		if (idx < 0 || idx >= getLength()) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		setChar((int)idx, value);
	}
	
	@Override
	public void insertChar(final int idx, final String name) {
		super.insertChar(idx, name);
		this.map.put(name, idx);
	}
	
	@Override
	public void remove(final int idx) {
		this.map.remove(getChar(idx));
		super.remove(idx);
	}
	
	
	@Override
	public boolean contains(final String value) {
		return (value != null && this.map.containsKey(value));
	}
	
	@Override
	public long indexOf(final String name, final long fromIdx) {
		return this.map.get(name);
	}
	
}
