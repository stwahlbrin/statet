/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public abstract class AbstractIntegerStore extends AbstractRStore<Integer>
		implements RIntegerStore {
	
	
	protected static final byte toRaw(final int integer) {
		if ((integer & 0xffffff00) == 0) {
			return (byte) (integer & 0xff);
		}
		throw new NumberFormatException(Integer.toString(integer));
	}
	
	
	@Override
	public final byte getStoreType() {
		return RStore.INTEGER;
	}
	
	@Override
	public final String getBaseVectorRClassName() {
		return RObject.CLASSNAME_INTEGER;
	}
	
	
	@Override
	public final boolean getLogi(final int idx) {
		return (getInt(idx) != 0);
	}
	
	@Override
	public final boolean getLogi(final long idx) {
		return (getInt(idx) != 0);
	}
	
	@Override
	public final void setLogi(final int idx, final boolean logi) {
		setInt(idx, logi ? 1 : 0);
	}
	
	@Override
	public final void setLogi(final long idx, final boolean logi) {
		setInt(idx, logi ? 1 : 0);
	}
	
	@Override
	public final double getNum(final int idx) {
		return getInt(idx);
	}
	
	@Override
	public final double getNum(final long idx) {
		return getInt(idx);
	}
	
	@Override
	public final void setNum(final int idx, final double real) {
		setInt(idx, (int)real);
	}
	
	@Override
	public final void setNum(final long idx, final double real) {
		setInt(idx, (int)real);
	}
	
	@Override
	public final String getChar(final int idx) {
		return Integer.toString(getInt(idx));
	}
	
	@Override
	public final String getChar(final long idx) {
		return Integer.toString(getInt(idx));
	}
	
	@Override
	public byte getRaw(final int idx) {
		return toRaw(getInt(idx));
	}
	
	@Override
	public byte getRaw(final long idx) {
		return toRaw(getInt(idx));
	}
	
	
	@Override
	public abstract @Nullable Integer [] toArray();
	
	
	@Override
	public long indexOf(final String character, final long fromIdx) {
		try {
			return indexOf(Integer.parseInt(character), fromIdx);
		}
		catch (final NumberFormatException e) {
			return -1;
		}
	}
	
	
	@Override
	public boolean allEqual(final RStore<?> other) {
		final long length= getLength();
		if (INTEGER != other.getStoreType() || length != other.getLength()) {
			return false;
		}
		if (length < 0) {
			return true;
		}
		else if (length <= Integer.MAX_VALUE) {
			final int ilength= (int)length;
			for (int idx= 0; idx < ilength; idx++) {
				if (!(isNA(idx) ? other.isNA(idx) :
						getInt(idx) == other.getInt(idx) )) {
					return false;
				}
			}
		}
		else {
			for (long idx= 0; idx < length; idx++) {
				if (!(isNA(idx) ? other.isNA(idx) :
						getInt(idx) == other.getInt(idx) )) {
					return false;
				}
			}
		}
		return true;
	}
	
}
