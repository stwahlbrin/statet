/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RRawStore;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public abstract class AbstractRawStore extends AbstractRStore<Byte>
		implements RRawStore {
	
	
	private static final char[] HEX_CHARS= new char[] {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
	};
	
	protected static final String toChar(final byte raw) {
		return new String(new char[] { HEX_CHARS[(raw >>> 4) & 0x0F], HEX_CHARS[(raw & 0x0F)] });
	}
	
	
	@Override
	public final byte getStoreType() {
		return RStore.RAW;
	}
	
	@Override
	public final String getBaseVectorRClassName() {
		return RObject.CLASSNAME_RAW;
	}
	
	
	@Override
	public boolean getLogi(final int idx) {
		return (getRaw(idx) != 0);
	}
	
	@Override
	public boolean getLogi(final long idx) {
		return (getRaw(idx) != 0);
	}
	
	@Override
	public int getInt(final int idx) {
		return (getRaw(idx) & 0xff);
	}
	
	@Override
	public int getInt(final long idx) {
		return (getRaw(idx) & 0xff);
	}
	
	@Override
	public void setInt(final long idx, final int integer) {
		setRaw(idx, AbstractIntegerStore.toRaw(integer));
	}
	
	@Override
	public void setInt(final int idx, final int integer) {
		setRaw(idx, AbstractIntegerStore.toRaw(integer));
	}
	
	@Override
	public final String getChar(final int idx) {
		return toChar(getRaw(idx));
	}
	
	@Override
	public final String getChar(final long idx) {
		return toChar(getRaw(idx));
	}
	
	
	@Override
	public abstract @NonNull Byte [] toArray();
	
	
	@Override
	public long indexOfNA(final long fromIdx) {
		return -1;
	}
	
}
