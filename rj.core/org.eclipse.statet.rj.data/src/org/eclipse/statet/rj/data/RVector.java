/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * An R object is of the type {@link RObject#TYPE_VECTOR vector}, if it is an R
 * data object of an "atomic" mode without a dimension attribute (<code>dim</code>).
 * Such an R vector object is represented by an instance of this interface.
 * <p>
 * The real data is stored in a {@link RStore} accessible by {@link #getData()}.</p>
 * <p>
 * The type {@link RObject#TYPE_VECTOR vector} equates not the R command
 * <code>is.vector(object)</code>, mainly because all attributes except the dimension
 * are allowed (in the R function only the names attribute is allowed).  Especially
 * an R factor object is of the type {@link RObject#TYPE_VECTOR vector}, implements
 * RVector and has a data store of the type {@link RStore#FACTOR}.  Also another S3
 * object based on a such a data object is of the type {@link RObject#TYPE_VECTOR vector}.
 * Whereas a S4 object is never directly of this type even the object simulates such
 * a data object in R. Such an object is of the type {@link RObject#TYPE_S4OBJECT S4 object},
 * and implements {@link RS4Object} with an object of the type {@link RObject#TYPE_VECTOR vector}
 * as data slot.</p>
 * <p>
 * The complementary type for objects with dimension attribute is the type
 * {@link RObject#TYPE_ARRAY array} and the interface {@link RArray}.
 * The data structure for multiple R objects is represented by the type
 * {@link RObject#TYPE_LIST list} respectively the interface {@link RList}.</p>
 * 
 * @param <TData> the type of the data store
 */
@NonNullByDefault
public interface RVector<TData extends RStore<?>> extends RObject {
	
	
	/**
	 * Returns the length of the object. The length of an {@link RObject#TYPE_VECTOR vector}
	 * is the count of all data values.
	 * 
	 * @return the length
	 */
	@Override
	long getLength();
	
	/**
	 * Returns the names for the indexes of the vector. This corresponds to the values of the
	 * R attribute (<code>names</code>) respectively the R function <code>names(object)</code>.
	 * 
	 * The returned character data has the same length the vector. If the R element does not have
	 * names, the names are invalid, or names are disabled, the method returns <code>null</code>.
	 * 
	 * @return a data store with the names of the indexes or <code>null</code>
	 * 
	 * @since de.walware.rj.data 0.5
	 */
	@Nullable RStore<?> getNames();
	
	
	@Override
	TData getData();
	
//	void insert(int idx);
//	void remove(int idx);
	
}
