/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.services.FQRObject;
import org.eclipse.statet.rj.services.RService;


/**
 * @since 1.2
 */
@NonNullByDefault
public interface RToolService extends ToolService, RService {
	
	
	@Override
	RTool getTool();
	
	
	@Override
	@Nullable FQRObject<? extends RTool> findData(final String symbol, final @Nullable RObject env, final boolean inherits,
			final @Nullable String factoryId, final int options, final int depth,
			final ProgressMonitor m) throws StatusException;
	
}
