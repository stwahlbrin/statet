/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.runtime;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.BasicRLibPaths;
import org.eclipse.statet.rj.renv.core.RLibGroup;
import org.eclipse.statet.rj.renv.core.RLibLocation;


@NonNullByDefault
public class RuntimeRLibPaths extends BasicRLibPaths {
	
	
	private final ImList<? extends RLibLocationInfo> libLocationInfos;
	
	
	public RuntimeRLibPaths(final ImList<? extends RLibGroup> libGroups,
			final ImList<? extends RLibLocation> libLocations,
			final ImList<? extends RLibLocationInfo> libLocationInfos) {
		super(libGroups, libLocations);
		this.libLocationInfos= nonNullAssert(libLocationInfos);
	}
	
	public RuntimeRLibPaths(final List<? extends RLibGroup> libGroups,
			final List<? extends RLibLocationInfo> libLocationInfos) {
		super(libGroups);
		this.libLocationInfos= ImCollections.toList(libLocationInfos);
	}
	
	
	public @Nullable RLibLocationInfo getInfo(final RLibLocation libLocation) {
		for (final RLibLocationInfo info : this.libLocationInfos) {
			if (info.getLibLocation().equals(libLocation)) {
				return info;
			}
		}
		return null;
	}
	
	
}
