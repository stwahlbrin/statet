/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class REnvUtils {
	
	
	public static String encode(final @Nullable REnv rEnv) {
		if (rEnv != null) {
			final String name= rEnv.getName();
			if (name != null && !rEnv.getId().startsWith("default-") /*rEnv.resolve() == rEnv*/) { //$NON-NLS-1$
				return rEnv.getId() + ';' + name;
			}
			else {
				return rEnv.getId() + ';';
			}
		}
		return ""; //$NON-NLS-1$
	}
	
	public static @Nullable REnv decode(final @Nullable String encodedSetting, final REnvManager manager) {
		if (encodedSetting != null) {
			final int idx= encodedSetting.indexOf(';');
			final REnv rEnv;
			if (idx >= 0) {
				rEnv= manager.get(encodedSetting.substring(0, idx), encodedSetting.substring(idx + 1));
			}
			else {
				rEnv= manager.get(encodedSetting, null);
			}
			if (rEnv != null) {
				return rEnv;
			}
		}
		return null;
	}
	
	
	public static @Nullable String standardizePathString(final @Nullable String pathString) {
		if (pathString == null || pathString.isEmpty()) {
			return null;
		}
		// replace all separators by single '/', remove separator at end
		int i= 0;
		int sep= 0;
		CHECK: for (; i < pathString.length();) {
			final char c= pathString.charAt(i++);
			switch (c) {
			case '/':
				if (++sep > 1) {
					break CHECK;
				}
				break;
			case '\\':
				sep++;
				break CHECK;
			default:
				sep= 0;
				break;
			}
		}
		if (sep == 0) {
			return pathString;
		}
		final StringBuilder sb= new StringBuilder(pathString.length());
		sb.append(pathString, 0, i - sep);
		while (i < pathString.length()) {
			final char c= pathString.charAt(i++);
			switch (c) {
			case '/':
			case '\\':
				sep++;
				break;
			default:
				if (sep != 0) {
					sb.append('/');
					sep= 0;
				}
				sb.append(c);
				break;
			}
		}
		if (sb.length() == 0) {
			return null;
		}
		return sb.toString();
	}
	
	
	private REnvUtils() {
	}
	
}
