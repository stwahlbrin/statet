/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Raster image.
 */
@NonNullByDefault
public class RRaster extends RGraphicElement {
	
	/**
	 * The image data to draw (RGBA).
	 */
	public final byte[] imgData;
	/**
	 * The dimension of the {@link #imgData image}.
	 */
	public final int imgWidth, imgHeight;
	/**
	 * The position for the bottom-left corner where to draw the image.
	 */
	public final double x, y;
	/**
	 * The dimension where to draw the image.
	 */
	public final double width, height;
	/**
	 * The degree to rotate anti-clockwise the image.
	 */
	public final double rotateDegree;
	
	public final boolean interpolate;
	
	
	/**
	 * Creates a new raster image element.
	 * 
	 * @param imgData {@link #imgData}
	 * @param imgWidth {@link #imgWidth}
	 * @param imgHeight {@link #imgHeight}
	 * @param x {@link #x}
	 * @param y {@link #y}
	 * @param w {@link #width}
	 * @param h {@link #height}
	 * @param rDeg {@link #rotateDegree}
	 * @param interpolate {@link #interpolate}
	 */
	public RRaster(final byte[] imgData, final int imgWidth, final int imgHeight,
			final double x, final double y, final double w, final double h,
			final double rDeg, final boolean interpolate) {
		this.imgData= imgData;
		this.imgWidth= imgWidth;
		this.imgHeight= imgHeight;
		this.x= x;
		this.y= y;
		this.width= w;
		this.height= h;
		this.rotateDegree= rDeg;
		this.interpolate= interpolate;
	}
	
	
	@Override
	public byte getInstructionType() {
		return DRAW_RASTER;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(50);
		sb.append("Raster[(");
		sb.append(this.x);
		sb.append(",");
		sb.append(this.y);
		sb.append("), (");
		sb.append(this.width);
		sb.append(" x ");
		sb.append(this.height);
		sb.append(", ");
		sb.append(this.rotateDegree);
		sb.append(", \"");
		sb.append(this.interpolate);
		sb.append("\"]");
		return sb.toString();
	}
	

}
