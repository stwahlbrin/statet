/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util.dataaccess;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RDataSubset {
	
	
	private final long rowBeginIdx;
	private final long rowCount;
	private final long columnBeginIdx;
	private final long columnCount;
	
	
	public RDataSubset(final long rowBeginIdx, final long rowCount,
			final long columnBeginIdx, final long columnCount) {
		this.rowBeginIdx= rowBeginIdx;
		this.rowCount= rowCount;
		this.columnBeginIdx= columnBeginIdx;
		this.columnCount= columnCount;
	}
	
	
	/**
	 * Returns the row begin index of the subset.
	 * 
	 * @return the begin index (zero-based)
	 */
	public final long getRowBeginIdx() {
		return this.rowBeginIdx;
	}
	
	/**
	 * Returns the row end index (exclusive) of the subset.
	 * 
	 * @return the end index (zero-based)
	 */
	public final long getRowEndIdx() {
		return this.rowBeginIdx + this.rowCount;
	}
	
	/**
	 * Returns the row count of the subset.
	 * 
	 * @return the count
	 */
	public final long getRowCount() {
		return this.rowCount;
	}
	
	public long toLocalRowIdx(final long rowIdx) {
		final long idx;
		if (rowIdx < this.rowBeginIdx
				|| (idx= rowIdx - this.rowBeginIdx) >= this.rowCount) {
			throw new IndexOutOfBoundsException(Long.toString(rowIdx));
		}
		return idx;
	}
	
	/**
	 * Returns the column begin index of the subset.
	 * 
	 * @return the index (zero-based)
	 */
	public final long getColumnBeginIdx() {
		return this.columnBeginIdx;
	}
	
	/**
	 * Returns the column end index (exclusive) of the subset.
	 * 
	 * @return the index
	 */
	public final long getColumnEndIdx() {
		return this.columnBeginIdx + this.columnCount;
	}
	
	/**
	 * Returns the column count of the subset.
	 * 
	 * @return the count
	 */
	public final long getColumnCount() {
		return this.columnCount;
	}
	
	public long toLocalColumnIdx(final long columnIdx) {
		final long idx;
		if (columnIdx < this.columnBeginIdx
				|| (idx= columnIdx - this.columnBeginIdx) >= this.columnCount) {
			throw new IndexOutOfBoundsException(Long.toString(columnIdx));
		}
		return idx;
	}
	
	
	public long getLength() {
		return (this.rowCount * this.columnCount);
	}
	
	
}
