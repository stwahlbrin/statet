/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util.dataaccess;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.services.FQRObjectRef;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RService;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore.Fragment;


/**
 * Data adapter for {@link RArray} objects of any dimension, loaded as one-dimensional vectors.
 * 
 * @since 2.0 (provisional)
 */
public class RArrayAsVectorDataAdapter extends AbstractRDataAdapter<RArray<?>, RVector<?>> {
	
	
	@Override
	public RArray<?> validate(final RObject rObject) throws UnexpectedRDataException {
		return RDataUtils.checkRArray(rObject);
	}
	
	@Override
	public RArray<?> validate(final RObject rObject, final RArray<?> referenceObject,
			final int flags) throws UnexpectedRDataException {
		final RArray<?> array= RDataUtils.checkRArray(rObject, referenceObject.getDim().getLength());
		// check dim ?
		if ((flags & ROW_COUNT) != 0) {
			RDataUtils.checkLengthEqual(array, referenceObject.getLength());
		}
		if ((flags & STORE_TYPE) != 0) {
			RDataUtils.checkData(array.getData(), referenceObject.getData().getStoreType());
		}
		return array;
	}
	
	@Override
	public long getRowCount(final RArray<?> rObject) {
		return rObject.getLength();
	}
	
	@Override
	public long getColumnCount(final RArray<?> rObject) {
		return 1;
	}
	
	
	@Override
	protected String getLoadDataFName() {
		return API_R_PREFIX + ".getDataVectorValues"; //$NON-NLS-1$
	}
	
	@Override
	protected String getSetDataFName() {
		return API_R_PREFIX + ".setDataVectorValues"; //$NON-NLS-1$
	}
	
	@Override
	protected RVector<?> validateData(final RObject rObject, final RArray<?> referenceObject,
			final Fragment<RVector<?>> fragment) throws UnexpectedRDataException {
		final RVector<?> vector= RDataUtils.checkRVector(rObject);
		RDataUtils.checkLengthEqual(vector, fragment.getRowCount());
		
		RDataUtils.checkData(rObject.getData(), referenceObject.getData().getStoreType());
		
		return vector;
	}
	
	@Override
	protected String getLoadRowNamesFName() {
		throw new UnsupportedOperationException();
	}
	
	public RVector<?> loadDimNames(final FQRObjectRef ref, final RArray<?> referenceObject,
			final LazyRStore.Fragment<RVector<?>> fragment,
			final RService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final RObject fragmentObject;
		{	final FunctionCall fcall= r.createFunctionCall(API_R_PREFIX + ".getDataArrayDimNames"); //$NON-NLS-1$
			addXRef(fcall, ref);
			fcall.add("idxs", DefaultRObjectFactory.INSTANCE.createNumVector(new double[] { //$NON-NLS-1$
					fragment.getRowBeginIdx() + 1,
					fragment.getRowEndIdx(),
			}));
			
			fragmentObject= fcall.evalData(m);
		}
		
		return validateRowNames(fragmentObject, referenceObject, fragment);
	}
	
	public RVector<?> loadDimItemNames(final FQRObjectRef ref, final RArray<?> referenceObject,
			final int dim, final LazyRStore.Fragment<RVector<?>> fragment,
			final RService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final RObject fragmentObject;
		{	final FunctionCall fcall= r.createFunctionCall(API_R_PREFIX + ".getDataArrayDimItemNames"); //$NON-NLS-1$
			addXRef(fcall, ref);
			fcall.addInt("dimIdx", dim + 1); //$NON-NLS-1$
			fcall.add("idxs", DefaultRObjectFactory.INSTANCE.createNumVector(new double[] { //$NON-NLS-1$
					fragment.getRowBeginIdx() + 1,
					fragment.getRowEndIdx(),
		}));
		
		fragmentObject= fcall.evalData(m);
		}
		
		return validateRowNames(fragmentObject, referenceObject, fragment);
	}
	
}
