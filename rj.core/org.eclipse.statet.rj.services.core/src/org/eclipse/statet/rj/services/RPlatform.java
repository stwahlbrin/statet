/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Information about the platform R is running on
 * and the running R version.
 * 
 * The properties usually doesn't change for a single RService
 * instance.
 */
@NonNullByDefault
public final class RPlatform implements Externalizable {
	
	/**
	 * OS type constant for windows operation systems
	 */
	public static final String OS_WINDOWS= "windows";
	
	/**
	 * OS type constant for unix operation systems
	 */
	public static final String OS_UNIX= "unix";
	
	
	private String osType;
	
	private String fileSep;
	private String pathSep;
	
	private String versionString;
	private @Nullable RVersion version;
	
	private String osName;
	private String osArch;
	private String osVersion;
	
	
	public RPlatform(final String osType, final String fileSep, final String pathSep,
			final String version,
			final String osName, final String osArch, final String osVersion) {
		this.osType= osType;
		this.fileSep= fileSep;
		this.pathSep= pathSep;
		this.versionString= version;
		
		this.osName= osName;
		this.osArch= osArch;
		this.osVersion= osVersion;
	}
	
	public RPlatform() {
	}
	
	@Override
	public void readExternal(final ObjectInput in) throws IOException, ClassNotFoundException {
		this.osType= in.readUTF();
		this.fileSep= in.readUTF();
		this.pathSep= in.readUTF();
		this.versionString= in.readUTF();
		
		this.osName= in.readUTF();
		this.osArch= in.readUTF();
		this.osVersion= in.readUTF();
	}
	
	@Override
	public void writeExternal(final ObjectOutput out) throws IOException {
		out.writeUTF(this.osType);
		out.writeUTF(this.fileSep);
		out.writeUTF(this.pathSep);
		out.writeUTF(this.versionString);
		
		out.writeUTF(this.osName);
		out.writeUTF(this.osArch);
		out.writeUTF(this.osVersion);
	}
	
	
	/**
	 * The OS type as defined in R <code>.Platform$OS.type</code>
	 * 
	 * @see #OS_WINDOWS
	 * @see #OS_UNIX
	 * 
	 * @return the os type constant
	 */
	public String getOsType() {
		return this.osType;
	}
	
	public String getFileSep() {
		return this.fileSep;
	}
	
	public String getPathSep() {
		return this.pathSep;
	}
	
	public RVersion getRVersion() {
		RVersion version= this.version;
		if (version == null) {
			version= new RVersion(this.versionString);
			this.version= version;
		}
		return version;
	}
	
	
	/**
	 * The OS name as defined by the Java property <code>os.name</code>
	 * 
	 * @return the OS name string
	 */
	public String getOSName() {
		return this.osName;
	}
	
	/**
	 * The OS architecture as defined by the Java property <code>os.arch</code>
	 * 
	 * @return the OS architecture string
	 */
	public String getOSArch() {
		return this.osArch;
	}
	
	/**
	 * The OS version as defined by the Java property <code>os.version</code>
	 * 
	 * @return the OS version string
	 */
	public String getOSVersion() {
		return this.osVersion;
	}
	
}
