/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import static org.eclipse.statet.rj.data.impl.AbstractRStore.DEFAULT_LONG_DATA_SEGMENT_LENGTH;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RRawStore;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public abstract class RRawStoreTest extends AbstractRStoreTest {
	
	
	static class RawCaseData extends CaseData<RRawStore> {
		
		final static Function<RawCaseData, RRawStore> REF_CONSTRUCTOR= new RRaw32StoreTest()::createStore;
		
		final byte[] values;
		
		
		public RawCaseData(final String label, final byte[] values) {
			super(label, values.length);
			assert (values.length == this.length);
			this.values= values;
			
			setReference(REF_CONSTRUCTOR.apply(this));
		}
		
	}
	
	protected static final List<RawCaseData> DEFAULT_DATA_SOURCES;
	static {
		final var datas= new ArrayList<RawCaseData>();
		
		datas.add(new RawCaseData("empty", new byte[0]));
		
		datas.add(new RawCaseData("single", new byte[] { 33 }));
		datas.add(new RawCaseData("single-Min", new byte[] { RRawStore.MIN_BYTE }));
		datas.add(new RawCaseData("single-Max", new byte[] { RRawStore.MAX_BYTE }));
		
		{	final byte[] values= new byte[0x100];
			for (int i= 0; i < values.length; i++) {
				values[i]= (byte)(i & 0xFF);
			}
			datas.add(new RawCaseData("seq", values));
		}
		
		{	final Random rand= new Random(16857);
			final byte[] values= new byte[100000];
			for (int i= 0; i < values.length; i++) {
				values[i]= (byte)(rand.nextInt(0xFF + 1) & 0xFF);
			}
			datas.add(new RawCaseData("rand100000", values));
		}
		if (isBigDataEnabled(1)) {
			final Random rand= new Random(46);
			final byte[] values= new byte[DEFAULT_LONG_DATA_SEGMENT_LENGTH * 2 + 13];
			for (int i= 0; i < values.length; i++) {
				values[i]= (byte)(rand.nextInt(0xFF + 1) & 0xFF);
			}
			datas.add(new RawCaseData("randMultiSeg", values));
		}
		
		DEFAULT_DATA_SOURCES= datas;
	}
	
	
	public RRawStoreTest() {
	}
	
	
	public static List<RawCaseData> provideCaseDatas() {
		return new ArrayList<>(DEFAULT_DATA_SOURCES);
	}
	
	protected abstract RRawStore createStore(final RawCaseData data);
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getStoreType(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		assertEquals(RStore.RAW, store.getStoreType());
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getBaseVectorRClassName(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		assertEquals(RObject.CLASSNAME_RAW, store.getBaseVectorRClassName());
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void length(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		checkLength(data, store);
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void isNA(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		checkIsNA(data, store);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void isMissing(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			assertFalse(store.isMissing(i));
		}
		assertIndexOutOfBounds(data, store::isMissing, store::isMissing);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	@SuppressWarnings("boxing")
	public void getLogi(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final boolean expected= (data.values[i] != 0);
			assertEquals(expected, store.getLogi(i));
			assertEquals(expected, store.getLogi((long)i));
		}
		assertIndexOutOfBounds(data, store::getLogi, store::getLogi);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getInt(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final int expected= Byte.toUnsignedInt(data.values[i]);
			assertEquals(expected, store.getInt(i));
			assertEquals(expected, store.getInt((long)i));
		}
		assertIndexOutOfBounds(data, store::getInt, store::getInt);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getNum(final RawCaseData data) {
//		final RRawStore store= createStore(data);
//		
//		for (int i= 0; i < data.length; i++) {
//			final double expected= Byte.toUnsignedInt(data.values[i]);
//			assertEquals(expected, store.getNum(i));
//			assertEquals(expected, store.getNum((long)i));
//		}
//		assertIndexOutOfBounds(data, store::getNum, store::getNum);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getChar(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			String expected= Integer.toHexString(Byte.toUnsignedInt(data.values[i]));
			if (expected.length() == 1) {
				expected= "0" + expected;
			}
			assertEquals(expected, store.getChar(i));
			assertEquals(expected, store.getChar((long)i));
		}
		assertIndexOutOfBounds(data, store::getChar, store::getChar);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getRaw(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final byte expected= data.values[i];
			assertEquals(expected, store.getRaw(i));
			assertEquals(expected, store.getRaw((long)i));
		}
		assertIndexOutOfBounds(data, store::getRaw, store::getRaw);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void toRawArray(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		final byte[] array= store.toRawArray();
		
		assertEquals(data.values.length, array.length);
		
		for (int i= 0; i < data.length; i++) {
			assertEquals(data.values[i], array[i], arrayDiffersAt(i));
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void get(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final byte expected= data.values[i];
			assertEquals(expected, store.get(i));
			assertEquals(expected, store.get((long)i));
		}
		assertIndexOutOfBounds(data, store::get, store::get);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void toArray(final RawCaseData data) {
		final RRawStore store= createStore(data);
		
		final Byte[] array= store.toArray();
		
		assertEquals(data.values.length, array.length);
		
		for (int i= 0; i < data.length; i++) {
			assertEquals(data.values[i], array[i], arrayDiffersAt(i));
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void writeExternal(final RawCaseData data) throws IOException {
		final RRawStore store= createStore(data);
		
		if (store instanceof ExternalizableRStore) {
			final byte[] ser= writeExternal(store);
			assertArrayEquals(data.getExternalBytes(), ser);
		}
	}
	
}
