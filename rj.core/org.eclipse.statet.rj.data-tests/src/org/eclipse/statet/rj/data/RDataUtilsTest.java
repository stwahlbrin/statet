/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;
import org.eclipse.statet.rj.data.impl.RInteger32Store;
import org.eclipse.statet.rj.data.impl.RLogicalByte32Store;
import org.eclipse.statet.rj.data.impl.RNullImpl;
import org.eclipse.statet.rj.data.impl.RNumericB32Store;


@NonNullByDefault
public class RDataUtilsTest {
	
	
	private static final DefaultRObjectFactory FACTORY= DefaultRObjectFactory.INSTANCE;
	
	
	public RDataUtilsTest() {
	}
	
	
	@Test
	public void isSingleLogi() {
		assertTrue(RDataUtils.isSingleLogi(FACTORY.createLogiVector(1)));
		assertTrue(RDataUtils.isSingleLogi(FACTORY.createLogiVector(new boolean[] { true })));
		
		assertTrue(RDataUtils.isSingleLogi(FACTORY.createVector(
				new RLogicalByte32Store(new boolean[1], new boolean[] { true }) )));
		
		assertFalse(RDataUtils.isSingleLogi(null));
		assertFalse(RDataUtils.isSingleLogi(RNullImpl.INSTANCE));
		assertFalse(RDataUtils.isSingleLogi(FACTORY.createCharVector(1)));
		assertFalse(RDataUtils.isSingleLogi(FACTORY.createLogiVector(0)));
		assertFalse(RDataUtils.isSingleLogi(FACTORY.createLogiVector(2)));
	}
	
	@Test
	public void checkSingleLogi() {
		try {
			assertEquals(Boolean.FALSE,
					RDataUtils.checkSingleLogi(FACTORY.createLogiVector(1)));
			assertEquals(Boolean.TRUE,
					RDataUtils.checkSingleLogi(FACTORY.createLogiVector(new boolean[] { true })));
			
			assertNull(
					RDataUtils.checkSingleLogi(FACTORY.createVector(
							new RLogicalByte32Store(new boolean[1], new boolean[] { true }) )));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogi(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogi(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogi(FACTORY.createCharVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogi(FACTORY.createLogiVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogi(FACTORY.createLogiVector(2)));
	}
	
	@Test
	@SuppressWarnings("boxing")
	public void checkSingleLogiValue() {
		try {
			assertEquals(false,
					RDataUtils.checkSingleLogiValue(FACTORY.createLogiVector(1)));
			assertEquals(true,
					RDataUtils.checkSingleLogiValue(FACTORY.createLogiVector(new boolean[] { true })));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogiValue(FACTORY.createVector(
						new RLogicalByte32Store(new boolean[1], new boolean[] { true }) )));
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogiValue(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogiValue(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogiValue(FACTORY.createCharVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogiValue(FACTORY.createLogiVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleLogiValue(FACTORY.createLogiVector(2)));
	}
	
	@Test
	public void isSingleInt() {
		assertTrue(RDataUtils.isSingleInt(FACTORY.createIntVector(1)));
		assertTrue(RDataUtils.isSingleInt(FACTORY.createIntVector(new int[] { 1 })));
		
		assertTrue(RDataUtils.isSingleInt(FACTORY.createVector(
				new RInteger32Store(new int[1], new boolean[] { true }) )));
		
		assertFalse(RDataUtils.isSingleInt(null));
		assertFalse(RDataUtils.isSingleInt(RNullImpl.INSTANCE));
		assertFalse(RDataUtils.isSingleInt(FACTORY.createLogiVector(1)));
		assertFalse(RDataUtils.isSingleInt(FACTORY.createIntVector(0)));
		assertFalse(RDataUtils.isSingleInt(FACTORY.createIntVector(2)));
	}
	
	@Test
	public void checkSingleInt() {
		try {
			assertEquals(Integer.valueOf(0),
					RDataUtils.checkSingleInt(FACTORY.createIntVector(1)));
			assertEquals(Integer.valueOf(1),
					RDataUtils.checkSingleInt(FACTORY.createIntVector(new int[] { 1 })));
			
			assertNull(
					RDataUtils.checkSingleInt(FACTORY.createVector(
							new RInteger32Store(new int[1], new boolean[] { true }) )));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleInt(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleInt(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleInt(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleInt(FACTORY.createIntVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleInt(FACTORY.createIntVector(2)));
	}
	
	@Test
	public void checkSingleIntValue() {
		try {
			assertEquals(0,
					RDataUtils.checkSingleIntValue(FACTORY.createIntVector(1)));
			assertEquals(1,
					RDataUtils.checkSingleIntValue(FACTORY.createIntVector(new int[] { 1 })));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleIntValue(FACTORY.createVector(
						new RInteger32Store(new int[1], new boolean[] { true }) )));
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleIntValue(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleIntValue(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleIntValue(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleIntValue(FACTORY.createIntVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleIntValue(FACTORY.createIntVector(2)));
	}
	
	@Test
	public void isSingleNum() {
		assertTrue(RDataUtils.isSingleNum(FACTORY.createNumVector(1)));
		assertTrue(RDataUtils.isSingleNum(FACTORY.createNumVector(new double[] { 1.0 })));
		
		assertTrue(RDataUtils.isSingleNum(FACTORY.createVector(
				new RNumericB32Store(new double[1], new boolean[] { true }) )));
		
		assertFalse(RDataUtils.isSingleNum(null));
		assertFalse(RDataUtils.isSingleNum(RNullImpl.INSTANCE));
		assertFalse(RDataUtils.isSingleNum(FACTORY.createLogiVector(1)));
		assertFalse(RDataUtils.isSingleNum(FACTORY.createNumVector(0)));
		assertFalse(RDataUtils.isSingleNum(FACTORY.createNumVector(2)));
	}
	
	@Test
	public void checkSingleNum() {
		try {
			assertEquals(Double.valueOf(0.0),
					RDataUtils.checkSingleNum(FACTORY.createNumVector(1)));
			assertEquals(Double.valueOf(1.0),
					RDataUtils.checkSingleNum(FACTORY.createNumVector(new double[] { 1.0 })));
			
			assertNull(
					RDataUtils.checkSingleNum(FACTORY.createVector(
							new RNumericB32Store(new double[1], new boolean[] { true }) )));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNum(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNum(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNum(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNum(FACTORY.createNumVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNum(FACTORY.createNumVector(2)));
	}
	
	@Test
	public void checkSingleNumValue() {
		try {
			assertEquals(0.0,
					RDataUtils.checkSingleNumValue(FACTORY.createNumVector(1)));
			assertEquals(1.0,
					RDataUtils.checkSingleNumValue(FACTORY.createNumVector(new double[] { 1.0 })));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNumValue(FACTORY.createVector(
						new RNumericB32Store(new double[1], new boolean[] { true }) )));
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNumValue(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNumValue(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNumValue(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNumValue(FACTORY.createNumVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleNumValue(FACTORY.createNumVector(2)));
	}
	
	@Test
	public void isSingleChar() {
		assertTrue(RDataUtils.isSingleChar(FACTORY.createCharVector(1)));
		assertTrue(RDataUtils.isSingleChar(FACTORY.createCharVector(new @Nullable String[] { "test" })));
		
		assertTrue(RDataUtils.isSingleChar(FACTORY.createVector(
				new RCharacter32Store(new @Nullable String[] { null }) )));
		
		assertFalse(RDataUtils.isSingleChar(null));
		assertFalse(RDataUtils.isSingleChar(RNullImpl.INSTANCE));
		assertFalse(RDataUtils.isSingleChar(FACTORY.createLogiVector(1)));
		assertFalse(RDataUtils.isSingleChar(FACTORY.createCharVector(0)));
		assertFalse(RDataUtils.isSingleChar(FACTORY.createCharVector(2)));
	}
	
	@Test
	public void checkSingleChar() {
		try {
			assertEquals("",
					RDataUtils.checkSingleChar(FACTORY.createCharVector(1)));
			assertEquals("test",
					RDataUtils.checkSingleChar(FACTORY.createCharVector(new @Nullable String[] { "test" })));
			
			assertNull(
					RDataUtils.checkSingleChar(FACTORY.createVector(
							new RCharacter32Store(new @Nullable String[] { null }) )));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleChar(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleChar(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleChar(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleChar(FACTORY.createCharVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleChar(FACTORY.createCharVector(2)));
	}
	
	@Test
	public void checkSingleCharValue() {
		try {
			assertEquals("",
					RDataUtils.checkSingleCharValue(FACTORY.createCharVector(1)));
			assertEquals("test",
					RDataUtils.checkSingleCharValue(FACTORY.createCharVector(new @Nullable String[] { "test" })));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleCharValue(FACTORY.createVector(
						new RCharacter32Store(new @Nullable String[] { null }) )));
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleCharValue(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleCharValue(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleCharValue(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleCharValue(FACTORY.createCharVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleCharValue(FACTORY.createCharVector(2)));
	}
	
	@Test
	public void isSingleRaw() {
		assertTrue(RDataUtils.isSingleRaw(FACTORY.createRawVector(1)));
		assertTrue(RDataUtils.isSingleRaw(FACTORY.createRawVector(new byte[] { 1 })));
		
		assertFalse(RDataUtils.isSingleRaw(null));
		assertFalse(RDataUtils.isSingleRaw(RNullImpl.INSTANCE));
		assertFalse(RDataUtils.isSingleRaw(FACTORY.createLogiVector(1)));
		assertFalse(RDataUtils.isSingleRaw(FACTORY.createRawVector(0)));
		assertFalse(RDataUtils.isSingleRaw(FACTORY.createRawVector(2)));
	}
	
	@Test
	public void checkSingleRaw() {
		try {
			assertEquals(Byte.valueOf((byte)0),
					RDataUtils.checkSingleRaw(FACTORY.createRawVector(1)));
			assertEquals(Byte.valueOf((byte)1),
					RDataUtils.checkSingleRaw(FACTORY.createRawVector(new byte[] { 1 })));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRaw(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRaw(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRaw(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRaw(FACTORY.createRawVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRaw(FACTORY.createRawVector(2)));
	}
	
	@Test
	public void checkSingleRawValue() {
		try {
			assertEquals(Byte.valueOf((byte)0),
					RDataUtils.checkSingleRawValue(FACTORY.createRawVector(1)));
			assertEquals(Byte.valueOf((byte)1),
					RDataUtils.checkSingleRawValue(FACTORY.createRawVector(new byte[] { 1 })));
		}
		catch (final UnexpectedRDataException e) {
			fail(e);
		}
		
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRawValue(null));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRawValue(RNullImpl.INSTANCE));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRawValue(FACTORY.createLogiVector(1)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRawValue(FACTORY.createRawVector(0)));
		assertThrows(UnexpectedRDataException.class, () ->
				RDataUtils.checkSingleRawValue(FACTORY.createRawVector(2)));
	}
	
}
