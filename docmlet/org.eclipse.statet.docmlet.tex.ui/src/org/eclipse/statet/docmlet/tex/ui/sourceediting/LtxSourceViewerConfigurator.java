/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.docmlet.tex.core.TexCodeStyleSettings;
import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentSetupParticipant;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;


/**
 * Configurator for LaTeX code source viewers.
 */
@NonNullByDefault
public class LtxSourceViewerConfigurator extends SourceEditorViewerConfigurator
		implements TexCoreAccess, PropertyChangeListener {
	
	
	private static final Set<String> RESET_GROUP_IDS= ImCollections.newSet(
			TexCodeStyleSettings.INDENT_GROUP_ID );
//			TaskTagsPreferences.GROUP_ID ));
	
	
	private TexCoreAccess sourceCoreAccess= nonNullLateInit();
	
	private final TexCodeStyleSettings texCodeStyleCopy;
	
	
	public LtxSourceViewerConfigurator(final @Nullable TexCoreAccess coreAccess,
			final LtxSourceViewerConfiguration config) {
		super(config);
		this.texCodeStyleCopy= new TexCodeStyleSettings(1);
		config.setCoreAccess(this);
		setSource(coreAccess);
		
		this.texCodeStyleCopy.load(this.sourceCoreAccess.getTexCodeStyle());
		this.texCodeStyleCopy.resetDirty();
		this.texCodeStyleCopy.addPropertyChangeListener(this);
	}
	
	
	@Override
	public IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new LtxDocumentSetupParticipant();
	}
	
	@Override
	protected Set<String> getResetGroupIds() {
		return RESET_GROUP_IDS;
	}
	
	
	public void setSource(@Nullable TexCoreAccess newAccess) {
		if (newAccess == null) {
			newAccess= TexCore.getWorkbenchAccess();
		}
		if (this.sourceCoreAccess != newAccess) {
			this.sourceCoreAccess= newAccess;
			handleSettingsChanged(null, null);
		}
	}
	
	
	@Override
	public void handleSettingsChanged(final @Nullable Set<String> groupIds,
			final @Nullable Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		
		this.texCodeStyleCopy.resetDirty();
	}
	
	@Override
	protected void checkSettingsChanges(final Set<String> groupIds, final Map<String, Object> options) {
		super.checkSettingsChanges(groupIds, options);
		
		if (groupIds.contains(TexCodeStyleSettings.INDENT_GROUP_ID)) {
			this.texCodeStyleCopy.load(this.sourceCoreAccess.getTexCodeStyle());
		}
		if (groupIds.contains(TexEditingSettings.EDITING_PREF_QUALIFIER)) {
			this.updateCompleteConfig= true;
		}
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.sourceCoreAccess.getPrefs();
	}
	
	@Override
	public TexCommandSet getTexCommandSet() {
		return this.sourceCoreAccess.getTexCommandSet();
	}
	
	@Override
	public TexCodeStyleSettings getTexCodeStyle() {
		return this.texCodeStyleCopy;
	}
	
}
