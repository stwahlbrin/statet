/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui.actions;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.tex.core.ast.NodeType;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.actions.AbstractOpenDeclarationHandler;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;


@NonNullByDefault
public class LtxOpenDeclarationHandler extends AbstractOpenDeclarationHandler {
	
	
//	private static final class TexOpenDeclaration extends OpenDeclaration {
//		
//		@Override
//		public ILabelProvider createLabelProvider() {
//			return new TexLabelProvider();
//		}
//	}
	
	
	public static @Nullable TexNameAccess searchAccess(final SourceEditor editor,
			final TextRegion region) {
		final SourceUnit sourceUnit= editor.getSourceUnit();
		if (sourceUnit instanceof TexSourceUnit) {
			final LtxSourceUnitModelInfo info= (LtxSourceUnitModelInfo) sourceUnit.getModelInfo(
					TexModel.LTX_TYPE_ID, ModelManager.MODEL_FILE, new NullProgressMonitor() );
			if (info != null) {
				final AstInfo astInfo= info.getAst();
				final AstSelection selection= AstSelection.search(astInfo.getRoot(),
						region.getStartOffset(), region.getEndOffset(),
						AstSelection.MODE_COVERING_SAME_LAST );
				final AstNode covering= selection.getCovering();
				if (covering instanceof TexAstNode) {
					final TexAstNode node= (TexAstNode) covering;
					if (node.getNodeType() == NodeType.LABEL) {
						TexAstNode current= node;
						do {
							for (final Object attachment : current.getAttachments()) {
								if (attachment instanceof TexNameAccess) {
									final TexNameAccess access= (TexNameAccess) attachment;
									if (access.getNameNode() == node) {
										return access;
									}
								}
							}
							current= current.getTexParent();
						} while (current != null);
					}
				}
			}
		}
		return null;
	}
	
	
	@Override
	public boolean execute(final SourceEditor editor, final TextRegion selection) {
		final TexNameAccess access= searchAccess(editor, selection);
		if (access != null) {
			final OpenDeclaration open= new OpenDeclaration();
			final TexNameAccess declAccess= open.selectAccess(access.getAllInUnit());
			if (declAccess != null) {
				open.open(editor, declAccess);
				return true;
			}
		}
		return false;
	}
	
}
