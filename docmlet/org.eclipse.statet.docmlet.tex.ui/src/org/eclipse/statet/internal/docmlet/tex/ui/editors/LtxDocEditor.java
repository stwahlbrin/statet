/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collection;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentContentInfo;
import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.docmlet.tex.ui.TexUI;
import org.eclipse.statet.docmlet.tex.ui.editors.LtxDefaultFoldingProvider;
import org.eclipse.statet.docmlet.tex.ui.editors.LtxEditor;
import org.eclipse.statet.docmlet.tex.ui.editors.TexMarkOccurrencesLocator;
import org.eclipse.statet.docmlet.tex.ui.sourceediting.LtxSourceViewerConfiguration;
import org.eclipse.statet.docmlet.tex.ui.sourceediting.LtxSourceViewerConfigurator;
import org.eclipse.statet.docmlet.tex.ui.sourceediting.TexEditingSettings;
import org.eclipse.statet.internal.docmlet.tex.ui.TexUIPlugin;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.ui.sourceediting.AbstractMarkOccurrencesProvider;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1OutlinePage;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorAddon;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.folding.FoldingEditorAddon;


@NonNullByDefault
public class LtxDocEditor extends SourceEditor1 implements LtxEditor {
	
	
	private static final ImList<String> KEY_CONTEXTS= ImCollections.newIdentityList(
			TexUI.EDITOR_CONTEXT_ID );
	
	private static final ImList<String> CONTEXT_IDS= ImCollections.concatList(
			ACTION_SET_CONTEXT_IDS, KEY_CONTEXTS );
	
	
	private static class ThisMarkOccurrencesProvider extends AbstractMarkOccurrencesProvider {
		
		
		private final TexMarkOccurrencesLocator locator= new TexMarkOccurrencesLocator();
		
		
		public ThisMarkOccurrencesProvider(final SourceEditor1 editor) {
			super(editor, TexDocumentConstants.LTX_DEFAULT_OR_MATH_CONTENT_CONSTRAINT);
		}
		
		@Override
		protected void doUpdate(final RunData run, final SourceUnitModelInfo info,
				final AstSelection astSelection, final @Nullable ITextSelection orgSelection)
				throws BadLocationException, BadPartitioningException, UnsupportedOperationException {
			this.locator.run(run, info, astSelection, orgSelection);
		}
		
	}
	
	
	private LtxSourceViewerConfigurator texConfig;
	
	
	public LtxDocEditor() {
		super(TexCore.LTX_CONTENT_TYPE);
		this.texConfig= nonNullAssert(this.texConfig);
	}
	
	@Override
	protected void initializeEditor() {
		super.initializeEditor();
		
		setEditorContextMenuId("org.eclipse.statet.docmlet.menus.LtxEditorContextMenu"); //$NON-NLS-1$
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfiguration() {
		setDocumentProvider(TexUIPlugin.getInstance().getTexDocumentProvider());
		
		enableStructuralFeatures(TexModel.getLtxModelManager(),
				TexEditingSettings.FOLDING_ENABLED_PREF,
				TexEditingSettings.MARKOCCURRENCES_ENABLED_PREF );
		
		this.texConfig= new LtxSourceViewerConfigurator(null,
				new LtxSourceViewerConfiguration(LtxDocumentContentInfo.INSTANCE, 0,
						this, null, null, null ));
		return this.texConfig;
	}
	
	
	@Override
	protected Collection<String> getContextIds() {
		return CONTEXT_IDS;
	}
	
	@Override
	protected SourceEditorAddon createCodeFoldingProvider() {
		return new FoldingEditorAddon(new LtxDefaultFoldingProvider());
	}
	
	@Override
	protected SourceEditorAddon createMarkOccurrencesProvider() {
		return new ThisMarkOccurrencesProvider(this);
	}
	
	
	@Override
	public @Nullable TexSourceUnit getSourceUnit() {
		return (TexSourceUnit)super.getSourceUnit();
	}
	
	@Override
	protected void setupConfiguration(final @Nullable IEditorInput newInput) {
		super.setupConfiguration(newInput);
		
		final var sourceUnit= getSourceUnit();
		this.texConfig.setSource(TexCore.getContextAccess(sourceUnit));
	}
	
	
	@Override
	protected void handlePreferenceStoreChanged(final org.eclipse.jface.util.PropertyChangeEvent event) {
		if (AbstractDecoratedTextEditorPreferenceConstants.EDITOR_TAB_WIDTH.equals(event.getProperty())
				|| AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS.equals(event.getProperty())) {
			return;
		}
		super.handlePreferenceStoreChanged(event);
	}
	
	
	@Override
	protected boolean isTabsToSpacesConversionEnabled() {
		return false;
	}
	
	
	@Override
	protected void collectContextMenuPreferencePages(final List<String> pageIds) {
		super.collectContextMenuPreferencePages(pageIds);
		pageIds.add(TexUI.EDITOR_PREF_PAGE_ID);
		pageIds.add("org.eclipse.statet.docmlet.preferencePages.LtxTextStyles"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.docmlet.preferencePages.LtxEditorTemplates"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.docmlet.preferencePages.TexCodeStyle"); //$NON-NLS-1$
	}
	
	@Override
	protected SourceEditor1OutlinePage createOutlinePage() {
		return new LtxOutlinePage(this);
	}
	
	@Override
	public @NonNull String[] getShowInTargetIds() {
		return new @NonNull String[] {
				IPageLayout.ID_PROJECT_EXPLORER,
				IPageLayout.ID_OUTLINE };
	}
	
}
