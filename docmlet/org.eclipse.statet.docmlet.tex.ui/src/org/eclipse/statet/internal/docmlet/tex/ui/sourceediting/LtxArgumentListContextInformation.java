/*=============================================================================#
 # Copyright (c) 2019, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.sourceediting;

import org.eclipse.jface.text.contentassist.IContextInformationExtension;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.IntArrayList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInformationProposal;


@NonNullByDefault
public class LtxArgumentListContextInformation implements AssistInformationProposal,
		IContextInformationExtension {
	
	
	private final int offset;
	
	private final TexCommand command;
	
	private final String information;
	private final ImIntList informationParameterIndexes;
	
	
	public LtxArgumentListContextInformation(final int offset, final TexCommand command) {
		this.offset= offset;
		this.command= command;
		
		{	// build information string
			final StringBuilder sb= new StringBuilder();
			final IntList idxs= new IntArrayList();
			appendArgumentList(sb, idxs, this.command);
			this.information= sb.toString();
			this.informationParameterIndexes= ImCollections.toIntList(idxs);
		}
	}
	
	
	public int getCallArgsOffset() {
		return this.offset;
	}
	
	public TexCommand getCommand() {
		return this.command;
	}
	
	
	@Override
	public String getContextDisplayString() {
		return getInformationDisplayString();
	}
	
	@Override
	public @Nullable Image getImage() {
		return null;
	}
	
	@Override
	public int getContextInformationPosition() {
		return Math.max(this.offset, 0);
	}
	
	@Override
	public String getInformationDisplayString() {
		return this.information;
	}
	
	/**
	 * Returns the indexes of the command parameters in the information display string.
	 * 
	 * @return list with the indexes
	 */
	public ImIntList getInformationDisplayStringParameterIndexes() {
		return this.informationParameterIndexes;
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		// prevent stacking of context information at the same position
		return true;
	}
	
	
	private static void appendArgumentList(final StringBuilder text, final IntList idxs, final TexCommand command) {
		for (final var param : command.getParameters()) {
			idxs.add(text.length());
			if ((param.getType() & Parameter.OPTIONAL) != 0) {
				text.append('[');
				if (param.getLabel() != null) {
					text.append(param.getLabel());
				}
				text.append(']');
			}
			else {
				text.append('{');
				if (param.getLabel() != null) {
					text.append(param.getLabel());
				}
				text.append('}');
			}
		}
	}
	
}
