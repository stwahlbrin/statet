/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.sourceediting;

import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.ltk.ui.sourceediting.QuickInformationProvider;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class LtxQuickOutlineInformationProvider extends QuickInformationProvider {
	
	
//	private @Nullable LtxHeuristicTokenScanner scanner;
	
	
	public LtxQuickOutlineInformationProvider(final SourceEditor editor, final int textOperation) {
		super(editor, TexModel.LTX_TYPE_ID, textOperation);
	}
	
	
//	@Override
//	public IRegion getSubject(final ITextViewer textViewer, final int offset) {
//		if (this.scanner == null) {
//			this.scanner= LtxHeuristicTokenScanner.create(getEditor().getDocumentContentInfo());
//		}
//		try {
//			final IDocument document= getEditor().getViewer().getDocument();
//			this.scanner.configure(document);
//			final IRegion word= this.scanner.findCommonWord(offset);
//			if (word != null) {
//				return word;
//			}
//		}
//		catch (final Exception e) {
//		}
//		return super.getSubject(textViewer, offset);
//	}
	
	@Override
	public IInformationControlCreator createInformationPresenterControlCreator() {
		return new IInformationControlCreator() {
			@Override
			public IInformationControl createInformationControl(final Shell parent) {
				return new LtxQuickOutlineInformationControl(parent, getCommandId());
			}
		};
	}
	
}
