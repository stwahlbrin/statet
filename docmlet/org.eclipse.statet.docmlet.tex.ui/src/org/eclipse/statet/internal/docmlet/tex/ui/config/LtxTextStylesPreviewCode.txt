\documentclass{article}
\title{Syntax Highlighting}

\begin{document}
\maketitle

\section{Examples}
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna
aliqua.  This is an inline \verb+{verbatim text}+ and
this is a formula $\overline{x}= \frac{1}{n} \sum{x_i}$.
% TODO add description
The analysis uses a confidence level of 95 \%. Ut enim ad
minim veniam, quis nostrud exercitation ullamco laboris
nisi ut aliquip ex ea commodo consequat.

\subsection{Environments}
The model in R:
\begin{verbatim*}
    m1 <- y ~ x1 + x2 + x1:x2
\end{verbatim*}
Duis aute irure dolor in reprehenderit in voluptate velit
esse cillum dolore eu fugiat nulla pariatur. Excepteur
sint occaecat cupidatat non proident, sunt in culpa qui
officia deserunt mollit anim id est laborum.

\end{document}
