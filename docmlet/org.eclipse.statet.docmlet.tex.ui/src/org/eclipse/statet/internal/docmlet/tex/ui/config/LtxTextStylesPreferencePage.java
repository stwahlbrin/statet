/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.config;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentContentInfo;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentSetupParticipant;
import org.eclipse.statet.docmlet.tex.ui.sourceediting.LtxSourceViewerConfiguration;
import org.eclipse.statet.docmlet.tex.ui.text.TexTextStyles;
import org.eclipse.statet.internal.docmlet.tex.ui.TexUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.AbstractTextStylesConfigurationBlock;
import org.eclipse.statet.ltk.ui.util.CombinedPreferenceStore;


@NonNullByDefault
public class LtxTextStylesPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public LtxTextStylesPreferencePage() {
		setPreferenceStore(TexUIPlugin.getInstance().getPreferenceStore());
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() throws CoreException {
		return new TexTextStylesConfigurationBlock();
	}
	
}


@NonNullByDefault
class TexTextStylesConfigurationBlock extends AbstractTextStylesConfigurationBlock {
	
	
	public TexTextStylesConfigurationBlock() {
	}
	
	
	@Override
	protected String getSettingsGroup() {
		return TexTextStyles.LTX_TEXTSTYLE_CONFIG_QUALIFIER;
	}
	
	@Override
	protected ImList<CategoryNode> createItems() {
		return ImCollections.newList(
			new CategoryNode(Messages.TextStyles_DefaultCodeCategory_label,
				new StyleNode(Messages.TextStyles_Default_label, Messages.TextStyles_Default_description,
						TexTextStyles.TS_DEFAULT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.TextStyles_ControlWord_label, Messages.TextStyles_ControlWord_description,
						TexTextStyles.TS_CONTROL_WORD, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ),
					new StyleNode(Messages.TextStyles_ControlWord_Sectioning_label, Messages.TextStyles_ControlWord_Sectioning_description,
							TexTextStyles.TS_CONTROL_WORD_SUB_SECTIONING, ImCollections.newList(
								SyntaxNode.createUseNoExtraStyle(TexTextStyles.TS_CONTROL_WORD),
								SyntaxNode.createUseCustomStyle() ))),
				new StyleNode(Messages.TextStyles_ControlChar_label, Messages.TextStyles_ControlChar_description,
						TexTextStyles.TS_CONTROL_CHAR, ImCollections.newList(
							SyntaxNode.createUseCustomStyle(),
							SyntaxNode.createUseOtherStyle(TexTextStyles.TS_CONTROL_WORD, Messages.TextStyles_ControlWord_label) )),
				new StyleNode(Messages.TextStyles_CurlyBracket_label, Messages.TextStyles_CurlyBracket_description,
						TexTextStyles.TS_CURLY_BRACKETS, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.TextStyles_Verbatim_label, Messages.TextStyles_Verbatim_description,
						TexTextStyles.TS_VERBATIM, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ))
			),
			new CategoryNode(Messages.TextStyles_MathCodeCategory_label,
				new StyleNode(Messages.TextStyles_MathDefault_label, Messages.TextStyles_MathDefault_description,
						TexTextStyles.TS_MATH, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.TextStyles_ControlWord_label, Messages.TextStyles_ControlWord_description,
						TexTextStyles.TS_MATH_CONTROL_WORD, ImCollections.newList(
							SyntaxNode.createUseCustomStyle(),
							SyntaxNode.createUseOtherStyle(TexTextStyles.TS_MATH, Messages.TextStyles_MathCodeCategory_short, Messages.TextStyles_Default_label),
							SyntaxNode.createUseOtherStyle(TexTextStyles.TS_CONTROL_WORD, Messages.TextStyles_DefaultCodeCategory_short, Messages.TextStyles_ControlWord_label) )),
				new StyleNode(Messages.TextStyles_ControlChar_label, Messages.TextStyles_ControlChar_description,
						TexTextStyles.TS_MATH_CONTROL_CHAR, ImCollections.newList(
							SyntaxNode.createUseCustomStyle(),
							SyntaxNode.createUseOtherStyle(TexTextStyles.TS_MATH_CONTROL_WORD, Messages.TextStyles_MathCodeCategory_short, Messages.TextStyles_ControlWord_label),
							SyntaxNode.createUseOtherStyle(TexTextStyles.TS_CONTROL_CHAR, Messages.TextStyles_DefaultCodeCategory_short, Messages.TextStyles_ControlChar_label) )),
				new StyleNode(Messages.TextStyles_CurlyBracket_label, Messages.TextStyles_CurlyBracket_description,
						TexTextStyles.TS_MATH_CURLY_BRACKETS, ImCollections.newList(
							SyntaxNode.createUseCustomStyle(),
							SyntaxNode.createUseOtherStyle(TexTextStyles.TS_MATH, Messages.TextStyles_MathCodeCategory_short, Messages.TextStyles_Default_label),
							SyntaxNode.createUseOtherStyle(TexTextStyles.TS_CURLY_BRACKETS, Messages.TextStyles_DefaultCodeCategory_short, Messages.TextStyles_CurlyBracket_label) ))
			),
			new CategoryNode(Messages.TextStyles_CommentCategory_label,
				new StyleNode(Messages.TextStyles_Comment_label, Messages.TextStyles_Comment_description,
						TexTextStyles.TS_COMMENT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.TextStyles_TaskTag_label, Messages.TextStyles_TaskTag_description,
						TexTextStyles.TS_TASK_TAG, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ))
			)
		);
	}
	
	@Override
	protected String getPreviewFileName() {
		return "LtxTextStylesPreviewCode.txt"; //$NON-NLS-1$
	}
	
	@Override
	protected IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new LtxDocumentSetupParticipant();
	}
	
	@Override
	protected SourceEditorViewerConfiguration getSourceEditorViewerConfiguration(
			final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		return new LtxSourceViewerConfiguration(LtxDocumentContentInfo.INSTANCE, 0,
				null,
				TexCore.getDefaultsAccess(),
				CombinedPreferenceStore.createStore(
						preferenceStore,
						EditorsUI.getPreferenceStore() ),
				textStyles );
	}
	
}
