<?xml version='1.0' encoding='UTF-8'?>
<schema targetNamespace="org.eclipse.statet.docmlet" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema id="TexDocTemplates"
               plugin="org.eclipse.statet.docmlet.tex.ui"
               name="TeX Document Templates"/>
      </appinfo>
      <documentation>
         This extension-point allows to define document templates for TeX documents.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <choice minOccurs="0" maxOccurs="unbounded">
            <element ref="category"/>
         </choice>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="category">
      <annotation>
         <documentation>
            This point allows to provide new template category.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  The type id of the template category.
               </documentation>
               <appinfo>
                  <meta.attribute kind="identifier"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="image" type="string" use="required">
            <annotation>
               <documentation>
                  URL to the image of the template category.
               </documentation>
               <appinfo>
                  <meta.attribute kind="resource"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="label" type="string" use="required">
            <annotation>
               <documentation>
                  Label of the template category.
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="itemImage" type="string" use="required">
            <annotation>
               <documentation>
                  URL to the image of the category items.
               </documentation>
               <appinfo>
                  <meta.attribute kind="resource"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="configurationClass" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.statet.ltk.ui.templates.config.ITemplateCategoryConfiguration"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         2.0
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="copyright"/>
      </appinfo>
      <documentation>
         Copyright (c) 2014, 2022 Stephan Wahlbrink and others.

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
which is available at https://www.apache.org/licenses/LICENSE-2.0.

SPDX-License-Identifier: EPL-2.0 OR Apache-2.0

Contributors:
    Stephan Wahlbrink &lt;sw@wahlbrink.eu&gt; - initial API and implementation
      </documentation>
   </annotation>

</schema>
