/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.base.ui.processing;

import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;

import org.eclipse.statet.docmlet.base.ui.processing.actions.RunDocProcessingOnSaveExtension;
import org.eclipse.statet.docmlet.base.ui.sourceediting.DocEditor;


@NonNullByDefault
public class ToggleRunOnSaveScopeHandler extends AbstractScopeHandler {
	
	
	private @Nullable Boolean currentChecked;
	
	
	public ToggleRunOnSaveScopeHandler(final Object scope, final @Nullable String commandId) {
		super(scope, commandId);
	}
	
	
	private IWorkbenchWindow getWindow() {
		return (IWorkbenchWindow)getScope();
	}
	
	private @Nullable RunDocProcessingOnSaveExtension getSaveExtension() {
		final IWorkbenchWindow window= getWindow();
		final IEditorPart editor= window.getActivePage().getActiveEditor();
		if (editor instanceof DocEditor) {
			return editor.getAdapter(RunDocProcessingOnSaveExtension.class);
		}
		return null;
	}
	
	
	private boolean isChecked(final @Nullable RunDocProcessingOnSaveExtension saveExtension) {
		return (saveExtension != null && saveExtension.isAutoRunEnabled());
	}
	
	@Override
	public void setEnabled(final IEvaluationContext context) {
		final RunDocProcessingOnSaveExtension saveExtension= getSaveExtension();
		
		setBaseEnabled(saveExtension != null);
		if (this.currentChecked == null || this.currentChecked != isChecked(saveExtension)) {
			refreshElements();
		}
	}
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		final RunDocProcessingOnSaveExtension saveExtension= getSaveExtension();
		
		element.setChecked(this.currentChecked= isChecked(saveExtension));
	}
	
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event, final IEvaluationContext context)
			throws ExecutionException {
		final RunDocProcessingOnSaveExtension saveExtension= getSaveExtension();
		if (saveExtension != null) {
			saveExtension.setAutoRunEnabled(!saveExtension.isAutoRunEnabled());
		}
		return null;
	}
	
}
