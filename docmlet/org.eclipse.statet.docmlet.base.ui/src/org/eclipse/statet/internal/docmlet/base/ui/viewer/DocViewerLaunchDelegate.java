/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.base.ui.viewer;

import java.io.IOException;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.variables.IStringVariable;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;

import org.eclipse.statet.jcommons.runtime.ProcessUtils;

import org.eclipse.statet.ecommons.debug.core.util.LaunchUtils;
import org.eclipse.statet.ecommons.io.win.DDE;
import org.eclipse.statet.ecommons.io.win.DDEClient;
import org.eclipse.statet.ecommons.ui.workbench.workspace.ResourceVariableUtil;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.viewer.DocViewerConfig;
import org.eclipse.statet.internal.docmlet.base.ui.DocmlBaseUIPlugin;
import org.eclipse.statet.internal.docmlet.base.ui.viewer.DocViewerLaunchConfig.DDETask;


public class DocViewerLaunchDelegate extends LaunchConfigurationDelegate {
	
	
	public DocViewerLaunchDelegate() {
	}
	
	
	@Override
	public void launch(final ILaunchConfiguration configuration, final String mode,
			final ILaunch launch, final IProgressMonitor monitor) throws CoreException {
		launch(null, configuration, mode, launch, monitor);
	}
	
	public void launch(final ResourceVariableUtil sourceFileUtil,
			final Map<String, ? extends IStringVariable> extraVariables,
			final ILaunchConfiguration configuration, final String mode,
			final ILaunch launch, final IProgressMonitor monitor) throws CoreException {
		launch(new DocViewerLaunchConfig(sourceFileUtil, extraVariables), configuration,
				mode, launch, monitor );
	}
	
	private void launch(DocViewerLaunchConfig config, 
			final ILaunchConfiguration configuration, final String mode,
			final ILaunch launch, final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= LaunchUtils.initProgressMonitor(configuration, monitor,
				1 + 1 + 2 + 10 + 2 + 2);
		final long timestamp= System.currentTimeMillis();
		
		DocmlBaseUIPlugin.getInstance().getDocViewerCloseDelegate().cancelFocus();
		
		try {
			if (config == null) {
				config= new DocViewerLaunchConfig();
				config.initDocFile(configuration, m.newChild(1));
			}
			
			final DDETask ddeTask= config.loadDDETask(configuration,
					DocViewerConfig.TASK_VIEW_OUTPUT_ATTR_QUALIFIER,
					Messages.DDE_ViewOutput_label,
					m.newChild(1) );
			if (ddeTask != null) {
				try {
					ddeTask.exec();
					return;
				}
				catch (final CoreException e) {
					switch (e.getStatus().getCode()) {
					case DDEClient.CONNECT_FAILED:
						break;
					default:
						throw e;
					}
				}
				m.worked(2);
			}
			
			if (m.isCanceled()) {
				return;
			}
			if (ddeTask == null) {
				m.setWorkRemaining(10);
			}
			
			{	final ProcessBuilder processBuilder= config.initProgram(configuration);
				
				final Process runtimeProcess;
				try {
					runtimeProcess= processBuilder.start();
				}
				catch (final IOException e) {
					throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
							"An error occurred when launching document viewer.", e ));
				}
				
				final String processName= processBuilder.command().get(0) + ' ' + LaunchUtils.createProcessTimestamp(timestamp);
				
				final IProcess process= DebugPlugin.newProcess(launch, runtimeProcess, processName);
				
				process.setAttribute(IProcess.ATTR_CMDLINE, ProcessUtils.generateCommandLine(
						processBuilder.command() ));
			}
			
			if (ddeTask != null) {
				final int max= 4;
				for (int i= 1; DDE.isSupported() && i <= max; i++) {
					if (m.isCanceled()) {
						return;
					}
					
					try {
						Thread.sleep(500);
					}
					catch (final InterruptedException e) {
						Thread.interrupted();
					}
					try {
						ddeTask.exec();
						return;
					}
					catch (final CoreException e) {
						switch (e.getStatus().getCode()) {
						case DDEClient.CONNECT_FAILED:
							if (i < max) {
								continue;
							}
							//$FALL-THROUGH$
						default:
							throw e;
						}
					}
				}
			}
		}
		finally {
			m.done();
		}
	}
	
}
