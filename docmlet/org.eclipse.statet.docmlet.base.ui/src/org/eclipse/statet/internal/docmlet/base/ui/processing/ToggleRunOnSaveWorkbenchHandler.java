/*=============================================================================#

 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.base.ui.processing;

import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.ui.commands.IElementUpdater;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.actions.WorkbenchScopingHandler;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI;


/**
 * Handlers for document output creation toolchain running with the active configuration.
 */
@NonNullByDefault
public class ToggleRunOnSaveWorkbenchHandler extends WorkbenchScopingHandler
		implements IElementUpdater, IExecutableExtension {
	
	
	/** For instantiation via plugin.xml */
	public ToggleRunOnSaveWorkbenchHandler() {
		super(DocProcessingUI.TOGGLE_RUN_ON_SAVE_COMMAND_ID);
	}
	
	
	@Override
	protected ToggleRunOnSaveScopeHandler createScopeHandler(final Object scope) {
		return new ToggleRunOnSaveScopeHandler(scope, getCommandId());
	}
	
}
