/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.actions;

import java.util.List;
import java.util.regex.Matcher;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.menus.IWorkbenchContribution;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;
import org.eclipse.statet.ecommons.debug.ui.config.actions.RunConfigsMenuContribution;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig.Format;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI;
import org.eclipse.statet.internal.docmlet.base.ui.processing.DocActionUtil;


@NonNullByDefault
public class RunDocConfigsMenuContribution extends RunConfigsMenuContribution<IFile>
		implements IWorkbenchContribution, IExecutableExtension {
	
	
	protected class DocConfigContribution extends ConfigContribution {
		
		
		public DocConfigContribution(final String label, final Image icon,
				final ILaunchConfiguration configuration) {
			super(label, icon, configuration);
		}
		
		
		@Override
		protected void fillMenu(final Menu menu) {
			addLaunchItems(menu);
			
			new MenuItem(menu, SWT.SEPARATOR);
			addActivateItem(menu, DocProcessingUI.ACTIONS_ACTIVATE_CONFIG_HELP_CONTEXT_ID);
			addEditItem(menu, DocProcessingUI.ACTIONS_EDIT_CONFIG_HELP_CONTEXT_ID);
		}
		
		protected void addLaunchItems(final Menu menu) {
		}
		
		
		protected @Nullable String createDetail(final @Nullable String inputExt,
				final @Nullable String outputExt) {
			if (inputExt == null || outputExt == null) {
				return null;
			}
			final StringBuilder sb= getStringBuilder();
			sb.append("\u2002["); //$NON-NLS-1$
			sb.append(inputExt);
			sb.append("\u2002\u2192\u2002"); //$NON-NLS-1$
			sb.append(outputExt);
			sb.append("]"); //$NON-NLS-1$
			
			return sb.toString();
		}
		
	}
	
	
	private @Nullable Matcher validExtMatcher;
	
	
	/** For instantiation via plugin.xml */
	public RunDocConfigsMenuContribution() {
		super(new DocActionUtil(ActionUtil.ACTIVE_MENU_SELECTION_MODE));
	}
	
	
	@Override
	protected DocActionUtil getUtil() {
		return (DocActionUtil) super.getUtil();
	}
	
	
	@Override
	protected List<ImIdentitySet<String>> getContextShortcutLaunchFlags() {
		return ImCollections.newList(
				DocProcessingUI.CommonFlags.PROCESS_AND_PREVIEW,
				DocProcessingUI.CommonFlags.PROCESS );
	}
	
	@Override
	protected ConfigContribution createConfigContribution(final StringBuilder label,
			final Image icon,
			final ILaunchConfiguration configuration) {
		return new DocConfigContribution(label.toString(), icon, configuration);
	}
	
	
	@SuppressWarnings("null")
	private Matcher getValidExtMatcher(final String ext) {
		if (this.validExtMatcher == null) {
			this.validExtMatcher= DocProcessingConfig.VALID_EXT_PATTERN.matcher(ext);
		}
		else {
			this.validExtMatcher.reset(ext);
		}
		return this.validExtMatcher;
	}
	
	protected @Nullable String resolveFormatExt(final @Nullable Format format, @Nullable String inputExt) {
		if (format == null) {
			return null;
		}
		if (inputExt != null && (inputExt.isEmpty() || !getValidExtMatcher(inputExt).matches())) {
			inputExt= null;
		}
		return format.getExt(inputExt);
	}
	
}
