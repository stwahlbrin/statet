/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.viewer;

import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.workspace.ResourceVariableUtil;

import org.eclipse.statet.internal.docmlet.base.ui.DocmlBaseUIPlugin;
import org.eclipse.statet.internal.docmlet.base.ui.viewer.DocViewerCloseDelegate;
import org.eclipse.statet.internal.docmlet.base.ui.viewer.DocViewerLaunchConfig;


@NonNullByDefault
public class DocViewerUI {
	
	
/*[ DocViewerLaunchConfig Attributes ]=========================================*/
	
	public static final String DOC_PATH_ATTR_NAME= DocViewerUI.BASE_RUN_ATTR_QUALIFIER + '/' + "Target.path"; //$NON-NLS-1$
	
	public static final String BASE_RUN_ATTR_QUALIFIER= "org.eclipse.statet.docmlet.base/run"; //$NON-NLS-1$
	
	
/*[ Actions ]==================================================================*/
	
	public static void runPreProduceOutputTask(final ResourceVariableUtil outputFileUtil,
			final @Nullable Map<String, ? extends IStringVariable> extraVariables,
			final SubMonitor m) throws CoreException {
		if (!DocViewerCloseDelegate.isAvailable()) {
			return;
		}
		
		final DocViewerLaunchConfig config= new DocViewerLaunchConfig(outputFileUtil, extraVariables);
		final DocViewerCloseDelegate delegate= DocmlBaseUIPlugin.getInstance().getDocViewerCloseDelegate();
		delegate.run(config, m);
	}
	
	
}
