/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.resources.core.variables.ResourceVariables;

import org.eclipse.statet.docmlet.base.ui.processing.operations.RunExternalProgramOperation.ExternalProgramLaunchConfig;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class RunExternalProgramOperationSettings extends AbstractLaunchConfigOperationSettings {
	
	
	public RunExternalProgramOperationSettings() {
		super(ExternalProgramLaunchConfig.TYPE_ID);
	}
	
	
	@Override
	public String getId() {
		return RunExternalProgramOperation.ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_RunExternalProgram_label;
	}
	
	
	@Override
	protected void initializeNewLaunchConfig(final ILaunchConfigurationWorkingCopy config) {
		config.setAttribute(ExternalProgramLaunchConfig.ARGUMENTS_ATTR_NAME,
				"${" + ResourceVariables.RESOURCE_LOC_VAR_NAME + "}" ); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
