/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;


@NonNullByDefault
public class DocProcessingToolOperationIterator implements Comparable<DocProcessingToolOperationIterator> {
	
	
	public static final byte PRE= 1;
	public static final byte MAIN= 2;
	public static final byte POST= 3;
	
	
	private final ImList<StepConfig> steps;
	
	private int stepIdx;
	private @Nullable StepConfig stepConfig;
	private byte stepPart;
	private @Nullable List<DocProcessingOperation> stepPartList;
	private int stepPartIdx;
	
	private @Nullable DocProcessingOperation operation;
	
	
	public DocProcessingToolOperationIterator(final ImList<StepConfig> steps) {
		this.steps= steps;
		reset();
	}
	
	
	@Override
	public int compareTo(final DocProcessingToolOperationIterator other) {
		int diff= this.stepIdx - other.stepIdx;
		if (diff == 0) {
			diff= this.stepPart - other.stepPart;
			if (diff == 0) {
				diff= this.stepPartIdx - other.stepPartIdx;
			}
		}
		return diff;
	}
	
	public void reset() {
		this.stepIdx= -1;
		this.stepConfig= null;
		this.stepPart= -1;
		this.stepPartList= null;
		this.stepPartIdx= -1;
		this.operation= null;
	}
	
	public void reset(final DocProcessingToolOperationIterator other) {
		this.stepIdx= other.stepIdx;
		this.stepConfig= other.stepConfig;
		this.stepPart= other.stepPart;
		this.stepPartList= other.stepPartList;
		this.stepPartIdx= other.stepPartIdx;
		this.operation= other.operation;
	}
	
	public boolean hasNext() {
		if (this.stepIdx < 0) {
			return nextStep();
		}
		return (this.operation != null);
	}
	
	public boolean next() {
		if (this.stepIdx < 0) {
			return nextStep();
		}
		if (this.stepIdx == Integer.MAX_VALUE) {
			return false;
		}
		
		this.operation= null;
		return (nextOperation() || nextPart() || nextStep());
	}
	
	
	private boolean nextStep() {
		while (++this.stepIdx < this.steps.size()) {
			final StepConfig stepConfig= this.steps.get(this.stepIdx);
			if (stepConfig.isRun()) {
				this.stepConfig= stepConfig;
				if (nextPart()) {
					return true;
				}
			}
		}
		this.stepIdx= Integer.MAX_VALUE;
		this.stepConfig= null;
		return false;
	}
	
	private boolean nextPart() {
		final StepConfig stepConfig= this.stepConfig;
		if (stepConfig == null) {
			return false;
		}
		while (true) {
			switch (++this.stepPart) {
			case 0:
				break;
			case PRE:
				this.stepPartList= stepConfig.getPre();
				if (nextOperation()) {
					return true;
				}
				break;
			case MAIN:
				this.operation= stepConfig.getOperation();
				if (this.operation != null) {
					return true;
				}
				break;
			case POST:
				this.stepPartList= stepConfig.getPost();
				if (nextOperation()) {
					return true;
				}
				break;
			default:
				this.stepPart= -1;
				return false;
			}
		}
	}
	
	private boolean nextOperation() {
		final List<DocProcessingOperation> stepPartList= this.stepPartList;
		if (stepPartList == null) {
			return false;
		}
		if (++this.stepPartIdx < stepPartList.size()) {
			this.operation= stepPartList.get(this.stepPartIdx);
			return true;
		}
		this.stepPartIdx= -1;
		this.stepPartList= null;
		return false;
	}
	
	public int getStepIdx() {
		return this.stepIdx;
	}
	
	public StepConfig getStepConfig() {
		return this.stepConfig;
	}
	
	public byte getStepPart() {
		return this.stepPart;
	}
	
	public DocProcessingOperation getOperation() {
		return this.operation;
	}
	
	
}
