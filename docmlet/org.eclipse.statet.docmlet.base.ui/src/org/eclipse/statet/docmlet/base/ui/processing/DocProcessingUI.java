/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.debug.core.ILaunchConfiguration;

import org.eclipse.statet.jcommons.collections.IdentitySet;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.base.ui.DocmlBaseUIPlugin;
import org.eclipse.statet.internal.docmlet.base.ui.processing.DocProcessingRegistry;


@NonNullByDefault
public class DocProcessingUI {
	
	
	public static final String PROCESS_DOC_DEFAULT_COMMAND_ID= "org.eclipse.statet.docmlet.commands.ProcessDocDefault"; //$NON-NLS-1$
	
	public static final String PREVIEW_DOC_DEFAULT_COMMAND_ID= "org.eclipse.statet.docmlet.commands.PreviewDocDefault"; //$NON-NLS-1$
	
	public static final String PROCESS_AND_PREVIEW_DOC_DEFAULT_COMMAND_ID= "org.eclipse.statet.docmlet.commands.ProcessAndPreviewDefault"; //$NON-NLS-1$
	
	
	public static final String WEAVE_STEP=                  "weave"; //$NON-NLS-1$
	
	public static final String PRODUCE_OUTPUT_STEP=         "produce_output"; //$NON-NLS-1$
	
	public static final String PREVIEW_OUTPUT_STEP=         "open_output"; //$NON-NLS-1$
	
	
	public static final String PROCESSING_STEPS_FLAG=       "processing_steps"; //$NON-NLS-1$
	
	
	public static class CommonFlags {
		
		public static final ImIdentitySet<String> PROCESS_AND_PREVIEW= ImCollections.newIdentitySet();
		public static final ImIdentitySet<String> PROCESS= ImCollections.newIdentitySet(PROCESSING_STEPS_FLAG);
		
		public static final ImIdentitySet<String> WEAVE= ImCollections.newIdentitySet(WEAVE_STEP);
		public static final ImIdentitySet<String> PRODUCE_OUTPUT= ImCollections.newIdentitySet(PRODUCE_OUTPUT_STEP);
		public static final ImIdentitySet<String> OPEN_OUTPUT= ImCollections.newIdentitySet(PREVIEW_OUTPUT_STEP);
		
	}
	
	
	public static final String TOGGLE_RUN_ON_SAVE_COMMAND_ID= "org.eclipse.statet.docmlet.commands.ToggleRunDocProcessingOnSave"; //$NON-NLS-1$
	
	
/*[ DocProcessingToolConfig Attributes ]=======================================*/
	
	public static final String BASE_RUN_ATTR_QUALIFIER= "org.eclipse.statet.docmlet.base/run"; //$NON-NLS-1$
	
	public static final String CONTENT_TYPE_ID_ATTR_NAME= BASE_RUN_ATTR_QUALIFIER + '/' + "ContentType.id"; //$NON-NLS-1$
	
	public static final String RUN_STEPS_ATTR_NAME= BASE_RUN_ATTR_QUALIFIER + '/' + "BuildSteps.set"; //$NON-NLS-1$
	
	@SuppressWarnings("null")
	public static IdentitySet<String> getRunSteps(final ILaunchConfiguration configuration)
			throws CoreException {
		return (IdentitySet<String>)configuration.getAttribute(RUN_STEPS_ATTR_NAME,
				ImCollections.emptyIdentitySet() );
	}
	
	public static final String SOURCE_PATH_ATTR_NAME= BASE_RUN_ATTR_QUALIFIER + '/' + "DocSource.path"; //$NON-NLS-1$
	
	
/*[ Managers ]=================================================================*/
	
	public static @Nullable DocProcessingManager getDocProcessingManager(final String contentTypeId) {
		final DocProcessingRegistry registry= DocmlBaseUIPlugin.getInstance().getDocProcessingRegistry();
		return registry.getDocProcessingManager(contentTypeId);
	}
	
	public static @Nullable DocProcessingManager getDocProcessingManager(final IContentType contentType,
			final boolean inherit) {
		final DocProcessingRegistry registry= DocmlBaseUIPlugin.getInstance().getDocProcessingRegistry();
		if (!inherit) {
			return registry.getDocProcessingManager(contentType.getId());
		}
		IContentType type= contentType;
		do {
			final DocProcessingManager manager= registry.getDocProcessingManager(type.getId());
			if (manager != null) {
				return manager;
			}
			type= type.getBaseType();
		}
		while (type != null);
		return null;
	}
	
	
/*[ Help Contexts ]============================================================*/
	
	public static final String ACTIONS_RUN_CONFIG_HELP_CONTEXT_ID= "org.eclipse.statet.docmlet.doc.user.DocProcessingActionsRunConfig"; //$NON-NLS-1$
	public static final String ACTIONS_RUN_CONFIG_PROCESS_HELP_CONTEXT_ID= "org.eclipse.statet.docmlet.doc.user.DocProcessingActionsRunConfigProcess"; //$NON-NLS-1$
	public static final String ACTIONS_RUN_CONFIG_STEP_HELP_CONTEXT_ID= "org.eclipse.statet.docmlet.doc.user.DocProcessingActionsRunConfigStep"; //$NON-NLS-1$
	public static final String ACTIONS_RUN_CONFIG_PREVIEW_HELP_CONTEXT_ID= "org.eclipse.statet.docmlet.doc.user.DocProcessingActionsRunConfigPreview"; //$NON-NLS-1$
	public static final String ACTIONS_ACTIVATE_CONFIG_HELP_CONTEXT_ID= "org.eclipse.statet.docmlet.doc.user.DocProcessingActionsActivateConfig"; //$NON-NLS-1$
	public static final String ACTIONS_EDIT_CONFIG_HELP_CONTEXT_ID= "org.eclipse.statet.docmlet.doc.user.DocProcessingActionsEditConfig"; //$NON-NLS-1$
	
}
