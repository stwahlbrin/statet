/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.progress.IProgressConstants;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class DocProcessingToolJob extends Job {
	
	
	private final DocProcessingToolProcess toolProcess;
	
	
	public DocProcessingToolJob(final DocProcessingToolProcess toolProcess) {
		super(toolProcess.getLabel());
		
		setUser(false);
		setPriority(Job.BUILD);
		setRule(new DocumentRule(toolProcess.getConfig().getSourceFile()));
		
		this.toolProcess= toolProcess;
		
		{	final Image image= toolProcess.getImage();
			if (image != null) {
				setProperty(IProgressConstants.ICON_PROPERTY, ImageDescriptor.createFromImage(image));
			}
		}
	}
	
	
	@Override
	protected IStatus run(final IProgressMonitor monitor) {
		final Thread thread= new Thread("DocProcessingWorker-" + this.toolProcess.getLabel()) { //$NON-NLS-1$
			@Override
			public void run() {
				DocProcessingToolJob.this.toolProcess.run(monitor);
			}
		};
		thread.setDaemon(true);
		thread.start();
		while (true) {
			try {
				thread.join();
				break;
			}
			catch (final InterruptedException e) {
			}
		}
		
		return this.toolProcess.getStatus();
	}
	
	@Override
	protected void canceling() {
		try {
			this.toolProcess.terminate();
		}
		catch (final DebugException e) {
		}
	}
	
}
