/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isNull;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.variables.IStringVariable;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.core.util.LaunchUtils;
import org.eclipse.statet.ecommons.variables.core.VariableText2;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingOperation;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolProcess;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public abstract class AbstractLaunchConfigOperation extends DocProcessingOperation {
	
	
	protected static final String LAUNCH_CONFIG_NAME_ATTR_KEY= "LaunchConfig.name"; //$NON-NLS-1$
	
	
	private final String launchConfigTypeId;
	
	private final String launchConfigNameAttrName;
	
	private ILaunchManager launchManager;
	private ILaunchConfigurationType launchConfigType;
	
	private ILaunchConfiguration launchConfig;
	
	
	public AbstractLaunchConfigOperation(final String launchConfigTypeId) {
		this.launchConfigTypeId= launchConfigTypeId;
		
		this.launchConfigNameAttrName= getId() + '/' + LAUNCH_CONFIG_NAME_ATTR_KEY;
	}
	
	
	@Override
	public void init(final StepConfig stepConfig, final Map<String, String> settings,
			final SubMonitor m) throws CoreException {
		super.init(stepConfig, settings, m);
		
		this.launchManager= DebugPlugin.getDefault().getLaunchManager();
		final ILaunchConfigurationType launchConfigType= this.launchManager.getLaunchConfigurationType(this.launchConfigTypeId);
		if (launchConfigType == null) {
			throw new RuntimeException("Launch configuration type is missing: id= " + this.launchConfigTypeId); //$NON-NLS-1$
		}
		this.launchConfigType= launchConfigType;
		
		final String name= settings.get(this.launchConfigNameAttrName);
		if (name == null || name.isEmpty()) {
			throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
					Messages.ProcessingOperation_RunLaunchConfig_Config_error_SpecMissing_message ));
		}
		
		final ILaunchConfiguration orgConfig= LaunchUtils.findLaunchConfiguration(
				this.launchManager.getLaunchConfigurations(), this.launchConfigType, name);
		
		if (orgConfig == null) {
			throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
					NLS.bind(Messages.ProcessingOperation_RunLaunchConfig_Config_error_DefMissing_message,
							name )));
		}
		
		this.launchConfig= preprocessConfig(orgConfig);
	}
	
	
	protected String getLaunchMode() {
		return ILaunchManager.RUN_MODE;
	}
	
	@Override
	public IStatus run(final DocProcessingToolProcess toolProcess,
			final SubMonitor m) throws CoreException {
		final ILaunchConfiguration config= this.launchConfig;
		if (isNull(config)) {
			throw new IllegalStateException("not initialized");
		}
		
		m.beginTask(getTaskLabel(config), 1 + 10);
		
		final String launchMode= getLaunchMode();
		final ILaunchConfigurationDelegate delegate= LaunchUtils.getLaunchConfigurationDelegate(
				config, launchMode, toolProcess.getStatus() );
		m.worked(1);
		
		launch(delegate, config, launchMode, toolProcess, m.newChild(10));
		
		return Status.OK_STATUS;
	}
	
	
	protected String getTaskLabel(final ILaunchConfiguration config) {
		final StepConfig stepConfig= getStepConfig();
		if (stepConfig.getId() == DocProcessingConfig.BASE_PREVIEW_ATTR_QUALIFIER) {
			return NLS.bind(Messages.ProcessingOperation_RunLaunchConfig_ForPreview_task,
					config.getName(), stepConfig.getInputFile().getName() );
		}
		return NLS.bind(Messages.ProcessingOperation_RunLaunchConfig_task,
				config.getName() );
	}
	
	
	protected VariableText2 createVariableResolver() {
		final Map<String, IStringVariable> variables= new HashMap<>();
		
		variables.putAll(getStepConfig().getVariableResolver().getExtraVariables());
		
		return new VariableText2(variables);
	}
	
	protected ILaunchConfiguration preprocessConfig(final ILaunchConfiguration config) throws CoreException {
		return config;
	}
	
	protected void launch(final ILaunchConfigurationDelegate delegate,
			final ILaunchConfiguration config, final String launchMode,
			final DocProcessingToolProcess toolProcess,
			final SubMonitor m) throws CoreException {
		delegate.launch(config, launchMode, toolProcess.getLaunch(), m);
	}
	
}
