/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.ISchedulingRule;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class DocumentRule implements ISchedulingRule {
	
	
	private final IFile sourceFile;
	
	
	public DocumentRule(final IFile sourceFile) {
		this.sourceFile= nonNullAssert(sourceFile);
	}
	
	
	private boolean equalRule(final DocumentRule other) {
		return (this.sourceFile.equals(other.sourceFile));
	}
	
	@Override
	public boolean contains(final ISchedulingRule rule) {
		return (rule instanceof DocumentRule && equalRule((DocumentRule) rule));
	}
	
	@Override
	public boolean isConflicting(final ISchedulingRule rule) {
		return (rule instanceof DocumentRule && equalRule((DocumentRule) rule));
	}
	
	
	@Override
	public int hashCode() {
		return this.sourceFile.hashCode() + 9543;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof DocumentRule && equalRule((DocumentRule) obj)) );
	}
	
}
