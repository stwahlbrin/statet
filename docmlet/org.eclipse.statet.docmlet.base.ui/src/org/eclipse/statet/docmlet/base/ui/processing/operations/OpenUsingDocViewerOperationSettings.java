/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.base.ui.viewer.DocViewerConfig;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class OpenUsingDocViewerOperationSettings extends AbstractLaunchConfigOperationSettings {
	
	
	public OpenUsingDocViewerOperationSettings() {
		super(DocViewerConfig.TYPE_ID);
	}
	
	
	@Override
	public String getId() {
		return OpenUsingDocViewerOperation.ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_OpenUsingDocViewer_label;
	}
	
	
	@Override
	protected void initializeNewLaunchConfig(final ILaunchConfigurationWorkingCopy config) {
	}
	
}
