/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingOperationSettings;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class OpenUsingEclipseOperationSettings extends DocProcessingOperationSettings {
	
	
	public OpenUsingEclipseOperationSettings() {
	}
	
	
	@Override
	public String getId() {
		return OpenUsingEclipseOperation.ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_OpenUsingEclipse_label;
	}
	
	
	@Override
	protected Composite createControl(final Composite parent) {
		final Composite composite= super.createControl(parent);
		
		composite.setLayout(LayoutUtils.newCompositeGrid(1));
		
		LayoutUtils.addSmallFiller(composite, true);
		
		final Link link= new Link(composite, SWT.NONE);
		link.setText("Global preferences: "
				+ "<a href=\"org.eclipse.ui.preferencePages.FileEditors\">Editor File Associations</a>.");
		composite.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false));
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				final PreferenceDialog dialog= org.eclipse.ui.dialogs.PreferencesUtil.createPreferenceDialogOn(null, e.text, null, null);
				if (dialog != null) {
					dialog.open();
				}
			}
		});
		
		return composite;
	}
	
	
}
