/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.mylyn.wikitext.parser.markup.MarkupLanguage;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCodeStyleSettings;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCoreAccess;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikidocDocumentSetupParticipant;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;


/**
 * Configurator for Wikitext document code source viewers.
 */
@NonNullByDefault
public class WikidocSourceViewerConfigurator extends SourceEditorViewerConfigurator
		implements WikitextCoreAccess, PropertyChangeListener {
	
	
	private static final Set<String> RESET_GROUP_IDS= ImCollections.newSet(
			WikitextCodeStyleSettings.INDENT_GROUP_ID );
	
	
	private final WikitextMarkupLanguage markupLanguage;
	
	private WikitextCoreAccess sourceCoreAccess= nonNullLateInit();
	
	private final WikitextCodeStyleSettings wikitextCodeStyleCopy;
	
	
	public WikidocSourceViewerConfigurator(final WikitextMarkupLanguage markupLanguage,
			final @Nullable WikitextCoreAccess coreAccess,
			final WikidocSourceViewerConfiguration config) {
		super(config);
		this.markupLanguage= nonNullAssert(markupLanguage);
		
		this.wikitextCodeStyleCopy= new WikitextCodeStyleSettings(1);
		config.setCoreAccess(this);
		setSource(coreAccess);
		
		this.wikitextCodeStyleCopy.load(this.sourceCoreAccess.getWikitextCodeStyle());
		this.wikitextCodeStyleCopy.resetDirty();
		this.wikitextCodeStyleCopy.addPropertyChangeListener(this);
	}
	
	
	@Override
	public IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new WikidocDocumentSetupParticipant(this.markupLanguage);
	}
	
	@Override
	protected Set<String> getResetGroupIds() {
		return RESET_GROUP_IDS;
	}
	
	
	public void setSource(@Nullable WikitextCoreAccess newAccess) {
		if (newAccess == null) {
			newAccess= WikitextCore.getWorkbenchAccess();
		}
		if (this.sourceCoreAccess != newAccess) {
			this.sourceCoreAccess= newAccess;
			handleSettingsChanged(null, null);
		}
	}
	
	
	@Override
	public void setTarget(final SourceEditor sourceEditor) {
		super.setTarget(sourceEditor);
		
		final SourceViewer viewer= sourceEditor.getViewer();
		viewer.getTextWidget().setData(ISourceViewer.class.getName(), viewer);
		viewer.getTextWidget().setData(MarkupLanguage.class.getName(), this.markupLanguage);
	}
	
	@Override
	public void handleSettingsChanged(final @Nullable Set<String> groupIds,
			final @Nullable Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		
		this.wikitextCodeStyleCopy.resetDirty();
	}
	
	@Override
	protected void checkSettingsChanges(final Set<String> groupIds, final Map<String, Object> options) {
		super.checkSettingsChanges(groupIds, options);
		
		if (groupIds.contains(WikitextCodeStyleSettings.INDENT_GROUP_ID)) {
			this.wikitextCodeStyleCopy.load(this.sourceCoreAccess.getWikitextCodeStyle());
		}
		if (groupIds.contains(WikitextEditingSettings.EDITING_PREF_QUALIFIER)) {
			this.updateCompleteConfig= true;
		}
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.sourceCoreAccess.getPrefs();
	}
	
	@Override
	public WikitextCodeStyleSettings getWikitextCodeStyle() {
		return this.wikitextCodeStyleCopy;
	}
	
}
