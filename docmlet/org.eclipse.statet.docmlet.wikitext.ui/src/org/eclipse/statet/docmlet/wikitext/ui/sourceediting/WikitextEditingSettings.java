/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.sourceediting;

import org.eclipse.jface.preference.IPreferenceStore;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.EnumPref;

import org.eclipse.statet.docmlet.wikitext.ui.WikitextUI;
import org.eclipse.statet.internal.docmlet.wikitext.ui.WikitextUIPlugin;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.HardWrapMode;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.TabAction;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistPreferences;


@NonNullByDefault
public class WikitextEditingSettings {
	// Default values see WikitextUIPreferenceInitializer
	
	
	public static final String EDITING_PREF_QUALIFIER= WikitextUI.BUNDLE_ID + "/editor/editing"; //$NON-NLS-1$
	
	
	public static final BooleanPref SPELLCHECKING_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SpellCheck.enabled"); //$NON-NLS-1$
	
	// not in group
	public static final BooleanPref FOLDING_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "Folding.enabled"); //$NON-NLS-1$
	
	public static final String FOLDING_SHARED_GROUP_ID= "Wikitext/editor/folding.shared"; //$NON-NLS-1$
	
	public static final BooleanPref FOLDING_RESTORE_STATE_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "Folding.RestoreState.enabled"); //$NON-NLS-1$
	
	public static final BooleanPref MARKOCCURRENCES_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "MarkOccurrences.enabled"); //$NON-NLS-1$
	
	
	public static final String SMARTINSERT_GROUP_ID= "Wikitext/editor/smartinsert"; //$NON-NLS-1$
	
	public static final Preference<Boolean> SMARTINSERT_BYDEFAULT_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.ByDefault.enabled"); //$NON-NLS-1$
	
	public static final EnumPref<TabAction> SMARTINSERT_TAB_ACTION_PREF= new EnumPref<>(
			EDITING_PREF_QUALIFIER, "SmartInsert.Tab.action", TabAction.class, //$NON-NLS-1$
			TabAction.INSERT_INDENT_LEVEL );
	
	public static final Preference<Boolean> SMARTINSERT_CLOSEBRACKETS_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.CloseBrackets.enabled"); //$NON-NLS-1$
	public static final Preference<Boolean> SMARTINSERT_CLOSEPARENTHESIS_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.CloseParenthesis.enabled"); //$NON-NLS-1$
	public static final Preference<Boolean> SMARTINSERT_CLOSEMATHDOLLAR_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.CloseMathDollar.enabled"); //$NON-NLS-1$
	
	public static final Preference<Boolean> SMARTINSERT_HARDWRAP_TEXT_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.HardWrapText.enabled"); //$NON-NLS-1$
	public static final EnumPref<HardWrapMode> SMARTINSERT_HARDWRAP_MODE_PREF= new EnumPref<>(
			EDITING_PREF_QUALIFIER, "SmartInsert.HardWrap.mode", HardWrapMode.class, //$NON-NLS-1$
			HardWrapMode.MERGE );
	
	
	public static final String TEXTSTYLE_CONFIG_QUALIFIER= WikitextUI.BUNDLE_ID + "/textstyle/Wikitext"; //$NON-NLS-1$
	
	public static final String ASSIST_PREF_QUALIFIER= WikitextUI.BUNDLE_ID + "/editor/assist"; //$NON-NLS-1$
	public static final String ASSIST_WIKIDOC_PREF_QUALIFIER= ASSIST_PREF_QUALIFIER + "/Wikidoc"; //$NON-NLS-1$
	
	public static AssistPreferences getAssistPreferences() {
		return LtkUIPreferences.getAssistPreferences();
	}
	
	
	public static IPreferenceStore getPreferenceStore() {
		return WikitextUIPlugin.getInstance().getPreferenceStore();
	}
	
}
