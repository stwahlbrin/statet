/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui;

import org.eclipse.mylyn.wikitext.parser.markup.MarkupLanguage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.ui.WikitextMarkupHelpProvider;


@NonNullByDefault
public class WikitextUI {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.docmlet.wikitext.ui"; //$NON-NLS-1$
	
	
	public static final String EDITOR_CONTEXT_ID= "org.eclipse.statet.docmlet.contexts.WikitextEditor"; //$NON-NLS-1$
	
	
	public static final String BASE_PREF_PAGE_ID= "org.eclipse.statet.docmlet.preferencePages.Wikitext"; //$NON-NLS-1$
	
	public static final String EDITOR_PREF_PAGE_ID= "org.eclipse.statet.docmlet.preferencePages.WikitextEditor"; //$NON-NLS-1$
	
	
	public static @Nullable String getMarkupHelpContentIdFor(final WikitextMarkupLanguage markupLanguage) {
		return WikitextMarkupHelpProvider.getContentIdFor((MarkupLanguage) markupLanguage);
	}
	
}
