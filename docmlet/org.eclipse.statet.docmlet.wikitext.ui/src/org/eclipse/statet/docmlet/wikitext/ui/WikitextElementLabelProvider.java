/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextElement;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.ltk.model.core.element.EmbeddingForeignElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.ElementLabelProvider;
import org.eclipse.statet.yaml.core.model.YamlModel;


@NonNullByDefault
public class WikitextElementLabelProvider implements ElementLabelProvider {
	
	
	private final DocmlBaseUIResources docBaseResources;
	
	
	public WikitextElementLabelProvider() {
		this.docBaseResources= DocmlBaseUIResources.INSTANCE;
	}
	
	
	protected Image getDocBaseImage(final String imageId) {
		return this.docBaseResources.getImage(imageId);
	}
	
	
	@Override
	public @Nullable Image getImage(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == WikitextModel.WIKIDOC_TYPE_ID) {
			switch (element.getElementType() & LtkModelElement.MASK_C123) {
			case WikitextElement.C12_PREAMBLE:
				return getDocBaseImage(DocmlBaseUIResources.OBJ_PREAMBLE_IMAGE_ID);
			case WikitextElement.C12_SECTIONING | 1:
				return getDocBaseImage(DocmlBaseUIResources.OBJ_HEADING1_IMAGE_ID);
			case WikitextElement.C12_SECTIONING | 2:
				return getDocBaseImage(DocmlBaseUIResources.OBJ_HEADING2_IMAGE_ID);
			case WikitextElement.C12_SECTIONING | 3:
				return getDocBaseImage(DocmlBaseUIResources.OBJ_HEADING3_IMAGE_ID);
			case WikitextElement.C12_SECTIONING | 4:
				return getDocBaseImage(DocmlBaseUIResources.OBJ_HEADING4_IMAGE_ID);
			case WikitextElement.C12_SECTIONING | 5:
				return getDocBaseImage(DocmlBaseUIResources.OBJ_HEADING5_IMAGE_ID);
			case WikitextElement.C12_SECTIONING | 6:
				return getDocBaseImage(DocmlBaseUIResources.OBJ_HEADING6_IMAGE_ID);
			case WikitextElement.C1_EMBEDDED: {
				final var foreignElement= ((EmbeddingForeignElement<?, ?>)element).getForeignElement();
				return (foreignElement != null) ? getEmbeddedForeignImage(foreignElement) : null; }
			default:
				return null;
			}
		}
		return null;
	}
	
	protected @Nullable Image getEmbeddedForeignImage(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == YamlModel.YAML_TYPE_ID) {
			return getDocBaseImage(DocmlBaseUIResources.OBJ_PREAMBLE_IMAGE_ID);
		}
		return null;
	}
	
	@Override
	public @Nullable String getText(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == WikitextModel.WIKIDOC_TYPE_ID) {
			return element.getElementName().getDisplayName();
		}
		return null;
	}
	
}
