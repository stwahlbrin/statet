/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.config;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.preference.ColorSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.ui.ColorSelectorObservableValue;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.PreferenceUIUtils;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.EmbeddedHtml;


@NonNullByDefault
public class WikitextBasePreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public WikitextBasePreferencePage() {
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() {
		return new WikitextBaseConfigurationBlock(createStatusChangedListener());
	}
	
}


@NonNullByDefault
class WikitextBaseConfigurationBlock extends ManagedConfigurationBlock {
	
	
	private ColorSelector htmlBackgroundColorSelector= nonNullLateInit();
	private ColorSelector htmlCommentColorSelector= nonNullLateInit();
	
	
	public WikitextBaseConfigurationBlock(final StatusChangeListener statusListener) {
		super(null, statusListener);
	}
	
	
	@Override
	public void createBlockArea(final Composite pageComposite) {
		final Map<Preference<?>, String> prefs= new HashMap<>();
		
		prefs.put(EmbeddedHtml.HTML_BACKGROUND_COLOR_PREF, null);
		prefs.put(EmbeddedHtml.HTML_COMMENT_COLOR_PREF, null);
		
		setupPreferenceManager(prefs);
		
		{	final Composite composite= createEditorGroup(pageComposite);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		
		initBindings();
		updateControls();
	}
	
	private Composite createEditorGroup(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newGroupGrid(2));
		composite.setText(Messages.Base_Editors_label + ':');
		
		{	final Link link= addLinkControl(composite, PreferenceUIUtils.composeSeeAlsoPreferencePages()
					.add("All <a>Text Editors</a> in Eclipse", "org.eclipse.ui.preferencePages.GeneralTextEditor+")
					.add("All <a>Source Editors of StatET</a>", "org.eclipse.statet.ltk.preferencePages.SourceEditors")
					.add("<a>Wikitext Syntax Hightlighting</a> (Mylyn)", "org.eclipse.mylyn.wikitext.ui.editor.preferences.EditorPreferencePage")
					.add("<a>Wikitext User Templates</a> (Mylyn)", "org.eclipse.mylyn.internal.wikitext.ui.editor.preferences.WikiTextTemplatePreferencePage")
					.toString() );
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
			applyWrapWidth(gd);
			link.setLayoutData(gd);
		}
		
		LayoutUtils.addSmallFiller(composite, false);
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("Supplementary preferences:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		}
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("Background color for &HTML ranges:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			
			final ColorSelector selector= new ColorSelector(composite);
			selector.getButton().setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
			this.htmlBackgroundColorSelector= selector;
		}
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("Text color for HTML c&omments:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			
			final ColorSelector selector= new ColorSelector(composite);
			selector.getButton().setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
			this.htmlCommentColorSelector= selector;
		}
		return composite;
	}
	
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		db.getContext().bindValue(
				new ColorSelectorObservableValue(this.htmlBackgroundColorSelector),
				createObservable(EmbeddedHtml.HTML_BACKGROUND_COLOR_PREF),
				null, null );
		db.getContext().bindValue(
				new ColorSelectorObservableValue(this.htmlCommentColorSelector),
				createObservable(EmbeddedHtml.HTML_COMMENT_COLOR_PREF),
				null, null );
	}
	
}
