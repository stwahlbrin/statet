/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.config;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.text.ui.settings.IndentSettingsUI;
import org.eclipse.statet.ecommons.ui.CombineStatusChangeListener;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCodeStyleSettings;


/**
 * A PreferenceBlock for WikitextCodeStyleSettings (code formatting preferences).
 */
public class WikitextCodeStylePreferenceBlock extends ManagedConfigurationBlock {
	// in future supporting multiple profiles?
	// -> we bind to bean not to preferences
	
	
	private WikitextCodeStyleSettings model;
	
	private IndentSettingsUI stdIndentSettings;
	
	private final CombineStatusChangeListener statusListener;
	
	
	public WikitextCodeStylePreferenceBlock(final IProject project, final StatusChangeListener statusListener) {
		super(project);
		this.statusListener= new CombineStatusChangeListener(statusListener);
		setStatusListener(this.statusListener);
	}
	
	
	@Override
	protected void createBlockArea(final Composite pageComposite) {
		final Map<Preference<?>, String> prefs= new HashMap<>();
		
		prefs.put(WikitextCodeStyleSettings.TAB_SIZE_PREF, WikitextCodeStyleSettings.INDENT_GROUP_ID);
		prefs.put(WikitextCodeStyleSettings.INDENT_DEFAULT_TYPE_PREF, WikitextCodeStyleSettings.INDENT_GROUP_ID);
		prefs.put(WikitextCodeStyleSettings.INDENT_SPACES_COUNT_PREF, WikitextCodeStyleSettings.INDENT_GROUP_ID);
		prefs.put(WikitextCodeStyleSettings.REPLACE_CONVERSATIVE_PREF, WikitextCodeStyleSettings.INDENT_GROUP_ID);
		prefs.put(WikitextCodeStyleSettings.REPLACE_TABS_WITH_SPACES_PREF, WikitextCodeStyleSettings.INDENT_GROUP_ID);
		
		setupPreferenceManager(prefs);
		
		this.model= new WikitextCodeStyleSettings(0);
		this.stdIndentSettings= new IndentSettingsUI();
		
		final Composite mainComposite= new Composite(pageComposite, SWT.NONE);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		mainComposite.setLayout(LayoutUtils.newCompositeGrid(2));
		
		final TabFolder folder= new TabFolder(mainComposite, SWT.NONE);
		folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		{	final TabItem item= new TabItem(folder, SWT.NONE);
			item.setText(this.stdIndentSettings.getGroupLabel());
			item.setControl(createIndentControls(folder));
		}
		{	final TabItem item= new TabItem(folder, SWT.NONE);
			item.setText("&Line Wrapping");
			item.setControl(createLineControls(folder));
		}
		
		initBindings();
		updateControls();
	}
	
	private Control createIndentControls(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newTabGrid(2));
		
		this.stdIndentSettings.createControls(composite);
		this.stdIndentSettings.getTabSizeControl().setEditable(false);
		LayoutUtils.addSmallFiller(composite, false);
		
		final Composite depthComposite= new Composite(composite, SWT.NONE);
		depthComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		depthComposite.setLayout(LayoutUtils.newCompositeGrid(4));
		
		LayoutUtils.addSmallFiller(composite, false);
		return composite;
	}
	
	private Control createLineControls(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newTabGrid(2));
		
		this.stdIndentSettings.addLineWidth(composite);
		
		return composite;
	}
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		this.stdIndentSettings.addBindings(db, this.model);
	}
	
	@Override
	protected void updateControls() {
		this.model.load(this);
		this.model.resetDirty();
		getDataBinding().getContext().updateTargets();  // required for invalid target values
	}
	
	@Override
	protected void updatePreferences() {
		if (this.model.isDirty()) {
			this.model.resetDirty();
			setPrefValues(this.model.toPreferencesMap());
		}
	}
	
}
