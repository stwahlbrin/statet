/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import org.eclipse.swt.graphics.RGB;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.ui.RGBPref;

import org.eclipse.statet.docmlet.wikitext.ui.WikitextUI;


public class EmbeddedHtml {
	
	
	public static final String QUALIFIER= WikitextUI.BUNDLE_ID;
	
	public static final String HTML_COMMENT_COLOR_KEY= "html_ts_Comment.Font.color"; //$NON-NLS-1$
	public static final Preference<RGB> HTML_COMMENT_COLOR_PREF= new RGBPref(QUALIFIER, HTML_COMMENT_COLOR_KEY);
	
	public static final String HTML_BACKGROUND_COLOR_KEY= "html_ts_Default.Background.color"; //$NON-NLS-1$
	public static final Preference<RGB> HTML_BACKGROUND_COLOR_PREF= new RGBPref(QUALIFIER, HTML_BACKGROUND_COLOR_KEY);
	
	
}
