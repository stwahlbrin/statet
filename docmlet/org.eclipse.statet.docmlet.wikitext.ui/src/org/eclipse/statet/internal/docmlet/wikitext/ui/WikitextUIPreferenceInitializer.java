/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.workbench.ui.IWaThemeConstants;
import org.eclipse.statet.ecommons.workbench.ui.util.ThemeUtil;

import org.eclipse.statet.docmlet.wikitext.ui.WikitextUI;
import org.eclipse.statet.docmlet.wikitext.ui.editors.WikitextEditorBuild;
import org.eclipse.statet.docmlet.wikitext.ui.sourceediting.WikitextEditingSettings;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.EmbeddedHtml;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.HardWrapMode;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.TabAction;


public class WikitextUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public WikitextUIPreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IScopeContext scope= DefaultScope.INSTANCE;
		final ThemeUtil theme= new ThemeUtil();
		
		final IPreferenceStore store= WikitextUIPlugin.getInstance().getPreferenceStore();
		EditorsUI.useAnnotationsPreferencePage(store);
		EditorsUI.useQuickDiffPreferencePage(store);
		
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.FOLDING_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.FOLDING_RESTORE_STATE_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.MARKOCCURRENCES_ENABLED_PREF, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.SMARTINSERT_BYDEFAULT_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.SMARTINSERT_TAB_ACTION_PREF, TabAction.CORRECT_INDENT);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.SMARTINSERT_CLOSEBRACKETS_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.SMARTINSERT_CLOSEPARENTHESIS_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.SMARTINSERT_CLOSEMATHDOLLAR_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.SMARTINSERT_HARDWRAP_TEXT_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, WikitextEditingSettings.SMARTINSERT_HARDWRAP_MODE_PREF, HardWrapMode.MERGE);
		
		PreferenceUtils.setPrefValue(scope, WikitextEditorBuild.PROBLEMCHECKING_ENABLED_PREF, Boolean.TRUE);
		
		final IEclipsePreferences pref= scope.getNode(WikitextUI.BUNDLE_ID);
		
		pref.put(EmbeddedHtml.HTML_BACKGROUND_COLOR_KEY,
				theme.getColorPrefValue(IWaThemeConstants.CODE_MOD1_BACKGROUND_COLOR) );
		pref.put(EmbeddedHtml.HTML_COMMENT_COLOR_KEY,
				theme.getColorPrefValue(IWaThemeConstants.CODE_COMMENT_COLOR) );
	}
	
}
