/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.model.build;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.project.TexIssues;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextIssues;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.yaml.core.project.YamlIssues;


@NonNullByDefault
public class WikidocSourceUnitModelContainer<TSourceUnit extends WikitextSourceUnit>
		extends SourceUnitModelContainer<TSourceUnit, WikidocSourceUnitModelInfo> {
	
	
	public static final IssueTypeSet ISSUE_TYPE_SET= new IssueTypeSet(WikitextCore.BUNDLE_ID,
			WikitextIssues.TASK_CATEGORY,
			ImCollections.newList(
					WikitextIssues.WIKIDOC_MODEL_PROBLEM_CATEGORY,
					YamlIssues.YAML_MODEL_PROBLEM_CATEGORY,
					TexIssues.LTX_MODEL_PROBLEM_CATEGORY ));
	
	
	public WikidocSourceUnitModelContainer(final TSourceUnit unit,
			final @Nullable SourceUnitIssueSupport issueSupport) {
		super(unit, issueSupport);
	}
	
	
	@Override
	public Class<?> getAdapterClass() {
		return WikidocSourceUnitModelContainer.class;
	}
	
	@Override
	public boolean isContainerFor(final String modelTypeId) {
		return (modelTypeId == WikitextModel.WIKIDOC_TYPE_ID);
	}
	
	public @Nullable String getNowebType() {
		return null;
	}
	
	@Override
	protected ModelManager getModelManager() {
		return WikitextModel.getWikidocModelManager();
	}
	
}
