/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.markup;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.source.SourceConfig;


@NonNullByDefault
public interface WikitextMarkupLanguage extends Cloneable, SourceConfig {
	
	
	int TEMPLATE_MODE=                  1 << 4;
	
	int MYLYN_COMPAT_MODE=              1 << 8;
	
	
	String getName();
	
	WikitextMarkupLanguage clone();
	
	@Nullable String getScope();
	WikitextMarkupLanguage clone(final String scope, final int mode);
	
	@Nullable MarkupConfig getMarkupConfig();
	void setMarkupConfig(final @Nullable MarkupConfig config);
	
	int getMode();
	boolean isModeEnabled(final int modeMask);
	
}
