/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.extdoc;

import java.util.regex.Pattern;

import org.eclipse.statet.docmlet.wikitext.core.source.RegexInlineWeaveParticipant;


public class TexMathSBackslashInlineWeaveParticipant extends RegexInlineWeaveParticipant {
	
	
	/**
	 * \(...\), no empty line
	 * \\\((?:(?!\n[\r \t]*\n|\\\))\p{all})+\\\)
	 */
	private final static Pattern DEFAULT_PATTERN= Pattern.compile("(\\\\\\((?:(?!\\n[\\r \\t]*\\n|\\\\\\))\\p{all})+\\\\\\))"); //$NON-NLS-1$
	
	
	public TexMathSBackslashInlineWeaveParticipant() {
		super(ExtdocMarkupLanguage.EMBEDDED_LTX, ExtdocMarkupLanguage.EMBEDDED_TEX_MATH_SBACKSLASH_INLINE_DESCR,
				DEFAULT_PATTERN);
	}
	
}
