/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.text.core.TextRegion;


public class SourceTextBlockAttributes extends SourceElementAttributes {
	
	
	private ImList<? extends TextRegion> textRegions;
	
	
	public SourceTextBlockAttributes(final ImList<? extends TextRegion> textRegions, final int flags) {
		super(flags);
		this.textRegions= textRegions;
	}
	
	
	public void setTextRegions(final ImList<? extends TextRegion> textRegions) {
		this.textRegions= textRegions;
	}
	
	public ImList<? extends TextRegion> getTextRegions() {
		return this.textRegions;
	}
	
}
