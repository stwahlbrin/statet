/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source;


public abstract class BlockWeaveParticipant extends WeaveParticipant {
	
	
	private static final int TEXT_LENGTH= 1 + REPLACEMENT_STRING.length();
	
	
	public BlockWeaveParticipant() {
	}
	
	
	public abstract boolean checkStartLine(int startOffset, int endOffset);
	
	public abstract int getStartOffset();
	
	public abstract boolean checkEndLine(int startOffset, int endOffset);
	
	protected void appendReplacement(final StringBuilder sb,
			final String source, final int startOffset, final int endOffset) {
		sb.append(REPLACEMENT_CHAR);
		sb.append(REPLACEMENT_LINE);
	}
	
	protected int getTextLength() {
		return TEXT_LENGTH;
	}
	
}
