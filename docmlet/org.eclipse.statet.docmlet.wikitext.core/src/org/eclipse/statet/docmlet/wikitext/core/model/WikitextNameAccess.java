/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.model;

import org.eclipse.jface.text.Position;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.ltk.model.core.element.NameAccess;


@NonNullByDefault
public abstract class WikitextNameAccess extends WikitextElementName
		implements NameAccess<WikitextAstNode, WikitextNameAccess> {
	
	
	public static Position getTextPosition(final WikitextAstNode node) {
		return new Position(node.getStartOffset(), node.getLength());
	}
	
	public static TextRegion getTextRegion(final WikitextAstNode node) {
		return new BasicTextRegion(node);
	}
	
	
	protected WikitextNameAccess() {
	}
	
	
}
