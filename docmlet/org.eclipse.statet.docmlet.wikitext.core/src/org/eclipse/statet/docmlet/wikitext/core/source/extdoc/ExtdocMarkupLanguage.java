/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.extdoc;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.wikitext.core.WikitextProblemReporter;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageExtension2;
import org.eclipse.statet.docmlet.wikitext.core.source.MarkupSourceFormatAdapter;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;
import org.eclipse.statet.yaml.core.model.YamlModel;


@NonNullByDefault
public interface ExtdocMarkupLanguage extends WikitextMarkupLanguage, WikitextMarkupLanguageExtension2 {
	
	
	String EMBEDDED_HTML= "Html"; //$NON-NLS-1$
	
	int EMBEDDED_HTML_OTHER_BLOCK_DESCR=                    EmbeddingAstNode.EMBED_CHUNK;
	int EMBEDDED_HTML_DISTINCT_MASK=                        0x000000_7_0;
	int EMBEDDED_HTML_DISTINCT_SHIFT=                       Integer.lowestOneBit(EMBEDDED_HTML_DISTINCT_MASK);
	int EMBEDDED_HTML_COMMENT_FLAG=                         0x000000_8_0;
	int EMBEDDED_HTML_COMMENT_BLOCK_DESCR=                  EmbeddingAstNode.EMBED_CHUNK | EMBEDDED_HTML_COMMENT_FLAG;
	
	int EMBEDDED_HTML_OTHER_INLINE_DESCR=                   EmbeddingAstNode.EMBED_INLINE;
	int EMBEDDED_HTML_COMMENT_INLINE_DESCR=                 EmbeddingAstNode.EMBED_INLINE | EMBEDDED_HTML_COMMENT_FLAG;
	
	
	String EMBEDDED_YAML= YamlModel.YAML_TYPE_ID;
	
	int EMBEDDED_YAML_METADATA_FLAG=                        0x000000_1_0;
	int EMBEDDED_YAML_METADATA_CHUNK_DESCR=                 EmbeddingAstNode.EMBED_CHUNK | EMBEDDED_YAML_METADATA_FLAG;
	
	
	String EMBEDDED_LTX= TexModel.LTX_TYPE_ID;
	
	int EMBEDDED_TEX_MATH_FLAG=                             0x000000_2_0;
	int EMBEDDED_TEX_MATH_DOLLARS_INLINE_DESCR=             EmbeddingAstNode.EMBED_INLINE | EMBEDDED_TEX_MATH_FLAG |
	                                                        0x0000_01_0_0;
	int EMBEDDED_TEX_MATH_DOLLARS_DISPLAY_DESCR=            EmbeddingAstNode.EMBED_INLINE | EMBEDDED_TEX_MATH_FLAG |
	                                                        0x0000_02_0_0;
	int EMBEDDED_TEX_MATH_SBACKSLASH_INLINE_DESCR=          EmbeddingAstNode.EMBED_INLINE | EMBEDDED_TEX_MATH_FLAG |
	                                                        0x0000_03_0_0;
	int EMBEDDED_TEX_MATH_SBACKSLASH_DISPLAY_DESCR=         EmbeddingAstNode.EMBED_INLINE | EMBEDDED_TEX_MATH_FLAG |
	                                                        0x0000_04_0_0;
	
	
	@Override
	ExtdocMarkupLanguage clone();
	@Override
	ExtdocMarkupLanguage clone(String scopeKey, int mode);
	
	
	WikitextProblemReporter getProblemReporter();
	
	@Nullable MarkupSourceFormatAdapter getSourceFormatAdapter();
	
}
