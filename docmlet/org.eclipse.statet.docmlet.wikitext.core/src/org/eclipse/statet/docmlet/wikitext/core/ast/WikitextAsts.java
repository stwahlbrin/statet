/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.Asts;


@NonNullByDefault
public class WikitextAsts extends Asts {
	
	
	/**
	 * Returns the index in the array for the node at the specified offset
	 * 
	 * @param nodes array with nodes (items can be <code>null</code>)
	 * @param offset the offset of the searched node
	 * @return the index in the array if found, otherwise -1
	 */
	public static int getIndexAt(final @Nullable WikitextAstNode[] nodes, final int offset) {
		for (int i= 0; i < nodes.length; i++) {
			if (nodes[i] != null) {
				if (offset < nodes[i].getStartOffset()) {
					return -1;
				}
				if (offset <= nodes[i].getEndOffset()) {
					return i;
				}
			}
		}
		return -1;
	}
	
}
