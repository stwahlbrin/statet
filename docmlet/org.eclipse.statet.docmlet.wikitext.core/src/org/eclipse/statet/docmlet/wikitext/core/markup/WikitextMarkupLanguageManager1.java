/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.markup;

import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface WikitextMarkupLanguageManager1 extends WikitextMarkupLanguageManager {
	
	
	interface MarkupConfigChangedListener {
		
		void configChanged(Map<String, List<IProject>> languages, IProgressMonitor monitor) throws CoreException;
		
		void configChanged(IFile file, IProgressMonitor monitor) throws CoreException;
		
	}
	
	
	void addConfigChangedListener(MarkupConfigChangedListener listener);
	void removeConfigChangedListern(MarkupConfigChangedListener listener);
	
//	void setConfig(String languageName, MarkupConfig config);
	@Nullable MarkupConfig getConfig(String languageName);
	
//	void setConfig(final IProject project, String languageName, MarkupConfig config);
//	MarkupConfig getConfig(final IProject project, String languageName);
	
	void setConfig(IFile file, @Nullable MarkupConfig config);
	@Nullable MarkupConfig getConfig(IFile file, String languageName);
	
	@Nullable WikitextMarkupLanguage getLanguage(IFile file,
			@Nullable String languageName, boolean inherit );
	
}
