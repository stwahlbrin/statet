/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.model.build;

import java.util.List;

import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.ast.Embedded;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.ltk.model.core.build.ReconcileConfig;
import org.eclipse.statet.ltk.model.core.build.SourceUnitEmbeddedModelReconciler;


@NonNullByDefault
public interface WikidocSourceUnitEmbeddedModelReconciler<TConfig extends ReconcileConfig<?>>
		extends SourceUnitEmbeddedModelReconciler<WikitextProject, TConfig> {
	
	
	void reconcileAst(SourceContent content,
			List<Embedded> list,
			WikitextMarkupLanguage markupLanguage,
			WikidocSourceUnitModelContainer<?> container, @NonNull TConfig config, int level);
	
	void reconcileModel(WikidocSourceUnitModelInfo wikitextModel, SourceContent content,
			List<? extends EmbeddingForeignReconcileTask<Embedded, WikitextSourceElement>> list,
			WikidocSourceUnitModelContainer<?> container, @NonNull TConfig config, int level,
			SubMonitor m);
	
	void reportIssues(WikidocSourceUnitModelInfo wikitextModel, SourceContent content,
			IssueRequestor issueRequestor,
			WikidocSourceUnitModelContainer<?> container, @NonNull TConfig config, int level);
	
}
