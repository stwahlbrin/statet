/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.util;

import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.preferences.core.util.PreferenceAccessWrapper;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCodeStyleSettings;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCoreAccess;


public class WikitextCoreAccessWrapper extends PreferenceAccessWrapper
		implements WikitextCoreAccess {
	
	
	private WikitextCoreAccess parent;
	
	
	public WikitextCoreAccessWrapper(final WikitextCoreAccess wikitextCoreAccess) {
		if (wikitextCoreAccess == null) {
			throw new NullPointerException("wikitextCoreAccess"); //$NON-NLS-1$
		}
		
		updateParent(null, wikitextCoreAccess);
	}
	
	
	public synchronized WikitextCoreAccess getParent() {
		return this.parent;
	}
	
	public synchronized boolean setParent(final WikitextCoreAccess wikitextCoreAccess) {
		if (wikitextCoreAccess == null) {
			throw new NullPointerException("wikitextCoreAccess"); //$NON-NLS-1$
		}
		if (wikitextCoreAccess != this.parent) {
			updateParent(this.parent, wikitextCoreAccess);
			return true;
		}
		return false;
	}
	
	protected void updateParent(final WikitextCoreAccess oldParent, final WikitextCoreAccess newParent) {
		this.parent= newParent;
		
		super.setPreferenceContexts(newParent.getPrefs().getPreferenceContexts());
	}
	
	@Override
	public void setPreferenceContexts(final ImList<IScopeContext> contexts) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public WikitextCodeStyleSettings getWikitextCodeStyle() {
		return this.parent.getWikitextCodeStyle();
	}
	
}
