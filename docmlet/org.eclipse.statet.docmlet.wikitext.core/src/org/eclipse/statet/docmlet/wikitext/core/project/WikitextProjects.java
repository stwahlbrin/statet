/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.project;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.resources.core.ProjectUtils;

import org.eclipse.statet.internal.docmlet.wikitext.core.Messages;
import org.eclipse.statet.internal.docmlet.wikitext.core.WikitextProjectNature;


@NonNullByDefault
public class WikitextProjects {
	
	
	public static final String WIKITEXT_NATURE_ID= "org.eclipse.statet.docmlet.resourceProjects.Wikitext"; //$NON-NLS-1$
	
	
	public static @Nullable WikitextProject getWikitextProject(final @Nullable IProject project) {
		return WikitextProjectNature.getWikitextProject(project);
	}
	
	/**
	 * 
	 * @param project the project to setup
	 * @param monitor SubMonitor-recommended
	 * @throws CoreException
	 */
	public static void setupWikitextProject(final IProject project,
			final @Nullable IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor,
				NLS.bind(Messages.WikitextProject_ConfigureTask_label, project.getName()),
				2 + 8 );
		
		final IProjectDescription description= project.getDescription();
		boolean changed= false;
		changed|= ProjectUtils.addNature(description, WIKITEXT_NATURE_ID);
		m.worked(2);
		
		if (changed) {
			project.setDescription(description, m.newChild(8));
		}
	}
	
}
