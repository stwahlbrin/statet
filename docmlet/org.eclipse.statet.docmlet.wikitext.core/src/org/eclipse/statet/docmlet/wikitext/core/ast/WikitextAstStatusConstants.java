/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface WikitextAstStatusConstants {
	
	
//	int STATUS_INCOMPLETE_ENV=                             0x00000011;
//	int STATUS2_ENV_MISSING_NAME=                          STATUS_INCOMPLETE_ENV | 0x00001100;
//	int STATUS2_ENV_NOT_CLOSED=                            STATUS_INCOMPLETE_ENV | 0x00002100;
//	int STATUS2_ENV_NOT_OPENED=                            STATUS_INCOMPLETE_ENV | 0x00002200;
//	int STATUS2_ENV_UNKNOWN_ENV=                           STATUS_INCOMPLETE_ENV | 0x00003100;
//	
//	int STATUS_INCOMPLETE_VERBATIM=                        0x00000012;
//	int STATUS2_VERBATIM_INLINE_C_MISSING=                 STATUS_INCOMPLETE_VERBATIM | 0x00001100;
//	int STATUS2_VERBATIM_INLINE_NOT_CLOSED=                STATUS_INCOMPLETE_VERBATIM | 0x00002100;
//	
//	int STATUS_INCOMPLETE_MATH=                            0x00000013;
//	int STATUS2_MATH_NOT_CLOSED=                           STATUS_INCOMPLETE_MATH | 0x00002100;
//	
//	int STATUS_INCOMPLETE_GROUP=                           0x00000014;
//	int STATUS2_GROUP_NOT_CLOSED=                          STATUS_INCOMPLETE_GROUP | 0x00002100;
//	int STATUS2_GROUP_NOT_OPENED=                          STATUS_INCOMPLETE_GROUP | 0x00002200;
//	
//	int STATUS_MISSING_SILENT= 0xf0000000; //?
	
	
}
