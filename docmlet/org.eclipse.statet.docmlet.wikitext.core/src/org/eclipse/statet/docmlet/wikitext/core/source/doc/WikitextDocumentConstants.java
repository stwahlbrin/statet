/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.doc;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;

import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentConstants;


@NonNullByDefault
public interface WikitextDocumentConstants {
	
	
	/**
	 * The id of partitioning of Wikitext documents.
	 */
	String WIKIDOC_PARTITIONING= "org.eclipse.statet.Wikidoc"; //$NON-NLS-1$
	
	
	String WIKIDOC_DEFAULT_CONTENT_TYPE= "Wikitext.Default"; //$NON-NLS-1$
	
	String WIKIDOC_YAML_CHUNK_CONTENT_TYPE= "WikidocYamlConfChunk"; //$NON-NLS-1$
	
	String WIKIDOC_HTML_DEFAULT_CONTENT_TYPE= "Html.Default"; //$NON-NLS-1$
	String WIKIDOC_HTML_COMMENT_CONTENT_TYPE= "Html.Comment"; //$NON-NLS-1$
	
	
	/**
	 * List with all partition content types of Wikitext documents.
	 */
	ImList<String> WIKIDOC_CONTENT_TYPES= ImCollections.newList(
			WIKIDOC_DEFAULT_CONTENT_TYPE,
			WIKIDOC_HTML_DEFAULT_CONTENT_TYPE,
			WIKIDOC_HTML_COMMENT_CONTENT_TYPE );
	
	ImList<String> YAML_CHUNK_CONTENT_TYPES= ImCollections.newList(
			WIKIDOC_YAML_CHUNK_CONTENT_TYPE );
	
	ImList<String> WIKIDOC_EXT_CONTENT_TYPES= ImCollections.concatList(
			WIKIDOC_CONTENT_TYPES,
			YAML_CHUNK_CONTENT_TYPES,
			YamlDocumentConstants.YAML_CONTENT_TYPES,
			TexDocumentConstants.LTX_MATH_CONTENT_TYPES );
	
	
	PartitionConstraint WIKIDOC_DEFAULT_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String partitionType) {
			return (partitionType == WIKIDOC_DEFAULT_CONTENT_TYPE);
		}
	};
	
}
