/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.extdoc;

import java.util.regex.Pattern;

import org.eclipse.statet.docmlet.wikitext.core.source.RegexInlineWeaveParticipant;


public class TexMathDollarsInlineWeaveParticipant extends RegexInlineWeaveParticipant {
	
	
	/**
	 * $...$, no whitespace after start/before end, no empty line, max 3 lines
	 * \$[^\$\s](?:[^\$\n]*(?:\n(?![\r \t]*\n)[^\$\n]*){0,2}[^\$\s])?\$
	 */
	private final static Pattern DEFAULT_PATTERN= Pattern.compile("(\\$[^\\$\\s](?:[^\\$\\n]*(?:\\n(?![\\r \\t]*\\n)[^\\$\\n]*){0,2}[^\\$\\s])?\\$)"); //$NON-NLS-1$
	
	
	/**
	 * 
	 * \$\$[^\$\s](?:[^\$\n]*(?:\n[^\$\n]*){0,2}[^\$\s])?\$\$
	 */
	private final static Pattern TEMPLATE_PATTERN= Pattern.compile("(\\$\\$[^\\$\\s](?:[^\\$\\n]*(?:\\n(?![\\r \\t]*\\n)[^\\$\\n]*){0,2}[^\\$\\s])?\\$\\$)"); //$NON-NLS-1$
	
	
	public TexMathDollarsInlineWeaveParticipant(final boolean templateMode) {
		super(ExtdocMarkupLanguage.EMBEDDED_LTX, ExtdocMarkupLanguage.EMBEDDED_TEX_MATH_DOLLARS_INLINE_DESCR,
				(templateMode) ? TEMPLATE_PATTERN : DEFAULT_PATTERN);
	}
	
}
