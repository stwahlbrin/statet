/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.resources.core.AbstractProjectNature;
import org.eclipse.statet.ecommons.resources.core.ProjectUtils;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCodeStyleSettings;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProjects;
import org.eclipse.statet.internal.docmlet.wikitext.core.builder.WikitextProjectBuilder;


@NonNullByDefault
public class WikitextProjectNature extends AbstractProjectNature implements WikitextProject {
	
	
	public static @Nullable WikitextProjectNature getWikitextProject(final @Nullable IProject project) {
		try {
			return (project != null) ? (WikitextProjectNature) project.getNature(WikitextProjects.WIKITEXT_NATURE_ID) : null;
		}
		catch (final CoreException e) {
			WikitextCorePlugin.log(e.getStatus());
			return null;
		}
	}
	
	
	public WikitextProjectNature() {
	}
	
	
	@Override
	public void addBuilders() throws CoreException {
		final IProject project= getProject();
		final IProjectDescription description= project.getDescription();
		boolean changed= false;
		changed|= ProjectUtils.addBuilder(description, WikitextProjectBuilder.BUILDER_ID);
		
		if (changed) {
			project.setDescription(description, null);
		}
	}
	
	@Override
	public void removeBuilders() throws CoreException {
		final IProject project= getProject();
		final IProjectDescription description= project.getDescription();
		boolean changed= false;
		changed|= ProjectUtils.removeBuilder(description, WikitextProjectBuilder.BUILDER_ID);
		
		if (changed) {
			project.setDescription(description, null);
		}
	}
	
	
	@Override
	public WikitextCodeStyleSettings getWikitextCodeStyle() {
		return WikitextCore.getWorkbenchAccess().getWikitextCodeStyle();
	}
	
	
}
