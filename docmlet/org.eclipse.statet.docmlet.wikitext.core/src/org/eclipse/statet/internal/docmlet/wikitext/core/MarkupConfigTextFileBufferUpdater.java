/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.IFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1.MarkupConfigChangedListener;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguageDocumentSetupParticipant;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguagePartitioner;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.SourceUnitManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;


public class MarkupConfigTextFileBufferUpdater implements MarkupConfigChangedListener {
	
	
	private class UpdateRunnable implements Runnable {
		
		
		private final IFile file;
		private final AbstractDocument document;
		private final List<String> partitionings;
		private final Map<String, List<IProject>> languages;
		
		
		public UpdateRunnable(final IFile file, final AbstractDocument document,
				final List<String> partitionings, final Map<String, List<IProject>> languages) {
			this.file= file;
			this.document= document;
			this.partitionings= partitionings;
			this.languages= languages;
		}
		
		
		@Override
		public void run() {
			// runs in UI
			for (final String partitioning : this.partitionings) {
				final IDocumentPartitioner partitioner= this.document.getDocumentPartitioner(partitioning);
				if (partitioner instanceof MarkupLanguagePartitioner) {
					try {
						final MarkupLanguagePartitioner markupPartitioner= (MarkupLanguagePartitioner) partitioner;
						final WikitextMarkupLanguage currentMarkupLanguage= markupPartitioner.getMarkupLanguage();
						if (this.languages != null && !this.languages.containsKey(currentMarkupLanguage.getName())) {
							continue;
						}
						WikitextMarkupLanguage newMarkupLanguage= null;
						if (this.file != null) {
							newMarkupLanguage= MarkupConfigTextFileBufferUpdater.this.markupLanguageManager.getLanguage(this.file,
									currentMarkupLanguage.getName(), true );
						}
						else {
							newMarkupLanguage= MarkupConfigTextFileBufferUpdater.this.markupLanguageManager.getLanguage(
									currentMarkupLanguage.getName() );
						}
						
						if (newMarkupLanguage != null) {
							markupPartitioner.setMarkupLanguage(newMarkupLanguage);
							this.document.setDocumentPartitioner(partitioning, partitioner);
						}
					}
					catch (final Exception e) {
						WikitextCorePlugin.log(new Status(IStatus.ERROR, WikitextCore.BUNDLE_ID,
								NLS.bind("An error occurred when trying to update the markup configuration of the open document (partitioning= {0}).", partitioning),
								e ));
					}
				}
			}
		}
		
	}
	
	
	private final WikitextMarkupLanguageManager1 markupLanguageManager;
	
	private final ITextFileBufferManager textFileBufferManager;
	
	// TODO lookup at runtime
	private final ImIdentityList<String> markupModelTypeIds= ImCollections.newIdentityList(
			WikitextModel.WIKIDOC_TYPE_ID,
			"WikidocRweave" );
	
	private final List<String> checkPartitionings= new ArrayList<>();
	
	
	public MarkupConfigTextFileBufferUpdater(final WikitextMarkupLanguageManager1 markupLanguageManager) {
		this.markupLanguageManager= markupLanguageManager;
		this.textFileBufferManager= FileBuffers.getTextFileBufferManager();
	}
	
	
	@Override
	public void configChanged(final Map<String, List<IProject>> languages,
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 10 + 10);
		
		{	final SubMonitor m1= m.newChild(10).setWorkRemaining(1 + 8 + 1 + 2);
			IFileBuffer[] fileBuffers;
			fileBuffers= this.textFileBufferManager.getFileBuffers();
			m.worked(1);
			checkFileBuffers(fileBuffers, languages, m1.newChild(8));
			fileBuffers= this.textFileBufferManager.getFileStoreFileBuffers();
			m.worked(1);
			checkFileBuffers(fileBuffers, languages, m1.newChild(2));
		}
		
		{	// Reconcile source units
			final SubMonitor m1= m.newChild(10).setWorkRemaining(10);
			final SourceUnitManager suManager= LtkModels.getSourceUnitManager();
			final List<SourceUnit> sus= suManager.getOpenSourceUnits(this.markupModelTypeIds,
					Ltk.EDITOR_CONTEXT);
			m1.worked(1);
			m1.setWorkRemaining(sus.size());
			for (final SourceUnit su : sus) {
				checkEditSourceUnit(su, languages, m1.newChild(1));
			}
		}
	}
	
	@Override
	public void configChanged(final IFile file,
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 10 + 10);
		
		{	// Update document
			final SubMonitor m1= m.newChild(10).setWorkRemaining(10);
			final ITextFileBuffer fileBuffer= this.textFileBufferManager.getTextFileBuffer(
					file.getFullPath(), LocationKind.IFILE );
			if (fileBuffer != null) {
				checkFileBuffer(fileBuffer, file, null);
			}
		}
		
		{	// Reconcile source units
			final SubMonitor m1= m.newChild(10).setWorkRemaining(10);
			final SourceUnitManager suManager= LtkModels.getSourceUnitManager();
			final List<SourceUnit> sus= suManager.getOpenSourceUnits(this.markupModelTypeIds,
					Ltk.EDITOR_CONTEXT, file );
			m1.worked(1);
			m1.setWorkRemaining(sus.size());
			for (final SourceUnit su : sus) {
				checkEditSourceUnit(su, null, m1.newChild(1));
			}
		}
	}
	
	
	private AbstractDocument getDocument(final ITextFileBuffer fileBuffer) {
		final IDocument document= fileBuffer.getDocument();
		if (document instanceof AbstractDocument) {
			return (AbstractDocument) document;
		}
		else {
			return null;
		}
	}
	
	private void checkFileBuffers(final IFileBuffer[] fileBuffers,
			final Map<String, List<IProject>> languages, final SubMonitor m) {
		// Update documents
		for (int i= 0; i < fileBuffers.length; i++) {
			m.setWorkRemaining(fileBuffers.length - i);
			if (fileBuffers[i] instanceof ITextFileBuffer) {
				checkFileBuffer((ITextFileBuffer) fileBuffers[i], null, languages);
				m.worked(1);
			}
		}
	}
	
	private void checkFileBuffer(final ITextFileBuffer fileBuffer, IFile file,
			final Map<String, List<IProject>> languages) {
		final AbstractDocument document= getDocument(fileBuffer);
		if (document == null) {
			return;
		}
		
		this.checkPartitionings.clear();
		final String[] partitionings= document.getPartitionings();
		for (int i= 0; i < partitionings.length; i++) {
			if (document.getDocumentPartitioner(partitionings[i]) instanceof MarkupLanguagePartitioner) {
				this.checkPartitionings.add(partitionings[i]);
			}
		}
		if (this.checkPartitionings.isEmpty()) {
			return;
		}
		
		if (file == null && fileBuffer.getLocation() != null) {
			file= FileBuffers.getWorkspaceFileAtLocation(fileBuffer.getLocation(), true);
		}
		
		final UpdateRunnable runnable= new UpdateRunnable(file, document,
				ImCollections.toList(this.checkPartitionings), languages );
		
		this.textFileBufferManager.execute(runnable); // async
	}
	
	private void checkEditSourceUnit(final SourceUnit su,
			final Map<String, List<IProject>> languages,
			final SubMonitor m) {
		if (su.isConnected()) {
			su.connect(m);
			try {
				if (su.getModelInfo(null, 0, m) == null) {
					return;
				}
				final AbstractDocument document= su.getDocument(m);
				final WikitextMarkupLanguage currentMarkupLanguage= MarkupLanguageDocumentSetupParticipant
						.getMarkupLanguage(document, su.getDocumentContentInfo().getPartitioning());
				if (currentMarkupLanguage != null && languages != null) {
					final List<IProject> projects= languages.get(currentMarkupLanguage.getName());
					if (projects == null) {
						return;
					}
					if (!projects.isEmpty() && su instanceof WorkspaceSourceUnit
							&& !projects.contains(((WorkspaceSourceUnit) su).getResource().getProject())) {
						return;
					}
				}
				su.getModelInfo(null, ModelManager.REFRESH | ModelManager.RECONCILE, m);
			}
			finally {
				su.disconnect(m);
			}
		}
	}
	
}
