/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCoreAccess;
import org.eclipse.statet.internal.docmlet.wikitext.core.model.WikitextModelManagerImpl;


public class WikitextCorePlugin extends Plugin {
	
	
	private static WikitextCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static WikitextCorePlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private List<Disposable> disposables;
	
	private WikitextModelManagerImpl wikitextModelManager;
	
	private BasicWikitextCoreAccess workbenchCoreAccess;
	private BasicWikitextCoreAccess defaultsCoreAccess;
	
	
	public WikitextCorePlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.disposables= new ArrayList<>();
		
		this.workbenchCoreAccess= new BasicWikitextCoreAccess(
				EPreferences.getInstancePrefs() );
		
		this.wikitextModelManager= new WikitextModelManagerImpl();
		this.disposables.add(this.wikitextModelManager);
		
		synchronized (this) {
			this.started= true;
		}
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				this.wikitextModelManager= null;
			}
			
			if (this.workbenchCoreAccess != null) {
				this.workbenchCoreAccess.dispose();
				this.workbenchCoreAccess= null;
			}
			if (this.defaultsCoreAccess != null) {
				this.defaultsCoreAccess.dispose();
				this.defaultsCoreAccess= null;
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, WikitextCore.BUNDLE_ID,
							"Error occured while disposing a module.", //$NON-NLS-1$
							e ));
				}
			}
			this.disposables= null;
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	private void checkStarted() {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
	}
	
	public WikitextModelManagerImpl getWikidocModelManager() {
		return this.wikitextModelManager;
	}
	
	public synchronized WikitextCoreAccess getWorkbenchAccess() {
		if (this.workbenchCoreAccess == null) {
			checkStarted();
		}
		return this.workbenchCoreAccess;
	}
	
	public synchronized WikitextCoreAccess getDefaultsAccess() {
		if (this.defaultsCoreAccess == null) {
			checkStarted();
			this.defaultsCoreAccess= new BasicWikitextCoreAccess(
					EPreferences.getDefaultPrefs() );
		}
		return this.defaultsCoreAccess;
	}
	
}
