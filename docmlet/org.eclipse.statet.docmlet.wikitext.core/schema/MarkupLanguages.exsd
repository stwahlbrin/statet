<?xml version='1.0' encoding='UTF-8'?>
<schema targetNamespace="org.eclipse.statet.docmlet" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema id="WikitextMarkupLanguages"
               plugin="org.eclipse.statet.docmlet.wikitext.core"
               name="Markup Languages (additional to Mylyn)"/>
      </appinfo>
      <documentation>
         This extension-point allows to register markup languages.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <choice minOccurs="0" maxOccurs="unbounded">
            <element ref="markupLanguage"/>
         </choice>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="markupLanguage">
      <annotation>
         <documentation>
            This point allows to provide new markup languages.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  The id of the language.
               </documentation>
               <appinfo>
                  <meta.attribute kind="identifier"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="label" type="string">
            <annotation>
               <documentation>
                  The label of the language.
If not specified, the name is used as label.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="configClass" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.statet.docmlet.wikitext.core.markup.MarkupConfig"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="contentTypeId" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute kind="identifier" basedOn="org.eclipse.core.contenttype.contentTypes/content-type/@id"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         0.4
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="copyright"/>
      </appinfo>
      <documentation>
         Copyright (c) 2014, 2022 Stephan Wahlbrink and others.

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
which is available at https://www.apache.org/licenses/LICENSE-2.0.

SPDX-License-Identifier: EPL-2.0 OR Apache-2.0

Contributors:
    Stephan Wahlbrink &lt;sw@wahlbrink.eu&gt; - initial API and implementation
      </documentation>
   </annotation>

</schema>
