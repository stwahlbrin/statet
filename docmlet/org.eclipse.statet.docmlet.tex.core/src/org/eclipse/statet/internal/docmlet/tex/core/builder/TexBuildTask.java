/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.builder;

import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.TexBuildParticipant;
import org.eclipse.statet.docmlet.tex.core.model.TexWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.docmlet.tex.core.project.TexProject;
import org.eclipse.statet.internal.docmlet.tex.core.TexCorePlugin;
import org.eclipse.statet.internal.docmlet.tex.core.model.LtxReconciler;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.project.core.builder.ProjectBuildTask;


@NonNullByDefault
public class TexBuildTask extends ProjectBuildTask<TexProject, TexWorkspaceSourceUnit, TexBuildParticipant> {
	
	
	private @Nullable LtxReconciler reconciler;
	
	
	public TexBuildTask(final TexProjectBuilder builder) {
		super(builder);
	}
	
	
	private LtxReconciler getReconciler() {
		var reconciler= this.reconciler;
		if (reconciler == null) {
			reconciler= new LtxReconciler(TexCorePlugin.getInstance().getLtxModelManager());
			reconciler.init(getBuilder().getLtkProject(), this.status);
			this.reconciler= reconciler;
		}
		return reconciler;
	}
	
	@Override
	protected void reconcileSourceUnit(final TexWorkspaceSourceUnit sourceUnit, final SubMonitor m) {
		final var adapter= sourceUnit.getAdapter(LtxSourceUnitModelContainer.class);
		if (adapter != null) {
			clearSourceUnit(adapter);
			
			getReconciler().reconcile(adapter,
					ModelManager.MODEL_DEPENDENCIES | ModelManager.RECONCILE,
					m );
		}
	}
	
}
