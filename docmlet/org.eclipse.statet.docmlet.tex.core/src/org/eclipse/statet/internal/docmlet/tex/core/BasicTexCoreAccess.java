/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesManageListener;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService.ChangeEvent;

import org.eclipse.statet.docmlet.tex.core.TexCodeStyleSettings;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;


@NonNullByDefault
final class BasicTexCoreAccess implements TexCoreAccess {
	
	
	private static final ImIdentitySet<String> PREF_QUALIFIERS= ImCollections.newIdentitySet(
			TexCommandSet.QUALIFIER );
	
	
	private boolean isDisposed;
	
	private final PreferenceAccess prefs;
	
	private PreferenceSetService.ChangeListener commonPrefsListener;
	
	private TexCommandSet commandSet;
	
	private @Nullable TexCodeStyleSettings codeStyle;
	private PreferencesManageListener codeStyleListener;
	
	
	public BasicTexCoreAccess(final PreferenceAccess prefs) {
		this.prefs= prefs;
		
		this.commonPrefsListener= new PreferenceSetService.ChangeListener() {
			@Override
			public void onPreferenceChanged(final ChangeEvent event) {
				if (event.contains(TexCommandSet.QUALIFIER)) {
					updateCommandSet();
				}
			}
		};
		this.prefs.addPreferenceSetListener(this.commonPrefsListener, PREF_QUALIFIERS);
		
		updateCommandSet();
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.prefs;
	}
	
	private synchronized void updateCommandSet() {
		this.commandSet= new TexCommandSet(this.prefs);
	}
	
	@Override
	public TexCommandSet getTexCommandSet() {
		return this.commandSet;
	}
	
	@Override
	public TexCodeStyleSettings getTexCodeStyle() {
		TexCodeStyleSettings codeStyle= this.codeStyle;
		if (codeStyle == null) {
			synchronized (this) {
				codeStyle= this.codeStyle;
				if (codeStyle == null) {
					codeStyle= new TexCodeStyleSettings(1);
					if (!this.isDisposed) {
						this.codeStyleListener= new PreferencesManageListener(codeStyle,
								this.prefs, TexCodeStyleSettings.ALL_GROUP_IDS );
					}
					codeStyle.load(this.prefs);
					codeStyle.resetDirty();
					this.codeStyle= codeStyle;
				}
			}
		}
		return codeStyle;
	}
	
	
	public synchronized void dispose() {
		this.isDisposed= true;
		
		if (this.codeStyleListener != null) {
			this.codeStyleListener.dispose();
			this.codeStyleListener= null;
		}
		if (this.commonPrefsListener != null) {
			this.prefs.removePreferenceSetListener(this.commonPrefsListener);
			this.commonPrefsListener= null;
		}
	}
	
}
