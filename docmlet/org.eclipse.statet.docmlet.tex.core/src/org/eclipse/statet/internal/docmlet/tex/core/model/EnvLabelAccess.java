/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.model.TexElementName;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;


public class EnvLabelAccess extends TexNameAccess {
	
	
	private final TexAstNode node;
	private final TexAstNode nameNode;
	
	ImList<EnvLabelAccess> all;
	
	
	protected EnvLabelAccess(final TexAstNode node, final TexAstNode labelNode) {
		this.node= node;
		this.nameNode= labelNode;
	}
	
	
	@Override
	public int getType() {
		return ENV;
	}
	
	@Override
	public String getSegmentName() {
		return this.node.getText();
	}
	
	@Override
	public String getDisplayName() {
		return this.node.getText();
	}
	
	@Override
	public TexElementName getNextSegment() {
		return null;
	}
	
	
	@Override
	public TexAstNode getNode() {
		return this.node;
	}
	
	@Override
	public TexAstNode getNameNode() {
		return this.nameNode;
	}
	
	@Override
	public ImList<? extends TexNameAccess> getAllInUnit() {
		return this.all;
	}
	
	
	@Override
	public boolean isWriteAccess() {
		return false;
	}
	
}
