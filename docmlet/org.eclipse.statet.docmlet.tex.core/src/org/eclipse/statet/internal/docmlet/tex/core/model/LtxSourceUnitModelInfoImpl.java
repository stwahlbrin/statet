/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.ast.TexAstInfo;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceElement;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceUnitModelInfo;


@NonNullByDefault
public class LtxSourceUnitModelInfoImpl extends BasicSourceUnitModelInfo implements LtxSourceUnitModelInfo {
	
	
	private final TexSourceElement sourceElement;
	
	private final int minSectionLevel;
	private final int maxSectionLevel;
	
	private final NameAccessSet<TexNameAccess> labels;
	
	private final Map<String, TexCommand> customCommands;
	private final Map<String, TexCommand> customEnvs;
	
	
	LtxSourceUnitModelInfoImpl(final TexAstInfo ast, final TexSourceElement unitElement,
			final int minSectionLevel, final int maxSectionLevel,
			final NameAccessSet<TexNameAccess> labels,
			final Map<String, TexCommand> customCommands, final Map<String, TexCommand> customEnvs) {
		super(ast);
		this.sourceElement= unitElement;
		
		this.minSectionLevel= minSectionLevel;
		this.maxSectionLevel= maxSectionLevel;
		
		this.labels= labels;
		this.customCommands= customCommands;
		this.customEnvs= customEnvs;
	}
	
	
	@Override
	public TexSourceElement getSourceElement() {
		return this.sourceElement;
	}
	
	@Override
	public TexAstInfo getAst() {
		return (TexAstInfo)super.getAst();
	}
	
	
	@Override
	public int getMinSectionLevel() {
		return this.minSectionLevel;
	}
	
	@Override
	public int getMaxSectionLevel() {
		return this.maxSectionLevel;
	}
	
	@Override
	public NameAccessSet<TexNameAccess> getLabels() {
		return this.labels;
	}
	
	@Override
	public Map<String, TexCommand> getCustomCommandMap() {
		return this.customCommands;
	}
	
	@Override
	public Map<String, TexCommand> getCustomEnvMap() {
		return this.customEnvs;
	}
	
}
