/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.runtime.core.util.MessageBuilder;

import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.model.LtxModelProblemConstants;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.impl.BasicProblem;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class LtxProblemModelCheck {
	
	
	private static final int REF_LABEL_LIMIT= 50;
	private static final int BUFFER_SIZE= 100;
	
	
	private SourceUnit sourceUnit= nonNullLateInit();
	private SourceContent sourceContent= nonNullLateInit();
	private IssueRequestor requestor= nonNullLateInit();
	
	private final MessageBuilder messageBuilder= new MessageBuilder();
	private final List<Problem> problemBuffer= new ArrayList<>(BUFFER_SIZE);
	
	private final int levelRefUndefined= Problem.SEVERITY_WARNING;
	
	
	public LtxProblemModelCheck() {
	}
	
	
	public void run(final TexSourceUnit sourceUnit,
			final LtxSourceUnitModelInfo model, final SourceContent content,
			final IssueRequestor requestor) {
		try {
			this.sourceUnit= nonNullAssert(sourceUnit);
			this.sourceContent= nonNullAssert(content);
			this.requestor= nonNullAssert(requestor);
			
			checkLabels(model);
			
			if (this.problemBuffer.size() > 0) {
				this.requestor.acceptProblems(TexModel.LTX_TYPE_ID, this.problemBuffer);
			}
		}
//		catch (final InvocationTargetException e) {}
		finally {
			clear();
		}
	}
	
	@SuppressWarnings("null")
	private void clear() {
		this.problemBuffer.clear();
		this.sourceUnit= null;
		this.sourceContent= null;
		this.requestor= null;
	}
	
	
	private void checkLabels(final LtxSourceUnitModelInfo model) {
		final NameAccessSet<TexNameAccess> labelSet= model.getLabels();
		final List<String> labels= labelSet.getNames();
		ITER_LABELS: for (final String label : labels) {
			if (label != null && label.length() > 0) {
				final ImList<TexNameAccess> accessList= nonNullAssert(labelSet.getAllInUnit(label));
				for (final TexNameAccess access : accessList) {
					if (access.isWriteAccess()) {
						continue ITER_LABELS;
					}
				}
				for (final TexNameAccess access : accessList) {
					final TexAstNode nameNode= access.getNameNode();
					addProblem(this.levelRefUndefined, LtxModelProblemConstants.STATUS12_LABEL_UNDEFINED,
							this.messageBuilder.bind(ProblemMessages.Labels_UndefinedRef_message, access.getDisplayName()),
							nameNode.getStartOffset(), nameNode.getEndOffset() );
				}
			}
		}
	}
	
	protected final void addProblem(final int severity, final int code, final String message,
			int startOffset, int endOffset) {
		if (startOffset < this.sourceContent.getStartOffset()) {
			startOffset= this.sourceContent.getStartOffset();
		}
		if (endOffset > this.sourceContent.getEndOffset()) {
			endOffset= this.sourceContent.getEndOffset();
		}
		this.problemBuffer.add(new BasicProblem(TexModel.LTX_TYPE_ID, severity, code, message,
				startOffset, endOffset ));
		if (this.problemBuffer.size() >= BUFFER_SIZE) {
			this.requestor.acceptProblems(TexModel.LTX_TYPE_ID, this.problemBuffer);
			this.problemBuffer.clear();
		}
	}
	
}
