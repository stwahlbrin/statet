/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.impl.AbstractAstNode;


@NonNullByDefault
public abstract class TexAstNode extends AbstractAstNode
		implements AstNode {
	
	
	protected static final TexAstNode[] NO_CHILDREN= new TexAstNode[0];
	
	
	int status;
	
	@Nullable TexAstNode texParent;
	
	int startOffset;
	int endOffset;
	
	
	TexAstNode() {
	}
	
	TexAstNode(final @Nullable TexAstNode parent, final int startOffset, final int endOffset) {
		this.texParent= parent;
		
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	
	public abstract NodeType getNodeType();
	
	@Override
	public final int getStatusCode() {
		return this.status;
	}
	
	
	public final @Nullable TexAstNode getTexParent() {
		return this.texParent;
	}
	
	@Override
	public @Nullable AstNode getParent() {
		return this.texParent;
	}
	
	@Override
	public abstract TexAstNode getChild(final int index);
	
	
	public abstract void acceptInTex(final TexAstVisitor visitor) throws InvocationTargetException;
	
	public abstract void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException;
	
	
	@Override
	public final int getStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public final int getEndOffset() {
		return this.endOffset;
	}
	
	@Override
	public final int getLength() {
		return this.endOffset - this.startOffset;
	}
	
	
}
