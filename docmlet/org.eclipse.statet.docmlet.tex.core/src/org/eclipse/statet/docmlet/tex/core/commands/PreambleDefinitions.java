/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_PREAMBLE_CONTROLDEF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_PREAMBLE_DOCDEF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_PREAMBLE_MISC;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_PREAMBLE_CONTROLDEF_COMMAND;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_PREAMBLE_CONTROLDEF_ENV;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;


@NonNullByDefault
public interface PreambleDefinitions {
	
	
	TexCommand PREAMBLE_documentclass_COMMAND= new TexCommand(C2_PREAMBLE_DOCDEF,
			"documentclass", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("options", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("class", Parameter.REQUIRED, Parameter.NONE)
			), "Sets the class of the document" );
	TexCommand PREAMBLE_usepackage_COMMAND= new TexCommand(C2_PREAMBLE_DOCDEF,
			"usepackage", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("options", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("package name", Parameter.REQUIRED, Parameter.NONE)
			), "Loads given package into use" );
	
	TexCommand PREAMBLE_title_COMMAND= new TexCommand(C2_PREAMBLE_DOCDEF,
			"title", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("title", Parameter.REQUIRED, Parameter.TITLE)
			), "Sets the title of the document" );
	TexCommand PREAMBLE_author_COMMAND= new TexCommand(C2_PREAMBLE_DOCDEF,
			"author", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("author", Parameter.REQUIRED, Parameter.TITLE)
			), "Sets the author of the document" );
	TexCommand PREAMBLE_date_COMMAND= new TexCommand(C2_PREAMBLE_DOCDEF,
			"date", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("date", Parameter.REQUIRED, Parameter.TITLE)
			), "Sets the date of the document" );
	
	TexCommand PREAMBLE_newcommand_COMMAND= new TexCommand(C3_PREAMBLE_CONTROLDEF_COMMAND,
			"newcommand", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter("command", Parameter.REQUIRED, Parameter.CONTROLWORD),
					new Parameter("number of arguments", Parameter.OPTIONAL, Parameter.NUM),
					new Parameter("default for 1st argument", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("definition", Parameter.REQUIRED, Parameter.NONE)
			), "Defines a new command" );
	TexCommand PREAMBLE_renewcommand_COMMAND= new TexCommand(C3_PREAMBLE_CONTROLDEF_COMMAND,
			"renewcommand", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter("command", Parameter.REQUIRED, Parameter.CONTROLWORD),
					new Parameter("number of arguments", Parameter.OPTIONAL, Parameter.NUM),
					new Parameter("default for 1st argument", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("definition", Parameter.REQUIRED, Parameter.NONE)
			), "Redefines a command" );
	TexCommand PREAMBLE_providecommand_COMMAND= new TexCommand(C3_PREAMBLE_CONTROLDEF_COMMAND,
			"providecommand", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter("command", Parameter.REQUIRED, Parameter.CONTROLWORD),
					new Parameter("number of arguments", Parameter.OPTIONAL, Parameter.NUM),
					new Parameter("default for 1st argument", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("definition", Parameter.REQUIRED, Parameter.NONE)
					), "Defines a new command if not yet exists" );
	TexCommand PREAMBLE_newenvironment_COMMAND= new TexCommand(C3_PREAMBLE_CONTROLDEF_ENV,
			"newenvironment", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter("environment name", Parameter.REQUIRED, Parameter.CONTROLWORD),
					new Parameter("number of arguments", Parameter.OPTIONAL, Parameter.NUM),
					new Parameter("default for 1st argument", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("definition for begin", Parameter.REQUIRED, Parameter.NONE),
					new Parameter("definition for end", Parameter.REQUIRED, Parameter.NONE)
			), "Defines a new environment" );
	TexCommand PREAMBLE_renewenvironment_COMMAND= new TexCommand(C3_PREAMBLE_CONTROLDEF_ENV,
			"renewenvironment", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter("environment name", Parameter.REQUIRED, Parameter.CONTROLWORD),
					new Parameter("number of arguments", Parameter.OPTIONAL, Parameter.NUM),
					new Parameter("default for 1st argument", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("definition for begin", Parameter.REQUIRED, Parameter.NONE),
					new Parameter("definition for end", Parameter.REQUIRED, Parameter.NONE)
			), "Redefines a environment" );
	TexCommand PREAMBLE_ensuremath_COMMAND= new TexCommand(C2_PREAMBLE_CONTROLDEF,
			"ensuremath", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("definition", Parameter.REQUIRED, Parameter.NONE)
			), "Ensures math-mode for given definition" );
	
	TexCommand PREAMBLE_insertonly_COMMAND= new TexCommand(C2_PREAMBLE_MISC,
			"insertonly", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("file list", Parameter.REQUIRED, Parameter.RESOURCE_LIST)
			), "Specifies which files will be included by \\include" );
	TexCommand PREAMBLE_hyphenation_COMMAND= new TexCommand(C2_PREAMBLE_MISC,
			"hyphenation", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("word list", Parameter.REQUIRED, Parameter.NONE)
			), "Defines hyphenation for given words" );
	
}
