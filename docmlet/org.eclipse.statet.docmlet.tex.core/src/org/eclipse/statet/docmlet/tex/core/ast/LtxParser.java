/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_ENV_MISSING_NAME;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_ENV_NOT_OPENED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_GROUP_NOT_CLOSED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_GROUP_NOT_OPENED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_VERBATIM_INLINE_C_MISSING;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_VERBATIM_INLINE_NOT_CLOSED;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE1_RUNTIME_ERROR;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.string.BasicStringFactory;
import org.eclipse.statet.jcommons.string.StringFactory;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.commands.EnvDefinitions;
import org.eclipse.statet.docmlet.tex.core.commands.LtxCommandDefinitions;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;
import org.eclipse.statet.docmlet.tex.core.commands.TexEmbedCommand;
import org.eclipse.statet.docmlet.tex.core.parser.CustomScanner;
import org.eclipse.statet.docmlet.tex.core.parser.LtxLexer;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.StatusDetail;


@NonNullByDefault
public class LtxParser {
	
	
	private static final byte ST_ROOT=                      0;
	private static final byte ST_CURLY=                     1;
	private static final byte ST_SQUARED=                   2;
	private static final byte ST_BEGINEND=                  3;
	private static final byte ST_MATHROUND=                 4;
	private static final byte ST_MATHSQUARED=               5;
	private static final byte ST_MATHDOLLAR=                6;
	private static final byte ST_MATHDOLLARDOLLAR=          7;
	
	private static final Object LABEL_TEXT= new Object() {
		@Override
		public String toString() {
			return "LABEL"; //$NON-NLS-1$
		}
	};
	private static final Object NUM_TEXT= new Object() {
		@Override
		public String toString() {
			return "NUM"; //$NON-NLS-1$
		}
	};
	
	
	public static class TaskConfig {
		
		public TexCommandSet commandSet;
		
		public @Nullable Map<String, TexCommand> additionalCommands;
		public @Nullable Map<String, TexCommand> additionalEnvs;
		
		public TaskConfig(final TexCommandSet commandSet) {
			this.commandSet= nonNullAssert(commandSet);
		}
		
	}
	
	public static final int COLLECT_COMMENTS= 1 << 0;
	
	
	private static @Nullable TexCommand getCommand(final @Nullable Map<String, TexCommand> map,
			final String key) {
		return (map != null) ? map.get(key) : null;
	}
	
	
	private final LtxLexer lexer;
	
	private TextParserInput parseInput;
	private int parseStartOffset;
	private int parseEndOffset;
	private @Nullable AstNode parseParent;
	
	private TexCommandSet commandSet;
	
	private @Nullable Map<String, TexCommand> customCommands;
	private @Nullable Map<String, TexCommand> customEnvs;
	private @Nullable Map<String, TexCommand> additionalCommands;
	private @Nullable Map<String, TexCommand> additionalEnvs;
	
	private byte[] stackTypes= new byte[32];
	private @Nullable Object[] stackEndKeys= new @Nullable Object[32];
	private int stackPos= -1;
	private int foundEndStackPos;
	private int foundEndOffset;
	private @Nullable TexAstNode foundEndNode;
	private boolean inMath;
	private final Text whitespace= new Text(null, -1, -1);
	private boolean wasLinebreak;
	
	private int commentsLevel;
	private final List<Comment> comments= new ArrayList<>();
	
	private @Nullable List<Embedded> embeddedList;
	
	private final StringFactory symbolTextFactory;
	private final StringFactory otherTextFactory;
	
	
	public LtxParser(final @Nullable LtxLexer lexer,
			final @Nullable StringFactory symbolTextFactory) {
		this.lexer= (lexer != null) ? lexer : new LtxLexer();
		this.lexer.setReportSquaredBrackets(true);
		if (symbolTextFactory != null) {
			this.symbolTextFactory= symbolTextFactory;
			this.otherTextFactory= BasicStringFactory.INSTANCE;
		}
		else {
			this.symbolTextFactory= BasicStringFactory.INSTANCE;
			this.otherTextFactory= BasicStringFactory.INSTANCE;
		}
	}
	
	public LtxParser() {
		this(null, null);
	}
	
	
	public void setCommentLevel(int level) {
		if ((level & COLLECT_COMMENTS) == 0) {
			level= 0;
		}
		else {
			level= (level & (COLLECT_COMMENTS));
		}
		this.commentsLevel= level;
	}
	
	
	private @Nullable TexCommand getCommand(final String controlWord) {
		if (controlWord.equals("end")) { //$NON-NLS-1$
			return LtxCommandDefinitions.GENERICENV_end_COMMAND;
		}
		TexCommand command;
		if (this.inMath) {
			command= this.commandSet.getLtxMathCommandMap().get(controlWord);
		}
		else {
			command= this.commandSet.getLtxPreambleCommandMap().get(controlWord);
			if (command == null) {
				command= this.commandSet.getLtxTextCommandMap().get(controlWord);
			}
		}
		if (command == null) {
			command= getCommand(this.customCommands, controlWord);
			if (command == null) {
				command= getCommand(this.additionalCommands, controlWord);
			}
		}
		return command;
	}
	
	private @Nullable TexCommand getEnv(final String name) {
		TexCommand command= this.commandSet.getLtxInternEnvMap().get(name);
		if (command == null) {
			if (this.inMath) {
				command= this.commandSet.getLtxMathEnvMap().get(name);
			}
			else {
				command= this.commandSet.getLtxTextEnvMap().get(name);
			}
			if (command == null) {
				command= getCommand(this.customEnvs, name);
				if (command == null) {
					command= getCommand(this.additionalEnvs, name);
				}
			}
		}
		return command;
	}
	
	
	public void setCollectEmebeddedNodes(final boolean enable) {
		this.embeddedList= (enable) ? new ArrayList<>(32) : null;
	}
	
	public @Nullable List<Embedded> getEmbeddedNodes() {
		return this.embeddedList;
	}
	
	public @Nullable Map<String, TexCommand> getCustomCommandMap() {
		return this.customCommands;
	}
	
	public @Nullable Map<String, TexCommand> getCustomEnvMap() {
		return this.customEnvs;
	}
	
	
	private void init0(final TextParserInput input, final @Nullable AstNode parent) {
		this.parseInput= nonNullAssert(input);
		this.parseStartOffset= input.getStartIndex();
		this.parseEndOffset= input.getStopIndex();
		this.parseParent= parent;
	}
	
	@SuppressWarnings("null")
	private void clear0() {
		this.parseInput= null;
		this.parseParent= null;
	}
	
	private void logParseError(final Exception e) {
		CommonsRuntime.log(new ErrorStatus(TexCore.BUNDLE_ID,
				"An error occured while parsing TeX source code. Input:\n"
						+ this.parseInput.toString(),
				e ));
	}
	
	private void initTask(final TextParserInput input, final TaskConfig config) {
		if (this.commentsLevel != 0) {
			this.comments.clear();
		}
		if (this.embeddedList != null) {
			this.embeddedList.clear();
		}
		this.commandSet= config.commandSet;
		this.additionalCommands= config.additionalCommands;
		this.additionalEnvs= config.additionalEnvs;
		this.customCommands= null;
		this.customEnvs= null;
		this.lexer.reset(input);
		this.foundEndStackPos= -1;
		this.stackPos= -1;
		this.inMath= false;
		
		this.lexer.setReport$$(true);
		this.lexer.setReportAsterisk(false);
		this.lexer.setReportSquaredBrackets(true);
	}
	
	public SourceComponent parseSourceUnit(final TextParserInput input,
			final TexCommandSet commandSet) {
		init0(input, null);
		try {
			initTask(input, new TaskConfig(commandSet));
			
			final SourceComponent sourceNode= new SourceComponent(null,
					this.parseStartOffset, this.parseEndOffset );
			
			parseInput(sourceNode);
			
			return sourceNode;
		}
		catch (final Exception e) {
			logParseError(e);
			return createErrorSourceComponent();
		}
		finally {
			clear0();
		}
	}
	
	public SourceComponent parseSourceFragment(final TextParserInput input,
			final @Nullable AstNode parent,
			final TexCommandSet commandSet) {
		init0(input, parent);
		try {
			initTask(input, new TaskConfig(commandSet));
			
			final SourceComponent sourceNode= new SourceComponent(parent,
					this.parseStartOffset, this.parseEndOffset );
			
			parseInput(sourceNode);
			
			return sourceNode;
		}
		catch (final Exception e) {
			logParseError(e);
			return createErrorSourceComponent();
		}
		finally {
			clear0();
		}
	}
	
	private void parseInput(final SourceComponent node) {
		if (this.lexer.next() != LtxLexer.EOF) {
			node.startOffset= this.lexer.getOffset();
		}
		putToStack(ST_ROOT, null);
		parseGroup(node);
		if (this.commentsLevel != 0) {
			node.comments= ImCollections.toList(this.comments);
		}
		node.endOffset= this.lexer.getStopOffset();
	}
	
	private SourceComponent createErrorSourceComponent() {
		final int startOffset= this.parseStartOffset;
		int endOffset= this.parseEndOffset;
		if (endOffset < startOffset) {
			endOffset= startOffset;
		}
		final SourceComponent dummy= new SourceComponent(this.parseParent, startOffset, endOffset);
		dummy.status= TYPE1_RUNTIME_ERROR;
		if (this.commentsLevel != 0) {
			dummy.comments= ImCollections.emptyList();
		}
		return dummy;
	}
	
	public ControlNode parseControlWordArgs(final TextParserInput input, final TexCommand command,
			final boolean expand,
			final TaskConfig config) {
		init0(input, null);
		try {
			initTask(input, config);
			
			this.wasLinebreak= false;
			final ControlNode.Word controlNode= new ControlNode.Word(command.getControlWord());
			controlNode.startOffset= this.parseStartOffset;
			controlNode.endOffset= this.parseStartOffset;
			this.lexer.consume();
			
			parseWord(controlNode, command, false);
			
			if (expand && this.lexer.getOffset() > controlNode.endOffset) {
				controlNode.endOffset= this.lexer.getOffset();
			}
			return controlNode;
		}
		finally {
			clear0();
		}
	}
	
	
	private void putToStack(final byte type, final @Nullable Object key) {
		if (++this.stackPos >= this.stackTypes.length) {
			final int l= this.stackTypes.length + 16;
			this.stackTypes= Arrays.copyOf(this.stackTypes, l);
			this.stackEndKeys= Arrays.copyOf(this.stackEndKeys, l);
		}
		this.stackTypes[this.stackPos]= type;
		this.stackEndKeys[this.stackPos]= key;
	}
	
	private void putToStack(final byte type, final byte argContent) {
		switch (argContent & 0xf0) {
		case Parameter.CONTROLWORD:
		case Parameter.LABEL:
			putToStack(type, LABEL_TEXT);
			break;
		case Parameter.NUM:
			putToStack(type, NUM_TEXT);
			break;
		default:
			putToStack(type, null);
			break;
		}
	}
	
	private void parseGroup(final ContainerNode group) {
		final List<TexAstNode> children= new ArrayList<>();
		Text textNode= null;
		
		GROUP: while (this.foundEndStackPos < 0) {
			switch (this.lexer.pop()) {
			case LtxLexer.EOF:
				group.endOffset= this.lexer.getStopOffset();
				if (this.stackTypes[this.stackPos] != ST_ROOT) {
					group.status= TYPE123_GROUP_NOT_CLOSED;
				}
				this.foundEndStackPos= 0;
				this.foundEndNode= null;
				break GROUP;
			case LtxLexer.CONTROL_WORD:
				{	final TexAstNode node= createAndParseWord();
					if (node != null) {
						node.texParent= group;
						children.add(node);
						textNode= null;
						continue GROUP;
					}
					else {
						break GROUP;
					}
				}
			case LtxLexer.CONTROL_CHAR:
				this.wasLinebreak= false;
				{	final ControlNode.Char node= new ControlNode.Char(
						this.lexer.getText(this.symbolTextFactory) );
					node.startOffset= this.lexer.getOffset();
					node.endOffset= this.lexer.getStopOffset();
					if (this.inMath) {
						if (node.getText() == ")") { //$NON-NLS-1$
							int endPos= this.stackPos;
							while (endPos >= 0) {
								if (this.stackTypes[endPos] == ST_MATHROUND) {
									node.command= EnvDefinitions.ENV_math_END_SHORTHAND;
									this.foundEndStackPos= endPos;
									this.foundEndNode= node;
									this.lexer.consume();
									break GROUP;
								}
								endPos--;
							}
						}
						else if (node.getText() == "]") { //$NON-NLS-1$
							int endPos= this.stackPos;
							while (endPos >= 0) {
								if (this.stackTypes[endPos] == ST_MATHSQUARED) {
									node.command= EnvDefinitions.ENV_displaymath_END_SHORTHAND;
									this.foundEndStackPos= endPos;
									this.foundEndNode= node;
									this.lexer.consume();
									break GROUP;
								}
								endPos--;
							}
						}
						node.texParent= group;
						children.add(node);
						this.lexer.consume();
						textNode= null;
						continue GROUP;
					}
					else {
						if (node.getText() == "(") { //$NON-NLS-1$
							final Environment env= new Environment.MathLatexShorthand(group, node);
							children.add(env);
							this.lexer.consume();
							node.command= EnvDefinitions.ENV_math_BEGIN_SHORTHAND;
							node.texParent= env;
							putToStack(ST_MATHROUND, null);
							this.inMath= true;
							parseGroup(env);
							this.inMath= false;
							textNode= null;
							continue GROUP;
						}
						else if (node.getText() == "[") { //$NON-NLS-1$
							final Environment env= new Environment.MathLatexShorthand(group, node);
							children.add(env);
							this.lexer.consume();
							node.command= EnvDefinitions.ENV_displaymath_BEGIN_SHORTHAND;
							node.texParent= env;
							putToStack(ST_MATHSQUARED, null);
							this.inMath= true;
							parseGroup(env);
							this.inMath= false;
							textNode= null;
							continue GROUP;
						}
						node.texParent= group;
						children.add(node);
						this.lexer.consume();
						textNode= null;
						continue GROUP;
					}
				}
			case LtxLexer.CURLY_BRACKET_OPEN:
				this.wasLinebreak= false;
				{	final Group node= new Group.Bracket(group, this.lexer.getOffset(), this.lexer.getStopOffset());
					node.startOffset= this.lexer.getOffset();
					node.endOffset= this.lexer.getStopOffset();
					children.add(node);
					this.lexer.consume();
					putToStack(ST_CURLY, null);
					parseGroup(node);
					node.texParent= group;
					textNode= null;
					continue GROUP;
				}
			case LtxLexer.CURLY_BRACKET_CLOSE:
				this.wasLinebreak= false;
				if (this.stackTypes[this.stackPos] == ST_CURLY) {
					this.foundEndStackPos= this.stackPos;
					this.foundEndOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					textNode= null;
					break GROUP;
				}
				else {
					final Dummy node= new Dummy();
					node.status= TYPE123_GROUP_NOT_OPENED;
					node.startOffset= this.lexer.getOffset();
					node.endOffset= this.lexer.getStopOffset();
					node.texParent= group;
					children.add(node);
					this.lexer.consume();
					textNode= null;
					continue GROUP;
				}
			case LtxLexer.SQUARED_BRACKET_CLOSE:
				this.wasLinebreak= false;
				if (this.stackTypes[this.stackPos] == ST_SQUARED) {
					this.foundEndStackPos= this.stackPos;
					this.foundEndOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					break GROUP;
				}
				if (textNode == null) {
					textNode= new Text(group, this.lexer.getOffset(), this.lexer.getStopOffset());
					children.add(textNode);
					this.lexer.consume();
					continue GROUP;
				}
				else if (textNode == this.whitespace) {
					textNode= new Text(group, textNode.startOffset, this.lexer.getStopOffset());
					children.add(textNode);
					this.lexer.consume();
					continue GROUP;
				}
				else {
					textNode.endOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					continue GROUP;
				}
			case LtxLexer.WHITESPACE:
				if (textNode == null) {
					if (children.size() > 0) {
						textNode= this.whitespace;
						textNode.startOffset= this.lexer.getOffset();
						this.lexer.consume();
						continue GROUP;
					}
					this.lexer.consume();
					continue GROUP;
				}
				else if (textNode == this.whitespace) {
					this.lexer.consume();
					continue GROUP;
				}
				else {
					textNode.endOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					continue GROUP;
				}
			case LtxLexer.DEFAULT_TEXT:
				this.wasLinebreak= false;
				if (children.isEmpty()) {
					if (this.stackEndKeys[this.stackPos] == LABEL_TEXT) {
						final Label node= new Label(group, this.lexer.getOffset(), this.lexer.getStopOffset(),
								this.lexer.getFullText(this.symbolTextFactory) );
						node.startOffset= this.lexer.getOffset();
						node.endOffset= this.lexer.getStopOffset();
						node.texParent= group;
						children.add(node);
						this.lexer.consume();
						continue GROUP;
					}
					if (this.stackEndKeys[this.stackPos] == NUM_TEXT) {
						final Text node= new Text.Num(group, this.lexer.getOffset(), this.lexer.getStopOffset(),
								checkNum(this.lexer.getFullText(this.otherTextFactory)) );
						node.startOffset= this.lexer.getOffset();
						node.endOffset= this.lexer.getStopOffset();
						node.texParent= group;
						children.add(node);
						this.lexer.consume();
						continue GROUP;
					}
				}
				if (textNode == null) {
					textNode= new Text(group, this.lexer.getOffset(), this.lexer.getStopOffset());
					children.add(textNode);
					this.lexer.consume();
					continue GROUP;
				}
				else if (textNode == this.whitespace) {
					textNode= new Text(group, textNode.startOffset, this.lexer.getStopOffset());
					children.add(textNode);
					this.lexer.consume();
					continue GROUP;
				}
				else {
					textNode.endOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					continue GROUP;
				}
			case LtxLexer.CONTROL_NONE:
			case LtxLexer.SQUARED_BRACKET_OPEN:
				this.wasLinebreak= false;
				if (textNode == null) {
					textNode= new Text(group, this.lexer.getOffset(), this.lexer.getStopOffset());
					children.add(textNode);
					this.lexer.consume();
					continue GROUP;
				}
				else {
					textNode.endOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					continue GROUP;
				}
			case LtxLexer.LINEBREAK:
				this.wasLinebreak= true;
				this.lexer.consume();
				textNode= null;
				continue GROUP;
			case LtxLexer.MATH_$:
				this.wasLinebreak= false;
				if (this.inMath) {
					int endPos= this.stackPos;
					while (endPos >= 0) {
						if (this.stackTypes[endPos] == ST_MATHDOLLAR) {
							this.foundEndStackPos= endPos;
							this.foundEndOffset= this.lexer.getStopOffset();
							this.lexer.consume();
							break GROUP;
						}
						endPos--;
					}
					if (textNode == null || textNode == this.whitespace) {
						textNode= new Text(group, this.lexer.getOffset(), this.lexer.getStopOffset());
						children.add(textNode);
						this.lexer.consume();
						continue GROUP;
					}
					textNode.endOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					continue GROUP;
				}
				else {
					final Math node= new Math.SingleDollar(group, this.lexer.getOffset(), this.lexer.getStopOffset());
					children.add(node);
					this.lexer.consume();
					putToStack(ST_MATHDOLLAR, null);
					this.inMath= true;
					this.lexer.setReport$$(false);
					parseGroup(node);
					this.inMath= false;
					this.lexer.setReport$$(true);
					textNode= null;
					continue GROUP;
				}
			case LtxLexer.MATH_$$:
				this.wasLinebreak= false;
				if (this.inMath) {
					int endPos= this.stackPos;
					while (endPos >= 0) {
						if (this.stackTypes[endPos] == ST_MATHDOLLARDOLLAR) {
							this.foundEndStackPos= endPos;
							this.foundEndOffset= this.lexer.getStopOffset();
							this.lexer.consume();
							break GROUP;
						}
						endPos--;
					}
					group.endOffset= this.lexer.getStopOffset();
					if (textNode == null || textNode == this.whitespace) {
						textNode= new Text(group, this.lexer.getOffset(), this.lexer.getStopOffset());
						children.add(textNode);
						this.lexer.consume();
						continue GROUP;
					}
					textNode.endOffset= this.lexer.getStopOffset();
					this.lexer.consume();
					continue GROUP;
				}
				else {
					final Math node= new Math.DoubleDollar(group, this.lexer.getOffset(), this.lexer.getStopOffset());
					children.add(node);
					this.lexer.consume();
					putToStack(ST_MATHDOLLARDOLLAR, null);
					this.inMath= true;
					parseGroup(node);
					this.inMath= false;
					textNode= null;
					continue GROUP;
				}
			case LtxLexer.LINE_COMMENT:
				handleComment();
				continue GROUP;
			case LtxLexer.VERBATIM_TEXT:
				this.wasLinebreak= false;
				{	final Verbatim node= new Verbatim();
					node.texParent= group;
					node.startOffset= this.lexer.getOffset();
					node.endOffset= this.lexer.getStopOffset();
					children.add(node);
					this.lexer.consume();
					textNode= null;
					continue GROUP;
				}
			case LtxLexer.EMBEDDED:
				this.wasLinebreak= false;
				{	final Embedded node= new Embedded(group, this.lexer.getOffset(), this.lexer.getStopOffset(),
							this.lexer.getText().intern() );
					children.add(node);
					this.lexer.consume();
					if (this.embeddedList != null) {
						this.embeddedList.add(node);
					}
					textNode= null;
					continue GROUP;
				}
			case LtxLexer.ASTERISK:
			default:
				throw new IllegalStateException("type= " + this.lexer.getType() + ", offset= "+this.lexer.getOffset()); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		
		if (!children.isEmpty()) {
			group.children= children.toArray(new TexAstNode[children.size()]);
		}
		
//		if (this.foundEndStackPos >= 0) {
			if (this.foundEndStackPos == this.stackPos) {
				group.setEndNode(this.foundEndOffset, this.foundEndNode);
				this.foundEndStackPos= -1;
			}
			else {
				group.setMissingEnd();
			}
//		}
		this.stackPos--;
	}
	
	private @Nullable TexAstNode createAndParseWord() {
		this.wasLinebreak= false;
		final String label= this.lexer.getText(this.symbolTextFactory);
		final ControlNode.Word controlNode= new ControlNode.Word(label);
		controlNode.startOffset= this.lexer.getOffset();
		controlNode.endOffset= this.lexer.getStopOffset();
		this.lexer.consume();
		
		return parseWord(controlNode, getCommand(label), true);
	}
	
	private @Nullable TexAstNode parseWord(final ControlNode.Word controlNode,
			@Nullable TexCommand command, final boolean exec) {
		if (command == null) {
			if (this.lexer.pop() == LtxLexer.WHITESPACE) {
				this.lexer.consume();
			}
			return controlNode;
		}
		
		String label= controlNode.getText();
		
		if (command.supportAsterisk()) {
			this.lexer.setReportAsterisk(true);
			ASTERISK: while (true) {
				switch (this.lexer.pop()) {
				case LtxLexer.WHITESPACE:
					this.lexer.consume();
					continue ASTERISK;
				case LtxLexer.ASTERISK:
					this.wasLinebreak= false;
					this.lexer.consume();
					break ASTERISK;
				default:
					break ASTERISK;
				}
			}
			this.lexer.setReportAsterisk(false);
		}
		List<? extends Parameter> parameters;
		if (!(parameters= command.getParameters()).isEmpty()) {
			int nextArg= 0;
			final List<TexAstNode> children= new ArrayList<>();
			ARGUMENTS: while (this.foundEndStackPos < 0 && nextArg < parameters.size()) {
				Parameter parameter= parameters.get(nextArg);
				boolean optional= ((parameter.getType() & Parameter.OPTIONAL) != 0);
				final ContainerNode argNode;
				switch (this.lexer.pop()) {
				case LtxLexer.WHITESPACE:
					this.lexer.consume();
					continue ARGUMENTS;
				case LtxLexer.LINEBREAK:
					if (this.wasLinebreak) {
						break ARGUMENTS;
					}
					this.wasLinebreak= true;
					this.lexer.consume();
					continue ARGUMENTS;
				case LtxLexer.SQUARED_BRACKET_OPEN:
					if (!optional) {
						break ARGUMENTS;
					}
					
					this.wasLinebreak= false;
					argNode= new Group.Square(controlNode, this.lexer.getOffset(), this.lexer.getStopOffset());
					children.add(argNode);
					this.lexer.consume();
					putToStack(ST_SQUARED, parameter.getContent());
					if (parameter.getContent() == Parameter.EMBEDDED) {
						consumeEmbedGroup(argNode, (TexEmbedCommand)command, nextArg);
					}
					else {
						parseGroup(argNode);
					}
					controlNode.endOffset= argNode.endOffset;
					
					nextArg++;
					continue ARGUMENTS;
				case LtxLexer.CURLY_BRACKET_OPEN:
					while (optional) {
						if (++nextArg >= parameters.size()) {
							break ARGUMENTS;
						}
						parameter= parameters.get(nextArg);
						optional= (parameter.getType() == Parameter.OPTIONAL);
					}
					if (parameter.getType() != Parameter.REQUIRED) {
						break ARGUMENTS;
					}
					
					this.wasLinebreak= false;
					argNode= new Group.Bracket(controlNode, this.lexer.getOffset(), this.lexer.getStopOffset());
					children.add(argNode);
					this.lexer.consume();
					putToStack(ST_CURLY, parameter.getContent());
					if (parameter.getContent() == Parameter.EMBEDDED) {
						consumeEmbedGroup(argNode, (TexEmbedCommand)command, nextArg);
					}
					else {
						parseGroup(argNode);
					}
					controlNode.endOffset= argNode.endOffset;
					
					if (command.getType() == TexCommand.C2_GENERICENV_BEGIN) {
						if (argNode.status == 0 && argNode.children.length == 1
								&& argNode.children[0].getNodeType() == NodeType.LABEL) {
							final TexCommand envCommand= getEnv(
									label= argNode.children[0].getText() );
							if (envCommand != null) {
								command= envCommand;
								parameters= command.getParameters();
							}
						}
						else {
							break ARGUMENTS;
						}
					}
					
					nextArg++;
					continue ARGUMENTS;
				default:
					break ARGUMENTS;
				}
			}
			
			controlNode.arguments= children.toArray(new TexAstNode[children.size()]);
		}
		
		controlNode.command= command;
		
		if (!exec || this.foundEndStackPos >= 0) {
			return controlNode;
		}
		
		switch (command.getType() & TexCommand.MASK_C2) {
		case TexCommand.C2_ENV_VERBATIM_BEGIN:
		case TexCommand.C2_ENV_COMMENT_BEGIN:
			if (this.lexer.getType() != 0) {
				break;
			}
			this.lexer.setModeVerbatimEnv(("end{" + label + "}").toCharArray()); //$NON-NLS-1$ //$NON-NLS-2$
			if (this.lexer.next() == LtxLexer.VERBATIM_TEXT) {
				final Environment envNode= new Environment.Word(null, controlNode);
				putToStack(ST_BEGINEND, label);
				parseGroup(envNode);
				return envNode;
			}
			else {
				throw new IllegalStateException();
			}
		case TexCommand.VERBATIM_INLINE:
			if (this.lexer.getType() != 0) {
				break;
			}
			this.lexer.setModeVerbatimLine();
			if (this.lexer.next() == LtxLexer.VERBATIM_TEXT) {
				this.wasLinebreak= false;
				final TexAstNode verbatimNode;
				final StatusDetail statusDetail;
				switch (this.lexer.getFlags()) {
				case LtxLexer.SUB_OPEN_MISSING:
//					verbatimNode= new Dummy();
//					verbatimNode.parent= controlNode;
//					verbatimNode.fsatus= STATUS2_VERBATIM_INLINE_C_MISSING;
//					verbatimNode.startOffset= verbatimNode.stopOffset= controlNode.stopOffset=
//							this.lexer.getStopOffset();
					controlNode.status= TYPE123_VERBATIM_INLINE_C_MISSING;
					this.lexer.consume();
					return controlNode;
				case LtxLexer.SUB_CLOSE_MISSING:
					verbatimNode= new Verbatim();
					verbatimNode.texParent= controlNode;
					verbatimNode.status= TYPE123_VERBATIM_INLINE_NOT_CLOSED;
					verbatimNode.startOffset= this.lexer.getOffset() + 1;
					verbatimNode.endOffset= controlNode.endOffset= this.lexer.getStopOffset();
					statusDetail= this.lexer.getStatusDetail();
					if (statusDetail != null) {
						verbatimNode.addAttachment(statusDetail);
					}
					this.lexer.consume();
					break;
				default:
					verbatimNode= new Verbatim();
					verbatimNode.texParent= controlNode;
					verbatimNode.startOffset= this.lexer.getOffset() + 1;
					verbatimNode.endOffset= controlNode.endOffset= this.lexer.getStopOffset() - 1;
					this.lexer.consume();
					break;
				}
				controlNode.arguments= new TexAstNode[] { verbatimNode };
				
				return controlNode;
			}
			else {
				throw new IllegalStateException();
			}
		case TexCommand.C2_ENV_MATH_BEGIN:
			{
				final Environment envNode= new Environment.Word(null, controlNode);
				controlNode.texParent= envNode;
				putToStack(ST_BEGINEND, label);
				this.inMath= true;
				parseGroup(envNode);
				this.inMath= false;
				return envNode;
			}
		case TexCommand.C2_GENERICENV_BEGIN:
		case TexCommand.C2_ENV_DOCUMENT_BEGIN:
		case TexCommand.C2_ENV_ELEMENT_BEGIN:
		case TexCommand.C2_ENV_MATHCONTENT_BEGIN:
		case TexCommand.C2_ENV_OTHER_BEGIN:
			if (label != null && !label.equals("begin")) { //$NON-NLS-1$
				final Environment envNode= new Environment.Word(null, controlNode);
				controlNode.texParent= envNode;
				putToStack(ST_BEGINEND, label);
				parseGroup(envNode);
				return envNode;
			}
			else {
				controlNode.status= TYPE123_ENV_MISSING_NAME;
			}
			return controlNode;
		case TexCommand.C2_GENERICENV_END:
			if (controlNode.arguments.length == 1) {
				final Group argNode= (Group)controlNode.arguments[0];
				if (argNode.status == 0 && argNode.children.length == 1
						&& argNode.children[0].getNodeType() == NodeType.LABEL) {
					label= argNode.children[0].getText();
					if (label != null) {
						int endPos= this.stackPos;
						while (endPos >= 0) {
							if (label.equals(this.stackEndKeys[endPos])) {
								this.foundEndStackPos= endPos;
								this.foundEndNode= controlNode;
								return null;
							}
							endPos--;
						}
					}
//					if (this.stackEndKeys[this.stackPos].length() > 1) {
//						this.foundEndStackPos= this.stackPos;
//						this.foundEndNode= controlNode;
//						return null;
//					}
					controlNode.status= TYPE123_ENV_NOT_OPENED;
				}
			}
			controlNode.status= TYPE123_ENV_MISSING_NAME;
			return controlNode;
		case TexCommand.C2_PREAMBLE_CONTROLDEF:
			if (controlNode.arguments.length > 0) {
				parseDef(controlNode, command);
			}
			return controlNode;
		default:
			break;
		}
		return controlNode;
	}
	
	private @Nullable String checkNum(final String text) {
		if (text.isEmpty()) {
			return null;
		}
		for (int i= 0; i < text.length(); i++) {
			final char c= text.charAt(i);
			if (c < '0' || c > '9') {
				return null;
			}
		}
		return text;
	}
	
	private void parseDef(final ControlNode node, final TexCommand command) {
		final int type;
		switch ((command.getType() & TexCommand.MASK_C3)) {
		case TexCommand.C3_PREAMBLE_CONTROLDEF_COMMAND:
			type= 0;
			break;
		case TexCommand.C3_PREAMBLE_CONTROLDEF_ENV:
			type= 1;
			break;
		default:
			return;
		}
		
		final TexAstNode[] argNodes= TexAsts.resolveArguments(node);
		TexAstNode argNode;
		if (argNodes[0] == null || argNodes[0].status != 0 || argNodes[0].getChildCount() != 1) {
			return;
		}
		argNode= argNodes[0].getChild(0);
		final String controlWord;
		switch (argNode.getNodeType()) {
		case LABEL:
			if (type == 1
					&& !(controlWord= ((Label)argNode).getText()).isEmpty()) {
				break;
			}
			return;
		case CONTROL:
			if (!(controlWord= ((ControlNode)argNode).getText()).isEmpty()) {
				break;
			}
			return;
		default:
			return;
		}
		int optionalArgs= 0;
		int requiredArgs= 0;
		if (argNodes[1] != null && argNodes[1].status == 0 && argNodes[1].getChildCount() == 1
				&& (argNode= argNodes[1].getChild(0)).getNodeType() == NodeType.TEXT
				&& argNode.getText() != null) {
			try {
				requiredArgs= Integer.parseInt(argNode.getText());
			}
			catch (final NumberFormatException e) {}
		}
		if (argNodes[2] != null && argNodes[2].status == 0) {
			optionalArgs= 1;
			requiredArgs-= 1;
		}
		if (requiredArgs < 0) {
			requiredArgs= 0;
		}
		final var args= new @NonNull Parameter[optionalArgs + requiredArgs];
		{	int i= 0;
			while (optionalArgs-- > 0) {
				args[i++]= new Parameter(Parameter.OPTIONAL, Parameter.NONE);
			}
			while (requiredArgs-- > 0) {
				args[i++]= new Parameter(Parameter.REQUIRED, Parameter.NONE);
			}
		}
		{	Map<String, TexCommand> map;
			if (type == 1) {
				map= this.customEnvs;
				if (map == null) {
					map= new HashMap<>();
					this.customEnvs= map;
				}
			}
			else {
				map= this.customCommands;
				if (map == null) {
					map= new HashMap<>();
					this.customCommands= map;
				}
			}
			map.put(controlWord, new TexCommand(0, controlWord, false,
					ImCollections.newList(args), "(custom)"));
		}
	}
	
	private void handleComment() {
		if (this.commentsLevel > 0) {
			final var commentNode= new Comment();
			commentNode.startOffset= this.lexer.getOffset();
			commentNode.endOffset= this.lexer.getStopOffset();
			this.comments.add(commentNode);
		}
		this.wasLinebreak= false;
		this.lexer.consume();
	}
	
	private void consumeEmbedGroup(final ContainerNode group, final TexEmbedCommand command,
			final int argIdx) {
		final Embedded embedded= new Embedded.Inline(group, this.lexer.getStopOffset(),
				command.getEmbeddedType(argIdx) );
		
		this.wasLinebreak= false;
		
		final CustomScanner scanner= command.getArgumentScanner(argIdx);
		byte s= (scanner != null) ? scanner.consume(this.lexer) : consumeEmbeddedDefault();
		if (s == 0) {
			s= this.lexer.pop();
		}
		
		switch (s) {
		case LtxLexer.EOF:
			this.foundEndStackPos= 0;
			this.foundEndNode= null;
			break;
		case LtxLexer.LINEBREAK:
			break;
		case LtxLexer.CURLY_BRACKET_CLOSE:
			if (this.stackTypes[this.stackPos] == ST_CURLY) {
				this.foundEndStackPos= this.stackPos;
				this.foundEndOffset= this.lexer.getStopOffset();
			}
			break;
		case LtxLexer.SQUARED_BRACKET_CLOSE:
			if (this.stackTypes[this.stackPos] == ST_SQUARED) {
				this.foundEndStackPos= this.stackPos;
				this.foundEndOffset= this.lexer.getStopOffset();
			}
			break;
		default:
			break;
		}
		
		embedded.endOffset= this.lexer.getOffset();
		group.children= new TexAstNode[] { embedded };
		if (this.embeddedList != null) {
			this.embeddedList.add(embedded);
		}
		
		if (this.foundEndStackPos == this.stackPos) {
			group.setEndNode(this.foundEndOffset, this.foundEndNode);
			this.foundEndStackPos= -1;
		}
		else {
			group.setMissingEnd();
		}
		this.stackPos--;
	}
	
	
	public byte consumeEmbeddedDefault() {
		final TextParserInput input= this.lexer.getInput();
		this.lexer.consume(true);
		
		final int endChar;
		final byte endReturn;
		switch (this.stackTypes[this.stackPos]) {
		case ST_SQUARED:
			endChar= ']';
			endReturn= LtxLexer.CURLY_BRACKET_CLOSE;
			break;
		case ST_CURLY:
			endChar= '}';
			endReturn= LtxLexer.SQUARED_BRACKET_CLOSE;
			break;
		default:
			throw new IllegalStateException("stateType= " + this.stackTypes[this.stackPos]); //$NON-NLS-1$
		}
		
		int offset= 0;
		while (true) {
			final int c= input.get(offset++);
			if (c < 0x20) {
				input.consume(offset - 1);
				this.lexer.consume(true);
				return 0;
			}
			if (c == endChar) {
				input.consume(offset);
				this.lexer.consume(true);
				return endReturn;
			}
		}
	}
	
}
