/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.tex.core.TexCorePlugin;
import org.eclipse.statet.internal.docmlet.tex.core.WorkspaceAdapterFactory;


@NonNullByDefault
public class TexCore {
	
	public static final String BUNDLE_ID= "org.eclipse.statet.docmlet.tex.core"; //$NON-NLS-1$
	
	
	public static final String LTX_CONTENT_ID= "org.eclipse.statet.docmlet.contentTypes.Ltx"; //$NON-NLS-1$
	
	public static final IContentType LTX_CONTENT_TYPE;
	
	/**
	 * Content type id for LaTeX documents
	 */
	public static final String LTX_CONTENT_ID_NG= "org.eclipse.statet.docmlet.contentTypes.Ltx"; //$NON-NLS-1$
	
	static {
		final IContentTypeManager contentTypeManager= Platform.getContentTypeManager();
		LTX_CONTENT_TYPE= contentTypeManager.getContentType(LTX_CONTENT_ID);
	}
	
	
	private static final TexCoreAccess WORKBENCH_ACCESS= TexCorePlugin.getInstance().getWorkbenchAccess();
	
	public static TexCoreAccess getWorkbenchAccess() {
		return WORKBENCH_ACCESS;
	}
	
	public static TexCoreAccess getDefaultsAccess() {
		return TexCorePlugin.getInstance().getDefaultsAccess();
	}
	
	public static TexCoreAccess getContextAccess(final @Nullable IAdaptable adaptable) {
		if (adaptable != null) {
			TexCoreAccess coreAccess= WorkspaceAdapterFactory.getResourceCoreAccess(adaptable);
			if (coreAccess == null) {
				coreAccess= adaptable.getAdapter(TexCoreAccess.class);
			}
			if (coreAccess != null) {
				return coreAccess;
			}
		}
		return getWorkbenchAccess();
	}
	
}
