/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.model.build;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.core.project.TexProject;
import org.eclipse.statet.internal.docmlet.tex.core.builder.TexTaskTagReporter;
import org.eclipse.statet.internal.docmlet.tex.core.model.AstProblemReporter;
import org.eclipse.statet.internal.docmlet.tex.core.model.LtxProblemModelCheck;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.TaskIssueConfig;


@NonNullByDefault
public class LtxIssueReporter {
	
	
	private final AstProblemReporter astVisitor= new AstProblemReporter();
	private final LtxProblemModelCheck modelCheck= new LtxProblemModelCheck();
	
	private final TexTaskTagReporter taskReporter= new TexTaskTagReporter();
	
	/* explicite configs */
	private @Nullable TaskIssueConfig taskIssueConfig;
	
	private boolean runProblems;
	private boolean runTasks;
	
	
	public LtxIssueReporter() {
	}
	
	
	public void configure(final PreferenceAccess prefs, final @Nullable TexProject config) {
		this.taskIssueConfig= TaskIssueConfig.getConfig(prefs);
	}
	
	
	public void run(final TexSourceUnit sourceUnit,
			final LtxSourceUnitModelInfo model, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		this.runProblems= requestor.isInterestedInProblems(TexModel.LTX_TYPE_ID);
		this.runTasks= requestor.isInterestedInTasks();
		if (!(this.runProblems || this.runTasks)) {
			return;
		}
		
		var taskIssueConfig= this.taskIssueConfig;
		if (taskIssueConfig == null) {
			final var prefs= EPreferences.getContextPrefs(sourceUnit);
			taskIssueConfig= TaskIssueConfig.getConfig(prefs);
		}
		if (this.runTasks) {
			this.taskReporter.configure(taskIssueConfig);
		}
		
		final var ast= model.getAst();
		final var node= (ast.getRoot() instanceof TexAstNode) ? (TexAstNode)ast.getRoot() : null;
		if (this.runProblems) {
			if (node != null) {
				this.astVisitor.run(sourceUnit, node, content, requestor);
			}
			this.modelCheck.run(sourceUnit, model, content, requestor);
		}
		if (this.runTasks) {
			if (node != null) {
				this.taskReporter.run(sourceUnit, node, content, requestor);
			}
		}
	}
	
}
