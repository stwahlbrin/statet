/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.COLLATOR;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class TexPackage implements Comparable<TexPackage> {
	
	
	protected static ImList<TexCommand> NO_COMMANDS= ImCollections.emptyList();
	
	
	private final String name;
	private final String label;
	
	
	public TexPackage(final String name, final String label) {
		this.name= name;
		this.label= label;
	}
	
	
	
	public String getName() {
		return this.name;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	
	public List<TexCommand>	getCommands(final String contextId) {
		return NO_COMMANDS;
	}
	
	public List<TexCommand> getEnvs(final String contextId) {
		return NO_COMMANDS;
	}
	
	public List<TexCommand>	getNestedCommands(final TexCommand env) {
		return NO_COMMANDS;
	}
	
	
	@Override
	public int compareTo(final TexPackage other) {
		return COLLATOR.compare(this.name, other.name);
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
