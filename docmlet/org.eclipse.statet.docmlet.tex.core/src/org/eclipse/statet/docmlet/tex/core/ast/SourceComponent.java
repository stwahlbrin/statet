/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public final class SourceComponent extends ContainerNode {
	
	
	private final @Nullable AstNode parent;
	
	@Nullable ImList<Comment> comments;
	
	
	SourceComponent(final @Nullable AstNode parent,
			final int startOffset, final int endOffset) {
		this.parent= parent;
		
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.SOURCELINES;
	}
	
	
	@Override
	public @Nullable AstNode getParent() {
		return this.parent;
	}
	
	@Override
	public final boolean hasChildren() {
		return (this.children.length > 0);
	}
	
	@Override
	public final int getChildCount() {
		return this.children.length;
	}
	
	@Override
	public final TexAstNode getChild(final int index) {
		return this.children[index];
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		for (int i= 0; i < this.children.length; i++) {
			if (this.children[i] == child) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * The comment nodes in this source component
	 * 
	 * @return the comments or <code>null</code>, if disabled
	 */
	public @Nullable ImList<Comment> getComments() {
		return this.comments;
	}
	
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		for (final TexAstNode child : this.children) {
			visitor.visit(child);
		}
	}
	
	@Override
	public void acceptInTex(final TexAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException {
		for (final TexAstNode child : this.children) {
			child.acceptInTex(visitor);
		}
	}
	
	
	@Override
	void setEndNode(final int endOffset, final TexAstNode endNode) {
		this.endOffset= endOffset;
	}
	
	@Override
	void setMissingEnd() {
		if (this.children.length > 0) {
			this.endOffset= this.children[this.children.length - 1].endOffset;
		}
	}
	
}
