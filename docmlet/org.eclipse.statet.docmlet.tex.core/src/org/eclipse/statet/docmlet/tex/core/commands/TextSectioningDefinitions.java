/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_SECTIONING_CHAPTER;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_SECTIONING_PART;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_SECTIONING_SECTION;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_SECTIONING_SUBSECTION;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_SECTIONING_SUBSUBSECTION;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.SECTIONING;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;


@NonNullByDefault
public interface TextSectioningDefinitions {
	
	
	TexCommand SECTIONING_part_COMMAND= new TexCommand(C2_SECTIONING_PART,
			"part", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter(Parameter.OPTIONAL, Parameter.TITLE),
					new Parameter(Parameter.REQUIRED, Parameter.TITLE)
			), "Starts a new Part" );
	TexCommand SECTIONING_chapter_COMMAND= new TexCommand(C2_SECTIONING_CHAPTER,
			"chapter", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter(Parameter.OPTIONAL, Parameter.TITLE),
					new Parameter(Parameter.REQUIRED, Parameter.TITLE)
			), "Starts a new Chapter" );
	TexCommand SECTIONING_section_COMMAND= new TexCommand(C2_SECTIONING_SECTION,
			"section", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter(Parameter.OPTIONAL, Parameter.TITLE),
					new Parameter(Parameter.REQUIRED, Parameter.TITLE)
			), "Starts a new Section" );
	TexCommand SECTIONING_subsection_COMMAND= new TexCommand(C2_SECTIONING_SUBSECTION,
			"subsection", true, ImCollections.newList(
					new Parameter(Parameter.OPTIONAL, Parameter.TITLE),
					new Parameter(Parameter.REQUIRED, Parameter.TITLE)
			), "Starts a new SubSection" );
	TexCommand SECTIONING_subsubsection_COMMAND= new TexCommand(C2_SECTIONING_SUBSUBSECTION,
			"subsubsection", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter(Parameter.OPTIONAL, Parameter.TITLE),
					new Parameter(Parameter.REQUIRED, Parameter.TITLE)
			), "Starts new SubSubSection" );
	TexCommand SECTIONING_paragraph_COMMAND= new TexCommand(SECTIONING | 0x60,
			"paragraph", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter(Parameter.OPTIONAL, Parameter.TITLE),
					new Parameter(Parameter.REQUIRED, Parameter.TITLE)
			), "Starts a new Paragraph" );
	TexCommand SECTIONING_subparagraph_COMMAND= new TexCommand(SECTIONING | 0x70,
			"subparagraph", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter(Parameter.OPTIONAL, Parameter.TITLE),
					new Parameter(Parameter.REQUIRED, Parameter.TITLE)
			), "Starts a new SubParagraph" );
	
	
}
