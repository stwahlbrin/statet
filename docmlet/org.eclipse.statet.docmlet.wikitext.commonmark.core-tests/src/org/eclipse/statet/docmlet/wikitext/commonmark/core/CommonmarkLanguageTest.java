/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import java.util.List;

import org.eclipse.mylyn.wikitext.parser.Attributes;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;
import org.eclipse.mylyn.wikitext.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.parser.builder.EventDocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.builder.event.BeginBlockEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.BeginDocumentEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.CharactersEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.DocumentBuilderEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.EndBlockEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.EndDocumentEvent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CommonmarkLanguageTest {
	
	
	private final CommonmarkLanguage language= new CommonmarkLanguage();
	
	
	public CommonmarkLanguageTest() {
	}
	
	
	@Test
	public void name() {
		assertEquals("CommonMark\u2002[StatET]", this.language.getName());
	}
	
	@Test
	public void processEmpty() {
		assertEvents("", new BeginDocumentEvent(), new EndDocumentEvent());
	}
	
	@Test
	public void processBlankLines() {
		assertEvents("\n\n\n\n\n", new BeginDocumentEvent(), new EndDocumentEvent());
	}
	
	@Test
	public void processDocumentFalse() {
		assertEvents("", false);
	}
	
	@Test
	public void processSimple() {
		assertEvents("first line\nsecond line\n\nnext para", new BeginDocumentEvent(),
				new BeginBlockEvent(BlockType.PARAGRAPH, new Attributes()), new CharactersEvent("first line"),
				new CharactersEvent("\n"), new CharactersEvent("second line"), new EndBlockEvent(),
				new BeginBlockEvent(BlockType.PARAGRAPH, new Attributes()), new CharactersEvent("next para"),
				new EndBlockEvent(), new EndDocumentEvent());
	}
	
//	@Test
//	public void isDiscoverable() {
//		WikitextMarkupLanguage markupLanguage= OsgiServiceLocator.getApplicableInstance().getMarkupLanguage("CommonMark");
//		assertNotNull(markupLanguage);
//		assertEquals(CommonmarkLanguage.class, markupLanguage.getClass());
//	}
	
	@Test
	public void modeStrictlyConforming() {
		final int mode= 0;
		final CommonmarkLanguage language= this.language;
		assertEquals(mode, language.getMode());
		assertEquals(mode, language.clone().getMode());
		assertNull(language.getIdGenerationStrategy());
		assertContent(language, "<p>one (http://example.com/#hey) two</p>", "one (http://example.com/#hey) two");
		assertContent(language, "<h1>A Heading</h1>", "# A Heading");
	}
	
	@Test
	public void modeCompat() {
		final int mode= CommonmarkLanguage.MYLYN_COMPAT_MODE | CommonmarkLanguage.MARKDOWN_COMPAT_MODE;
		final CommonmarkLanguage language= this.language.clone("Test", mode);
		assertEquals(mode, language.getMode());
		assertEquals(mode, language.clone().getMode());
		assertNotNull(language.getIdGenerationStrategy());
		assertContent(language, "<p>one (<a href=\"http://example.com/#hey\">http://example.com/#hey</a>) two</p>",
				"one (http://example.com/#hey) two");
		assertContent(language, "<h1 id=\"a-heading\">A Heading</h1>", "# A Heading");
	}
	
	@Test
	public void cloneTest() {
		final CommonmarkLanguage language= new CommonmarkLanguage();
		assertNotNull(language.clone());
		assertEquals(language.getName(), language.clone().getName());
		assertEquals(language.getMode(), language.clone().getMode());
	}
	
	@Test
	public void linksWithHash() {
		assertContent("<p><a href=\"#FooBar\">text</a></p>", "[text](#FooBar)");
		assertContent("<p><a href=\"A#FooBar\">text</a></p>", "[text](A#FooBar)");
		assertContent("<p><a href=\"http://example.com/page.html#someId\">text</a></p>",
				"[text](http://example.com/page.html#someId)");
	}
	
	@Test
	public void linksByDef() {
		assertContent("<p><a href=\"/url\" title=\"title\">foo</a></p>", "[foo][bar]\n\n[bar]: /url \"title\"");
		assertContent("<p><a href=\"/url\" title=\"title\">foo</a></p>", "[foo][]\n\n[foo]: /url \"title\"");
		// No link
		assertContent("<p><a href=\"/url\" title=\"title\">foo</a> []</p>", "[foo] []\n\n[foo]: /url \"title\"");
	}
	
	private void assertEvents(final String content,
			final DocumentBuilderEvent... expectedEvents) {
		assertEvents(content, true, expectedEvents);
	}
	
	private void assertEvents(final String content, final boolean asDocument,
			final DocumentBuilderEvent... expectedEvents) {
		final MarkupParser parser= new MarkupParser(this.language);
		final EventDocumentBuilder builder= new EventDocumentBuilder();
		parser.setBuilder(builder);
		parser.parse(content, asDocument);
		final List<DocumentBuilderEvent> actualEvents= builder.getDocumentBuilderEvents().getEvents();
		assertArrayEquals(expectedEvents, (actualEvents != null) ? actualEvents.toArray() : null);
	}
	
}
