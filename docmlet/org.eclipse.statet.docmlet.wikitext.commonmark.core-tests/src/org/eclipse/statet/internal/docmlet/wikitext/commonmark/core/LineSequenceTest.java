/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ConcurrentModificationException;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class LineSequenceTest {
	
	
	public LineSequenceTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void createRequiresContent() {
		assertThrows(NullPointerException.class,
				() -> LineSequence.create((String)null) );
	}
	
	@Test
	public void empty() {
		assertOneLine("", LineSequence.create(""));
	}
	
	@Test
	public void oneLine() {
		assertOneLine("a", LineSequence.create("a"));
	}
	
	@Test
	public void twoLines() {
		assertTwoLines(LineSequence.create("abc\r\ndefg"));
	}
	
	
	@Test
	public void advance() {
		assertAdvance(LineSequence.create("one"));
	}
	
	@Test
	public void lookAhead() {
		assertLookAhead(LineSequence.create("a\nb\nc"));
	}
	
	@Test
	public void lookAheadFailsFast() {
		assertLookAheadFailsFast(LineSequence.create("a\nb\nc"));
	}
	
	private void assertLookAheadFailsFast(final LineSequence lineSequence) {
		final LineSequence lookAhead= lineSequence.lookAhead();
		lineSequence.advance();
		assertThrows(ConcurrentModificationException.class,
				() -> lookAhead.advance() );
	}
	
	private void assertAdvance(final LineSequence lineSequence) {
		lineSequence.advance();
		assertNoLinesRemain(lineSequence);
		lineSequence.advance();
		assertNoLinesRemain(lineSequence);
		lineSequence.advance();
		assertNoLinesRemain(lineSequence);
	}
	
	private void assertNoLinesRemain(final LineSequence lineSequence) {
		assertNull(lineSequence.getCurrentLine());
		assertNull(lineSequence.getNextLine());
	}
	
	private void assertLookAhead(final LineSequence lineSequence) {
		Line line;
		lineSequence.advance();
		line= lineSequence.getCurrentLine();
		assertNotNull(line);
		assertEquals("b", line.getText());
		
		final LineSequence lookAhead= lineSequence.lookAhead();
		assertEquals(line, lookAhead.getCurrentLine());
		lookAhead.advance();
		line= lineSequence.getCurrentLine();
		assertNotNull(line);
		assertEquals("b", line.getText());
		line= lookAhead.getCurrentLine();
		assertNotNull(line);
		assertEquals("c", line.getText());
		
		final LineSequence lookAhead2= lookAhead.lookAhead();
		assertNotNull(lookAhead2);
		assertNotSame(lookAhead, lookAhead2);
		assertNotSame(lookAhead2, lookAhead.lookAhead());
		lookAhead.advance();
		line= lookAhead2.getCurrentLine();
		assertNotNull(line);
		assertEquals("c", line.getText());
		assertNoLinesRemain(lookAhead);
		assertNoLinesRemain(lookAhead.lookAhead());
		
		line= lineSequence.getCurrentLine();
		assertNotNull(line);
		assertEquals("b", line.getText());
		
		lineSequence.advance();
		line= lineSequence.getCurrentLine();
		assertNotNull(line);
		assertEquals("c", line.getText());
	}
	
	private void assertTwoLines(final LineSequence lineSequence) {
		final Line line1= lineSequence.getCurrentLine();
		assertNotNull(line1);
		assertEquals("abc", line1.getText());
		assertEquals(0, line1.getStartOffset());
		assertEquals(0, line1.getLineNumber());
		assertSame(line1, lineSequence.getCurrentLine());
		
		final Line nextLine= lineSequence.getNextLine();
		assertNotNull(nextLine);
		assertEquals("defg", nextLine.getText());
		assertEquals(5, nextLine.getStartOffset());
		assertEquals(1, nextLine.getLineNumber());
		assertSame(nextLine, lineSequence.getNextLine());
		
		lineSequence.advance();
		final Line line2= lineSequence.getCurrentLine();
		assertNotNull(line2);
		assertNotSame(line1, line2);
		assertEquals("defg", line2.getText());
		
		assertNull(lineSequence.getNextLine());
		
		lineSequence.advance();
		assertNoLinesRemain(lineSequence);
	}
	
	private void assertOneLine(final String line1, final LineSequence lineSequence) {
		final Line currentLine= lineSequence.getCurrentLine();
		assertNotNull(currentLine);
		assertEquals(line1, currentLine.getText());
		assertSame(currentLine, lineSequence.getCurrentLine());
		assertNull(lineSequence.getNextLine());
		
		lineSequence.advance();
		assertNoLinesRemain(lineSequence);
	}
	
}
