/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import org.eclipse.mylyn.wikitext.parser.builder.HtmlDocumentBuilder;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.TextSegment;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.spec.SimplifiedHtmlDocumentBuilder;


@NonNullByDefault
abstract class AbstractSourceSpanTest {
	
	
	public static void assertParse(final String content, final InlineParser parser,
			final @NonNull Inline... expectedInlines) {
		final List<Inline> expected= Arrays.asList(expectedInlines);
		final List<Inline> actual= parser.parse(CommonmarkAsserts.newContext(),
				new TextSegment(LineSequence.create(content)), true );
		for (int x= 0; x < expected.size() && x < actual.size(); ++x) {
			assertEquivalent(x, expected.get(x), actual.get(x));
		}
		assertEquals(expected, actual);
	}
	
	public static void assertEquivalent(final int index, final Inline expected, final Inline actual) {
		final String message= String.format("inlines[%1$s]", index);
		assertEquals(expected.getClass(), actual.getClass(), message + ".type");
		assertEquals(expected.getStartOffset(), actual.getStartOffset(), message + ".startOffset");
		assertEquals(expected.getLength(), actual.getLength(), message + ".length");
		assertEquals(expected.getCursorLength(), actual.getCursorLength(), message + ".cursorLength");
		assertEquals(expected, actual, message);
	}
	
	
	protected SourceSpan span;
	
	
	public AbstractSourceSpanTest(final SourceSpan span) {
		this.span= nonNullAssert(span);
	}
	
	public AbstractSourceSpanTest() {
	}
	
	
	public void assertNoInline(final Cursor cursor) {
		assertNoInline(cursor, 0);
	}
	
	public void assertNoInline(final Cursor cursor, final int offset) {
		final ProcessingContext context= CommonmarkAsserts.newContext();
		final Inline inline= this.span.createInline(context, cursor);
		assertNull(inline);
		assertEquals(offset, cursor.getOffset());
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Inline> T assertInline(final Class<T> type, final int offset, final int length, final Cursor cursor) {
		final ProcessingContext context= CommonmarkAsserts.newContext();
		final Inline inline= this.span.createInline(context, cursor);
		assertNotNull(inline);
		assertEquals(type, inline.getClass());
		assertEquals(offset, inline.getStartOffset());
		assertEquals(length, inline.getLength());
		
		return (T) inline;
	}
	
	public <T extends Inline> void assertInline(final Class<T> type, final int offset, final int length, final String expectedHtml, final Cursor cursor) {
		final ProcessingContext context= CommonmarkAsserts.newContext();
		final Inline inline= this.span.createInline(context, cursor);
		assertNotNull(inline);
		assertEquals(type, inline.getClass());
		assertEquals(offset, inline.getStartOffset());
		assertEquals(length, inline.getLength());
		
		final String html= emitToHtml(context, ImCollections.newList(inline));
		assertEquals(expectedHtml, html);
	}
	
	public void assertParseToHtml(final String expected, final String markup) {
		final InlineParser parser= new InlineParser(ImCollections.newList(
				this.span, new AllCharactersSpan() ));
		final ProcessingContext context= CommonmarkAsserts.newContext();
		final List<Inline> inlines= parser.parse(context,
				new TextSegment(ImCollections.newList(new Line(1, 0, 0, markup, ""))), true);
		
		final String html= emitToHtml(context, inlines);
		
		assertEquals(expected, html);
	}
	
	public String emitToHtml(final ProcessingContext context, final List<Inline> inlines) {
		context.setMode(ProcessingContext.EMIT_DOCUMENT);
		
		final StringWriter writer= new StringWriter();
		
		final HtmlDocumentBuilder builder= new SimplifiedHtmlDocumentBuilder(writer);
		builder.setEmitAsDocument(false);
		
		final CommonmarkLocator locator= new CommonmarkLocator();
		builder.setLocator(locator);
		
		builder.beginDocument();
		
		for (final Inline inline : inlines) {
			inline.emit(context, locator, builder);
		}
		
		builder.endDocument();
		
		return writer.toString();
	}
	
}
