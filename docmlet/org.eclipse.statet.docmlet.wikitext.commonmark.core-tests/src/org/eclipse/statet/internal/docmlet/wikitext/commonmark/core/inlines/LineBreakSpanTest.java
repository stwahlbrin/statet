/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Cursors.createCursor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Commonmark;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;


@NonNullByDefault
public class LineBreakSpanTest extends AbstractSourceSpanTest {
	
	
	public LineBreakSpanTest() {
		super(new LineBreakSpan());
	}
	
	
	@Test
	public void createInline() {
		assertInline(SoftLineBreak.class, 1, 1, createCursor("a\nb", 1));
		assertInline(SoftLineBreak.class, 1, 2, createCursor("a \nb", 1));
		assertInline(SoftLineBreak.class, 1, 2, createCursor("a \nb\nc", 1));
		assertInline(HardLineBreak.class, 1, 4, createCursor("a   \nb", 1));
		assertInline(HardLineBreak.class, 1, 3, createCursor("a \\\nb", 1));
		assertNoInline(createCursor("one"));
	}
	
	@Test
	public void parse_CommonMarkStrict() {
		parse(Commonmark.newInlineParserCommonMarkStrict());
	}
	
	@Test
	public void parse_Markdown() {
		parse(Commonmark.newInlineParserMarkdown());
	}
	
	private void parse(final InlineParser parser) {
		final Line line= new Line(0, 1, 0, "test", "\n");
		assertParse("a \\\\\nb", parser,
				new Characters(line, 0, 2, 2, "a "),
				new EscapedCharacter(line, 2, '\\'),
				new SoftLineBreak(line, 4, 1, 1),
				new Characters(line, 5, 1, 1, "b") );
		assertParse("a \\\\\\\nb", parser,
				new Characters(line, 0, 2, 2, "a "),
				new EscapedCharacter(line, 2, '\\'),
				new HardLineBreak(line, 4, 2, 2),
				new Characters(line, 6, 1, 1, "b") );
		
		assertParse("a \\\\\r\nb", parser,
				new Characters(line, 0, 2, 2, "a "),
				new EscapedCharacter(line, 2, '\\'),
				new SoftLineBreak(line, 4, 2, 1),
				new Characters(line, 6, 1, 1, "b") );
		assertParse("a \\\\\\\r\nb", parser,
				new Characters(line, 0, 2, 2, "a "),
				new EscapedCharacter(line, 2, '\\'),
				new HardLineBreak(line, 4, 3, 2),
				new Characters(line, 7, 1, 1, "b") );
	}
	
}
