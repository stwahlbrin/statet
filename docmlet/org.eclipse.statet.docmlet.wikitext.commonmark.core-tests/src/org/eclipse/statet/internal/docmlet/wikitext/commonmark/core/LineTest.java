/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class LineTest {
	
	
	public LineTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void requiresText() {
		assertThrows(NullPointerException.class, () ->
				new Line(0, 0, 0, null, "\n") );
	}
	
	@Test
	@SuppressWarnings("null")
	public void requiresDelimiter() {
		assertThrows(NullPointerException.class, () ->
				new Line(0, 0, 0, "", null) );
	}
	
	@Test
	public void requiresNonNegativeLineOffset() {
		assertThrows(IllegalArgumentException.class, () ->
				new Line(1, -1, 0, "test", "\n") );
	}
	
	@Test
	public void requiresNonNegativeLineNumber() {
		assertThrows(IllegalArgumentException.class, () ->
				new Line(-1, 1, 0, "test", "\n") );
	}
	
	@Test
	public void isBlank() {
		assertBlank("");
		assertBlank("\t");
		assertBlank("   ");
		assertBlank("      ");
		assertBlank("   \t");
		assertNotBlank("a");
		assertNotBlank(" a");
		assertNotBlank("a ");
	}
	
	@Test
	public void getText() {
		assertEquals("abc", new Line(1, 0, 0, "abc", "\n").getText());
	}
	
	@Test
	public void getLineNumber() {
		assertEquals(0, new Line(0, 1, 0, "abc", "\n").getLineNumber());
		assertEquals(1, new Line(1, 1, 0, "abc", "\n").getLineNumber());
	}
	
	@Test
	public void getOffset() {
		assertEquals(0, new Line(1, 0, 0, "abc", "\n").getStartOffset());
		assertEquals(1, new Line(1, 1, 0, "abc", "\n").getStartOffset());
	}
	
	@Test
	public void getColumn() {
		final Line line= new Line(2, 15, 0, "  \t0123456789", "\n");
		assertEquals(0, line.getColumn(0));
		assertEquals(1, line.getColumn(1));
		assertEquals(2, line.getColumn(2));
		assertEquals(4, line.getColumn(3));
		assertEquals(5, line.getColumn(4));
	}
	
	@Test
	public void toStringTest() {
		assertEquals("Line (lineNumber= 1, startOffset= 15, column= 0)\n\ttext= 1",
				new Line(1, 15, 0, "1", "\n").toString() );
		assertEquals("Line (lineNumber= 2, startOffset= 0, column= 3)\n\ttext= \\tabc",
				new Line(2, 0, 3, "\tabc", "\n").toString() );
		assertEquals("Line (lineNumber= 0, startOffset= 0, column= 0)\n\ttext= aaaaaaaaaaaaaaaaaaaa...",
				new Line(0, 0, 0, "a".repeat(100), "\n").toString() );
	}
	
	@Test
	public void segment() {
		final Line segment= new Line(2, 15, 0, "0123456789", "\n").segment(3, 5);
		assertNotNull(segment);
		assertEquals(2, segment.getLineNumber());
		assertEquals(15 + 3, segment.getStartOffset());
		assertEquals("34567", segment.getText());
		assertEquals(3, segment.getColumn());
	}
	
	
	@Test
	public void toLocator() {
		final CommonmarkLocator locator= new CommonmarkLocator();
		final Line line= new Line(2, 15, 0, "0123456789", "\n");
		locator.setLine(line);
		assertNotNull(locator);
		assertEquals(3, locator.getLineNumber());
		assertEquals(15, locator.getLineDocumentOffset());
	}
	
	
	private void assertNotBlank(final String string) {
		assertFalse(new Line(0, 0, 0, string, "\n").isBlank(), string);
	}
	
	private void assertBlank(final String string) {
		assertTrue(new Line(0, 0, 0, string, "\n").isBlank(), string);
	}
	
}
