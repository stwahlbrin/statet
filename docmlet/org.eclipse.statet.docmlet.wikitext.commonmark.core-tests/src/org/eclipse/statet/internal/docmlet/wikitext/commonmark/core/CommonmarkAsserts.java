/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.HtmlParser;
import org.eclipse.mylyn.wikitext.parser.IdGenerator;
import org.eclipse.mylyn.wikitext.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.parser.builder.HtmlDocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.builder.HtmlDocumentHandler;
import org.eclipse.mylyn.wikitext.parser.markup.MarkupLanguage;
import org.eclipse.mylyn.wikitext.util.XmlStreamWriter;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkConfig;
import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.spec.SimplifiedHtmlDocumentBuilder;


@NonNullByDefault
public class CommonmarkAsserts {
	
	
	public static ProcessingContext newContext() {
		return new ProcessingContext(Commonmark.newSourceBlocks(), Commonmark.newInlineParser(),
				createCommonmarkIdGenerator(), ProcessingContext.INITIALIZE_CONTEXT );
	}
	
	private static IdGenerator createCommonmarkIdGenerator() {
		final IdGenerator idGenerator= new IdGenerator();
		idGenerator.setGenerationStrategy(new CommonmarkIdGenerationStrategy());
		return idGenerator;
	}
	
	
	public static ImList<String> createLineList(final @NonNull String... lines) {
		return ImCollections.newList(lines);
	}
	
	public static String toContent(final Collection<String> lines) {
		return CollectionUtils.toString(lines, "\n");
	}
	
	
	public static void assertCanStart(final SourceBlockType<?> block, final String input) {
		final LineSequence lineSequence= LineSequence.create(input);
		assertTrue(block.canStart(lineSequence, null));
	}
	
	public static void assertCannotStart(final SourceBlockType<?> block, final String input) {
		final LineSequence lineSequence= LineSequence.create(input);
		assertFalse(block.canStart(lineSequence, null));
	}
	
	public static void assertContent(final String expectedHtml, final String input) {
		assertContent(expectedHtml, input, 0);
	}
	
	public static void assertContent(final String expectedHtml, final String input, final int mode) {
		final CommonmarkLanguage language= new CommonmarkLanguage("Test", mode, null);
		final String html= parseToHtml(language, input);
		assertHtmlEquals(expectedHtml, html);
	}
	
	public static void assertContent(final String expectedHtml, final String input, final CommonmarkConfig config) {
		final CommonmarkLanguage language= new CommonmarkLanguage("Test", 0, null);
		language.setMarkupConfig(config);
		final String html= parseToHtml(language, input);
		assertHtmlEquals(expectedHtml, html);
	}
	
	public static void assertContent(final MarkupLanguage language, final String expectedHtml, final String input) {
		final String html= parseToHtml(language, input);
		assertHtmlEquals(expectedHtml, html);
	}
	
	private static void assertHtmlEquals(String expectedHtml, String html) {
		if (expectedHtml.equals(html)) {
			return;
		}
		expectedHtml= expectedHtml.trim();
		html= html.trim();
		if (expectedHtml.equals(html)) {
			return;
		}
		assertEquals(toComparisonValue(expectedHtml), toComparisonValue(html));
	}
	
	private static @Nullable String toComparisonValue(final @Nullable String html) {
		if (html == null) {
			return null;
		}
		try {
			final StringWriter out= new StringWriter();
			final DocumentBuilder builder= createDocumentBuilder(out);
			HtmlParser.instance().parse(new InputSource(new StringReader(html)), builder);
			return out.toString();
		} catch (IOException | SAXException e) {
			throw new RuntimeException(html, e);
		}
	}
	
	private static String parseToHtml(final MarkupLanguage markupLanguage, final String input) {
		final StringWriter out= new StringWriter();
		final DocumentBuilder builder= createDocumentBuilder(out);
		final MarkupParser parser= new MarkupParser(markupLanguage, builder);
		parser.parse(input);
		return out.toString();
	}
	
	private static DocumentBuilder createDocumentBuilder(final StringWriter out) {
		final SimplifiedHtmlDocumentBuilder builder= new SimplifiedHtmlDocumentBuilder(out);
		builder.setResolveEntityReferences(true);
		builder.setDocumentHandler(new HtmlDocumentHandler() {
			
			@Override
			public void beginDocument(final HtmlDocumentBuilder builder, final XmlStreamWriter writer) {
			}
			
			@Override
			public void endDocument(final HtmlDocumentBuilder builder, final XmlStreamWriter writer) {
			}
			
		});
		return builder;
	}
	
}
