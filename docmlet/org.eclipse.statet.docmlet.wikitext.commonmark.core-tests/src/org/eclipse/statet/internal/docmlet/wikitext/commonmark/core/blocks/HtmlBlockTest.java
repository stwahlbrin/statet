/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.ParseHelper;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks;


@NonNullByDefault
public class HtmlBlockTest {
	
	
	private final HtmlBlock block= new HtmlBlock();
	
	
	public HtmlBlockTest() {
	}
	
	
	@Test
	public void canStart_Type1() {
		for (final String tagName : new String[] { "script", "pre", "style" }) {
			assertCanStart(true, 1, "<" + tagName);
			assertCanStart(true, 1, " <" + tagName);
			assertCanStart(true, 1, "   <" + tagName);
			assertCanStart(false, 1, "    <" + tagName);
			assertCanStart(true, 1, "<" + tagName + ">");
			assertCanStart(true, 1, "<" + tagName + "> ");
			assertCanStart(true, 7, "<" + tagName + "/>");
			assertCanStart(true, 1, "<" + tagName + ">with some text");
			assertCanStart(true, 1, "<" + tagName + "></" + tagName + ">");
			assertCanStart(true, 1, "<" + tagName + "></" + tagName + " >");
			assertCanStart(true, 1, "<" + tagName + ">  sdf</" + tagName + " >");
		}
	}
	
	@Test
	public void canStart_Comment() {
		assertCanStart(true, 2, "<!-- a comment -->");
		assertCanStart(true, 2, "<!-- <");
		assertCanStart(true, 2, "<!--");
		assertCanStart(true, 2, "<!-- <-");
		assertCanStart(true, 2, "<!--<-");
		// CommonMark spec != HTML spec
//		assertCanStart(false, "<!-->");
//		assertCanStart(false, "<!--->");
		assertCanStart(true, 2, "<!-- ->");
//		assertCanStart(false, "<!-- -- -->");
	}
	
	@Test
	public void canStart() {
		assertFalse(this.block.canStart(LineSequence.create(""), null));
		assertTrue(this.block.canStart(LineSequence.create("<div>"), null));
		assertTrue(this.block.canStart(LineSequence.create("<table>"), null));
		assertTrue(this.block.canStart(LineSequence.create("<p>"), null));
		assertTrue(this.block.canStart(LineSequence.create("<one>"), null));
		assertFalse(this.block.canStart(LineSequence.create("<one invalid=>"), null));
		assertFalse(this.block.canStart(LineSequence.create("<one> with text"), null));
		assertTrue(this.block.canStart(LineSequence.create("   <p>"), null));
		assertFalse(this.block.canStart(LineSequence.create("    <p>"), null));
		assertFalse(this.block.canStart(LineSequence.create("\t<p>"), null));
		assertTrue(this.block.canStart(LineSequence.create("<p"), null));
		assertTrue(this.block.canStart(LineSequence.create("<p >"), null));
		assertTrue(this.block.canStart(LineSequence.create("<p />"), null));
		assertTrue(this.block.canStart(LineSequence.create("<p/>"), null));
		assertTrue(this.block.canStart(LineSequence.create("<p\n  a=\"b\"\n>"), null));
	}
	
	@Test
	public void canStartDoesNotAdvanceLineSequencePosition() {
		final LineSequence lineSequence= LineSequence.create("<p\n  a=\"b\"\n>");
		final Line firstLine= lineSequence.getCurrentLine();
		assertTrue(this.block.canStart(lineSequence, null));
		assertSame(firstLine, lineSequence.getCurrentLine());
	}
	
	private void assertCanStart(final boolean expected, final int type, final String string) {
		assertEquals(expected, this.block.canStart(LineSequence.create(string), null));
		if (expected && type > 0) {
			final SourceBlockNode<?> paragraph= new SourceBlocks(ImCollections.newList(new ParagraphBlock<>()))
					.createNodes(LineSequence.create("abc"), new ParseHelper())
					.get(0);
			assertEquals(type != 7, this.block.canStart(LineSequence.create(string), paragraph));
		}
	}
	
}
