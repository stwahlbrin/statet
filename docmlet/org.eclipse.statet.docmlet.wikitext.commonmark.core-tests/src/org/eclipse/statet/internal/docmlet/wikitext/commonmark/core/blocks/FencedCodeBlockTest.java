/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCannotStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class FencedCodeBlockTest {
	
	
	private final FencedCodeBlock block= new FencedCodeBlock();
	
	
	public FencedCodeBlockTest() {
	}
	
	
	@Test
	public void canStart() {
		assertCanStart(this.block, "```");
		assertCanStart(this.block, " ```");
		assertCanStart(this.block, "  ```");
		assertCanStart(this.block, "   ```");
		assertCanStart(this.block, "````````````````");
		assertCannotStart(this.block, "    ```");
		assertCanStart(this.block, "~~~");
		assertCanStart(this.block, " ~~~");
		assertCanStart(this.block, "  ~~~");
		assertCanStart(this.block, "   ~~~");
		assertCannotStart(this.block, "    ~~~");
		assertCanStart(this.block, "~~~~~~~~~~~~~~~~~");
	}
	
	@Test
	public void canStartWithInfoText() {
		assertCanStart(this.block, "```````````````` some info text");
		assertCanStart(this.block, "~~~~~~~~~ some info text");
		assertCannotStart(this.block, "``` one ``");
	}
	
	@Test
	public void basic() {
		assertContent(
				"<p>first para</p><pre><code class=\"language-java\">public void foo() {\n\n}\n</code></pre><p>text</p>",
				"first para\n\n```` java and stuff\npublic void foo() {\n\n}\n````\ntext");
	}
	
	@Test
	public void encodedCharacters() {
		assertContent("<pre><code>&lt;\n &gt;\n</code></pre>", "```\n<\n >\n```");
	}
	
	@Test
	public void infoString() {
		assertContent("<pre class=\"language-info\"><code class=\"language-info\">code here\n</code></pre>",
				"``` info\ncode here\n```");
		assertContent("<pre class=\"language-info\"><code class=\"language-R\">code here\n</code></pre>",
				"``` R test class\ncode here\n```");
	}
	
	@Test
	public void restString() {
		assertContent("<pre class=\"language-info\"><code class=\"language-info\">code here\n</code></pre>",
				"``` info arg=1\ncode here\n```");
	}
	
}
