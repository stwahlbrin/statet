/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class TextSegmentTest {
	
	
	public TextSegmentTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void requiresLines() {
		assertThrows(NullPointerException.class,
				() -> new TextSegment((ImList<Line>)null) );
	}
	
	@Test
	public void getText() {
		assertEquals("", new TextSegment(ImCollections.<Line>newList()).getText());
		assertEquals("one\ntwo", new TextSegment(createLines("one\r\ntwo")).getText());
		assertEquals("one\ntwo\nthree", new TextSegment(createLines("one\r\ntwo\nthree")).getText());
	}
	
	@Test
	public void toOffset() {
		final TextSegment segment= new TextSegment(createLines("one\r\ntwo\r\nthree four"));
		final String text= segment.getText();
		assertEquals(0, segment.toOffset(text.indexOf("one")));
		assertEquals(5, segment.toOffset(text.indexOf("two")));
		assertEquals(3, segment.toOffset(text.indexOf("two") - 1));
		assertEquals(10, segment.toOffset(text.indexOf("three")));
		assertEquals(8, segment.toOffset(text.indexOf("three") - 1));
		assertEquals(16, segment.toOffset(text.indexOf("four")));
	}
	
	@Test
	public void toOffset_withIndent() {
		final TextSegment segment= new TextSegment(createLines("one\r\n  two\r\nthree four"));
		final String text= segment.getText();
		assertEquals(0, segment.toOffset(text.indexOf("one")));
		assertEquals(7, segment.toOffset(text.indexOf("two")));
		assertEquals(3, segment.toOffset(text.indexOf("two") - 1));
		assertEquals(12, segment.toOffset(text.indexOf("three")));
		assertEquals(10, segment.toOffset(text.indexOf("three") - 1));
		assertEquals(18, segment.toOffset(text.indexOf("four")));
	}
	
	@Test
	public void toTextOffset() {
		final TextSegment segment= new TextSegment(ImCollections.newList(
				new Line(1, 10, 0, "abc", "\n"),
				new Line(2, 15, 0, "def", "\n") ));
		assertEquals(0, segment.toTextOffset(10));
		assertEquals(2, segment.toTextOffset(12));
		assertEquals(4, segment.toTextOffset(15));
		assertEquals(6, segment.toTextOffset(17));
		try {
			segment.toTextOffset(19);
			fail("expected exception");
		} catch (final IllegalArgumentException e) {
			// expected
		}
		try {
			segment.toTextOffset(9);
			fail("expected exception");
		} catch (final IllegalArgumentException e) {
			// expected
		}
	}
	
	@Test
	public void toTextOffset_withIndent() {
		final TextSegment segment= new TextSegment(createLines("one\r\n  two\r\nthree four"));
		final String text= segment.getText();
		assertEquals(text.indexOf("one"), segment.toTextOffset(0));
		assertEquals(text.indexOf("two"), segment.toTextOffset(7));
		assertEquals(text.indexOf("two") - 1, segment.toTextOffset(3));
		assertEquals(text.indexOf("three"), segment.toTextOffset(12));
		assertEquals(text.indexOf("three") - 1, segment.toTextOffset(10));
		assertEquals(text.indexOf("four"), segment.toTextOffset(18));
	}
	
	@Test
	public void getLineAtOffset() {
		final ImList<Line> lines= ImCollections.newList(
				new Line(1, 10, 0, "abc", "\n"),
				new Line(2, 15, 0, "def", "\n") );
		final TextSegment segment= new TextSegment(lines);
		assertEquals(lines.get(0), segment.getLineAtOffset(0));
		assertEquals(lines.get(0), segment.getLineAtOffset(3));
		assertEquals(lines.get(1), segment.getLineAtOffset(4));
		assertEquals(lines.get(1), segment.getLineAtOffset(6));
	}
	
	
	private static void assertToString(final String expected, final TextSegment segment) {
		final String s= segment.toString();
		assertTrue(s.startsWith("TextSegment "));
		assertTrue(s.endsWith("text= " + expected));
	}
	
	@Test
	public void toStringTest() {
		assertToString("one\\ntwo\\nthree four",
				new TextSegment(createLines("one\ntwo\r\nthree four")) );
		assertToString("01234567890123456789...",
				new TextSegment(createLines("0123456789".repeat(10))) );
	}
	
	private LineSequence createLines(final String content) {
		return LineSequence.create(content);
	}
	
}
