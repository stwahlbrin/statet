/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Cursors.createCursor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class StringCharactersSpanTest extends AbstractSourceSpanTest {
	
	
	public StringCharactersSpanTest() {
		super(new StringCharactersSpan());
	}
	
	
	@Test
	public void createInline() {
		assertInline(Characters.class, 0, 1, createCursor("``one"));
		assertInline(Characters.class, 0, 1, createCursor("__two"));
		assertInline(Characters.class, 0, 1, createCursor("***three"));
		assertInline(Characters.class, 0, 3, createCursor("one`"));
		assertInline(Characters.class, 0, 3, createCursor("one\ntwo"));
		assertInline(Characters.class, 1, 3, createCursor(" one *two"));
		assertInline(Characters.class, 1, 7, createCursor(" one two *three"));
		assertInline(Characters.class, 1, 7, createCursor(" one two \\[ab"));
		assertInline(Characters.class, 1, 7, createCursor(" one two !"));
		assertInline(Characters.class, 1, 7, createCursor(" one two <"));
	}
	
}
