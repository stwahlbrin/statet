/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Function;

import com.google.common.net.UrlEscapers;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.ParseHelper;


@NonNullByDefault
public class ProcessingHelperTest {
	
	
	public ProcessingHelperTest() {
	}
	
	
	@Test
	public void replaceHtmlEntities() {
		final ParseHelper helper= new ParseHelper();
		final Function<String, String> escaper= UrlEscapers.urlFormParameterEscaper().asFunction();
		assertEquals("asf", helper.replaceHtmlEntities("asf", escaper));
		assertEquals("&amp", helper.replaceHtmlEntities("&amp", escaper));
		assertEquals("&amp ;", helper.replaceHtmlEntities("&amp ;", escaper));
		assertEquals("%26", helper.replaceHtmlEntities("&amp;", escaper));
		assertEquals("a%26", helper.replaceHtmlEntities("a&amp;", escaper));
		assertEquals("a%26b", helper.replaceHtmlEntities("a&amp;b", escaper));
		assertEquals("%C3%A4", helper.replaceHtmlEntities("&auml;", escaper));
		assertEquals("&", helper.replaceHtmlEntities("&amp;", null));
		assertEquals("\"", helper.replaceHtmlEntities("&quot;", null));
		assertEquals("\u00e4", helper.replaceHtmlEntities("&auml;", null));
		assertEquals("&xdfsldk;", helper.replaceHtmlEntities("&xdfsldk;", null));
		assertEquals("&0;", helper.replaceHtmlEntities("&0;", null));
	}
	
	@Test
	public void unescape() {
		final ParseHelper helper= new ParseHelper();
		assertEquals("(", helper.replaceEscaping("\\("));
		assertEquals("abc(def", helper.replaceEscaping("abc\\(def"));
	}
	
}
