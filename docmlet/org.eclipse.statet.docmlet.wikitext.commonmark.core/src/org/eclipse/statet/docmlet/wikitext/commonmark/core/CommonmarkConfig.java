/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.commonmark.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupConfig;


@NonNullByDefault
public interface CommonmarkConfig extends MarkupConfig {
	
	
	/**
	 * Returns if ATX header blocks do not interrupt paragraphs.
	 * That means a ATX header block after a paragraph requires a blank line.
	 * 
	 * <p>Defaults:
	 * <table>
	 *   <tr><td>CommonMark</td>				<td><code>false</code></td>	<td></td></tr>
	 *   <tr><td>Pandoc's Markdown</td>			<td><code>true</code></td>	<td>blank_before_header</td></tr>
	 * </table></p>
	 * 
	 * @return if extension is enabled
	 */
	boolean isHeaderInterruptParagraphDisabled();
	
	/**
	 * Returns if quote blocks do not interrupt paragraphs.
	 * That means quote blocks after a paragraph requires a blank line.
	 * 
	 * <p>Defaults:
	 * <table>
	 *   <tr><td>CommonMark</td>				<td><code>false</code></td>	<td></td></tr>
	 *   <tr><td>Pandoc's Markdown</td>			<td><code>true</code></td>	<td>blank_before_blockquote</td></tr>
	 * </table></p>
	 * 
	 * @return if extension is enabled
	 */
	boolean isBlockquoteInterruptParagraphDisabled();
	
	
	/**
	 * Returns if strikeout typesetting spans by delimiter '~~' is enabled.
	 * 
	 * <p>Defaults:
	 * <table>
	 *   <tr><td>CommonMark</td>				<td><code>false</code></td>	<td></td></tr>
	 *   <tr><td>Pandoc's Markdown</td>			<td><code>true</code></td>	<td>strikeout</td></tr>
	 * </table></p>
	 * 
	 * @return if extension is enabled
	 */
	boolean isStrikeoutByDTildeEnabled();
	
	/**
	 * Returns if superscript typesetting span by delimiter '^' is enabled.
	 * 
	 * <p>Defaults:
	 * <table>
	 *   <tr><td>CommonMark</td>				<td><code>false</code></td>	<td></td></tr>
	 *   <tr><td>Pandoc's Markdown</td>			<td><code>true</code></td>	<td>superscript</td></tr>
	 * </table></p>
	 * 
	 * @return if extension is enabled
	 */
	boolean isSuperscriptBySCircumflexEnabled();
	
	/**
	 * Returns if subscript typesetting span by delimiter '~' is enabled.
	 * 
	 * <p>Defaults:
	 * <table>
	 *   <tr><td>CommonMark</td>				<td><code>false</code></td>	<td></td></tr>
	 *   <tr><td>Pandoc's Markdown</td>			<td><code>true</code></td>	<td>subscript</td></tr>
	 * </table></p>
	 * 
	 * @return if extension is enabled
	 */
	boolean isSubscriptBySTildeEnabled();
	
}
