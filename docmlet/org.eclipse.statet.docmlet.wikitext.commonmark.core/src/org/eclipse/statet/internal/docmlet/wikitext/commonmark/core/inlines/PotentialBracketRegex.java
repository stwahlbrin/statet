/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References.REF_LABEL_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References.REF_TITLE_REGEX;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class PotentialBracketRegex {
	
	
	private static final Pattern END_PATTERN= Pattern.compile(
				"(?:[ \t\\n\\r]+(" + REF_TITLE_REGEX + "))?" +
				"[ \t]*" + "(\\)).*",
			Pattern.DOTALL );
	
	private static final Pattern REFERENCE_LABEL_PATTERN= Pattern.compile(
			"(\\[(" + REF_LABEL_REGEX + ")\\]).*",
			Pattern.DOTALL );
	
	private static final Pattern REFERENCE_NAME_PATTERN= Pattern.compile(REF_LABEL_REGEX,
			Pattern.DOTALL );
	
	
	private final Matcher endMatcher= END_PATTERN.matcher("");
	private final Matcher referenceLabelMatcher= REFERENCE_LABEL_PATTERN.matcher("");
	private final Matcher referenceNameMatcher= REFERENCE_NAME_PATTERN.matcher("");
	
	
	public Matcher getEndMatcher() {
		return this.endMatcher;
	}
	
	public Matcher getReferenceLabelMatcher() {
		return this.referenceLabelMatcher;
	}
	
	public Matcher getReferenceNameMatcher() {
		return this.referenceNameMatcher;
	}
	
}
