/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.ConcurrentModificationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
class LookAheadLineSequence extends LineSequence {
	
	
	private final ContentLineSequence lineSequence;
	
	private @Nullable Line currentLine;
	
	private final @Nullable Line referenceLine;
	
	private int index;
	
	
	public LookAheadLineSequence(final ContentLineSequence lineSequence) {
		this.lineSequence= lineSequence;
		this.currentLine= lineSequence.getCurrentLine();
		this.referenceLine= this.currentLine;
		this.index= -1;
	}
	
	public LookAheadLineSequence(final LookAheadLineSequence lookAheadLineSequence) {
		this.lineSequence= lookAheadLineSequence.lineSequence;
		this.currentLine= lookAheadLineSequence.currentLine;
		this.referenceLine= lookAheadLineSequence.referenceLine;
		this.index= lookAheadLineSequence.index;
	}
	
	
	@Override
	public LineSequence lookAhead() {
		return new LookAheadLineSequence(this);
	}
	
	
	@Override
	public @Nullable Line getCurrentLine() {
		return this.currentLine;
	}
	
	@Override
	public @Nullable Line getNextLine() {
		checkConcurrentModification();
		return this.lineSequence.getNextLine(this.index + 1);
	}
	
	@Override
	public void advance() {
		checkConcurrentModification();
		this.currentLine= getNextLine();
		++this.index;
	}
	
	private void checkConcurrentModification() {
		if (this.referenceLine != this.lineSequence.getCurrentLine()) {
			throw new ConcurrentModificationException();
		}
	}
	
}
