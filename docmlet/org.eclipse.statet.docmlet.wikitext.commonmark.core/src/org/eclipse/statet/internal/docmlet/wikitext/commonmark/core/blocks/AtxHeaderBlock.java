/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.HeadingAttributes;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementDetail;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceHeadingAttributes;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Lines;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.TextSegment;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.InlineParser;


@NonNullByDefault
public class AtxHeaderBlock extends SourceBlockType<AtxHeaderBlock.AtxHeaderBlockNode> {
	
	
	static final class AtxHeaderBlockNode extends SourceBlockNode<AtxHeaderBlock> {
		
		private AtxHeaderBlockNode(final AtxHeaderBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
	}
	
	
	private static final Pattern START_PATTERN= Pattern.compile(
			"#{1,6}(?:[ \t].*)?",
			Pattern.DOTALL );
	
	private static final Pattern PATTERN= Pattern.compile(
			"(#{1,6})(?:[ \t]??(.+?))??(?:[ \t]+#+)?[ \t]*",
			Pattern.DOTALL );
	
	
	private final Matcher startMatcher= START_PATTERN.matcher("");
	
	private @Nullable Matcher matcher;
	
	
	public AtxHeaderBlock() {
	}
	
	
	private Matcher getProcessMatcher() {
		Matcher matcher= this.matcher;
		if (matcher == null) {
			matcher= PATTERN.matcher(""); //$NON-NLS-1$
			this.matcher= matcher;
		}
		return matcher;
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line currentLine= lineSequence.getCurrentLine();
		return (currentLine != null
				&& !currentLine.isBlank() && currentLine.getIndent() < 4
				&& currentLine.setupIndent(this.startMatcher).matches() );
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		@SuppressWarnings("unused")
		final AtxHeaderBlockNode node= new AtxHeaderBlockNode(this, builder);
		
		builder.getLineSequence().advance();
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final AtxHeaderBlockNode node) {
	}
	
	@Override
	public void emit(final ProcessingContext context, final AtxHeaderBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final Line startLine= node.getLines().get(0);
		final Matcher matcher= startLine.setup(getProcessMatcher(), true, true);
		assertMatches(matcher);
		
		final int headingLevel= getHeadingLevel(startLine, matcher);
		final Line contentLine= getContentLine(startLine, matcher);
		if (contentLine != null) {
			final TextSegment textSegment= new TextSegment(ImCollections.newList(contentLine));
			
			final SourceHeadingAttributes attributes= new SourceHeadingAttributes(
					(startLine.getLineDelimiter().isEmpty()) ? SourceElementDetail.END_UNCLOSED : 0 );
			
			final InlineParser inlineParser= context.getInlineParser();
			final String headingText= inlineParser.toStringContent(context, textSegment);
			attributes.setId(context.generateHeadingId(headingLevel, headingText));
			
			locator.setBlockBegin(node);
			builder.beginHeading(headingLevel, attributes);
			
			inlineParser.emit(context, textSegment, locator, builder);
			
			locator.setBlockEnd(node);
			builder.endHeading();
		}
		else {
			locator.setBlockBegin(node);
			builder.beginHeading(headingLevel, new HeadingAttributes());
			locator.setBlockEnd(node);
			builder.endHeading();
		}
	}
	
	
	private int getHeadingLevel(final Line line, final Matcher matcher) {
		return matcher.end(1) - matcher./*start(1)*/regionStart();
	}
	
	private @Nullable Line getContentLine(final Line line, final Matcher matcher) {
		final int contentOffset= matcher.start(2);
		final int contentEnd= matcher.end(2);
		if (contentOffset >= contentEnd) {
			return null;
		}
		return Lines.trimWhitespace(line.segment(contentOffset, contentEnd - contentOffset));
	}
	
}
