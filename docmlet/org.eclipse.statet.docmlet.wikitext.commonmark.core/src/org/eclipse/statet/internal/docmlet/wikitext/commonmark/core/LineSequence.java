/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.text.core.util.TextLineInformationCreator;


@NonNullByDefault
public abstract class LineSequence {
	
	
	public static LineSequence create(final String text) {
		return new ContentLineSequence(text, new TextLineInformationCreator().create(text));
	}
	
	
	public abstract LineSequence lookAhead();
	
	public LineSequence lookAhead(final int lineNumber) {
		final LineSequence lookAhead= lookAhead();
		Line line= lookAhead.getCurrentLine();
		if (line != null) {
			if (line.getLineNumber() > lineNumber) {
				throw new IllegalArgumentException("lineNumber= " + lineNumber);
			}
			do {
				if (line.getLineNumber() >= lineNumber) {
					break;
				}
				else {
					lookAhead.advance();
					line= lookAhead.getCurrentLine();
				}
			}
			while (line != null);
		}
		return lookAhead;
	}
	
	
	public abstract @Nullable Line getCurrentLine();
	
	public abstract @Nullable Line getNextLine();
	
	public abstract void advance();
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(LineSequence.class, getClass());
		sb.addProp("currentLine", getCurrentLine()); //$NON-NLS-1$
		sb.addProp("nextLine", getNextLine()); //$NON-NLS-1$
		return sb.build();
	}
	
}
