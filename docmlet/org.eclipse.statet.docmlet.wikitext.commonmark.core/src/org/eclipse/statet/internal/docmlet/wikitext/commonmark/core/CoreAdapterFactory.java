/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import org.eclipse.core.runtime.IAdapterFactory;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.MarkupSourceFormatAdapter;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.source.CommonmarkSourceFormatAdapter;


@NonNullByDefault
public class CoreAdapterFactory implements IAdapterFactory {
	
	
	private static final @NonNull Class<?>[] ADAPTERS= new @NonNull Class[] {
			MarkupSourceFormatAdapter.class
	};
	
	
	private @Nullable MarkupSourceFormatAdapter sourceAdapter;
	
	
	public CoreAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == MarkupSourceFormatAdapter.class) {
			synchronized (this) {
				var sourceAdapter= this.sourceAdapter;
				if (sourceAdapter == null) {
					sourceAdapter= new CommonmarkSourceFormatAdapter();
					this.sourceAdapter= sourceAdapter;
				}
				return (T)sourceAdapter;
			}
		}
		return null;
	}
	
}
