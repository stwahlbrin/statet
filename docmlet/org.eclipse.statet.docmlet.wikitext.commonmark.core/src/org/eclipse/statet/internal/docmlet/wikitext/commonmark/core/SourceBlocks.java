/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.ParseHelper;


@NonNullByDefault
public class SourceBlocks {
	
	
	public abstract class SourceBlockBuilder {
		
		
		private final CollectLineSequence collectLineSequence;
		
		private @Nullable SourceBlockNode<?> currentNode;
		
		private final ImList<? extends SourceBlockType<?>> supportedTypes;
		
		private @Nullable SourceBlockBuilder nested;
		
		
		private SourceBlockBuilder(final LineSequence lineSequence,
				final ImList<? extends SourceBlockType<?>> supportedTypes) {
			this.collectLineSequence= new CollectLineSequence(lineSequence);
			this.supportedTypes= supportedTypes;
		}
		
		
		public abstract ParseHelper getParseHelper();
		
		
		public LineSequence getLineSequence() {
			return this.collectLineSequence;
		}
		
		public SourceBlocks getSourceBlocks() {
			return SourceBlocks.this;
		}
		
		public @Nullable SourceBlockBuilder getParent() {
			return null;
		}
		
		public SourceBlockNode<?> getCurrentNode() {
			return this.currentNode;
		}
		
		public @Nullable SourceBlockBuilder getActiveLeaf() {
			SourceBlockBuilder active= null;
			SourceBlockBuilder candidate= this.nested;
			while (candidate != null && candidate.currentNode != null) {
				active= candidate;
				candidate= candidate.nested;
			}
			return active;
		}
		
		public @Nullable SourceBlockNode<?> getParentNode() {
			final SourceBlockBuilder parent= getParent();
			return (parent != null) ? parent.currentNode : null;
		}
		
		public @Nullable SourceBlockNode<?> getActiveNode() {
			final SourceBlockBuilder active= getActiveLeaf();
			return (active != null) ? active.currentNode : null;
		}
		
		public void createNestedNodes(final LineSequence lineSequence,
				final @Nullable ImList<? extends SourceBlockType<?>> supportedTypes) {
			try {
				final SourceBlockBuilder nested= new SourceBlockBuilder(lineSequence,
						(supportedTypes != null) ? supportedTypes : getSourceBlocks().supportedTypes ) {
					@Override
					public @Nullable SourceBlockBuilder getParent() {
						return SourceBlockBuilder.this;
					}
					@Override
					public ParseHelper getParseHelper() {
						return SourceBlockBuilder.this.getParseHelper();
					}
				};
				this.nested= nested;
				nested.run();
			}
			finally {
				this.nested= null;
			}
		}
		
		
		void beginNode(final SourceBlockNode<?> node) {
			finishNode();
			
			this.currentNode= node;
		}
		
		void finishNode() {
			final SourceBlockNode<?> node= this.currentNode;
			if (node != null) {
				this.currentNode= null;
				node.setLines(this.collectLineSequence.getLines());
				this.collectLineSequence.reset();
				
				accept(node);
			}
		}
		
		void accept(final SourceBlockNode<?> node) {
		}
		
		void run() {
			while (this.collectLineSequence.getCurrentLine() != null) {
				final SourceBlockType block= selectBlock(this.collectLineSequence, this.supportedTypes, null);
				if (block != null) {
					block.createNodes(this);
					finishNode();
				}
				else {
					return;
				}
			}
		}
		
	}
	
	private static class CollectLineSequence extends LineSequence {
		
		
		private final LineSequence delegate;
		
		private @Nullable Line currentLine;
		
		private final List<Line> lines= new ArrayList<>();
		
		
		public CollectLineSequence(final LineSequence delegate) {
			this.delegate= delegate;
		}
		
		
		@Override
		public LineSequence lookAhead() {
			return this.delegate.lookAhead();
		}
		
		@Override
		public LineSequence lookAhead(final int lineNumber) {
			return this.delegate.lookAhead(lineNumber);
		}
		
		@Override
		public @Nullable Line getCurrentLine() {
			return this.currentLine= this.delegate.getCurrentLine();
		}
		
		@Override
		public @Nullable Line getNextLine() {
			return this.delegate.getNextLine();
		}
		
		@Override
		public void advance() {
			Line line= this.currentLine;
			if (line == null) {
				line= this.delegate.getCurrentLine();
			}
			else {
				this.currentLine= null;
			}
			
			this.delegate.advance();
			
			if (line != null) {
				this.lines.add(line);
			}
		}
		
		
		public ImList<Line> getLines() {
			return ImCollections.toList(this.lines);
		}
		
		public void reset() {
			this.lines.clear();
		}
		
	}
	
	
	private final ImList<SourceBlockType<?>> supportedTypes;
	
	
	public SourceBlocks(final ImList<SourceBlockType<?>> supportedTypes) {
		this.supportedTypes= nonNullAssert(supportedTypes);
	}
	
	
	public List<SourceBlockNode<?>> createNodes(final LineSequence lineSequence,
			final ParseHelper parseHelper) {
		final List<SourceBlockNode<?>> nodes= new ArrayList<>();
		
		final SourceBlockBuilder sourceBlockBuilder= new SourceBlockBuilder(lineSequence,
				this.supportedTypes ) {
			@Override
			public ParseHelper getParseHelper() {
				return parseHelper;
			}
			@Override
			public void accept(final SourceBlockNode<?> node) {
				nodes.add(node);
			}
		};
		sourceBlockBuilder.run();
		
		return nodes;
	}
	
	public void parseSourceStruct(final ProcessingContext context, final LineSequence lineSequence,
			final DocumentBuilder builder) {
		context.setMode(ProcessingContext.PARSE_SOURCE_STRUCT);
		
		final CommonmarkLocator locator= new CommonmarkLocator();
		builder.setLocator(locator);
		
		final SourceBlockBuilder sourceBlockBuilder= new SourceBlockBuilder(lineSequence,
				this.supportedTypes ) {
			@Override
			public ParseHelper getParseHelper() {
				return context.getHelper();
			}
			@Override
			public void accept(final SourceBlockNode<?> node) {
				node.getType().emit(context, node, locator, builder);
			}
		};
		sourceBlockBuilder.run();
	}
	
	public void initializeContext(final ProcessingContext context, final List<SourceBlockNode<?>> nodes) {
		context.setMode(ProcessingContext.INITIALIZE_CONTEXT);
		for (final SourceBlockNode<?> node : nodes) {
			node.getType().initializeContext(context, node);
		}
	}
	
	public void emit(final ProcessingContext context, final List<SourceBlockNode<?>> nodes,
			final DocumentBuilder builder) {
		context.setMode(ProcessingContext.EMIT_DOCUMENT);
		
		final CommonmarkLocator locator= new CommonmarkLocator();
		builder.setLocator(locator);
		
		for (final SourceBlockNode<?> node : nodes) {
			node.getType().emit(context, node, locator, builder);
		}
	}
	
	
	public @Nullable SourceBlockType<?> selectBlock(final LineSequence lineSequence,
			final SourceBlockNode<?> currentNode) {
		return selectBlock(lineSequence, this.supportedTypes, currentNode);
	}
	
	public @Nullable SourceBlockType<?> selectBlock(final LineSequence lineSequence) {
		return selectBlock(lineSequence, this.supportedTypes, null);
	}
	
	public @Nullable SourceBlockType<?> selectBlock(final LineSequence lineSequence,
			final ImList<? extends SourceBlockType<?>> supportedTypes,
			final @Nullable SourceBlockNode<?> currentNode) {
		for (final SourceBlockType<?> candidate : supportedTypes) {
			if (candidate.canStart(lineSequence, currentNode)) {
				return candidate;
			}
		}
		return null;
	}
	
}
