/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.regex.Matcher;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class Cursor {
	
	
	private final TextSegment segment;
	
	private final String text;
	
	private int textOffset;
	
	
	public Cursor(final TextSegment segment) {
		this.segment= nonNullAssert(segment);
		this.text= segment.getText();
		this.textOffset= 0;
	}
	
	/**
	 * Provides the offset of the current cursor position relative to the document.
	 * 
	 * @return the current cursor position offset
	 */
	public int getOffset() {
		return this.segment.toOffset(this.textOffset);
	}
	
	/**
	 * Provides the offset of the given cursor position relative to the document.
	 * 
	 * @param offset
	 *            the position relative to the cursor
	 * @return the current cursor position offset
	 */
	public int getOffset(final int offset) {
		return this.segment.toOffset(this.textOffset + offset);
	}
	
	public int toCursorOffset(final int documentOffset) {
		return this.segment.toTextOffset(documentOffset);
	}
	
	public char getChar() {
		return this.text.charAt(this.textOffset);
	}
	
	/**
	 * Provides the character at the cursor's 0-based offset, where the given offset is not affected by the position of
	 * the cursor.
	 * 
	 * @param offset
	 *            the absolute offset of the character relative to this cursor
	 * @return the character
	 */
	public char getChar(final int offset) {
		return this.text.charAt(offset);
	}
	
	public boolean hasChar() {
		return (this.textOffset < this.text.length());
	}
	
	/**
	 * Provides the string at the cursor's 0-based offset, where the given offset is not affected by the position of the
	 * cursor.
	 * 
	 * @param offset
	 *            the absolute offset of the character relative to this cursor
	 * @param endIndex
	 *            the end index of the string to provide, exclusive
	 * @return the string
	 */
	public String getText(final int offset, final int endIndex) {
		return this.text.substring(offset, endIndex);
	}
	
	public char getPrevious() {
		return getPrevious(1);
	}
	
	public boolean hasPrevious() {
		return (this.textOffset - 1 >= 0);
	}
	
	public boolean hasPrevious(final int offset) {
		if (offset <= 0) {
			throw new IllegalArgumentException();
		}
		return (this.textOffset - offset >= 0);
	}
	
	public char getPrevious(final int offset) {
		if (offset <= 0) {
			throw new IllegalArgumentException();
		}
		final int charOffset= this.textOffset - offset;
		return this.text.charAt(charOffset);
	}
	
	public boolean hasNext() {
		return (this.textOffset + 1 < this.text.length());
	}
	
	public boolean hasNext(final int offset) {
		if (offset <= 0) {
			throw new IllegalArgumentException();
		}
		return (this.textOffset + offset < this.text.length());
	}
	
	public char getNext() {
		return getNext(1);
	}
	
	public char getNext(final int offset) {
		if (offset <= 0) {
			throw new IllegalArgumentException();
		}
		return this.text.charAt(this.textOffset + offset);
	}
	
	public int getRemaining() {
		return this.text.length() - this.textOffset;
	}
	
	
	public String getTextAtOffset() {
		return this.text.substring(this.textOffset);
	}
	
	public String getTextAtOffset(final int start, final int end) {
		return this.text.substring(this.textOffset + start, this.textOffset + end);
	}
	
	
	public void advance() {
		if (this.textOffset < this.text.length()) {
			++this.textOffset;
		}
	}
	
	public void advance(final int count) {
		if (count < 0) {
			throw new IllegalArgumentException("count= " + count); //$NON-NLS-1$
		}
		for (int x= 0; x < count; ++x) {
			advance();
		}
	}
	
	public void rewind(final int count) {
		if (count < 0) {
			throw new IllegalArgumentException("count= " + count); //$NON-NLS-1$
		}
		for (int x= 0; x < count; ++x) {
			rewind();
		}
	}
	
	public void rewind() {
		if (this.textOffset > 0) {
			--this.textOffset;
		}
	}
	
	public Line getLineAtOffset() {
		return this.segment.getLineAtOffset(this.textOffset);
	}
	
	
	public Matcher setup(final Matcher matcher) {
		matcher.reset(this.text);
		matcher.region(this.textOffset, this.text.length());
		return matcher;
	}
	
	public Matcher setup(final Matcher matcher, final int offset) {
		matcher.reset(this.text);
		matcher.region(this.textOffset + offset, this.text.length());
		return matcher;
	}
	
	public int getMatcherOffset(final int matchOffset) {
		return this.segment.toOffset(matchOffset);
	}
	
}
