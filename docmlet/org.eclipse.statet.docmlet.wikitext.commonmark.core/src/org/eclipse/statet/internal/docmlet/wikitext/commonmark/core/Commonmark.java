/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkConfig;
import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.AtxHeaderBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.BlockQuoteBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.EmptyBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.FencedCodeBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.HtmlBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.IndentedCodeBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ListBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ParagraphAndTheLikeBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ParagraphBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ThematicBreakBlock;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.AutoLinkSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.AutoLinkWithoutDemarcationSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.BackslashEscapeSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.CodeSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.HtmlEntitySpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.HtmlTagSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.InlineParser;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.LineBreakSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.PotentialBracketSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.PotentialStyleSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.SourceSpan;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.StringCharactersSpan;


@NonNullByDefault
public class Commonmark {
	
	
	public static class MylynLanguage extends CommonmarkLanguage {
		
		public MylynLanguage() {
			super("Mylyn", CommonmarkLanguage.MYLYN_COMPAT_MODE, null);
		}
		
	}
	
	
	public static SourceBlocks newSourceBlocks() {
		return new SourceBlocks(ImCollections.newList(
				new BlockQuoteBlock(),
				new AtxHeaderBlock(),
				new ThematicBreakBlock(),
				new ListBlock(),
				new FencedCodeBlock(),
				new IndentedCodeBlock(),
				new HtmlBlock(),
				new ParagraphAndTheLikeBlock(),
				new EmptyBlock() ));
	}
	
	public static SourceBlocks newSourceBlocks(final @Nullable CommonmarkConfig config) {
		if (config == null) {
			return newSourceBlocks();
		}
		
		final List<Class<? extends SourceBlockType<?>>> interruptParagraphExclusions;
		if (config.isHeaderInterruptParagraphDisabled()
				|| config.isBlockquoteInterruptParagraphDisabled() ) {
			interruptParagraphExclusions= new ArrayList<>();
			interruptParagraphExclusions.addAll(ParagraphBlock.DEFAULT_INTERRUPT_EXCLUSIONS);
			if (config.isHeaderInterruptParagraphDisabled()) {
				interruptParagraphExclusions.add(AtxHeaderBlock.class);
			}
			if (config.isBlockquoteInterruptParagraphDisabled()) {
				interruptParagraphExclusions.add(BlockQuoteBlock.class);
			}
		}
		else {
			interruptParagraphExclusions= ParagraphBlock.DEFAULT_INTERRUPT_EXCLUSIONS;
		}
		
		return new SourceBlocks(ImCollections.newList(
				new BlockQuoteBlock(),
				new AtxHeaderBlock(),
				new ThematicBreakBlock(),
				new ListBlock(),
				new FencedCodeBlock(),
				new IndentedCodeBlock(),
				new HtmlBlock(),
				new ParagraphAndTheLikeBlock(ImCollections.toIdentityList(interruptParagraphExclusions)),
				new EmptyBlock() ));
	}
	
	public static InlineParser newInlineParser() {
		return newInlineParserCommonMarkStrict();
	}
	
	
	public static InlineParser newInlineParserCommonMarkStrict() {
		return newInlineParserCommonMark(null);
	}
	
	public static InlineParser newInlineParserCommonMark(final @Nullable CommonmarkConfig config) {
		final ArrayList<SourceSpan> spans= new ArrayList<>();
		spans.add(new LineBreakSpan());
		spans.add(new BackslashEscapeSpan());
		spans.add(new CodeSpan());
		spans.add(new AutoLinkSpan());
		spans.add(new HtmlTagSpan());
		spans.add(new HtmlEntitySpan());
		final PotentialStyleSpan styleSpan= (config != null) ?
				new PotentialStyleSpan(
						config.isStrikeoutByDTildeEnabled(),
						config.isSuperscriptBySCircumflexEnabled(),
						config.isSubscriptBySTildeEnabled() ) :
				new PotentialStyleSpan();
		spans.add(styleSpan);
		spans.add(new PotentialBracketSpan());
		spans.add(new StringCharactersSpan(styleSpan.getControlChars()));
		return new InlineParser(ImCollections.toList(spans));
	}
	
	public static InlineParser newInlineParserMarkdown() {
		final ArrayList<SourceSpan> spans= new ArrayList<>();
		spans.add(new LineBreakSpan());
		spans.add(new BackslashEscapeSpan());
		spans.add(new CodeSpan());
		spans.add(new AutoLinkSpan());
		spans.add(new AutoLinkWithoutDemarcationSpan());
		spans.add(new HtmlTagSpan());
		spans.add(new HtmlEntitySpan());
		spans.add(new PotentialStyleSpan());
		spans.add(new PotentialBracketSpan());
		spans.add(new StringCharactersSpan("h"));
		return new InlineParser(ImCollections.toList(spans));
	}
	
}
