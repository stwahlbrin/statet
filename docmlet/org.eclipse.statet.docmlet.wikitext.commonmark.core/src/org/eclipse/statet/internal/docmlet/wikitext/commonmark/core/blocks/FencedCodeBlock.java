/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.ParseHelper;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementDetail;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;


@NonNullByDefault
public class FencedCodeBlock extends SourceBlockType<FencedCodeBlock.CodeBlockNode> {
	
	
	static final class CodeBlockNode extends SourceBlockNode<FencedCodeBlock> {
		
		private @Nullable String infoText;
		private boolean isClosed;
		
		private CodeBlockNode(final FencedCodeBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
	}
	
	
	private static final Pattern START_PATTERN= Pattern.compile(
			"(?:`{3,}[^`]*|~{3,}.*)", //$NON-NLS-1$
			Pattern.DOTALL );
	
	private static final Pattern OPEN_PATTERN= Pattern.compile(
			"(`{3,}|~{3,})[ \t]*([^ \t]+)?.*", //$NON-NLS-1$
			Pattern.DOTALL );
	private static final int OPEN_SYMBOL_GROUP= 1;
	private static final int OPEN_INFO_TEXT_GROUP= 2;
	
	private static final Pattern CLOSE_BACKTICK_PATTERN= Pattern.compile(
			"(`{3,})[ \t]*", //$NON-NLS-1$
			Pattern.DOTALL );
	private static final Pattern CLOSE_TILDE_PATTERN= Pattern.compile(
			"(~{3,})[ \t]*", //$NON-NLS-1$
			Pattern.DOTALL );
	
	
	private final Matcher startMatcher= START_PATTERN.matcher("");
	
	private @Nullable Matcher openMatcher;
	private @Nullable Matcher closeBacktickMatcher;
	private @Nullable Matcher closeTildeMatcher;
	
	
	public FencedCodeBlock() {
	}
	
	
	private Matcher getOpenMatcher() {
		Matcher matcher= this.openMatcher;
		if (matcher == null) {
			matcher= OPEN_PATTERN.matcher(""); //$NON-NLS-1$
			this.openMatcher= matcher;
		}
		return matcher;
	}
	
	private Matcher getCloseMatcher(final Line line, final Matcher openMatcher) {
		Matcher matcher;
		switch (line.getText().charAt(openMatcher.start(OPEN_SYMBOL_GROUP))) {
		case '`':
			matcher= this.closeBacktickMatcher;
			if (matcher == null) {
				matcher= CLOSE_BACKTICK_PATTERN.matcher(""); //$NON-NLS-1$
				this.closeBacktickMatcher= matcher;
			}
			return matcher;
		case '~':
			matcher= this.closeTildeMatcher;
			if (matcher == null) {
				matcher= CLOSE_TILDE_PATTERN.matcher(""); //$NON-NLS-1$
				this.closeTildeMatcher= matcher;
			}
			return matcher;
		default:
			throw new IllegalStateException();
		}
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line currentLine= lineSequence.getCurrentLine();
		return (currentLine != null
				&& !currentLine.isBlank() && currentLine.getIndent() < 4
				&& currentLine.setupIndent(this.startMatcher).matches() );
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		final CodeBlockNode node= new CodeBlockNode(this, builder);
		final LineSequence lineSequence= builder.getLineSequence();
		
		final Line startLine= (@NonNull Line)lineSequence.getCurrentLine();
		lineSequence.advance();
		
		final Matcher openMatcher= startLine.setup(getOpenMatcher(), true, false);
		assertMatches(openMatcher);
		final int minCount= openMatcher.end(OPEN_SYMBOL_GROUP) - openMatcher./*start(1)*/regionStart();
		node.infoText= getInfoText(openMatcher, builder.getParseHelper());
		final Matcher closeMatcher= getCloseMatcher(startLine, openMatcher);
		
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null) {
				lineSequence.advance();
				
				if (!line.isBlank() && line.getIndent() < 4
						&& (line.setupIndent(closeMatcher)).matches()
						&& closeMatcher.end(1) - closeMatcher./*start(1)*/regionStart() >= minCount ) {
					node.isClosed= true;
					break;
				}
				continue;
			}
			break;
		}
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final CodeBlockNode node) {
	}
	
	@Override
	public void emit(final ProcessingContext context, final CodeBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final ImList<Line> lines= node.getLines();
		
		final Line startLine= lines.get(0);
		final Matcher openMatcher= startLine.setup(getOpenMatcher(), true, false);
		assertMatches(openMatcher);
		
		final SourceElementAttributes attributes= new SourceElementAttributes(
				(!node.isClosed) ? SourceElementDetail.END_UNCLOSED : 0 );
		if (node.infoText != null) {
			final String language= getLanguage(node.infoText);
			attributes.setCssClass("language-" + context.getHelper().replaceEscaping(language)); //$NON-NLS-1$
		}
		locator.setBlockBegin(node);
		builder.beginBlock(BlockType.CODE, attributes);
		
		final int startIndent= startLine.getIndent();
		for (final Line line : lines.subList(1, (node.isClosed) ? lines.size() - 1 : lines.size())) {
			final Line codeSegment;
			if (startIndent > 0 && line.getIndent() > 0) {
				codeSegment= line.segmentByIndent(Math.min(startIndent, line.getIndent()));
			}
			else {
				codeSegment= line;
			}
			locator.setLine(codeSegment);
			builder.characters(codeSegment.getText());
			builder.characters("\n"); //$NON-NLS-1$
		}
		
		locator.setBlockEnd(node);
		builder.endBlock();
	}
	
	
	private @Nullable String getInfoText(final Matcher openMatcher,
			final ParseHelper parseHelper) {
		final String infoText= openMatcher.group(OPEN_INFO_TEXT_GROUP);
		if (infoText != null && !infoText.isEmpty()) {
			return parseHelper.trimWhitespace(infoText);
		}
		return null;
	}
	
	private String getLanguage(final String infoString) {
		final int indexSep= CommonRegex.indexOfWhitespace(infoString, 0, infoString.length());
		return (indexSep != -1) ? infoString.substring(0, indexSep) : infoString;
	}
	
}
