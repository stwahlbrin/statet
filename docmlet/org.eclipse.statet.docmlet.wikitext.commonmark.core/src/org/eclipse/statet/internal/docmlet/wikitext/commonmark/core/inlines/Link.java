/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.Objects;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.SpanType;
import org.eclipse.mylyn.wikitext.parser.LinkAttributes;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.docmlet.wikitext.core.source.LabelInfo;
import org.eclipse.statet.docmlet.wikitext.core.source.LinkByRefAttributes;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ToStringHelper;


@NonNullByDefault
public class Link extends InlineWithNestedContents {
	
	
	private final String href;
	private final @Nullable LabelInfo labelReference;
	
	private final @Nullable String title;
	
	
	public Link(final Line line, final int offset, final int length, final int cursorLength,
			final String href, final @Nullable String title, final List<? extends Inline> contents) {
		super(line, offset, length, cursorLength, contents);
		this.href= nonNullAssert(href);
		this.labelReference= null;
		this.title= title;
	}
	
	public Link(final Line line, final int offset, final int length,
			final String href, final @Nullable String title, final List<Inline> contents) {
		this(line, offset, length, -1, href, title, contents);
	}
	
	public Link(final Line line, final int offset, final int length,
			final LabelInfo labelReference, final List<Inline> contents) {
		super(line, offset, length, -1, contents);
		this.href= LinkByRefAttributes.REF_SCHEME + ':' + labelReference.getLabel();
		this.labelReference= labelReference;
		this.title= null;
	}
	
	
	public String getHref() {
		return this.href;
	}
	
	@Override
	public void emit(final ProcessingContext context,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final LinkAttributes attributes= (this.labelReference != null) ?
				new LinkByRefAttributes(this.labelReference) : new LinkAttributes();
		attributes.setTitle(this.title);
		attributes.setHref(this.href);
		builder.beginSpan(SpanType.LINK, attributes);
		
		InlineParser.emit(context, getContents(), locator, builder);
		
		builder.endSpan();
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(getStartOffset(), getLength(), getContents(), this.href, this.title);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (super.equals(obj)) {
			@SuppressWarnings("null")
			final Link other= (@NonNull Link)obj;
			
			return (this.href.equals(other.href)
					&& getContents().equals(other.getContents())
					&& Objects.equals(this.title, other.title) );
		}
		return false;
	}
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(Link.class, getClass());
		sb.addProp("startOffset", getStartOffset()); //$NON-NLS-1$
		sb.addProp("length", getLength()); //$NON-NLS-1$
		sb.addProp("href", ToStringHelper.toStringValue(this.href)); //$NON-NLS-1$
		sb.addProp("title", this.title); //$NON-NLS-1$
		sb.addProp("contents", getContents()); //$NON-NLS-1$
		return sb.build();
	}
	
}
