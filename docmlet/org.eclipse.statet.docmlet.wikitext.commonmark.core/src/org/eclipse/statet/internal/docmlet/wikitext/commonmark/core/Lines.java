/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class Lines {
	
	
	public static @Nullable Line trimWhitespace(final Line line) {
		final Line line0= line.trimWhitespace(true, true);
		if (line0.isEmpty()) {
			return null;
		}
		return line0;
	}
	
	public static ImList<Line> trimWhitespace(final ImList<Line> lines, final int startIdx, final int endIdx) {
		final int n= endIdx - startIdx;
		switch (n) {
		case 0:
			return ImCollections.emptyList();
		case 1:
			{	final Line line0= lines.get(0).trimWhitespace(true, true);
				if (line0.isEmpty()) {
					return ImCollections.emptyList();
				}
				return ImCollections.newList(line0);
			}
		default:
			{	final var array= new @NonNull Line[n];
				int added= 0;
				int i= startIdx;
				while (i < endIdx) {
					final Line line= lines.get(i++).trimWhitespace(true, false);
					if (line.isEmpty()) {
						continue;
					}
					array[added++]= line;
					break;
				}
				while (i < endIdx) {
					final Line line= lines.get(i++);
					array[added++]= line;
				}
				while (added > 0) {
					final Line line= array[added - 1].trimWhitespace(false, true);
					if (line.isEmpty()) {
						added--;
						continue;
					}
					array[added - 1]= line;
					break;
				}
				return ImCollections.newList(array, 0, added);
			}
		}
	}
	
	
	private Lines() {
	}
	
}
