/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;


@NonNullByDefault
public abstract class PotentialStyleDelimiterInfo {
	
	public static final byte FLANKING=                      1 << 0;
	public static final byte FLANKING_UNDERSCORE=           1 << 1;
	public static final byte NO_SPACE=                      1 << 4;
	
	
	static abstract class EmphasisDelimiter extends PotentialStyleDelimiterInfo {
		
		
		@Override
		public boolean isPotentialSequence(final int length) {
			return true;
		}
		
		@Override
		public Inline createStyleInline(final int size, final Line line,
				final int offset, final int length, final List<Inline> contents) {
			switch (size) {
			case 1:
				return new Emphasis(line, offset, length, contents);
			case 2:
				return new Strong(line, offset, length, contents);
			default:
				throw new IllegalStateException();
			}
		}
		
	}
	
	static abstract class ExtDelimiter extends PotentialStyleDelimiterInfo {
		
		private final byte flags;
		
		public ExtDelimiter(final boolean size1, final boolean size2) {
			byte f= 0;
			if (size1) {
				f|= 1;
			}
			if (size2) {
				f|= 2;
			}
			if (f == 0) {
				throw new IllegalArgumentException();
			}
			this.flags= f;
		}
		
		@Override
		public boolean isPotentialSequence(final int length) {
			switch (this.flags & 3) {
			case 2:
				return (length >= 2);
			case 1:
			case 3:
			default:
				return true;
			}
		}
		
		@Override
		public int getSize(final int openingLength, final int closingLength) {
			switch (this.flags & 3) {
			case 1:
				return 1;
			case 2:
				return 2;
			case 3:
			default:
				return super.getSize(openingLength, closingLength);
			}
		}
		
		@Override
		public byte getRequirements(final int size) {
			return (size == 1) ? NO_SPACE : FLANKING;
		}
		
	}
	
	
	static final PotentialStyleDelimiterInfo DEFAULT_ASTERISK= new EmphasisDelimiter() {
		
		@Override
		public char getChar() {
			return '*';
		}
		
		@Override
		public byte getRequirements(final int size) {
			return FLANKING;
		}
		
	};
	
	static final PotentialStyleDelimiterInfo DEFAULT_UNDERSCORE= new EmphasisDelimiter() {
		
		@Override
		public char getChar() {
			return '_';
		}
		
		@Override
		public byte getRequirements(final int size) {
			return FLANKING_UNDERSCORE;
		}
		
	};
	
	
	public abstract char getChar();
	
	public abstract boolean isPotentialSequence(int length);
	
	public int getSize(final int openingLength, final int closingLength) {
		final int min= Math.min(openingLength, closingLength);
		if (min <= 2) {
			return min;
		}
		return 2;
	}
	
	public abstract byte getRequirements(int size);
	
	public abstract Inline createStyleInline(int size, Line line, int offset, int length, List<Inline> contents);
	
	
	@Override
	public String toString() {
		return "DelimiterInfo '" + getChar() + "'";
	}
	
}
