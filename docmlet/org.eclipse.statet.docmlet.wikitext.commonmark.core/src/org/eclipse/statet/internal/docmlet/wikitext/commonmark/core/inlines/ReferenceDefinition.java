/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Objects;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.LinkAttributes;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.docmlet.wikitext.core.source.LabelInfo;
import org.eclipse.statet.docmlet.wikitext.core.source.LinkRefDefinitionAttributes;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ToStringHelper;


@NonNullByDefault
public class ReferenceDefinition extends Inline {
	
	
	private final String href;
	
	private final @Nullable String title;
	
	private final LabelInfo referenceLabel;
	
	
	public ReferenceDefinition(final Line line, final int offset, final int length,
			final String href, final @Nullable String title, final LabelInfo referenceLabel) {
		super(line, offset, length, -1);
		this.href= nonNullAssert(href);
		this.title= title;
		this.referenceLabel= nonNullAssert(referenceLabel);
	}
	
	
	public String getHref() {
		return this.href;
	}
	
	public String getLabel() {
		return this.referenceLabel.getLabel();
	}
	
	public @Nullable String getTitle() {
		return this.title;
	}
	
	
	@Override
	public void emit(final ProcessingContext context,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		if (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) {
			final LinkAttributes attributes= new LinkRefDefinitionAttributes(this.referenceLabel);
			attributes.setTitle(this.title);
			attributes.setHref(this.href);
			builder.link(attributes, this.href, null);
		}
		// nothing to do
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(getStartOffset(), getLength(), this.referenceLabel, this.href, this.title);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (super.equals(obj)) {
			@SuppressWarnings("null")
			final ReferenceDefinition other= (@NonNull ReferenceDefinition)obj;
			
			return (this.href.equals(other.href)
					&& this.referenceLabel.equals(other.referenceLabel)
					&& Objects.equals(this.title, other.title) );
		}
		return false;
	}
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(ReferenceDefinition.class, getClass());
		sb.addProp("startOffset", getStartOffset()); //$NON-NLS-1$
		sb.addProp("length", getLength()); //$NON-NLS-1$
		sb.addProp("name", this.referenceLabel); //$NON-NLS-1$
		sb.addProp("href", ToStringHelper.toStringValue(this.href)); //$NON-NLS-1$
		sb.addProp("title", this.title); //$NON-NLS-1$
		return sb.build();
	}
	
}
