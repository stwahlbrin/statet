/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;


@NonNullByDefault
public class TextSegment {
	
	
	private static String computeText(final List<Line> lines) {
		final StringBuilder text= new StringBuilder(lines.size() * 32);
		
		final Iterator<Line> iter= lines.iterator();
		if (iter.hasNext()) {
			Line line= iter.next();
			while (iter.hasNext()) {
				text.append(line.getTextContent(false));
				text.append('\n');
				line= iter.next();
			}
			text.append(line.getTextContent(true));
		}
		
		return text.toString();
	}
	
	
	private final ImList<Line> lines;
	
	private final String text;
	
	
	public TextSegment(final ImList<Line> lines) {
		this.lines= lines;
		this.text= computeText(this.lines);
	}
	
	public TextSegment(final LineSequence lines) {
		final List<Line> list= new ArrayList<>();
		while (true) {
			final Line line= lines.getCurrentLine();
			if (line == null) {
				break;
			}
			list.add(line);
			lines.advance();
		}
		this.lines= ImCollections.toList(list);
		this.text= computeText(this.lines);
	}
	
	
	public String getText() {
		return this.text;
	}
	
	public ImList<Line> getLines() {
		return this.lines;
	}
	
	public int toOffset(final int textOffset) {
		if (textOffset < 0) {
			throw new IllegalArgumentException();
		}
		int contentOffsetOfLine= 0;
		int remainder= textOffset;
		for (final Line line : this.lines) {
			contentOffsetOfLine= line.getTextContentOffset();
			final int lineContentPlusSeparatorLength= line.getTextContentLength() + 1;
			if (lineContentPlusSeparatorLength > remainder) {
				break;
			}
			remainder-= lineContentPlusSeparatorLength;
		}
		return contentOffsetOfLine + remainder;
	}
	
	public int toTextOffset(final int documentOffset) {
		int textOffset= 0;
		for (final Line line : this.lines) {
			final int lineRelativeOffset= documentOffset - line.getTextContentOffset();
			final int lineContentPlusSeparatorLength= line.getTextContentLength() + 1;
			if (lineRelativeOffset >= 0 && lineRelativeOffset < lineContentPlusSeparatorLength) {
				return textOffset + lineRelativeOffset;
			}
			textOffset+= lineContentPlusSeparatorLength;
		}
		throw new IllegalArgumentException();
	}
	
	public Line getLineAtOffset(final int textOffset) {
		final int documentOffset= toOffset(textOffset);
		Line previous= null;
		for (final Line line : this.lines) {
			if (line.getStartOffset() > documentOffset) {
				break;
			}
			previous= line;
		}
		return nonNullAssert(previous);
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(TextSegment.class, getClass());
		sb.addProp("text", ToStringHelper.toStringValue(this.text)); //$NON-NLS-1$
		return sb.build();
	}
	
}
