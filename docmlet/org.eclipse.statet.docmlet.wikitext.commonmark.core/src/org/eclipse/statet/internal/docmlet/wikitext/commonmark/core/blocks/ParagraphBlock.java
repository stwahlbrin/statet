/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import java.util.Collection;
import java.util.List;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementDetail;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceTextBlockAttributes;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.TextSegment;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ParagraphBlock.ParagraphBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Inline;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.InlineParser;


@NonNullByDefault
public class ParagraphBlock<TNode extends ParagraphBlockNode<?>> extends SourceBlockType<TNode> {
	
	
	static class ParagraphBlockNode<TType extends ParagraphBlock<?>> extends SourceBlockNode<TType> {
		
		protected ParagraphBlockNode(final TType type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
		
		@Override
		public boolean isParagraph() {
			return true;
		}
		
	}
	
	
	public static final ImIdentityList<Class<? extends SourceBlockType<?>>> DEFAULT_INTERRUPT_EXCLUSIONS= ImCollections.newIdentityList();
	
	
	private final Collection<Class<? extends SourceBlockType<?>>> interruptExclusions;
	
	
	public ParagraphBlock() {
		this(DEFAULT_INTERRUPT_EXCLUSIONS);
	}
	
	public ParagraphBlock(final ImCollection<Class<? extends SourceBlockType<?>>> interruptExclusions) {
		this.interruptExclusions= interruptExclusions;
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line line= lineSequence.getCurrentLine();
		return (line != null && !line.isBlank());
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		final ParagraphBlockNode<ParagraphBlock<?>> node= new ParagraphBlockNode<>(this, builder);
		final LineSequence lineSequence= builder.getLineSequence();
		
		lineSequence.advance();
		
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null
					&& !line.isBlank()
					&& !isAnotherBlockStart(lineSequence, builder.getSourceBlocks(), node) ) {
				lineSequence.advance();
				continue;
			}
			break;
		}
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final TNode node) {
	}
	
	@Override
	public void emit(final ProcessingContext context, final TNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		emitParagraph(context, node, true, locator, builder);
	}
	
	void emitParagraph(final ProcessingContext context, final ParagraphBlockNode<?> node,
			final boolean asBlock,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final ImList<Line> lines= node.getLines();
		
		final TextSegment textSegment= new TextSegment(lines);
		
		final List<Inline> inlines= context.getInlineParser().parse(context, textSegment, true);
		if (!inlines.isEmpty()) {
			locator.setBlockBegin(node);
			if (asBlock) {
				final SourceTextBlockAttributes attributes= new SourceTextBlockAttributes(lines,
						SourceElementDetail.END_UNCLOSED );
				builder.beginBlock(BlockType.PARAGRAPH, attributes);
			}
			InlineParser.emit(context, inlines, locator, builder);
			
			locator.setBlockEnd(node);
			if (asBlock) {
				builder.endBlock();
			}
		}
	}
	
	
	boolean isAnotherBlockStart(final LineSequence lineSequence, final SourceBlocks sourceBlocks,
			final SourceBlockNode<?> currentNode) {
		final SourceBlockType<?> block= sourceBlocks.selectBlock(lineSequence, currentNode);
		if (block != null && !(block instanceof ParagraphBlock)) {
			if (!this.interruptExclusions.contains(block.getClass())) {
				return true;
			}
		}
		return false;
	}
	
}
