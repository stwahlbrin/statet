/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementDetail;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;


@NonNullByDefault
public class IndentedCodeBlock extends SourceBlockType<IndentedCodeBlock.CodeBlockNode> {
	
	
	static final class CodeBlockNode extends SourceBlockNode<IndentedCodeBlock> {
		
		private CodeBlockNode(final IndentedCodeBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
	}
	
	
	public IndentedCodeBlock() {
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line line= lineSequence.getCurrentLine();
		return (line != null
				&& !line.isBlank() && line.getIndent() >= 4
				&& currentNode == null );
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		@SuppressWarnings("unused")
		final CodeBlockNode node= new CodeBlockNode(this, builder);
		final LineSequence lineSequence= builder.getLineSequence();
		
		lineSequence.advance();
		
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null) {
				if (line.isBlank()) {
					final int end= lookAheadSafeLine(lineSequence.lookAhead());
					if (end != Integer.MIN_VALUE) {
						advanceLinesUpto(lineSequence, end);
						continue;
					}
				}
				else {
					if (line.getIndent() >= 4) {
						lineSequence.advance();
						continue;
					}
				}
			}
			break;
		}
	}
	
	private int lookAheadSafeLine(final LineSequence lineSequence) {
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null) {
				if (line.isBlank()) {
					lineSequence.advance();
					continue;
				}
				if (line.getIndent() >= 4) {
					return line.getLineNumber();
				}
			}
			return Integer.MIN_VALUE;
		}
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final CodeBlockNode node) {
	}
	
	@Override
	public void emit(final ProcessingContext context, final CodeBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final ImList<Line> lines= node.getLines();
		
		final SourceElementAttributes attributes= new SourceElementAttributes(
				SourceElementDetail.END_UNCLOSED );
		
		locator.setBlockBegin(node);
		builder.beginBlock(BlockType.CODE, attributes);
		
		for (final Line line : lines) {
			if (line.getIndent() >= 4) {
				final Line codeSegment= line.segmentByIndent(4);
				locator.setLine(codeSegment);
				builder.characters(codeSegment.getCodeContent());
				builder.characters("\n");
			}
			else {
				builder.characters("\n");
			}
		}
		
		locator.setBlockEnd(node);
		builder.endBlock();
	}
	
}
