/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;


public class ToStringHelper {
	
	private static final String ELIPSES= "...";
	private static final int STRING_MAX_LENGTH= 20;
	
	
	public static String toStringValue(final String text) {
		if (text == null) {
			return null;
		}
		String stringValue= text;
		if (stringValue.length() > STRING_MAX_LENGTH) {
			stringValue= stringValue.substring(0, STRING_MAX_LENGTH) + ELIPSES;
		}
		return stringValue.replace("\t", "\\t").replace("\n", "\\n").replace("\r", "\\r");
	}
	
	
	private ToStringHelper() {
	}
	
}
