/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class CommonRegex {
	
	
	/*-- XML/HTML --*/
	
	public static final String TAG_NAME_REGEX= "\\p{Alpha}[\\p{Alnum}-]*";
	
	public static final String ATTR_NAME_REGEX= "[\\p{Alpha}_:][\\p{Alnum}_.:-]*";
	public static final String ATTR_VALUE_D_QUOTED= "\"[^\"]*\"";
	public static final String ATTR_VALUE_S_QUOTED= "'[^']*'";
	public static final String ATTR_VALUE_U_QUOTED= "[^\\s\"'=><`]+";
	public static final String ATTR_VALUE_REGEX= ATTR_VALUE_D_QUOTED + "|" + ATTR_VALUE_S_QUOTED + "|" + ATTR_VALUE_U_QUOTED;
	public static final String ATTR_SPEC_REGEX= "\\s+" + ATTR_NAME_REGEX + "(?:\\s*=\\s*(?:" + ATTR_VALUE_REGEX + "))?";
	
	public static final String COMMENT_START1_REGEX= "!--";
	public static final String COMMENT_END_REGEX= "-->";
	public static final String COMMENT_1_REGEX= COMMENT_START1_REGEX +
			"(?:" + COMMENT_END_REGEX + "|-?[^>-](?:-?[^-])*" + COMMENT_END_REGEX + ")";
	
	public static final String PI_START1_REGEX= "\\?";
	public static final String PI_END_REGEX= "\\?>";
	public static final String PI_1_REGEX= PI_START1_REGEX + ".*?" + PI_END_REGEX;
	
	public static final String DECL_START1_REGEX= "!\\p{Alpha}+";
	public static final String DECL_END_REGEX= ">";
	public static final String DECL_1_REGEX= DECL_START1_REGEX + "\\s[^>]*" + DECL_END_REGEX;
	
	public static final String CDATA_START1_REGEX= "!\\[CDATA\\[";
	public static final String CDATA_END_REGEX= "\\]\\]>";
	public static final String CDATA_1_REGEX= CDATA_START1_REGEX + ".*?" + CDATA_END_REGEX;
	
	public static final String OPEN_TAG_1_REGEX= TAG_NAME_REGEX +
			"(?:" + ATTR_SPEC_REGEX + ")" +
			"*\\s*/?>";
	public static final String CLOSE_TAG_1_REGEX="/" + TAG_NAME_REGEX +
			"\\s*>";
	
	public static final String HTML_ENTITY_REGEX=
			"&(#[Xx]\\p{XDigit}{1,6}|#\\p{Digit}{1,7}|\\p{Alpha}\\p{Alnum}{1,31});";
	
	public static final Pattern HTML_ENTITY_PATTERN= Pattern.compile(HTML_ENTITY_REGEX,
			Pattern.DOTALL );
	
	
	public static final String CTRL_OR_SPACE= "\\x00-\\x20\\x7F";
	
	
	public static boolean isWhitespace(final char ch) {
		switch (ch) {
		case '\t':
		case '\n':
		case '\r':
		case ' ':
			return true;
		default:
			return false;
		}
	}
	
	public static int indexOfWhitespace(final String s, final int startIndex, final int endIndex) {
		for (int index= startIndex; index < endIndex; index++) {
			if (isWhitespace(s.charAt(index))) {
				return index;
			}
		}
		return -1;
	}
	
	public static int indexOfNonWhitespace(final String s, final int startIndex, final int endIndex) {
		for (int index= startIndex; index < endIndex; index++) {
			if (!isWhitespace(s.charAt(index))) {
				return index;
			}
		}
		return -1;
	}
	
	public static int indexOfNonWhitespaceEnd(final String s, final int startIndex, final int endIndex) {
		for (int index= endIndex - 1; index >= startIndex; index--) {
			if (!isWhitespace(s.charAt(index))) {
				return index + 1;
			}
		}
		return -1;
	}
	
	
	public static boolean isUnicodeWhitespace(final char ch) {
		if (ch >= 0 && ch < 0x1000) {
			switch (ch) {
			case '\t':
			case '\n':
			case 0x0C:
			case '\r':
			case ' ':
			case 0xA0:
				return true;
			default:
				return false;
			}
		}
		else {
			return (Character.getType(ch) == Character.SPACE_SEPARATOR);
		}
	}
	
}
