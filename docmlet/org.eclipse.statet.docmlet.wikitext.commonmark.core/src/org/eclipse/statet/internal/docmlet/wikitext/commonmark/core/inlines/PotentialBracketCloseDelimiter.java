/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import java.util.List;
import java.util.regex.Matcher;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.LabelInfo;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext.ReferenceDef;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References.LinkDestination;


@NonNullByDefault
public class PotentialBracketCloseDelimiter extends InlineWithText {
	
	
	private static int findLastPotentialBracketDelimiter(final List<Inline> inlines) {
		for (int idx= inlines.size() - 1; idx >= 0; --idx) {
			final Inline inline= inlines.get(idx);
			if (inline instanceof PotentialBracketOpenDelimiter) {
				return idx;
			}
		}
		return -1;
	}
	
	private static boolean isIndentInline(final Inline inline, final Line line) {
		return (inline instanceof Characters
				&& inline.getStartOffset() == line.getStartOffset()
				&& inline.getLength() == line.getIndentLength() );
	}
	
	
	private final PotentialBracketRegex shared;
	
	
	public PotentialBracketCloseDelimiter(final Line line, final int offset,
			final PotentialBracketRegex shared) {
		super(line, offset, 1, 1, "]");
		
		this.shared= shared;
	}
	
	
	@Override
	public void apply(final ProcessingContext context, final List<Inline> inlines,
			final Cursor cursor, final boolean inBlock) {
		final int openingDelimiterIndex= findLastPotentialBracketDelimiter(inlines);
		if (openingDelimiterIndex >= 0) {
			final PotentialBracketOpenDelimiter openingDelimiter= (PotentialBracketOpenDelimiter)
					inlines.get(openingDelimiterIndex);
			
			if (openingDelimiter.isActive()) {
				final List<Inline> contents= InlineParser.secondPass(
						inlines.subList(openingDelimiterIndex + 1, inlines.size() ));
				
				if (!openingDelimiter.isLinkDelimiter() || !containsLink(contents)) {
					final LinkDestination linkDestination;
					final Matcher matcher;
					int length= 0;
					if (cursor.hasNext() && cursor.getNext() == '(') {
						linkDestination= References.readLinkDestination(cursor.getTextAtOffset(), 2);
						if (linkDestination != null) {
							length= linkDestination.getEndOffset();
							matcher= cursor.setup(this.shared.getEndMatcher(), length);
						}
						else {
							length= 2;
							matcher= cursor.setup(this.shared.getEndMatcher(), length);
						}
					}
					else {
						linkDestination= null;
						matcher= null;
					}
					if (matcher != null /*== cursor.hasNext()*/ && matcher.matches()) {
						final String title= linkTitle(matcher, context, cursor);
						
						length+= matcher.end(2) - matcher.regionStart();
						cursor.advance(length);
						
						final int startOffset= openingDelimiter.getStartOffset();
						final int endOffset= cursor.getOffset();
						
						truncate(inlines, openingDelimiter, openingDelimiterIndex);
						
						if (openingDelimiter.isLinkDelimiter()) {
							inactivatePreceding(inlines);
						}
						
						final String uri= (linkDestination != null) ?
								References.normalizeUri(
										context.getHelper().replaceEscaping(linkDestination.getEscapedUri()) ) :
								"";
						
						if (openingDelimiter.isImageDelimiter()) {
							inlines.add(new Image(openingDelimiter.getLine(),
									startOffset, endOffset - startOffset, uri,
									title, contents ));
						}
						else {
							inlines.add(new Link(openingDelimiter.getLine(),
									startOffset, endOffset - startOffset,
									uri, title, contents ));
						}
						
						return;
					}
					else {
						length= 1;
						LabelInfo referenceLabel= referenceLabel(context, cursor, contents);
						if (cursor.hasNext()) {
							final Matcher referenceLabelMatcher= cursor.setup(
									this.shared.getReferenceLabelMatcher(),
									1 );
							if (referenceLabelMatcher.matches()) {
								final String label= context.normalizeLabel(referenceLabelMatcher.group(2));
								if (label != null) {
									final int start= cursor.getMatcherOffset(referenceLabelMatcher.start(2));
									final int end= cursor.getMatcherOffset(referenceLabelMatcher.end(2));
									referenceLabel= new LabelInfo(label, start, end);
								}
								length+= referenceLabelMatcher.end(1) - referenceLabelMatcher.regionStart();
							}
						}
						
						if (referenceLabel != null) {
							if (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) {
								cursor.advance(length);
								
								final int startOffset= openingDelimiter.getStartOffset();
								final int endOffset= cursor.getOffset();
								
								truncate(inlines, openingDelimiter, openingDelimiterIndex);
								
								if (openingDelimiter.isLinkDelimiter()) {
									inactivatePreceding(inlines);
								}
								
								if (openingDelimiter.isLinkDelimiter()) {
									inlines.add(new Link(openingDelimiter.getLine(),
											startOffset, endOffset - startOffset,
											referenceLabel, ImCollections.<Inline>emptyList() ));
								}
								else {
									inlines.add(new Image(openingDelimiter.getLine(),
											startOffset, endOffset - startOffset,
											referenceLabel, contents ));
								}
								
								return;
							}
							
							final ReferenceDef uriWithTitle= context.getReferenceDef(referenceLabel.getLabel());
							if (uriWithTitle != null) {
								cursor.advance(length);
								
								final int startOffset= openingDelimiter.getStartOffset();
								final int endOffset= cursor.getOffset();
								
								truncate(inlines, openingDelimiter, openingDelimiterIndex);
								
								if (openingDelimiter.isLinkDelimiter()) {
									inactivatePreceding(inlines);
								}
								
								if (openingDelimiter.isLinkDelimiter()) {
									inlines.add(new Link(openingDelimiter.getLine(),
											startOffset, endOffset - startOffset,
											uriWithTitle.getUri(), uriWithTitle.getTitle(), contents ));
								}
								else {
									inlines.add(new Image(openingDelimiter.getLine(),
											startOffset, endOffset - startOffset,
											uriWithTitle.getUri(), uriWithTitle.getTitle(), contents ));
								}
								
								return;
							}
						}
					}
				}
			}
			
			replaceDelimiter(inlines, openingDelimiterIndex, openingDelimiter);
		}
		
		if (Characters.append(inlines, inlines.size(), this)) {
			cursor.advance(getLength());
			return;
		}
		super.apply(context, inlines, cursor, inBlock);
	}
	
	@Override
	public void emit(final ProcessingContext context,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		builder.characters(this.text);
	}
	
	
	private @Nullable LabelInfo referenceLabel(final ProcessingContext context, final Cursor cursor,
			final List<Inline> contents) {
		if (contents.isEmpty()) {
			return null;
		}
		final int start= contents.get(0).getStartOffset();
		final int end= getStartOffset();
		String name= cursor.getText(
				cursor.toCursorOffset(start),
				cursor.toCursorOffset(end) );
		if (this.shared.getReferenceNameMatcher().reset(name).matches()) {
			name= context.normalizeLabel(name);
			if (name != null) {
				return new LabelInfo(name, start, end);
			}
		}
		return null;
	}
	
	private boolean containsLink(final List<Inline> contents) {
		for (final Inline inline : contents) {
			if (inline instanceof Link) {
				return true;
			} else if (inline instanceof InlineWithNestedContents
					&& containsLink(((InlineWithNestedContents)inline).getContents())) {
				return true;
			}
		}
		return false;
	}
	
	boolean isEligibleForReferenceDefinition(final List<Inline> inlines,
			final PotentialBracketOpenDelimiter openingDelimiter, final int openingDelimiterIndex) {
		final Line openingLine= openingDelimiter.getLine();
		if (openingDelimiter.isLinkDelimiter()
				&& openingLine.getIndent() < 4
				&& openingDelimiter.getStartOffset() == openingLine.getStartOffset() + openingLine.getIndentLength() ) {
			if (openingDelimiterIndex > 0) {
				int idx= openingDelimiterIndex - 1;
				{	final Inline inline= inlines.get(idx--);
					if (!(inline instanceof ReferenceDefinition
							|| isIndentInline(inline, openingLine) )) {
						return false;
					}
				}
				while (idx >= 0) {
					final Inline inline= inlines.get(idx--);
					if (!(inline instanceof ReferenceDefinition)) {
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}
	
	private String linkTitle(final Matcher matcher, final ProcessingContext context, final Cursor cursor) {
		final int start= matcher.start(1);
		if (start != -1) {
			final int end= matcher.end(1);
			final String title= cursor.getText(start + 1, end - 1);
			return context.getHelper().replaceEscaping(title);
		}
		return "";
	}
	
	public void truncate(final List<Inline> inlines,
			final PotentialBracketOpenDelimiter openingDelimiter, final int indexOfOpeningDelimiter) {
		while (inlines.size() > indexOfOpeningDelimiter) {
			inlines.remove(indexOfOpeningDelimiter);
		}
	}
	
	private void inactivatePreceding(final List<Inline> inlines) {
		for (int idx= inlines.size() - 1; idx >= 0; idx--) {
			final Inline inline= inlines.get(idx);
			if (inline instanceof PotentialBracketOpenDelimiter) {
				final PotentialBracketOpenDelimiter openDelimiter= (PotentialBracketOpenDelimiter)inline;
				if (openDelimiter.isLinkDelimiter()) {
					if (openDelimiter.isActive()) {
						openDelimiter.setInactive();
					}
					else {
						return;
					}
				}
			}
		}
	}
	
	private void replaceDelimiter(final List<Inline> inlines,
			final int index, final PotentialBracketOpenDelimiter delimiter) {
		if (Characters.append(inlines, index, delimiter)) {
			inlines.remove(index);
			return;
		}
		inlines.set(index,
				new Characters(delimiter.getLine(),
						delimiter.getStartOffset(), delimiter.getLength(), delimiter.getCursorLength(),
						delimiter.getText() ));
	}
	
}
