/*=============================================================================#
 # Copyright (c) 2015, 2022 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.jcommons.collections.CollectionUtils.addNonNull;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.indexOfNonWhitespace;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.indexOfNonWhitespaceEnd;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References.REF_LABEL_REGEX;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;
import org.eclipse.mylyn.wikitext.parser.HeadingAttributes;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.LabelInfo;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementDetail;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceHeadingAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceTextBlockAttributes;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Lines;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References.LinkDestination;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.TextSegment;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ParagraphAndTheLikeBlock.ParagraphOrTheLikeBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Inline;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.InlineParser;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.ReferenceDefinition;


@NonNullByDefault
public class ParagraphAndTheLikeBlock extends ParagraphBlock<ParagraphOrTheLikeBlockNode> {
	
	
	static final class ParagraphOrTheLikeBlockNode extends ParagraphBlock.ParagraphBlockNode<ParagraphAndTheLikeBlock> {
		
		private byte headingLevel;
		
		private @Nullable Object refDef;
		
		public ParagraphOrTheLikeBlockNode(final ParagraphAndTheLikeBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
		
		@Override
		public boolean isParagraph() {
			return (this.headingLevel == 0);
		}
		
	}
	
	
	private static final Pattern SETEXT_UNDERLINE_PATTERN= Pattern.compile(
			"(=+|-+)[ \t]*", //$NON-NLS-1$
			Pattern.DOTALL );
	private static final int SETEXT_UNDERLINE_GROUP= 1;
	
	
	private static final Pattern REFDEF_LABEL_PATTERN= Pattern.compile(
			"(" + REF_LABEL_REGEX + ")" //$NON-NLS-1$ //$NON-NLS-2$
					+ "(?:" + "(\\]\\:)" //$NON-NLS-1$ //$NON-NLS-2$
							+ "(?:[ \t]*" //$NON-NLS-1$
									+ "([^ \t].+)?" //$NON-NLS-1$
							+ ")?" //$NON-NLS-1$
					+ ")?" //$NON-NLS-1$
			);
	
	private static final int REFDEF_LABEL_CHARS_GROUP= 1;
	private static final int REFDEF_LABEL_DEFSEP_GROUP= 2;
	private static final int REFDEF_LABEL_LINK_GROUP= 3;
	
	
	private static class ReferenceDefParser {
		
		private static final byte START= 0;
		private static final byte IN_LABEL= 1;
		private static final byte DEFSEP= 2;
		private static final byte LINK= 3;
		private static final byte IN_TITLE= 4;
		private static final byte COMPLETE= 5;
		
		private final ParagraphOrTheLikeBlockNode node;
		
		private byte state;
		private int stateDetail;
		private String label= "";
		private int labelStartOffset= -1;
		private int labelEndOffset;
		private String escapedUri;
		private @Nullable String escapedTitle;
		private int endOffset;
		
		private ReferenceDefParser(final ParagraphOrTheLikeBlockNode node) {
			this.node= node;
			this.state= START;
		}
		
		public @Nullable ReferenceDefParser process(final SourceBlockBuilder builder) {
			final LineSequence lineSequence= builder.getLineSequence();
			final byte orgState= this.state;
			
			{	final Line line= nonNullAssert(lineSequence.getCurrentLine());
				switch (this.state) {
				case START:
					checkLabel(line, line.getIndentLength() + 1);
					break;
				case IN_LABEL:
					checkLabel(line, line.getIndentLength());
					break;
				case DEFSEP:
					checkLink(line, line.getIndentLength());
					break;
				case LINK:
					checkTitleStart(line, line.getIndentLength());
					break;
				case IN_TITLE:
					checkTitleEnd(line, line.getIndentLength());
					break;
				default:
					this.state= -1;
					break;
				}
			}
			{	switch (this.state) {
				case IN_LABEL:
				case DEFSEP:
				case LINK:
				case COMPLETE:
					lineSequence.advance();
					return this;
				case IN_TITLE:
					this.state= completeInTitle(builder);
					break;
				default:
					this.state= -1;
					break;
				}
			}
			
			if (this.state == -1 && orgState == LINK) {
				this.state= COMPLETE;
			}
			return (this.state == COMPLETE) ? this : null;
		}
		
		private byte completeInTitle(final SourceBlockBuilder builder) {
			assert (this.state == IN_TITLE);
			final List<String> titleSegments= new ArrayList<>();
			addNonNull(titleSegments, this.escapedTitle);
			final LineSequence lineSequence= builder.getLineSequence();
			final LineSequence lookAhead= lineSequence.lookAhead();
			do {
				lookAhead.advance();
				this.escapedTitle= null;
				checkTitleEnd(builder, lookAhead);
				addNonNull(titleSegments, this.escapedTitle);
			} while (this.state == IN_TITLE);
			if (this.state == COMPLETE) {
				this.escapedTitle= builder.getParseHelper().joinLines(titleSegments);
				advanceLinesUpto(lineSequence, lookAhead.getCurrentLine().getLineNumber() + 1);
				return COMPLETE;
			}
			return -1;
		}
		
		public boolean isComplete() {
			return (this.state == LINK || this.state == COMPLETE);
		}
		
		public boolean isCompleteNoContinue() {
			return (this.state == COMPLETE);
		}
		
		private ReferenceDefinition createInline(final ParagraphOrTheLikeBlockNode node,
				final ProcessingContext context) {
			assert (isComplete());
			final LabelInfo labelInfo= new LabelInfo(context.normalizeLabel(this.label),
					this.labelStartOffset, this.labelEndOffset );
			final String uri= References.normalizeUri(
					context.getHelper().replaceEscaping(this.escapedUri) );
			final String escapedTitle= this.escapedTitle;
			final String title= (escapedTitle != null && !escapedTitle.isEmpty()) ?
					context.getHelper().replaceEscaping(escapedTitle) : null;
			final Line startLine= node.getLines().get(0);
			final int startOffset= startLine.getStartOffset() + startLine.getIndentLength();
			return new ReferenceDefinition(startLine, startOffset, this.endOffset - startOffset,
					uri, title, labelInfo );
		}
		
		private void checkLabel(final Line line, final int offset) {
			assert (this.state >= START && this.state <= IN_LABEL);
			final Matcher matcher= line.setup(this.node.getType().refDefLabelMatcher,
					offset, line.getText().length() );
			if (!matcher.matches()) {
				this.state= -1;
				return;
			}
			final String label0= matcher.group(REFDEF_LABEL_CHARS_GROUP);
			this.stateDetail += label0.length();
			if (this.stateDetail >= 1000) {
				this.state= -1;
				return;
			}
			this.label= (this.label.isEmpty()) ? label0 : (this.label + ' ' + label0);
			if (this.labelStartOffset == -1) {
				final int index= indexOfNonWhitespace(line.getText(),
							matcher.start(REFDEF_LABEL_CHARS_GROUP), matcher.end(REFDEF_LABEL_CHARS_GROUP));
				if (index >= 0) {
					this.labelStartOffset= line.getStartOffset() + index;
					this.labelEndOffset= line.getStartOffset() + indexOfNonWhitespaceEnd(line.getText(),
							matcher.start(REFDEF_LABEL_CHARS_GROUP), matcher.end(REFDEF_LABEL_CHARS_GROUP));
				}
			}
			else {
				final int index= indexOfNonWhitespaceEnd(line.getText(),
						matcher.start(REFDEF_LABEL_CHARS_GROUP), matcher.end(REFDEF_LABEL_CHARS_GROUP));
				if (index >= 0) {
					this.labelEndOffset= line.getStartOffset() + index;
				}
			}
			if (matcher.start(REFDEF_LABEL_DEFSEP_GROUP) == -1) {
				this.state= IN_LABEL;
				return;
			}
			if (this.labelStartOffset == -1) {
				this.state= -1;
				return;
			}
			this.state= DEFSEP;
			int linkStart;
			if ((linkStart= matcher.start(REFDEF_LABEL_LINK_GROUP)) != -1) {
				checkLink(line, linkStart);
			}
		}
		
		private void checkLink(final Line line, int offset) {
			assert (this.state == DEFSEP);
			final String text= line.getText();
			final LinkDestination destination= References.readLinkDestination(line.getText(), offset);
			if (destination == null) {
				this.state= -1;
				return;
			}
			this.escapedUri= destination.getEscapedUri();
			this.endOffset= line.getStartOffset() + destination.getEndOffset();
			
			offset= destination.getEndOffset();
			while (offset < text.length() && text.charAt(offset) == ' ') {
				offset++;
			}
			this.state= LINK;
			if (offset < text.length()) {
				checkTitleStart(line, offset);
			}
		}
		
		private void checkTitleStart(final Line line, int offset) {
			assert (this.state == LINK);
			final String text= line.getText();
			switch (text.charAt(offset++)) {
			case '"':
				this.stateDetail= '"';
				break;
			case '\'':
				this.stateDetail= '\'';
				break;
			case '(':
				this.stateDetail= '(';
				break;
			default:
				this.state= -1;
				return;
			}
			this.state= IN_TITLE;
			checkTitleEnd(line, offset);
		}
		
		private void checkTitleEnd(final SourceBlockBuilder builder, final LineSequence lineSequence) {
			assert (this.state == IN_TITLE);
			final Line line= lineSequence.getCurrentLine();
			if (line == null || line.isBlank()
					|| this.node.getType().isAnotherBlockStart(lineSequence, builder.getSourceBlocks(),
							builder.getCurrentNode() )) {
				this.state= -1;
				return;
			}
			checkTitleEnd(line, line.getIndentLength());
		}
		
		private void checkTitleEnd(final Line line, final int startOffset) {
			assert (this.state == IN_TITLE);
			final String text= line.getText();
			final char sep= (char)this.stateDetail;
			int offset= startOffset;
			while (offset < text.length()) {
				final char c= text.charAt(offset++);
				if (c == '\\') {
					offset++;
				}
				else if (c == sep) {
					if (indexOfNonWhitespace(text, offset, text.length()) == -1) {
						this.state= COMPLETE;
						this.endOffset= line.getStartOffset() + offset;
						this.escapedTitle= text.substring(startOffset, offset - 1);
						return;
					}
					else {
						this.state= -1;
						return;
					}
				}
			}
			// this.state= IN_TITLE;
			this.escapedTitle= text.substring(startOffset, offset);
		}
		
	}
	
	
	private final Matcher refDefLabelMatcher= REFDEF_LABEL_PATTERN.matcher(""); //$NON-NLS-1$
	
	private final Matcher setextMatcher= SETEXT_UNDERLINE_PATTERN.matcher(""); //$NON-NLS-1$
	
	
	public ParagraphAndTheLikeBlock() {
		super();
	}
	
	public ParagraphAndTheLikeBlock(final ImCollection<Class<? extends SourceBlockType<?>>> interruptExclusions) {
		super(interruptExclusions);
	}
	
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		final LineSequence lineSequence= builder.getLineSequence();
		
		final ParagraphOrTheLikeBlockNode node= new ParagraphOrTheLikeBlockNode(this, builder);
		ReferenceDefParser referenceDefParser= null;
		
		{	final Line line= nonNullAssert(lineSequence.getCurrentLine());
			if (!line.isBlank()
					&& line.getText().charAt(line.getIndentLength()) == '['
					&& (referenceDefParser= new ReferenceDefParser(node).process(builder)) != null) {
				if (referenceDefParser.isCompleteNoContinue()) {
					node.refDef= referenceDefParser;
					return;
				}
			}
			else {
				lineSequence.advance();
			}
		}
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null
					&& !line.isBlank()) {
				Matcher setextMatcher;
				if (!line.isLazy() && line.getIndent() < 4
						&& (setextMatcher= line.setupIndent(this.setextMatcher)).matches() ) {
					if (referenceDefParser != null && referenceDefParser.isComplete()) {
						node.refDef= referenceDefParser;
						return;
					}
					node.headingLevel= getHeadingLevel(setextMatcher, line);
					lineSequence.advance();
					return;
				}
				if (isAnotherBlockStart(lineSequence, builder.getSourceBlocks(), node)) {
					break;
				}
				if (referenceDefParser != null && (referenceDefParser= referenceDefParser.process(builder)) != null) {
					if (referenceDefParser.isCompleteNoContinue()) {
						node.refDef= referenceDefParser;
						return;
					}
				}
				else {
					lineSequence.advance();
				}
				continue;
			}
			break;
		}
		if (referenceDefParser != null && referenceDefParser.isComplete()) {
			node.refDef= referenceDefParser;
		}
	}
	
	private byte getHeadingLevel(final Matcher matcher, final Line line) {
		switch (line.getText().charAt(matcher.start(SETEXT_UNDERLINE_GROUP))) {
		case '=':
			return 1;
		case '-':
			return 2;
		default:
			throw new IllegalStateException();
		}
	}
	
	private ReferenceDefinition getReferenceDefinition(final ProcessingContext context, final ParagraphOrTheLikeBlockNode node) {
		final Object obj= nonNullAssert(node.refDef);
		final ReferenceDefinition inline;
		if (obj instanceof ReferenceDefParser) {
			inline= ((ReferenceDefParser)obj).createInline(node, context);
			node.refDef= inline;
		}
		else {
			inline= (ReferenceDefinition)obj;
		}
		return inline;
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final ParagraphOrTheLikeBlockNode node) {
		if (node.refDef != null) {
			final ReferenceDefinition inline= getReferenceDefinition(context, node);
			context.addReferenceDef(inline.getLabel(), inline.getHref(), inline.getTitle());
		}
	}
	
	@Override
	public void emit(final ProcessingContext context, final ParagraphOrTheLikeBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		if (node.refDef != null) {
			emitRefDef(context, node, locator, builder);
		}
		else if (node.headingLevel > 0) {
			emitHeading(context, node, locator, builder);
		}
		else {
			emitParagraph(context, node, true, locator, builder);
		}
	}
	
	private void emitHeading(final ProcessingContext context, final ParagraphOrTheLikeBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final ImList<Line> lines= node.getLines();
		
		final ImList<Line> textLines= Lines.trimWhitespace(lines, 0, lines.size() - 1);
		if (!textLines.isEmpty()) {
			final TextSegment textSegment= new TextSegment(textLines);
			
			final SourceHeadingAttributes attributes= new SourceHeadingAttributes(0);
			
			final InlineParser inlineParser= context.getInlineParser();
			final String headingText= inlineParser.toStringContent(context, textSegment);
			attributes.setId(context.generateHeadingId(node.headingLevel, headingText));
			
			locator.setBlockBegin(node);
			builder.beginHeading(node.headingLevel, attributes);
			
			final List<Inline> inlines= inlineParser.parse(context, textSegment, true);
			InlineParser.emit(context, inlines, locator, builder);
			
			locator.setBlockEnd(node);
			builder.endHeading();
		}
		else {
			locator.setBlockBegin(node);
			builder.beginHeading(node.headingLevel, new HeadingAttributes());
			locator.setBlockEnd(node);
			builder.endHeading();
		}
	}
	
	private void emitRefDef(final ProcessingContext context, final ParagraphOrTheLikeBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		if (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) {
			final TextSegment textSegment= new TextSegment(node.getLines());
			
			final List<Inline> inlines= ImCollections.newList(getReferenceDefinition(context, node));
			
			locator.setBlockBegin(node);
			final SourceTextBlockAttributes attributes= new SourceTextBlockAttributes(textSegment.getLines(),
					SourceElementDetail.END_UNCLOSED );
			builder.beginBlock(BlockType.PARAGRAPH, attributes);
			
			InlineParser.emit(context, inlines, locator, builder);
			
			locator.setBlockEnd(node);
			builder.endBlock();
		}
	}
	
}
