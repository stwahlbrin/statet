/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.BlockWithNestedBlocks;


@NonNullByDefault
public class SourceBlockNode<TType extends SourceBlockType> {
	
	
	private final TType type;
	
	private final @Nullable SourceBlockNode<?> parent;
	private final List<SourceBlockNode<?>> nested;
	
	private ImList<Line> lines;
	
	
	public SourceBlockNode(final TType type, final SourceBlockBuilder builder) {
		this.type= type;
		this.parent= builder.getParentNode();
		
		this.nested= (type instanceof BlockWithNestedBlocks) ?
				new ArrayList<>() :
				ImCollections.emptyList();
		if (this.parent != null) {
			this.parent.nested.add(this);
		}
		builder.beginNode(this);
	}
	
	
	public TType getType() {
		return this.type;
	}
	
	
	public @Nullable SourceBlockNode<?> getParent() {
		return this.parent;
	}
	
	public List<SourceBlockNode<?>> getNested() {
		return this.nested;
	}
	
	public ImList<Line> getLines() {
		return this.lines;
	}
	
	void setLines(final ImList<Line> lines) {
		this.lines= lines;
	}
	
	
	public boolean isParagraph() {
		return false;
	}
	
	public boolean isEmpty() {
		return false;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("SourceBlock");
		sb.append(" type= ").append(this.type);
		sb.append(" (num nested= ").append(this.nested.size()).append(")");
		if (this.lines != null) {
			sb.append("\n").append(this.lines.toString());
		}
		return sb.toString();
	}
	
}
