/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * A fixed pool status including note states.
 * 
 * @param <N> type of node state items, see {@link #createNodeState(PoolNodeItem)}
 * 
 * @since 2.0
 */
@NonNullByDefault
public abstract class PoolStatus<N> {
	
	
	private final RServiPoolManager.Counter NO_MANAGER= new RServiPoolManager.Counter();
	
	
	protected final PoolServer server;
	
	/*-- init in ::check() --*/
	private Instant stamp= nonNullLateInit();
	private RServiPoolManager.Counter counter= nonNullLateInit();
	private List<N> nodeStates= nonNullLateInit();
	
	
	public PoolStatus(final PoolServer server) {
		this.server= nonNullAssert(server);
	}
	
	
	/**
	 * @since 4.5
	 */
	protected Instant getStatusStamp() {
		return this.stamp;
	}
	
	public synchronized int getNumInUse() {
		check();
		return this.counter.numInUse;
	}
	
	public synchronized int getNumIdling() {
		check();
		return this.counter.numIdling;
	}
	
	public synchronized int getNumTotal() {
		check();
		return this.counter.numTotal;
	}
	
	public synchronized int getMaxInUse() {
		check();
		return this.counter.maxInUse;
	}
	
	public synchronized int getMaxIdling() {
		check();
		return this.counter.maxIdling;
	}
	
	public synchronized int getMaxTotal() {
		check();
		return this.counter.maxTotal;
	}
	
	
	public synchronized List<N> getNodeStates() {
		check();
		return this.nodeStates;
	}
	
	
	protected void check() {
	}
	
	
	protected void refresh(final @Nullable RServiPoolManager manager, final Instant stamp) {
		final List<N> list;
		RServiPoolManager.Counter counter;
		
		if (manager == null) {
			counter= this.NO_MANAGER;
			list= Collections.emptyList();
		}
		else {
			final Collection<? extends PoolNodeObject> objects= manager.getPoolNodeObjects();
			counter= manager.getCounter();
			list= new ArrayList<>();
			counter.numIdling= 0;
			counter.numInUse= 0;
			counter.numTotal= 0;
			for (final PoolNodeObject nodeObj : objects) {
				final PoolNodeItem item= createPoolItem(nodeObj, stamp);
				final N nodeState= createNodeState(item);
				switch (item.getState()) {
				case ALLOCATED:
					counter.numInUse++;
					break;
				case IDLING:
					counter.numIdling++;
					break;
				default:
					break;
				}
				counter.numTotal++;
				list.add(nodeState);
			}
		}
		
		this.stamp= stamp;
		this.counter= counter;
		this.nodeStates= list;
	}
	
	protected PoolNodeItem createPoolItem(final PoolNodeObject node, final Instant stamp) {
		return new PoolNodeItem(node, stamp);
	}
	
	protected abstract N createNodeState(PoolNodeItem item);
	
}
