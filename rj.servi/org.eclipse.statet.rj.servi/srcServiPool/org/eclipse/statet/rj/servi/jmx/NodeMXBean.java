/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.jmx;

import java.time.Instant;

import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface NodeMXBean {
	
	
	String getId();
	
	/**
	 * @since 4.5
	 */
	Instant getCreationTime();
	
	
	NodeStateMX getState();
	
	boolean isConsoleEnabled();
	void setConsoleEnabled(boolean enable) throws OperationsException;
	
	
	@DisplayName("Stop using the default timeout")
	void stop() throws OperationsException;
	@DisplayName("Stop using given timeout (millisec)")
	void stop(long timeoutMillis) throws OperationsException;
	@DisplayName("Stop directly")
	void kill() throws OperationsException;
	
}
