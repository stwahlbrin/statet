/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.node;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.NoSuchElementException;

import javax.security.auth.login.LoginException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.server.ServerLogin;
import org.eclipse.statet.rj.servi.RServi;


/**
 * Generic interface to a pool providing RServi instances.
 */
@NonNullByDefault
public interface RServiPool extends Remote {
	
	
	/**
	 * Requests a {@link RServi} instance from this pool.
	 * 
	 * <p>The R services returned by this method are available for exclusive usage by the caller
	 * (consumer). The consumer is responsible to return it to the pool by {@link RServi#close()
	 * closing} the RServi.</p>
	 * 
	 * @param name a name which can be used to identify the client
	 * @param login not yet used
	 * @return an R services node
	 * 
	 * @throws NoSuchElementException if there is currently no free RServi instance available. A
	 *     later call with the same configuration can be successfully.
	 * @throws RjException when an server error occurs (retry not promising)
	 * @throws LoginException when the login failed
	 * @throws RemoteException when communication or runtime error occurs
	 */
	RServi getRServi(String name, @Nullable ServerLogin login)
			throws NoSuchElementException, RjException, LoginException, RemoteException;
	
}
