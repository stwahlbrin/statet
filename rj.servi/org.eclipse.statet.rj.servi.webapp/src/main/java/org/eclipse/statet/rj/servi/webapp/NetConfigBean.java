/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import javax.annotation.PostConstruct;
import javax.management.OperationsException;
import javax.naming.directory.InvalidAttributeValueException;

import org.eclipse.statet.rj.servi.pool.NetConfig;
import org.eclipse.statet.rj.servi.pool.PoolServer;


public class NetConfigBean extends NetConfig {
	
	
	public NetConfigBean() {
	}
	
	
	@PostConstruct
	public void init() {
		FacesUtils.loadFromFile(this, false);
	}
	
	
	public String getEffectivePoolAddress() {
		final String address= getPoolAddress(this, FacesUtils.getPoolId());
		return (address != null) ? address : "";
	}
	
	
	public String actionLoadCurrent() {
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		synchronized (this) {
			poolServer.getNetConfig(this);
			FacesUtils.validate(this);
		}
		
		return RJWeb.NETCONFIG_NAV;
	}
	
	public String actionLoadDefaults() throws InvalidAttributeValueException {
		synchronized (this) {
			load(new NetConfigBean());
			FacesUtils.validate(this);
		}
		
		return RJWeb.NETCONFIG_NAV;
	}
	
	public String actionRestart() {
		final NetConfig config= new NetConfig(this);
		
		if (!FacesUtils.validate(config)) {
			return null;
		}
		
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		poolServer.setNetConfig(config);
		try {
			poolServer.restart();
		}
		catch (final OperationsException e) {
			FacesUtils.addErrorMessage(null, e.getMessage());
		}
		return null;
	}
	
	public String actionSave() {
		FacesUtils.saveToFile(this);
		return null;
	}
	
}
