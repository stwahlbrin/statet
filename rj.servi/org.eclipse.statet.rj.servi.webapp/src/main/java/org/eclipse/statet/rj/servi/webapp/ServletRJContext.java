/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;

import org.eclipse.statet.rj.RjInitFailedException;
import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.server.util.RJContext;


public class ServletRJContext extends RJContext {
	
	
	private final ServletContext servletContext;
	
	
	public ServletRJContext(final ServletContext context) throws RjInitFailedException {
		this.servletContext= context;
	}
	
	
	@Override
	public String getServerPolicyFilePath() throws RjInvalidConfigurationException {
		String path= this.servletContext.getRealPath("WEB-INF/lib");
		final int length= path.length();
		if (length == 0 || (path.charAt(length-1) != '/' && path.charAt(length-1) != File.separatorChar)) {
			path+= File.separatorChar;
		}
		return path + "security.policy";
	}
	
	
	@Override
	protected String getPropertiesDirPath() {
		return "/WEB-INF/";
	}
	
	@Override
	protected InputStream getInputStream(final String path) throws IOException {
		return this.servletContext.getResourceAsStream(path);
	}
	
	@Override
	protected OutputStream getOutputStream(final String path) throws IOException {
		final String realPath= this.servletContext.getRealPath(path);
		if (realPath == null) {
			throw new IOException("Writing to '" + path + "' not supported.");
		}
		final File file= new File(realPath);
		if (!file.exists()) {
			file.createNewFile();
		}
		return new FileOutputStream(file, false);
	}
	
}
