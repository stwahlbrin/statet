/*
 * Copyright (c) 2018, 2022 http://www.snakeyaml.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.scanner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class CharConstants {
	
	private static final String ALPHA_S = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
	
	private static final String LINEBR_S = "\n";
	private final static String FULL_LINEBR_S = "\r" + LINEBR_S;
	private static final String NULL_OR_LINEBR_S = "\0" + FULL_LINEBR_S;
	private static final String NULL_BL_T_LINEBR_S = " \t" + NULL_OR_LINEBR_S;
	
	public static final CharConstants LINEBR = new CharConstants(LINEBR_S);
	public static final CharConstants NULL_BL_T_LINEBR = new CharConstants(NULL_BL_T_LINEBR_S);
	
	public static final CharConstants ALPHA = new CharConstants(ALPHA_S);
	
	private static final int ASCII_SIZE = 128;
	boolean[] contains = new boolean[ASCII_SIZE];
	
	private CharConstants(final String content) {
		Arrays.fill(this.contains, false);
		for (int i = 0; i < content.length(); i++) {
			final int c = content.codePointAt(i);
			this.contains[c] = true;
		}
	}
	
	public boolean has(final int c) {
		return (c < ASCII_SIZE) && this.contains[c];
	}
	
	public boolean hasNo(final int c) {
		return !has(c);
	}
	
	public boolean has(final int c, final String additional) {
		return has(c) || additional.indexOf(c) != -1;
	}
	
	public boolean hasNo(final int c, final String additional) {
		return !has(c, additional);
	}
	
	
	/**
	 * {@code b-char | EOF }
	 */
	public static boolean isEol(final int ch) {
		switch (ch) {
		case '\n':
		case '\r':
		case '\0':
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * {@code s-white}
	 */
	public static boolean isWhiteSpace(final int ch) {
		switch (ch) {
		case ' ':
		case '\t':
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * {@code s-white | b-char | EOF }
	 */
	public static boolean isWhiteSpaceOrEol(final int ch) {
		switch (ch) {
		case '\n':
		case '\r':
		case '\0':
		case ' ':
		case '\t':
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * {@code ns-hex-digit}
	 */
	public static boolean isHexDigit(final int ch) {
		switch (ch) {
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
			return true;
		default:
			return false;
		}
	}
	
	public static boolean isFlowCollectionControl(final int c) {
		switch (c) {
		case '{':
		case '}':
		case '[':
		case ']':
		case ',':
			return true;
		default:
			return false;
		}
	}
	
	public static boolean isFlowCollectionEntryEndControl(final int c) {
		switch (c) {
		case '}':
		case ']':
		case ',':
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * {@code ns-uri-char}
	 */
	public static boolean isUriChar(final int c) {
		switch (c) {
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'v':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
		case '-':
		case '#':
		case ';':
		case '/':
		case '?':
		case ':':
		case '@':
		case '&':
		case '=':
		case '+':
		case '$':
		case ',':
		case '_':
		case '.':
		case '!':
		case '~':
		case '*':
		case '\'':
		case '(':
		case ')':
		case '[':
		case ']':
		case '%':
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * {@code ns-tag-char}
	 */
	public static boolean isTagChar(final int c) {
		switch (c) {
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'v':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
		case '-':
		case '#':
		case ';':
		case '/':
		case '?':
		case ':':
		case '@':
		case '&':
		case '=':
		case '+':
		case '$':
		case '_':
		case '.':
		case '~':
		case '*':
		case '\'':
		case '%':
			return true;
		default:
			return false;
		}
	}
	
	
	/**
	 * A mapping from an escaped character in the input stream to the character
	 * that they should be replaced with.
	 * <p>
	 * YAML defines several common and a few uncommon escape sequences.
	 */
	private static final Map<Character, Character> ESCAPE_REPLACEMENTS;
	
	/**
	 * A mapping from a character to be escaped to its code in the output stream. (used for emitting)
	 * It contains the same as ESCAPE_REPLACEMENTS except ' ' and '/'
	 * <p>
	 * YAML defines several common and a few uncommon escape sequences.
	 */
	private static final Map<Character, Character> ESCAPES;
	
	
	public static @Nullable Character getEscapeReplacement(final Character c) {
		return ESCAPE_REPLACEMENTS.get(c);
	}
	
	public static @Nullable Character getEscapeChar(final Character c) {
		return ESCAPES.get(c);
	}
	
	/**
	 * A mapping from a character to a number of bytes to read-ahead for that
	 * escape sequence. These escape sequences are used to handle unicode
	 * escaping in the following formats, where H is a hexadecimal character:
	 * <pre>
	 * &#92;xHH         : escaped 8-bit Unicode character
	 	92;uHHHH       : escaped 16-bit Unicode character
	 * &#92;UHHHHHHHH   : escaped 32-bit Unicode character
	 * </pre>
	 */
	public static int getEscapeCodeBytes(final char c) {
		switch (c) {
		case 'x': // 8-bit Unicode
			return 2;
		case 'u': // 16-bit Unicode
			return 4;
		case 'U': // 32-bit Unicode (Supplementary characters are supported)
			return 8;
		default:
			return -1;
		}
	}
	
	static {
		final Map<Character, Character> escapeReplacements = new HashMap<>();
		final Map<Character, Character> escapes = new HashMap<>();
		escapeReplacements.put('0', '\0');// ASCII null
		escapeReplacements.put('a', '\u0007');// ASCII bell
		escapeReplacements.put('b', '\u0008'); // ASCII backspace
		escapeReplacements.put('t', '\u0009'); // ASCII horizontal tab
		escapeReplacements.put('n', '\n');// ASCII newline (line feed; &#92;n maps to 0x0A)
		escapeReplacements.put('v', '\u000B');// ASCII vertical tab
		escapeReplacements.put('f', '\u000C');// ASCII form-feed
		escapeReplacements.put('r', '\r');// carriage-return (&#92;r maps to 0x0D)
		escapeReplacements.put('e', '\u001B');// ASCII escape character (Esc)
		escapeReplacements.put(' ', '\u0020');// ASCII space
		escapeReplacements.put('"', '\"');// ASCII double-quote
		escapeReplacements.put('/', '/');// ASCII slash (#x2F), for JSON compatibility.
		escapeReplacements.put('\\', '\\');// ASCII backslash
		escapeReplacements.put('N', '\u0085');// Unicode next line
		escapeReplacements.put('_', '\u00A0');// Unicode non-breaking-space
		escapeReplacements.put('L', '\u2028');// Unicode line-separator
		escapeReplacements.put('P', '\u2029');// Unicode paragraph separator
		
		escapeReplacements.entrySet().stream()
				.filter(entry -> entry.getKey() != ' ' && entry.getKey() != '/')
				.forEach(entry -> escapes.put(entry.getValue(), entry.getKey()));
		ESCAPE_REPLACEMENTS = Collections.unmodifiableMap(escapeReplacements);
		ESCAPES = Collections.unmodifiableMap(escapes);
	}
	
}
