/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.snakeyaml.scanner;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.source.StatusDetail;


@NonNullByDefault
public class SyntaxProblem {
	
	
	private final byte context;
	private final int status;
	
	private final @Nullable StatusDetail statusDetail;
	
	
	public SyntaxProblem(final byte context,
			final int status, final @Nullable StatusDetail statusDetail) {
		this.context= context;
		this.status= status;
		this.statusDetail= statusDetail;
	}
	
	
	public byte getContext() {
		return this.context;
	}
	
	public int getStatus() {
		return this.status;
	}
	
	public @Nullable StatusDetail getStatusDetail() {
		return this.statusDetail;
	}
	
	
}
