/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.snakeyaml.scanner;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ScanningContext {
	
	// -- context --
	
	public static final byte SCANNING_FOR_NEXT_TOKEN= 1;
	public static final byte SCANNING_DIRECTIVE= 3;
	public static final byte SCANNING_YAML_DIRECTIVE= 4;
	public static final byte SCANNING_TAG_DIRECTIVE= 5;
	public static final byte SCANNING_SIMPLE_KEY= 7;
	public static final byte SCANNING_ANCHOR= 8;
	public static final byte SCANNING_ALIAS= 9;
	public static final byte SCANNING_TAG= 10;
	public static final byte SCANNING_BLOCK_SCALAR= 11;
	public static final byte SCANNING_SQUOTED_SCALAR= 12;
	public static final byte SCANNING_DQUOTED_SCALAR= 13;
	public static final byte SCANNING_PLAIN_SCALAR= 14;
	
	
	public static String getString(final byte state) {
		switch (state) {
		case SCANNING_FOR_NEXT_TOKEN:
			return "while scanning for the next token";
		case SCANNING_SIMPLE_KEY:
			return "while scanning a simple key";
		case SCANNING_DIRECTIVE:
			return "while scanning a directive";
		case SCANNING_ANCHOR:
			return "while scanning an anchor";
		case SCANNING_ALIAS:
			return "while scanning an alias";
		case SCANNING_TAG:
			return "while scanning a tag";
		case SCANNING_BLOCK_SCALAR:
			return "while scanning a block scalar";
		case SCANNING_SQUOTED_SCALAR:
			return "while scanning a single-quoted scalar";
		case SCANNING_DQUOTED_SCALAR:
			return "while scanning a double-quoted scalar";
		case SCANNING_PLAIN_SCALAR:
			return "while scanning a plain scalar";
		default:
			return null;
		}
	}
	
}
