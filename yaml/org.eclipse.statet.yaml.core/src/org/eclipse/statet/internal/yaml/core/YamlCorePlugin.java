/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;

import org.eclipse.statet.internal.yaml.core.model.YamlModelManagerImpl;
import org.eclipse.statet.yaml.core.BasicYamlCoreAccess;
import org.eclipse.statet.yaml.core.YamlCoreAccess;


public class YamlCorePlugin extends Plugin {
	
	
	private static YamlCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static YamlCorePlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private YamlModelManagerImpl yamlModelManager;
	
	private volatile YamlCoreAccess workbenchAccess;
	private volatile YamlCoreAccess defaultsAccess;
	
	
	public YamlCorePlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.yamlModelManager= new YamlModelManagerImpl();
		
		synchronized (this) {
			this.started= true;
		}
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
			if (this.yamlModelManager != null) {
				this.yamlModelManager.dispose();
				this.yamlModelManager= null;
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	private void checkStarted() {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
	}
	
	public YamlModelManagerImpl getYamlModelManager() {
		return this.yamlModelManager;
	}
	
	public YamlCoreAccess getWorkbenchAccess() {
		YamlCoreAccess access= this.workbenchAccess;
		if (access == null) {
			synchronized (this) {
				access= this.workbenchAccess;
				if (access == null) {
					checkStarted();
					access= this.workbenchAccess= new BasicYamlCoreAccess(
							EPreferences.getInstancePrefs() );
				}
			}
		}
		return access;
	}
	
	public YamlCoreAccess getDefaultsAccess() {
		YamlCoreAccess access= this.defaultsAccess;
		if (access == null) {
			synchronized (this) {
				access= this.defaultsAccess;
				if (access == null) {
					checkStarted();
					access= this.defaultsAccess= new BasicYamlCoreAccess(
							EPreferences.getDefaultPrefs() );
				}
			}
		}
		return access;
	}
	
}
