/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.core.YamlReconciler;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementDelta;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.core.impl.AbstractModelEventJob;
import org.eclipse.statet.ltk.model.core.impl.AbstractModelManager;
import org.eclipse.statet.yaml.core.model.YamlChunkElement;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.YamlModelManager;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;
import org.eclipse.statet.yaml.core.model.build.YamlSourceUnitModelContainer;


@NonNullByDefault
public class YamlModelManagerImpl extends AbstractModelManager implements YamlModelManager {
	
	
	private static class ModelDelta implements LtkModelElementDelta {
		
		private final int level;
		private final LtkModelElement<?> element;
		private final @Nullable SourceUnitModelInfo oldInfo;
		private final @Nullable AstInfo oldAst;
		private final @Nullable SourceUnitModelInfo newInfo;
		private final @Nullable AstInfo newAst;
		
		
		public ModelDelta(final LtkModelElement<?> element,
				final @Nullable SourceUnitModelInfo oldInfo, final @Nullable SourceUnitModelInfo newInfo) {
			this.level= ModelManager.MODEL_FILE;
			this.element= element;
			this.oldInfo= oldInfo;
			this.oldAst= (oldInfo != null) ? oldInfo.getAst() : null;
			this.newInfo= newInfo;
			this.newAst= (newInfo != null) ? newInfo.getAst() : null;
		}
		
		
		@Override
		public LtkModelElement<?> getModelElement() {
			return this.element;
		}
		
		@Override
		public @Nullable AstInfo getOldAst() {
			return this.oldAst;
		}
		
		@Override
		public @Nullable AstInfo getNewAst() {
			return this.newAst;
		}
		
	}
	
	public static class EventJob extends AbstractModelEventJob<SourceUnit, YamlSourceUnitModelInfo> {
		
		public EventJob(final YamlModelManagerImpl manager) {
			super(manager);
		}
		
		@Override
		protected LtkModelElementDelta createDelta(final Task task) {
			return new ModelDelta(task.getElement(), task.getOldInfo(), task.getNewInfo());
		}
		
		@Override
		protected void dispose() {
			super.dispose();
		}
		
	}
	
	
	private final EventJob eventJob= new EventJob(this);
	
	private final YamlReconciler reconciler= new YamlReconciler(this);
	
	
	public YamlModelManagerImpl() {
		super(YamlModel.YAML_TYPE_ID);
	}
	
	
	public void dispose() {
		this.eventJob.dispose();
	}
	
	
	public EventJob getEventJob() {
		return this.eventJob;
	}
	
	@Override
	public void reconcile(final SourceUnitModelContainer<?, ?> adapter,
			final int level, final IProgressMonitor monitor) {
		if (adapter instanceof YamlSourceUnitModelContainer) {
			this.reconciler.reconcile((YamlSourceUnitModelContainer)adapter, level, monitor);
		}
	}
	
	@Override
	public YamlSourceUnitModelInfo reconcile(final SourceUnit sourceUnit, final SourceUnitModelInfo modelInfo,
			@Nullable List<? extends YamlChunkElement> chunks,
			final int level, final IProgressMonitor monitor) {
		if (chunks == null) {
			chunks= ImCollections.emptyList();
		}
		return this.reconciler.reconcile(sourceUnit, modelInfo, chunks, level, monitor);
	}
	
}
