/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.yaml.core.model.YamlElementName;
import org.eclipse.statet.yaml.core.model.YamlLabelAccess;
import org.eclipse.statet.yaml.core.source.ast.YamlAstNode;


public class LabelAccess extends YamlLabelAccess {
	
	
	public final static int A_READ=                       0x00000000;
	public final static int A_WRITE=                      0x00000002;
	
	
	static class Shared {
		
		
		private final String label;
		
		private List<YamlLabelAccess> all;
		
		
		public Shared(final String label) {
			this.label= label;
			this.all= new ArrayList<>(8);
		}
		
		
		public void finish() {
			this.all= ImCollections.toList(this.all);
		}
		
		public List<YamlLabelAccess> getAll() {
			return this.all;
		}
		
	}
	
	
	private final Shared shared;
	
	private final YamlAstNode node;
	private final YamlAstNode nameNode;
	
	int flags;
	
	
	protected LabelAccess(final Shared shared, final YamlAstNode node, final YamlAstNode labelNode) {
		this.shared= shared;
		shared.all.add(this);
		this.node= node;
		this.nameNode= labelNode;
	}
	
	
	@Override
	public int getType() {
		return ANCHOR;
	}
	
	@Override
	public String getSegmentName() {
		return this.shared.label;
	}
	
	@Override
	public String getDisplayName() {
		return this.shared.label;
	}
	
	@Override
	public YamlElementName getNextSegment() {
		return null;
	}
	
	
	@Override
	public YamlAstNode getNode() {
		return this.node;
	}
	
	@Override
	public YamlAstNode getNameNode() {
		return this.nameNode;
	}
	
	@Override
	public List<? extends YamlLabelAccess> getAllInUnit() {
		return this.shared.all;
	}
	
	
	@Override
	public boolean isWriteAccess() {
		return ((this.flags & A_WRITE) != 0);
	}
	
}
