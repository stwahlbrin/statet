/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import org.eclipse.core.resources.IFile;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.impl.AbstractFilePersistenceSourceUnitFactory;
import org.eclipse.statet.yaml.core.model.YamlResourceSourceUnit;


/**
 * Factory for common YAML files
 */
@NonNullByDefault
public class YamlSourceUnitFactory extends AbstractFilePersistenceSourceUnitFactory {
	
	
	public YamlSourceUnitFactory() {
	}
	
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final IFile file) {
		return new YamlResourceSourceUnit(id, file);
	}
	
}
