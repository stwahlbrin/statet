/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.concurrent.CancellationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.core.model.SourceAnalyzer;
import org.eclipse.statet.internal.yaml.core.model.YamlModelManagerImpl;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.ltk.project.core.LtkProject;
import org.eclipse.statet.yaml.core.YamlCore;
import org.eclipse.statet.yaml.core.model.YamlChunkElement;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;
import org.eclipse.statet.yaml.core.model.build.YamlIssueReporter;
import org.eclipse.statet.yaml.core.model.build.YamlSourceUnitModelContainer;
import org.eclipse.statet.yaml.core.source.ast.SourceComponent;
import org.eclipse.statet.yaml.core.source.ast.YamlParser;


@NonNullByDefault
public class YamlReconciler {
	
	
	protected static final class Data {
		
		public final YamlSourceUnitModelContainer adapter;
		
		public final SourceUnit sourceUnit;
		
		public final SourceContent content;
		
		@Nullable AstInfo ast;
		
		@Nullable YamlSourceUnitModelInfo oldModel;
		@Nullable YamlSourceUnitModelInfo newModel;
		
		
		public Data(final YamlSourceUnitModelContainer adapter, final SubMonitor monitor) {
			this.adapter= adapter;
			this.sourceUnit= adapter.getSourceUnit();
			this.content= adapter.getParseContent(monitor);
		}
		
		
		boolean isOK() {
			return (this.content != null);
		}
		
		@SuppressWarnings("null")
		public AstInfo getAst() {
			return this.ast;
		}
		
		@SuppressWarnings("null")
		public YamlSourceUnitModelInfo getModel() {
			return this.newModel;
		}
		
	}
	
	
	private final YamlModelManagerImpl yamlManager;
	protected boolean stop= false;
	
	private final Object raLock= new Object();
	private final YamlParser raParser= new YamlParser();
	
	private final Object rmLock= new Object();
	private final SourceAnalyzer rmSourceAnalyzer= new SourceAnalyzer();
	
	private final Object riLock= new Object();
	private final YamlIssueReporter riReporter= new YamlIssueReporter();
	
	private LtkProject project;
	private @Nullable MultiStatus statusCollector;
	
	
	public YamlReconciler(final YamlModelManagerImpl manager) {
		this.yamlManager= manager;
	}
	
	
	public void init(final LtkProject project, final MultiStatus statusCollector) {
		this.project= nonNullAssert(project);
		this.statusCollector= statusCollector;
	}
	
	void stop() {
		this.stop= true;
	}
	
	
	public void reconcile(final YamlSourceUnitModelContainer adapter, final int flags,
			final IProgressMonitor monitor) {
		final var m= SubMonitor.convert(monitor);
		
		final Data data= new Data(adapter, m);
		if (data == null || !data.isOK()) {
			adapter.clear();
			return;
		}
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		
		synchronized (this.raLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			updateAst(data, flags);
		}
		
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		if ((flags & 0xf) < ModelManager.MODEL_FILE) {
			return;
		}
		
		synchronized (this.rmLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			final boolean updated= updateModel(data, flags, m);
			if (updated) {
				this.yamlManager.getEventJob().addUpdate(data.sourceUnit,
						data.oldModel, data.newModel );
			}
		}
		
		if ((flags & ModelManager.RECONCILE) != 0 && data.newModel != null) {
			synchronized (this.riLock) {
				if (this.stop || m.isCanceled()
						|| data.newModel != data.adapter.getCurrentModel() ) {
					return;
				}
				
				reportIssues(data, flags);
			}
		}
	}
	
	public YamlSourceUnitModelInfo reconcile(final SourceUnit su, final SourceUnitModelInfo modelInfo,
			final List<? extends YamlChunkElement> chunkElements,
			final int level, final IProgressMonitor monitor) {
		synchronized (this.rmLock) {
			return updateModel(su, modelInfo, chunkElements);
		}
	}
	
	
	protected final void updateAst(final Data data, final int flags) {
		final BasicSourceModelStamp stamp= new BasicSourceModelStamp(data.content.getStamp());
		
		AstInfo ast= data.adapter.getCurrentAst();
		if (ast != null && !stamp.equals(ast.getStamp())) {
			ast= null;
		}
		if (ast != null) {
			data.ast= ast;
		}
		else {
			final SourceComponent sourceNode;
			
			this.raParser.setScalarText(true);
			this.raParser.setCommentLevel(YamlParser.COLLECT_COMMENTS);
			
			sourceNode= this.raParser.parseSourceUnit(data.content.getString(), data.content.getStartOffset(),
					null );
			
			ast= new AstInfo(1, stamp, sourceNode);
			
			synchronized (data.adapter) {
				data.adapter.setAst(ast);
			}
			data.ast= ast;
		}
	}
	
	protected final boolean updateModel(final Data data, final int flags,
			final SubMonitor m) {
		YamlSourceUnitModelInfo model= data.adapter.getCurrentModel();
		if (model != null && !data.getAst().getStamp().equals(model.getStamp())) {
			model= null;
		}
		if (model != null) {
			data.newModel= model;
			return false;
		}
		else {
			model= this.rmSourceAnalyzer.createModel(data.adapter.getSourceUnit(), data.getAst());
			final boolean isOK= (model != null);
			
			if (isOK) {
				synchronized (data.adapter) {
					data.oldModel= data.adapter.getCurrentModel();
					data.adapter.setModel(model);
				}
				data.newModel= model;
				return true;
			}
			return false;
		}
	}
	
	private YamlSourceUnitModelInfo updateModel(final SourceUnit sourceUnit,
			final SourceUnitModelInfo modelInfo,
			final List<? extends YamlChunkElement> chunkElements) {
		YamlSourceUnitModelInfo model;
		try {
			final AstInfo ast= modelInfo.getAst();
			this.rmSourceAnalyzer.beginChunkSession(sourceUnit, ast);
			for (final var chunkElement : chunkElements) {
				final SourceComponent rootNode= chunkElement.getAdapter(SourceComponent.class);
				if (rootNode == null) {
					continue;
				}
				this.rmSourceAnalyzer.processChunk(chunkElement, rootNode);
			}
		}
		finally {
			model= this.rmSourceAnalyzer.stopChunkSession();
		}
		return model;
	}
	
	protected void reportIssues(final Data data, final int flags) {
		try {
			final var issueSupport= data.adapter.getIssueSupport();
			if (issueSupport == null) {
				return;
			}
			final var issueRequestor= issueSupport.createIssueRequestor(data.sourceUnit);
			if (issueRequestor != null) {
				try {
					this.riReporter.run(data.sourceUnit, data.getModel(), data.content,
							issueRequestor, flags );
				}
				finally {
					issueRequestor.finish();
				}
			}
		}
		catch (final Exception e) {
			handleStatus(new Status(IStatus.ERROR, YamlCore.BUNDLE_ID, 0,
					String.format("An error occurred when reporting issues for source unit %1$s.",
							data.sourceUnit ),
					e ));
		}
	}
	
	
	protected void handleStatus(final IStatus status) {
		final MultiStatus collector= this.statusCollector;
		if (collector != null) {
			collector.add(status);
		}
		else {
			YamlCorePlugin.log(status);
		}
	}
	
}
