/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public class NodeWithProperties extends YamlAstNode {
	
	
	ImIdentityList<YamlAstNode> properties= ImCollections.emptyIdentityList();
	
	YamlAstNode nodeChild;
	
	
	public NodeWithProperties(final YamlAstNode parent, final int startOffset, final int endOffset) {
		super(parent, startOffset, endOffset);
	}
	
	@Override
	void finish(final int endOffset) {
		boolean hasAnchor= false;
		boolean hasTag= false;
		for (final YamlAstNode property : this.properties) {
			switch (property.getNodeType()) {
			case ANCHOR:
				if (!hasAnchor) {
					hasAnchor= true;
				}
				else {
					property.status= TYPE12_SYNTAX_TOKEN_UNEXPECTED;
				}
				continue;
			case TAG:
				if (!hasTag) {
					hasTag= true;
				}
				else {
					property.status= TYPE12_SYNTAX_TOKEN_UNEXPECTED;
				}
				continue;
			default:
				throw new RuntimeException();
			}
		}
		if (this.nodeChild == null) {
			this.nodeChild= new Scalar.Plain(this,
					(!this.properties.isEmpty()) ?
							this.properties.get(this.properties.size() - 1).endOffset :
							this.endOffset );
		}
		else if (this.nodeChild.status == 0) {
			switch (this.nodeChild.getNodeType()) {
			case ALIAS:
				if (hasAnchor) {
					this.nodeChild.status= TYPE12_SYNTAX_TOKEN_UNEXPECTED;
				}
				break;
			default:
				break;
			}
		}
		
		super.finish(endOffset);
		{	final int minEnd= this.nodeChild.endOffset;
			if (this.endOffset < minEnd) {
				this.endOffset= minEnd;
			}
		}
	}
	
	@Override
	protected boolean hasErrorInChild() {
		for (final YamlAstNode property : this.properties) {
			if (YamlAsts.hasErrors(property)) {
				return true;
			}
		}
		return (YamlAsts.hasErrors(this.nodeChild));
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.PROPERTIES_CONTAINER;
	}
	
	
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	@Override
	public int getChildCount() {
		return this.properties.size() + 1;
	}
	
	public ImIdentityList<YamlAstNode> getProperties() {
		return this.properties;
	}
	
	public YamlAstNode getNode() {
		return this.nodeChild;
	}
	
	@Override
	public YamlAstNode getChild(final int index) {
		if (index == this.properties.size()) {
			return this.nodeChild;
		}
		return this.properties.get(index);
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		if (this.nodeChild == child) {
			return this.properties.size();
		}
		return this.properties.indexOf(child);
	}
	
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		for (final YamlAstNode property : this.properties) {
			visitor.visit(property);
		}
		visitor.visit(this.nodeChild);
	}
	
	@Override
	public void acceptInYaml(final YamlAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInYamlChildren(final YamlAstVisitor visitor) throws InvocationTargetException {
		for (final YamlAstNode property : this.properties) {
			property.acceptInYaml(visitor);
		}
		this.nodeChild.acceptInYaml(visitor);
	}
	
}
