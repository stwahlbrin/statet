/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.impl.AbstractAstNode;
import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public abstract class YamlAstNode extends AbstractAstNode
		implements AstNode {
	
	
	int status;
	
	@Nullable YamlAstNode yamlParent;
	
	int startOffset;
	int endOffset;
	
	
	YamlAstNode() {
	}
	
	YamlAstNode(final @Nullable YamlAstNode parent, final int startOffset, final int endOffset) {
		this.yamlParent= parent;
		
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	void finish(final int endOffset) {
		if (hasErrorInChild()) {
			this.status|= StatusCodes.ERROR_IN_CHILD;
		}
	}
	
	protected boolean hasErrorInChild() {
		return false;
	}
	
	
	public abstract NodeType getNodeType();
	
	public char getOperator() {
		return 0;
	}
	
	@Override
	public final int getStatusCode() {
		return this.status;
	}
	
	public final @Nullable YamlAstNode getYamlParent() {
		return this.yamlParent;
	}
	
	@Override
	public @Nullable AstNode getParent() {
		return this.yamlParent;
	}
	
	
	@Override
	public int getStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public final int getEndOffset() {
		return this.endOffset;
	}
	
	@Override
	public final int getLength() {
		return this.endOffset - this.startOffset;
	}
	
	
	@Override
	public abstract YamlAstNode getChild(final int index);
	
	
	public abstract void acceptInYaml(YamlAstVisitor visitor) throws InvocationTargetException;
	
	public abstract void acceptInYamlChildren(YamlAstVisitor visitor) throws InvocationTargetException;
	
	
	static void visit(final AstVisitor visitor, final @Nullable YamlAstNode node) throws InvocationTargetException {
		if (node != null) {
			visitor.visit(node);
		}
	}
	
	static void acceptInYaml(final YamlAstVisitor visitor, final @Nullable YamlAstNode node) throws InvocationTargetException {
		if (node != null) {
			node.acceptInYaml(visitor);
		}
	}
	
}
