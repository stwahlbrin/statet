/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_CTX1;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_CTX2;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE1;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE2;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE3;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public interface YamlSourceConstants {
	
	
	static int BLOCK=                   0b0 << SHIFT_CTX2 + 3;
	static int FLOW=                    0b1 << SHIFT_CTX2 + 3;
	static int CTX1_DOCUMENT=                   0x2 << SHIFT_CTX1;
	static int CTX12_DIRECTIVES_END_MARKER=         CTX1_DOCUMENT | 0x1 << SHIFT_CTX2;
	static int CTX12_DOC_CONTENT=                   CTX1_DOCUMENT | 0x2 << SHIFT_CTX2;
	static int CTX12_DOC_END_MARKER=                CTX1_DOCUMENT | 0x3 << SHIFT_CTX2;
	static int CTX1_COLLECTION=                 0x3 << SHIFT_CTX1;
	static int CTX12_SEQ_BLOCK=                     CTX1_COLLECTION | 0x0 << SHIFT_CTX2 | BLOCK;
	static int CTX12_SEQ_FLOW=                      CTX1_COLLECTION | 0x0 << SHIFT_CTX2 | FLOW;
	static int CTX12_MAP_BLOCK=                     CTX1_COLLECTION | 0x1 << SHIFT_CTX2 | BLOCK;
	static int CTX12_MAP_FLOW=                      CTX1_COLLECTION | 0x1 << SHIFT_CTX2 | FLOW;
	static int CTX1_COLLECTION_SUB=             0x4 << SHIFT_CTX1;
	static int CTX12_SEQ_ENTRY=                     CTX1_COLLECTION_SUB | 0x0 << SHIFT_CTX2;
	static int CTX12_MAP_ENTRY=                     CTX1_COLLECTION_SUB | 0x2 << SHIFT_CTX2;
	static int CTX12_MAP_KEY=                       CTX1_COLLECTION_SUB | 0x4 << SHIFT_CTX2;
	static int CTX12_MAP_VALUE=                     CTX1_COLLECTION_SUB | 0x5 << SHIFT_CTX2;
	static int CTX1_SCALAR=                     0x8 << SHIFT_CTX1;
	static int CTX12_SCALAR_BLOCK=                  CTX1_SCALAR | BLOCK;
	static int CTX12_SCALAR_FLOW=                   CTX1_SCALAR | FLOW;
	static int CTX1_TAG=                        0xB << SHIFT_CTX1;
	static int CTX12_TAG_VERBATIM=                  CTX1_TAG | 0x1 << SHIFT_CTX2;
	static int CTX12_TAG_SHORTHAND=                 CTX1_TAG | 0x2 << SHIFT_CTX2;
	static int CTX12_TAG_NONSPECIFIC=               CTX1_TAG | 0x3 << SHIFT_CTX2;
	
	static int TYPE1_SYNTAX_INCORRECT_TOKEN=                StatusCodes.TYPE1_SYNTAX_INCORRECT_TOKEN;
	
	static int TYPE12_SYNTAX_TOKEN_NOT_CLOSED=                  TYPE1_SYNTAX_INCORRECT_TOKEN | 0x1 << SHIFT_TYPE2 | ERROR;
	
	/** Invalid escape */
	static int TYPE12_SYNTAX_ESCAPE_INVALID=                    TYPE1_SYNTAX_INCORRECT_TOKEN | 0x2 << SHIFT_TYPE2 | ERROR;
	/**  */
	static int TYPE12_SYNTAX_CHAR_INVALID=                      TYPE1_SYNTAX_INCORRECT_TOKEN | 0x3 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_SPACE_BEFORE_COMMENT_MISSING=         TYPE12_SYNTAX_CHAR_INVALID | 0x1 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_LINE_BREAK_MISSING=                   TYPE12_SYNTAX_CHAR_INVALID | 0x2 << SHIFT_TYPE3;
	
	static int TYPE12_SYNTAX_INDICATOR_INCORRECT=               TYPE1_SYNTAX_INCORRECT_TOKEN | 0x6 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_INDENTATION_INDICATOR_INVALID=        TYPE12_SYNTAX_INDICATOR_INCORRECT | 0x1 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_CHOMPING_INDICATOR_MULTIPLE=          TYPE12_SYNTAX_INDICATOR_INCORRECT | 0x3 << SHIFT_TYPE3;
	static int TYPE12_SYNTAX_NAME_INCORRECT=                    TYPE1_SYNTAX_INCORRECT_TOKEN | 0x7 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_NAME_MISSING=                         TYPE12_SYNTAX_NAME_INCORRECT | 0x1 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_NAME_INVALID_END_SEP=                 TYPE12_SYNTAX_NAME_INCORRECT | 0x3 << SHIFT_TYPE3;
	static int TYPE12_SYNTAX_NUMBER_INCORRECT=                  TYPE1_SYNTAX_INCORRECT_TOKEN | 0x8 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_NUMBER_MISSING=                       TYPE12_SYNTAX_NUMBER_INCORRECT | 0x1 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_NUMBER_INVALID=                       TYPE12_SYNTAX_NUMBER_INCORRECT | 0x2 << SHIFT_TYPE3;
	static int TYPE12_SYNTAX_URI_INCORRECT=                     TYPE1_SYNTAX_INCORRECT_TOKEN | 0x9 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_URI_MISSING=                          TYPE12_SYNTAX_URI_INCORRECT | 0x1 << SHIFT_TYPE3;
	static int TYPE12_SYNTAX_TAG_INCORRECT=                     TYPE1_SYNTAX_INCORRECT_TOKEN | 0xA << SHIFT_TYPE2 | ERROR;
	static int TYPE123_SYNTAX_TAG_HANDLE_MISSING=                   TYPE12_SYNTAX_TAG_INCORRECT | 0x1 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TAG_HANDLE_NOT_CLOSED=                TYPE12_SYNTAX_TAG_INCORRECT | 0x2 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TAG_HANDLE_INVALID_END_SEP=           TYPE12_SYNTAX_TAG_INCORRECT | 0x3 << SHIFT_TYPE3;
	static int TYPE123_SYNTAX_TAG_SUFFIX_MISSING=                   TYPE12_SYNTAX_TAG_INCORRECT | 0x5 << SHIFT_TYPE3;
	
	static int TYPE12_INDENTATION_INCORRECT=                    TYPE1_SYNTAX_INCORRECT_TOKEN | 0xD << SHIFT_TYPE2 | ERROR;
	
	static int TYPE12_SYNTAX_TOKEN_UNKNOWN=                     TYPE1_SYNTAX_INCORRECT_TOKEN | 0xE << SHIFT_TYPE2 | ERROR;
	
	static int TYPE1_SYNTAX_TOKEN_UNEXPECTED=               0x2 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_TOKEN_UNEXPECTED=                  TYPE1_SYNTAX_TOKEN_UNEXPECTED | 0x0 << SHIFT_TYPE2 | ERROR;
	
	/** The node represents a missing token */
	static int TYPE1_SYNTAX_MISSING_TOKEN=                  0x3 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_MISSING_MARKER=                    TYPE1_SYNTAX_MISSING_TOKEN | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_MISSING_NODE=                      TYPE1_SYNTAX_MISSING_TOKEN | 0x2 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_MISSING_INDICATOR=                 TYPE1_SYNTAX_MISSING_TOKEN | 0x3 << SHIFT_TYPE2 | ERROR;
	
	/** The statement is incomplete */
	static int TYPE1_SYNTAX_INCOMPLETE_CC=                  0x5 << SHIFT_TYPE1;
	static int TYPE12_SYNTAX_COLLECTION_NOT_CLOSED=             TYPE1_SYNTAX_INCOMPLETE_CC | 0x2 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_SYNTAX_SEP_COMMA_MISSING=                 TYPE1_SYNTAX_INCOMPLETE_CC | 0x7 << SHIFT_TYPE2 | ERROR;
	
}
