/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.core.YamlCorePlugin;


@NonNullByDefault
public class YamlCore {
	
	public static final String BUNDLE_ID= "org.eclipse.statet.yaml.core"; //$NON-NLS-1$
	
	
	public static final String YAML_CONTENT_ID= "org.eclipse.statet.yaml.contentTypes.Yaml"; //$NON-NLS-1$
	
	public static final IContentType YAML_CONTENT_TYPE;
	
	static {
		final IContentTypeManager contentTypeManager= Platform.getContentTypeManager();
		YAML_CONTENT_TYPE= contentTypeManager.getContentType(YAML_CONTENT_ID);
	}
	
	
	private static final YamlCoreAccess WORKBENCH_ACCESS= YamlCorePlugin.getInstance().getWorkbenchAccess();
	
	public static YamlCoreAccess getWorkbenchAccess() {
		return WORKBENCH_ACCESS;
	}
	
	public static YamlCoreAccess getDefaultsAccess() {
		return YamlCorePlugin.getInstance().getDefaultsAccess();
	}
	
	public static YamlCoreAccess getContextAccess(final @Nullable IAdaptable adaptable) {
		if (adaptable != null) {
			final YamlCoreAccess access= adaptable.getAdapter(YamlCoreAccess.class);
			if (access != null) {
				return access;
			}
		}
		return getWorkbenchAccess();
	}
	
}
