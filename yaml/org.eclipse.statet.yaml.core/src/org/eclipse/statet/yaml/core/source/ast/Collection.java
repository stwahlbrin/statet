/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_BLOCK;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_FLOW;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_BLOCK;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_FLOW;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_COLLECTION_NOT_CLOSED;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_SEP_COMMA_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.yaml.core.source.ast.YamlParser.NContainerBuilder;


@NonNullByDefault
public abstract class Collection extends NContainer {
	
	
	public static abstract class FlowCollection extends Collection {
		
		
		private ImIntList sepOffsets;
		
		int closeIndicatorOffset= NA_OFFSET;
		
		
		@SuppressWarnings("null")
		FlowCollection(final YamlAstNode parent, final int offset, final int endOffset) {
			super(parent, offset, endOffset);
		}
		
		@Override
		void add(final NContainerBuilder containerBuilder, final YamlAstNode child) {
			if (containerBuilder.sepOffsets.size() < containerBuilder.children.size()) {
				containerBuilder.sepOffsets.add(NA_OFFSET);
				if (this.status == 0) {
					this.status= TYPE12_SYNTAX_SEP_COMMA_MISSING | getNodeCode();
				}
			}
			super.add(containerBuilder, child);
		}
		
		@Override
		void finish(final int endOffset, final NContainerBuilder containerBuilder) {
			if (this.status == 0 && this.closeIndicatorOffset == NA_OFFSET) {
				this.status= TYPE12_SYNTAX_COLLECTION_NOT_CLOSED | getNodeCode();
			}
			
			super.finish(endOffset, containerBuilder);
			
			this.sepOffsets= ImCollections.toIntList(containerBuilder.sepOffsets);
			if (!this.sepOffsets.isEmpty()) {
				final int minEnd= this.sepOffsets.getAt(this.sepOffsets.size() - 1) + 1;
				if (this.endOffset < minEnd) {
					this.endOffset= minEnd;
				}
			}
		}
		
		
		@Override
		public int getOpenIndicatorOffset() {
			return this.startOffset;
		}
		
		public ImIntList getSepOffsets() {
			return this.sepOffsets;
		}
		
		@Override
		public int getCloseIndicatorOffset() {
			return this.closeIndicatorOffset;
		}
		
		
	}
	
	static final class FlowSeq extends FlowCollection {
		
		
		FlowSeq(final YamlAstNode parent, final int offset, final int endOffset) {
			super(parent, offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.SEQ;
		}
		
		@Override
		public char getOperator() {
			return '[';
		}
		
		@Override
		int getNodeCode() {
			return CTX12_SEQ_FLOW;
		}
		
	}
	
	static final class FlowMap extends FlowCollection {
		
		
		FlowMap(final YamlAstNode parent, final int offset, final int endOffset) {
			super(parent, offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.MAP;
		}
		
		@Override
		public char getOperator() {
			return '{';
		}
		
		@Override
		int getNodeCode() {
			return CTX12_MAP_FLOW;
		}
		
	}
	
	public static abstract class BlockCollection extends Collection {
		
		
		BlockCollection(final YamlAstNode parent, final int offset, final int endOffset) {
			super(parent, offset, endOffset);
		}
		
		
		@Override
		public int getOpenIndicatorOffset() {
			return NA_OFFSET;
		}
		
		@Override
		public int getCloseIndicatorOffset() {
			return NA_OFFSET;
		}
		
		
	}
	
	static final class BlockSeq extends BlockCollection {
		
		
		BlockSeq(final YamlAstNode parent, final int offset, final int endOffset) {
			super(parent, offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.SEQ;
		}
		
		@Override
		public char getOperator() {
			return '-';
		}
		
		@Override
		int getNodeCode() {
			return CTX12_SEQ_BLOCK;
		}
		
	}
	
	static final class BlockMap extends BlockCollection {
		
		
		BlockMap(final YamlAstNode parent, final int offset, final int endOffset) {
			super(parent, offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.MAP;
		}
		
		@Override
		public char getOperator() {
			return '?'; // symbolic
		}
		
		@Override
		int getNodeCode() {
			return CTX12_MAP_BLOCK;
		}
		
	}
	
	
	private Collection(final YamlAstNode parent, final int startOffset, final int endOffset) {
		this.yamlParent= parent;
		
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	
	abstract int getNodeCode();
	
	
	@Override
	public void acceptInYaml(final YamlAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	public abstract int getOpenIndicatorOffset();
	
	public abstract int getCloseIndicatorOffset();
	
	
}
