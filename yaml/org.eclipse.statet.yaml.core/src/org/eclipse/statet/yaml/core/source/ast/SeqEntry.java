/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public class SeqEntry extends YamlAstNode {
	
	
	int valueIndicatorOffset= NA_OFFSET;
	YamlAstNode valueChild;
	
	
	SeqEntry(final YamlAstNode parent, final int startOffset, final int endOffset) {
		super(parent, startOffset, endOffset);
		this.valueIndicatorOffset= startOffset;
	}
	
	@Override
	void finish(final int endOffset) {
		if (this.valueChild == null) {
			this.valueChild= new Scalar.Plain(this, // empty node
					(this.valueIndicatorOffset != NA_OFFSET) ?
							this.valueIndicatorOffset :
							this.valueChild.endOffset );
		}
		
		super.finish(endOffset);
		{	final int min= this.valueChild.endOffset;
			if (this.endOffset < min) {
				this.endOffset= min;
			}
		}
	}
	
	@Override
	protected boolean hasErrorInChild() {
		return YamlAsts.hasErrors(this.valueChild);
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.SEQ_ENTRY;
	}
	
	
	public int getValueIndicatorOffset() {
		return this.valueIndicatorOffset;
	}
	
	
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	@Override
	public int getChildCount() {
		return 1;
	}
	
	public YamlAstNode getValue() {
		return this.valueChild;
	}
	
	@Override
	public YamlAstNode getChild(final int index) {
		switch (index) {
		case 0:
			return this.valueChild;
		default:
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		if (child == this.valueChild) {
			return 0;
		}
		return -1;
	}
	
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.valueChild);
	}
	
	@Override
	public void acceptInYaml(final YamlAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInYamlChildren(final YamlAstVisitor visitor) throws InvocationTargetException {
		this.valueChild.acceptInYaml(visitor);
	}
	
}
