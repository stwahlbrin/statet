/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source;

import org.eclipse.statet.ecommons.text.ITokenScanner;
import org.eclipse.statet.ecommons.text.PairMatcher;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.yaml.core.source.doc.YamlDocumentConstants;


/**
 * A pair finder class for implementing the pair matching.
 */
public class YamlBracketPairMatcher extends PairMatcher {
	
	
	public static final char[][] BRACKETS= { {'{', '}'}, {'[', ']'} };
	
	private static final String[] CONTENT_TYPES= new String[] {
		YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE,
	};
	
	
	public YamlBracketPairMatcher(final DocContentSections documentContentInfo) {
		this(new YamlHeuristicTokenScanner(documentContentInfo));
	}
	
	public YamlBracketPairMatcher(final YamlHeuristicTokenScanner scanner) {
		this(scanner, scanner.getDocumentPartitioning());
	}
	
	public YamlBracketPairMatcher(final ITokenScanner scanner,
			final String partitioning) {
		super(BRACKETS, partitioning, CONTENT_TYPES, scanner, '\\');
	}
	
}
