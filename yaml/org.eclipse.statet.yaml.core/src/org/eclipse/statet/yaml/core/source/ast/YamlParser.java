/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE1_RUNTIME_ERROR;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_DIRECTIVES_END_MARKER;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_ENTRY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_KEY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_VALUE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_ENTRY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_INDICATOR;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_MARKER;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.IntArrayList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;

import org.eclipse.statet.internal.yaml.snakeyaml.scanner.ScannerImpl;
import org.eclipse.statet.internal.yaml.snakeyaml.scanner.SyntaxProblem;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.AliasToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.AnchorToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.CommentType;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.DirectiveToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.ScalarToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.TagToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.TagTuple;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.Token;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.StatusCodes;
import org.eclipse.statet.yaml.core.YamlCore;


@NonNullByDefault
public class YamlParser {
	
	
	public static final int COLLECT_COMMENTS= 1 << 0;
	
	
	static final class NContainerBuilder {
		
		final List<YamlAstNode> children= new ArrayList<>(8);
		
		final IntList sepOffsets= new IntArrayList(8);
		
		
		void clear() {
			this.children.clear();
			this.sepOffsets.clear();
		}
		
	}
	
	private static final int DOC_NONE= 0;
	private static final int DOC_DIRECTIVES= 1;
	private static final int DOC_CONTENT= 2;
	
	
	private final ScannerImpl lexer= new ScannerImpl(true, false, false) {
		@Override
		protected void handleComment(final CommentType type, final int startIndex, final int endIndex) {
			if (YamlParser.this.commentsLevel != 0) {
				YamlParser.this.comments.add(new Comment(startIndex, endIndex));
			}
		}
	};
	
	private String parseInput;
	private int parseStartOffset;
	private int parseEndOffset;
	private @Nullable AstNode parseParent;
	
	private @Nullable YamlAstNode currentNode;
	
	private int depth;
	private final List<NContainerBuilder> containerStack= new ArrayList<>();
	private int docState;
	
	private int commentsLevel;
	private final List<Comment> comments= new ArrayList<>();
	
	
	public YamlParser() {
	}
	
	
	public void setScalarText(final boolean create) {
		this.lexer.setCreateContentText(create);
	}
	
	public void setCommentLevel(int level) {
		if ((level & COLLECT_COMMENTS) == 0) {
			level= 0;
		}
		else {
			level= (level & (COLLECT_COMMENTS));
		}
		this.commentsLevel= level;
	}
	
	
	private void init0(final String input, final int inputOffset, final @Nullable AstNode parent) {
		this.parseInput= nonNullAssert(input);
		this.parseStartOffset= inputOffset;
		this.parseEndOffset= inputOffset + input.length();
		this.parseParent= parent;
	}
	
	@SuppressWarnings("null")
	private void clear0() {
		this.parseInput= null;
		this.parseParent= null;
	}
	
	private void logParseError(final Exception e) {
		CommonsRuntime.log(new ErrorStatus(YamlCore.BUNDLE_ID,
				"An error occured while parsing R source code. Input:\n"
						+ this.parseInput.toString(),
				e ));
	}
	
	
	private void init() {
		this.depth= -1;
		if (this.commentsLevel != 0) {
			this.comments.clear();
		}
		this.docState= DOC_NONE;
	}
	
	public SourceComponent parseSourceUnit(final String input, final int inputOffset,
			final @Nullable AstNode parent) {
		init0(input, inputOffset, parent);
		try {
			init();
			this.lexer.reset(input, inputOffset);
			
			final SourceComponent sourceNode= new SourceComponent(parent,
					this.parseStartOffset, this.parseEndOffset );
			enterNode(sourceNode);
			
			processTokens();
			
			while (this.depth >= 0) {
				exit(NA_OFFSET);
			}
			
			if (this.commentsLevel != 0) {
				sourceNode.comments= ImCollections.toList(this.comments);
			}
			
			return sourceNode;
		}
		catch (final Exception e) {
			logParseError(e);
			return createErrorSourceComponent();
		}
		finally {
			clear0();
			while (this.depth >= 0) {
				if (this.depth < this.containerStack.size()) {
					final var builder= this.containerStack.get(this.depth);
					builder.clear();
				}
				this.depth--;
			}
		}
	}
	
	public SourceComponent parseSourceUnit(final String text) {
		return parseSourceUnit(text, 0, null);
	}
	
	private SourceComponent createErrorSourceComponent() {
		final int startOffset= this.parseStartOffset;
		int endOffset= this.parseEndOffset;
		if (endOffset < startOffset) {
			endOffset= startOffset;
		}
		final SourceComponent dummy= new SourceComponent(this.parseParent, startOffset, endOffset);
		dummy.status= TYPE1_RUNTIME_ERROR;
		if (this.commentsLevel != 0) {
			dummy.comments= ImCollections.emptyList();
		}
		return dummy;
	}
	
	
	private void addChildTerm(final YamlAstNode node) {
		addChild(node);
		
		checkExit();
	}
	
	private void addChild(final YamlAstNode node) {
		final YamlAstNode currentNode= nonNullAssert(this.currentNode);
		switch (currentNode.getNodeType()) {
		case MAP_ENTRY: {
				final var entryNode= (MapEntry)currentNode;
				if (entryNode.keyChild == null && entryNode.valueIndicatorOffset == NA_OFFSET) {
					entryNode.keyChild= node;
				}
				else {
					entryNode.valueChild= node;
				}
				return;
			}
		case SEQ_ENTRY: {
				final var entryNode= (SeqEntry)currentNode;
				entryNode.valueChild= node;
				return;
			}
		case PROPERTIES_CONTAINER: {
				final var propertiesNode= (NodeWithProperties)currentNode;
				if (YamlAsts.isNodeProperty(node)) {
					propertiesNode.properties= ImCollections.addElement(propertiesNode.properties, node);
				}
				else {
					propertiesNode.nodeChild= node;
				}
				return;
			}
		default: {
				final var container= (NContainer)currentNode;
				final var builder= this.containerStack.get(this.depth);
				container.add(builder, node);
				return;
			}
		}
	}
	
	private void finish(final int endOffset) {
		final YamlAstNode currentNode= nonNullAssert(this.currentNode);
		switch (currentNode.getNodeType()) {
		case MAP_ENTRY:
		case SEQ_ENTRY:
		case PROPERTIES_CONTAINER:
				currentNode.finish(endOffset);
				return;
		case DOCUMENT:
			this.docState= DOC_NONE;
			//$FALL-THROUGH$
		default: {
				final var container= (NContainer)this.currentNode;
				if (endOffset != NA_OFFSET) {
					container.endOffset= endOffset;
				}
				
				final var builder= this.containerStack.get(this.depth);
				container.finish(endOffset, builder);
				builder.clear();
			}
		}
	}
	
	private void enterNode(final NContainer node) {
		if (this.depth >= 0) {
			addChild(node);
		}
		
		this.depth++;
		this.currentNode= node;
		
		while (this.depth >= this.containerStack.size()) {
			this.containerStack.add(new NContainerBuilder());
		}
	}
	
	private void enterNode(final NodeWithProperties node) {
		addChild(node);
		
		this.depth++;
		this.currentNode= node;
	}
	
	private void enterNode(final MapEntry node) {
		addChild(node);
		
		this.depth++;
		this.currentNode= node;
	}
	
	private void enterNode(final SeqEntry node) {
		addChild(node);
		
		this.depth++;
		this.currentNode= node;
	}
	
	private boolean exitTo(final Class<?> type1) {
		while (this.depth > 1) {
			if (this.currentNode.getClass() == type1) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	private boolean exitTo(final Class<?> type1, final Class<?> type2) {
		while (this.depth > 1) {
			if (this.currentNode.getClass() == type1 || this.currentNode.getClass() == type2) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	private boolean exitTo1(final Class<?> type1, final Class<?> type2) {
		while (this.depth > 1) {
			final YamlAstNode currentNode= nonNullAssert(this.currentNode);
			if (currentNode.getClass() == type1 || currentNode.getClass() == type2) {
				return true;
			}
			if (currentNode.getNodeType() == NodeType.MAP_ENTRY) {
				exit(NA_OFFSET);
				continue;
			}
			break;
		}
		int checkDepth= this.depth - 1;
		YamlAstNode node= this.currentNode.getYamlParent();
		while (checkDepth > 1) {
			if (node.getClass() == type1 || node.getClass() == type2) {
				while (this.depth > checkDepth) {
					exit(NA_OFFSET);
				}
				return true;
			}
			checkDepth--;
			node= node.getYamlParent();
		}
		return false;
	}
	
	private boolean exitTo(final NodeType type1) {
		while (this.depth > 1) {
			if (this.currentNode.getNodeType() == type1) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	private boolean exitTo(final NodeType type1, final NodeType type2) {
		while (this.depth > 1) {
			if (this.currentNode.getNodeType() == type1 || this.currentNode.getNodeType() == type2) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	@SuppressWarnings("null")
	private YamlAstNode exitToSourceComponent(final int offset) {
		while (this.depth > 1) {
			exit(NA_OFFSET);
		}
		if (this.depth > 0) {
			exit(offset);
		}
		return this.currentNode;
	}
	
	private void exit(final int offset) {
		finish(offset);
		this.currentNode= this.currentNode.yamlParent;
		this.depth--;
		
		checkExit();
		if (this.depth == 1) {
			this.lexer.resetToRoot();
		}
	}
	
	private void checkExit() {
		if (this.depth > 1) {
			switch (this.currentNode.getNodeType()) {
			case PROPERTIES_CONTAINER:
				if (((NodeWithProperties)this.currentNode).nodeChild != null) {
					exit(NA_OFFSET);
				}
				break;
			case MAP_ENTRY:
				if (((MapEntry)this.currentNode).valueChild != null) {
					exit(NA_OFFSET);
				}
				break;
			case SEQ_ENTRY:
				if (((SeqEntry)this.currentNode).valueChild != null) {
					exit(NA_OFFSET);
				}
				break;
			}
		}
	}
	
	private void ensureDocContent(final int startOffset) {
		DocumentNode docNode;
		switch (this.docState) {
		case DOC_NONE:
			docNode= new DocumentNode((SourceComponent)this.currentNode, startOffset);
			enterNode(docNode);
			this.docState= DOC_CONTENT;
			return;
		case DOC_DIRECTIVES:
			if (!this.containerStack.get(this.depth).children.isEmpty()) {
				docNode= (DocumentNode)this.currentNode;
				final var markerNode= new Dummy(TYPE12_SYNTAX_MISSING_MARKER | CTX12_DIRECTIVES_END_MARKER,
						docNode, startOffset );
				addChildTerm(markerNode);
				docNode.directivesEndChild= markerNode;
			}
			this.docState= DOC_CONTENT;
			return;
		default:
			return;
		}
	}
	
	private YamlAstNode checkParent(final Token token) {
		ensureDocContent(token.getStartIndex());
		final YamlAstNode currentNode= nonNullAssert(this.currentNode);
		switch (currentNode.getNodeType()) {
		case SEQ: if (currentNode.getOperator() == '-') {
				final var seqEntry= new SeqEntry(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				seqEntry.status= TYPE12_SYNTAX_MISSING_INDICATOR | CTX12_SEQ_ENTRY;
				enterNode(seqEntry);
				return seqEntry;
			}
			return currentNode;
		case MAP: {
				final var mapEntry= new MapEntry(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				if (currentNode.getOperator() == '?') {
					mapEntry.status= TYPE12_SYNTAX_MISSING_INDICATOR | CTX12_MAP_KEY;
				}
				enterNode(mapEntry);
				return mapEntry;
			}
		default:
			return currentNode;
		}
	}
	
	private void processTokens() {
		while (true) {
			final Token token= this.lexer.nextToken();
			if (token == null) {
				break;
			}
			
			switch (token.getTokenId()) {
			case StreamStart:
			case StreamEnd:
				continue;
			
			case Directive: {
				YamlAstNode currentNode= nonNullAssert(this.currentNode);
				DocumentNode docNode;
				switch (this.docState) {
				case DOC_NONE:
					docNode= new DocumentNode((SourceComponent)currentNode, token.getStartIndex());
					enterNode(docNode);
					this.docState= DOC_DIRECTIVES;
					break;
				case DOC_DIRECTIVES:
					docNode= (DocumentNode)currentNode;
					break;
				default:
					currentNode= exitToSourceComponent(token.getStartIndex());
					docNode= null;
					break;
				}
				final Directive node= new Directive(currentNode,
						token.getStartIndex(), token.getEndIndex(),
						((DirectiveToken<?>)token).getName() );
				addChildTerm(node);
				if (docNode != null) {
					attachProblem(node, token);
				}
				else {
					node.status= TYPE12_SYNTAX_TOKEN_UNEXPECTED;
				}
				continue;
			}
			case DocumentStart: {
				YamlAstNode currentNode= nonNullAssert(this.currentNode);
				DocumentNode docNode;
				switch (this.docState) {
				case DOC_DIRECTIVES:
					docNode= (DocumentNode)currentNode;
					this.docState= DOC_CONTENT;
					break;
				default:
					currentNode= exitToSourceComponent(token.getStartIndex());
					docNode= new DocumentNode((SourceComponent)currentNode, token.getStartIndex());
					enterNode(docNode);
					this.docState= DOC_CONTENT;
					break;
				}
				final var node= new Marker.DirectivesEnd(docNode,
						token.getStartIndex(), token.getEndIndex() );
				addChildTerm(node);
				attachProblem(node, token);
				docNode.directivesEndChild= node;
				continue;
			}
			case DocumentEnd: {
				YamlAstNode currentNode= nonNullAssert(this.currentNode);
				DocumentNode docNode;
				switch (this.docState) {
				case DOC_NONE:
					docNode= null;
					break;
				case DOC_DIRECTIVES:
					docNode= (DocumentNode)currentNode;
					break;
				default:
					while (this.depth > 1) {
						exit(NA_OFFSET);
					}
					currentNode= nonNullAssert(this.currentNode);
					docNode= (DocumentNode)currentNode;
					break;
				}
				final var node= new Marker.DocumentEnd(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				addChildTerm(node);
				attachProblem(node, token);
				if (docNode != null) {
					docNode.documentEndChild= node;
					exit(token.getEndIndex());
				}
				continue;
			}
			
			case BlockSequenceStart: {
				ensureDocContent(token.getStartIndex());
				final YamlAstNode currentNode= nonNullAssert(this.currentNode);
				final Collection.BlockSeq node= new Collection.BlockSeq(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case BlockMappingStart: {
				ensureDocContent(token.getStartIndex());
				final YamlAstNode currentNode= nonNullAssert(this.currentNode);
				final Collection.BlockMap node= new Collection.BlockMap(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case BlockEnd: {
				final boolean found= exitTo(Collection.BlockSeq.class, Collection.BlockMap.class);
				if (found) {
					exit(token.getEndIndex());
				}
				continue;
			}
			case FlowSequenceStart: {
				ensureDocContent(token.getStartIndex());
				final YamlAstNode currentNode= nonNullAssert(this.currentNode);
				final Collection.FlowCollection node= new Collection.FlowSeq(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case FlowSequenceEnd: {
				final boolean found= exitTo(Collection.FlowSeq.class);
				final YamlAstNode currentNode= nonNullAssert(this.currentNode);
				final Collection.FlowSeq collection;
				if (found) {
					collection= (Collection.FlowSeq) currentNode;
					collection.closeIndicatorOffset= token.getStartIndex();
					exit(token.getEndIndex());
				}
				else {
					addChildTerm(new Dummy(TYPE12_SYNTAX_TOKEN_UNEXPECTED, currentNode,
							token.getStartIndex(), token.getEndIndex() ));
				}
				continue;
			}
			case FlowMappingStart: {
				ensureDocContent(token.getStartIndex());
				final YamlAstNode currentNode= nonNullAssert(this.currentNode);
				final Collection.FlowCollection node= new Collection.FlowMap(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case FlowMappingEnd: {
				final boolean found= exitTo(Collection.FlowMap.class);
				final YamlAstNode currentNode= nonNullAssert(this.currentNode);
				final Collection.FlowMap node;
				if (found) {
					node= (Collection.FlowMap) currentNode;
					node.closeIndicatorOffset= token.getStartIndex();
					exit(token.getEndIndex());
				}
				else {
					addChildTerm(new Dummy(TYPE12_SYNTAX_TOKEN_UNEXPECTED, currentNode,
							token.getStartIndex(), token.getEndIndex() ));
				}
				continue;
			}
			
			case BlockEntry: {
				ensureDocContent(token.getStartIndex());
				YamlAstNode currentNode= nonNullAssert(this.currentNode);
				final Collection.BlockSeq seqNode;
				if (currentNode instanceof NodeWithProperties
						&& currentNode.yamlParent instanceof SeqEntry) {
					exit(NA_OFFSET);
					currentNode= nonNullAssert(this.currentNode);
				}
				if (currentNode instanceof Collection.BlockSeq) {
					seqNode= (Collection.BlockSeq)currentNode;
				}
				else if (currentNode instanceof SeqEntry) {
					exit(NA_OFFSET);
					seqNode= (Collection.BlockSeq)nonNullAssert(this.currentNode);
				}
				else {
					seqNode= new Collection.BlockSeq(currentNode,
							token.getStartIndex(), token.getEndIndex() );
					enterNode(seqNode);
				}
				final SeqEntry node= new SeqEntry(seqNode, token.getStartIndex(), token.getEndIndex());
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case FlowEntry: {
				ensureDocContent(token.getStartIndex());
				final boolean found= exitTo1(Collection.FlowSeq.class, Collection.FlowMap.class);
				final YamlAstNode currentNode= nonNullAssert(this.currentNode);
				if (found) {
					final var builder= this.containerStack.get(this.depth);
					if (currentNode.status == 0
							&& builder.sepOffsets.size() == builder.children.size() ) {
						YamlAstNode node;
						switch (currentNode.getNodeType()) {
						case SEQ:
							node= new Dummy(TYPE12_SYNTAX_MISSING_NODE | CTX12_SEQ_ENTRY,
									currentNode, token.getStartIndex());
							break;
						case MAP:
							node= new Dummy(TYPE12_SYNTAX_MISSING_NODE | CTX12_MAP_ENTRY,
									currentNode, token.getStartIndex());
							break;
						default:
							throw new RuntimeException();
						}
						addChildTerm(node);
					}
					builder.sepOffsets.add(token.getStartIndex());
				}
				else {
					addChildTerm(new Dummy(TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_SEQ_ENTRY,
							currentNode,
							token.getStartIndex(), token.getEndIndex() ));
				}
				continue;
			}
			
			case Anchor: {
				final YamlAstNode currentNode= checkParent(token);
				
				final NodeWithProperties propertiesNode;
				if (currentNode.getNodeType() == NodeType.PROPERTIES_CONTAINER) {
					propertiesNode= (NodeWithProperties)currentNode;
				}
				else {
					propertiesNode= new NodeWithProperties(currentNode,
							token.getStartIndex(), token.getEndIndex() );
					enterNode(propertiesNode);
				}
				final Anchor node= new Anchor(propertiesNode,
						token.getStartIndex(), token.getEndIndex(),
						((AnchorToken)token).getValue() );
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			case Alias: {
				final YamlAstNode currentNode= checkParent(token);
				
				final NodeLink node= new Alias(currentNode,
						token.getStartIndex(), token.getEndIndex(),
						((AliasToken)token).getValue() );
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			
			case Tag: {
				final TagTuple tagTuple= ((TagToken)token).getValue();
				final YamlAstNode currentNode= checkParent(token);
				
				final NodeWithProperties propertiesNode;
				if (currentNode instanceof NodeWithProperties) {
					propertiesNode= (NodeWithProperties)currentNode;
				}
				else {
					propertiesNode= new NodeWithProperties(currentNode,
							token.getStartIndex(), token.getEndIndex() );
					enterNode(propertiesNode);
				}
				final Tag node= new Tag(propertiesNode,
						token.getStartIndex(), token.getEndIndex(),
						tagTuple.getHandle(), tagTuple.getSuffix() );
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			
			case Key: {
				ensureDocContent(token.getStartIndex());
				YamlAstNode currentNode= nonNullAssert(this.currentNode);
				
				final boolean found;
				if (currentNode instanceof Collection.FlowSeq) {
					found= true;
				}
				else {
					found= exitTo(NodeType.MAP);
					currentNode= nonNullAssert(this.currentNode);
				}
				final var entryNode= new MapEntry(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				if (!found) {
					entryNode.status|= TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_MAP_KEY;
				}
				enterNode(entryNode);
				attachProblem(entryNode, token);
				if (token.getLength() > 0) { // explicite
					entryNode.keyIndicatorOffset= entryNode.startOffset;
				}
				continue;
			}
			case Value: {
				ensureDocContent(token.getStartIndex());
				YamlAstNode currentNode= nonNullAssert(this.currentNode);
				
				final boolean found;
				if (currentNode instanceof Collection.FlowSeq) {
					found= true;
				}
				else {
					found= exitTo(NodeType.MAP_ENTRY, NodeType.MAP);
					currentNode= nonNullAssert(this.currentNode);
				}
				final MapEntry entryNode;
				if (currentNode.getNodeType() == NodeType.MAP_ENTRY) {
					entryNode= (MapEntry)currentNode;
					entryNode.endOffset= token.getEndIndex();
				}
				else {
					entryNode= new MapEntry(currentNode,
							token.getStartIndex(), token.getEndIndex() );
					if (!found) {
						entryNode.status|= TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_MAP_VALUE;
					}
					enterNode(entryNode);
				}
				attachProblem(entryNode, token);
				if (token.getLength() > 0) {
					entryNode.valueIndicatorOffset= token.getStartIndex();
				}
				continue;
			}
			
			case Scalar: {
				final ScalarToken scalarToken= (ScalarToken)token;
				final YamlAstNode currentNode= checkParent(token);
				final Scalar node;
				switch (scalarToken.getStyle()) {
				case DOUBLE_QUOTED:
					node= new Scalar.DQuoted(currentNode,
							token.getStartIndex(), token.getEndIndex(),
							scalarToken.getValue() );
					break;
				case SINGLE_QUOTED:
					node= new Scalar.SQuoated(currentNode,
							token.getStartIndex(), token.getEndIndex(),
							scalarToken.getValue() );
					break;
				case LITERAL:
					node= new Scalar.Literal(currentNode,
							token.getStartIndex(), token.getEndIndex(),
							scalarToken.getValue() );
					break;
				case FOLDED:
					node= new Scalar.Folded(currentNode,
							token.getStartIndex(), token.getEndIndex(),
							scalarToken.getValue() );
					break;
				default:
					node= new Scalar.Plain(currentNode,
							token.getStartIndex(), token.getEndIndex(),
							scalarToken.getValue() );
					break;
				}
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			
			default:
				continue;
			}
		}
	}
	
	private void attachProblem(final YamlAstNode node, final Token token) {
		if ((node.getStatusCode() & StatusCodes.ERROR) != 0) {
			return;
		}
		final SyntaxProblem problem= token.getProblem();
		if (problem != null) {
			node.status= problem.getStatus();
			final var statusDetail= problem.getStatusDetail();
			if (statusDetail != null) {
				node.addAttachment(statusDetail);
			}
		}
	}
	
}
