/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.refactoring;

import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;


public class YamlRefactoring {
	
	
	private static final CommonRefactoringFactory YAML_FACTORY= new YamlRefactoringFactory();
	
	public static CommonRefactoringFactory getYamlFactory() {
		return YAML_FACTORY;
	}
	
	
	public static final String DELETE_YAML_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.yaml.refactoring.DeleteYamlElementsOperation"; //$NON-NLS-1$
	
	public static final String MOVE_YAML_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.yaml.refactoring.MoveYamlElementsOperation"; //$NON-NLS-1$
	
	public static final String COPY_YAML_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.yaml.refactoring.CopyYamlElementsOperation"; //$NON-NLS-1$
	
	public static final String PASTE_YAML_CODE_REFACTORING_ID= "org.eclipse.statet.yaml.refactoring.PasteYamlCodeOperation"; //$NON-NLS-1$
	
	
	public static final String DELETE_YAML_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.yaml.refactoring.DeleteYamlElementsProcessor"; //$NON-NLS-1$
	
	public static final String MOVE_YAML_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.yaml.refactoring.MoveYamlElementsProcessor"; //$NON-NLS-1$
	
	public static final String COPY_YAML_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.yaml.refactoring.CopyYamlElementsProcessor"; //$NON-NLS-1$
	
	public static final String PASTE_YAML_CODE_PROCESSOR_ID= "org.eclipse.statet.yaml.refactoring.PasteYamlCodeProcessor"; //$NON-NLS-1$
	
}
