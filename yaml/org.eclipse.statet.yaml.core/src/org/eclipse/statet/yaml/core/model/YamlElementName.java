/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model;

import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.ElementName;


@NonNullByDefault
public abstract class YamlElementName implements ElementName {
	
	
	public static final byte RESOURCE=                      0x02;
	public static final byte OTHER=                         0x0F;
	
	public static final byte SCALAR=                        0x10;
	public static final byte ANCHOR=                        0x41;
	public static final byte DOC_NUM=                       0x51;
	public static final byte SEQ_NUM=                       0x52;
	public static final byte COMPLEX=                       0x60;
	
	
	private static boolean equals(final @Nullable String s1, final @Nullable String s2) {
		return (s1 == s2
				|| (s1 != null && s2 != null && s1.hashCode() == s2.hashCode() && s1.equals(s2)) );
	}
	
	
	private static class Default extends YamlElementName {
		
		
		protected final int type;
		protected final @Nullable String segment;
		
		
		private Default(final int type, final @Nullable String name) {
			this.type= type;
			this.segment= name;
		}
		
		
		@Override
		public int getType() {
			return this.type;
		}
		
		@Override
		public @Nullable YamlElementName getNextSegment() {
			return null;
		}
		
		@Override
		public @Nullable String getSegmentName() {
			return this.segment;
		}
		
		@Override
		public String getDisplayName() {
			switch (this.type) {
			case SEQ_NUM:
				return "#" + this.segment;
			case SCALAR:
				return (this.segment != null) ? this.segment : "<null>"; //$NON-NLS-1$
			case ANCHOR:
			default:
				return (this.segment != null) ? this.segment : "<missing>"; //$NON-NLS-1$
			}
		}
		
	}
	
	
	public static YamlElementName create(final int type, final @Nullable String name) {
		return new Default(type, name);
	}
	
	
	@Override
	public abstract @Nullable YamlElementName getNextSegment();
	
	@Override
	public YamlElementName getLastSegment() {
		@NonNull YamlElementName lastSegment;
		YamlElementName nextSegment= this;
		do {
			lastSegment= nextSegment;
		} while ((nextSegment= nextSegment.getNextSegment()) != null);
		return lastSegment;
	}
	
	
	@Override
	public int hashCode() {
		final String name= getSegmentName();
		final ElementName next= getNextSegment();
		if (next != null) {
			return getType() * ((name != null) ? name.hashCode() : 1) * (next.hashCode()+7);
		}
		else {
			return getType() * ((name != null) ? name.hashCode() : 1);
		}
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof YamlElementName) {
			final YamlElementName other= (YamlElementName) obj;
			return ((getType() == other.getType())
					&& equals(getSegmentName(), other.getSegmentName())
					&& Objects.equals(getNextSegment(), other.getNextSegment()) );
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getDisplayName();
	}
	
}
