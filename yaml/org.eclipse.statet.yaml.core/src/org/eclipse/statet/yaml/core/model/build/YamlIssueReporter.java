/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model.build;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;

import org.eclipse.statet.internal.yaml.core.model.AstProblemReporter;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.TaskIssueConfig;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.model.YamlCompositeSourceElement;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;
import org.eclipse.statet.yaml.core.source.ast.YamlAstNode;


@NonNullByDefault
public class YamlIssueReporter {
	
	
	private final AstProblemReporter syntaxProblemReporter= new AstProblemReporter();
	
	private final YamlTaskTagReporter taskReporter= new YamlTaskTagReporter();
	
	/* explicite configs */
	private @Nullable TaskIssueConfig taskIssueConfig;
	
	private boolean runProblems;
	private boolean runTasks;
	
	
	public YamlIssueReporter() {
	}
	
	
	public void run(final SourceUnit sourceUnit,
			final YamlSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		this.runProblems= requestor.isInterestedInProblems(YamlModel.YAML_TYPE_ID);
		this.runTasks= requestor.isInterestedInTasks();
		if (!(this.runProblems || this.runTasks)) {
			return;
		}
		
		var taskIssueConfig= this.taskIssueConfig;
		if (taskIssueConfig == null) {
			final var prefs= EPreferences.getContextPrefs(sourceUnit);
			taskIssueConfig= TaskIssueConfig.getConfig(prefs);
		}
		if (this.runTasks) {
			this.taskReporter.configure(taskIssueConfig);
		}
		
		final YamlSourceElement element= modelInfo.getSourceElement();
		if (element instanceof YamlCompositeSourceElement) {
			final var elements= ((YamlCompositeSourceElement)element).getCompositeElements();
			for (final var yamlChunk : elements) {
				runReporters(sourceUnit, yamlChunk.getAdapter(AstNode.class),
						modelInfo, content, requestor, level );
			}
		}
		else {
			runReporters(sourceUnit, element.getAdapter(AstNode.class),
					modelInfo, content, requestor, level );
		}
	}
	
	
	private void runReporters(final SourceUnit sourceUnit, final YamlAstNode node,
			final @Nullable YamlSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (this.runProblems) {
			this.syntaxProblemReporter.run(node, content, requestor);
		}
		if (this.runTasks) {
			this.taskReporter.run(sourceUnit, node, content, requestor);
		}
	}
	
	private void runReporters(final SourceUnit sourceUnit, final @Nullable AstNode node,
			final @Nullable YamlSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (node == null) {
			return;
		}
		if (node instanceof YamlAstNode) {
			runReporters(sourceUnit, (YamlAstNode)node, modelInfo,
					content, requestor, level );
		}
	}
	
}
