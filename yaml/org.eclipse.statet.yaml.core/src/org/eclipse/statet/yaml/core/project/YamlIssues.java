/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.project;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.yaml.core.model.YamlModel;


@NonNullByDefault
public final class YamlIssues {
	
	
	public static final String TASK_MARKER_TYPE= "org.eclipse.statet.yaml.resourceMarkers.Tasks"; //$NON-NLS-1$
	
	public static final IssueTypeSet.TaskCategory TASK_CATEGORY= new IssueTypeSet.TaskCategory(
			TASK_MARKER_TYPE, null );
	
	
//	public static final String YAML_MODEL_PROBLEM_MARKER_TYPE= "org.eclipse.statet.yaml.resourceMarkers.YamlModelProblem"; //$NON-NLS-1$
//	
//	public static final IssueTypeSet.ProblemTypes YAML_MODEL_PROBLEM_MARKER_TYPES= new IssueTypeSet.ProblemTypes(
//			YAML_MODEL_PROBLEM_MARKER_TYPE );
	
	public static final IssueTypeSet.ProblemCategory YAML_MODEL_PROBLEM_CATEGORY= new IssueTypeSet.ProblemCategory(
			YamlModel.YAML_TYPE_ID,
			null, null );
	
	
	private YamlIssues() {
	}
	
}
