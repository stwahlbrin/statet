/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_DOC_CONTENT;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.yaml.core.source.ast.YamlParser.NContainerBuilder;


@NonNullByDefault
public class DocumentNode extends NContainer {
	
	
	ImIdentityList<Directive> directiveChildren;
	@Nullable YamlAstNode directivesEndChild;
	@Nullable YamlAstNode documentEndChild;
	ImIdentityList<YamlAstNode> contentChildren;
	
	
	DocumentNode(final SourceComponent parent, final int offset) {
		this.yamlParent= parent;
		
		this.startOffset= offset;
		this.endOffset= offset;
	}
	
	@Override
	void finish(final int endOffset, final NContainerBuilder containerBuilder) {
		super.finish(endOffset, containerBuilder);
		
		int startIdx= 0;
		int endIdx= 0;
		while (endIdx < this.children.size()
				&& this.children.get(endIdx).getNodeType() == NodeType.DIRECTIVE) {
			endIdx++;
		}
		this.directiveChildren= (ImIdentityList)this.children.subList(startIdx, endIdx);
		if (this.directivesEndChild != null) {
			endIdx++;
		}
		startIdx= endIdx;
		endIdx= this.children.size();
		if (this.documentEndChild != null) {
			endIdx--;
		}
		if (startIdx == endIdx && this.directivesEndChild == null) {
			final Dummy dummy= new Dummy(TYPE12_SYNTAX_MISSING_NODE | CTX12_DOC_CONTENT,
					this, (this.documentEndChild != null) ?
							this.documentEndChild.getStartOffset() : this.endOffset );
			this.children= ImCollections.addElement(this.children, startIdx, dummy);
			endIdx++;
		}
		this.contentChildren= this.children.subList(startIdx, endIdx);
		if (this.contentChildren.size() > 1) {
			final var child= this.contentChildren.get(1);
			child.status= TYPE12_SYNTAX_TOKEN_UNEXPECTED
					| (child.status & ERROR_IN_CHILD);
		}
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.DOCUMENT;
	}
	
	
	public ImIdentityList<Directive> getDirectives() {
		return this.directiveChildren;
	}
	
	public @Nullable YamlAstNode getDirectivesEndMarker() {
		return this.directivesEndChild;
	}
	
	public ImIdentityList<YamlAstNode> getContentNodes() {
		return this.contentChildren;
	}
	
	public @Nullable YamlAstNode getDocumentEndMarker() {
		return this.documentEndChild;
	}
	
	
	@Override
	public void acceptInYaml(final YamlAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
}
