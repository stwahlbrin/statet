/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;


public class BasicYamlCoreAccess implements YamlCoreAccess {
	
	
	private final PreferenceAccess prefs;
	
	private final YamlCodeStyleSettings codeStyle;
	
	
	public BasicYamlCoreAccess(final PreferenceAccess prefs) {
		this.prefs= prefs;
		this.codeStyle= new YamlCodeStyleSettings(1);
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.prefs;
	}
	
	@Override
	public YamlCodeStyleSettings getYamlCodeStyle() {
		return this.codeStyle;
	}
	
}
