/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public final class Tag extends YamlAstNode {
	
	
	private final @Nullable String handle;
	private final String suffix;
	
	
	Tag(final YamlAstNode parent, final int startOffset, final int endOffset,
			final @Nullable String handle, final String suffix) {
		super(parent, startOffset, endOffset);
		
		this.handle= handle;
		this.suffix= suffix;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.TAG;
	}
	
	
	public @Nullable String getHandle() {
		return this.handle;
	}
	
	public String getSuffix() {
		return this.suffix;
	}
	
	@Override
	public String getText() {
		return this.suffix;
	}
	
	
	@Override
	public boolean hasChildren() {
		return false;
	}
	
	@Override
	public int getChildCount() {
		return 0;
	}
	
	@Override
	public YamlAstNode getChild(final int index) {
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		return -1;
	}
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
	}
	
	@Override
	public void acceptInYaml(final YamlAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInYamlChildren(final YamlAstVisitor visitor) throws InvocationTargetException {
	}
	
}
