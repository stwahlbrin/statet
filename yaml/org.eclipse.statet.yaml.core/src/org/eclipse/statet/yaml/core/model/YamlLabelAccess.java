/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model;

import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.text.Position;

import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.yaml.core.source.ast.YamlAstNode;


public abstract class YamlLabelAccess extends YamlElementName {
	
	
	public static final Comparator<YamlLabelAccess> NAME_POSITION_COMPARATOR= new Comparator<>() {
		@Override
		public int compare(final YamlLabelAccess o1, final YamlLabelAccess o2) {
			return o1.getNameNode().getStartOffset() - o2.getNameNode().getStartOffset();
		}
	};
	
	
	public static Position getTextPosition(final YamlAstNode node) {
		return new Position(node.getStartOffset(), node.getLength());
	}
	
	public static TextRegion getTextRegion(final YamlAstNode node) {
		return new BasicTextRegion(node);
	}
	
	
	protected YamlLabelAccess() {
	}
	
	
	public abstract YamlAstNode getNode();
	
	public abstract YamlAstNode getNameNode();
	
	public abstract List<? extends YamlLabelAccess> getAllInUnit();
	
	
	public abstract boolean isWriteAccess();
	
}
