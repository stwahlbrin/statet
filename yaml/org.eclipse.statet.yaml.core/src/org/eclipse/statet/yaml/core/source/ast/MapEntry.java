/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public class MapEntry extends YamlAstNode {
	
	
	int keyIndicatorOffset= NA_OFFSET;
	YamlAstNode keyChild;
	
	int valueIndicatorOffset= NA_OFFSET;
	YamlAstNode valueChild;
	
	
	MapEntry(final YamlAstNode parent, final int startOffset, final int endOffset) {
		super(parent, startOffset, endOffset);
	}
	
	MapEntry(final YamlAstNode parent, final int startOffset) {
		super(parent, startOffset, startOffset);
	}
	
	@Override
	void finish(final int endOffset) {
		if (this.keyChild == null) {
			this.keyChild= new Scalar.Plain(this, // empty node
					(this.keyIndicatorOffset != NA_OFFSET) ?
							this.keyIndicatorOffset : this.startOffset );
		}
		if (this.valueChild == null) {
			this.valueChild= new Scalar.Plain(this, // empty node
					(this.valueIndicatorOffset != NA_OFFSET) ?
							this.valueIndicatorOffset :
							this.keyChild.endOffset );
		}
		
		super.finish(endOffset);
		{	final int min= this.valueChild.endOffset;
			if (this.endOffset < min) {
				this.endOffset= min;
			}
		}
	}
	
	@Override
	protected boolean hasErrorInChild() {
		return (YamlAsts.hasErrors(this.keyChild)
				|| YamlAsts.hasErrors(this.valueChild) );
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.MAP_ENTRY;
	}
	
	
	public int getKeyIndicatorOffset() {
		return this.keyIndicatorOffset;
	}
	
	public int getValueIndicatorOffset() {
		return this.valueIndicatorOffset;
	}
	
	
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	@Override
	public int getChildCount() {
		return 2;
	}
	
	public YamlAstNode getKey() {
		return this.keyChild;
	}
	
	public YamlAstNode getValue() {
		return this.valueChild;
	}
	
	@Override
	public YamlAstNode getChild(final int index) {
		switch (index) {
		case 0:
			return this.keyChild;
		case 1:
			return this.valueChild;
		default:
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		if (child == this.keyChild) {
			return 0;
		}
		if (child == this.valueChild) {
			return 1;
		}
		return -1;
	}
	
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.keyChild);
		visitor.visit(this.valueChild);
	}
	
	@Override
	public void acceptInYaml(final YamlAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInYamlChildren(final YamlAstVisitor visitor) throws InvocationTargetException {
		this.keyChild.acceptInYaml(visitor);
		this.valueChild.acceptInYaml(visitor);
	}
	
}
