/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;


@NonNullByDefault
public final class SourceComponent extends NContainer {
	
	
	private final @Nullable AstNode parent;
	
	@Nullable ImList<Comment> comments;
	
	
	SourceComponent(final @Nullable AstNode parent,
			final int startOffset, final int endOffset) {
		this.parent= parent;
		
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.SOURCELINES;
	}
	
	@Override
	public @Nullable AstNode getParent() {
		return this.parent;
	}
	
	/**
	 * The comment nodes in this source component
	 * 
	 * @return the comments or <code>null</code>, if disabled
	 */
	public @Nullable ImList<Comment> getComments() {
		return this.comments;
	}
	
	
	@Override
	public void acceptInYaml(final YamlAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
}
