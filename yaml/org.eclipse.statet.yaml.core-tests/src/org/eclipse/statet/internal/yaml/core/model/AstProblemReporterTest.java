/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.source.YamlSourceConstants;
import org.eclipse.statet.yaml.core.source.ast.SourceComponent;
import org.eclipse.statet.yaml.core.source.ast.YamlParser;


@NonNullByDefault
public class AstProblemReporterTest {
	
	
	private final YamlParser yamlParser= new YamlParser();
	
	private final AstProblemReporter syntaxProblemReporter= new AstProblemReporter();
	
	
	public AstProblemReporterTest() {
	}
	
	
	@Test
	public void Doc_invalidHeader() {
		List<Problem> problems;
		
		problems= collectProblems("%YAML 1.2\ncontent");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_MISSING_MARKER | YamlSourceConstants.CTX12_DIRECTIVES_END_MARKER,
				"Syntax Error/Missing Marker: directive end marker › --- ‹ expected.",
				10, 1,
				problems.get(0) );
	}
	
	@Test
	public void Doc_invalidContent() {
		List<Problem> problems;
		
		problems= collectProblems("%YAML 1.2\n...");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE | YamlSourceConstants.CTX12_DOC_CONTENT,
				"Syntax Error/Missing Node: content node expected.",
				10, 1,
				problems.get(0) );
	}
	
	@Test
	public void Scalar_invalidIndicator() {
		List<Problem> problems;
		
		problems= collectProblems(">0\nabc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_INDENTATION_INDICATOR_INVALID,
				"Syntax Error/Invalid Scalar: indentation indicator › 0 ‹ is invalid, integer 1 - 9 expected.",
				1, 1,
				problems.get(0) );
		problems= collectProblems(">11\n          abc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_INDENTATION_INDICATOR_INVALID,
				"Syntax Error/Invalid Scalar: indentation indicator › 11 ‹ is invalid, integer 1 - 9 expected.",
				1, 2,
				problems.get(0) );
		problems= collectProblems(">+10\n          abc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_INDENTATION_INDICATOR_INVALID,
				"Syntax Error/Invalid Scalar: indentation indicator › 10 ‹ is invalid, integer 1 - 9 expected.",
				2, 2,
				problems.get(0) );
		
		problems= collectProblems(">++1\n abc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_CHOMPING_INDICATOR_MULTIPLE,
				"Syntax Error/Invalid Scalar: chomping indicator is invalid, chomping is already defined.",
				2, 1,
				problems.get(0) );
		problems= collectProblems(">+1-\n abc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_CHOMPING_INDICATOR_MULTIPLE,
				"Syntax Error/Invalid Scalar: chomping indicator is invalid, chomping is already defined.",
				3, 1,
				problems.get(0) );
	}
	
	@Test
	public void Scalar_notClosed() {
		List<Problem> problems;
		
		problems= collectProblems("\"abc ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED | YamlSourceConstants.CTX12_SCALAR_FLOW,
				"Syntax Error/Unclosed Scalar: quoted scalar › abc  ‹ is not closed, › \" ‹ expected.",
				4, 1,
				problems.get(0) );
		problems= collectProblems("\'abc ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED | YamlSourceConstants.CTX12_SCALAR_FLOW,
				"Syntax Error/Unclosed Scalar: quoted scalar › abc  ‹ is not closed, › ' ‹ expected.",
				4, 1,
				problems.get(0) );
		
		problems= collectProblems(">2abc\n  abc");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_LINE_BREAK_MISSING,
				"Syntax Error/Unexpected Token: line break expected.",
				2, 1,
				problems.get(0) );
	}
	
	@Test
	public void Scalar_invalidString() {
		List<Problem> problems;
		
		problems= collectProblems("\"abc \\x0G \" ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_ESCAPE_INVALID | YamlSourceConstants.CTX12_SCALAR_FLOW,
				"Syntax Error/Invalid String: escape sequence › \\x0 ‹ in quoted scalar is invalid.",
				5, 3,
				problems.get(0) );
		problems= collectProblems("\"abc \\u123 \" ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_ESCAPE_INVALID | YamlSourceConstants.CTX12_SCALAR_FLOW,
				"Syntax Error/Invalid String: escape sequence › \\u123 ‹ in quoted scalar is invalid.",
				5, 5,
				problems.get(0) );
		problems= collectProblems("\"abc \\u123\" ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_ESCAPE_INVALID | YamlSourceConstants.CTX12_SCALAR_FLOW,
				"Syntax Error/Invalid String: escape sequence › \\u123 ‹ in quoted scalar is invalid.",
				5, 5,
				problems.get(0) );
		problems= collectProblems("\"abc \\UABCDEFG\" ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_ESCAPE_INVALID | YamlSourceConstants.CTX12_SCALAR_FLOW,
				"Syntax Error/Invalid String: escape sequence › \\UABCDEF ‹ in quoted scalar is invalid.",
				5, 8,
				problems.get(0) );
		problems= collectProblems("\"abc \\( \" ");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_ESCAPE_INVALID | YamlSourceConstants.CTX12_SCALAR_FLOW,
				"Syntax Error/Invalid String: escape sequence › \\( ‹ in quoted scalar is invalid.",
				5, 2,
				problems.get(0) );
	}
	
	@Test
	public void Seq_incompleteBlockSeq() {
		List<Problem> problems;
		
		problems= collectProblems("key:\n - bar\n - baz\n 3rd");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_MISSING_INDICATOR | YamlSourceConstants.CTX12_SEQ_ENTRY,
				"Syntax Error/Incomplete Statement: entry indicator › - ‹ for a new sequence entry expected.",
				20, 1,
				problems.get(0) );
	}
	
	@Test
	public void Seq_incompleteFlowSeq() {
		List<Problem> problems;
		
		problems= collectProblems("{ key: [ v1, v2 }");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_COLLECTION_NOT_CLOSED | YamlSourceConstants.CTX12_SEQ_FLOW,
				"Syntax Error/Incomplete Statement: sequence is not closed, › ] ‹ missing.",
				7, 1,
				problems.get(0) );
		
		problems= collectProblems("[ \"v1\" \"v2\" ]");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_SEP_COMMA_MISSING | YamlSourceConstants.CTX12_SEQ_FLOW,
				"Syntax Error/Incomplete Statement: entry separator › , ‹ or closing › ] ‹ expected.",
				7, 1,
				problems.get(0) );
		
		problems= collectProblems("[ \"v1\", , ]");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE | YamlSourceConstants.CTX12_SEQ_ENTRY,
				"Syntax Error/Missing Node: content node for sequence entry expected.",
				8, 1,
				problems.get(0) );
	}
	
	@Test
	public void Map_incompleteBlockMap() {
		List<Problem> problems;
		
		problems= collectProblems("?key1 : value1\nkey2\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_MISSING_INDICATOR | YamlSourceConstants.CTX12_MAP_KEY,
				"Syntax Error/Incomplete Statement: key indicator › ? ‹ for a new map entry expected.",
				15, 1,
				problems.get(0) );
	}
	
	@Test
	public void Map_incompleteFlowMap() {
		List<Problem> problems;
		
		problems= collectProblems("[ { key: value ]");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_COLLECTION_NOT_CLOSED | YamlSourceConstants.CTX12_MAP_FLOW,
				"Syntax Error/Incomplete Statement: map is not closed, › } ‹ missing.",
				2, 1,
				problems.get(0) );
		
		problems= collectProblems("{ key1: \"v1\" key2: \"v2\" }");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_SEP_COMMA_MISSING | YamlSourceConstants.CTX12_MAP_FLOW,
				"Syntax Error/Incomplete Statement: entry separator › , ‹ or closing › } ‹ expected.",
				13, 1,
				problems.get(0) );
		
		problems= collectProblems("{ key1: value1, , }");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE | YamlSourceConstants.CTX12_MAP_ENTRY,
				"Syntax Error/Missing Node: content node for map entry expected.",
				16, 1,
				problems.get(0) );
	}
	
	@Test
	public void Tag_incomplete() {
		List<Problem> problems;
		
		problems= collectProblems("!<tag: value");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED | YamlSourceConstants.CTX12_TAG_VERBATIM,
				"Syntax Error/Incomplete Tag: verbatime tag is not closed, › > ‹ expected.",
				6, 1,
				problems.get(0) );
	}
	
	@Test
	public void Comment() {
		List<Problem> problems;
		
		problems= collectProblems("%YAML 1.2# comment\n---\n");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_SPACE_BEFORE_COMMENT_MISSING,
				"Syntax Error/Unexptected Token: space ›   ‹ expected before comment.",
				9, 1,
				problems.get(0) );
		problems= collectProblems(">#comment\n scalar");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_SPACE_BEFORE_COMMENT_MISSING,
				"Syntax Error/Unexptected Token: space ›   ‹ expected before comment.",
				1, 1,
				problems.get(0) );
		problems= collectProblems("\"scalar\"#comment");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				YamlSourceConstants.TYPE123_SYNTAX_SPACE_BEFORE_COMMENT_MISSING,
				"Syntax Error/Unexptected Token: space ›   ‹ expected before comment.",
				8, 1,
				problems.get(0) );
	}
	
	
	private List<Problem> collectProblems(final String source) {
		final SourceUnit sourceUnit= SourceModelTests.createSourceUnit(source);
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final SourceComponent sourceComponent= this.yamlParser.parseSourceUnit(
				sourceContent.getString() );
		final List<Problem> collectedProblems= new ArrayList<>();
		this.syntaxProblemReporter.run(sourceComponent, sourceContent, new IssueRequestor() {
			@Override
			public boolean isInterestedInProblems(final String categoryId) {
				return true;
			}
			@Override
			public void acceptProblems(final String categoryId, final List<Problem> problems) {
				collectedProblems.addAll(problems);
			}
			@Override
			public void acceptProblems(final Problem problem) {
				collectedProblems.add(problem);
			}
			@Override
			public boolean isInterestedInTasks() {
				return false;
			}
			@Override
			public void acceptTask(final Task task) {
			}
			@Override
			public void finish() {
			}
		});
		return collectedProblems;
	}
	
	private void assertProblem(final int expectedSeverity, final int expectedCode, final String expectedMessage,
			final int expectedStartOffset, final int expectedLength,
			final Problem actual) {
		assertEquals(expectedSeverity, actual.getSeverity(), "severity");
		assertEquals(String.format("0x%1$08X", expectedCode), String.format("0x%1$08X", actual.getCode()), "code");
		assertEquals(expectedMessage, actual.getMessage(), "message");
		assertEquals(expectedStartOffset, actual.getSourceStartOffset(), "startOffset");
		assertEquals(expectedStartOffset + expectedLength, actual.getSourceEndOffset(), "endOffset");
	}
	
}
