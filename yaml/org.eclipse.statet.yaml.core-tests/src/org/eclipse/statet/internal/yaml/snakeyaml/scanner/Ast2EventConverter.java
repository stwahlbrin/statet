/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.snakeyaml.scanner;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.snakeyaml.scanner.YamlSpecTest.YamlEvent;
import org.eclipse.statet.yaml.core.source.ast.Alias;
import org.eclipse.statet.yaml.core.source.ast.Anchor;
import org.eclipse.statet.yaml.core.source.ast.Collection;
import org.eclipse.statet.yaml.core.source.ast.Directive;
import org.eclipse.statet.yaml.core.source.ast.DocumentNode;
import org.eclipse.statet.yaml.core.source.ast.Dummy;
import org.eclipse.statet.yaml.core.source.ast.MapEntry;
import org.eclipse.statet.yaml.core.source.ast.Marker;
import org.eclipse.statet.yaml.core.source.ast.NodeType;
import org.eclipse.statet.yaml.core.source.ast.NodeWithProperties;
import org.eclipse.statet.yaml.core.source.ast.Scalar;
import org.eclipse.statet.yaml.core.source.ast.SourceComponent;
import org.eclipse.statet.yaml.core.source.ast.Tag;
import org.eclipse.statet.yaml.core.source.ast.YamlAstNode;
import org.eclipse.statet.yaml.core.source.ast.YamlAstVisitor;


@NonNullByDefault
class Ast2EventConverter extends YamlAstVisitor {
	
	
	private final List<YamlEvent> events= new ArrayList<>();
	
	private final List<String> nextProperties= new ArrayList<>();
	private int nextStatus;
	
	
	public static List<YamlEvent> collect(final SourceComponent node) throws InvocationTargetException {
		final Ast2EventConverter collector= new Ast2EventConverter();
		collector.visit(node);
		return collector.events;
	}
	
	
	private ImList<String> getProperties() {
		return ImCollections.clearToList(this.nextProperties);
	}
	
	private ImList<String> noProperties() {
		return ImCollections.newList();
	}
	
	private void addProperty(final String property, final int status) {
		this.nextProperties.add(property);
		if (status != 0) {
			this.nextStatus= status;
		}
	}
	
	private int getStatus(final @Nullable YamlAstNode node) {
		int status= this.nextStatus;
		if (status != 0) {
			this.nextStatus= 0;
		}
		else if (node != null) {
			status= node.getStatusCode();
		}
		return status;
	}
	
	
	@Override
	public void visit(final SourceComponent node) throws InvocationTargetException {
		this.events.add(new YamlEvent('+', "STR", noProperties(),
				getStatus(node) ));
		node.acceptInYamlChildren(this);
		this.events.add(new YamlEvent('-', "STR", noProperties(),
				getStatus(node) ));
	}
	
	@Override
	public void visit(final DocumentNode node) throws InvocationTargetException {
		final var startMarker= node.getDirectivesEndMarker();
		final var endMarker= node.getDocumentEndMarker();
		this.events.add(new YamlEvent('+', "DOC", noProperties(), (startMarker != null) ? startMarker.getText() : "",
				getStatus(startMarker) ));
		final var content= node.getContentNodes();
		if (content.isEmpty()) {
			this.events.add(new YamlEvent('=', "VAL", noProperties(), ":",
					getStatus(node) ));
		}
		else {
			for (final YamlAstNode child : content) {
				child.acceptInYaml(this);
			}
		}
		this.events.add(new YamlEvent('-', "DOC", noProperties(), (endMarker != null) ? endMarker.getText() : "",
				getStatus(endMarker) ));
	}
	
	@Override
	public void visit(final Directive node) {
		final YamlEvent event= new YamlEvent('=', "DIR", noProperties(),
				getStatus(node) );
		if (event.hasError()) {
			this.events.add(event);
		}
	}
	
	@Override
	public void visit(final Marker node) throws InvocationTargetException {
	}
	
	@Override
	public void visit(final NodeWithProperties node) throws InvocationTargetException {
		super.visit(node);
	}
	
	@Override
	public void visit(final Tag node) throws InvocationTargetException {
		addProperty("<", node.getStatusCode());
	}
	
	@Override
	public void visit(final Anchor node) throws InvocationTargetException {
		String value= String.valueOf(node.getOperator());
		if (node.getText() != null) {
			value+= node.getText();
		}
		addProperty(value, node.getStatusCode());
	}
	
	@Override
	public void visit(final Collection node) throws InvocationTargetException {
		String type;
		String sub;
		final int entry;
		switch (node.getOperator()) {
		case '[':
			type= "SEQ";
			sub= "[]";
			entry= 1;
			break;
		case '{':
			type= "MAP";
			sub= "{}";
			entry= 2;
			break;
		case '-':
			type= "SEQ";
			sub= null;
			entry= 0;
			break;
		case '?':
			type= "MAP";
			sub= null;
			entry= 2;
			break;
		default:
			throw new IllegalArgumentException();
		}
		
		this.events.add(new YamlEvent('+', type, sub, getProperties(),
				getStatus(node) ));
		
		for (int childIdx= 0; childIdx < node.getChildCount(); ) {
			final YamlAstNode child= node.getChild(childIdx++);
			if (entry == 1 && child.getNodeType() == NodeType.MAP_ENTRY) {
				this.events.add(new YamlEvent('+', "MAP", "{}", noProperties(),
						child.getStatusCode() ));
				child.acceptInYaml(this);
				this.events.add(new YamlEvent('-', "MAP", noProperties(),
						child.getStatusCode() ));
			}
			else {
				child.acceptInYaml(this);
			}
		}
		
		this.events.add(new YamlEvent('-', type, noProperties(),
				getStatus(node) ));
	}
	
	@Override
	public void visit(final MapEntry node) throws InvocationTargetException {
		this.nextStatus= node.getStatusCode();
		node.getKey().acceptInYaml(this);
		node.getValue().acceptInYaml(this);
	}
	
	@Override
	public void visit(final Scalar node) throws InvocationTargetException {
		String value= (node.getOperator() != 0) ? String.valueOf(node.getOperator()) : ":";
		if (node.getText() != null) {
			value+= node.getText();
		}
		this.events.add(new YamlEvent('=', "VAL", getProperties(), value,
				getStatus(node) ));
	}
	
	@Override
	public void visit(final Alias node) throws InvocationTargetException {
		String value= String.valueOf(node.getOperator());
		if (node.getText() != null) {
			value+= node.getText();
		}
		this.events.add(new YamlEvent('=', "ALI", noProperties(), value,
				getStatus(node) ));
	}
	
	@Override
	public void visit(final Dummy node) throws InvocationTargetException {
		this.events.add(new YamlEvent('=', "ERR", getProperties(),
				getStatus(node) ));
	}
	
	
}
