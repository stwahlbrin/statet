/*=============================================================================#
 # Copyright (c) 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.internal.yaml.core.model.SourceModelTests.assertElementName;
import static org.eclipse.statet.internal.yaml.core.model.SourceModelTests.assertRegion;

import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.yaml.core.model.YamlElement;
import org.eclipse.statet.yaml.core.model.YamlElementName;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;
import org.eclipse.statet.yaml.core.source.ast.SourceComponent;
import org.eclipse.statet.yaml.core.source.ast.YamlParser;


@NonNullByDefault
public class SourceAnalyzerTest {
	
	
	private final YamlParser yamlParser= new YamlParser();
	
	private final SourceAnalyzer sourceAnalyzer= new SourceAnalyzer();
	
	
	public SourceAnalyzerTest() {
		this.yamlParser.setScalarText(true);
	}
	
	
	@Test
	public void empty() {
		YamlSourceUnitModelInfo modelInfo;
		List<? extends SourceStructElement<?, ?>> elements;
		
		modelInfo= createModel("");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(0, elements.size());
	}
	
	@Test
	public void Document_empty() {
		YamlSourceUnitModelInfo modelInfo;
		List<? extends SourceStructElement<?, ?>> elements;
		
		modelInfo= createModel("===\n...\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertDocument(0, 8, elements.get(0));
	}
	
	
	@Test
	public void Seq() {
		YamlSourceUnitModelInfo modelInfo;
		List<? extends SourceStructElement<?, ?>> elements;
		
		modelInfo= createModel("- apple\n- egg\n- tea");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		final var document= assertDocument(0, 19, elements.get(0));
		final List<? extends SourceStructElement<?, ?>> docChildren= document.getSourceChildren(null);
		assertEquals(1, docChildren.size());
		
		final var mapElement= assertYamlElement(YamlElement.C12_SEQ, 0, 19,
				docChildren.get(0) );
		assertElementName(YamlElementName.OTHER, "", mapElement.getElementName());
		final List<? extends SourceStructElement<?, ?>> mapChildren= mapElement.getSourceChildren(null);
		assertEquals(3, mapChildren.size());
		
		final YamlSourceElement entry0= assertYamlElement(YamlElement.C1_SCALAR, 0, 7,
				mapChildren.get(0) );
		assertElementName(YamlElementName.SEQ_NUM, "1", entry0.getElementName());
		assertNull(entry0.getNameSourceRange());
		final YamlSourceElement entry1= assertYamlElement(YamlElement.C1_SCALAR, 8, 5,
				mapChildren.get(1) );
		assertElementName(YamlElementName.SEQ_NUM, "2", entry1.getElementName());
		assertNull(entry1.getNameSourceRange());
	}
	
	@Test
	public void Map() {
		YamlSourceUnitModelInfo modelInfo;
		List<? extends SourceStructElement<?, ?>> elements;
		
		modelInfo= createModel("{ key1 : A, key2 : [ 1, 3 ] }");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		final var document= assertDocument(0, 29, elements.get(0));
		final List<? extends SourceStructElement<?, ?>> docChildren= document.getSourceChildren(null);
		assertEquals(1, docChildren.size());
		
		final var mapElement= assertYamlElement(YamlElement.C12_MAP, 0, 29,
				docChildren.get(0) );
		assertElementName(YamlElementName.OTHER, "", mapElement.getElementName());
		final List<? extends SourceStructElement<?, ?>> mapChildren= mapElement.getSourceChildren(null);
		assertEquals(2, mapChildren.size());
		
		final YamlSourceElement entry0= assertYamlElement(YamlElement.C1_SCALAR, 2, 8,
				mapChildren.get(0) );
		assertElementName(YamlElementName.SCALAR, "key1", entry0.getElementName());
		assertRegion(2, 4, entry0.getNameSourceRange(), "nameSourceRange");
		final YamlSourceElement entry1= assertYamlElement(YamlElement.C12_SEQ, 12, 15,
				mapChildren.get(1) );
		assertElementName(YamlElementName.SCALAR, "key2", entry1.getElementName());
		assertRegion(12, 4, entry1.getNameSourceRange(), "nameSourceRange");
	}
	
	
	protected YamlSourceElement assertYamlElement(final int expectedType,
			final int expectedStartOffset, final int expectedLength,
			final SourceStructElement<?, ?> actual) {
		assertEquals(YamlModel.YAML_TYPE_ID, actual.getModelTypeId());
		assertEquals(expectedType, (actual.getElementType() & LtkModelElement.MASK_C123));
		assertRegion(expectedStartOffset, expectedLength, actual.getSourceRange(), "sourceRange");
		assertTrue(actual instanceof YamlSourceElement);
		final YamlSourceElement yamlElement= (YamlSourceElement)actual;
		return yamlElement;
	}
	
	protected YamlSourceElement assertDocument(
			final int expectedStartOffset, final int expectedLength,
			final SourceStructElement<?, ?> actual) {
		final YamlSourceElement yamlElement= assertYamlElement(YamlElement.C12_DOC,
				expectedStartOffset, expectedLength,
				actual );
		assertElementName(YamlElementName.DOC_NUM, "1", yamlElement.getElementName());
		return yamlElement;
	}
	
	
	protected YamlSourceUnitModelInfo createModel(final String code) {
		final SourceUnit sourceUnit= SourceModelTests.createSourceUnit(code);
		
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final SourceComponent sourceComponent= this.yamlParser.parseSourceUnit(
				sourceContent.getString(), sourceContent.getStartOffset(), null );
		final var ast= new AstInfo(ModelManager.MODEL_FILE, new BasicSourceModelStamp(0),
				sourceComponent );
		
		final var model= this.sourceAnalyzer.createModel(sourceUnit, ast);
		assertNotNull(model);
		return model;
	}
	
}
