/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui.sourceediting;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.ICharPairMatcher;
import org.eclipse.statet.ecommons.text.IIndentSettings;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.presentation.SingleTokenScanner;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.internal.yaml.ui.YamlUIPlugin;
import org.eclipse.statet.internal.yaml.ui.sourceediting.YamlAutoEditStrategy;
import org.eclipse.statet.internal.yaml.ui.sourceediting.YamlQuickOutlineInformationProvider;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.EcoReconciler2;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorAddon;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceUnitReconcilingStrategy;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistProcessor;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.CommentScanner;
import org.eclipse.statet.yaml.core.YamlCore;
import org.eclipse.statet.yaml.core.YamlCoreAccess;
import org.eclipse.statet.yaml.core.source.YamlBracketPairMatcher;
import org.eclipse.statet.yaml.core.source.YamlHeuristicTokenScanner;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentConstants;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentContentInfo;
import org.eclipse.statet.yaml.ui.text.YamlDefaultTextStyleScanner;
import org.eclipse.statet.yaml.ui.text.YamlDoubleClickStrategy;
import org.eclipse.statet.yaml.ui.text.YamlTextStyles;


/**
 * Configuration for YAML source editors.
 */
public class YamlSourceViewerConfiguration extends SourceEditorViewerConfiguration {
	
	
	private static final String[] CONTENT_TYPES= YamlDocumentConstants.YAML_CONTENT_TYPES.toArray(
			new String[YamlDocumentConstants.YAML_CONTENT_TYPES.size()] );
	
	
	protected ITextDoubleClickStrategy doubleClickStrategy;
	
	private YamlAutoEditStrategy autoEditStrategy;
	
	private YamlCoreAccess coreAccess;
	private @Nullable PreferenceAccess mainDocTypePrefs;
	
	
	public YamlSourceViewerConfiguration(final int flags) {
		this(YamlDocumentContentInfo.INSTANCE, flags, null, null, null, null, null);
	}
	
	public YamlSourceViewerConfiguration(final DocContentSections documentContentInfo, final int flags,
			final SourceEditor editor,
			final YamlCoreAccess access, final @Nullable PreferenceAccess mainDocTypePrefs,
			final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		super(documentContentInfo, flags, editor);
		setCoreAccess(access, mainDocTypePrefs);
		
		setup((preferenceStore != null) ? preferenceStore : YamlUIPlugin.getInstance().getEditorPreferenceStore(),
				LtkUIPreferences.getEditorDecorationPreferences(),
				YamlEditingSettings.getAssistPrefences() );
		setTextStyles(textStyles);
	}
	
	protected void setCoreAccess(final YamlCoreAccess access, final @Nullable PreferenceAccess mainDocTypePrefs) {
		this.coreAccess= (access != null) ? access : YamlCore.getWorkbenchAccess();
		this.mainDocTypePrefs= mainDocTypePrefs;
	}
	
	
	private PreferenceAccess getMainDocTypePrefs() {
		var mainDocTypePrefs= this.mainDocTypePrefs;
		return (mainDocTypePrefs != null) ? mainDocTypePrefs : this.coreAccess.getPrefs();
	}
	
	@Override
	protected void initTextStyles() {
		setTextStyles(YamlUIPlugin.getInstance().getYamlTextStyles());
	}
	
	@Override
	protected void initScanners() {
		final var textStyles= getTextStyles();
		
		addScanner(YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE,
				new YamlDefaultTextStyleScanner(textStyles) );
		addScanner(YamlDocumentConstants.YAML_DIRECTIVE_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, YamlTextStyles.TS_DIRECTIVE) );
		addScanner(YamlDocumentConstants.YAML_COMMENT_CONTENT_TYPE,
				new CommentScanner(textStyles, YamlTextStyles.TS_COMMENT, YamlTextStyles.TS_TASK_TAG,
						getMainDocTypePrefs() ));
		addScanner(YamlDocumentConstants.YAML_KEY_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, YamlTextStyles.TS_KEY) );
		addScanner(YamlDocumentConstants.YAML_TAG_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, YamlTextStyles.TS_TAG) );
		addScanner(YamlDocumentConstants.YAML_VALUE_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, YamlTextStyles.TS_DEFAULT) );
	}
	
	
	@Override
	public List<SourceEditorAddon> getAddOns() {
		final List<SourceEditorAddon> addons= super.getAddOns();
		if (this.autoEditStrategy != null) {
			addons.add(this.autoEditStrategy);
		}
		return addons;
	}
	
	@Override
	public void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		if (this.autoEditStrategy != null) {
			this.autoEditStrategy.getSettings().handleSettingsChanged(groupIds, options);
		}
	}
	
	
	@Override
	public String[] getConfiguredContentTypes(final ISourceViewer sourceViewer) {
		return CONTENT_TYPES;
	}
	
	
	@Override
	public ICharPairMatcher createPairMatcher() {
		return new YamlBracketPairMatcher(getDocumentContentInfo());
	}
	
	@Override
	public ITextDoubleClickStrategy getDoubleClickStrategy(final ISourceViewer sourceViewer, final String contentType) {
		if (this.doubleClickStrategy == null) {
			this.doubleClickStrategy= new YamlDoubleClickStrategy(
					YamlHeuristicTokenScanner.create(getDocumentContentInfo()) );
		}
		return this.doubleClickStrategy;
	}
	
	
	@Override
	protected IIndentSettings getIndentSettings() {
		return this.coreAccess.getYamlCodeStyle();
	}
	
	@Override
	public String[] getDefaultPrefixes(final ISourceViewer sourceViewer, final String contentType) {
		return new String[] { "#", "" }; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	
	@Override
	public boolean isSmartInsertSupported() {
		return true;
	}
	
	@Override
	public boolean isSmartInsertByDefault() {
		return EPreferences.getInstancePrefs()
				.getPreferenceValue(YamlEditingSettings.SMARTINSERT_BYDEFAULT_ENABLED_PREF);
	}
	
	
	@Override
	public IAutoEditStrategy[] getAutoEditStrategies(final ISourceViewer sourceViewer, final String contentType) {
		if (getSourceEditor() == null) {
			return super.getAutoEditStrategies(sourceViewer, contentType);
		}
		if (this.autoEditStrategy == null) {
			this.autoEditStrategy= createYamlAutoEditStrategy();
		}
		return new IAutoEditStrategy[] { this.autoEditStrategy };
	}
	
	protected YamlAutoEditStrategy createYamlAutoEditStrategy() {
		return new YamlAutoEditStrategy(this.coreAccess, getSourceEditor());
	}
	
	
	@Override
	public IReconciler getReconciler(final ISourceViewer sourceViewer) {
		final SourceEditor editor= getSourceEditor();
		if (!(editor instanceof SourceEditor1)) {
			return null;
		}
		final EcoReconciler2 reconciler= new EcoReconciler2(editor);
		reconciler.setDelay(500);
		reconciler.addReconcilingStrategy(new SourceUnitReconcilingStrategy());
		
//		final IReconcilingStrategy spellingStrategy= getSpellingStrategy(sourceViewer);
//		if (spellingStrategy != null) {
//			reconciler.addReconcilingStrategy(spellingStrategy);
//		}
		
		return reconciler;
	}
	
	
	@Override
	public void initContentAssist(final ContentAssist assistant) {
		final ContentAssistComputerRegistry registry= YamlUIPlugin.getInstance().getYamlEditorContentAssistRegistry();
		
		{	final ContentAssistProcessor processor= new ContentAssistProcessor(assistant,
					YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE, registry, getSourceEditor());
			assistant.setContentAssistProcessor(processor, YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE);
		}
	}
	
	@Override
	protected void collectHyperlinkDetectorTargets(final Map<String, IAdaptable> targets,
			final ISourceViewer sourceViewer) {
		targets.put("org.eclipse.statet.yaml.editorHyperlinks.YamlEditorTarget", getSourceEditor()); //$NON-NLS-1$
	}
	
	
	@Override
	protected IInformationProvider getQuickInformationProvider(final ISourceViewer sourceViewer,
			final int operation) {
		final SourceEditor editor= getSourceEditor();
		if (editor == null) {
			return null;
		}
		switch (operation) {
		case SourceEditorViewer.SHOW_SOURCE_OUTLINE:
			return new YamlQuickOutlineInformationProvider(editor, operation);
		default:
			return null;
		}
	}
	
}
