/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui.editors;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;

import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.ui.YamlUI;
import org.eclipse.statet.yaml.ui.sourceediting.YamlEditingSettings;


@NonNullByDefault
public class YamlEditorBuild {
	
	
	public static final String GROUP_ID= "Yaml/editor/build"; //$NON-NLS-1$
	
	
	public static final BooleanPref PROBLEMCHECKING_ENABLED_PREF= new BooleanPref(
			YamlEditingSettings.EDITING_PREF_QUALIFIER, "ProblemChecking.enabled"); //$NON-NLS-1$
	
	
	public static final String ERROR_ANNOTATION_TYPE=       "org.eclipse.statet.yaml.editorAnnotations.ErrorProblem"; //$NON-NLS-1$
	public static final String WARNING_ANNOTATION_TYPE=     "org.eclipse.statet.yaml.editorAnnotations.WarningProblem"; //$NON-NLS-1$
	public static final String INFO_ANNOTATION_TYPE=        "org.eclipse.statet.yaml.editorAnnotations.InfoProblem"; //$NON-NLS-1$
	
	private static final IssueTypeSet.ProblemTypes PROBLEM_ANNOTATION_TYPES= new IssueTypeSet.ProblemTypes(
			ERROR_ANNOTATION_TYPE, WARNING_ANNOTATION_TYPE, INFO_ANNOTATION_TYPE );
	
	public static final IssueTypeSet.ProblemCategory YAML_MODEL_PROBLEM_CATEGORY= new IssueTypeSet.ProblemCategory(
			YamlModel.YAML_TYPE_ID,
			null, PROBLEM_ANNOTATION_TYPES );
	
	public static final IssueTypeSet YAML_ISSUE_TYPE_SET= new IssueTypeSet(YamlUI.BUNDLE_ID,
			new IssueTypeSet.TaskCategory(null, null),
			ImCollections.newList(
					YamlEditorBuild.YAML_MODEL_PROBLEM_CATEGORY ));
	
}
