/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui.sourceediting;

import org.eclipse.jface.preference.IPreferenceStore;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.EnumPref;

import org.eclipse.statet.internal.yaml.ui.YamlUIPlugin;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.TabAction;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistPreferences;
import org.eclipse.statet.yaml.ui.YamlUI;


@NonNullByDefault
public class YamlEditingSettings {
	
	
	public static final String EDITING_PREF_QUALIFIER= YamlUI.BUNDLE_ID + "/editor/editing"; //$NON-NLS-1$
	
	
	public static final BooleanPref SPELLCHECKING_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SpellCheck.enabled"); //$NON-NLS-1$
	
	// not in group
	public static final BooleanPref FOLDING_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "Folding.enabled"); //$NON-NLS-1$
	
	public static final String FOLDING_SHARED_GROUP_ID= "yaml/editor/folding.shared"; //$NON-NLS-1$
	
	public static final BooleanPref FOLDING_RESTORE_STATE_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "Folding.RestoreState.enabled"); //$NON-NLS-1$
	
	public static final BooleanPref MARKOCCURRENCES_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "MarkOccurrences.enabled"); //$NON-NLS-1$
	
	
	public static final String SMARTINSERT_GROUP_ID= "yaml/editor/smartinsert"; //$NON-NLS-1$
	
	public static final Preference<Boolean> SMARTINSERT_BYDEFAULT_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.ByDefault.enabled"); //$NON-NLS-1$
	
	public static final EnumPref<TabAction> SMARTINSERT_TAB_ACTION_PREF= new EnumPref<>(
			EDITING_PREF_QUALIFIER, "SmartInsert.Tab.action", TabAction.class, //$NON-NLS-1$
			TabAction.INSERT_INDENT_LEVEL );
	
	public static final Preference<Boolean> SMARTINSERT_CLOSEBRACKETS_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.CloseBrackets.enabled"); //$NON-NLS-1$
	public static final Preference<Boolean> SMARTINSERT_CLOSEQUOTES_ENABLED_PREF= new BooleanPref(
			EDITING_PREF_QUALIFIER, "SmartInsert.CloseQuotes.enabled"); //$NON-NLS-1$
	
	
	public static final String ASSIST_PREF_QUALIFIER= YamlUI.BUNDLE_ID + "/editor/assist"; //$NON-NLS-1$
	public static final String ASSIST_YAML_PREF_QUALIFIER= ASSIST_PREF_QUALIFIER + "/Yaml"; //$NON-NLS-1$
	
	public static AssistPreferences getAssistPrefences() {
		return LtkUIPreferences.getAssistPreferences();
	}
	
	
	public static IPreferenceStore getPreferenceStore() {
		return YamlUIPlugin.getInstance().getPreferenceStore();
	}
	
}
