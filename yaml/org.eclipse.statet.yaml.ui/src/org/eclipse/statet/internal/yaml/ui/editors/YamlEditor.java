/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.yaml.ui.YamlUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1OutlinePage;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.yaml.core.YamlCore;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentContentInfo;
import org.eclipse.statet.yaml.ui.sourceediting.YamlEditingSettings;
import org.eclipse.statet.yaml.ui.sourceediting.YamlSourceViewerConfiguration;
import org.eclipse.statet.yaml.ui.sourceediting.YamlSourceViewerConfigurator;


@NonNullByDefault
public class YamlEditor extends SourceEditor1 {
	
	
	private YamlSourceViewerConfigurator yamlConfig;
	
	
	public YamlEditor() {
		super(YamlCore.YAML_CONTENT_TYPE);
		this.yamlConfig= nonNullAssert(this.yamlConfig);
	}
	
	
	@Override
	protected void initializeEditor() {
		super.initializeEditor();
		
		setEditorContextMenuId("org.eclipse.statet.yaml.menus.YamlEditorContextMenu"); //$NON-NLS-1$
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfiguration() {
		setDocumentProvider(YamlUIPlugin.getInstance().getYamlDocumentProvider());
		
		enableStructuralFeatures(YamlModel.getYamlModelManager(),
				YamlEditingSettings.FOLDING_ENABLED_PREF,
				YamlEditingSettings.MARKOCCURRENCES_ENABLED_PREF );
		
		this.yamlConfig= new YamlSourceViewerConfigurator(null,
				new YamlSourceViewerConfiguration(YamlDocumentContentInfo.INSTANCE, 0,
						this, null, null, null, null ));
		return this.yamlConfig;
	}
	
	
//	@Override
//	protected SourceEditorAddon createCodeFoldingProvider() {
//		return new FoldingEditorAddon(new YamlDefaultFoldingProvider());
//	}
	
	
	@Override
	protected void handlePreferenceStoreChanged(final org.eclipse.jface.util.PropertyChangeEvent event) {
		if (AbstractDecoratedTextEditorPreferenceConstants.EDITOR_TAB_WIDTH.equals(event.getProperty())
				|| AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS.equals(event.getProperty())) {
			return;
		}
		super.handlePreferenceStoreChanged(event);
	}
	
	
	@Override
	protected boolean isTabsToSpacesConversionEnabled() {
		return false;
	}
	
	
	@Override
	protected void collectContextMenuPreferencePages(final List<String> pageIds) {
		super.collectContextMenuPreferencePages(pageIds);
		pageIds.add("org.eclipse.statet.yaml.preferencePages.YamlEditor"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.yaml.preferencePages.YamlTextStyles"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.yaml.preferencePages.YamlEditorTemplates"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.yaml.preferencePages.YamlCodeStyle"); //$NON-NLS-1$
	}
	
	@Override
	protected SourceEditor1OutlinePage createOutlinePage() {
		return new YamlOutlinePage(this);
	}
	
	
	@Override
	public @NonNull String[] getShowInTargetIds() {
		return new @NonNull String[] {
			IPageLayout.ID_PROJECT_EXPLORER,
			IPageLayout.ID_OUTLINE };
	}
	
}
