/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui;

import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_BOLD_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_COLOR_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_ITALIC_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_STRIKETHROUGH_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_UNDERLINE_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_USE_SUFFIX;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.workbench.ui.IWaThemeConstants;
import org.eclipse.statet.ecommons.workbench.ui.util.ThemeUtil;

import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.TabAction;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.yaml.ui.YamlUI;
import org.eclipse.statet.yaml.ui.editors.YamlEditorBuild;
import org.eclipse.statet.yaml.ui.sourceediting.YamlEditingSettings;
import org.eclipse.statet.yaml.ui.text.YamlTextStyles;


public class YamlUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public YamlUIPreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store= YamlUIPlugin.getInstance().getPreferenceStore();
		final IScopeContext scope= DefaultScope.INSTANCE;
		final IEclipsePreferences pref= scope.getNode(YamlUI.BUNDLE_ID);
		final ThemeUtil theme= new ThemeUtil();
		
		EditorsUI.useAnnotationsPreferencePage(store);
		EditorsUI.useQuickDiffPreferencePage(store);
		
		pref.put(YamlTextStyles.TS_DEFAULT + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_DEFAULT_COLOR));
		pref.putBoolean(YamlTextStyles.TS_DEFAULT + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DEFAULT + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DEFAULT + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DEFAULT + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(YamlTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_UNDEFINED_COLOR));
		pref.putBoolean(YamlTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(YamlTextStyles.TS_DIRECTIVE + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_PREPROCESSOR_COLOR));
		pref.putBoolean(YamlTextStyles.TS_DIRECTIVE + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DIRECTIVE + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DIRECTIVE + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_DIRECTIVE + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(YamlTextStyles.TS_INDICATOR + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_OPERATOR_COLOR));
		pref.putBoolean(YamlTextStyles.TS_INDICATOR + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(YamlTextStyles.TS_INDICATOR + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_INDICATOR + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_INDICATOR + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(YamlTextStyles.TS_BRACKET + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_DEFAULT_COLOR));
		pref.putBoolean(YamlTextStyles.TS_BRACKET + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_BRACKET + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_BRACKET + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_BRACKET + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		pref.put(YamlTextStyles.TS_BRACKET + TEXTSTYLE_USE_SUFFIX, YamlTextStyles.TS_INDICATOR);
		
		pref.put(YamlTextStyles.TS_KEY + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_STRING_COLOR));
		pref.putBoolean(YamlTextStyles.TS_KEY + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_KEY + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_KEY + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_KEY + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(YamlTextStyles.TS_TAG + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_DOCU_TAG_COLOR));
		pref.putBoolean(YamlTextStyles.TS_TAG + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(YamlTextStyles.TS_TAG + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_TAG + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_TAG + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(YamlTextStyles.TS_COMMENT + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_COMMENT_COLOR));
		pref.putBoolean(YamlTextStyles.TS_COMMENT + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_COMMENT + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_COMMENT + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_COMMENT + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(YamlTextStyles.TS_TASK_TAG + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_COMMENT_TASKTAG_COLOR));
		pref.putBoolean(YamlTextStyles.TS_TASK_TAG + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(YamlTextStyles.TS_TASK_TAG + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_TASK_TAG + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(YamlTextStyles.TS_TASK_TAG + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		{	final IEclipsePreferences node= scope.getNode(YamlEditingSettings.ASSIST_YAML_PREF_QUALIFIER);
			node.put(ContentAssistComputerRegistry.CIRCLING_ORDERED, "yaml-elements:false,templates:true,paths:true"); //$NON-NLS-1$
			node.put(ContentAssistComputerRegistry.DEFAULT_DISABLED, ""); //$NON-NLS-1$
		}
		// EditorPreferences
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.FOLDING_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.FOLDING_RESTORE_STATE_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.MARKOCCURRENCES_ENABLED_PREF, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_BYDEFAULT_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_TAB_ACTION_PREF, TabAction.INSERT_INDENT_LEVEL);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_CLOSEBRACKETS_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_CLOSEQUOTES_ENABLED_PREF, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, YamlEditorBuild.PROBLEMCHECKING_ENABLED_PREF, Boolean.TRUE);
		
	}
	
}
