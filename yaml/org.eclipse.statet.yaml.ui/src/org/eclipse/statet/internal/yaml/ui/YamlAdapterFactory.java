/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.texteditor.IDocumentProvider;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.ElementLabelProvider;
import org.eclipse.statet.ltk.ui.SourceStructContentProvider;
import org.eclipse.statet.yaml.ui.YamlElementLabelProvider;


@NonNullByDefault
public class YamlAdapterFactory implements IAdapterFactory {
	
	
	private static final @NonNull Class<?>[] ADAPTERS= new @NonNull Class<?>[] {
		SourceStructContentProvider.class,
		ElementLabelProvider.class,
		IDocumentProvider.class,
	};
	
	
	public YamlAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == SourceStructContentProvider.class) {
			return (T)new YamlSourceStructContentProvider();
		}
		if (adapterType == ElementLabelProvider.class) {
			return (T)new YamlElementLabelProvider();
		}
		if (adapterType == IDocumentProvider.class) {
			return (T)YamlUIPlugin.getInstance().getYamlDocumentProvider();
		}
		return null;
	}
	
}
