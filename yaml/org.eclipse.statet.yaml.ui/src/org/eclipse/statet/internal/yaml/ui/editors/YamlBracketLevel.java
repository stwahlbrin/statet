/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.editors;

import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedPosition;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.TextUtil;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.assist.LinkedModeBracketLevel;


@NonNullByDefault
public class YamlBracketLevel extends LinkedModeBracketLevel {
	
	
	public static final class SquareBracketPosition extends InBracketPosition {
		
		public SquareBracketPosition(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '[';
		}
		
		@Override
		public char getCloseChar() {
			return ']';
		}
		
	}
	
	public static final class CurlyBracketPosition extends InBracketPosition {
		
		public CurlyBracketPosition(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '{';
		}
		
		@Override
		public char getCloseChar() {
			return '}';
		}
		
	}
	
	public final static class QuotedDPosition extends InBracketPosition {
		
		public QuotedDPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '"';
		}
		
		@Override
		public char getCloseChar() {
			return '"';
		}
		
		@Override
		public boolean insertCR(final int charOffset) {
			return false;
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return (TextUtil.countBackward(getDocument(), offset, '\\') % 2 == 1);
		}
		
	}
	
	public final static class QuotedSPosition extends InBracketPosition {
		
		public QuotedSPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '\'';
		}
		
		@Override
		public char getCloseChar() {
			return '\'';
		}
		
		@Override
		public boolean insertCR(final int charOffset) {
			return false;
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			final IDocument document= getDocument();
			return ((TextUtil.countBackward(document, offset, '\'') % 2 == 1)
					|| (offset + 1 < document.getLength() && document.getChar(offset + 1) == '\'') );
		}
		
	}
	
	
	public static InBracketPosition createPosition(final char c, final IDocument document,
			final int offset, final int length, final int sequence) {
		switch(c) {
		case '{':
			return new CurlyBracketPosition(document, offset, length, sequence);
		case '[':
			return new SquareBracketPosition(document, offset, length, sequence);
		case '"':
			return new QuotedDPosition(document, offset, length, sequence);
		case '\'':
			return new QuotedSPosition(document, offset, length, sequence);
		default:
			throw new IllegalArgumentException("Invalid position type: " + c); //$NON-NLS-1$
		}
	}
	
	
	public YamlBracketLevel(final LinkedModeModel model,
			final IDocument document, final DocContentSections docContentSections,
			final List<? extends LinkedPosition> positions, final int mode) {
		super(model, document, docContentSections, positions, mode);
	}
	
}
