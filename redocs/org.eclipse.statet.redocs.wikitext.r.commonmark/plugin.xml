<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<!--
 #=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<plugin>
   
   <extension
         point="org.eclipse.core.runtime.preferences">
      <initializer class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.CommonmarkRweavePreferenceInitializer"/>
   </extension>
   
   <extension
         point="org.eclipse.core.contenttype.contentTypes">
      <content-type
            id="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            base-type="org.eclipse.mylyn.wikitext.commonmark"
            name="%contentTypes_CommonmarkRweave_name"
            file-extensions="Rmd,rmd,Rcm,rcm"
            priority="normal">
         <property
               name="bom">
         </property>
      </content-type>
   </extension>
   <extension
         point="org.eclipse.statet.docmlet.WikitextMarkupLanguages">
      <markupLanguage
            name="CommonMark+R"
            label="RMarkdown (CommonMark+R)"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkLanguage"
            configClass="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkConfigImpl"
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave">
      </markupLanguage>
   </extension>
   <extension
         point="org.eclipse.statet.ltk.ContentTypeActivation">
      <contentType
            id="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            secondaryId="org.eclipse.statet.r.contentTypes.R"/>
      <contentType
            id="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            secondaryId="org.eclipse.statet.redocs.contentTypes.WikidocRweave"/>
   </extension>
   
   <extension
         point="org.eclipse.statet.autonature.AutoConfigurations">
      <onFileContent
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            enable="true">
         <ensureProjectNature
               natureId="org.eclipse.statet.r.resourceProjects.R"/>
         <ensureProjectNature
               natureId="org.eclipse.statet.docmlet.resourceProjects.Wikitext"/>
      </onFileContent>
   </extension>
   
   <extension
         point="org.eclipse.statet.ltk.ModelTypes">
      <contentType
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            modelTypeId="WikidocRweave">
      </contentType>
   </extension>
   
   <extension
         point="org.eclipse.team.core.fileTypes">
      <fileTypes
            extension="Rmd"
            type="text"/>
      <fileTypes
            extension="rmd"
            type="text"/>
      <fileTypes
            extension="Rcm"
            type="text"/>
      <fileTypes
            extension="rcm"
            type="text"/>
   </extension>
   
   <extension
         point="org.eclipse.core.filebuffers.documentSetup">
      <participant
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.CommonmarkRweaveDocumentSetupParticipant">
      </participant>
   </extension>
   
   <extension
         point="org.eclipse.core.runtime.adapters">
      <factory
            adaptableType="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkConfigImpl"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.ConfigUIAdapterFactory">
         <adapter
               type="org.eclipse.statet.docmlet.wikitext.ui.config.MarkupConfigUIAdapter">
         </adapter>
      </factory>
   </extension>
   <extension
         point="org.eclipse.core.runtime.adapters">
      <factory
            adaptableType="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkLanguage"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.LanguageUIAdapterFactory">
         <adapter
               type="org.eclipse.statet.docmlet.wikitext.ui.sourceediting.MarkupCompletionExtension">
         </adapter>
      </factory>
   </extension>
   
   <extension
         point="org.eclipse.core.expressions.definitions">
      <definition
            id="org.eclipse.statet.redocs.expressions.isSelectionActive.CommonmarkRweaveResource">
         <with
               variable="selection">
            <count
                  value="1"/>
            <iterate>
               <adapt
                     type="org.eclipse.core.resources.IFile">
                  <test
                        property="org.eclipse.core.resources.contentTypeId"
                        value="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
                        forcePluginActivation="true"/>
               </adapt>
            </iterate>
         </with>
      </definition>
   </extension>
   
   <extension
         point="org.eclipse.ui.editors">
      <editor
            id="org.eclipse.statet.redocs.editors.CommonmarkRweave"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.CommonmarkRweaveDocEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            icon="platform:/plugin/org.eclipse.statet.redocs.wikitext.r/icons/obj_16/wikidoc_rweave-file.png"
            name="%editors_CommonmarkRweave_name"
            default="true">
         <contentTypeBinding contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"/>
      </editor>
   </extension>
   
   <extension
         point="org.eclipse.ui.actionSetPartAssociations">
      <actionSetPartAssociation
            targetID="org.eclipse.ui.edit.text.actionSet.presentation">
         <part
               id="org.eclipse.statet.redocs.editors.CommonmarkRweave"/>
      </actionSetPartAssociation>
   </extension>
   
   <extension
         point="org.eclipse.ui.editors.templates">
      <contextType
            id="CommonMark+Rweave_NewDoc"
            registryId="org.eclipse.statet.redocs.templates.WikitextRweaveDoc"
            class="org.eclipse.statet.redocs.wikitext.r.core.source.WikitextRweaveTemplateContextType">
      </contextType>
      <contextType
            id="CommonMark+Rweave_Weave:DocDefault"
            registryId="org.eclipse.statet.redocs.templates.WikitextRweaveCodegen"
            class="org.eclipse.statet.redocs.wikitext.r.core.source.WikitextRweaveTemplateContextType">
      </contextType>
      <include
            file="templates/default-doc-templates.xml"
            translations="templates/default-templates.properties">
      </include>
   </extension>
   <extension
         point="org.eclipse.statet.docmlet.WikitextDocTemplates">
      <category
            id="CommonmarkRweave.NewDoc"
            image="platform:/plugin/org.eclipse.statet.redocs.wikitext.r/icons/tool_16/new-wikidoc_rweave-file.png"
            label="%docTemplates_NewCommonmarkRweave_name"
            itemImage="platform:/plugin/org.eclipse.statet.redocs.wikitext.r/icons/obj_16/wikidoc_rweave-file.png"
            configurationClass="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.NewDocTemplateCategoryConfiguration">
      </category>
   </extension>
   
   <extension
         point="org.eclipse.ui.newWizards">
      <wizard
            id="org.eclipse.statet.redocs.newWizards.CommonmarkRweaveDocCreation"
            category="org.eclipse.statet.r.newWizards.RCategory"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.NewDocCreationWizard"
            icon="platform:/plugin/org.eclipse.statet.redocs.wikitext.r/icons/tool_16/new-wikidoc_rweave-file.png"
            name="%wizards_NewCommonmarkRweaveDoc_name"
            project="false">
         <description>
            %wizards_NewCommonmarkRweaveDoc_description
         </description>
      </wizard>
   </extension>
   
   <extension
         point="org.eclipse.ui.perspectiveExtensions">
      <perspectiveExtension targetID="org.eclipse.statet.base.perspectives.StatetPerspective">
         <newWizardShortcut id="org.eclipse.statet.redocs.newWizards.CommonmarkRweaveDocCreation"/>
      </perspectiveExtension>
   </extension>
   <extension
         point="org.eclipse.ui.navigator.navigatorContent">
      <commonWizard
            type="new"
            menuGroupId="org.eclipse.statet"
            wizardId="org.eclipse.statet.redocs.newWizards.CommonmarkRweaveDocCreation">
         <enablement>
            <or>
               <adapt
                     type="org.eclipse.core.resources.IContainer">
                  <test
                        property="org.eclipse.core.resources.projectNature"
                        value="org.eclipse.statet.r.resourceProjects.R">
                  </test>
               </adapt>
            </or></enablement>
      </commonWizard>
   </extension>
   
   <extension
         point="org.eclipse.compare.contentViewers">
      <viewer
            id="org.eclipse.statet.redocs.compareContentViewers.CommonmarkRweave"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.CommonmarkRweaveContentViewerCreator">
      </viewer>
      <contentTypeBinding
            contentViewerId="org.eclipse.statet.redocs.compareContentViewers.CommonmarkRweave"
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"/>
   </extension>
   <extension
         point="org.eclipse.compare.contentMergeViewers">
      <viewer
            id="org.eclipse.statet.redocs.compareContentViewers.CommonmarkRweaveMergeViewer"
            class="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.CommonmarkRweaveMergeViewerCreator">
      </viewer>
      <contentTypeBinding
            contentMergeViewerId="org.eclipse.statet.redocs.compareContentViewers.CommonmarkRweaveMergeViewer"
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"/>
   </extension>
   
<!-- R Code Launch -->
   <extension
         point="org.eclipse.statet.r.ui.rCodeLaunchContentHandler">
      <contentHandler
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            handler="org.eclipse.statet.internal.redocs.wikitext.r.commonmark.ui.CommonmarkRweaveSubmitContentHandler">
      </contentHandler>
   </extension>
   
<!-- Document Processing -->
   <extension
         point="org.eclipse.statet.docmlet.DocProcessing">
      <processingType
            contentTypeId="org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"
            configTypeId="org.eclipse.statet.redocs.launchConfigurations.WikitextRweaveDocProcessing"
            managerClass="org.eclipse.statet.redocs.r.ui.processing.RweaveDocProcessingManager">
      </processingType>
   </extension>
   
   <extension
         point="org.eclipse.debug.ui.launchShortcuts">
      <shortcut
            id="org.eclipse.statet.redocs.launchShortcuts.CommonmarkRweaveDocProcessing"
            label="%launchShortcuts_CommonmarkRweaveProcessing_name"
            icon="platform:/plugin/org.eclipse.statet.docmlet.base.ui/icons/tool_16/process_and_preview.png"
            class="org.eclipse.statet.docmlet.base.ui.processing.actions.RunActiveDocConfigLaunchShortcut"
            modes="run">
         <configurationType
               id="org.eclipse.statet.redocs.launchConfigurations.WikitextRweaveDocProcessing"/>
         <enablement>
            <reference
                  definitionId="org.eclipse.statet.redocs.expressions.isSelectionActive.CommonmarkRweaveResource"/>
         </enablement>
         <contextualLaunch>
            <enablement>
               <reference
                     definitionId="org.eclipse.statet.redocs.expressions.isSelectionActive.CommonmarkRweaveResource"/>
            </enablement>
         </contextualLaunch>
      </shortcut>
   </extension>
   
</plugin>
