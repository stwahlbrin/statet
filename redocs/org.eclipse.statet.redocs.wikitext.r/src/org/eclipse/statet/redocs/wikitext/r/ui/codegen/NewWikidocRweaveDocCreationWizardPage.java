/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.ui.codegen;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProjects;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizardPage;
import org.eclipse.statet.r.core.project.RProjects;


/**
 * The "New" wizard page allows setting the container for
 * the new file as well as the file name. The page
 * will only accept file name without the extension or
 * with the extension that matches the expected one (r).
 */
public class NewWikidocRweaveDocCreationWizardPage extends NewElementWizardPage {
	
	
	ResourceGroup resourceGroup;
	
	
	public NewWikidocRweaveDocCreationWizardPage(final IStructuredSelection selection,
			final String defaultExtension) {
		super("NewWikidocRweaveDocCreationWizardPage", selection); //$NON-NLS-1$
		
		this.resourceGroup= new ResourceGroup(defaultExtension, new ProjectNatureContainerFilter(
				ImCollections.newSet(WikitextProjects.WIKITEXT_NATURE_ID, RProjects.R_NATURE_ID) ));
	}
	
	@Override
	protected void createContents(final Composite parent) {
		this.resourceGroup.createGroup(parent);
	}
	
	@Override
	public void setVisible(final boolean visible) {
		super.setVisible(visible);
		if (visible) {
			this.resourceGroup.setFocus();
		}
	}
	
	public void saveSettings() {
		this.resourceGroup.saveSettings();
	}
	
	@Override
	protected void validatePage() {
		updateStatus(this.resourceGroup.validate());
	}
	
}
