/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.r.core.project.RIssues;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.redocs.wikitext.r.core.WikitextRweaveCore;
import org.eclipse.statet.redocs.wikitext.r.core.model.WikidocRweaveSourceUnit;
import org.eclipse.statet.redocs.wikitext.r.core.model.WikitextRweaveModel;


@NonNullByDefault
public class WikidocRweaveSuModelContainer
		extends WikidocSourceUnitModelContainer<WikidocRweaveSourceUnit> {
	
	
	public static final IssueTypeSet ISSUE_TYPE_SET= new IssueTypeSet(WikitextRweaveCore.BUNDLE_ID,
			WikidocSourceUnitModelContainer.ISSUE_TYPE_SET,
			RIssues.R_MODEL_PROBLEM_CATEGORY );
	
	
	public WikidocRweaveSuModelContainer(final WikidocRweaveSourceUnit unit,
			final @Nullable SourceUnitIssueSupport issueSupport) {
		super(unit, issueSupport);
	}
	
	
	@Override
	public boolean isContainerFor(final String modelTypeId) {
		return (modelTypeId == WikitextModel.WIKIDOC_TYPE_ID
				|| modelTypeId == WikitextRweaveModel.WIKIDOC_R_MODEL_TYPE_ID );
	}
	
	@Override
	public String getNowebType() {
		return RModel.R_TYPE_ID;
	}
	
	
}
