/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.core.model;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.redocs.r.core.model.RChunkReconciler;
import org.eclipse.statet.redocs.wikitext.r.core.model.WikidocRweaveSourceUnit;
import org.eclipse.statet.redocs.wikitext.r.core.source.RweaveMarkupLanguage;


@NonNullByDefault
class WikidocRChunkReconciler extends RChunkReconciler<
		WikidocRweaveSourceUnit, WikidocSourceUnitModelInfo, WikidocRweaveSuModelContainer> {
	
	
	private static final Map<String, WikidocRChunkReconciler> INSTANCES= new HashMap<>();
	
	static final WikidocRChunkReconciler getInstance(final RweaveMarkupLanguage markupLanguage) {
		synchronized(WikidocRChunkReconciler.class) {
			WikidocRChunkReconciler reconciler= INSTANCES.get(markupLanguage.getName());
			if (reconciler == null) {
				reconciler= new WikidocRChunkReconciler(markupLanguage.getName(),
						markupLanguage.getRChunkStartLinePattern(),
						markupLanguage.getRChunkRefLinePattern(),
						markupLanguage.getRChunkEndLinePattern() );
				INSTANCES.put(markupLanguage.getName(), reconciler);
			}
			return reconciler;
		}
	}
	
	
	public WikidocRChunkReconciler(final String name, final Pattern chunkStartLinePattern,
			final @Nullable Pattern chunkRefLinePattern, final Pattern chunkEndLinePattern) {
		super(name, chunkStartLinePattern, chunkRefLinePattern, chunkEndLinePattern);
	}
	
	
}
