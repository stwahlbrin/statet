/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.ui.codegen;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateBuffer;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.ecommons.templates.TemplateMessages;

import org.eclipse.statet.internal.redocs.tex.r.RedocsTexRPlugin;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.templates.CodeGenerationTemplateContext;
import org.eclipse.statet.ltk.ui.templates.TemplateUtils.EvaluatedTemplate;
import org.eclipse.statet.redocs.tex.r.ui.TexRweaveUI;


/**
 * Class that offers access to the code templates contained.
 */
public class CodeGeneration {
	
	
	public static ContextTypeRegistry getDocContextTypeRegistry() {
		return RedocsTexRPlugin.getInstance().getDocTemplateContextTypeRegistry();
	}
	
	public static TemplateStore getDocTemplateStore() {
		return RedocsTexRPlugin.getInstance().getDocTemplateStore();
	}
	
	
	/**
	 * Generates initial content for a new document file.
	 * 
	 * @param sourceUnit the source unit to create the source for. The unit does not need to exist.
	 * @param lineDelimiter The line delimiter to be used.
	 * @return the new content or <code>null</code> if the template is undefined or empty.
	 * @throws CoreException thrown when the evaluation of the code template fails.
	 */
	public static EvaluatedTemplate getNewDocContent(final SourceUnit sourceUnit,
			final String lineDelimiter, final Template template) throws CoreException {
		if (template == null) {
			return null;
		}
		
		final var context= new CodeGenerationTemplateContext(
				getDocContextTypeRegistry().getContextType(template.getContextTypeId()),
				sourceUnit, lineDelimiter );
		
		try {
			final TemplateBuffer buffer= context.evaluate(template);
			if (buffer == null) {
				return null;
			}
			return new EvaluatedTemplate(buffer, lineDelimiter);
		}
		catch (final Exception e) {
			throw new CoreException(new Status(IStatus.ERROR, TexRweaveUI.BUNDLE_ID,
					NLS.bind(TemplateMessages.TemplateEvaluation_error_description,
							template.getDescription() ),
					e ));
		}
	}
	
}
