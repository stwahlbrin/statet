/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.redocs.tex.r.core.util.TexRweaveCoreAccessWrapper;


@NonNullByDefault
public class TexRweaveCore {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.redocs.tex.r"; //$NON-NLS-1$
	
	
	/**
	 * Content type id for Sweave (LaTeX+R) document sources
	 */
	public static final String LTX_R_CONTENT_ID= "org.eclipse.statet.redocs.contentTypes.LtxRweave"; //$NON-NLS-1$
	
	public static final IContentType LTX_R_CONTENT_TYPE;
	
	static {
		final IContentTypeManager contentTypeManager= Platform.getContentTypeManager();
		LTX_R_CONTENT_TYPE= nonNullAssert(contentTypeManager.getContentType(LTX_R_CONTENT_ID));
	}
	
	
	public static final TexCoreAccess TEX_WORKBENCH_ACCESS= new TexRweaveCoreAccessWrapper(
			TexCore.getWorkbenchAccess() );
	
	public static TexCoreAccess getTexWorkbenchAccess() {
		return TEX_WORKBENCH_ACCESS;
	}
	
	private static final Map<IProject, TexRweaveCoreAccessWrapper> TEX_PROJECT_ACCESS_MAP= new HashMap<>();
	
	private static TexCoreAccess getResourceTexCoreAccess(final IResource resource) {
		final IProject project= resource.getProject();
		if (project != null) {
			final TexCoreAccess parent= TexCore.getContextAccess(project);
			TexRweaveCoreAccessWrapper wrapper;
			synchronized (TEX_PROJECT_ACCESS_MAP) {
				wrapper= TEX_PROJECT_ACCESS_MAP.get(project);
				if (wrapper == null) {
					wrapper= new TexRweaveCoreAccessWrapper(parent);
					TEX_PROJECT_ACCESS_MAP.put(project, wrapper);
					return wrapper;
				}
			}
			wrapper.setParent(parent);
			return wrapper;
		}
		return getTexWorkbenchAccess();
	}
	
	public static TexCoreAccess getTexContextAccess(final @Nullable IAdaptable adaptable) {
		if (adaptable != null) {
			if (adaptable instanceof IResource) {
				return getResourceTexCoreAccess((IResource)adaptable);
			}
		}
		return getTexWorkbenchAccess();
	}
	
	
}
