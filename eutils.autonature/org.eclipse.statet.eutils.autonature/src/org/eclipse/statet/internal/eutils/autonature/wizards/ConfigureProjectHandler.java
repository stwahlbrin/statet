/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature.wizards;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


public class ConfigureProjectHandler extends AbstractHandler {
	
	
	public ConfigureProjectHandler() {
	}
	
	
	private IProject getProject(final Object element) {
		if (element instanceof IProject) {
			return (IProject) element;
		}
		if (element instanceof IAdaptable) {
			return ((IAdaptable) element).getAdapter(IProject.class);
		}
		return null;
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		if (selection instanceof IStructuredSelection) {
			final IStructuredSelection structuredSelection= (IStructuredSelection) selection;
			if (structuredSelection.size() == 1) {
				final IProject project= getProject(structuredSelection.getFirstElement());
				if (project != null) {
					final IWorkbenchPart activePart= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
					
					final ConfigureProjectWizard wizard= new ConfigureProjectWizard(project);
					final WizardDialog dialog= new WizardDialog(
							(activePart != null) ? activePart.getSite().getShell() : UIAccess.getActiveWorkbenchShell(true),
							wizard );
					dialog.open();
				}
			}
		}
		return null;
	}
	
}
