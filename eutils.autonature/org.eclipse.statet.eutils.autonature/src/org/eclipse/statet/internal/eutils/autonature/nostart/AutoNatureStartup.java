/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature.nostart;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.ui.IStartup;

import org.eclipse.statet.internal.eutils.autonature.Activator;


public class AutoNatureStartup implements IStartup {
	
	
	public AutoNatureStartup() {
	}
	
	
	@Override
	public void earlyStartup() {
		final IPreferencesService preferences= Platform.getPreferencesService();
		if (preferences.getBoolean(Activator.BUNDLE_ID, Activator.ENABLED_PREF_KEY, true, null)) {
			Activator.getInstance().runStartup();
		}
	}
	
}
