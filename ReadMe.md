# Eclipse StatET™: Tooling for the R Language

Eclipse StatET is an Eclipse-based IDE for R.

  * Home Page: https://www.eclipse.org/statet
  * Project Page: https://projects.eclipse.org/projects/science.statet
  * [NOTICE](NOTICE.md) (with license information)


## Contributing

Contributions are always welcome, please see [CONTRIBUTING](CONTRIBUTING.md) for information.


## Contact

Contact other users and the project developers via the project’s Users Mailing List `statet-users`:
  * https://projects.eclipse.org/projects/science.statet/contact
