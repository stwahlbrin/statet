/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.emf.ui;

import java.util.List;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.value.DecoratingObservableValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.observable.value.ValueDiff;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.databinding.viewers.IViewerObservableValue;
import org.eclipse.jface.viewers.AbstractListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;


public class ListViewerObservableValueDecorator extends DecoratingObservableValue
		implements IViewerObservableValue, Listener {
	
	
	private final List<Object> input;
	
	private AbstractListViewer viewer;
	
	
	/**
	 * @param decorated
	 * @param viewer
	 */
	public ListViewerObservableValueDecorator(final IObservableValue decorated,
			final AbstractListViewer viewer, final List<Object> input) {
		super(decorated, true);
		this.viewer= viewer;
		this.input= input;
		viewer.getControl().addListener(SWT.Dispose, this);
	}
	
	
	@Override
	public void handleEvent(final Event event) {
		if (event.type == SWT.Dispose) {
			dispose();
		}
	}
	
	@Override
	public Viewer getViewer() {
		return this.viewer;
	}
	
	@Override
	public Object getValue() {
		return checkValue(super.getValue());
	}
	
	@Override
	public void setValue(Object value) {
		if (value == null) {
			value= this.input.get(0);
		}
		else if (value instanceof EObject) {
			checkInput((EObject) value);
		}
		super.setValue(value);
	}
	
	private void checkInput(final EObject value) {
		for (int i= 1; i < this.input.size(); i++) {
			if (this.input.get(i) == value) {
				return;
			}
		}
		for (int i= 1; i < this.input.size(); i++) {
			final Object item= this.input.get(i);
			if (item instanceof EObject
					&& ((EObject) item).eClass() == value.eClass()) {
				this.input.set(i, value);
				this.viewer.setInput(this.input);
				return;
			}
		}
	}
	
	private EObject checkValue(final Object value) {
		return ((value instanceof EObject) ? (EObject) value : null);
	}
	
	@Override
	protected void handleValueChange(final ValueChangeEvent event) {
		final ValueDiff diff= Diffs.createValueDiff(
				checkValue(event.diff.getOldValue()),
				checkValue(event.diff.getNewValue()) );
		fireValueChange(diff);
	}
	
	@Override
	public synchronized void dispose() {
		if (this.viewer != null) {
			final Control control= this.viewer.getControl();
			if (control != null && !control.isDisposed()) {
				control.removeListener(SWT.Dispose, this);
			}
			this.viewer= null;
		}
		super.dispose();
	}
	
}
