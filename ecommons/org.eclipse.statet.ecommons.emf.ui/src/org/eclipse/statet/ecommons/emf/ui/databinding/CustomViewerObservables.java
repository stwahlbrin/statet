/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.databinding;

import java.util.List;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.databinding.viewers.IViewerObservableValue;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.viewers.AbstractListViewer;
import org.eclipse.jface.viewers.AbstractTableViewer;
import org.eclipse.jface.viewers.ISelectionProvider;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ecommons.emf.ui.ListViewerObservableValueDecorator;
import org.eclipse.statet.internal.ecommons.emf.ui.TableViewerObservableValueDecorator;


@NonNullByDefault
public class CustomViewerObservables {
	
	
	public static <T extends EObject> IViewerObservableValue<T> observeComboSelection(
			final AbstractTableViewer viewer,
			final List<Object> input) {
		viewer.setInput(input);
		final IObservableValue<@Nullable Object> observable= ViewerProperties.singleSelection()
				.observe((ISelectionProvider)viewer);
		return new TableViewerObservableValueDecorator(observable, viewer, input);
	}
	
	public static <T extends EObject> IViewerObservableValue<T> observeComboSelection(
			final AbstractListViewer viewer,
			final List<Object> input) {
		viewer.setInput(input);
		final IObservableValue<@Nullable Object> observable= ViewerProperties.singleSelection()
				.observe((ISelectionProvider)viewer);
		return new ListViewerObservableValueDecorator(observable, viewer, input);
	}
	
}
