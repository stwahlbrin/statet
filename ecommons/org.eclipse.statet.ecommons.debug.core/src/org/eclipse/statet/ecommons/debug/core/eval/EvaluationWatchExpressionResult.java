/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.core.eval;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IWatchExpressionResult;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class EvaluationWatchExpressionResult implements IWatchExpressionResult {
	
	
	private static final String[] NO_MESSAGES= new String[0];
	
	
	private final IEvaluationResult result;
	
	
	public EvaluationWatchExpressionResult(final IEvaluationResult result) {
		this.result= result;
		
		result.free();
	}
	
	
	@Override
	public String getExpressionText() {
		return this.result.getExpressionText();
	}
	
	@Override
	public boolean hasErrors() {
		return (this.result.getStatus() == IStatus.ERROR);
	}
	
	@Override
	public @Nullable IValue getValue() {
		return this.result.getValue();
	}
	
	@Override
	public String @Nullable [] getErrorMessages() {
		if (this.result.getStatus() >= IStatus.ERROR) {
			final ImList<@NonNull String> messages= this.result.getMessages();
			if (messages != null) {
				return messages.toArray(new String[messages.size()]);
			}
		}
		return NO_MESSAGES;
	}
	
	@Override
	public @Nullable DebugException getException() {
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return this.result.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof EvaluationWatchExpressionResult) {
			return (this.result.equals(((EvaluationWatchExpressionResult) obj).result));
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "EvaluationWatchExpressionResult= " + this.result.toString(); //$NON-NLS-1$
	}
	
}
