/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.core.eval;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.debug.core.model.IValue;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface IEvaluationResult {
	
	
	int SKIPPED= IStatus.CANCEL | 0x10;
	
	
	String getExpressionText();
	
	IThread getThread();
	
	int getStatus();
	
	@Nullable IValue getValue();
	
	@Nullable ImList<@NonNull String> getMessages();
	
	
	void free();
	
}
