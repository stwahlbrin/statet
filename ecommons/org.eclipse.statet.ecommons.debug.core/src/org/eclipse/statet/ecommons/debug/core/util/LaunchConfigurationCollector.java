/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.core.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationListener;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchManager;

import org.eclipse.statet.jcommons.collections.CopyOnWriteListSet;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.debug.core.ECommonsDebugCore;
import org.eclipse.statet.internal.ecommons.debug.core.ECommonsDebugCorePlugin;


public class LaunchConfigurationCollector implements ILaunchConfigurationListener, Disposable {
	
	
	private final ILaunchConfigurationType type;
	
	private final CopyOnWriteListSet<ILaunchConfiguration> configurations= new CopyOnWriteListSet<>();
	
	
	public LaunchConfigurationCollector(final String typeId) {
		final ILaunchManager launchManager= DebugPlugin.getDefault().getLaunchManager();
		
		this.type= launchManager.getLaunchConfigurationType(typeId);
		
		launchManager.addLaunchConfigurationListener(this);
		
		try {
			for (final ILaunchConfiguration configuration : launchManager.getLaunchConfigurations()) {
				launchConfigurationAdded(configuration);
			}
		}
		catch (final CoreException e) {
			log(e);
		}
	}
	
	
	@Override
	public void dispose() {
		final ILaunchManager launchManager= DebugPlugin.getDefault().getLaunchManager();
		
		if (launchManager != null) {
			launchManager.removeLaunchConfigurationListener(this);
		}
	}
	
	
	protected final ILaunchConfigurationType getLaunchConfigurationType() {
		return this.type;
	}
	
	protected boolean include(final ILaunchConfiguration configuration) throws CoreException {
		return this.type.equals(configuration.getType());
	}
	
	
	public final ImList<ILaunchConfiguration> getConfigurations() {
		return this.configurations.toList();
	}
	
	
	@Override
	public void launchConfigurationAdded(final ILaunchConfiguration configuration) {
		try {
			if (include(configuration)) {
				this.configurations.add(configuration);
			}
		}
		catch (final CoreException e) {
			log(e);
		}
	}
	
	@Override
	public void launchConfigurationChanged(final ILaunchConfiguration configuration) {
		try {
			if (include(configuration)) {
				this.configurations.add(configuration);
			}
			else {
				this.configurations.remove(configuration);
			}
		}
		catch (final CoreException e) {
			log(e);
		}
	}
	
	@Override
	public void launchConfigurationRemoved(final ILaunchConfiguration configuration) {
		this.configurations.remove(configuration);
	}
	
	
	protected void log(final CoreException e) {
		ECommonsDebugCorePlugin.log(new Status(IStatus.ERROR, ECommonsDebugCore.BUNDLE_ID,
				"An error occurred when checking launch configurations.", e ));
	}
	
}
