/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.core.eval;

import java.util.Collection;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IWatchExpressionResult;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ErrorWatchExpressionResult implements IWatchExpressionResult {
	
	
	private final String expression;
	
	private final String[] messages;
	
	
	public ErrorWatchExpressionResult(final String expression, final String message) {
		this.expression= expression;
		this.messages= new String[] { message };
	}
	
	public ErrorWatchExpressionResult(final String expression, final Collection<String> messages) {
		this.expression= expression;
		this.messages= messages.toArray(new String[messages.size()]);
	}
	
	
	@Override
	public String getExpressionText() {
		return this.expression;
	}
	
	@Override
	public boolean hasErrors() {
		return true;
	}
	
	@Override
	public @Nullable IValue getValue() {
		return null;
	}
	
	@Override
	public String[] getErrorMessages() {
		return this.messages;
	}
	
	@Override
	public @Nullable DebugException getException() {
		return null;
	}
	
}
