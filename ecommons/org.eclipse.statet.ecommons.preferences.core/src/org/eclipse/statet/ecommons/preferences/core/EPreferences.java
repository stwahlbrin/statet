/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core;

import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.util.PreferenceObjectCache;
import org.eclipse.statet.internal.ecommons.preferences.core.ECommonsPreferencesCorePlugin;


@NonNullByDefault
public final class EPreferences {
	
	
	private static final class DefaultImpl extends PreferenceObjectCache
			implements PreferenceAccess, PreferenceObjectAccess {
		
		
		private final ImList<IScopeContext> contexts;
		
		
		private DefaultImpl(final ImList<IScopeContext> contexts) {
			this.contexts= contexts;
		}
		
		
		@Override
		public PreferenceAccess getPrefs() {
			return this;
		}
		
		
		@Override
		public ImList<IScopeContext> getPreferenceContexts() {
			return this.contexts;
		}
		
		@Override
		public <T> T getPreferenceValue(final Preference<T> pref) {
			return PreferenceUtils.getPrefValue(this.contexts, pref);
		}
		
		@Override
		public void addPreferenceNodeListener(final String nodeQualifier, final IPreferenceChangeListener listener) {
			for (int i= 0; i < this.contexts.size(); i++) {
				final IScopeContext context= this.contexts.get(i);
				if (context instanceof DefaultScope) {
					continue;
				}
				final IEclipsePreferences node= context.getNode(nodeQualifier);
				if (node != null) {
					node.addPreferenceChangeListener(listener);
				}
			}
		}
		
		@Override
		public void removePreferenceNodeListener(final String nodeQualifier, final IPreferenceChangeListener listener) {
			for (int i= 0; i < this.contexts.size(); i++) {
				final IScopeContext context= this.contexts.get(i);
				if (context instanceof DefaultScope) {
					continue;
				}
				final IEclipsePreferences node= context.getNode(nodeQualifier);
				if (node != null) {
					node.removePreferenceChangeListener(listener);
				}
			}
		}
		
		@Override
		public void addPreferenceSetListener(final PreferenceSetService.ChangeListener listener,
				final ImSet<String> qualifiers) {
			getPreferenceSetService().addChangeListener(listener,
					getPreferenceContexts(), qualifiers );
		}
		
		@Override
		public void removePreferenceSetListener(final PreferenceSetService.ChangeListener listener) {
			final PreferenceSetService service= getPreferenceSetService();
			if (service != null) {
				service.removeChangeListener(listener);
			}
		}
		
	}
	
	private static class MapImpl implements PreferenceAccess {
		
		
		private final Map<Preference<?>, Object> preferencesMap;
		
		MapImpl(final Map<Preference<?>, Object> preferencesMap) {
			this.preferencesMap= preferencesMap;
		}
		
		@Override
		public ImList<IScopeContext> getPreferenceContexts() {
			return ImCollections.emptyList();
		}
		
		@Override
		@SuppressWarnings("unchecked")
		public <T> T getPreferenceValue(final Preference<T> pref) {
			return (T) this.preferencesMap.get(pref);
		}
		
		/**
		 * Not (yet) supported
		 */
		@Override
		public void addPreferenceNodeListener(final String nodeQualifier, final IPreferenceChangeListener listener) {
			throw new UnsupportedOperationException();
		}
		
		/**
		 * Not (yet) supported
		 */
		@Override
		public void removePreferenceNodeListener(final String nodeQualifier, final IPreferenceChangeListener listener) {
			throw new UnsupportedOperationException();
		}
		
		/**
		 * Not (yet) supported
		 */
		@Override
		public void addPreferenceSetListener(final PreferenceSetService.ChangeListener listener,
				final ImSet<String> qualifiers) {
			throw new UnsupportedOperationException();
		}
		
		/**
		 * Not (yet) supported
		 */
		@Override
		public void removePreferenceSetListener(final PreferenceSetService.ChangeListener listener) {
			throw new UnsupportedOperationException();
		}
		
	}
	
	
	private final static DefaultImpl DEFAULT_PREFS= new DefaultImpl(ImCollections.newList(
			DefaultScope.INSTANCE ));
	
	private final static DefaultImpl INSTANCE_PREFS= new DefaultImpl(ImCollections.newList(
			InstanceScope.INSTANCE,
			DefaultScope.INSTANCE ));
	
	
	public final static PreferenceAccess getInstancePrefs() {
		return INSTANCE_PREFS;
	}
	
	public final static PreferenceAccess getDefaultPrefs() {
		return DEFAULT_PREFS;
	}
	
	public final static PreferenceAccess getContextPrefs(final @Nullable IAdaptable adaptable) {
		if (adaptable != null) {
			final var access= adaptable.getAdapter(PreferenceAccess.class);
			if (access != null) {
				return access;
			}
		}
		return INSTANCE_PREFS;
	}
	
	
	public static PreferenceAccess createAccess(final Map<Preference<?>, Object> preferencesMap) {
		return new MapImpl(preferencesMap);
	}
	
	public static PreferenceAccess createAccess(final List<IScopeContext> contexts) {
		return new DefaultImpl(ImCollections.toList(contexts));
	}
	
	
	public static PreferenceSetService getPreferenceSetService() {
		// Adapt this if used in other context
		final ECommonsPreferencesCorePlugin plugin= ECommonsPreferencesCorePlugin.getInstance();
		if (plugin != null) {
			return plugin.getPreferenceSetService();
		}
		return null;
	}
	
	
	private EPreferences() {
	}
	
}
