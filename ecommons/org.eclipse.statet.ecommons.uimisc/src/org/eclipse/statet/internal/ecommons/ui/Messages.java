/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String CopyToClipboard_error_title;
	public static String CopyToClipboard_error_message;
	
	public static String SearchWorkspace_label;
	public static String BrowseFilesystem_label;
	public static String BrowseWorkspace_label;
	public static String BrowseFilesystem_ForFile_label;
	public static String BrowseWorkspace_ForFile_label;
	public static String BrowseFilesystem_ForDir_label;
	public static String BrowseWorkspace_ForDir_label;
	
	public static String ChooseResource_Task_description;
	public static String ChooseResource_AllFiles_name;
	public static String ResourceSelectionDialog_title;
	public static String ResourceSelectionDialog_SelectFile_message;
	public static String ResourceSelectionDialog_SelectContainer_message;
	
	public static String ContainerSelectionControl_label_EnterOrSelectFolder;
	public static String ContainerSelectionControl_label_SelectFolder;
	public static String ContainerSelectionControl_error_FolderEmpty;
	public static String ContainerSelectionControl_error_ProjectNotExists;
	public static String ContainerSelectionControl_error_PathOccupied;
	
	public static String FilterFavouredContainersAction_label;
	public static String FilterFavouredContainersAction_description;
	public static String FilterFavouredContainersAction_tooltip;
	
	public static String Hover_TooltipFocusAffordance_message;
	public static String Hover_ProposalInfoFocusAffordance_message;
	
	public static String Browser_Go_tooltip;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
