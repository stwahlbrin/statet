/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui.swt.css.dom;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.workbench.css.VirtualComposite;
import org.eclipse.statet.ecommons.ui.workbench.css.dom.VirtualStylableElement;


/**
 * Add a listener for {@link SWT#Skin} to apply styles to {@link VirtualStylableElement}s of
 * {@link VirtualComposite}s.
 */
@NonNullByDefault
@SuppressWarnings("restriction")
public class VirtualCompositeSkinListener implements Listener {
	
	
	private static boolean isEnabled;
	
	public static void enable(final Display display, final CSSEngine engine) {
		if (isEnabled) {
			return;
		}
		display.addListener(SWT.Skin, new VirtualCompositeSkinListener(display, engine));
		isEnabled= true;
	}
	
	
	private final CSSEngine engine;
	
	
	public VirtualCompositeSkinListener(final Display display, final CSSEngine cssEngine) {
		this.engine = cssEngine;
	}
	
	@Override
	public void handleEvent(final Event event) {
		if (event.widget instanceof VirtualComposite) {
			VirtualCompositeSkinListener.this.engine.applyStyles(event.widget, true);
		}
	}
	
}