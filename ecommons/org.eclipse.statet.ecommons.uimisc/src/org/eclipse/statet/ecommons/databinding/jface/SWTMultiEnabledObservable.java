/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.jface;

import java.util.List;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;


public class SWTMultiEnabledObservable extends AbstractSWTObservableValue {
	
	
	private final Control[] fControls;
	private final List<? extends Control> fExceptions;
	
	
	public SWTMultiEnabledObservable(final Realm realm, final Control[] controls, final List<? extends Control> exceptions) {
		super(realm, controls[0]);
		fControls = controls;
		fExceptions = exceptions;
	}
	
	
	@Override
	protected void doSetValue(final Object value) {
		if (value instanceof Boolean) {
			DialogUtils.setEnabled(fControls, fExceptions, ((Boolean) value).booleanValue());
		}
	}
	
	@Override
	protected Object doGetValue() {
		return null;
	}
	
	@Override
	public Object getValueType() {
		return null;
	}
	
}
