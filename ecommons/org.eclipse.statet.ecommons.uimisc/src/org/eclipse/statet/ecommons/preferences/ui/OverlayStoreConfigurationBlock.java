/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import java.util.Set;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.preferences.IWorkbenchPreferenceContainer;


public abstract class OverlayStoreConfigurationBlock extends ConfigurationBlock {
	
	
	private IPreferenceStore fOriginalStore;
	protected OverlayPreferenceStore fOverlayStore;
	private boolean fIsDirty;
	private boolean fInLoading;
	
	
	public OverlayStoreConfigurationBlock() {
		super();
	}
	
	
	@Override
	public void createContents(final Composite pageComposite, final IWorkbenchPreferenceContainer container, final IPreferenceStore preferenceStore) {
		fOriginalStore = preferenceStore;
		super.createContents(pageComposite, container, preferenceStore);
	}
	
	protected void setupOverlayStore(final OverlayStorePreference[] keys) {
		fOverlayStore = new OverlayPreferenceStore(fOriginalStore, keys);
		fOverlayStore.load();
		fOverlayStore.start();
		fOverlayStore.addPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				if (!fInLoading) {
					fIsDirty = true;
					handlePropertyChange();
				}
			}
		});
		fIsDirty = false;
	}
	
	protected abstract Set<String> getChangedGroups();
	
	protected void handlePropertyChange() {
	}
	
	@Override
	public void dispose() {
		if (fOverlayStore != null) {
			fOverlayStore.stop();
			fOverlayStore= null;
		}
		super.dispose();
	}
	
	
	@Override
	public boolean performOk(final int flags) {
		if (fOverlayStore != null && fIsDirty) {
			fOverlayStore.propagate();
			fIsDirty = false;
			scheduleChangeNotification(getChangedGroups(), ((flags & SAVE_STORE) != 0));
			return true;
		}
		return true;
	}
	
	@Override
	public void performDefaults() {
		if (fOverlayStore != null) {
			fInLoading = true;
			fOverlayStore.loadDefaults();
			fInLoading = false;
			fIsDirty = true;
			handlePropertyChange();
			updateControls();
		}
	}
	
	protected void updateControls() {
	}
	
}
