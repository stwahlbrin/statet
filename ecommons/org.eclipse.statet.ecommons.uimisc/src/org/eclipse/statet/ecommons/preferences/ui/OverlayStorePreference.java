/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import org.eclipse.statet.ecommons.preferences.core.Preference;


public class OverlayStorePreference {
	
	public static enum Type { 
			STRING, 
			BOOLEAN, 
			DOUBLE,
			FLOAT,
			LONG,
			INT;
	}
	
	
	public static OverlayStorePreference create(final Preference pref) {
		final Type type;
		if (pref instanceof Preference.BooleanPref) {
			type= Type.BOOLEAN;
		}
		else if (pref instanceof Preference.IntPref) {
			type= Type.INT;
		}
		else {
			type = Type.STRING;
		}
		return new OverlayStorePreference(pref.getKey(), type);
	}
	
	
	public final String fKey;
	public final Type fType;
	
	
	public OverlayStorePreference(final String key, final Type type) {
		fKey = key;
		fType = type;
	}
	
}
