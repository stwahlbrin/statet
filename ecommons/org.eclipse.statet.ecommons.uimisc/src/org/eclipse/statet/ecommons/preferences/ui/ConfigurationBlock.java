/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import java.util.Set;

import org.osgi.service.prefs.BackingStoreException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.preferences.IWorkbenchPreferenceContainer;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


@NonNullByDefault
public abstract class ConfigurationBlock {
	
	
	public static final int SAVE_STORE= 1 << 4;
	
	
	public static GridData applyWrapWidth(final GridData gd) {
		gd.widthHint= 300;
		return gd;
	}
	
	public static void scheduleChangeNotification(final IWorkbenchPreferenceContainer container,
			final @Nullable ImCollection<String> groupIds, final boolean directly) {
		if (groupIds != null) {
			final String source= (directly) ? null : container.toString();
			final Job job= PreferencesUtil.getSettingsChangeNotifier().getNotifyJob(source, groupIds);
			if (job == null) {
				return;
			}
			if (directly) {
				job.schedule();
			}
			else {
				container.registerUpdateJob(job);
			}
		}
	}
	
	protected class LinkSelectionListener extends SelectionAdapter {
		
		
		public LinkSelectionListener() {
		}
		
		
		@Override
		public void widgetSelected(final SelectionEvent e) {
			String pageId= e.text;
			boolean expand= false;
			if (pageId != null && pageId.endsWith("+")) {
				pageId= pageId.substring(0, pageId.length() - 1);
				expand= true;
			}
			
			final var dialog= org.eclipse.ui.dialogs.PreferencesUtil.createPreferenceDialogOn(
					getShell(), pageId, null, getData(e));
			
			if (expand) {
				final var treeViewer= dialog.getTreeViewer();
				final Object element= ((IStructuredSelection)treeViewer.getSelection()).getFirstElement();
				if (element != null) {
					treeViewer.setExpandedState(element, true);
				}
			}
		}
		
		protected @Nullable Object getData(final SelectionEvent e) {
			return null;
		}
		
	}
	
	
	private Shell shell;
	
	private final @Nullable String title;
	
	private IWorkbenchPreferenceContainer container;
	
	private boolean useProjectSettings= true;
	
	
	protected ConfigurationBlock() {
		this(null);
	}
	
	protected ConfigurationBlock(final @Nullable String title) {
		this.title= title;
	}
	
	
	public IWorkbenchPreferenceContainer getContainer() {
		return this.container;
	}
	
	public void createContents(final Composite pageComposite, final IWorkbenchPreferenceContainer container,
			final IPreferenceStore preferenceStore) {
		this.shell= pageComposite.getShell();
		this.container= container;
		createBlockArea(pageComposite);
	}
	
	protected abstract void createBlockArea(Composite pageComposite);
	
	public void dispose() {
	}
	
	protected @Nullable String getTitle() {
		return this.title;
	}
	
	/**
	 * Returns the help context for the configuration block.
	 * <p>
	 * It is used by configuration block containers to set the help context automatically.</p>
	 * 
	 * @return the help context id or <code>null</code>, if not available
	 */
	protected @Nullable String getHelpContext() {
		return null;
	}
	
	
	public abstract boolean performOk(int flags);
	
	public abstract void performDefaults();
	
	public void performCancel() {
	}
	
	public void setUseProjectSpecificSettings(final boolean enable) {
		this.useProjectSettings= enable;
	}
	
	public boolean isUseProjectSpecificSettings() {
		return this.useProjectSettings;
	}
	
	protected Shell getShell() {
		return this.shell;
	}
	
	protected void addLinkHeader(final Composite pageComposite, final String text) {
		final Link link= addLinkControl(pageComposite, text);
		link.setLayoutData(applyWrapWidth(new GridData(SWT.FILL, SWT.FILL, false, false)));
		LayoutUtils.addSmallFiller(pageComposite, false);
	}
	
	protected Link addLinkControl(final Composite composite, final String text) {
		return addLinkControl(composite, text, new LinkSelectionListener());
	}
	
	protected Link addLinkControl(final Composite composite, final String text,
			final LinkSelectionListener listener) {
		final Link link= new Link(composite, SWT.NONE);
		link.setText(text);
		link.addSelectionListener(listener);
		return link;
	}
	
	protected void scheduleChangeNotification(final Set<String> groupIds, final boolean directly) {
		scheduleChangeNotification(this.container, ImCollections.toList(groupIds), directly);
	}
	
	protected void logSaveError(final BackingStoreException e) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR,
				"org.osgi.service.prefs", 0, //$NON-NLS-1$
				"An error occurred when saving preferences to backing store.", e)); //$NON-NLS-1$
	}
	
}
