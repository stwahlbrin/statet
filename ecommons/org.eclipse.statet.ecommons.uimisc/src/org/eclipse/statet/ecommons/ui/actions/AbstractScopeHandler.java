/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isNonNull;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.menus.UIElement;
import org.eclipse.ui.services.IServiceScopes;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


/**
 * Handler limited to a specified scope.
 * 
 * @see WorkbenchScopingHandler
 */
@NonNullByDefault
public abstract class AbstractScopeHandler extends AbstractHandler {
	
	
	private Object scope;
	
	private @Nullable String commandId;
	
	private @Nullable Map<String, Object> filter;
	
	
	@SuppressWarnings("null")
	public AbstractScopeHandler(final Object scope, final @Nullable String commandId) {
		init(scope, commandId);
	}
	
	@SuppressWarnings("null")
	public AbstractScopeHandler() {
	}
	
	protected void init(final Object scope, final @Nullable String commandId) {
		if (isNonNull(this.scope)) {
			throw new IllegalStateException();
		}
		
		this.scope= scope;
		this.commandId= commandId;
		
		if (this.scope instanceof IWorkbenchWindow) {
			this.filter= Collections.singletonMap(IServiceScopes.WINDOW_SCOPE, this.scope);
		}
	}
	
	public Object getScope() {
		return this.scope;
	}
	
	public @Nullable String getCommandId() {
		return this.commandId;
	}
	
	
	@Override
	public final void setEnabled(final @Nullable Object appContext) {
		if (appContext instanceof IEvaluationContext) {
			setEnabled((IEvaluationContext)appContext);
		}
	}
	
	public void setEnabled(final IEvaluationContext context) {
	}
	
	protected void refreshElements() {
		final String commandId= this.commandId;
		if (commandId != null) {
			WorkbenchUIUtils.refreshCommandElements(commandId, this, this.filter);
		}
	}
	
	public void updateElement(final UIElement element, final Map parameters) {
	}
	
	@Override
	public final @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final Object appContext= event.getApplicationContext();
		if (appContext instanceof IEvaluationContext) {
			return execute(event, (IEvaluationContext) appContext);
		}
		return null;
	}
	
	public abstract @Nullable Object execute(ExecutionEvent event, IEvaluationContext evalContext)
			throws ExecutionException;
	
}
