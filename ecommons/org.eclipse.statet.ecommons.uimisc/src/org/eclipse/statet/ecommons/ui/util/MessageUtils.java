/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.action.LegacyActionTools;
import org.eclipse.osgi.util.TextProcessor;
import org.eclipse.swt.SWT;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class MessageUtils {
	
	
	public static final Pattern AMPERSAND_PATTERN= Pattern.compile("\\&"); //$NON-NLS-1$
	public static final String AMPERSAND_TOOLTIP_REPLACEMENT= Matcher.quoteReplacement("&&"); //$NON-NLS-1$
	
	public static String removeMnemonics(final String label) {
		return LegacyActionTools.removeMnemonics(label);
	}
	
	
	public static String escapeForTooltip(final CharSequence text) {
		return AMPERSAND_PATTERN.matcher(text).replaceAll(AMPERSAND_TOOLTIP_REPLACEMENT);
	}
	
	public static String escapeForMenu(final String text) {
		final int length= text.length();
		StringBuilder escaped= null;
		int idxWritten= 0;
		ITERATE_CHARS : for (int idx= 0; idx < length; ) {
			final char c= text.charAt(idx);
			final String replacement;
			switch (c) {
			
			case '&':
				replacement= "&&"; //$NON-NLS-1$
				break;
			case '\t':
			case '\n':
				replacement= "  "; //$NON-NLS-1$
				break;
			
			default:
				idx++;
				continue ITERATE_CHARS;
			}
			
			if (escaped == null) {
				escaped= new StringBuilder(length + 16);
			}
			if (idx > idxWritten) {
				escaped.append(text, idxWritten, idx);
			}
			escaped.append(replacement);
			idxWritten= ++idx;
			continue ITERATE_CHARS;
		}
		
		if (escaped == null) {
			return text;
		}
		if (length > idxWritten) {
			escaped.append(text, idxWritten, length);
		}
		return escaped.toString();
	}
	
	public static String escapeForFormText(final String text) {
		final int length= text.length();
		StringBuilder escaped= null;
		int idxWritten= 0;
		ITERATE_CHARS : for (int idx= 0; idx < length; ) {
			final char c= text.charAt(idx);
			final String replacement;
			switch (c) {
			
			case '<':
				replacement= "&lt;"; //$NON-NLS-1$
				break;
			case '>':
				replacement= "&gt;"; //$NON-NLS-1$
				break;
			case '&':
				replacement= "&amp;"; //$NON-NLS-1$
				break;
			case '"':
				replacement= "&quot;"; //$NON-NLS-1$
				break;
			case '\'':
				replacement= "&apos;"; //$NON-NLS-1$
				break;
			
			default:
				idx++;
				continue ITERATE_CHARS;
			}
			
			if (escaped == null) {
				escaped= new StringBuilder(length + 16);
			}
			if (idx > idxWritten) {
				escaped.append(text, idxWritten, idx);
			}
			escaped.append(replacement);
			idxWritten= ++idx;
			continue ITERATE_CHARS;
		}
		
		if (escaped == null) {
			return text;
		}
		if (length > idxWritten) {
			escaped.append(text, idxWritten, length);
		}
		return escaped.toString();
	}
	
	
	private static final String URL_DELIMITERS= TextProcessor.getDefaultDelimiters() + ":@?-"; //$NON-NLS-1$
	
	public static String processURLPart(final String url) {
		return TextProcessor.process(url, URL_DELIMITERS);
	}
	
	public static String processPath(final String path) {
		return TextProcessor.process(path);
	}
	
	private static final String FILE_PATTERN_DELIMITERS= TextProcessor.getDefaultDelimiters() + "*.?"; //$NON-NLS-1$
	
	/**
	 * Returns the label for a file pattern like '*.java'
	 *
	 * @param name the pattern
	 * @return the label of the pattern.
	 */
	public static String processPathPattern(final String name) {
		return TextProcessor.process(name, FILE_PATTERN_DELIMITERS);
	}
	
	
	/**
	 * Returns the modifier string for the given SWT modifier
	 * modifier bits.
	 *
	 * @param stateMask	the SWT modifier bits
	 * @return the modifier string
	 */
	public static String getModifierString(final int stateMask) {
		if (stateMask != 0) {
			final StringBuilder sb= new StringBuilder();
			if ((stateMask & SWT.CTRL) == SWT.CTRL) {
				sb.append(LegacyActionTools.findModifierString(SWT.CTRL));
				sb.append(" + "); //$NON-NLS-1$
			}
			if ((stateMask & SWT.ALT) == SWT.ALT) {
				sb.append(LegacyActionTools.findModifierString(SWT.ALT));
				sb.append(" + "); //$NON-NLS-1$
			}
			if ((stateMask & SWT.SHIFT) == SWT.SHIFT) {
				sb.append(LegacyActionTools.findModifierString(SWT.SHIFT));
				sb.append(" + "); //$NON-NLS-1$
			}
			if ((stateMask & SWT.COMMAND) == SWT.COMMAND) {
				sb.append(LegacyActionTools.findModifierString(SWT.COMMAND));
				sb.append(" + "); //$NON-NLS-1$
			}
			if (sb.length() > 0) {
				return sb.substring(0, sb.length()-3);
			}
		}
		return ""; //$NON-NLS-1$
	}
	
	
	private MessageUtils() {}
	
}
