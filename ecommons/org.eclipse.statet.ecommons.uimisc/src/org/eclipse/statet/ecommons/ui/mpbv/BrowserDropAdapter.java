/*=============================================================================#
 # Copyright (c) 2005, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.platform: Initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import java.nio.file.Path;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.FileTransfer;


public class BrowserDropAdapter implements DropTargetListener {
	
	
	/**
	 * The view to which this drop support has been added.
	 */
	private final Browser browser;
	
	
	protected BrowserDropAdapter(final Browser view) {
		this.browser= view;
	}
	
	
	private void validate(final DropTargetEvent event) {
		if (FileTransfer.getInstance().isSupportedType(event.currentDataType)) {
			if ((event.operations & DND.DROP_LINK) == DND.DROP_LINK) {
				event.detail= DND.DROP_LINK;
				event.feedback= DND.FEEDBACK_SELECT;
				return;
			}
		}
		event.detail= DND.DROP_NONE;
	}
	
	@Override
	public void dragEnter(final DropTargetEvent event) {
		validate(event);
	}
	
	@Override
	public void dragOperationChanged(final DropTargetEvent event) {
		validate(event);
	}
	
	@Override
	public void dragOver(final DropTargetEvent event) {
	}
	
	@Override
	public void dragLeave(final DropTargetEvent event) {
	}
	
	@Override
	public void dropAccept(final DropTargetEvent event) {
	}
	
	@Override
	public void drop(final DropTargetEvent event) {
		final String[] urls= (String[])event.data;
		final String url0= (urls != null && urls.length > 0) ? urls[0] : null;
		if (url0 == null) {
			return;
		}
		try {
			final Path path= Path.of(url0);
			if (path.isAbsolute()) {
				this.browser.setUrl(path.toUri().toString());
			}
		}
		catch (final Exception e) {}
	}
	
	
}
