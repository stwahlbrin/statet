/*=============================================================================#
 # Copyright (c) 2008, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers.breadcrumb;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.OpenEvent;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.css.StylingUtils;


/**
 * A breadcrumb viewer shows a the parent chain of its input element in a list. Each breadcrumb item
 * of that list can be expanded and a sibling of the element presented by the breadcrumb item can be
 * selected.
 * <p>
 * Content providers for breadcrumb viewers must implement the <code>ITreeContentProvider</code>
 * interface.</p>
 * <p>
 * Label providers for breadcrumb viewers must implement the <code>ILabelProvider</code> interface.
 * </p>
 */
@NonNullByDefault
public abstract class BreadcrumbViewer extends StructuredViewer {
	
	
	private static final boolean IS_GTK= "gtk".equals(SWT.getPlatform()); //$NON-NLS-1$
	
	
	private final ArrayList<BreadcrumbItem> breadcrumbItems= new ArrayList<>();
	
	private final CopyOnWriteIdentityListSet<MenuDetectListener> menuListeners= new CopyOnWriteIdentityListSet<>();
	
	private final Composite container;
	
	private @Nullable BreadcrumbItem selectedItem;
	
	private @Nullable ILabelProvider toolTipLabelProvider;
	
	private @Nullable Predicate<Object> canOpen;
	
	
	/**
	 * Create a new <code>BreadcrumbViewer</code>.
	 * <p>
	 * Style is one of:
	 * <ul>
	 * <li>SWT.NONE</li>
	 * <li>SWT.VERTICAL</li>
	 * <li>SWT.HORIZONTAL</li>
	 * </ul>
	 * 
	 * @param parent the container for the viewer
	 * @param style the style flag used for this viewer
	 */
	public BreadcrumbViewer(final Composite parent, final int style) {
		this.container= new Composite(parent, SWT.NONE);
		this.container.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		
		this.container.setBackgroundMode(SWT.INHERIT_DEFAULT);
		this.container.setData(StylingUtils.WIDGET_CSS_ID_KEY, "BreadcrumbComposite"); //$NON-NLS-1$
		
		this.container.addTraverseListener(new TraverseListener() {
			@Override
			public void keyTraversed(final TraverseEvent e) {
				e.doit= true;
			}
		});
		hookControl(this.container);
		
		int columns= 1000;
		if ((SWT.VERTICAL & style) != 0) {
			columns= 1;
		}
		final GridLayout gridLayout= new GridLayout(columns, false);
		gridLayout.marginWidth= 0;
		gridLayout.marginHeight= 0;
		gridLayout.verticalSpacing= 0;
		gridLayout.horizontalSpacing= 0;
		this.container.setLayout(gridLayout);
		
		this.container.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				refresh();
			}
		});
	}
	
	@Override
	protected void handleDispose(final DisposeEvent event) {
		{	final var toolTipLabelProvider= this.toolTipLabelProvider;
			if (toolTipLabelProvider != null) {
				this.toolTipLabelProvider= null;
				toolTipLabelProvider.dispose();
			}
		}
		
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			indexItem.dispose();
		}
		this.breadcrumbItems.clear();
		
		super.handleDispose(event);
	}
	
	
	/**
	 * The tool tip to use for the tool tip labels. <code>null</code> if the viewers label provider
	 * should be used.
	 * 
	 * @param toolTipLabelProvider the label provider for the tool tips or <code>null</code>
	 */
	public void setToolTipLabelProvider(final @Nullable ILabelProvider toolTipLabelProvider) {
		this.toolTipLabelProvider= toolTipLabelProvider;
	}
	
	public void setCanOpen(final @Nullable Predicate<Object> predicate) {
		this.canOpen= predicate;
	}
	
	
	@Override
	public Control getControl() {
		return this.container;
	}
	
	
	@Override
	protected @Nullable Object getRoot() {
		if (this.breadcrumbItems.isEmpty()) {
			return null;
		}
		
		return (this.breadcrumbItems.get(0)).getElement();
	}
	
	@Override
	public void reveal(final Object element) {
		//all elements are always visible
	}
	
	/**
	 * Transfers the keyboard focus into the viewer.
	 */
	public void setFocus() {
		this.container.setFocus();
		
		BreadcrumbItem item= this.selectedItem;
		if (item != null) {
			item.setFocus(true);
			return;
		}
		
		if (this.breadcrumbItems.isEmpty()) {
			return;
		}
		
		item= this.breadcrumbItems.get(this.breadcrumbItems.size() - 1);
		if (item.getData() == null) {
			if (this.breadcrumbItems.size() < 2) {
				return;
			}
			
			item= this.breadcrumbItems.get(this.breadcrumbItems.size() - 2);
		}
		item.setFocus(true);
	}
	
	/**
	 * @return true if any of the items in the viewer is expanded
	 */
	public boolean isDropDownOpen() {
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			if (indexItem.isMenuShown()) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * The shell used for the shown drop down or <code>null</code>
	 * if no drop down is shown at the moment.
	 * 
	 * @return the drop downs shell or <code>null</code>
	 */
	public @Nullable Shell getDropDownShell() {
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			if (indexItem.isMenuShown()) {
				return indexItem.getDropDownShell();
			}
		}
		
		return null;
	}
	
	/**
	 * Returns the selection provider which provides the selection of the drop down currently opened
	 * or <code>null</code> if no drop down is open at the moment.
	 * 
	 * @return the selection provider of the open drop down or <code>null</code>
	 */
	public @Nullable ISelectionProvider getDropDownSelectionProvider() {
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			if (indexItem.isMenuShown()) {
				return indexItem.getDropDownSelectionProvider();
			}
		}
		
		return null;
	}
	
	/**
	 * Add the given listener to the set of listeners which will be informed
	 * when a context menu is requested for a breadcrumb item.
	 * 
	 * @param listener the listener to add
	 */
	public void addMenuDetectListener(final MenuDetectListener listener) {
		this.menuListeners.add(listener);
	}
	
	/**
	 * Remove the given listener from the set of menu detect listeners.
	 * Does nothing if the listener is not element of the set.
	 * 
	 * @param listener the listener to remove
	 */
	public void removeMenuDetectListener(final MenuDetectListener listener) {
		this.menuListeners.remove(listener);
	}
	
	@Override
	protected void assertContentProviderType(final IContentProvider provider) {
		super.assertContentProviderType(provider);
		Assert.isTrue(provider instanceof ITreeContentProvider);
	}
	
	@Override
	protected void inputChanged(final @Nullable Object input, final @Nullable Object oldInput) {
		if (this.container.isDisposed()) {
			return;
		}
		
		disableRedraw();
		try {
			if (this.breadcrumbItems.size() > 0) {
				final BreadcrumbItem last= this.breadcrumbItems.get(this.breadcrumbItems.size() - 1);
				last.setIsLastItem(false);
			}
			
			final int lastIndex= buildItemChain(input);
			
			if (lastIndex > 0) {
				final BreadcrumbItem last= this.breadcrumbItems.get(lastIndex - 1);
				last.setIsLastItem(true);
			}
			
			while (lastIndex < this.breadcrumbItems.size()) {
				final BreadcrumbItem item= this.breadcrumbItems.remove(this.breadcrumbItems.size() - 1);
				if (item == this.selectedItem) {
					selectItem(null);
				}
				if (item.getData() != null) {
					unmapElement(item.getData());
				}
				item.dispose();
			}
			
			updateSize();
			this.container.layout(true, true);
		}
		finally {
			enableRedraw();
		}
	}
	
	@Override
	protected @Nullable Widget doFindInputItem(final @Nullable Object element) {
		if (element == null) {
			return null;
		}
		
		if (element == getInput() || element.equals(getInput())) {
			return doFindItem(element);
		}
		
		return null;
	}
	
	@Override
	protected @Nullable Widget doFindItem(final @Nullable Object element) {
		if (element == null) {
			return null;
		}
		
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			if (element.equals(indexItem.getData())) {
				return indexItem;
			}
		}
		
		return null;
	}
	
	@Override
	protected void doUpdateItem(final Widget widget, final Object element, final boolean fullMap) {
		if (widget instanceof BreadcrumbItem) {
			final BreadcrumbItem item= (BreadcrumbItem)widget;
			
			// remember element we are showing
			if (fullMap) {
				associate(element, item);
			}
			else {
				final Object data= item.getData();
				if (data != null) {
					unmapElement(data, item);
				}
				item.setData(element);
				mapElement(element, item);
			}
			
			final BreadcrumbViewerRow row= new BreadcrumbViewerRow(this, item);
			final ViewerCell cell= nonNullAssert(row.getCell(0));
			
			((CellLabelProvider)getLabelProvider()).update(cell);
			
			item.refreshArrow();
			
			if (this.toolTipLabelProvider != null) {
				item.setToolTip(this.toolTipLabelProvider.getText(item.getElement()));
			}
			else {
				item.setToolTip(cell.getText());
			}
		}
	}
	
	@Override
	protected ImList<@NonNull Object> getSelectionFromWidget() {
		final var selectedItem= this.selectedItem;
		if (selectedItem == null || selectedItem.getData() == null) {
			return ImCollections.emptyList();
		}
		
		return ImCollections.newList(selectedItem.getElement());
	}
	
	@Override
	protected void internalRefresh(final @Nullable Object element) {
		disableRedraw();
		try {
			final BreadcrumbItem item= (BreadcrumbItem)doFindItem(element);
			if (item == null) {
				final int size= this.breadcrumbItems.size();
				for (int index= 0; index < size; index++) {
					final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
					indexItem.refresh();
				}
			}
			else {
				item.refresh();
			}
			if (updateSize()) {
				this.container.layout(true, true);
			}
		}
		finally {
			enableRedraw();
		}
	}
	
	@Override
	protected void setSelectionToWidget(@SuppressWarnings("rawtypes") final @Nullable List l,
			final boolean reveal) {
		BreadcrumbItem focusItem= null;
		
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			if (indexItem.hasFocus()) {
				focusItem= indexItem;
			}
			
			indexItem.setSelected(false);
		}
		
		if (l == null) {
			return;
		}
		
		for (final Object element : l) {
			final BreadcrumbItem item= (BreadcrumbItem)doFindItem(element);
			if (item != null) {
				item.setSelected(true);
				this.selectedItem= item;
				if (item == focusItem) {
					item.setFocus(true);
				}
			}
		}
	}
	
	/**
	 * Set a single selection to the given item. <code>null</code> to deselect all.
	 * 
	 * @param item the item to select or <code>null</code>
	 */
	void selectItem(final @Nullable BreadcrumbItem item) {
		final var oldItem= this.selectedItem;
		if (oldItem != null) {
			oldItem.setSelected(false);
		}
		
		this.selectedItem= item;
		setSelectionToWidget(getSelection(), false);
		
		if (item != null) {
			setFocus();
		}
		else {
			final int size= this.breadcrumbItems.size();
			for (int index= 0; index < size; index++) {
				final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
				indexItem.setFocus(false);
			}
		}
		
		fireSelectionChanged(new SelectionChangedEvent(this, getSelection()));
	}
	
	/**
	 * Returns the item count.
	 * 
	 * @return number of items shown in the viewer
	 */
	int getItemCount() {
		return this.breadcrumbItems.size();
	}
	
	/**
	 * Returns the item for the given item index.
	 * 
	 * @param index the index of the item
	 * @return the item ad the given <code>index</code>
	 */
	BreadcrumbItem getItem(final int index) {
		return this.breadcrumbItems.get(index);
	}
	
	/**
	 * Returns the index of the given item.
	 *
	 * @param item the item to search
	 * @return the index of the item or -1 if not found
	 */
	int getIndexOfItem(final BreadcrumbItem item) {
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			if (indexItem == item) {
				return index;
			}
		}
		
		return -1;
	}
	
	/**
	 * Notifies all double click listeners.
	 */
	void fireDoubleClick() {
		fireDoubleClick(new DoubleClickEvent(this, getSelection()));
	}
	
	boolean canOpen(final Object element) {
		final var predicate= this.canOpen;
		return (predicate == null || predicate.test(element));
	}
	
	/**
	 * Notifies all open listeners.
	 */
	void fireOpen() {
		fireOpen(new OpenEvent(this, getSelection()));
	}
	
	/**
	 * The given element was selected from a drop down menu.
	 * 
	 * @param element the selected element
	 */
	void fireMenuSelection(final Object element) {
		fireOpen(new OpenEvent(this, new StructuredSelection(element)));
	}
	
	/**
	 * A context menu has been requested for the selected breadcrumb item.
	 * 
	 * @param event the event issued the menu detection
	 */
	void fireMenuDetect(final MenuDetectEvent event) {
		for (final var listener : this.menuListeners) {
			listener.menuDetected(event);
		}
	}
	
	/**
	 * Set selection to the next or previous element if possible.
	 * 
	 * @param next <code>true</code> if the next element should be selected, otherwise the previous
	 *     one will be selected
	 */
	void doTraverse(final boolean next) {
		if (this.selectedItem == null) {
			return;
		}
		
		final int index= this.breadcrumbItems.indexOf(this.selectedItem);
		if (next) {
			if (index == this.breadcrumbItems.size() - 1) {
				final var item= this.breadcrumbItems.get(index);
				
				final ITreeContentProvider contentProvider= (ITreeContentProvider)getContentProvider();
				if (!contentProvider.hasChildren(item.getElement())) {
					return;
				}
				
				item.openDropDownMenu();
			}
			else {
				final BreadcrumbItem nextItem= this.breadcrumbItems.get(index + 1);
				selectItem(nextItem);
				return;
			}
		}
		else {
			if (index == 1) {
				final var item= this.breadcrumbItems.get(0);
				
				item.openDropDownMenu();
			}
			else {
				selectItem(this.breadcrumbItems.get(index - 1));
				return;
			}
		}
	}
	
	
	/**
	 * Generates the parent chain of the given element.
	 * 
	 * @param element element to build the parent chain for
	 * @return the first index of an item in fBreadcrumbItems which is not part of the chain
	 */
	private int buildItemChain(final @Nullable Object element) {
		if (element == null) {
			return 0;
		}
		
		final ITreeContentProvider contentProvider= (ITreeContentProvider)getContentProvider();
		final Object parent= contentProvider.getParent(element);
		
		final int index= buildItemChain(parent);
		
		BreadcrumbItem item;
		if (index < this.breadcrumbItems.size()) {
			item= this.breadcrumbItems.get(index);
			if (item.getData() != null) {
				unmapElement(item.getData());
			}
		}
		else {
			item= createItem();
			this.breadcrumbItems.add(item);
		}
		
		if (equals(element, item.getData())) {
			update(element, null);
		}
		else {
			item.setData(element);
			item.refresh();
		}
		if (parent == null) {
			//don't show the models root
			item.setDetailsVisible(false);
		}
		
		mapElement(element, item);
		
		return index + 1;
	}
	
	/**
	 * Creates and returns a new instance of a breadcrumb item.
	 * 
	 * @return new instance of a breadcrumb item
	 */
	private BreadcrumbItem createItem() {
		final BreadcrumbItem result= new BreadcrumbItem(this, this.container);
		
		result.setLabelProvider((ILabelProvider)getLabelProvider());
		if (this.toolTipLabelProvider != null) {
			result.setToolTipLabelProvider(this.toolTipLabelProvider);
		}
		else {
			result.setToolTipLabelProvider((ILabelProvider)getLabelProvider());
		}
		result.setContentProvider((ITreeContentProvider)getContentProvider());
		
		return result;
	}
	
	/**
	 * Update the size of the items such that all items are visible, if possible.
	 * 
	 * @return <code>true</code> if any item has changed, <code>false</code> otherwise
	 */
	private boolean updateSize() {
		final int width= this.container.getClientArea().width;
		
		int currentWidth= getCurrentWidth();
		
		boolean requiresLayout= false;
		
		if (currentWidth > width) {
			int index= 0;
			while (currentWidth > width && index < this.breadcrumbItems.size() - 1) {
				final BreadcrumbItem item= this.breadcrumbItems.get(index);
				if (item.isShowText()) {
					item.setShowText(false);
					currentWidth= getCurrentWidth();
					requiresLayout= true;
				}
				
				index++;
			}
		}
		else if (currentWidth < width) {
			int index= this.breadcrumbItems.size() - 1;
			while (currentWidth < width && index >= 0) {
				final BreadcrumbItem item= this.breadcrumbItems.get(index);
				if (!item.isShowText()) {
					item.setShowText(true);
					currentWidth= getCurrentWidth();
					if (currentWidth > width) {
						item.setShowText(false);
						index= 0;
					}
					else {
						requiresLayout= true;
					}
				}
				
				index--;
			}
		}
		
		return requiresLayout;
	}
	
	/**
	 * Returns the current width of all items in the list.
	 * 
	 * @return the width of all items in the list
	 */
	private int getCurrentWidth() {
		int result= 0;
		final int size= this.breadcrumbItems.size();
		for (int index= 0; index < size; index++) {
			final BreadcrumbItem indexItem= this.breadcrumbItems.get(index);
			result+= indexItem.getWidth();
		}
		return result;
	}
	
	/**
	 * Enables redrawing of the breadcrumb.
	 */
	private void enableRedraw() {
		if (IS_GTK) {
			return;
		}
		
		this.container.setRedraw(true);
	}
	
	/**
	 * Disables redrawing of the breadcrumb.
	 * 
	 * <p>
	 * <strong>A call to this method must be followed by a call to {@link #enableRedraw()}</strong>
	 * </p>
	 */
	private void disableRedraw() {
		if (IS_GTK) {
			return;
		}
		
		this.container.setRedraw(false);
	}
	
	
	/**
	 * Configure the given drop down viewer. The given input is used for the viewers input. Clients
	 * must at least set the label and the content provider for the viewer.
	 * 
	 * @param viewer the viewer to configure
	 * @param input the input for the viewer
	 */
	protected abstract void configureDropDownViewer(TreeViewer viewer, Object input);
	
	public void fillDropDownContextMenu(final IMenuManager manager, final Object selection) {
	}
	
}
