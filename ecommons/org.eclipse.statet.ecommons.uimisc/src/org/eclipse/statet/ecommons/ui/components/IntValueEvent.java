/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import org.eclipse.swt.events.TypedEvent;
import org.eclipse.swt.widgets.Control;


public class IntValueEvent extends TypedEvent {
	
	private static final long serialVersionUID = 7509242153124380586L;
	
	
	public final int valueIdx;
	
	public final int oldValue;
	
	public int newValue;
	
	
	IntValueEvent(final Control source, final int time, final int idx, final int oldValue, final int newValue) {
		super(source);
		
		display = source.getDisplay();
		widget = source;
		this.time = time;
		
		valueIdx = idx;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
	
	
}
