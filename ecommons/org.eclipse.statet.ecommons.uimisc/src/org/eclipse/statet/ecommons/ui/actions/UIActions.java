/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.actions;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class UIActions {
	
	
	public static final String OPEN_GROUP_ID=               "open"; //$NON-NLS-1$
	public static final String EDIT_GROUP_ID=               "edit"; //$NON-NLS-1$
	public static final String ADDITIONS_GROUP_ID=          "additions"; //$NON-NLS-1$
	public static final String SETTINGS_GROUP_ID=           "settings"; //$NON-NLS-1$
	
	public static final String VIEW_GROUP_ID=               "view"; //$NON-NLS-1$
	public static final String VIEW_EXPAND_GROUP_ID=        "view.expand"; //$NON-NLS-1$
	public static final String VIEW_SORT_GROUP_ID=          "view.sort"; //$NON-NLS-1$
	public static final String VIEW_FILTER_GROUP_ID=        "view.filter"; //$NON-NLS-1$
	
	
	public static final String NO_COMMAND_ID=               "NO_COMMAND"; //$NON-NLS-1$
	
	
	private UIActions() {}
	
}
