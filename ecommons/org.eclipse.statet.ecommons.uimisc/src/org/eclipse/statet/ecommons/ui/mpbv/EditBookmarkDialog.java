/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


class EditBookmarkDialog extends Dialog {
	
	
	private Text nameControl;
	private Text urlControl;
	
	private BrowserBookmark bookmark;
	
	
	public EditBookmarkDialog(final Shell parentShell, final BrowserBookmark bookmark) {
		super(parentShell);
		
		this.bookmark= bookmark;
		create();
	}
	
	
	@Override
	protected void configureShell(final Shell shell) {
		shell.setText("Edit Bookmark");
		
		super.configureShell(shell);
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newDialogGrid(2));
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			label.setText("&Name:");
		}
		{	final Text text= new Text(composite, SWT.BORDER);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, true, false);
			gd.widthHint= LayoutUtils.hintWidth(text, 80);
			text.setLayoutData(gd);
			this.nameControl= text;
		}
		{	final Label label= new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			label.setText("&URL:");
		}
		{	final Text text= new Text(composite, SWT.BORDER | SWT.LEFT);
			text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.urlControl= text;
		}
		
		if (this.bookmark != null) {
			this.nameControl.setText(this.bookmark.getLabel());
			this.urlControl.setText(this.bookmark.getUrl());
		}
		
		applyDialogFont(composite);
		return composite;
	}
	
	
	@Override
	protected void okPressed() {
		this.bookmark= new BrowserBookmark(this.nameControl.getText(), this.urlControl.getText());
		
		super.okPressed();
	}
	
	public BrowserBookmark getBookmark() {
		return this.bookmark;
	}
	
}
