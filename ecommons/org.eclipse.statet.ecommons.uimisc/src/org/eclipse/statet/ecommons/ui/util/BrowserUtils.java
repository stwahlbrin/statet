/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.swt.browser.Browser;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.internal.ecommons.ui.UIMiscellanyPlugin;


@NonNullByDefault
public class BrowserUtils {
	
	
	private static void appendEscapedJavascriptString(final StringBuilder sb, final String s) {
		for (int i= 0; i < s.length(); i++) {
			final char c= s.charAt(i);
			switch (c) {
			case '\\':
			case '\"':
			case '\'':
				sb.append('\\');
				sb.append(c);
				continue;
			default:
				sb.append(c);
				continue;
			}
		}
	}
	
	public static @Nullable String getSelectedText(final Browser browser) {
		final Object value= browser.evaluate(
				"if (window.getSelection) {" + //$NON-NLS-1$
					"var sel= window.getSelection();" + //$NON-NLS-1$
					"if (sel.rangeCount && sel.getRangeAt) {" + //$NON-NLS-1$
						"return sel.getRangeAt(0).toString();" + //$NON-NLS-1$
					"}" + //$NON-NLS-1$
					"return sel;" + //$NON-NLS-1$
				"}" + //$NON-NLS-1$
				"else if (document.getSelection) {" + //$NON-NLS-1$
					"return document.getSelection();" + //$NON-NLS-1$
				"}" + //$NON-NLS-1$
				"else if (document.selection) {" + //$NON-NLS-1$
					"return document.selection.createRange().text;" + //$NON-NLS-1$
				"}" + //$NON-NLS-1$
				"else {" + //$NON-NLS-1$
					"return '';" + //$NON-NLS-1$
				"}"); //$NON-NLS-1$
		if (value instanceof String) {
			return (String) value;
		}
		return null;
	}
	
	public static boolean searchText(final Browser browser, final String text,
			final boolean forward, final boolean caseSensitive, final boolean wrap) {
		final StringBuilder script= new StringBuilder(50);
		script.append("return window.find(\""); //$NON-NLS-1$
		appendEscapedJavascriptString(script, text);
		script.append("\","); //$NON-NLS-1$
		script.append(caseSensitive);
		script.append(',');
		script.append(!forward); // upward
		script.append(',');
		script.append(wrap); // wrap
		script.append(",false,true)"); // wholeWord, inFrames //$NON-NLS-1$
						// inFrames fixes wrap in some situations
		final Object found= browser.evaluate(script.toString());
		return Boolean.TRUE.equals(found);
	}
	
	
	private static @Nullable InputHistory<String> addressInputHistory;
	
	public static InputHistory<String> getDefaultAddressInputHistory() {
		InputHistory<String> history= addressInputHistory;
		if (history == null) {
			final InputHistoryController<String> controller= new InputHistoryController.ForString(
					DialogUtils.getDialogSettings(UIMiscellanyPlugin.getInstance(), "browser"), //$NON-NLS-1$
					"AddressInput.history" ); //$NON-NLS-1$
			addressInputHistory= history= controller.getList();
		}
		return history;
	}
	
	
	private BrowserUtils() {}
	
}
