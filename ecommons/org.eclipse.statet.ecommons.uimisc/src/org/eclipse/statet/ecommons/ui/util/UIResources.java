/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class UIResources {
	
	
	private final ImageRegistry registry;
	
	
	public UIResources(final ImageRegistry imageRegistry) {
		this.registry= imageRegistry;
	}
	
	
	public ImageDescriptor getImageDescriptor(final String id) {
		final var imageDescriptor= this.registry.getDescriptor(id);
		if (imageDescriptor == null) {
			throw new NullPointerException(String.format("Image description missing for '%1$s'."));
		}
		return imageDescriptor;
	}
	
	public Image getImage(final String id) {
		final var image= this.registry.get(id);
		if (image == null) {
			throw new NullPointerException(String.format("Image description missing for '%1$s'."));
		}
		return image;
	}
	
}
