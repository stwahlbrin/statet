/*=============================================================================#
 # Copyright (c) 2008, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers.breadcrumb;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.css.StylingUtils;


/**
 * An item in a breadcrumb viewer.
 * <p>
 * The item shows a label and an image. It also has the ability to expand, that is to open a drop
 * down menu.</p>
 * <p>
 * The drop down allows to select any child of the items input element. The item shows the label and
 * icon of its data element, if any.</p>
 */
@NonNullByDefault
class BreadcrumbItem extends Item {
	
	
	private ITreeContentProvider contentProvider;
	
	private ILabelProvider labelProvider;
	private ILabelProvider toolTipLabelProvider;
	
	private final BreadcrumbViewer parent;
	private final Composite container;
	
	private final BreadcrumbItemDetails detailsBlock;
	private final BreadcrumbItemDropDown expandBlock;
	private boolean isLast;
	
	
	/**
	 * A new breadcrumb item which is shown inside the given viewer.
	 * 
	 * @param viewer the items viewer
	 * @param parent the container containing the item
	 */
	public BreadcrumbItem(final BreadcrumbViewer viewer, final Composite parent) {
		super(parent, SWT.NONE);
		
		this.parent= viewer;
		
		this.container= new Composite(parent, SWT.NONE);
		this.container.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		final GridLayout layout= new GridLayout(2, false);
		layout.marginBottom= 1;
		layout.marginHeight= 0;
		layout.marginWidth= 0;
		layout.horizontalSpacing= 0;
		this.container.setLayout(layout);
		
		this.container.setData(StylingUtils.WIDGET_CSS_ID_KEY, "BreadcrumbItemComposite"); //$NON-NLS-1$
		
		this.detailsBlock= new BreadcrumbItemDetails(this, this.container);
		
		this.expandBlock= new BreadcrumbItemDropDown(this, this.container);
	}
	
	
	@SuppressWarnings("null")
	public Object getElement() {
		return super.getData();
	}
	
	
	/**
	 * Returns this items viewer.
	 * 
	 * @return the viewer showing this item
	 */
	public BreadcrumbViewer getViewer() {
		return this.parent;
	}
	
	/**
	 * Sets the content provider of this item.
	 * 
	 * @param contentProvider the content provider to use
	 */
	public void setContentProvider(final ITreeContentProvider contentProvider) {
		this.contentProvider= contentProvider;
	}
	
	/**
	 * Sets the label provider of this item.
	 * 
	 * @param labelProvider the label provider to use
	 */
	public void setLabelProvider(final ILabelProvider labelProvider) {
		this.labelProvider= labelProvider;
	}
	
	/**
	 * Sets the the label provider for the tool tips of this item.
	 * 
	 * @param toolTipLabelProvider the label provider for the tool tips
	 */
	public void setToolTipLabelProvider(final ILabelProvider toolTipLabelProvider) {
		this.toolTipLabelProvider= toolTipLabelProvider;
	}
	
	@Override
	public void dispose() {
		this.container.dispose();
		super.dispose();
	}
	
	/**
	 * Should this item show a text label.
	 * 
	 * @param enabled true if it should
	 */
	void setShowText(final boolean enabled) {
		this.detailsBlock.setTextVisible(enabled);
	}
	
	/**
	 * Does this item show a text label?
	 * 
	 * @return true if it does.
	 */
	boolean isShowText() {
		return this.detailsBlock.isTextVisible();
	}
	
	/**
	 * Returns the width of this item.
	 * 
	 * @return the width of this item
	 */
	int getWidth() {
		return this.detailsBlock.getWidth() + this.expandBlock.getWidth() + 2;
	}
	
	/**
	 * Sets whether this item has to be marked as
	 * selected or not.
	 * 
	 * @param selected true if marked as selected
	 */
	void setSelected(final boolean selected) {
		this.detailsBlock.setSelected(selected);
	}
	
	/**
	 * Sets whether this item has the keyboard focus.
	 * 
	 * @param state <code>true</code> if it has focus, <code>false</code> otherwise
	 */
	void setFocus(final boolean state) {
		this.detailsBlock.setFocus(state);
	}
	
	/**
	 * Returns whether this item has the keyboard focus.
	 * 
	 * @return <code>true</code> if this item has the keyboard focus
	 */
	boolean hasFocus() {
		return this.detailsBlock.hasFocus();
	}
	
	/**
	 * Redraw this item, retrieves new labels from its label provider.
	 */
	void refresh() {
		final String text= this.labelProvider.getText(getElement());
		final Image image= this.labelProvider.getImage(getElement());
		final String toolTip= this.toolTipLabelProvider.getText(getElement());
		
		this.detailsBlock.setText(text);
		this.detailsBlock.setImage(image);
		this.detailsBlock.setToolTip(toolTip);
		
		refreshArrow();
	}
	
	/**
	 * Refresh the arrows visibility.
	 */
	void refreshArrow() {
		this.expandBlock.setEnabled(this.contentProvider.hasChildren(getElement()));
	}
	
	/**
	 * Set whether this is the last item in the breadcrumb item chain or not.
	 * 
	 * @param isLast <code>true</code> if this is the last item, <code>false</code> otherwise
	 */
	void setIsLastItem(final boolean isLast) {
		this.isLast= isLast;
		
		final GridData data= (GridData)this.container.getLayoutData();
		data.grabExcessHorizontalSpace= isLast;
	}
	
	/**
	 * Sets whether or not the this item should show the details (name and label).
	 * 
	 * @param visible true if the item shows details
	 */
	void setDetailsVisible(final boolean visible) {
		this.detailsBlock.setVisible(visible);
	}
	
	/**
	 * Expand this item, shows the drop down menu.
	 */
	void openDropDownMenu() {
		this.expandBlock.showMenu();
		
		final var shell= this.expandBlock.getDropDownShell();
		if (shell != null) {
			shell.setFocus();
		}
	}
	
	/**
	 * @return true if this item is expanded
	 */
	boolean isMenuShown() {
		return this.expandBlock.isMenuShown();
	}
	
	/**
	 * Returns the drop down shell.
	 * 
	 * @return the shell of the drop down if shown, <code>null</code> otherwise
	 */
	@Nullable Shell getDropDownShell() {
		return this.expandBlock.getDropDownShell();
	}
	
	/**
	 * Returns the drop down selection provider of this item.
	 * 
	 * @return the selection provider of the drop down or <code>null</code>
	 */
	@Nullable ISelectionProvider getDropDownSelectionProvider() {
		return this.expandBlock.getDropDownSelectionProvider();
	}
	
	/**
	 * Returns the bounds of this item.
	 * 
	 * @return the bounds of this item
	 */
	public Rectangle getBounds() {
		return this.container.getBounds();
	}
	
	/**
	 * Set the tool tip of the item to the given text.
	 * 
	 * @param text the tool tip for the item
	 */
	public void setToolTip(final String text) {
		this.detailsBlock.setToolTip(text);
	}
	
	@Override
	public void setText(final String string) {
		super.setText(string);
		this.detailsBlock.setText(string);
		
		//more or less space might be required for the label
		if (this.isLast) {
			this.container.layout(true, true);
		}
	}
	
	@Override
	public void setImage(final @Nullable Image image) {
		super.setImage(image);
		this.detailsBlock.setImage(image);
	}
	
}
