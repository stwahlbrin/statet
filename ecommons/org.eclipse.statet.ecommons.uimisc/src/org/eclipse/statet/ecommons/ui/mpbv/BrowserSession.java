/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import java.net.URI;

import org.eclipse.jface.resource.ImageDescriptor;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.collections.IntArrayMap;


/**
 * Session for page browser views of the type {@link PageBookBrowserView}.
 */
@NonNullByDefault
public class BrowserSession implements PageBookSession {
	
	
	public static final @Nullable BrowserSession findSessionByUrl(final ImCollection<BrowserSession> sessions,
			final String url) {
		for (final BrowserSession session : sessions) {
			if (url.equals(session.getUrl())) {
				return session;
			}
		}
		return null;
	}
	
	public static final @Nullable BrowserSession findSessionByUrl(final ImCollection<BrowserSession> sessions,
			final URI url) {
		// TODO
		return findSessionByUrl(sessions, url.toString());
	}
	
	public static final @Nullable BrowserSession findSessionById(final ImCollection<BrowserSession> sessions,
			final Object id) {
		for (final BrowserSession session : sessions) {
			if (id.equals(session.getId())) {
				return session;
			}
		}
		return null;
	}
	
	
	private boolean bound;
	
	private String url;
	
	private String title;
	private @Nullable ImageDescriptor imageDescriptor;
	
	private @Nullable IntArrayMap<String> staticContent;
	
	
	public BrowserSession(final String initialUrl) {
		this.url= initialUrl;
		this.title= ""; //$NON-NLS-1$
		this.imageDescriptor= null;
	}
	
	public BrowserSession() {
		this(""); //$NON-NLS-1$
	}
	
	
	public @Nullable Object getId() {
		return null;
	}
	
	void bind() {
		if (this.bound) {
			throw new IllegalArgumentException("Session is already bound");
		}
		this.bound= true;
	}
	
	void unbind() {
		this.bound= false;
	}
	
	public boolean isBound() {
		return this.bound;
	}
	
	void setUrl(final String url) {
		this.url= url;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public String getViewUrl() {
		if (this.url.equals(PageBookBrowserPage.ABOUT_BLANK_URI_STRING)) {
			return ""; //$NON-NLS-1$
		}
		return this.url;
	}
	
	void setTitle(final String title) {
		this.title= title;
	}
	
	String getTitle() {
		return this.title;
	}
	
	@Override
	public String getLabel() {
		return this.title;
	}
	
	void setImageDescriptor(final @Nullable ImageDescriptor imageDescriptor) {
		this.imageDescriptor= imageDescriptor;
	}
	
	@Override
	public @Nullable ImageDescriptor getImageDescriptor() {
		return this.imageDescriptor;
	}
	
	
	int putStatic(final String html) {
		var staticContent= this.staticContent;
		if (staticContent == null) {
			staticContent= new IntArrayMap<>();
			this.staticContent= staticContent;
		}
		final int id= staticContent.size();
		staticContent.put(id, html);
		return id;
	}
	
	@Nullable String getStatic(final int id) {
		final var staticContent= this.staticContent;
		if (staticContent == null) {
			return null;
		}
		return staticContent.get(id);
	}
	
}
