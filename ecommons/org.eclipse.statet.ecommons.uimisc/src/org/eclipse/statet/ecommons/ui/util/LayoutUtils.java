/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import java.util.Collection;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class LayoutUtils {
	
	private static class DialogValues {
		
		int defaultEntryFieldWidth;
		
		int defaultHMargin;
		int defaultVMargin;
		int defaultHSpacing;
		int defaultVSpacing;
		int defaultIndent;
		int defaultSmallIndent;
		
		public DialogValues() {
			final GC gc= new GC(Display.getCurrent());
			try {
				gc.setFont(JFaceResources.getDialogFont());
				final FontMetrics fontMetrics= gc.getFontMetrics();
				
				this.defaultHMargin= Dialog.convertHorizontalDLUsToPixels(fontMetrics, IDialogConstants.HORIZONTAL_MARGIN);
				this.defaultVMargin= Dialog.convertVerticalDLUsToPixels(fontMetrics, IDialogConstants.VERTICAL_MARGIN);
				this.defaultHSpacing= Dialog.convertHorizontalDLUsToPixels(fontMetrics, IDialogConstants.HORIZONTAL_SPACING);
				this.defaultVSpacing= Dialog.convertVerticalDLUsToPixels(fontMetrics, IDialogConstants.VERTICAL_SPACING);
				this.defaultEntryFieldWidth= Dialog.convertHorizontalDLUsToPixels(fontMetrics, IDialogConstants.ENTRY_FIELD_WIDTH);
				this.defaultIndent= Dialog.convertHorizontalDLUsToPixels(fontMetrics, IDialogConstants.INDENT);
				this.defaultSmallIndent= Dialog.convertHorizontalDLUsToPixels(fontMetrics, IDialogConstants.SMALL_INDENT);
			}
			finally {
				gc.dispose();
			}
		}
	}
	
	private static @Nullable DialogValues gDialogValues;
	
	@SuppressWarnings("null")
	private static DialogValues getDialogValues() {
		if (gDialogValues == null) {
			JFaceResources.getFontRegistry().addListener(new IPropertyChangeListener() {
				@Override
				public void propertyChange(final PropertyChangeEvent event) {
					if (JFaceResources.DIALOG_FONT.equals(event.getProperty())) {
						UIAccess.getDisplay().asyncExec(new Runnable() {
							@Override
							public void run() {
								gDialogValues= new DialogValues();
							}
						});
					}
				}
			});
			gDialogValues= new DialogValues();
		}
		return gDialogValues;
	}
	
	
	public static int defaultHMargin() {
		return getDialogValues().defaultHMargin;
	}
	
	public static int defaultVMargin() {
		return getDialogValues().defaultVMargin;
	}
	
	public static Point defaultSpacing() {
		return new Point(getDialogValues().defaultHSpacing, getDialogValues().defaultVSpacing);
	}
	
	public static int defaultHSpacing() {
		return getDialogValues().defaultHSpacing;
	}
	
	public static int defaultVSpacing() {
		return getDialogValues().defaultVSpacing;
	}
	
	public static int defaultIndent() {
		return getDialogValues().defaultIndent;
	}
	
	public static int defaultSmallIndent() {
		return getDialogValues().defaultSmallIndent;
	}
	
	
	public static int hintWidth(final Button button) {
		button.setFont(JFaceResources.getFontRegistry().get(JFaceResources.DIALOG_FONT));
		final PixelConverter converter= new PixelConverter(button);
		final int widthHint= converter.convertHorizontalDLUsToPixels(IDialogConstants.BUTTON_WIDTH);
		return Math.max(widthHint, button.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
	}
	
	public static GridData hintWidth(final GridData gd, final Button button) {
		gd.widthHint= hintWidth(button);
		return gd;
	}
	
	public static int hintWidth(final Text text, final int numChars) {
		return hintWidth(text, JFaceResources.DIALOG_FONT, numChars);
	}
	
	public static GridData hintWidth(final GridData gd, final Text text, final int numChars) {
		gd.widthHint= hintWidth(text, JFaceResources.DIALOG_FONT, numChars);
		return gd;
	}
	
	public static int hintWidth(final Text text, final String symbolicName, final int numChars) {
		if (symbolicName != null) {
			text.setFont(JFaceResources.getFontRegistry().get(symbolicName));
		}
		if (numChars == -1) {
			return getDialogValues().defaultEntryFieldWidth;
		}
		final PixelConverter converter= new PixelConverter(text);
		final int widthHint= converter.convertWidthInCharsToPixels(numChars);
		return widthHint;
	}
	
	public static int hintWidth(final StyledText text, final String symbolicName, final int numChars) {
		if (symbolicName != null) {
			text.setFont(JFaceResources.getFontRegistry().get(symbolicName));
		}
		if (numChars == -1) {
			return getDialogValues().defaultEntryFieldWidth;
		}
		final PixelConverter converter= new PixelConverter(text);
		final int widthHint= converter.convertWidthInCharsToPixels(numChars);
		return widthHint;
	}
	
	public static int hintWidth(final Combo combo, final int numChars) {
		return hintWidth(combo, JFaceResources.DIALOG_FONT, numChars);
	}
	
	public static int hintWidth(final Combo combo, final String fontName, final int numChars) {
		combo.setFont(JFaceResources.getFontRegistry().get(fontName));
		if (numChars == -1) {
			return getDialogValues().defaultEntryFieldWidth;
		}
		final PixelConverter converter= new PixelConverter(combo);
		int widthHint= converter.convertWidthInCharsToPixels(numChars+1);
		
		final Rectangle trim= combo.computeTrim(0, 0, widthHint, 0);
		if (trim.width > widthHint) {
			widthHint= trim.width;
		}
		else if ((combo.getStyle() & SWT.DROP_DOWN) == SWT.DROP_DOWN) {
			final Button button= new Button(combo.getParent(), SWT.ARROW | SWT.DOWN);
			widthHint += button.computeSize(SWT.DEFAULT, SWT.DEFAULT).x + 2;
			button.dispose();
//			widthHint += combo.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
		}
		
		return widthHint;
	}
	
	public static GridData hintWidth(final GridData gd, final Combo combo, final int numChars) {
		return hintWidth(gd, combo, JFaceResources.DIALOG_FONT, numChars);
	}
	
	public static GridData hintWidth(final GridData gd, final Combo combo, final String fontName, final int numChars) {
		gd.widthHint= hintWidth(combo, fontName, numChars);
		return gd;
	}
	
	public static int hintWidth(final Combo combo, final String[] items) {
		return hintWidth(combo, ImCollections.newList(items));
	}
	
	public static int hintWidth(final Combo combo, final Collection<String> items) {
		int max= 0;
		for (final String s : items) {
			max= Math.max(max, s.length());
		}
		return hintWidth(combo, JFaceResources.DIALOG_FONT, max);
	}
	
	public static int hintWidth(final Combo combo, final java.util.List<?> input,
			final ILabelProvider labelProvider) {
		combo.setFont(JFaceResources.getFontRegistry().get(JFaceResources.DIALOG_FONT));
		final GC gc= new GC(combo);
		int widthHint= 0;
		for (final Object o : input) {
			final String s= labelProvider.getText(o);
			if (s != null) {
				widthHint= Math.max(widthHint, gc.stringExtent(s).x);
			}
		}
		gc.dispose();
		
		final Rectangle trim= combo.computeTrim(0, 0, widthHint, 0);
		if (trim.width > widthHint) {
			widthHint= trim.width;
		}
		
		return widthHint;
	}
	
	
	public static int hintWidth(final Table table, final String fontName,
			final boolean icon, final int numChars) {
		if (fontName != null) {
			table.setFont(JFaceResources.getFontRegistry().get(fontName));
		}
		final PixelConverter converter= new PixelConverter(table);
		int width= converter.convertWidthInCharsToPixels(numChars);
		{	final ScrollBar scrollBar= table.getVerticalBar();
			if (scrollBar != null) {
				width+= scrollBar.getSize().x;
			}
		}
		if ((table.getStyle() & SWT.CHECK) == SWT.CHECK) {
			width+= 16 + converter.convertHorizontalDLUsToPixels(4) +  converter.convertWidthInCharsToPixels(1);
		}
		if (icon) {
			width+= 16 + converter.convertHorizontalDLUsToPixels(4);
		}
		return width;
	}
	
	public static int hintWidth(final Table table, final int numChars) {
		return hintWidth(table, JFaceResources.DIALOG_FONT, false, numChars);
	}
	
	public static int hintWidth(final Table table, final Collection<String> items) {
		int max= 0;
		for (final String s : items) {
			max= Math.max(max, s.length());
		}
		return hintWidth(table, max);
	}
	
	public static int hintWidth(final Table table, final Object[] input, final ILabelProvider labelProvider) {
		int max= 0;
		for (final Object o : input) {
			final String s= labelProvider.getText(o);
			if (s != null) {
				max= Math.max(max, s.length());
			}
		}
		return hintWidth(table, max);
	}
	
	public static int hintColWidth(final Table table, final int numChars) {
		table.setFont(JFaceResources.getFontRegistry().get(JFaceResources.DIALOG_FONT));
		final PixelConverter converter= new PixelConverter(table);
		final int width= converter.convertWidthInCharsToPixels(numChars);
		return width;
	}
	
	
	public static int hintWidth(final Tree tree, final String fontName,
			final boolean icon, final int numChars) {
		if (fontName != null) {
			tree.setFont(JFaceResources.getFontRegistry().get(fontName));
		}
		final PixelConverter converter= new PixelConverter(tree);
		int width= converter.convertWidthInCharsToPixels(numChars);
		{	final ScrollBar scrollBar= tree.getVerticalBar();
			if (scrollBar != null) {
				width+= scrollBar.getSize().x;
			}
		}
		if ((tree.getStyle() & SWT.CHECK) == SWT.CHECK) {
			width+= 16 + converter.convertHorizontalDLUsToPixels(4) +  converter.convertWidthInCharsToPixels(1);
		}
		if (icon) {
			width+= 16 + converter.convertHorizontalDLUsToPixels(4);
		}
		return width;
	}
	
	public static int hintWidth(final Tree tree, final int numChars) {
		return hintWidth(tree, JFaceResources.DIALOG_FONT, false, numChars);
	}
	
	public static int hintWidth(final Tree tree, final Collection<String> items) {
		int max= 0;
		for (final String s : items) {
			max= Math.max(max, s.length());
		}
		return hintWidth(tree, max);
	}
	
	
	public static int hintHeight(final List control, final int rows) {
		control.setFont(JFaceResources.getFontRegistry().get(JFaceResources.DIALOG_FONT));
		return control.getItemHeight() * rows;
	}
	
	public static int hintHeight(final Table control, final int rows, final boolean withScrollbar) {
		control.setFont(JFaceResources.getFontRegistry().get(JFaceResources.DIALOG_FONT));
		
		int height= control.getHeaderHeight();
		height += control.getItemHeight() * rows;
		
		if (!withScrollbar && Platform.getWS().equals(Platform.WS_WIN32)) {
			final ScrollBar hBar= control.getHorizontalBar();
			if (hBar != null) {
				height -= hBar.getSize().y;
			}
		}
		else if (Platform.getWS().equals(Platform.WS_WIN32)) {
			height += control.getBorderWidth() * 2;
		}
		
		return height;
	}
	
	public static int hintHeight(final Table control, final int rows) {
		return hintHeight(control, rows, true);
	}
	
	
	public static int hintHeight(final Tree control, final int rows, final boolean withScrollbar) {
		control.setFont(JFaceResources.getFontRegistry().get(JFaceResources.DIALOG_FONT));
		
		int height= control.getHeaderHeight();
		height += control.getItemHeight() * rows;
		
		if (!withScrollbar && Platform.getWS().equals(Platform.WS_WIN32)) {
			final ScrollBar hBar= control.getHorizontalBar();
			if (hBar != null) {
				height -= hBar.getSize().y;
			}
		}
		else if (Platform.getWS().equals(Platform.WS_WIN32)) {
			height += control.getBorderWidth() * 2;
		}
		
		return height;
	}
	
	public static int hintHeight(final Tree control, final int rows) {
		return hintHeight(control, rows, true);
	}
	
	
	public static int hintHeight(final Label control, final int lines) {
		final PixelConverter converter= new PixelConverter(control);
		return converter.convertHeightInCharsToPixels(lines);
	}
	
	public static int hintHeight(final StyledText control, final int lines) {
		int height= control.getLineHeight() * lines;
		
		final ScrollBar hBar= control.getHorizontalBar();
		if (hBar != null) {
			height+= hBar.getSize().y;
		}
		
		return height;
	}
	
	
	public static GridData createGD(final Button button) {
		final GridData gd= new GridData(SWT.FILL, SWT.FILL, false, false);
		gd.widthHint= hintWidth(button);
		return gd;
	}
	
	
	public static GridLayout applyDialogDefaults(final GridLayout layout) {
		final DialogValues dialogValues= getDialogValues();
		layout.marginWidth= dialogValues.defaultHMargin;
		layout.marginHeight= dialogValues.defaultVMargin;
		layout.horizontalSpacing= dialogValues.defaultHSpacing;
		layout.verticalSpacing= dialogValues.defaultVSpacing;
		return layout;
	}
	
	public static GridLayout newDialogGrid(final int numColumns) {
		final GridLayout layout= new GridLayout(numColumns, false);
		layout.numColumns= numColumns;
		applyDialogDefaults(layout);
		return layout;
	}
	
	
	public static GridLayout applyCompositeDefaults(final GridLayout layout) {
		final DialogValues dialogValues= getDialogValues();
		layout.marginWidth= 0;
		layout.marginHeight= 0;
		layout.horizontalSpacing= dialogValues.defaultHSpacing;
		layout.verticalSpacing= dialogValues.defaultVSpacing;
		return layout;
	}
	
	public static GridLayout newCompositeGrid() {
		final GridLayout layout= new GridLayout(); // (1, false)
		applyCompositeDefaults(layout);
		return layout;
	}
	
	public static GridLayout newCompositeGrid(final int numColumns) {
		final GridLayout layout= new GridLayout(numColumns, false);
		applyCompositeDefaults(layout);
		return layout;
	}
	
	public static GridLayout newCompositeGrid(final int numColumns, final boolean equalWidth) {
		final GridLayout layout= new GridLayout(numColumns, equalWidth);
		applyCompositeDefaults(layout);
		return layout;
	}
	
	
	public static GridLayout applyGroupDefaults(final GridLayout layout) {
		final DialogValues dialogValues= getDialogValues();
		layout.marginWidth= dialogValues.defaultHSpacing;
		layout.marginHeight= dialogValues.defaultVSpacing;
		layout.horizontalSpacing= dialogValues.defaultHSpacing;
		layout.verticalSpacing= dialogValues.defaultVSpacing;
		return layout;
	}
	
	public static GridLayout newGroupGrid(final int numColumns) {
		final GridLayout layout= new GridLayout(numColumns, false);
		applyGroupDefaults(layout);
		return layout;
	}
	
	public static GridLayout newGroupGrid(final int numColumns, final boolean equalWidth) {
		final GridLayout layout= new GridLayout(numColumns, equalWidth);
		applyGroupDefaults(layout);
		return layout;
	}
	
	
	public static GridLayout applyContentDefaults(final GridLayout layout) {
		final DialogValues dialogValues= getDialogValues();
		layout.marginWidth= dialogValues.defaultHSpacing;
		layout.marginHeight= dialogValues.defaultVSpacing;
		layout.horizontalSpacing= dialogValues.defaultHSpacing;
		layout.verticalSpacing= dialogValues.defaultVSpacing;
		return layout;
	}
	
	public static GridLayout newContentGrid() {
		final GridLayout layout= new GridLayout(); // (1, false)
		applyContentDefaults(layout);
		return layout;
	}
	
	public static GridLayout newContentGrid(final int numColumns) {
		final GridLayout layout= new GridLayout(numColumns, false);
		applyContentDefaults(layout);
		return layout;
	}
	
	public static GridLayout newContentGrid(final int numColumns, final boolean equalWidth) {
		final GridLayout layout= new GridLayout(numColumns, equalWidth);
		applyContentDefaults(layout);
		return layout;
	}
	
	
	public static GridLayout applyTabDefaults(final GridLayout layout) {
		final DialogValues dialogValues= getDialogValues();
		layout.marginWidth= dialogValues.defaultHSpacing;
		layout.marginHeight= dialogValues.defaultVSpacing;
		layout.horizontalSpacing= dialogValues.defaultHSpacing;
		layout.verticalSpacing= dialogValues.defaultVSpacing;
		return layout;
	}
	
	public static FillLayout applyTabDefaults(final FillLayout layout) {
		final DialogValues dialogValues= getDialogValues();
		layout.marginWidth= dialogValues.defaultHSpacing;
		layout.marginHeight= dialogValues.defaultVSpacing;
		layout.spacing= (layout.type == SWT.HORIZONTAL) ?
				dialogValues.defaultHSpacing : dialogValues.defaultVSpacing;
		return layout;
	}
	
	public static GridLayout newTabGrid() {
		final GridLayout layout= new GridLayout(); // (1, false)
		applyTabDefaults(layout);
		return layout;
	}
	
	public static GridLayout newTabGrid(final int numColumns) {
		final GridLayout layout= new GridLayout(numColumns, false);
		applyTabDefaults(layout);
		return layout;
	}
	
	public static GridLayout newTabGrid(final int numColumns, final boolean equalWidth) {
		final GridLayout layout= new GridLayout(numColumns, equalWidth);
		applyTabDefaults(layout);
		return layout;
	}
	
	
	public static GridLayout applySashDefaults(final GridLayout layout) {
		layout.marginWidth= 0;
		layout.marginHeight= 0;
		layout.horizontalSpacing= 0;
		layout.verticalSpacing= 0;
		return layout;
	}
	
	public static GridLayout newSashGrid(final int numColumns) {
		final GridLayout layout= new GridLayout();
		layout.numColumns= numColumns;
		applySashDefaults(layout);
		return layout;
	}
	
	public static GridLayout newSashGrid() {
		final GridLayout layout= new GridLayout();
		applySashDefaults(layout);
		return layout;
	}
	
	
	public static void addGDDummy(final Composite composite) {
		addGDDummy(composite, false);
	}
	public static void addGDDummy(final Composite composite, final boolean grab) {
		final Label dummy= new Label(composite, SWT.NONE);
		dummy.setVisible(false);
		dummy.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, grab, false));
	}
	public static void addGDDummy(final Composite composite, final boolean grab, final int span) {
		final Label dummy= new Label(composite, SWT.NONE);
		dummy.setVisible(false);
		dummy.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, grab, false, span, 1));
	}
	
	/**
	 * Adds a small vertical space (filler) to the given composite
	 * 
	 * @param composite The composite to add the filler to
	 * @param grab Whether the filler should grap vertical space
	 */
	public static void addSmallFiller(final Composite composite, final boolean grab) {
		final Label filler= new Label(composite, SWT.NONE);
		final Layout layout= composite.getLayout();
		if (layout instanceof GridLayout) {
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, false, grab);
			gd.horizontalSpan= ((GridLayout) layout).numColumns;
			gd.heightHint= defaultVSpacing() / 2;
			filler.setLayoutData(gd);
		}
	}
	
	
	private LayoutUtils() {}
	
}
