/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;

import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;


/**
 * To support status update for {@link TitleAreaDialog}
 */
public class TitleAreaStatusUpdater implements StatusChangeListener {
	
	
	private final TitleAreaDialog fDialog;
	
	private final String fDefaultMessage;
	
	
	public TitleAreaStatusUpdater(final TitleAreaDialog dialog, final String defaultMessage) {
		fDialog = dialog;
		fDefaultMessage = defaultMessage;
		
		fDialog.setMessage(defaultMessage);
	}
	
	
	@Override
	public void statusChanged(final IStatus status) {
//			setErrorMessage(null);
		switch (status.getSeverity()) {
		case IStatus.ERROR:
			fDialog.setMessage(status.getMessage(), IMessageProvider.ERROR);
			break;
		case IStatus.WARNING:
			fDialog.setMessage(status.getMessage(), IMessageProvider.WARNING);
			break;
		case IStatus.INFO:
			fDialog.setMessage(status.getMessage(), IMessageProvider.INFORMATION);
			break;
		case IStatus.OK:
			fDialog.setMessage(fDefaultMessage);
			break;
		default:
			break;
		}
	}
	
}
