/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;


public class ImageRegistryUtil {
	
	/** Icons of Views */
	public static final String T_VIEW=          "view_16"; //$NON-NLS-1$
	/** Icons for global tools */
	public static final String T_TOOL=          "tool_16"; //$NON-NLS-1$
	/** Icons for deactivated global tools */
	public static final String T_TOOLD=         "tool_16_d"; //$NON-NLS-1$
	/** Icons for object, e.g. files or model objects. */
	public static final String T_OBJ=           "obj_16"; //$NON-NLS-1$
	/** Icon overlays */
	public static final String T_OVR=           "ovr_16"; //$NON-NLS-1$
	/** Icons in the banners of wizards */
	public static final String T_WIZBAN=        "wizban"; //$NON-NLS-1$
	/** Icons in local tools */
	public static final String T_LOCTOOL=       "loctool_16"; //$NON-NLS-1$
	/** Icons of deactivated local tools */
	public static final String T_LOCTOOL_D=     "loctool_16_d"; //$NON-NLS-1$
	
	
	private final AbstractUIPlugin plugin;
	
	private final URL iconBaseURL;
	
	
	public ImageRegistryUtil(final AbstractUIPlugin plugin) {
		this.plugin= plugin;
		this.iconBaseURL= plugin.getBundle().getEntry("/icons/"); //$NON-NLS-1$
	}
	
	
	public void register(final String key, final String prefix, final String name) {
		ImageDescriptor descriptor;
		try {
			final var url= createIconFileURL(prefix + '/' + name);
			descriptor= createDescriptor(url);
		}
		catch (final MalformedURLException | RuntimeException e) {
			this.plugin.getLog().log(new Status(IStatus.ERROR, this.plugin.getBundle().getSymbolicName(), 0,
					String.format("Error occured while loading an image descriptor (key= %1$s).", key), //$NON-NLS-1$
					e ));
			descriptor= ImageDescriptor.getMissingImageDescriptor();
		}
		this.plugin.getImageRegistry().put(key, descriptor);
	}
	
	public void register(final String key, final String name) {
		ImageDescriptor descriptor;
		try {
			final int typeStartIdx= key.indexOf("/images/") + 8; //$NON-NLS-1$
			final int typeEndIdx= key.indexOf('/', typeStartIdx);
			final String type= key.substring(typeStartIdx, typeEndIdx);
			String path;
			switch (type) {
			case "obj": //$NON-NLS-1$
				path= T_OBJ + '/';
				break;
			case "tool": //$NON-NLS-1$
				path= T_TOOL + '/';
				break;
			case "ovr": //$NON-NLS-1$
				path= T_OVR + '/';
				break;
			case "loctool": //$NON-NLS-1$
				path= T_LOCTOOL + '/';
				break;
			default:
				throw new IllegalArgumentException(key);
			}
			
			if (name.charAt(0) == '.') {
				path= path + key.substring(typeEndIdx + 1) + name;
			}
			else {
				path= path + name;
			}
			final var url= createIconFileURL(path);
			descriptor= createDescriptor(url);
		}
		catch (final MalformedURLException | RuntimeException e) {
			this.plugin.getLog().log(new Status(IStatus.ERROR, this.plugin.getBundle().getSymbolicName(), 0,
					String.format("Error occured while loading an image descriptor (key= %1$s).", key), //$NON-NLS-1$
					e ));
			descriptor= ImageDescriptor.getMissingImageDescriptor();
		}
		this.plugin.getImageRegistry().put(key, descriptor);
	}
	
	protected URL createIconFileURL(final String path) throws MalformedURLException {
		if (this.iconBaseURL == null) {
			throw new MalformedURLException();
		}
		return new URL(this.iconBaseURL, path);
	}
	
	protected ImageDescriptor createDescriptor(final URL url) {
		return ImageDescriptor.createFromURL(url);
	}
	
}
