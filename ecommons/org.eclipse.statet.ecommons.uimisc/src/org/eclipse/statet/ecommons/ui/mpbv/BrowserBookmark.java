/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class BrowserBookmark {
	
	
	private final String label;
	
	private final String url;
	
	
	public BrowserBookmark(final String label, final String url) {
		this.label= label;
		this.url= url;
	}
	
	
	public String getLabel() {
		return this.label;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	
	@Override
	public String toString() {
		return this.label + " [" + this.url + "]";
	}
	
}
