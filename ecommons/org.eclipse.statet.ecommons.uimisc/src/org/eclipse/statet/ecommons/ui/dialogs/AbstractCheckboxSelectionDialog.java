/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs;

import java.util.Collection;
import java.util.Set;

import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


@NonNullByDefault
public abstract class AbstractCheckboxSelectionDialog<T> extends ExtStatusDialog {
	
	
	private final WritableSet<T> checkedValue;
	
	private CheckboxTableViewer viewer;
	
	
	/**
	 * Creates a new dialog
	 * 
	 * @param shell
	 * @param checkedElements optional collection of initially checked elements
	 */
	public AbstractCheckboxSelectionDialog(final Shell shell, final int options,
			final Collection<T> checkedElements ) {
		super(shell, options);
		setShellStyle(getShellStyle() | SWT.RESIZE);
		
		this.checkedValue= (checkedElements != null) ? new WritableSet<>() : new WritableSet<>(checkedElements, null);
	}
	
	
	/**
	 * Returns the viewer cast to the correct instance.  Possibly <code>null</code> if
	 * the viewer has not been created yet.
	 * 
	 * @return the viewer cast to CheckboxTableViewer
	 */
	protected CheckboxTableViewer getCheckBoxTableViewer() {
		return this.viewer;
	}
	
	protected Composite createCheckboxComposite(final Composite parent, final String message) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(3));
		
		if (message != null)
		{	final Label label= new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
			label.setText(message);
		}
		{	final Table table= new Table(composite, SWT.BORDER | SWT.SINGLE | SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
			gd.heightHint= 250;
			gd.widthHint= 300;
			table.setLayoutData(gd);
			
			this.viewer= new CheckboxTableViewer(table);
			this.viewer.addCheckStateListener(new ICheckStateListener() {
				@Override
				public void checkStateChanged(final CheckStateChangedEvent event) {
					updateCheckedStatus();
				}
			});
			
			configureViewer(this.viewer);
		}
		
		{	final Button button= new Button(composite, SWT.PUSH);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, false, false);
			gd.widthHint= LayoutUtils.hintWidth(button);
			button.setLayoutData(gd);
			button.setText("Select All");
			button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					getCheckBoxTableViewer().setAllChecked(true);
					updateCheckedStatus();
				}
			});
		}
		{	final Button button= new Button(composite, SWT.PUSH);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, false, false);
			gd.widthHint= LayoutUtils.hintWidth(button);
			button.setLayoutData(gd);
			button.setText("Deselect All");
			button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					getCheckBoxTableViewer().setAllChecked(false);
					updateCheckedStatus();
				}
			});
		}
		return composite;
	}
	
	protected abstract void configureViewer(CheckboxTableViewer viewer);
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		db.getContext().bindSet(
				ViewerProperties.checkedElements(null)
						.observe((ICheckable)this.viewer),
				this.checkedValue );
	}
	
	public Set<T> getCheckedElements() {
		return this.checkedValue;
	}
	
	protected void updateCheckedStatus() {
		getButton(IDialogConstants.OK_ID).setEnabled(isValid());
	}
	
	protected boolean isValid() {
		return getCheckBoxTableViewer().getCheckedElements().length > 0;
	}
	
}
