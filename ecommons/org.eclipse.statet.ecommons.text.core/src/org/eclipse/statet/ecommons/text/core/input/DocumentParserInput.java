/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.input;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;


/**
 * Text parser input for JFace document.
 */
@NonNullByDefault
public class DocumentParserInput extends TextParserInput {
	
	
	private IDocument document;
	
	
	public DocumentParserInput(final int defaultBufferSize) {
		super(defaultBufferSize);
	}
	
	public DocumentParserInput() {
		this(DEFAULT_BUFFER_SIZE);
	}
	
	public DocumentParserInput(final IDocument document) {
		this(Math.min(document.getLength(), DEFAULT_BUFFER_SIZE));
		
		this.document= document;
	}
	
	
	public DocumentParserInput reset(final IDocument document) {
		if (document == null) {
			throw new NullPointerException("document"); //$NON-NLS-1$
		}
		this.document= document;
		
		super.reset();
		
		return this;
	}
	
	
	public IDocument getDocument() {
		return this.document;
	}
	
	@Override
	protected int getSourceLength() {
		return (this.document != null) ? this.document.getLength() : 0;
	}
	
	@Override
	protected String getSourceString() {
		return (this.document != null) ? this.document.get() : null;
	}
	
	
	@Override
	protected void doUpdateBuffer(final int index, final char[] buffer,
			final int requiredLength, final int recommendLength) {
		try {
			final int length= Math.min(recommendLength, getStopIndex() - index);
			final int reused= copyBuffer0(buffer);
			if (length > reused) {
				final int l= length - reused;
				this.document.get(index + reused, l).getChars(0, l, buffer, reused);
			}
			setBuffer(buffer, 0, length);
		}
		catch (final BadLocationException e) {
			throw new RuntimeException(e);
		}
	}
	
}
