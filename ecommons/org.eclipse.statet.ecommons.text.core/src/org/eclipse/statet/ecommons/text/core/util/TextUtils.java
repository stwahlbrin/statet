/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.util;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.ISynchronizable;
import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;


/**
 * @since de.walware.ecommons.text 1.0
 */
@NonNullByDefault
public final class TextUtils {
	
	
	public static Object getLockObject(final Object o) {
		final Object lockObject= (o instanceof ISynchronizable) ?
				((ISynchronizable)o).getLockObject() : null;
		return (lockObject != null) ? lockObject : o;
	}
	
	
	/**
	 * @since de.walware.ecommons.text 1.1
	 */
	public static String getContentType(final IDocument document, final DocContentSections contentInfo,
			final int offset, final boolean preferOpenPartition)
			throws BadPartitioningException, BadLocationException {
		return ((IDocumentExtension3)document).getContentType(contentInfo.getPartitioning(),
				offset, preferOpenPartition );
	}
	
	/**
	 * @since de.walware.ecommons.text 1.1
	 */
	public static ITypedRegion getPartition(final IDocument document, final DocContentSections contentInfo,
			final int offset, final boolean preferOpenPartition)
					throws BadPartitioningException, BadLocationException {
		return ((IDocumentExtension3)document).getPartition(contentInfo.getPartitioning(),
				offset, preferOpenPartition );
	}
	
	
}
