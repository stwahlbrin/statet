/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.sections;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Description of document content which can be composed of sections with different content types.
 */
@NonNullByDefault
public interface DocContentSections {
	
	
	String ERROR= ""; //$NON-NLS-1$
	
	
	/**
	 * Returns the document partitioning associated to this description.
	 * 
	 * @return the id of the paritioning
	 */
	String getPartitioning();
	
	/**
	 * Returns all supported content types.
	 * 
	 * @return list of ids of the section content types
	 */
	ImList<String> getAllTypes();
	
	/**
	 * Returns the primary content type.
	 * 
	 * @return id of the primary section content type
	 */
	String getPrimaryType();
	
	/**
	 * Returns the content type at the specified document position.
	 * 
	 * @param document
	 * @param offset offset in the document
	 * @return id of the section content type, or
	 *     the {@link #ERROR error id} if the specified offset is invalid
	 */
	String getType(final IDocument document, int offset);
	
	/**
	 * Returns the content type of the specified partition content type.
	 * 
	 * @param partitionContentType id of the partition content type
	 * @return id of the section content type
	 */
	String getTypeByPartition(final String partitionContentType);
	
}
