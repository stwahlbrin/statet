/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.sections;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextUtilities;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class BasicDocContentSections implements DocContentSections {
	
	
	private final String partitioning;
	
	private final String primaryType;
	private final ImList<String> allTypes;
	
	
	protected BasicDocContentSections(final String partitioning, final String primaryType,
			final ImList<String> secondaryTypes) {
		this.partitioning= partitioning;
		this.primaryType= primaryType;
		this.allTypes= secondaryTypes;
	}
	
	protected BasicDocContentSections(final String partitioning, final String primaryType) {
		this(partitioning, primaryType, ImCollections.newList(primaryType));
	}
	
	
	@Override
	public final String getPartitioning() {
		return this.partitioning;
	}
	
	@Override
	public String getPrimaryType() {
		return this.primaryType;
	}
	
	@Override
	public ImList<String> getAllTypes() {
		return this.allTypes;
	}
	
	
	@Override
	public String getType(final IDocument document, final int offset) {
		try {
			return getTypeByPartition(
					TextUtilities.getPartition(document, getPartitioning(), offset, true).getType()
					);
		}
		catch (final BadLocationException e) {
			return ERROR;
		}
	}
	
	@Override
	public abstract String getTypeByPartition(final String contentType);
	
}
