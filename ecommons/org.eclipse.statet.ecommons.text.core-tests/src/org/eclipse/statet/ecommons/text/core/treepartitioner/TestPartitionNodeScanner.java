/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNodeScan.BreakException;


@NonNullByDefault
public class TestPartitionNodeScanner implements TreePartitionNodeScanner {
	
	
	@Override
	public int getRestartOffset(final TreePartitionNode node, final IDocument document, final int offset)
			throws BadLocationException {
		return 0;
	}
	
	@Override
	public BasicPartitionNodeType getDefaultRootType() {
		return TestPartitionNodeType.DEFAULT_ROOT;
	}
	
	
	@Override
	public void execute(final TreePartitionNodeScan scan) throws BreakException {
		try {
			final IDocument document= scan.getDocument();
			TreePartitionNode node= scan.getBeginNode();
			int offset= scan.getStartOffset();
			while (offset < scan.getEndOffset()) {
				switch (document.getChar(offset)) {
				case '{':
					node= scan.add(TestPartitionNodeType.T1, node, offset, 0);
					break;
				case '[':
					node= scan.add(TestPartitionNodeType.T2, node, offset, 0);
					break;
				case '(':
					node= scan.add(TestPartitionNodeType.T3, node, offset, 0);
					break;
				case '}':
				case ']':
				case ')':
					scan.expand(node, offset + 1, 0, true);
					node= node.getParent();
					assertNotNull(node);
					break;
				default:
					break;
				}
				offset++;
			}
			if (TestPartitionNodeType.DEFAULT_ROOT != node.getType()) {
				throw new AssertionError(node.getType().toString());
			}
		}
		catch (final BadLocationException e) {
			throw new AssertionError(e);
		}
	}
	
}
