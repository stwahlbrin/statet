/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.validation;

import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ValidationUtils {
	
	
	public static IStatus newStatus(final int severity, final String message) {
		switch (severity) {
		case IStatus.OK:
			return ValidationStatus.ok();
		case IStatus.INFO:
			return ValidationStatus.info(message);
		case IStatus.WARNING:
			return ValidationStatus.warning(message);
		case IStatus.ERROR:
			return ValidationStatus.error(message);
		default:
			throw new IllegalArgumentException("severity= " + severity); //$NON-NLS-1$
		}
	}
	
	
	private ValidationUtils() {}
	
}
