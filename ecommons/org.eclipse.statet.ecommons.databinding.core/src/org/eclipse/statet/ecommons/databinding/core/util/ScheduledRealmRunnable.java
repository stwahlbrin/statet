/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.util;

import java.util.concurrent.TimeUnit;

import org.eclipse.core.databinding.observable.Realm;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * @see org.eclipse.statet.ecommons.ui.swt.ScheduledDisplayRunnable
 */
@NonNullByDefault
public abstract class ScheduledRealmRunnable implements Runnable {
	
	
	public static ScheduledRealmRunnable adapt(final Runnable runnable, final Realm realm) {
		return new ScheduledRealmRunnable(realm) {
			@Override
			protected void execute() {
				runnable.run();
			}
			
		};
	}
	
	
	private static final int IDLE= 0;
	private static final int STOPPED= -1;
	
	private static final byte NOT_SCHEDULED= 0;
	private static final byte DIRECT_SCHEDULED= 1;
	private static final byte TIMED_SCHEDULED= 2;
	
	private static final int PREV_NANOS_TOL= 2_000;
	private static final int SCHEDULE_NANOS_TOL= 2_000_000;
	
	
	private final Realm realm;
	
	/** {@link #STOPPED}, {@link #IDLE}, > 0: scheduledPower */
	private int state= IDLE;
	/** {@link #NOT_SCHEDULED}, {@link #DIRECT_SCHEDULED}, {@link #TIMED_SCHEDULED} */
	private byte scheduled= NOT_SCHEDULED;
	private long scheduledTime;
	
	
	public ScheduledRealmRunnable(final Realm realm) {
		this.realm= realm;
	}
	
	
	public void scheduleFor(final long nanoTime, final int power) {
		if (power <= 0) {
			throw new IllegalArgumentException();
		}
		synchronized (this) {
			try {
				if (this.state == STOPPED) {
					return;
				}
				final long prevDiff= (this.state > IDLE) ? nanoTime - this.scheduledTime : -PREV_NANOS_TOL;
				if (power < this.state
						|| (power == this.state && prevDiff <= 0) ) {
					return;
				}
				this.state= power;
				this.scheduledTime= nanoTime;
				if (this.scheduled == DIRECT_SCHEDULED
						|| (this.scheduled == TIMED_SCHEDULED && prevDiff > -PREV_NANOS_TOL) ) {
					return;
				}
				
				final long scheduleDiff= nanoTime - System.nanoTime();
				if (scheduleDiff > SCHEDULE_NANOS_TOL && this.realm.isCurrent()) {
					this.scheduled= TIMED_SCHEDULED;
					this.realm.timerExec((int)(scheduleDiff / 1_000_000), this);
				}
				else {
					this.scheduled= DIRECT_SCHEDULED;
					this.realm.asyncExec(this);
				}
			}
			catch (final RuntimeException e) {
				handleScheduleException(e);
			}
		}
	}
	
	public void scheduleFor(final long nanoTime) {
		scheduleFor(nanoTime, 1);
	}
	
	public void scheduleForNow(final int power) {
		scheduleFor(System.nanoTime(), power);
	}
	
	public void scheduleWithDelay(final long delay, final TimeUnit unit, final int power) {
		if (delay < 0) {
			throw new IllegalArgumentException();
		}
		scheduleFor(System.nanoTime() + unit.toNanos(delay), power);
	}
	
	public void scheduleWithDelay(final long delay, final TimeUnit unit) {
		if (delay < 0) {
			throw new IllegalArgumentException();
		}
		scheduleFor(System.nanoTime() + unit.toNanos(delay), 1);
	}
	
	public void runNow() {
		if (this.realm.isCurrent()) {
			synchronized (this) {
				if (this.state == STOPPED) {
					return;
				}
				this.state= IDLE;
			}
			execute();
		}
		else {
			scheduleFor(System.nanoTime(), Integer.MAX_VALUE);
		}
	}
	
	public synchronized void cancel() {
		if (this.state == STOPPED) {
			return;
		}
		this.state= IDLE;
	}
	
	public synchronized void stop() {
		this.state= STOPPED;
	}
	
	@Override
	public final void run() {
		synchronized (this) {
			this.scheduled= NOT_SCHEDULED;
			if (this.state <= IDLE) {
				return;
			}
			final long scheduleDiff= this.scheduledTime - System.nanoTime();
			if (scheduleDiff > SCHEDULE_NANOS_TOL) {
				this.scheduled= TIMED_SCHEDULED;
				this.realm.timerExec((int)(scheduleDiff / 1_000_000L), this);
				return;
			}
			this.state= IDLE;
		}
		execute();
	}
	
	
	protected abstract void execute();
	
	
	protected void handleScheduleException(final RuntimeException e) throws RuntimeException {
		if (e.getClass().getName().equals("org.eclipse.swt.SWTException")) { // disposed //$NON-NLS-1$
			stop();
		}
		else {
			throw e;
		}
	}
	
}
