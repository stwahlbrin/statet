/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.conversion;

import org.eclipse.core.databinding.conversion.IConverter;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ChainedConverter<TFrom, TTo> implements IConverter<TFrom, TTo> {
	
	
	private final @Nullable Object fromType;
	private final @Nullable Object toType;
	
	@SuppressWarnings("rawtypes")
	private final ImList<IConverter> converters;
	
	
	public ChainedConverter(final @SuppressWarnings("rawtypes") ImList<IConverter> converters) {
		if (converters.isEmpty()) {
			throw new IllegalArgumentException("converters.size= 0");
		}
		
		this.converters= converters;
		this.fromType= converters.get(0).getFromType();
		this.toType= converters.get(converters.size() - 1).getToType();
	}
	
	
	@Override
	public @Nullable Object getFromType() {
		return this.fromType;
	}
	
	@Override
	public @Nullable Object getToType() {
		return this.toType;
	}
	
	@Override
	@NonNullByDefault({})
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TTo convert(final TFrom fromObject) {
		Object o= fromObject;
		for (final IConverter converter : this.converters) {
			o= converter.convert(o);
		}
		return (TTo)o;
	}
	
}
