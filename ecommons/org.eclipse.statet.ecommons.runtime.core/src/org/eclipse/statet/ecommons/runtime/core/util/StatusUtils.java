/*=============================================================================#
 # Copyright (c) 2000, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.runtime.core.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;


/**
 * A utility class to work with IStatus.
 */
@NonNullByDefault
public class StatusUtils {
	
	
	/**
	 * Compares two instances of <code>IStatus</code>. The more severe is returned:
	 * An error is more severe than a warning, and a warning is more severe
	 * than ok. If the two stati have the same severity, the second is returned.
	 */
	public static IStatus getMoreSevere(final IStatus s1, final IStatus s2) {
		if (s1.getSeverity() > s2.getSeverity()) {
			return s1;
		}
		else {
			return s2;
		}
	}
	
	
	public static IStatus convert(final Status status) {
		return EStatusUtils.convert(status);
	}
	
	public static CoreException convert(final StatusException e) {
		return EStatusUtils.convert(e);
	}
	
	
	public static IProgressMonitor convert(final @Nullable ProgressMonitor m,
			final String name, final int totalWork) {
		return EStatusUtils.convert(m, name, totalWork);
	}
	
	public static IProgressMonitor convert(final @Nullable ProgressMonitor m,
			final int totalWork) {
		return EStatusUtils.convert(m, totalWork);
	}
	
	public static IProgressMonitor convert(final @Nullable ProgressMonitor m) {
		return EStatusUtils.convert(m);
	}
	
}
