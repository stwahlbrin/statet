/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.runtime.core.util;

import org.eclipse.core.runtime.IPath;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class PathUtils {
	
	
	public static boolean isValid(final IPath path) {
		for (int i= 0; i < path.segmentCount(); i++) {
			if (!path.isValidSegment(path.segment(i))) {
				return false;
			}
		}
		return true;
	}
	
	public static @Nullable IPath check(final @Nullable IPath path) {
		return (path != null && isValid(path)) ? path : null;
	}
	
}
