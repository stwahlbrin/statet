/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Adapters;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


@NonNullByDefault
public abstract class ActionUtil<TElement> {
	
	
/*[ Action Parameters ]========================================================*/
	
	public static final String CONTENT_TYPE_PAR_NAME=       "contentTypeId"; //$NON-NLS-1$
	
	public static final String LAUNCH_FLAGS_PAR_NAME=       "launchFlags"; //$NON-NLS-1$
	
	
/*[ Action Modes ]=============================================================*/
	
	public static final byte ACTIVE_EDITOR_MODE=            1;
	public static final byte ACTIVE_MENU_SELECTION_MODE=    2;
	
	
	static void activateActiveEditor(final IWorkbenchWindow window) {
		final IWorkbenchPage page= window.getActivePage();
		final IEditorPart activeEditor= page.getActiveEditor();
		if (activeEditor != null && activeEditor != page.getActivePart()) {
			page.activate(activeEditor);
		}
	}
	
	static IWorkbenchWindow getWindow(final @Nullable IServiceLocator serviceLocator) {
		IWorkbenchWindow window= null;
		if (serviceLocator != null) {
			window= serviceLocator.getService(IWorkbenchWindow.class);
		}
		if (window == null) {
			window= PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		}
		return window;
	}
	
	
	private byte mode;
	
	
	public ActionUtil(final byte initialMode) {
		this.mode= initialMode;
	}
	
	
	public void setMode(final byte mode) {
		this.mode= mode;
	}
	
	public byte getMode() {
		return this.mode;
	}
	
	
	public @Nullable TElement getLaunchElement(final @Nullable IWorkbenchWindow window) {
		if (window == null) {
			return null;
		}
		switch (getMode()) {
		case ACTIVE_EDITOR_MODE:
			return getLaunchElement(getActiveEditor(window));
		case ACTIVE_MENU_SELECTION_MODE:
			return getLaunchElement(getActiveSelection(window));
		default:
			return null;
		}
	}
	
	public abstract @Nullable TElement getLaunchElement(@Nullable IEditorPart editor);
	
	public abstract @Nullable TElement getLaunchElement(@Nullable ISelection selection);
	
	public abstract @Nullable LaunchConfigManager<TElement> getManager(IWorkbenchWindow window, TElement element);
	
	
	protected @Nullable IEditorPart getActiveEditor(final IWorkbenchWindow window) {
		return window.getActivePage().getActiveEditor();
	}
	
	protected @Nullable ISelection getActiveSelection(final IWorkbenchWindow window) {
		final @Nullable IEclipseContext eContext= window.getService(IEclipseContext.class);
		if (eContext != null) {
			final Object selection= eContext.get("activeMenuSelection"); //$NON-NLS-1$
			if (selection instanceof ISelection) {
				return (ISelection) selection;
			}
		}
		return null;
	}
	
	protected @Nullable IResource getSingleResource(final @Nullable IEditorInput editorInput) {
		if (editorInput == null) {
			return null;
		}
		{	final IResource resource= Adapters.adapt(editorInput, IResource.class);
			if (resource != null) {
				return resource;
			}
		}
		return Adapters.adapt(editorInput, IFile.class);
	}
	
	protected @Nullable IResource getSingleResource(final @Nullable IStructuredSelection selection) {
		if (selection != null && selection.size() == 1) {
			final Object element= selection.getFirstElement();
			if (element instanceof IResource) {
				return (IResource) element;
			}
			if (element instanceof IAdaptable) {
				return (((IAdaptable) element).getAdapter(IResource.class));
			}
		}
		return null;
	}
	
	
	public void launchActive(final IWorkbenchWindow window,
			final LaunchConfigManager<TElement> manager,
			final TElement element, final ImIdentitySet<String> launchFlags) {
		final ILaunchConfiguration config= manager.getActiveConfig();
		if (config != null) {
			manager.launch(config, element, launchFlags);
		}
		else {
			final Runnable runnable= new Runnable() {
				@Override
				public void run() {
					if (getMode() == ACTIVE_EDITOR_MODE) {
						ActionUtil.activateActiveEditor(window);
					}
					manager.openConfigurationDialog(window.getShell(), null);
				}
			};
			UIAccess.getDisplay().asyncExec(runnable);
		}
	}
	
}
