/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.breakpoints;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.databinding.AggregateValidationStatus;
import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.ObservableEvent;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.IWorkbenchPartConstants;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.IdentitySet;

import org.eclipse.statet.ecommons.databinding.core.util.DirtyTracker;
import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.MessageUtils;


public abstract class AbstractBreakpointDetailEditor {
	
	
	private final boolean mnemonics;
	
	private Composite composite;
	
	private boolean isDirty;
	
	private DataBindingContext dbc;
	private boolean ignoreChanges;
	private AggregateValidationStatus aggregateStatus;
	private IStatus currentStatus;
	
	private final Set<IObservable> autosaveBindings;
	
	private final IdentitySet<IPropertyListener> propertyListeners;
	
	private boolean saveError;
	
	
	protected AbstractBreakpointDetailEditor(final boolean mnemonics, final boolean autosave,
			final IdentitySet<IPropertyListener> listeners) {
		this.mnemonics= mnemonics;
		this.autosaveBindings= (autosave) ? new HashSet<>() : null;
		this.propertyListeners= listeners;
	}
	
	
	public Composite createControl(final Composite parent) {
		this.composite= new Composite(parent, SWT.NONE);
		this.composite.setLayout(LayoutUtils.newCompositeGrid());
		
		addContent(this.composite);
		
		initBindings();
		
		return this.composite;
	}
	
	protected void addContent(final Composite parent) {
	}
	
	protected Control getComposite() {
		return this.composite;
	}
	
	protected String checkLabel(final String text) {
		if (this.mnemonics) {
			return text;
		}
		else {
			return MessageUtils.removeMnemonics(text);
		}
	}
	
	protected DataBindingContext getDataBindingContext() {
		return this.dbc;
	}
	
	protected void initBindings() {
		final Realm realm= Realm.getDefault();
		this.dbc= new DataBindingContext(realm);
		
		addBindings(this.dbc, realm);
		
		this.aggregateStatus= new AggregateValidationStatus(this.dbc, AggregateValidationStatus.MAX_SEVERITY);
		this.aggregateStatus.addValueChangeListener(new IValueChangeListener<IStatus>() {
			@Override
			public void handleValueChange(final ValueChangeEvent<? extends IStatus> event) {
				AbstractBreakpointDetailEditor.this.currentStatus= event.diff.getNewValue();
//				updateDialogState();
			}
		});
		this.currentStatus= ValidationStatus.ok();
		
		new DirtyTracker(this.dbc) {
			@Override
			public void handleChange(final ObservableEvent event) {
				if (!AbstractBreakpointDetailEditor.this.ignoreChanges && AbstractBreakpointDetailEditor.this.autosaveBindings != null && event != null
						&& AbstractBreakpointDetailEditor.this.autosaveBindings.contains(event.getObservable())) {
					save();
					return;
				}
				if (!isDirty()) {
//					this.currentStatus= (IStatus) this.aggregateStatus.getValue();
					setDirty(true);
//					updateDialogState();
				}
			}
		};
		
		this.composite.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				disposeBindings();
			}
		});
	}
	
	
	protected void enableAutosave(final Binding binding) {
		this.autosaveBindings.add(binding.getModel());
	}
	
	private void disposeBindings() {
		if (this.aggregateStatus != null) {
			this.aggregateStatus.dispose();
			this.aggregateStatus= null;
		}
		if (this.dbc != null) {
			this.dbc.dispose();
			this.dbc= null;
		}
		if (this.autosaveBindings != null) {
			this.autosaveBindings.clear();
		}
	}
	
	/**
	 * @param dbc
	 * @param realm
	 */
	protected void addBindings(final DataBindingContext dbc, final Realm realm) {
	}
	
	public boolean isDirty() {
		return this.isDirty;
	}
	
	protected void setDirty(final boolean dirty) {
		if (this.isDirty != dirty) {
			this.isDirty= dirty;
			firePropertyChange(IWorkbenchPartConstants.PROP_DIRTY);
		}
	}
	
	protected void firePropertyChange(final int propId) {
		if (this.propertyListeners != null) {
			for (final IPropertyListener listener : this.propertyListeners) {
				listener.propertyChanged(this, propId);
			}
		}
	}
	
	/**
	 * @return
	 */
	public IStatus getStatus() {
		return this.currentStatus;
	}
	
	/**
	 * @param input
	 */
	public final void setInput(final Object input) {
		this.ignoreChanges= true;
		try {
			doSetInput(input);
		}
		finally {
			this.ignoreChanges= false;
			setDirty(false);
		}
	}
	
	protected void doSetInput(final Object input) {
	}
	
	protected void logLoadError(final CoreException e) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID,
				"An error occurred when loading settings for the selected breakpoint." ));
	}
	
	/**
	 * Applies the settings to the breakpoints
	 * @return 
	 */
	public final IStatus save() {
		this.saveError= false;
		try {
			doSave();
		}
		finally {
			if (!this.saveError) {
				setDirty(false);
			}
		}
		
		final IStatus status= this.aggregateStatus.getValue();
		if ((status == null || status.isOK()) && this.saveError) {
			return new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID,
					"Failed to save all changes." );
		}
		return status;
	}
	
	protected void doSave() {
	}
	
	protected void logSaveError(final CoreException e) {
		this.saveError= true;
		StatusManager.getManager().handle(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID,
				"An error occurred when saving settings for the selected breakpoint." ));
	}
	
}
