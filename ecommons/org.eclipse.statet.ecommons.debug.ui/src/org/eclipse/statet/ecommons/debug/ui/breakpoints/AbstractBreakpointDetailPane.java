/*=============================================================================#
 # Copyright (c) 2009, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.breakpoints;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.ui.IDetailPane3;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPartSite;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


/**
 * Common detail pane function.
 */
public abstract class AbstractBreakpointDetailPane implements IDetailPane3 {
	
	
	private final String id;
	private final String name;
	private final String description;
	
	private AbstractBreakpointDetailEditor editor;
	private IWorkbenchPartSite site;
	
	// property listeners
	private final CopyOnWriteIdentityListSet<IPropertyListener> propertyListeners= new CopyOnWriteIdentityListSet<>();
	
	private Composite composite;
	
	
	/**
	 * Constructs a detail pane.
	 * 
	 * @param id detail pane ID
	 * @param name detail pane name
	 * @param description detail pane description
	 */
	public AbstractBreakpointDetailPane(final String id, final String name, final String description) {
		this.id= id;
		this.name= name;
		this.description= description;
	}
	
	@Override
	public void init(final IWorkbenchPartSite partSite) {
		this.site= partSite;
	}
	
	@Override
	public void dispose() {
		this.editor= null;
		this.site= null;
		this.propertyListeners.clear();
		this.composite.dispose();
	}
	
	
	@Override
	public String getID() {
		return this.id;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public String getDescription() {
		return this.description;
	}
	
	
	@Override
	public void addPropertyListener(final IPropertyListener listener) {
		this.propertyListeners.add(listener);
	}
	
	@Override
	public void removePropertyListener(final IPropertyListener listener) {
		this.propertyListeners.remove(listener);
	}
	
	protected CopyOnWriteIdentityListSet<IPropertyListener> getPropertyListeners() {
		return this.propertyListeners;
	}
	
	/**
	 * Fires a property change to all listeners.
	 * 
	 * @param property the property
	 */
	protected void firePropertyChange(final int property) {
		for (final IPropertyListener listener : this.propertyListeners) {
			listener.propertyChanged(this, property);
		}
	}
	
	@Override
	public Control createControl(final Composite parent) {
		this.composite= new Composite(parent, SWT.NONE);
		this.composite.setLayout(LayoutUtils.newContentGrid());
		this.composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		this.editor= createEditor(this.composite);
		final Control editorControl= this.editor.createControl(this.composite);
		editorControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		return this.composite;
	}
	
	/**
	 * Creates the detail pane specific editor.
	 * 
	 * @param parent parent composite
	 * @return editor
	 */
	protected abstract AbstractBreakpointDetailEditor createEditor(Composite parent);
	
	/**
	 * Returns the editor associated with this detail pane.
	 * 
	 * @return editor
	 */
	protected AbstractBreakpointDetailEditor getEditor() {
		return this.editor;
	}
	
	@Override
	public boolean setFocus() {
		return this.composite.setFocus();
	}
	
	
	@Override
	public boolean isDirty() {
		return (this.editor != null && this.editor.isDirty());
	}
	
	@Override
	public void doSaveAs() {
		// do nothing
	}
	
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	@Override
	public boolean isSaveOnCloseNeeded() {
		return isDirty() && this.editor.getStatus().isOK();
	}
	
	@Override
	public void doSave(final IProgressMonitor monitor) {
		final IStatusLineManager statusLine= getStatusLine();
		if (statusLine != null) {
			statusLine.setErrorMessage(null);
		}
		final IStatus status= this.editor.save();
		if (status != null && !status.isOK()) {
			if (statusLine != null) {
				statusLine.setErrorMessage(status.getMessage());
				Display.getCurrent().beep();
			}
		}
	}
	
	private IStatusLineManager getStatusLine() {
		// we want to show messages globally hence we
		// have to go through the active part
		if (this.site instanceof IViewSite) {
			final IViewSite site= (IViewSite) this.site;
//			IWorkbenchPage page= site.getPage();
//			IWorkbenchPart activePart= page.getActivePart();
//			
//			if (activePart instanceof IViewPart) {
//				IViewPart activeViewPart= (IViewPart) activePart;
//				IViewSite activeViewSite= activeViewPart.getViewSite();
//				return activeViewSite.getActionBars().getStatusLineManager();
//			}
//			
//			if (activePart instanceof IEditorPart) {
//				IEditorPart activeEditorPart= (IEditorPart) activePart;
//				IEditorActionBarContributor contributor= activeEditorPart.getEditorSite().getActionBarContributor();
//				if (contributor instanceof EditorActionBarContributor)
//					return ((EditorActionBarContributor) contributor).getActionBars().getStatusLineManager();
//			}
			// no active part
			return site.getActionBars().getStatusLineManager();
		}
		return null;
	}
	
	@Override
	public void display(final IStructuredSelection selection) {
		// clear status line
		final IStatusLineManager statusLine= getStatusLine();
		if (statusLine != null) {
			statusLine.setErrorMessage(null);
		}
		final AbstractBreakpointDetailEditor editor= getEditor();
		Object input= null;
		if (selection != null && selection.size() == 1) {
			input= selection.getFirstElement();
			// update even if the same in case attributes have changed
		}
		
		editor.setInput(input);
	}
	
}
