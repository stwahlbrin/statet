/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config.actions;

import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;
import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;
import org.eclipse.statet.ecommons.ui.util.MessageUtils;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


@NonNullByDefault
public class RunActiveConfigScopeHandler<TElement> extends AbstractScopeHandler
		implements LaunchConfigManager.Listener {
	
	protected static final byte UNCHANGED= 0;
	protected static final byte CHANGED_NULL= 1;
	protected static final byte CHANGED_OK= 2;
	
	
	private final ActionUtil<TElement> util;
	
	private final ImIdentitySet<String> launchFlags;
	
	private @Nullable LaunchConfigManager<TElement> manager;
	
	private @Nullable String tooltip;
	
	
	public RunActiveConfigScopeHandler(final Object scope, final @Nullable String commandId,
			final ActionUtil<TElement> util,
			final ImIdentitySet<String> launchFlags) {
		super(scope, commandId);
		
		this.util= util;
		this.launchFlags= launchFlags;
	}
	
	
	@Override
	public void dispose() {
		updateManager((LaunchConfigManager<TElement>) null);
	}
	
	
	protected IWorkbenchWindow getWindow() {
		return (IWorkbenchWindow) getScope();
	}
	
	protected ActionUtil<TElement> getUtil() {
		return this.util;
	}
	
	protected ImIdentitySet<String> getLaunchFlags() {
		return this.launchFlags;
	}
	
	protected synchronized byte updateManager(final @Nullable LaunchConfigManager<TElement> manager) {
		if (manager == this.manager) {
			return UNCHANGED;
		}
		
		if (this.manager != null) {
			this.manager.removeListener(this);
		}
		this.manager= manager;
		if (manager != null) {
			manager.addListener(this);
			updateInfo(manager, manager.getActiveConfig());
		}
		else {
			updateInfo(null, null);
		}
		return ((manager != null) ? CHANGED_OK : CHANGED_NULL);
	}
	
	private void updateInfo(final @Nullable LaunchConfigManager<TElement> manager,
			final @Nullable ILaunchConfiguration config) {
		if (manager != null && config != null) {
			this.tooltip= MessageUtils.escapeForTooltip(manager.getLabel(
					config, this.launchFlags, true ));
		}
		else {
			this.tooltip= null;
		}
	}
	
	@Override
	public void availableConfigChanged(final ImList<ILaunchConfiguration> configs) {
	}
	
	@Override
	public void activeConfigChanged(final ILaunchConfiguration config) {
		synchronized (this) {
			updateInfo(this.manager, config);
		}
		
		refreshElements();
	}
	
	
	protected synchronized byte updateManager(final IEvaluationContext context) {
		final IWorkbenchWindow window= getWindow();
		final @Nullable TElement element= this.util.getLaunchElement(window);
		
		LaunchConfigManager<TElement> manager= null;
		if (element != null) {
			manager= this.util.getManager(window, element);
		}
		
		return updateManager(manager);
	}
	
	@Override
	public void setEnabled(final IEvaluationContext context) {
		final byte changed= updateManager(context);
		if (changed != 0) {
			setBaseEnabled((changed > CHANGED_NULL));
			refreshElements();
		}
	}
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
		try {
			element.setTooltip(this.tooltip);
		}
		finally {
			WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
		}
	}
	
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event, final IEvaluationContext context)
			throws ExecutionException {
		final IWorkbenchWindow window= getWindow();
		final @Nullable TElement element= this.util.getLaunchElement(window);
		if (element == null) {
			return null;
		}
		final LaunchConfigManager<TElement> manager= this.manager;
		if (manager == null) {
			return null;
		}
		
		this.util.launchActive(window, manager, element, getLaunchFlags());
		return null;
	}
	
}
