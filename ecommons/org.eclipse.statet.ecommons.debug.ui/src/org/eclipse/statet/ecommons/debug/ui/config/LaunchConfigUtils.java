/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config;

import java.util.Comparator;

import com.ibm.icu.text.Collator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.RefreshTab;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI;
import org.eclipse.statet.internal.ecommons.debug.ui.Messages;


/**
 * Methods for common task when working with launch configurations and processes
 */
@NonNullByDefault
public class LaunchConfigUtils {
	
	
	public static class LaunchConfigurationComparator implements Comparator<ILaunchConfiguration> {
		
		private final Collator collator= Collator.getInstance();
		
		@Override
		public int compare(final ILaunchConfiguration c1, final ILaunchConfiguration c2) {
			return this.collator.compare(c1.getName(), c2.getName());
		}
		
	}
	
	public static boolean isActiveTabGroup(final ILaunchConfigurationDialog dialog,
			final ILaunchConfigurationTab tab) {
		final ILaunchConfigurationTab[] tabs= dialog.getTabs();
		if (tabs != null) {
			for (int i= 0; i < tabs.length; i++) {
				if (tabs[i] == tab) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * Refreshes resources as specified by a launch configuration, when
	 * an associated process terminates.
	 */
	private static class BackgroundResourceRefresher implements IDebugEventSetListener  {
		
		
		private final ILaunchConfiguration configuration;
		private @Nullable IProcess process;
		
		
		public BackgroundResourceRefresher(final ILaunchConfiguration configuration, final IProcess process) {
			this.configuration= configuration;
			this.process= process;
			
			initialize();
		}
		
		/**
		 * If the process has already terminated, resource refreshing is scheduled
		 * immediately. Otherwise, refreshing is done when the process terminates.
		 */
		private synchronized void initialize() {
			DebugPlugin.getDefault().addDebugEventListener(this);
			final IProcess process= this.process;
			if (process != null && process.isTerminated()) {
				sheduleRefresh();
			}
		}
		
		@Override
		public void handleDebugEvents(final DebugEvent[] events) {
			for (int i= 0; i < events.length; i++) {
				final DebugEvent event= events[i];
				if (event.getSource() == this.process && event.getKind() == DebugEvent.TERMINATE) {
					sheduleRefresh();
					return;
				}
			}
		}
		
		/**
		 * Submits a job to do the refresh
		 */
		protected synchronized void sheduleRefresh() {
			if (this.process != null) {
				DebugPlugin.getDefault().removeDebugEventListener(this);
				this.process= null;
				final Job job= new Job(Messages.BackgroundResourceRefresher_Job_name) {
					@Override
					public IStatus run(final IProgressMonitor monitor) {
						try {
							RefreshTab.refreshResources(BackgroundResourceRefresher.this.configuration, monitor);
						}
						catch (final CoreException e) {
							StatusManager.getManager().handle(new Status(
									IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID, 0,
									NLS.bind("An error occurred when refreshing resources for launch configuration ''{0}''.", BackgroundResourceRefresher.this.configuration.getName()),
									e ));
							return e.getStatus();
						}
						return Status.OK_STATUS;
					}
				};
				job.schedule();
			}
		}
	}
	
	
	/**
	 * Manages resource refresh according to the settings in launch configuration.
	 */
	public static void launchResourceRefresh(final ILaunchConfiguration configuration,
			final IProcess process, final IProgressMonitor monitor) throws CoreException {
		if (CommonTab.isLaunchInBackground(configuration)) {
			// refresh resources after process finishes
			if (RefreshTab.getRefreshScope(configuration) != null) {
				new BackgroundResourceRefresher(configuration, process);
			}
		} else {
			// wait for process to exit
			while (!process.isTerminated()) {
				try {
					if (monitor.isCanceled()) {
						process.terminate();
						break;
					}
					Thread.sleep(50);
				}
				catch (final InterruptedException e) {
					// continue loop, monitor and process is checked
				}
			}
			
			// refresh resources
			RefreshTab.refreshResources(configuration, monitor);
		}
	}
	
	
	private LaunchConfigUtils() {}
	
}
