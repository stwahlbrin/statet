/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.net.resourcemapping.core;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.IPath;


/**
 * Specifies a mapping of a file path from a remote system
 * accessible at the local system
 */
public interface IResourceMapping {
	
	/**
	 * Path from viewpoint of the local system
	 * 
	 * @return EFS resource
	 */
	IFileStore getFileStore();
	
	/**
	 * Hostname (name or IP) of the remote system
	 * 
	 * @return the hostname
	 */
	String getHost();
	
	/**
	 * Path from viewpoint of the remote system
	 * 
	 * @return path object
	 */
	IPath getRemotePath();
	
}
