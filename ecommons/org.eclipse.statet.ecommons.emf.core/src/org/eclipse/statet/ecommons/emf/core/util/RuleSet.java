/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.core.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;


public class RuleSet {
	
	
	public static final String PARENT_FEATURES_ID_SUFFIX= ".parent.features"; //$NON-NLS-1$
	public static final String DISJOINT_FEATURES_ID= "disjoint.features"; //$NON-NLS-1$
	
	
	public Object get(final EObject obj, final EStructuralFeature eFeature, final String ruleType) {
		return null;
	}
	
}
