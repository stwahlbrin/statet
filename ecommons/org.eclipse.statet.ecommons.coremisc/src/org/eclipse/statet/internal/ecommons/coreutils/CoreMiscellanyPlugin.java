/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.coreutils;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;


/**
 * The activator class controls the plug-in life cycle
 */
public final class CoreMiscellanyPlugin extends Plugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ecommons.coremisc"; //$NON-NLS-1$
	
	
	private static CoreMiscellanyPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static CoreMiscellanyPlugin getInstance() {
		return instance;
	}
	
	
	public static void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private final List<Disposable> disposables= new ArrayList<>();
	
	private SettingsChangeNotifier settingsNotifier;
	
	
	/**
	 * The default constructor
	 */
	public CoreMiscellanyPlugin() {
		instance= this;
	}
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		
		synchronized (this) {
			this.started= true;
			
			this.settingsNotifier= new SettingsChangeNotifier();
			addStoppingListener(this.settingsNotifier);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
			
			try {
				for (final Disposable listener : this.disposables) {
					listener.dispose();
				}
			}
			finally {
				this.disposables.clear();
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public void addStoppingListener(final Disposable listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.disposables.add(listener);
		}
	}
	
	public void removeStoppingListener(final Disposable listener) {
		this.disposables.remove(listener);
	}
	
	public SettingsChangeNotifier getSettingsChangeNotifier() {
		return this.settingsNotifier;
	}
	
}
