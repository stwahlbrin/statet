/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.ecommons.models.AbstractSettingsModelObject;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;


/**
 * Extends settings object with preference related features
 * (e.g. load, save and listen to changes).
 */
public abstract class AbstractPreferencesModelObject extends AbstractSettingsModelObject {
	
	
	protected AbstractPreferencesModelObject() {
	}
	
	
	/**
	 * Returns the qualifier of all nodes, this model have preferences from.
	 * @return
	 */
	public abstract String[] getNodeQualifiers();
	
	/**
	 * Loads the default settings.
	 */
	public abstract void loadDefaults();
	
	/**
	 * Loads the settings from preferences.
	 */
	public abstract void load(PreferenceAccess prefs);
	
	/**
	 * Adds all preferences (definition and settings) to the map.
	 * 
	 * @param map
	 * @return the same map.
	 */
	public abstract Map<Preference<?>, Object> deliverToPreferencesMap(Map<Preference<?>, Object> map);
	
	/**
	 * Return map with all preferences (definition and settings)
	 * 
	 * @return new created map.
	 */
	public Map<Preference<?>, Object> toPreferencesMap() {
		return deliverToPreferencesMap(new HashMap<Preference<?>, Object>());
	}
	
}
