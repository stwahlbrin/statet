/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ecommons.preferences.Messages;


@NonNullByDefault
public class SettingsChangeNotifier implements ISchedulingRule, Disposable {
	
	
	public static interface ChangeListener {
		
		/**
		 * Is called inside a job. So, a bit longer tasks are allowed directly,
		 * but not UI tasks.
		 * 
		 * @param groupIds set of ids of changed preference groups
		 */
		public void settingsChanged(Set<String> groupIds);
		
	}
	
	public static interface ManageListener {
		
		public void beforeSettingsChangeNotification(Set<String> groupIds);
		
		public void afterSettingsChangeNotification(Set<String> groupIds);
		
	}
	
	
	private class NotifyJob extends Job {
		
		private final String source;
		private final Set<String> changedGroupIds= new HashSet<>();
		
		public NotifyJob(final String source) {
			super(Messages.SettingsChangeNotifier_Job_title);
			setPriority(Job.SHORT);
			setRule(SettingsChangeNotifier.this);
			this.source= source;
		}
		
		public void addGroups(final ImCollection<String> groupIds) {
			this.changedGroupIds.addAll(groupIds);
		}
		
		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			ImList<ManageListener> managers;
			ImList<ChangeListener> listeners;
			synchronized (SettingsChangeNotifier.this) {
				managers= SettingsChangeNotifier.this.managers.toList();
				listeners= SettingsChangeNotifier.this.listeners.toList();
				SettingsChangeNotifier.this.pendingJobs.remove(this.source);
			}
			monitor.beginTask(Messages.SettingsChangeNotifier_Task_name, managers.size() * 5 + listeners.size() * 5);
			for (final Object obj : managers) {
				((ManageListener) obj).beforeSettingsChangeNotification(this.changedGroupIds);
				monitor.worked(3);
			}
			for (final Object obj : listeners) {
				((ChangeListener) obj).settingsChanged(this.changedGroupIds);
				monitor.worked(5);
			}
			for (final Object obj : managers) {
				((ManageListener) obj).afterSettingsChangeNotification(this.changedGroupIds);
				monitor.worked(2);
			}
			return Status.OK_STATUS;
		}
	}
	
	private volatile boolean isDisposed;
	
	private final CopyOnWriteIdentityListSet<ManageListener> managers= new CopyOnWriteIdentityListSet<>();
	private final CopyOnWriteIdentityListSet<ChangeListener> listeners= new CopyOnWriteIdentityListSet<>();
	private final Map<String, NotifyJob> pendingJobs= new HashMap<>();
	
	
	public SettingsChangeNotifier() {
		this.isDisposed= false;
	}
	
	
	public @Nullable Job getNotifyJob(@Nullable String source, final ImCollection<String> groupIds) {
		if (this.isDisposed) {
			return null;
		}
		if (source == null) {
			source= "direct"; //$NON-NLS-1$
		}
		synchronized (SettingsChangeNotifier.this) {
			NotifyJob job= this.pendingJobs.get(source);
			if (job != null) {
				job.addGroups(groupIds);
				return null;
			}
			job= new NotifyJob(source);
			this.pendingJobs.put(source, job);
			job.addGroups(groupIds);
			return job;
		}
	}
	
	@Deprecated
	public @Nullable Job getNotifyJob(@Nullable final String source, final @NonNull String[] groupIds) {
		return getNotifyJob(source, ImCollections.newList(groupIds));
	}
	
	@Override
	public boolean contains(final ISchedulingRule rule) {
		return (rule == this);
	}
	@Override
	public boolean isConflicting(final ISchedulingRule rule) {
		return (rule == this);
	}
	
	public void addChangeListener(final ChangeListener listener) {
		if (!this.isDisposed) {
			this.listeners.add(listener);
		}
	}
	
	public void removeChangeListener(final ChangeListener listener) {
		if (!this.isDisposed) {
			this.listeners.remove(listener);
		}
	}
	
	public void addManageListener(final ManageListener listener) {
		if (!this.isDisposed) {
			this.managers.add(listener);
		}
	}
	
	public void removeManageListener(final ManageListener listener) {
		if (!this.isDisposed) {
			this.managers.remove(listener);
		}
	}
	
	@Override
	public void dispose() {
		this.isDisposed= true;
		this.managers.clear();
		this.listeners.clear();
	}
	
}
