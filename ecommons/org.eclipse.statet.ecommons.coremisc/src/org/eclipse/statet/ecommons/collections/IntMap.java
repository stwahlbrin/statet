/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.collections;

import java.util.Map;


/**
 * An object that maps integer keys to values. This interface and its implementations provide
 * methods with primitive integer arguments.
 * 
 * @param <V> type of the values
 * @since 1.0
 */
public interface IntMap<V> extends Map<Integer, V> {
	
	interface IntEntry<V> {
		
		int getIntKey();
		V getValue();
		
	}
	
	boolean containsKey(int key);
	V get(int key);
	
	V put(int key, V value);
	
	/*Set<IntEntry<V>> entryIntSet();*/
	
}
