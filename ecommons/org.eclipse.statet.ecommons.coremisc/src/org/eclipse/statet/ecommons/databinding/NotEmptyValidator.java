/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.osgi.util.NLS;


/**
 * Simple validator to make sure the string value is not empty
 */
public class NotEmptyValidator implements IValidator<Object> {
	
	
	private final String message;
	
	private final IValidator nested;
	
	
	public NotEmptyValidator(final String message) {
		this.message= message;
		this.nested= null;
	}
	
	public NotEmptyValidator(final String label, final IValidator validator) {
		this.message= NLS.bind("{0} is missing.", label);
		this.nested= validator;
	}
	
	
	@Override
	public IStatus validate(final Object value) {
		if (value instanceof String) {
			final String s= ((String) value).trim();
			if (s.length() > 0) {
				return (this.nested != null) ? this.nested.validate(value) : ValidationStatus.ok();
			}
		}
		return ValidationStatus.error(this.message);
	}
	
}
