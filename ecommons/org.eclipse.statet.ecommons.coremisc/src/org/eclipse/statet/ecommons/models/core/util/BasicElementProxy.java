/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.models.core.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.runtime.IAdaptable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicElementProxy implements ElementProxy {
	
	
	private final IAdaptable element;
	
	
	public BasicElementProxy(final IAdaptable element) {
		this.element= nonNullAssert(element);
	}
	
	
	@Override
	public IAdaptable getElement() {
		return this.element;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return this.element.getAdapter(adapterType);
	}
	
	
	@Override
	public int hashCode() {
		return this.element.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		return (obj instanceof BasicElementProxy
				&& this.element.equals(((BasicElementProxy)obj).element) );
	}
	
	
	@Override
	public String toString() {
		return getClass().getCanonicalName() + ": " + this.element.toString(); //$NON-NLS-1$
	}
	
}
