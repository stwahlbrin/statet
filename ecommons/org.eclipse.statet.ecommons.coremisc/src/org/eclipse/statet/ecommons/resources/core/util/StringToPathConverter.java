/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.resources.core.util;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.core.conversion.ClassTypedConverter;


@NonNullByDefault
public class StringToPathConverter implements ClassTypedConverter<String, IPath> {
	
	
	public StringToPathConverter() {
	}
	
	
	@Override
	public Class<String> getFromType() {
		return String.class;
	}
	
	@Override
	public Class<IPath> getToType() {
		return IPath.class;
	}
	
	
	@Override
	public IPath convert(final String fromObject) {
		final String s= fromObject.trim();
		return new Path(s);
	}
	
}
