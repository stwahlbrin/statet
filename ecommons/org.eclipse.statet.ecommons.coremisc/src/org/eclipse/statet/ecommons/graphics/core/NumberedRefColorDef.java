/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.graphics.core;


public class NumberedRefColorDef extends ColorRefDef {
	
	
	private final int fNumber;
	
	
	public NumberedRefColorDef(final int number, final ColorDef ref) {
		super(ref);
		fNumber = number;
	}
	
	
	public int getNumber() {
		return fNumber;
	}
	
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof NumberedRefColorDef)) {
			return false;
		}
		final NumberedRefColorDef other = (NumberedRefColorDef) obj;
		return (fNumber == other.fNumber && getRef().equals(other.getRef()));
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Id: "); //$NON-NLS-1$
		sb.append(fNumber);
		sb.append(" / "); //$NON-NLS-1$
		sb.append(getRef().toString());
		return sb.toString();
	}
	
}
