/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.graphics.core;


public class NamedColorDef extends ColorDef {
	
	
	private final String fName;
	
	
	public NamedColorDef(final String name, final int red, final int green, final int blue) {
		super(red, green, blue);
		fName = name;
	}
	
	
	public String getName() {
		return fName;
	}
	
	
	@Override
	public String getType() {
		return "rgb-"; //$NON-NLS-1$
	}
	
	@Override
	public int hashCode() {
		return fName.hashCode() + super.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof NamedColorDef)) {
			return false;
		}
		final NamedColorDef other = (NamedColorDef) obj;
		return (fName.equals(other.fName) && equalsRGB(other));
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(18);
		sb.append(fName);
		sb.append(" (#"); //$NON-NLS-1$
		printRGBHex(sb);
		sb.append(')');
		return sb.toString();
	}
	
}