/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.commands.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.IHandler2;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractHandlerCollection<TRecord extends AbstractHandlerCollection.CommandRecord>
		implements HandlerCollection {
	
	
	public static class CommandRecord {
		
		final IHandler2 handler;
		
		private final int flags;
		
		
		public CommandRecord(final IHandler2 handler, final int flags) {
			this.handler= handler;
			this.flags= flags;
		}
		
		
		public IHandler2 getHandler() {
			return this.handler;
		}
		
		public int getFlags() {
			return this.flags;
		}
		
	}
	
	
	private final Map<String, TRecord> commandRecords= new HashMap<>();
	
	private boolean isDisposed;
	
	
	public AbstractHandlerCollection() {
	}
	
	
	public TRecord addCommandRecord(final String commandId, final IHandler2 handler, final int flags) {
		if (commandId == null || handler == null) {
			throw new NullPointerException();
		}
		if (this.isDisposed) {
			throw new IllegalStateException();
		}
		final TRecord record= createCommandRecord(commandId, handler, flags);
		this.commandRecords.put(commandId, record);
		return record;
	}
	
	protected abstract TRecord createCommandRecord(String commandId, IHandler2 handler, int flags);
	
	protected Collection<TRecord> getCommandRecords() {
		return this.commandRecords.values();
	}
	
	
	@Override
	public void add(final String commandId, final IHandler2 handler, final int flags) {
		addCommandRecord(commandId, handler, flags);
	}
	
	@Override
	public void add(final String commandId, final IHandler2 handler) {
		addCommandRecord(commandId, handler, 0);
	}
	
	@Override
	public @Nullable IHandler2 get(final String commandId) {
		final var record= this.commandRecords.get(commandId);
		return (record != null) ? record.handler : null;
	}
	
	@Override
	public void update(final @Nullable Object evaluationContext) {
		for (final var record : this.commandRecords.values()) {
			record.handler.setEnabled(evaluationContext);
		}
	}
	
	@Override
	public void update(final @Nullable Object evaluationContext, final int flags) {
		for (final var record : this.commandRecords.values()) {
			if ((record.getFlags() & flags) != 0) {
				record.handler.setEnabled(evaluationContext);
			}
		}
	}
	
	
	@Override
	public void dispose() {
		for (final var record : this.commandRecords.values()) {
			record.handler.dispose();
		}
		this.commandRecords.clear();
	}
	
}
