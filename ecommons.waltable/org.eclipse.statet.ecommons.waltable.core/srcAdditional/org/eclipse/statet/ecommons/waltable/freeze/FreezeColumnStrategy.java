/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;


public class FreezeColumnStrategy implements IFreezeCoordinatesProvider {
	
	
	private final FreezeLayer freezeLayer;
	
	private final long columnPosition;
	
	
	public FreezeColumnStrategy(final FreezeLayer freezeLayer, final long columnPosition) {
		this.freezeLayer= freezeLayer;
		this.columnPosition= columnPosition;
	}
	
	
	@Override
	public PositionCoordinate getTopLeftPosition() {
		return new PositionCoordinate(this.freezeLayer, 0, -1);
	}
	
	@Override
	public PositionCoordinate getBottomRightPosition() {
		return new PositionCoordinate(this.freezeLayer, this.columnPosition, -1);
	}
	
}
