/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.export.config;

import org.eclipse.swt.SWT;

import org.eclipse.statet.ecommons.waltable.config.IConfiguration;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.export.ExportConfigAttributes;
import org.eclipse.statet.ecommons.waltable.export.excel.DefaultExportFormatter;
import org.eclipse.statet.ecommons.waltable.export.excel.ExcelExporter;
import org.eclipse.statet.ecommons.waltable.export.ui.action.ExportAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.KeyEventMatcher;


public class DefaultExportBindings implements IConfiguration {

	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.MOD1, 'e'), new ExportAction());
	}

	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		configRegistry.registerAttribute(
				ExportConfigAttributes.EXPORTER, new ExcelExporter());
		configRegistry.registerAttribute(
				ExportConfigAttributes.EXPORT_FORMATTER, new DefaultExportFormatter());
		configRegistry.registerAttribute(
				ExportConfigAttributes.DATE_FORMAT, "m/d/yy h:mm"); //$NON-NLS-1$
	}

	@Override
	public void configureLayer(final Layer layer) {
	}

}
