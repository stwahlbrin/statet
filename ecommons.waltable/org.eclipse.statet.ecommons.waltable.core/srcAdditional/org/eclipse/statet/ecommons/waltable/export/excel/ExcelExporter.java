/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.export.excel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.RegistryStyle;
import org.eclipse.statet.ecommons.waltable.export.FileOutputStreamProvider;
import org.eclipse.statet.ecommons.waltable.export.ILayerExporter;
import org.eclipse.statet.ecommons.waltable.export.IOutputStreamProvider;
import org.eclipse.statet.internal.ecommons.waltable.WaLTablePlugin;


/**
 * This class is used to export a NatTable to an Excel spreadsheet by using a 
 * XML format.
 */
public class ExcelExporter implements ILayerExporter {

	private static final String EXCEL_HEADER_FILE= "excelExportHeader.txt"; //$NON-NLS-1$
	
	/**
	 * The IOutputStreamProvider that is used to create new OutputStreams on
	 * beginning new export operations.
	 */
	private final IOutputStreamProvider outputStreamProvider;

	/**
	 * Creates a new ExcelExporter using a FileOutputStreamProvider with default values.
	 */
	public ExcelExporter() {
		this(new FileOutputStreamProvider("table_export.xls", new String[] { "Excel Workbok (*.xls)" }, new String[] { "*.xls" })); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
	
	/**
	 * Creates a new ExcelExporter that uses the given IOutputStreamProvider for retrieving
	 * the OutputStream to write the export to.
	 * @param outputStreamProvider The IOutputStreamProvider that is used to retrieve the 
	 * 			OutputStream to write the export to.
	 */
	public ExcelExporter(final IOutputStreamProvider outputStreamProvider) {
		this.outputStreamProvider= outputStreamProvider;
	}
	
	@Override
	public OutputStream getOutputStream(final Shell shell) {
		return this.outputStreamProvider.getOutputStream(shell);
	}
	
	@Override
	public void exportBegin(final OutputStream outputStream) throws IOException {
	}

	@Override
	public void exportEnd(final OutputStream outputStream) throws IOException {
	}

	@Override
	public void exportLayerBegin(final OutputStream outputStream, final String layerName) throws IOException {
		writeHeader(outputStream);
		outputStream.write(asBytes("<body><table border='1'>")); //$NON-NLS-1$
	}

	/**
	 * Writes the Excel header informations that are stored locally in the package
	 * structure.
	 * @throws IOException if an I/O error occurs on closing the stream to
	 * 			the header content file
	 */
	private void writeHeader(final OutputStream outputStream) throws IOException {
		try (InputStream headerStream= this.getClass().getResourceAsStream(EXCEL_HEADER_FILE)) {
			int c;
			while ((c= headerStream.read()) != -1) {
				outputStream.write(c);
			}
		} catch (final Exception e) {
			WaLTablePlugin.log(new Status(IStatus.ERROR, WaLTablePlugin.BUNDLE_ID,
					"Excel Exporter failed: " + e.getMessage(), e)); //$NON-NLS-1$
		}
	}

	@Override
	public void exportLayerEnd(final OutputStream outputStream, final String layerName) throws IOException {
		outputStream.write(asBytes("</table></body></html>")); //$NON-NLS-1$
	}

	@Override
	public void exportRowBegin(final OutputStream outputStream, final long rowPosition) throws IOException {
		outputStream.write(asBytes("<tr>\n")); //$NON-NLS-1$
	}

	@Override
	public void exportRowEnd(final OutputStream outputStream, final long rowPosition) throws IOException {
		outputStream.write(asBytes("</tr>\n")); //$NON-NLS-1$
	}

	@Override
	public void exportCell(final OutputStream outputStream, final Object exportDisplayValue, final LayerCell cell, final ConfigRegistry configRegistry) throws IOException {
		final var cellStyle= new RegistryStyle(configRegistry, CellConfigAttributes.CELL_STYLE,
				DisplayMode.NORMAL, cell.getLabels().getLabels() );
		final Color fg= cellStyle.getAttributeValue(CellStyling.FOREGROUND_COLOR);
		final Color bg= cellStyle.getAttributeValue(CellStyling.BACKGROUND_COLOR);
		final Font font= cellStyle.getAttributeValue(CellStyling.FONT);

		String htmlAttributes= String.format("style='color: %s; background-color: %s; %s;'", //$NON-NLS-1$
		                     getColorInCSSFormat(fg),
		                     getColorInCSSFormat(bg),
		                     getFontInCSSFormat(font));
		
		String htmlText= exportDisplayValue != null ? exportDisplayValue.toString() : ""; //$NON-NLS-1$
		
		if (htmlText.startsWith(" ")) { //$NON-NLS-1$
			htmlAttributes+= " x:str=\"'" + htmlText + "\";"; //$NON-NLS-1$ //$NON-NLS-2$
			htmlText= htmlText.replaceFirst("^(\\ *)", "<span style='mso-spacerun:yes'>$1</span>"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		outputStream.write(asBytes(String.format("\t<td %s>%s</td>\n", htmlAttributes, htmlText))); //$NON-NLS-1$
	}

	private byte[] asBytes(final String string) {
		return string.getBytes();
	}
	
	private String getFontInCSSFormat(final Font font) {
		final FontData fontData= font.getFontData()[0];
		final String fontName= fontData.getName();
		final int fontStyle= fontData.getStyle();
		final String HTML_STYLES[]= new String[] { "NORMAL", "BOLD", "ITALIC" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		return String.format("font: %s; font-family: %s", //$NON-NLS-1$
		                     fontStyle <= 2 ? HTML_STYLES[fontStyle] : HTML_STYLES[0],
		                     fontName);
	}

	private String getColorInCSSFormat(final Color color) {
		return String.format("rgb(%d,%d,%d)", //$NON-NLS-1$
		                     Integer.valueOf(color.getRed()),
		                     Integer.valueOf(color.getGreen()),
		                     Integer.valueOf(color.getBlue()));
	}

	@Override
	public Object getResult() {
		return this.outputStreamProvider.getResult();
	}
	
}
