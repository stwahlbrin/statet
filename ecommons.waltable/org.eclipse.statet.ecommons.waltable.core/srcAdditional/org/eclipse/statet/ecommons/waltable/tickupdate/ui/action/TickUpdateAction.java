/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.tickupdate.ui.action;

import org.eclipse.swt.events.KeyEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.tickupdate.TickUpdateCommand;
import org.eclipse.statet.ecommons.waltable.ui.action.IKeyAction;


/**
 * {@link IKeyAction} that will execute the {@link TickUpdateCommand}
 * with the additional information if the update increments or decrements
 * the current value.
 */
public class TickUpdateAction implements IKeyAction {

	/**
	 * Flag to determine whether the current value in the data model
	 * should be incremented or decremented. 
	 */
	private final boolean increment;

	/**
	 * @param increment Flag to determine whether the current value in the data model
	 * 			should be incremented or decremented. 
	 */
	public TickUpdateAction(final boolean increment) {
		this.increment= increment;
	}

	@Override
	public void run(final NatTable natTable, final KeyEvent event) {
		natTable.doCommand(new TickUpdateCommand(natTable.getConfigRegistry(), this.increment));
	}

}
