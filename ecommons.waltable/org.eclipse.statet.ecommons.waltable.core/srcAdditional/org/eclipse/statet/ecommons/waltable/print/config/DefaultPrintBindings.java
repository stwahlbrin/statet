/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.print.config;

import org.eclipse.swt.SWT;

import org.eclipse.statet.ecommons.waltable.config.AbstractUiBindingConfiguration;
import org.eclipse.statet.ecommons.waltable.print.ui.action.PrintAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.KeyEventMatcher;


/**
 * Simple UI binding configuration that adds the binding for [Ctrl] + [P] to trigger
 * the PrintAction.
 * 
 * @see PrintAction
 */
public class DefaultPrintBindings extends AbstractUiBindingConfiguration {

	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.MOD1, 'p'), new PrintAction());
	}

}
