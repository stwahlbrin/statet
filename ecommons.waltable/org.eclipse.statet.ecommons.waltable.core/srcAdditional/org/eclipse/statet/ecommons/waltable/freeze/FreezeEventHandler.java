/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.util.Collection;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerListener;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralChangeEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralDiff;


@NonNullByDefault
public class FreezeEventHandler implements LayerListener {
	
	
	private final FreezeLayer freezeLayer;
	
	
	public FreezeEventHandler(final FreezeLayer freezeLayer) {
		this.freezeLayer= freezeLayer;
	}
	
	
	@Override
	public void handleLayerEvent(final LayerEvent event) {
		if (event instanceof StructuralChangeEvent) {
			final var changeEvent= (StructuralChangeEvent)event;
			final PositionCoordinate topLeftPosition= this.freezeLayer.getTopLeftPosition();
			final PositionCoordinate bottomRightPosition= this.freezeLayer.getBottomRightPosition();
			
			// The handling of diffs have to be in sync with ViewportLayerDim#handleStructuralChanges
			final Collection<StructuralDiff> columnDiffs= changeEvent.getDiffs(HORIZONTAL);
			if (columnDiffs != null) {
				int leftOffset= 0;
				int rightOffset= 0;
				int freezeMove= 0; // 0= unset, 1 == true, -1 == false
				
				for (final StructuralDiff diff : columnDiffs) {
					final long start= diff.getBeforePositionRange().start;
					switch (diff.getDiffType()) {
					case ADD:
						if (start < topLeftPosition.columnPosition) {
							leftOffset+= diff.getAfterPositionRange().size();
						}
						if (start <= bottomRightPosition.columnPosition
								|| (freezeMove == 1 && start == bottomRightPosition.columnPosition + 1) ) {
							rightOffset+= diff.getAfterPositionRange().size();
						}
						continue;
					case DELETE:
						if (start < topLeftPosition.columnPosition) {
							leftOffset-= Math.min(diff.getBeforePositionRange().end, topLeftPosition.columnPosition + 1) - start;
						}
						if (start <= bottomRightPosition.columnPosition) {
							rightOffset-= Math.min(diff.getBeforePositionRange().end, bottomRightPosition.columnPosition + 1) - start;
							if (freezeMove == 0) {
								freezeMove= 1;
							}
						}
						else {
							freezeMove= -1;
						}
						continue;
					default:
						continue;
					}
				}
				
				topLeftPosition.columnPosition+= leftOffset;
				bottomRightPosition.columnPosition+= rightOffset;
			}
			
			final Collection<StructuralDiff> rowDiffs= changeEvent.getDiffs(VERTICAL);
			if (rowDiffs != null) {
				int leftOffset= 0;
				int rightOffset= 0;
				int freezeMove= 0; // 0= unset, 1 == true, -1 == false
				
				for (final StructuralDiff diff : rowDiffs) {
					final long start= diff.getBeforePositionRange().start;
					switch (diff.getDiffType()) {
					case ADD:
						if (start < topLeftPosition.rowPosition) {
							leftOffset+= diff.getAfterPositionRange().size();
						}
						if (start <= bottomRightPosition.rowPosition
								|| (freezeMove == 1 && start == bottomRightPosition.rowPosition + 1) ) {
							rightOffset+= diff.getAfterPositionRange().size();
						}
						continue;
					case DELETE:
						if (start < topLeftPosition.rowPosition) {
							leftOffset-= Math.min(diff.getBeforePositionRange().end, topLeftPosition.rowPosition + 1) - start;
						}
						if (start <= bottomRightPosition.rowPosition) {
							rightOffset-= Math.min(diff.getBeforePositionRange().end, bottomRightPosition.rowPosition + 1) - start;
							if (freezeMove == 0) {
								freezeMove= 1;
							}
						}
						else {
							freezeMove= -1;
						}
						continue;
					default:
						continue;
					}
				}
				
				topLeftPosition.rowPosition+= leftOffset;
				bottomRightPosition.rowPosition+= rightOffset;
			}
		}
	}
	
}
