/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.cell;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.data.convert.IDisplayConverter;


public final class CellDisplayConversionUtils {
	
	
	public static String convertDataType(final LayerCell cell, final ConfigRegistry configRegistry) {
		return convertDataType(cell, cell.getDataValue(0, null), configRegistry);
	}
	
	public static String convertDataType(final LayerCell cell, final Object canonicalValue, final ConfigRegistry configRegistry) {
		Object displayValue;
		
		final IDisplayConverter displayConverter= configRegistry.getAttribute(
				CellConfigAttributes.DISPLAY_CONVERTER,
				cell.getDisplayMode(),
				cell.getLabels().getLabels());
		
		if (displayConverter != null) {
			displayValue= displayConverter.canonicalToDisplayValue(cell, configRegistry, canonicalValue);
		}
		else {
			displayValue= canonicalValue;
		}
		
		return (displayValue == null) ? "" : String.valueOf(displayValue); //$NON-NLS-1$
	}
	
}
