/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.command;

import java.util.Collection;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;


@NonNullByDefault
public abstract class AbstractDimPositionsCommand implements LayerCommand {
	
	protected static final long NO_REF= Long.MIN_VALUE + 1;
	
	
	private LayerDim layerDim;
	
	private long refPosition;
	
	private Collection<LRange> positions;
	
	
	protected AbstractDimPositionsCommand(final LayerDim layerDim,
			final long refPosition, final Collection<LRange> positions) {
		this.layerDim= layerDim;
		this.refPosition= refPosition;
		this.positions= positions;
	}
	
	protected AbstractDimPositionsCommand(final LayerDim layerDim,
			final Collection<LRange> positions) {
		this(layerDim, NO_REF, positions);
	}
	
	protected AbstractDimPositionsCommand(final AbstractDimPositionsCommand command) {
		this.layerDim= command.layerDim;
		this.refPosition= command.refPosition;
		this.positions= command.positions;
	}
	
	
	public final Orientation getOrientation() {
		return this.layerDim.getOrientation();
	}
	
	public final LayerDim getDim() {
		return this.layerDim;
	}
	
	public long getRefPosition() {
		return this.refPosition;
	}
	
	public Collection<LRange> getPositions() {
		return this.positions;
	}
	
	
	@Override
	public boolean convertToTargetLayer(final Layer targetLayer) {
		final LayerDim targetLayerDim= targetLayer.getDim(getOrientation());
		if (this.layerDim == targetLayerDim) {
			return true;
		}
		
		return convertToTargetLayer(this.layerDim, this.refPosition, targetLayerDim);
	}
	
	protected boolean convertToTargetLayer(final LayerDim dim,
			final long refPosition, final LayerDim targetDim) {
		final long targetRefPosition;
		final LRangeList targetPositions= new LRangeList();
		if (refPosition == NO_REF) {
			targetRefPosition= NO_REF;
			for (final LRange lRange : this.positions) {
				for (long position= lRange.start; position < lRange.end; position++) {
					final long targetPosition= LayerUtils.convertPosition(dim,
							position, position, targetDim );
					if (targetPosition != LayerDim.POSITION_NA) {
						targetPositions.values().add(targetPosition);
					}
				}
			}
		}
		else if (refPosition != LayerDim.POSITION_NA) {
			targetRefPosition= LayerUtils.convertPosition(dim, refPosition, refPosition, targetDim);
			if (targetRefPosition == LayerDim.POSITION_NA) {
				return false;
			}
			for (final LRange lRange : this.positions) {
				for (long position= lRange.start; position < lRange.end; position++) {
					final long targetPosition= LayerUtils.convertPosition(dim,
							refPosition, position, targetDim );
					if (targetPosition != LayerDim.POSITION_NA) {
						targetPositions.values().add(targetPosition);
					}
				}
			}
		}
		else {
			targetRefPosition= LayerDim.POSITION_NA;
		}
		
		if (targetPositions.isEmpty()) {
			return false;
		}
		
		this.layerDim= targetDim;
		this.refPosition= targetRefPosition;
		this.positions= targetPositions;
		return true;
	}
	
}
