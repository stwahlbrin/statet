/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PixelOutOfBoundsException;


/**
 * A layer dimension for a {@link TransformLayer}.
 * 
 * @param <TLayer> the type of the layer
 */
@NonNullByDefault
public abstract class TransformLayerDim<TLayer extends Layer> extends ForwardLayerDim<TLayer> {
	
	
	public TransformLayerDim(final TLayer layer, final LayerDim underlyingDim) {
		super(layer, underlyingDim.getOrientation(), underlyingDim);
	}
	
	public TransformLayerDim(final TLayer layer, final Orientation orientation,
			final LayerDim underlyingDim) {
		super(layer, orientation, underlyingDim);
	}
	
	
	@Override
	public long getPositionId(final long refPosition, final long position) {
		final long underlyingRefPosition= localToUnderlyingPosition(refPosition, refPosition);
		return this.underlyingDim.getPositionId(underlyingRefPosition,
				(refPosition == position) ?
						underlyingRefPosition :
						localToUnderlyingPosition(refPosition, position) );
	}
	
	@Override
	public long getPositionById(final long id) {
		final long underlyingPosition= this.underlyingDim.getPositionById(id);
		if (underlyingPosition == POSITION_NA) {
			return POSITION_NA;
		}
		final long position= doUnderlyingToLocalPosition(underlyingPosition);
		if (position < 0 || position >= getPositionCount()) {
			return POSITION_NA;
		}
		return position;
	}
	
	
	@Override
	public abstract long localToUnderlyingPosition(final long refPosition, final long position);
	
	@Override
	public long underlyingToLocalPosition(final LayerDim sourceUnderlyingDim,
			final long underlyingPosition) {
		if (sourceUnderlyingDim != this.underlyingDim) {
			throw new IllegalArgumentException("underlyingLayer"); //$NON-NLS-1$
		}
		
		return doUnderlyingToLocalPosition(underlyingPosition);
	}
	
	@Override
	public List<LRange> underlyingToLocalPositions(final LayerDim sourceUnderlyingDim,
			final Collection<LRange> underlyingPositions) {
		if (sourceUnderlyingDim != this.underlyingDim) {
			throw new IllegalArgumentException("underlyingLayer"); //$NON-NLS-1$
		}
		
		final List<LRange> localPositions= new ArrayList<>(underlyingPositions.size());
		
		for (final LRange underlyingPositionRange : underlyingPositions) {
			if (underlyingPositionRange.start == underlyingPositionRange.end) {
				final long position= doUnderlyingToLocalPosition(underlyingPositionRange.start);
				localPositions.add(new LRange(position, position));
			}
			else {
				final long first= doUnderlyingToLocalPosition(underlyingPositionRange.start);
				final long last= doUnderlyingToLocalPosition(underlyingPositionRange.end - 1);
				if (first <= last) {
					localPositions.add(new LRange(first, last + 1));
				}
			}
		}
		return localPositions;
	}
	
	protected abstract long doUnderlyingToLocalPosition(final long underlyingPosition);
	
	
	@Override
	public long getPositionByPixel(final long pixel) {
		final long position= doUnderlyingToLocalPosition(this.underlyingDim.getPositionByPixel(pixel));
		if (position < 0 || position >= getPositionCount()) {
			throw new PixelOutOfBoundsException(pixel, getOrientation());
		}
		return position;
	}
	
	@Override
	public long getPositionStart(final long refPosition, final long position) {
		final long underlyingRefPosition= localToUnderlyingPosition(refPosition, refPosition);
		return this.underlyingDim.getPositionStart(underlyingRefPosition,
				(refPosition == position) ?
						underlyingRefPosition :
						localToUnderlyingPosition(refPosition, position) );
	}
	
	@Override
	public long getPositionStart(final long position) {
		return this.underlyingDim.getPositionStart(
				localToUnderlyingPosition(position, position) );
	}
	
	@Override
	public int getPositionSize(final long refPosition, final long position) {
		final long underlyingRefPosition= localToUnderlyingPosition(refPosition, refPosition);
		return this.underlyingDim.getPositionSize(underlyingRefPosition,
				(refPosition == position) ?
						underlyingRefPosition : 
						localToUnderlyingPosition(refPosition, position) );
	}
	
	@Override
	public int getPositionSize(final long position) {
		return this.underlyingDim.getPositionSize(
				localToUnderlyingPosition(position, position) );
	}
	
	@Override
	public boolean isPositionResizable(final long position) {
		final long underlyingPosition= localToUnderlyingPosition(position, position);
		return this.underlyingDim.isPositionResizable(underlyingPosition);
	}
	
}
