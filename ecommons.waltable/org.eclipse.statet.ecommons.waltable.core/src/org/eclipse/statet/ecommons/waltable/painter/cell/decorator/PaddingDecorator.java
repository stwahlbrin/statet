/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell.decorator;

import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;
import org.eclipse.statet.ecommons.waltable.painter.cell.CellPainterWrapper;
import org.eclipse.statet.ecommons.waltable.style.CellStyleUtil;


public class PaddingDecorator extends CellPainterWrapper {
	
	
	private final long topPadding;
	private final long rightPadding;
	private final long bottomPadding;
	private final long leftPadding;
	
	
	public PaddingDecorator(final LayerCellPainter interiorPainter) {
		this(interiorPainter, 2);
	}
	
	public PaddingDecorator(final LayerCellPainter interiorPainter, final long padding) {
		this(interiorPainter, padding, padding, padding, padding);
	}
	
	public PaddingDecorator(final LayerCellPainter interiorPainter, final long topPadding, final long rightPadding, final long bottomPadding, final long leftPadding) {
		super(interiorPainter);
		this.topPadding= topPadding;
		this.rightPadding= rightPadding;
		this.bottomPadding= bottomPadding;
		this.leftPadding= leftPadding;
	}
	
	
	@Override
	public long getPreferredWidth(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		return this.leftPadding + super.getPreferredWidth(cell, gc, configRegistry) + this.rightPadding;
	}
	
	@Override
	public long getPreferredHeight(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		return this.topPadding + super.getPreferredHeight(cell, gc, configRegistry) + this.bottomPadding;
	}
	
	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle adjustedCellBounds, final ConfigRegistry configRegistry) {
		final Color originalBg= gc.getBackground();
		final Color cellStyleBackground= getBackgroundColor(cell, configRegistry);
		if (cellStyleBackground != null) {
			gc.setBackground(cellStyleBackground);
			gc.fillRectangle(safe(adjustedCellBounds));
			gc.setBackground(originalBg);
		}
		else {
			gc.fillRectangle(safe(adjustedCellBounds));
		}
		
		final LRectangle interiorBounds= getInteriorBounds(adjustedCellBounds);
		if (interiorBounds.width > 0 && interiorBounds.height > 0) {
			super.paintCell(cell, gc, interiorBounds, configRegistry);
		}
	}
	
	protected LRectangle getInteriorBounds(final LRectangle adjustedCellBounds) {
		return new LRectangle(
				adjustedCellBounds.x + this.leftPadding,
				adjustedCellBounds.y + this.topPadding,
				adjustedCellBounds.width - this.leftPadding - this.rightPadding,
				adjustedCellBounds.height - this.topPadding - this.bottomPadding
		);
	}
	
	protected Color getBackgroundColor(final LayerCell cell, final ConfigRegistry configRegistry) {
		return CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyling.BACKGROUND_COLOR);		
	}
	
	@Override
	public LayerCellPainter getCellPainterAt(final long x, final long y, final LayerCell cell, final GC gc, final LRectangle adjustedCellBounds, final ConfigRegistry configRegistry) {
		//need to take the alignment into account
		final Style cellStyle= CellStyleUtil.getCellStyle(cell, configRegistry);
		
		final HorizontalAlignment horizontalAlignment= cellStyle.getAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT);
		long horizontalAlignmentPadding= 0;
		switch (horizontalAlignment) {
			case LEFT: horizontalAlignmentPadding= this.leftPadding;
						break;
			case CENTER: horizontalAlignmentPadding= this.leftPadding/2;
						break;
		}
		
		final VerticalAlignment verticalAlignment= cellStyle.getAttributeValue(CellStyling.VERTICAL_ALIGNMENT);
		long verticalAlignmentPadding= 0;
		switch (verticalAlignment) {
			case TOP: verticalAlignmentPadding= this.topPadding;
						break;
			case MIDDLE: verticalAlignmentPadding= this.topPadding/2;
						break;
		}
		
		return super.getCellPainterAt(x - horizontalAlignmentPadding, y - verticalAlignmentPadding, cell, gc, adjustedCellBounds,
				configRegistry);
	}
	
}
