/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.swt.painters;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;
import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import java.util.function.Function;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.style.GridStyling.ConfigGridLineColorSupplier;


@NonNullByDefault
public class GridLineCellLayerPainter extends CellLayerPainter {
	
	
	private final Function<ConfigRegistry, Color> gridLineColorSupplier;
	
	
	public GridLineCellLayerPainter(final Function<ConfigRegistry, Color> gridLineColorSupplier) {
		this.gridLineColorSupplier= gridLineColorSupplier;
	}
	
	public GridLineCellLayerPainter() {
		this(new ConfigGridLineColorSupplier());
	}
	
	
	@Override
	public LRectangle adjustCellBounds(final long columnPosition, final long rowPosition,
			final LRectangle bounds) {
		return new LRectangle(bounds.x, bounds.y, Math.max(bounds.width - 1, 0), Math.max(bounds.height - 1, 0));
	}
	
	@Override
	public void paintLayer(final Layer natLayer, final GC gc,
			final int xOffset, final int yOffset, final Rectangle pixelRectangle,
			final ConfigRegistry configRegistry) {
		gc.setForeground(this.gridLineColorSupplier.apply(configRegistry));
		drawHorizontalLines(natLayer, gc, pixelRectangle);
		drawVerticalLines(natLayer, gc, pixelRectangle);
		
		super.paintLayer(natLayer, gc, xOffset, yOffset, pixelRectangle, configRegistry);
	}
	
	protected void drawHorizontalLines(final Layer natLayer, final GC gc, final Rectangle rectangle) {
		final int startX= safe(rectangle.x);
		final int endX= safe(rectangle.x + Math.min(natLayer.getWidth() - 1, rectangle.width));
		
		final LayerDim dim= natLayer.getDim(VERTICAL);
		final long endPosition= Math.min(dim.getPositionCount(), dim.getPositionByPixel(rectangle.y + rectangle.height - 1) + 1);
		for (long position= dim.getPositionByPixel(rectangle.y); position < endPosition; position++) {
			final int size= dim.getPositionSize(position);
			if (size > 0) {
				final int y= safe(dim.getPositionStart(position) + size - 1);
				gc.drawLine(startX, y, endX, y);
			}
		}
	}
	
	protected void drawVerticalLines(final Layer natLayer, final GC gc, final Rectangle rectangle) {
		final int startY= safe(rectangle.y);
		final int endY= safe(rectangle.y + Math.min(natLayer.getHeight() - 1, rectangle.height));
		
		final LayerDim dim= natLayer.getDim(HORIZONTAL);
		final long endPosition= Math.min(dim.getPositionCount(), dim.getPositionByPixel(rectangle.x + rectangle.width - 1) + 1);
		for (long position= dim.getPositionByPixel(rectangle.x); position < endPosition; position++) {
			final long size= dim.getPositionSize(position);
			if (size > 0) {
				final int x= safe(dim.getPositionStart(position) + size - 1);
				gc.drawLine(x, startY, x, endY);
			}
		}
	}
	
}
