/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.edit.CheckBoxStateEnum;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public abstract class TreeCheckBoxPainter extends ImagePainter {
	
	private final Image checkedImg;
	private final Image semicheckedImg;
	private final Image uncheckedImg;

	public TreeCheckBoxPainter() {
		this(
				GUIHelper.getImage("checked"), //$NON-NLS-1$
				GUIHelper.getImage("semichecked"), //$NON-NLS-1$
				GUIHelper.getImage("unchecked") //$NON-NLS-1$
		);
	}

	public TreeCheckBoxPainter(final Image checkedImg, final Image semicheckedImage, final Image uncheckedImg) {
		this.checkedImg= checkedImg;
		this.semicheckedImg= semicheckedImage;
		this.uncheckedImg= uncheckedImg;
	}

	public long getPreferredWidth(final boolean checked) {
		return checked ? this.checkedImg.getBounds().width : this.uncheckedImg.getBounds().width;
	}

	public long getPreferredHeight(final boolean checked) {
		return checked ? this.checkedImg.getBounds().height : this.uncheckedImg.getBounds().height;
	}

	@Override
	protected Image getImage(final LayerCell cell, final ConfigRegistry configRegistry) {
		switch (getCheckBoxState(cell)) {
		case CHECKED:
			return this.checkedImg;
		case SEMICHECKED:
			return this.semicheckedImg;
		default:
			return this.uncheckedImg;
		}
	}
	
	protected abstract CheckBoxStateEnum getCheckBoxState(LayerCell cell);

}
