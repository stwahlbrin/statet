/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;

import org.eclipse.statet.ecommons.waltable.config.AbstractRegistryConfiguration;
import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.painter.cell.TextPainter;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public class DefaultRowHeaderStyleConfiguration extends AbstractRegistryConfiguration {

	public Font font= GUIHelper.getFont(new FontData("Verdana", 10, SWT.NORMAL)); //$NON-NLS-1$
	public Color bgColor= GUIHelper.COLOR_WIDGET_BACKGROUND;
	public Color fgColor= GUIHelper.COLOR_WIDGET_FOREGROUND;
	public Color gradientBgColor= GUIHelper.COLOR_WHITE;
	public Color gradientFgColor= new Color(136, 212, 215);
	public HorizontalAlignment hAlign= HorizontalAlignment.CENTER;
	public VerticalAlignment vAlign= VerticalAlignment.MIDDLE;
	public BorderStyle borderStyle= null;

	public LayerCellPainter layerCellPainter= new TextPainter();

	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		configureRowHeaderCellPainter(configRegistry);
		configureRowHeaderStyle(configRegistry);
	}

	protected void configureRowHeaderStyle(final ConfigRegistry configRegistry) {
		final BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.bgColor);
		cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.fgColor);
		cellStyle.setAttributeValue(CellStyling.GRADIENT_BACKGROUND_COLOR, this.gradientBgColor);
		cellStyle.setAttributeValue(CellStyling.GRADIENT_FOREGROUND_COLOR, this.gradientFgColor);
		cellStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, this.hAlign);
		cellStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, this.vAlign);
		cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, this.borderStyle);
		cellStyle.setAttributeValue(CellStyling.FONT, this.font);

		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle, DisplayMode.NORMAL, GridLabels.ROW_HEADER);
	}

	protected void configureRowHeaderCellPainter(final ConfigRegistry configRegistry) {
		configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.layerCellPainter, DisplayMode.NORMAL, GridLabels.ROW_HEADER);
	}

}
