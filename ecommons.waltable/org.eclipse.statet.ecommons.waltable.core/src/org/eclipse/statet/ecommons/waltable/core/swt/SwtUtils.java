/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Scrollable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle.LineStyle;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;


@NonNullByDefault
public class SwtUtils {
	
	
	public static final @Nullable ScrollBar getScrollBar(final Scrollable control,
			final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return control.getHorizontalBar();
		case VERTICAL:
			return control.getVerticalBar();
		default:
			throw new IllegalStateException();
		}
	}
	
	
	public static final int get(final Point point, final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return point.x;
		case VERTICAL:
			return point.y;
		default:
			throw new IllegalStateException();
		}
	}
	
	public static final int getStart(final Rectangle rect, final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return rect.x;
		case VERTICAL:
			return rect.y;
		default:
			throw new IllegalStateException();
		}
	}
	
	public static final int getEnd(final Rectangle rect, final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return rect.x + rect.width;
		case VERTICAL:
			return rect.y + rect.height;
		default:
			throw new IllegalStateException();
		}
	}
	
	public static final int getSize(final Rectangle rect, final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return rect.width;
		case VERTICAL:
			return rect.height;
		default:
			throw new IllegalStateException();
		}
	}
	
	
	public static final int toSWT(final long pixel) {
		if (pixel < Integer.MIN_VALUE || pixel > Integer.MAX_VALUE) {
			throw new IndexOutOfBoundsException();
		}
		return (int)pixel;
	}
	
	public static final Rectangle toSWT(final LRectangle rect) {
		return new Rectangle(toSWT(rect.x), toSWT(rect.y), toSWT(rect.width), toSWT(rect.height));
	}
	
	public static final LRectangle toNatTable(final Rectangle rect) {
		return new LRectangle(rect.x, rect.y, rect.width, rect.height);
	}
	
	public static final Point toSWT(final LPoint lPoint) {
		return new Point(toSWT(lPoint.x), toSWT(lPoint.y));
	}
	
	
	public static final int toSWT(final HorizontalAlignment alignment) {
		switch (alignment) {
		case LEFT:
			return SWT.LEFT;
		case CENTER:
			return SWT.CENTER;
		case RIGHT:
			return SWT.RIGHT;
		default:
			throw new IllegalStateException();
		}
	}
	
	public static final int toSWT(final LineStyle lineStyle) {
		switch (lineStyle) {
		case SOLID:
			return SWT.LINE_SOLID;
		case DASHED:
			return SWT.LINE_DASH;
		case DOTTED:
			return SWT.LINE_DOT;
		case DASHDOT:
			return SWT.LINE_DASHDOT;
		case DASHDOTDOT:
			return SWT.LINE_DASHDOTDOT;
		default:
			throw new IllegalStateException();
		}
	}
	
	
	public static final int getMouseWheelEventType(final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return SWT.MouseHorizontalWheel;
		case VERTICAL:
			return SWT.MouseVerticalWheel;
		default:
			throw new IllegalStateException();
		}
	}
	
	public static final int getPixel(final MouseEvent event, final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return event.x;
		case VERTICAL:
			return event.y;
		default:
			throw new IllegalStateException();
		}
	}
	
}
