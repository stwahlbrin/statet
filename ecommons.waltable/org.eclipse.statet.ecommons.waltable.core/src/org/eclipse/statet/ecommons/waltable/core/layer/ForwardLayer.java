/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralChangeEvent;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;


@NonNullByDefault
public abstract class ForwardLayer<TLayerDim extends ForwardLayerDim<? extends ForwardLayer<TLayerDim>>>
		extends AbstractLayer<TLayerDim> {
	
	
	private final Layer underlyingLayer;
	
	
	public ForwardLayer(final Layer underlyingLayer,
			final @Nullable LayerPainter layerPainter) {
		super(layerPainter);
		this.underlyingLayer= nonNullAssert(underlyingLayer);
		this.underlyingLayer.setClientAreaProvider(getClientAreaProvider());
		this.underlyingLayer.addLayerListener(this);
	}
	
	public ForwardLayer(final Layer underlyingLayer) {
		this(underlyingLayer, null);
	}
	
	
	protected final Layer getUnderlyingLayer() {
		return this.underlyingLayer;
	}
	
	// Dispose
	
	@Override
	public void dispose() {
		this.underlyingLayer.dispose();
	}
	
	// Persistence
	
	@Override
	public void saveState(final String prefix, final Map<String, String> properties) {
		this.underlyingLayer.saveState(prefix, properties);
		super.saveState(prefix, properties);
	}
	
	/**
	 * Underlying layers <i>must</i> load state first.
	 * If this is not done, {@link StructuralChangeEvent} from underlying
	 * layers will reset caches after state has been loaded
	 */
	@Override
	public void loadState(final String prefix, final Map<String, String> properties) {
		this.underlyingLayer.loadState(prefix, properties);
		super.loadState(prefix, properties);
	}
	
	// Configuration
	
	@Override
	public void configure(final ConfigRegistry configRegistry, final UiBindingRegistry uiBindingRegistry) {
		this.underlyingLayer.configure(configRegistry, uiBindingRegistry);
		super.configure(configRegistry, uiBindingRegistry);
	}
	
	@Override
	public LayerPainter getLayerPainter() {
		final var layerPainter= super.getLayerPainter();
		return (layerPainter != null) ? layerPainter : this.underlyingLayer.getLayerPainter();
	}
	
	@Override
	@SuppressWarnings("null")
	protected LayerPainter createPainter() {
		return null;
	}
	
	
	// Command
	
	@Override
	public boolean doCommand(final LayerCommand command) {
		if (super.doCommand(command)) {
			return true;
		}
		
		return this.underlyingLayer.doCommand(command);
	}
	
	// Client area
	
	@Override
	public void setClientAreaProvider(final ClientAreaProvider clientAreaProvider) {
		super.setClientAreaProvider(clientAreaProvider);
		this.underlyingLayer.setClientAreaProvider(clientAreaProvider);
	}
	
	
	// Cell features
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final LayerCell underlyingCell= this.underlyingLayer.getCellByPosition(
				columnPosition, rowPosition );
		
		return createCell(
				underlyingCell.getDim(HORIZONTAL),
				underlyingCell.getDim(VERTICAL),
				underlyingCell );
	}
	
	protected LayerCell createCell(final LayerCellDim hDim, final LayerCellDim vDim,
			final LayerCell underlyingCell) {
		return new ForwardLayerCell(this, hDim, vDim, underlyingCell);
	}
	
	// IRegionResolver
	
	@Override
	public LabelStack getRegionLabelsByXY(final long x, final long y) {
		return this.underlyingLayer.getRegionLabelsByXY(x, y);
	}
	
	@Override
	public Layer getUnderlyingLayerByPosition(final long columnPosition, final long rowPosition) {
		return this.underlyingLayer;
	}
	
}
