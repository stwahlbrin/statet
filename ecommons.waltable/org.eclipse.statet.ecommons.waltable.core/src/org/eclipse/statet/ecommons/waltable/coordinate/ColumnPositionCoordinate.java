/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.coordinate;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


public final class ColumnPositionCoordinate {


	private final Layer layer;

	public final long columnPosition;


	public ColumnPositionCoordinate(final Layer layer, final long columnPosition) {
		this.layer= layer;
		this.columnPosition= columnPosition;
	}


	public Layer getLayer() {
		return this.layer;
	}

	public long getColumnPosition() {
		return this.columnPosition;
	}


	@Override
	public int hashCode() {
		final int h= 253 + (int) (this.columnPosition ^ (this.columnPosition >>> 32));
		return this.layer.hashCode() + (h ^ (h >>> 7));
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ColumnPositionCoordinate)) {
			return false;
		}
		final ColumnPositionCoordinate other= (ColumnPositionCoordinate) obj;
		return (this.layer == other.layer
				&& this.columnPosition == other.columnPosition );
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [" + this.layer + ":" + this.columnPosition + "]";
	}

}
