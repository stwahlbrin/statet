/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;
import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import java.util.Map;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.layer.cell.AggregrateConfigLabelAccumulator;
import org.eclipse.statet.ecommons.waltable.layer.cell.CellLabelContributor;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;


/**
 * A composite layer is a layer that is made up of a number of underlying
 * child layers.
 * 
 * <p>This class assumes that the child layers are laid out in a regular grid
 * pattern where the child layers in each composite row all have the same
 * number of rows and the same height, and the child layers in each composite
 * column each have the same number of columns and the same width.</p>
 */
@NonNullByDefault
public class CompositeLayer extends AbstractLayer<CompositeLayerDim> {
	
	
	protected static final class Child {
		
		public final int layoutX;
		public final int layoutY;
		
		public final String label;
		
		public final Layer layer;
		
		private CellLabelContributor configLabelAccumulator;
		
		
		private Child(final int x, final int y, final String label, final Layer layer) {
			this.layoutX= x;
			this.layoutY= y;
			this.label= label;
			this.layer= layer;
		}
		
	}
	
	
	protected final int layoutXCount;
	
	protected final int layoutYCount;
	
	/** Data struct. for child Layers */
	private final @NonNull Child [] @NonNull [] childLayout;
	
	
	public CompositeLayer(final int layoutXCount, final int layoutYCount) {
		this.layoutXCount= layoutXCount;
		this.layoutYCount= layoutYCount;
		this.childLayout= new @NonNull Child @NonNull [layoutXCount] @NonNull [layoutYCount];
		
		init();
	}
	
	@Override
	protected CompositeLayerDim createDim(final Orientation orientation) {
		return (ignoreRef(orientation)) ?
					new CompositeLayerDim.IgnoreRef(this, orientation) :
					new CompositeLayerDim(this, orientation);
	}
	
	protected boolean ignoreRef(final Orientation orientation) {
		return false;
	}
	
	
	@Override
	protected LayerPainter createPainter() {
		return new CompositeLayerPainter(this);
	}
	
	
	// Dispose
	
	@Override
	public void dispose() {
		for (int layoutX= 0; layoutX < this.layoutXCount; layoutX++) {
			for (int layoutY= 0; layoutY < this.layoutYCount; layoutY++) {
				final Child child= this.childLayout[layoutX][layoutY];
				if (child != null) {
					child.layer.dispose();
				}
			}
		}
	}
	
	// Persistence
	
	@Override
	public void saveState(final String prefix, final Map<String, String> properties) {
		for (int layoutX= 0; layoutX < this.layoutXCount; layoutX++) {
			for (int layoutY= 0; layoutY < this.layoutYCount; layoutY++) {
				final Child child= this.childLayout[layoutX][layoutY];
				if (child != null) {
					child.layer.saveState(prefix + "." + child.label, properties); //$NON-NLS-1$
				}
			}
		}
		super.saveState(prefix, properties);
	}
	
	@Override
	public void loadState(final String prefix, final Map<String, String> properties) {
		for (int layoutX= 0; layoutX < this.layoutXCount; layoutX++) {
			for (int layoutY= 0; layoutY < this.layoutYCount; layoutY++) {
				final Child child= this.childLayout[layoutX][layoutY];
				if (child != null) {
					child.layer.loadState(prefix + "." + child.label, properties); //$NON-NLS-1$
				}
			}
		}
		super.loadState(prefix, properties);
	}
	
	// Configuration
	
	@Override
	public void configure(final ConfigRegistry configRegistry, final UiBindingRegistry uiBindingRegistry) {
		for (int layoutX= 0; layoutX < this.layoutXCount; layoutX++) {
			for (int layoutY= 0; layoutY < this.layoutYCount; layoutY++) {
				this.childLayout[layoutX][layoutY].layer.configure(configRegistry, uiBindingRegistry);
			}
		}
		
		super.configure(configRegistry, uiBindingRegistry);
	}
	
	/**
	 * {@inheritDoc}
	 * Handle commands
	 */
	@Override
	public boolean doCommand(final LayerCommand command) {
		if (super.doCommand(command)) {
			return true;
		}
		return doCommandOnChildLayers(command);
	}
	
	protected boolean doCommandOnChildLayers(final LayerCommand command) {
		for (int layoutX= 0; layoutX < this.layoutXCount; layoutX++) {
			for (int layoutY= 0; layoutY < this.layoutYCount; layoutY++) {
				if (this.childLayout[layoutX][layoutY].layer.doCommand(command)) {
					return true;
				}
			}
		}
		return false;
	}
	
	// Cell features
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final Child child= getChildByPosition(columnPosition, rowPosition);
		
		if (child == null) {
			throw new PositionOutOfBoundsException(columnPosition + ", " + rowPosition); //$NON-NLS-1$
		}
		
		final long childColumnPosition= columnPosition - getDim(HORIZONTAL).getLayoutPosition(child.layoutX);
		final long childRowPosition= rowPosition - getDim(VERTICAL).getLayoutPosition(child.layoutY);
		
		final LayerCell underlyingCell= child.layer.getCellByPosition(childColumnPosition, childRowPosition);
		
		final LayerCellDim hDim= transformCellDim(underlyingCell.getDim(HORIZONTAL), columnPosition);
		final LayerCellDim vDim= transformCellDim(underlyingCell.getDim(VERTICAL), rowPosition);
		
		return new ForwardLayerCell(this, hDim, vDim, underlyingCell) {
			
			@Override
			public LabelStack getLabels() {
				final LabelStack configLabels= super.getLabels();
				
				if (child.configLabelAccumulator != null) {
					child.configLabelAccumulator.addLabels(configLabels, childColumnPosition, childRowPosition);
				}
				
				configLabels.addLabel(child.label);
				
				return configLabels;
			}
			
		};
	}
	
	protected LayerCellDim transformCellDim(final LayerCellDim underlyingDim, final long position) {
		final long originPosition= (underlyingDim.getPosition() == underlyingDim.getOriginPosition()) ?
				position :
				getDim(underlyingDim.getOrientation()).underlyingToLocalPosition(
						position, underlyingDim.getOriginPosition() );
		return new BasicLayerCellDim(underlyingDim.getOrientation(), underlyingDim.getId(),
				position, originPosition, underlyingDim.getPositionSpan() );
	}
	
	// Child layer stuff
	
	public void setChildLayer(final String label, final Layer childLayer, final int layoutX, final int layoutY) {
		final Child child= new Child(layoutX, layoutY, label, nonNullAssert(childLayer));
		
		childLayer.addLayerListener(this);
		this.childLayout[layoutX][layoutY]= child;
		
		getDim(HORIZONTAL).setChild(layoutX, layoutY, childLayer);
		getDim(VERTICAL).setChild(layoutY, layoutX, childLayer);
		
		childLayer.setClientAreaProvider(new ClientAreaProvider() {
			@Override
			public LRectangle getClientArea() {
				return getChildClientArea(child);
			}
		});
	}
	
	/**
	 * Adds the configLabelAccumulator to the existing label accumulators.
	 */
	public void addCellLabelContributor(final String regionLabel, final CellLabelContributor labelContributor) {
		final Child child= getChildByLabel(regionLabel);
		if (child.configLabelAccumulator == null) {
			child.configLabelAccumulator= labelContributor;
		}
		else {
			final AggregrateConfigLabelAccumulator aggregateAccumulator;
			if (child.configLabelAccumulator instanceof AggregrateConfigLabelAccumulator) {
				aggregateAccumulator= (AggregrateConfigLabelAccumulator) child.configLabelAccumulator;
			}
			else {
				aggregateAccumulator= new AggregrateConfigLabelAccumulator();
				aggregateAccumulator.add(child.configLabelAccumulator);
				child.configLabelAccumulator= aggregateAccumulator;
			}
			aggregateAccumulator.add(labelContributor);
		}
	}
	
	private LRectangle getChildClientArea(final Child child) {
		final LRectangle compositeClientArea= getClientAreaProvider().getClientArea();
		
		final LRectangle childClientArea= new LRectangle(
				compositeClientArea.x + getDim(HORIZONTAL).getLayoutStart(child.layoutX),
				compositeClientArea.y + getDim(VERTICAL).getLayoutStart(child.layoutY),
				child.layer.getDim(HORIZONTAL).getPreferredSize(),
				child.layer.getDim(VERTICAL).getPreferredSize() );
		
		final LRectangle intersection= compositeClientArea.intersection(childClientArea);
		
		return intersection;
	}
	
	/**
	 * @param layoutX col position in the CompositeLayer
	 * @param layoutY row position in the CompositeLayer
	 * @return child layer according to the Composite Layer Layout
	 */
	public @Nullable Layer getChildLayerByLayoutCoordinate(final int layoutX, final int layoutY) {
		if (layoutX < 0 || layoutX >= this.layoutXCount || layoutY < 0 || layoutY >= this.layoutYCount) {
			return null;
		}
		else {
			return this.childLayout[layoutX][layoutY].layer;
		}
	}
	
	/**
	 * @param x pixel position
	 * @param y pixel position
	 * @return Region which the given position is in
	 */
	@Override
	public LabelStack getRegionLabelsByXY(final long x, final long y) {
		final Child child= getChildByPixelXY(x, y);
		if (child == null) {
			return null;
		}
		
		final long childX= x - getDim(HORIZONTAL).getLayoutStart(child.layoutX);
		final long childY= y - getDim(VERTICAL).getLayoutStart(child.layoutY);
		final LabelStack regionLabels= child.layer.getRegionLabelsByXY(childX, childY);
		
		regionLabels.addLabel(child.label);
		
		return regionLabels;
	}
	
	@Override
	public @Nullable Layer getUnderlyingLayerByPosition(final long columnPosition, final long rowPosition) {
		final Child child= getChildByPosition(columnPosition, rowPosition);
		return (child != null) ? child.layer : null;
	}
	
	// Layout coordinate accessors
	
	protected final @Nullable Child getChildByLabel(final String label) {
		for (int layoutX= 0; layoutX < this.layoutXCount; layoutX++) {
			for (int layoutY= 0; layoutY < this.layoutYCount; layoutY++) {
				if (label.equals(this.childLayout[layoutX][layoutY].label)) {
					return this.childLayout[layoutX][layoutY];
				}
			}
		}
		return null;
	}
	
	protected final @Nullable Child getChildByLayer(final Layer childLayer) {
		for (int layoutX= 0; layoutX < this.layoutXCount; layoutX++) {
			for (int layoutY= 0; layoutY < this.layoutYCount; layoutY++) {
				if (this.childLayout[layoutX][layoutY].layer == childLayer) {
					return this.childLayout[layoutX][layoutY];
				}
			}
		}
		return null;
	}
	
	protected final @Nullable Child getChildByPixelXY(final long x, final long y) {
		final int layoutX= getDim(HORIZONTAL).getLayoutByPixel(x);
		if (layoutX < 0) {
			return null;
		}
		final int layoutY= getDim(VERTICAL).getLayoutByPixel(y);
		if (layoutY < 0) {
			return null;
		}
		return this.childLayout[layoutX][layoutY];
	}
	
	protected final @Nullable Child getChildByPosition(final long compositeColumnPosition, final long compositeRowPosition) {
		final int layoutX= getDim(HORIZONTAL).getLayoutByPosition(compositeColumnPosition);
		if (layoutX < 0) {
			return null;
		}
		final int layoutY= getDim(VERTICAL).getLayoutByPosition(compositeRowPosition);
		if (layoutY < 0) {
			return null;
		}
		return this.childLayout[layoutX][layoutY];
	}
	
	
	public static class CompositeLayerPainter implements LayerPainter {
		
		
		protected final CompositeLayer layer;
		
		
		public CompositeLayerPainter(final CompositeLayer layer) {
			this.layer= layer;
		}
		
		
		@Override
		public LRectangle adjustCellBounds(final long columnPosition, final long rowPosition,
				final LRectangle cellBounds) {
			final Child child= this.layer.getChildByPosition(columnPosition, rowPosition);
			final var hDim= this.layer.getDim(HORIZONTAL);
			final var vDim= this.layer.getDim(VERTICAL);
			
			final long widthOffset= hDim.getLayoutStart(child.layoutX);
			final long heightOffset= vDim.getLayoutStart(child.layoutY);
			
//			LRectangle bounds= new LRectangle(cellBounds.x - widthOffset, cellBounds.y - heightOffset, cellBounds.width, cellBounds.height);
			cellBounds.x-= widthOffset;
			cellBounds.y-= heightOffset;
			
			final var childLayerPainter= child.layer.getLayerPainter();
			final long childColumnPosition= columnPosition - hDim.getLayoutPosition(child.layoutX);
			final long childRowPosition= rowPosition - vDim.getLayoutPosition(child.layoutY);
			final LRectangle adjustedChildCellBounds= childLayerPainter.adjustCellBounds(childColumnPosition, childRowPosition, cellBounds);
//			LRectangle adjustedChildCellBounds= childLayerPainter.adjustCellBounds(childColumnPosition, childRowPosition, bounds);
			
			adjustedChildCellBounds.x+= widthOffset;
			adjustedChildCellBounds.y+= heightOffset;
			
			return adjustedChildCellBounds;
		}
		
		@Override
		public void paintLayer(final Layer natLayer, final GC gc,
				final int xOffset, final int yOffset, final Rectangle pixelRectangle,
				final ConfigRegistry configuration) {
			int x= xOffset;
			for (int layoutX= 0; layoutX < this.layer.layoutXCount; layoutX++) {
				int y= yOffset;
				final @NonNull Child[] layoutXChilds= this.layer.childLayout[layoutX];
				final int width= safe(layoutXChilds[0].layer.getWidth());
				for (int layoutY= 0; layoutY < this.layer.layoutYCount; layoutY++) {
					final Child child= layoutXChilds[layoutY];
					final int height= safe(child.layer.getHeight());
					
					final Rectangle childRectangle= new Rectangle(x, y, width, height);
					final Rectangle childPaintRectangle= pixelRectangle.intersection(childRectangle);
					
					if (!childPaintRectangle.isEmpty()) {
						final Rectangle originalClipping= gc.getClipping();
						gc.setClipping(childPaintRectangle);
						
						final var layerPainter= child.layer.getLayerPainter();
						layerPainter.paintLayer(natLayer, gc, x, y, childPaintRectangle, configuration);
						
						gc.setClipping(originalClipping);
					}
					
					y+= height;
				}
				
				x+= width;
			}
		}
		
	}
	
}
