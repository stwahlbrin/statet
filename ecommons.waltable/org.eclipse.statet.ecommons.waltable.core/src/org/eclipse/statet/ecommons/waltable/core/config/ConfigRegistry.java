/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.config;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * @see BasicConfigRegistry
 */
@NonNullByDefault
public interface ConfigRegistry {
	
	
	DisplayModeLookupStrategy getDisplayModeOrdering();
	
	
	/**
	 * If retrieving registered values
	 * <p>
	 * Example 1:
	 * 	 <p>registry.getAttribute(attribute, DisplayMode.EDIT);</p>
	 * <ol>
	 * <li>It will look for an attribute registered using the EDIT display mode</li>
	 * <li>If it can't find that it will try and find an attribute under the NORMAL mode</li>
	 * <li>If it can't find one it will try and find one registered without a display mode {@link #registerAttribute(ConfigAttribute, Object)}</li>
	 * </ol>
	 * Example 2:
	 *   <p>registry.getAttribute(attribute, DisplayMode.NORMAL, "testLabel", "testLabel_1");</p>
	 * <ol>
	 * <li>It will look for an attribute registered by display mode NORMAL and "testLabel"</li>
	 * <li>It will look for an attribute registered by display mode NORMAL and "testLabel_1"</li>
	 * </ol>
	 * @param <T> Type of the attribute
	 * @param configAttribute to be registered
	 * @param targetDisplayMode display mode the cell needs to be in, for this attribute to be returned
	 * @param configLabels the cell needs to have,  for this attribute to be returned
	 * @return the configAttribute, if the display mode and the configLabels match
	 */
	<T> @Nullable T getAttribute(ConfigAttribute<T> configAttribute, DisplayMode targetDisplayMode,
			@NonNull String... configLabels);
	
	/**
	 * @see #getAttribute(ConfigAttribute, DisplayMode, String...)
	 */
	<T> @Nullable T getAttribute(ConfigAttribute<T> configAttribute, DisplayMode targetDisplayMode,
			List<String> configLabels);
	
	/**
	 * @see #getAttribute(ConfigAttribute, DisplayMode, String...)
	 */
	<T> @Nullable T getSpecificAttribute(ConfigAttribute<T> configAttribute, DisplayMode displayMode,
			@Nullable String configLabel);
	
	
	/**
	 * Register a configuration attribute
	 */
	<T> void registerAttribute(ConfigAttribute<T> configAttribute, T attributeValue);
	
	/**
	 * Register an attribute against a {@link DisplayMode}.
	 */
	<T> void registerAttribute(ConfigAttribute<T> configAttribute, T attributeValue, DisplayMode targetDisplayMode);
	
	/**
	 * Register an attribute against a {@link DisplayMode} and configuration label (applied to cells)
	 */
	<T> void registerAttribute(ConfigAttribute<T> configAttribute, T attributeValue, DisplayMode targetDisplayMode, String configLabel);
	
	<T> void unregisterAttribute(ConfigAttribute<T> configAttributeType, DisplayMode displayMode, String configLabel);
	
	
}
