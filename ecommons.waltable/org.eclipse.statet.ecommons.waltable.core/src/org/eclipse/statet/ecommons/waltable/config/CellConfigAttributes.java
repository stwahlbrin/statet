/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.config;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.data.convert.IDisplayConverter;


public interface CellConfigAttributes {
	
	ConfigAttribute<LayerCellPainter> CELL_PAINTER= new ConfigAttribute<>();
	
	ConfigAttribute<Style> CELL_STYLE= new ConfigAttribute<>();
	
	ConfigAttribute<IDisplayConverter> DISPLAY_CONVERTER= new ConfigAttribute<>();
	
}
