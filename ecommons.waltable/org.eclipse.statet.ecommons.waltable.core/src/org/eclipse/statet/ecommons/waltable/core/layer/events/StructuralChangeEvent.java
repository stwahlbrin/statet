/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;


/**
 * An event indicating a structural change to the layer. A structural change is
 * defined as something that modifies the number of columns/rows in the layer or
 * their associated widths/heights.
 */
@NonNullByDefault
public interface StructuralChangeEvent extends VisualChangeEvent {
	
	
	boolean isStructureChanged(Orientation orientation);
	
	@Nullable List<StructuralDiff> getDiffs(Orientation orientation);
	
}
