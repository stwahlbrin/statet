/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.coordinate;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


public final class RowPositionCoordinate {
	
	
	private final Layer layer;
	
	public final long rowPosition;
	
	
	public RowPositionCoordinate(final Layer layer, final long rowPosition) {
		this.layer= layer;
		this.rowPosition= rowPosition;
	}
	
	
	public Layer getLayer() {
		return this.layer;
	}
	
	public long getRowPosition() {
		return this.rowPosition;
	}
	
	
	@Override
	public int hashCode() {
		final int h= 125315 + (int) (this.rowPosition ^ (this.rowPosition >>> 32));
		return this.layer.hashCode() + (h ^ (h >>> 7));
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RowPositionCoordinate)) {
			return false;
		}
		final RowPositionCoordinate other= (RowPositionCoordinate) obj;
		return (this.layer == other.layer
				&& this.rowPosition == other.rowPosition);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + this.layer + ":" + this.rowPosition + "]";
	}
	
}
