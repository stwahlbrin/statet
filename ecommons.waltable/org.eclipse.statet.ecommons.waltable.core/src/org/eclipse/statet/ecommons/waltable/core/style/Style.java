/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.style;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;


/**
 * Used to store attributes reflecting a (usually display) style.
 */
@NonNullByDefault
public interface Style {
	
	
	<T> @Nullable T getAttributeValue(ConfigAttribute<T> configAttribute);
	
	<T> void setAttributeValue(ConfigAttribute<T> configAttribute, @Nullable T value);
	
}
