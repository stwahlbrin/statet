/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;


@NonNullByDefault
public class StructuralDiff {
	
	public enum DiffType {
		ADD, CHANGE, DELETE;
	}
	
	
	private final DiffType diffType;
	
	private final LRange beforePositionRange;
	
	private final LRange afterPositionRange;
	
	
	public StructuralDiff(final DiffType diffType, final LRange beforePositionRange, final LRange afterPositionRange) {
		this.diffType= nonNullAssert(diffType);
		this.beforePositionRange= nonNullAssert(beforePositionRange);
		this.afterPositionRange= nonNullAssert(afterPositionRange);
	}
	
	public DiffType getDiffType() {
		return this.diffType;
	}
	
	public LRange getBeforePositionRange() {
		return this.beforePositionRange;
	}
	
	public LRange getAfterPositionRange() {
		return this.afterPositionRange;
	}
	
	
	@Override
	public int hashCode() {
		return ((((this.diffType.hashCode()
				* 13) + this.beforePositionRange.hashCode())
				* 14) + this.afterPositionRange.hashCode());
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof StructuralDiff) {
			final StructuralDiff other= (StructuralDiff)obj;
			return (this.diffType == other.diffType
					&& this.beforePositionRange == other.beforePositionRange
					&& this.afterPositionRange == other.afterPositionRange );
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName()
			+ " " + this.diffType + " ("
			+ " before= " + this.beforePositionRange + ", "
			+ " after= " + this.afterPositionRange + ")";
	}
	
}
