/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.command;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;


@NonNullByDefault
public abstract class AbstractDimPositionCommand implements LayerCommand {
	
	
	private LayerDim layerDim;
	
	private long position;
	
	
	protected AbstractDimPositionCommand(final LayerDim layerDim, final long position) {
		this.layerDim= layerDim;
		this.position= position;
	}
	
	protected AbstractDimPositionCommand(final AbstractDimPositionCommand command) {
		this.layerDim= command.layerDim;
		this.position= command.position;
	}
	
	
	public final Orientation getOrientation() {
		return this.layerDim.getOrientation();
	}
	
	public final LayerDim getDim() {
		return this.layerDim;
	}
	
	public long getPosition() {
		return this.position;
	}
	
	
	@Override
	public boolean convertToTargetLayer(final Layer targetLayer) {
		final LayerDim targetLayerDim= targetLayer.getDim(getOrientation());
		if (this.layerDim == targetLayerDim) {
			return true;
		}
		
		return convertToTargetLayer(this.layerDim, this.position, targetLayerDim);
	}
	
	protected boolean convertToTargetLayer(final LayerDim dim,
			final long position, final LayerDim targetDim) {
		final long targetPosition= LayerUtils.convertPosition(dim, position, position, targetDim);
		if (targetPosition == LayerDim.POSITION_NA) {
			return false;
		}
		
		this.layerDim= targetDim;
		this.position= targetPosition;
		return true;
	}
	
}
