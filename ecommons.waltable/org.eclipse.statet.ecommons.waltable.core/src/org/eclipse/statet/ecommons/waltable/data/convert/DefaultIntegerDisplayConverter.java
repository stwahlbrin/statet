/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.convert;


/**
 * Converts the display value to an integer and vice versa.
 */
public class DefaultIntegerDisplayConverter extends NumericDisplayConverter {

	@Override
	protected Object convertToNumericValue(final String value){
		return Integer.valueOf(value);
	}
	
}
