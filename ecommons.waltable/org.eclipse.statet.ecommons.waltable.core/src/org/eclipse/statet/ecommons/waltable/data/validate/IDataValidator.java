/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.validate;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.data.convert.IDisplayConverter;
import org.eclipse.statet.ecommons.waltable.edit.editor.TextCellEditor;


public interface IDataValidator {

	/**
	 *
	 * @param columnIndex Index of the column being validated
	 * @param rowIndex Index of the row being validated
	 * @param newValue Value entered through the edit control text box, combo box etc.
	 * 	Note: In case of the {@link TextCellEditor} the text typed in by the user
	 * 	will be converted to the canonical value using the {@link IDisplayConverter}
	 * 	before it hits this method
	 *
	 * @see DataProvider#getDataValue(int, int)
	 *
	 * @return true is newValue is valid. False otherwise.
	 */
	public boolean validate(long columnIndex, long rowIndex, Object newValue);

	/**
	 * 
	 * @param cell ForwardLayerCell which should be validated
	 * @param configRegistry 
	 * @param newValue Value entered through the edit control text box, combo box etc.
	 * 	Note: In case of the {@link TextCellEditor} the text typed in by the user
	 * 	will be converted to the canonical value using the {@link IDisplayConverter}
	 * 	before it hits this method
	 *
	 * @see DataProvider#getDataValue(int, int)
	 *
	 * @return true is newValue is valid. False otherwise.
	 */
	public boolean validate(LayerCell cell, ConfigRegistry configRegistry, Object newValue);

	public static final IDataValidator ALWAYS_VALID= new IDataValidator() {

		@Override
		public boolean validate(final LayerCell cell, final ConfigRegistry configRegistry, final Object newValue) {
			return true;
		}

		@Override
		public boolean validate(final long columnIndex, final long rowIndex, final Object newValue) {
			return true;
		}

	};

	public static final IDataValidator NEVER_VALID= new IDataValidator() {

		@Override
		public boolean validate(final LayerCell cell, final ConfigRegistry configRegistry, final Object newValue) {
			return false;
		}

		@Override
		public boolean validate(final long columnIndex, final long rowIndex, final Object newValue) {
			return false;
		}

	};
}
