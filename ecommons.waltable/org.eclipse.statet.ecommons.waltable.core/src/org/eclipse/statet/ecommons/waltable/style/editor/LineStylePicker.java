/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import static org.eclipse.swt.SWT.NONE;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.waltable.Messages;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle.LineStyle;


/**
 * Component to select a {@link LineStyle}.
 */
public class LineStylePicker extends Composite {
    
    private final Combo combo;
    
    public LineStylePicker(final Composite parent) {
        super(parent, NONE);
        setLayout(new RowLayout());
        
		this.combo= new Combo(this, SWT.READ_ONLY | SWT.DROP_DOWN);
		this.combo.setItems(
				Messages.getString("LineStylePicker.solid"), //$NON-NLS-1$
				Messages.getString("LineStylePicker.dashed"), //$NON-NLS-1$
				Messages.getString("LineStylePicker.dotted"), //$NON-NLS-1$
				Messages.getString("LineStylePicker.dashdot"), //$NON-NLS-1$
				Messages.getString("LineStylePicker.dashdotdot") ); //$NON-NLS-1$
        this.combo.select(0);
    }
    
    @Override
    public void setEnabled(final boolean enabled) {
        this.combo.setEnabled(enabled);
    }
    
    public void setSelectedLineStyle(final LineStyle lineStyle) {
        int index= 0;
        if (lineStyle.equals(LineStyle.SOLID)) {
			index= 0;
		} else if (lineStyle.equals(LineStyle.DASHED)) {
			index= 1;
		} else if (lineStyle.equals(LineStyle.DOTTED)) {
			index= 2;
		} else if (lineStyle.equals(LineStyle.DASHDOT)) {
			index= 3;
		} else if (lineStyle.equals(LineStyle.DASHDOTDOT)) {
			index= 4;
		}
        this.combo.select(index);
    }
    
    public LineStyle getSelectedLineStyle() {
        final int index= this.combo.getSelectionIndex();
        if (index == 0) {
			return LineStyle.SOLID;
		} else if (index == 1) {
			return LineStyle.DASHED;
		} else if (index == 2) {
			return LineStyle.DOTTED;
		} else if (index == 3) {
			return LineStyle.DASHDOT;
		} else if (index == 4) {
			return LineStyle.DASHDOTDOT;
		}
		else {
			throw new IllegalStateException("never happen"); //$NON-NLS-1$
		}
    }

}
