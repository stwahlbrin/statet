/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.data.convert.IDisplayConverter;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public class CheckBoxPainter extends ImagePainter {

	private final Image checkedImg;
	private final Image uncheckedImg;

	public CheckBoxPainter() {
		this.checkedImg= GUIHelper.getImage("checked"); //$NON-NLS-1$
		this.uncheckedImg= GUIHelper.getImage("unchecked"); //$NON-NLS-1$
	}

	public CheckBoxPainter(final Image checkedImg, final Image uncheckedImg) {
		super();
		this.checkedImg= checkedImg;
		this.uncheckedImg= uncheckedImg;
	}

	public long getPreferredWidth(final boolean checked) {
		return checked ? this.checkedImg.getBounds().width : this.uncheckedImg.getBounds().width;
	}

	public long getPreferredHeight(final boolean checked) {
		return checked ? this.checkedImg.getBounds().height : this.uncheckedImg.getBounds().height;
	}

	@Override
	protected Image getImage(final LayerCell cell, final ConfigRegistry configRegistry) {
		return isChecked(cell, configRegistry) ? this.checkedImg : this.uncheckedImg;
	}

	protected boolean isChecked(final LayerCell cell, final ConfigRegistry configRegistry) {
		return convertDataType(cell, configRegistry).booleanValue();
	}

	protected Boolean convertDataType(final LayerCell cell, final ConfigRegistry configRegistry) {
		if (cell.getDataValue(0, null) instanceof Boolean) {
			return (Boolean) cell.getDataValue(0, null);
		}
		final IDisplayConverter displayConverter= configRegistry.getAttribute(CellConfigAttributes.DISPLAY_CONVERTER, cell.getDisplayMode(), cell.getLabels().getLabels());
		Boolean convertedValue= null;
		if (displayConverter != null) {
			convertedValue= (Boolean) displayConverter.canonicalToDisplayValue(cell, configRegistry, cell.getDataValue(0, null));
		}
		if (convertedValue == null) {
			convertedValue= Boolean.FALSE;
		}
		return convertedValue;
	}
}
