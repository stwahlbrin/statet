/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Pattern;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;


/**
 * Paints the cell background using an image.
 * Image is repeated to cover the background. Similar to HTML table painting.
 */
public class BackgroundImagePainter extends CellPainterWrapper {

	public final Color separatorColor;
	private final Image bgImage;

	/**
	 * @param interiorPainter used for painting the cell contents
	 * @param bgImage to be used for painting the background
	 * @param separatorColor to be used for drawing left and right borders for the cell.
	 * 	Set to null if the borders are not required.
	 */
	public BackgroundImagePainter(final LayerCellPainter interiorPainter, final Image bgImage, final Color separatorColor) {
		super(interiorPainter);
		this.bgImage= bgImage;
		this.separatorColor= separatorColor;
	}

	@Override
	public long getPreferredWidth(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		return super.getPreferredWidth(cell, gc, configRegistry) + 4;
	}

	@Override
	public long getPreferredHeight(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		return super.getPreferredHeight(cell, gc, configRegistry) + 4;
	}

	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle lRectangle, final ConfigRegistry configRegistry) {
		// Save GC settings
		final Color originalBackground= gc.getBackground();
		final Color originalForeground= gc.getForeground();

		final Pattern pattern= new Pattern(Display.getCurrent(), this.bgImage);
		gc.setBackgroundPattern(pattern);

		gc.fillRectangle(safe(lRectangle));
		
		gc.setBackgroundPattern(null);
		pattern.dispose();

		if (this.separatorColor != null) {
			gc.setForeground(this.separatorColor);
			gc.drawLine(safe(lRectangle.x - 1), safe(lRectangle.y), safe(lRectangle.x - 1), safe(lRectangle.y + lRectangle.height));
			gc.drawLine(safe(lRectangle.x - 1 + lRectangle.width), safe(lRectangle.y), safe(lRectangle.x - 1 + lRectangle.width), safe(lRectangle.y + lRectangle.height));
		}

		// Restore original GC settings
		gc.setBackground(originalBackground);
		gc.setForeground(originalForeground);

		// Draw interior
		final LRectangle interiorBounds= new LRectangle(safe(lRectangle.x + 2), safe(lRectangle.y + 2), safe(lRectangle.width - 4), safe(lRectangle.height - 4));
		super.paintCell(cell, gc, interiorBounds, configRegistry);
	}

}
