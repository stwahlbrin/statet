/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.util.data;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;


@NonNullByDefault
public class PropertyColumnHeaderDataProvider implements DataProvider {
	
	
	private final ImList<String> propertyNames;
	
	private final @Nullable Map<String, String> propertyToLabelMap;
	
	
	public PropertyColumnHeaderDataProvider(final ImList<String> propertyNames,
			final Map<String, String> propertyToLabelMap) {
		this.propertyNames= nonNullAssert(propertyNames);
		this.propertyToLabelMap= propertyToLabelMap;
	}
	
	
	@Override
	public long getColumnCount() {
		return this.propertyNames.size();
	}
	
	@Override
	public long getRowCount() {
		return 1;
	}
	
	/**
	 * This class does not support multiple rows in the column header layer.
	 */
	@Override
	public Object getDataValue(final long columnIndex, final long rowIndex, final int flags,
			final @Nullable IProgressMonitor monitor) {
		if (columnIndex < 0 || columnIndex >= this.propertyNames.size()) {
			throw new IndexOutOfBoundsException();
		}
		final String propertyName= this.propertyNames.get((int)columnIndex);
		if (this.propertyToLabelMap != null) {
			final String label= this.propertyToLabelMap.get(propertyName);
			if (label != null) {
				return label;
			}
		}
		return propertyName;
	}
	
	@Override
	public void setDataValue(final long columnIndex, final long rowIndex, final @Nullable Object newValue) {
		throw new UnsupportedOperationException();
	}
	
}
