/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.refresh.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.events.GeneralVisualChangeEvent;


/**
 * Command that triggers a {@link GeneralVisualChangeEvent}.
 * To support refreshing of every layer in a NatTable the
 * {@link VisualRefreshCommandHandler} should be registered 
 * against the DataLayer.
 */
@NonNullByDefault
public class VisualRefreshCommand extends AbstractContextFreeCommand {
	
	
}
