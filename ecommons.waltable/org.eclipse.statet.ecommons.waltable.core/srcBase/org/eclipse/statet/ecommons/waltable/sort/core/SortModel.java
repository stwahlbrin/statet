/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Interface providing sorting functionality.
 */
@NonNullByDefault
public interface SortModel {
	
	
	/**
	 * @return List of column ids that are sorted.
	 */
	List<Long> getSortedColumnIds();
	
	/**
	 * @return TRUE if the column with the given id is sorted at the moment.
	 */
	boolean isSorted(long columnId);
	
	/**
	 * @return the direction in which the column with the given id is
	 * currently sorted
	 */
	SortDirection getSortDirection(long columnId);
	
	/**
	 * @return when multiple columns are sorted, this returns the order of the
	 * column id in the sort
	 * <p>
	 * Example: If column ids 3, 6, 9 are sorted (in that order) the sort order
	 * for id 6 is 1.
	 */
	int getSortOrder(long columnId);
	
	/**
	 * This method is called by the {@link SortCommandHandler} in response to a sort command.
	 * It is responsible for sorting the requested column.
	 *
	 * @param accumulate flag indicating if the column should added to a previous sort.
	 */
	void sort(long columnId, SortDirection sortDirection, boolean accumulate);
	
	/**
	 * Remove all sorting
	 */
	void clear();
	
}
