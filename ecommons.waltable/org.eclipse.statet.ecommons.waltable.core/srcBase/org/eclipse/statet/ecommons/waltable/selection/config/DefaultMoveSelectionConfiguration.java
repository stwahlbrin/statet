/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.config;

import org.eclipse.statet.ecommons.waltable.config.AbstractLayerConfiguration;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;


/**
 * Configure the behavior when the selection is moved. Example: by using arrow keys.<br/>
 * This default configuration moves by cell.<br/>
 * 
 * {@link MoveSelectionCommand} are fired by the {@link DefaultSelectionBindings}.<br/>
 * An suitable handler can be plugged in to handle the move commands as required.<br/>
 * 
 * @see MoveRowSelectionCommandHandler
 */
public class DefaultMoveSelectionConfiguration extends AbstractLayerConfiguration<SelectionLayer>{

	@Override
	public void configureTypedLayer(final SelectionLayer layer) {
//		layer.registerCommandHandler(new MoveCellSelectionCommandHandler(layer));
	}

}
