/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Direction;


@NonNullByDefault
public class ScrollStepCommandHandler implements LayerCommandHandler<ScrollStepCommand> {
	
	
	private final ViewportLayer viewportLayer;
	
	
	public ScrollStepCommandHandler(final ViewportLayer viewportLayer) {
		this.viewportLayer= viewportLayer;
	}
	
	@Override
	public Class<ScrollStepCommand> getCommandClass() {
		return ScrollStepCommand.class;
	}
	
	
	@Override
	public boolean executeCommand(final ScrollStepCommand command) {
		final Direction direction= command.getDirection();
		final var viewportDim= this.viewportLayer.getDim(direction.getOrientation());
		if (direction.isBackward()) {
			viewportDim.scrollBackwardByStep();
		}
		else /*if (direction.isForward())*/ {
			viewportDim.scrollForwardByStep();
		}
		return true;
	}
	
}
