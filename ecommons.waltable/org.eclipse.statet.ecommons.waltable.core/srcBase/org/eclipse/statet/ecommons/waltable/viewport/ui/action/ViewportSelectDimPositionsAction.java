/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.ui.action;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.selection.ui.action.SelectCellAction;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportSelectDimPositionsCommand;


/**
 * Action to select the column/row at the mouse position.
 */
@NonNullByDefault
public class ViewportSelectDimPositionsAction implements IMouseAction {
	
	
	private final Orientation orientation;
	
	
	public ViewportSelectDimPositionsAction(final Orientation orientation) {
		this.orientation= orientation;
	}
	
	
	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		final LayerCell cell= natTable.getCellByPosition(
				natTable.getColumnPositionByX(event.x),
				natTable.getRowPositionByY(event.y) );
		if (cell == null) {
			return;
		}
		final LayerDim layerDim= natTable.getDim(this.orientation);
		final LayerCellDim cellDim= cell.getDim(this.orientation);
		if (cellDim.getPositionSpan() > 1) {
			final var positions= ImCollections.newList(
					new LRange(cellDim.getOriginPosition(), cellDim.getOriginPosition() + cellDim.getPositionSpan()) );
			natTable.doCommand(new ViewportSelectDimPositionsCommand(layerDim,
					cellDim.getPosition(), positions, cellDim.getPosition(),
					SelectCellAction.swt2Flags(event.stateMask) ));
		}
		else {
			natTable.doCommand(new ViewportSelectDimPositionsCommand(layerDim,
					cellDim.getPosition(),
					SelectCellAction.swt2Flags(event.stateMask) ));
		}
	}

}
