/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;

import org.eclipse.statet.ecommons.waltable.config.AbstractRegistryConfiguration;
import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle.LineStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.style.SelectionStyleLabels;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


/**
 * Sets up rendering style used for selected areas and the selection anchor.
 */
public class DefaultSelectionStyleConfiguration extends AbstractRegistryConfiguration {
	
	// Selection style
	public Font selectionFont= GUIHelper.getFont(new FontData("Verdana", 8, SWT.BOLD | SWT.ITALIC)); //$NON-NLS-1$
	public Color selectionBgColor= GUIHelper.COLOR_TITLE_INACTIVE_BACKGROUND;
	public Color selectionFgColor= GUIHelper.COLOR_BLACK;
	
	// Anchor style
	public Color anchorBorderColor= GUIHelper.COLOR_DARK_GRAY;
	public BorderStyle anchorBorderStyle= new BorderStyle(1, this.anchorBorderColor, LineStyle.SOLID);
	public Color anchorBgColor= GUIHelper.COLOR_GRAY;
	public Color anchorFgColor= GUIHelper.COLOR_WHITE;
	
	// Selected headers style
	public Color selectedHeaderBgColor= GUIHelper.COLOR_GRAY;
	public Color selectedHeaderFgColor= GUIHelper.COLOR_WHITE;
	public Font selectedHeaderFont= GUIHelper.getFont(new FontData("Verdana", 10, SWT.BOLD)); //$NON-NLS-1$
	public BorderStyle selectedHeaderBorderStyle= new BorderStyle(-1, this.selectedHeaderFgColor, LineStyle.SOLID);
	
	// Anchor grid line style
	public Color anchorGridBorderColor= GUIHelper.COLOR_BLACK;
	public BorderStyle anchorGridBorderStyle= new BorderStyle(1, this.anchorGridBorderColor, LineStyle.DOTTED);
	
	
	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		configureSelectionStyle(configRegistry);
		configureSelectionAnchorStyle(configRegistry);
		configureSelectionAnchorGridLineStyle(configRegistry);
		configureHeaderHasSelectionStyle(configRegistry);
		configureHeaderFullySelectedStyle(configRegistry);
	}
	
	protected void configureSelectionStyle(final ConfigRegistry configRegistry) {
		final BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.FONT, this.selectionFont);
		cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.selectionBgColor);
		cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.selectionFgColor);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED );
	}
	
	protected void configureSelectionAnchorStyle(final ConfigRegistry configRegistry) {
		// Selection anchor style for normal display mode
		BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, this.anchorBorderStyle);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.NORMAL, SelectionStyleLabels.SELECTION_ANCHOR_STYLE );
		
		// Selection anchor style for select display mode
		cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.anchorBgColor);
		cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.anchorFgColor);
		cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, this.anchorBorderStyle);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED, SelectionStyleLabels.SELECTION_ANCHOR_STYLE );
	}
	
	protected void configureSelectionAnchorGridLineStyle(final ConfigRegistry configRegistry) {
		final BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, this.anchorGridBorderStyle);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED, SelectionStyleLabels.SELECTION_ANCHOR_GRID_LINE_STYLE );
	}
	
	protected void configureHeaderHasSelectionStyle(final ConfigRegistry configRegistry) {
		final BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.selectedHeaderFgColor);
		cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.selectedHeaderBgColor);
		cellStyle.setAttributeValue(CellStyling.FONT, this.selectedHeaderFont);
		cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, this.selectedHeaderBorderStyle);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED, GridLabels.COLUMN_HEADER );
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED, GridLabels.CORNER );
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED, GridLabels.ROW_HEADER );
	}
	
	protected void configureHeaderFullySelectedStyle(final ConfigRegistry configRegistry) {
		// Header fully selected
		final BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, GUIHelper.COLOR_WIDGET_NORMAL_SHADOW);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED, SelectionStyleLabels.COLUMN_FULLY_SELECTED_STYLE);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
				DisplayMode.SELECTED, SelectionStyleLabels.ROW_FULLY_SELECTED_STYLE);
	}
	
}
