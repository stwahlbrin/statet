/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.util.data;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;


public class DummyColumnHeaderDataProvider implements DataProvider {
	
	
	private final DataProvider bodyDataProvider;
	
	
	public DummyColumnHeaderDataProvider(final DataProvider bodyDataProvider) {
		this.bodyDataProvider= bodyDataProvider;
	}
	
	@Override
	public long getColumnCount() {
		return this.bodyDataProvider.getColumnCount();
	}
	
	@Override
	public long getRowCount() {
		return 1;
	}
	
	@Override
	public Object getDataValue(final long columnIndex, final long rowIndex, final int flags, final IProgressMonitor monitor) {
		return "Column " + (columnIndex + 1); //$NON-NLS-1$
	}
	
	@Override
	public void setDataValue(final long columnIndex, final long rowIndex, final Object newValue) {
		throw new UnsupportedOperationException();
	}
	
}
