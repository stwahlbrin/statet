/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.labeled;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.CompositeLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.PlaceholderLayer;


@NonNullByDefault
public class ExtColumnHeaderLayer extends CompositeLayer {
	
	
	public ExtColumnHeaderLayer(final Layer columnHeaderLayer) {
		super(1, 2);
		
		setChildLayer(GridLabels.COLUMN_HEADER, columnHeaderLayer, 0, 0);
		setChildLayer(GridLabels.HEADER_PLACEHOLDER, new PlaceholderLayer(columnHeaderLayer, null,
				false, columnHeaderLayer.getLayerPainter() ), 0, 1);
	}
	
	
	@Override
	protected boolean ignoreRef(final Orientation orientation) {
		return (orientation == VERTICAL);
	}
	
	
	public void setSpaceSize(final int pixel) {
		((PlaceholderLayer)getChildByLabel(GridLabels.HEADER_PLACEHOLDER).layer).setSize(pixel);
	}
	
}
