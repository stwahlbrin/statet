/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.ui.action;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.selection.config.RowOnlySelectionBindings;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectDimPositionsCommand;


/**
 * Selects the entire row when the mouse is dragged on the body.
 * <i>Multiple</i> rows are selected as the user drags.
 * 
 * @see RowOnlySelectionBindings
 */
public class RowSelectionDragMode extends CellSelectionDragMode {
	
	
	public RowSelectionDragMode() {
	}
	
	
	@Override
	protected void fireSelectionCommand(final NatTable natTable,
			final long columnPosition,	final long rowPosition, final int selectionFlags) {
		natTable.doCommand(new SelectDimPositionsCommand(natTable.getDim(VERTICAL),
				rowPosition, columnPosition, selectionFlags ));
	}
	
}
