/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.events.CellVisualChangeEvent;


@NonNullByDefault
public class CellSelectionEvent extends CellVisualChangeEvent implements SelectionEvent {
	
	
	private final SelectionLayer selectionLayer;
	
	private final boolean revealCell;
	
	
	public CellSelectionEvent(final SelectionLayer selectionLayer,
			final long columnPosition, final long rowPosition,
			final boolean revealCell) {
		super(selectionLayer, columnPosition, rowPosition);
		this.selectionLayer= selectionLayer;
		this.revealCell= revealCell;
	}
	
	protected CellSelectionEvent(final SelectionLayer selectionLayer,
			final Layer layer, final long columnPosition, final long rowPosition, final boolean revealCell) {
		super(layer, columnPosition, rowPosition);
		this.selectionLayer= selectionLayer;
		this.revealCell= revealCell;
	}
	
	@Override
	protected @Nullable CellSelectionEvent toLayer(final Layer targetLayer,
			final long columnPosition, final long rowPosition) {
		return new CellSelectionEvent(this.selectionLayer,
				targetLayer, columnPosition, rowPosition, this.revealCell );
	}
	
	
	@Override
	public SelectionLayer getSelectionLayer() {
		return this.selectionLayer;
	}
	
	public boolean getRevealCell() {
		return this.revealCell;
	}
	
}
