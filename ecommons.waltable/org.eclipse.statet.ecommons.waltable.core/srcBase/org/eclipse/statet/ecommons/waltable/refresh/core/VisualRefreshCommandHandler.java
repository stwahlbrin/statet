/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.refresh.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.events.GeneralVisualChangeEvent;


/**
 * Command handler for handling {@link VisualRefreshCommand}s.
 * Simply fires a {@link GeneralVisualChangeEvent}.
 * 
 * Needed to be able to refresh all layers by simply calling a command on the NatTable
 * instance itself (Remember that events are fired bottom up the layer stack while commands
 * are propagated top down). 
 * 
 * To refresh all layers by calling a {@link VisualRefreshCommand} on the NatTable
 * instance, the {@link VisualRefreshCommandHandler} should be registered against
 * the DataLayer.
 */
@NonNullByDefault
public class VisualRefreshCommandHandler implements LayerCommandHandler<VisualRefreshCommand> {
	
	
	private final Layer layer;
	
	
	public VisualRefreshCommandHandler(final Layer layer) {
		this.layer= layer;
	}
	
	
	@Override
	public Class<VisualRefreshCommand> getCommandClass() {
		return VisualRefreshCommand.class;
	}
	
	@Override
	public boolean executeCommand(final VisualRefreshCommand command) {
		this.layer.fireLayerEvent(new GeneralVisualChangeEvent(this.layer));
		return true;
	}

}
