/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;


@NonNullByDefault
public class PositionResizeCommandHandler extends AbstractLayerCommandHandler<DimPositionResizeCommand> {
	
	
	private final DataLayer dataLayer;
	
	
	public PositionResizeCommandHandler(final DataLayer dataLayer) {
		this.dataLayer= dataLayer;
	}
	
	
	@Override
	public Class<DimPositionResizeCommand> getCommandClass() {
		return DimPositionResizeCommand.class;
	}
	
	@Override
	protected boolean doCommand(final DimPositionResizeCommand command) {
		if (command.getOrientation() == HORIZONTAL) {
			this.dataLayer.setColumnWidthByPosition(command.getPosition(), command.getNewSize());
		}
		else {
			this.dataLayer.setRowHeightByPosition(command.getPosition(), command.getNewSize());
		}
		return true;
	}
	
}
