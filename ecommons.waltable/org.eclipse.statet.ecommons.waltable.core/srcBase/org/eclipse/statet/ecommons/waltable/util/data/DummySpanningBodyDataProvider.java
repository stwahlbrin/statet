/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.util.data;

import org.eclipse.statet.ecommons.waltable.core.data.DataCell;
import org.eclipse.statet.ecommons.waltable.core.data.SpanningDataProvider;


public class DummySpanningBodyDataProvider extends DummyBodyDataProvider implements SpanningDataProvider {
	
	private static final long BLOCK_SIZE= 4;
	
	private static final long CELL_SPAN= 2;
	
	
	public DummySpanningBodyDataProvider(final long columnCount, final long rowCount) {
		super(columnCount, rowCount);
	}
	
	@Override
	public DataCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final long columnBlock= columnPosition / BLOCK_SIZE;
		final long rowBlock= rowPosition / BLOCK_SIZE;
		
		final boolean isSpanned= isEven(columnBlock + rowBlock) && (columnPosition % BLOCK_SIZE) < CELL_SPAN && (rowPosition % BLOCK_SIZE) < CELL_SPAN;
		final long columnSpan= isSpanned ? CELL_SPAN : 1;
		final long rowSpan= isSpanned ? CELL_SPAN : 1;
		
		long cellColumnPosition= columnPosition;
		long cellRowPosition= rowPosition;
		
		if (isSpanned) {
			cellColumnPosition-= columnPosition % BLOCK_SIZE;
			cellRowPosition-= rowPosition % BLOCK_SIZE;
		}
		
		return new DataCell(cellColumnPosition, cellRowPosition, columnSpan, rowSpan);
	}
	
	private boolean isEven(final long i) {
		return i % 2 == 0;
	}
	
}
