/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.CompositeLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;
import org.eclipse.statet.ecommons.waltable.export.ExportCommandHandler;
import org.eclipse.statet.ecommons.waltable.grid.config.DefaultGridLayerConfiguration;
import org.eclipse.statet.ecommons.waltable.print.PrintCommandHandler;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectCellCommand;


/**
 * Top level layer. It is composed of the smaller child layers: RowHeader,
 * ColumnHeader, Corner and Body It does not have its own coordinate system
 * unlike the other layers. It simply delegates most functions to its child
 * layers.
 */
@NonNullByDefault
public class GridLayer extends CompositeLayer {
	
	public GridLayer(final Layer bodyLayer, final Layer columnHeaderLayer, final Layer rowHeaderLayer, final Layer cornerLayer) {
		this(bodyLayer, columnHeaderLayer, rowHeaderLayer, cornerLayer, true);
	}
	
	public GridLayer(final Layer bodyLayer, final Layer columnHeaderLayer, final Layer rowHeaderLayer, final Layer cornerLayer, final boolean useDefaultConfiguration) {
		super(2, 2);
		
		setBodyLayer(bodyLayer);
		setColumnHeaderLayer(columnHeaderLayer);
		setRowHeaderLayer(rowHeaderLayer);
		setCornerLayer(cornerLayer);
		
		init(useDefaultConfiguration);
	}

	protected GridLayer(final boolean useDefaultConfiguration) {
		super(2, 2);
		init(useDefaultConfiguration);
	}

	protected void init(final boolean useDefaultConfiguration) {
		registerCommandHandlers();
		
		if (useDefaultConfiguration) {
			addConfiguration(new DefaultGridLayerConfiguration(this));
		}
	}

	@Override
	protected void registerCommandHandlers() {
		registerCommandHandler(new PrintCommandHandler(this));
		registerCommandHandler(new ExportCommandHandler(this));
//		registerCommandHandler(new AutoResizeColumnCommandHandler(this));
//		registerCommandHandler(new AutoResizeRowCommandHandler(this));
	}
	
	/**
	 * How the GridLayer processes commands is very important. <strong>Do not
	 * change this unless you know what you are doing and understand the full
	 * ramifications of your change. Otherwise your grid will not behave
	 * correctly!</strong>
	 * 
	 * The Body is always given the first chance to process a command. There are
	 * two reasons for this: (1) most commands (80%) are destined for the body
	 * anyways so it's faster to check there first (2) the other layers all
	 * transitively depend on the body so it's not wise to ask them to do stuff
	 * until after the body has done it. This is especially true of grid
	 * initialization where the body must be initialized before any of its
	 * dependent layers.
	 * 
	 * Because of this, if you want to intercept well-known commands to
	 * implement custom behavior (for example, you want to intercept the
	 * {@link SelectCellCommand}) then <strong>you must inject your special
	 * layer into the body. </strong> An injected column or row header will
	 * never see the command because it will be consumed first by the body. In
	 * practice, it's a good idea to implement all your command-handling logic
	 * in the body.
	 **/
	@Override
	protected boolean doCommandOnChildLayers(final LayerCommand command) {
		if (getBodyLayer().doCommand(command)) {
			return true;
		}
		else if (getColumnHeaderLayer().doCommand(command)) {
			return true;
		}
		else if (getRowHeaderLayer().doCommand(command)) {
			return true;
		}
		else {
			return getCornerLayer().doCommand(command);
		}
	}
	
	// Sub-layer accessors
	
	public Layer getCornerLayer() {
		return getChildLayerByLayoutCoordinate(0, 0);
	}
	
	public void setCornerLayer(final Layer cornerLayer) {
		setChildLayer(GridLabels.CORNER, cornerLayer, 0, 0);
	}
	
	public Layer getColumnHeaderLayer() {
		return getChildLayerByLayoutCoordinate(1, 0);
	}
	
	public void setColumnHeaderLayer(final Layer columnHeaderLayer) {
		setChildLayer(GridLabels.COLUMN_HEADER, columnHeaderLayer, 1, 0);
	}
	
	public Layer getRowHeaderLayer() {
		return getChildLayerByLayoutCoordinate(0, 1);
	}
	
	public void setRowHeaderLayer(final Layer rowHeaderLayer) {
		setChildLayer(GridLabels.ROW_HEADER, rowHeaderLayer, 0, 1);
	}
	
	public Layer getBodyLayer() {
		return getChildLayerByLayoutCoordinate(1, 1);
	}
	
	public void setBodyLayer(final Layer bodyLayer) {
		setChildLayer(GridLabels.BODY, bodyLayer, 1, 1);
		
		//update the command handlers for auto resize because of the connection to the body layer stack
//		unregisterCommandHandler(AutoResizePositionsCommand.class);
//		unregisterCommandHandler(AutoResizeRowsCommand.class);
		
//		registerCommandHandler(new AutoResizeColumnCommandHandler(this));
//		registerCommandHandler(new AutoResizeRowCommandHandler(this));		
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName()
				+ "[corner=" + getCornerLayer() //$NON-NLS-1$
				+ " columnHeader=" + getColumnHeaderLayer() //$NON-NLS-1$
				+ " rowHeader=" + getRowHeaderLayer() //$NON-NLS-1$
				+ " bodyLayer=" + getBodyLayer() + "]"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	
	@Override
	public boolean doCommand(final LayerCommand command) {
		if (command instanceof ClientAreaResizeCommand) {
			final ClientAreaResizeCommand resizeCommand= (ClientAreaResizeCommand)command;
			final LRectangle possibleArea= SwtUtils.toNatTable(resizeCommand.getScrollable().getClientArea());
			
			//remove the column header height and the row header width from the client area to 
			//ensure that only the body region is used for percentage calculation
			final LRectangle rowLayerArea= getRowHeaderLayer().getClientAreaProvider().getClientArea();
			final LRectangle columnLayerArea= getColumnHeaderLayer().getClientAreaProvider().getClientArea();
			
			resizeCommand.setCalcArea(new LRectangle(
					possibleArea.x,
					possibleArea.y,
					possibleArea.width - rowLayerArea.width,
					possibleArea.height - columnLayerArea.height ));
		}
		return super.doCommand(command);
	}
	
}
