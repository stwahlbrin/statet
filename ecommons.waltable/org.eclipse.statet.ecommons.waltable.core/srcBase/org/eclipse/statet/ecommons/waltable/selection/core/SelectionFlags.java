/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class SelectionFlags {
	
	
	/** Extend current selection */
	public static final int RANGE_SELECTION= 1 << 17; // SWT.SHIFT;
	/** Retain or toggle */
	public static final int RETAIN_SELECTION= 1 << 18; // SWT.CTRL;
	
	
}
