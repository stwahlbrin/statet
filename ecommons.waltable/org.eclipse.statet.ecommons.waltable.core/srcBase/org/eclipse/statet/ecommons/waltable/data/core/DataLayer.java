/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.AbstractLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.events.GeneralStructuralChangeEvent;
import org.eclipse.statet.ecommons.waltable.edit.UpdateDataCommandHandler;
import org.eclipse.statet.ecommons.waltable.refresh.core.StructuralRefreshCommandHandler;
import org.eclipse.statet.ecommons.waltable.refresh.core.VisualRefreshCommandHandler;
import org.eclipse.statet.ecommons.waltable.resize.core.DimResizeEvent;
import org.eclipse.statet.ecommons.waltable.resize.core.MultiColumnResizeCommandHandler;
import org.eclipse.statet.ecommons.waltable.resize.core.MultiRowResizeCommandHandler;
import org.eclipse.statet.ecommons.waltable.resize.core.PositionResizeCommandHandler;


/**
 * Wraps the {@link DataProvider}, and serves as the data source for all
 * other layers. Also, tracks the size of the columns and the rows using
 * {@link SizeConfig} objects. Since this layer sits directly on top of the
 * data source, at this layer index == position.
 */
@NonNullByDefault
public class DataLayer extends AbstractLayer<DataLayerDim<? extends DataLayer>> implements Layer {
	
	
	protected class DataLayerCell extends BasicLayerCell {
		
		
		public DataLayerCell(final LayerCellDim horizontalDim, final LayerCellDim verticalDim) {
			super(DataLayer.this, horizontalDim, verticalDim);
		}
		
		
		@Override
		public @Nullable Object getDataValue(final int flags,
				final @Nullable IProgressMonitor monitor) {
			return DataLayer.this.dataProvider.getDataValue(getColumnPosition(), getRowPosition(),
					flags, monitor );
		}
		
	}
	
	
	public static final String PERSISTENCE_KEY_ROW_HEIGHT= ".rowHeight"; //$NON-NLS-1$
	public static final String PERSISTENCE_KEY_COLUMN_WIDTH= ".columnWidth"; //$NON-NLS-1$
	
	public static final int DEFAULT_COLUMN_WIDTH= 100;
	public static final int DEFAULT_ROW_HEIGHT= 20;
	
	
	protected DataProvider dataProvider;
	
	private final long columnIdCat;
	private final SizeConfig columnWidthConfig;
	
	private final long rowIdCat;
	private final SizeConfig rowHeightConfig;
	
	
	public DataLayer(final DataProvider dataProvider, final long idCat) {
		this(dataProvider,
				idCat, DEFAULT_COLUMN_WIDTH,
				idCat, DEFAULT_ROW_HEIGHT );
	}
	
	public DataLayer(final DataProvider dataProvider,
			final long columnIdCat, final int defaultColumnWidth,
			final long rowIdCat, final int defaultRowHeight) {
		this(dataProvider,
				columnIdCat, new SizeConfig(defaultColumnWidth),
				rowIdCat, new SizeConfig(defaultRowHeight) );
	}
	
	public DataLayer(final DataProvider dataProvider,
			final long columnIdCat, final SizeConfig columnWidthConfig,
			final long rowIdCat, final SizeConfig rowHeightConfig) {
		this.columnIdCat= columnIdCat;
		this.columnWidthConfig= columnWidthConfig;
		this.rowIdCat= rowIdCat;
		this.rowHeightConfig= rowHeightConfig;
		
		init();
		
		registerCommandHandlers();
		
		setDataProvider(dataProvider);
	}
	
	@Override
	protected DataLayerDim<? extends DataLayer> createDim(final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return new SizeConfigDim<>(this, HORIZONTAL, this.columnIdCat, this.columnWidthConfig) {
				@Override
				public long getPositionCount() {
					return DataLayer.this.dataProvider.getColumnCount();
				}
			};
		case VERTICAL:
			return new SizeConfigDim<>(this, VERTICAL, this.rowIdCat, this.rowHeightConfig) {
				@Override
				public long getPositionCount() {
					return DataLayer.this.dataProvider.getRowCount();
				}
			};
		default:
			throw new IllegalStateException();
		}
	}
	
	
	// Persistence
	
	@Override
	public void saveState(final String prefix, final Map<String, String> properties) {
		super.saveState(prefix, properties);
		this.columnWidthConfig.saveState(prefix + PERSISTENCE_KEY_COLUMN_WIDTH, properties);
		this.rowHeightConfig.saveState(prefix + PERSISTENCE_KEY_ROW_HEIGHT, properties);
	}
	
	@Override
	public void loadState(final String prefix, final Map<String, String> properties) {
		super.loadState(prefix, properties);
		this.columnWidthConfig.loadState(prefix + PERSISTENCE_KEY_COLUMN_WIDTH, properties);
		this.rowHeightConfig.loadState(prefix + PERSISTENCE_KEY_ROW_HEIGHT, properties);
		fireLayerEvent(new GeneralStructuralChangeEvent(this));
	}
	
	// Configuration
	
	@Override
	protected void registerCommandHandlers() {
		registerCommandHandler(new PositionResizeCommandHandler(this));
		registerCommandHandler(new MultiColumnResizeCommandHandler(this));
		registerCommandHandler(new MultiRowResizeCommandHandler(this));
		registerCommandHandler(new UpdateDataCommandHandler(this));
		registerCommandHandler(new StructuralRefreshCommandHandler(this));
		registerCommandHandler(new VisualRefreshCommandHandler(this));
	}
	
	public DataProvider getDataProvider() {
		return this.dataProvider;
	}
	
	protected void setDataProvider(final DataProvider dataProvider) {
		if (this.dataProvider instanceof Persistable) {
			unregisterPersistable((Persistable) this.dataProvider);
		}
		
		this.dataProvider= dataProvider;
		
		if (dataProvider instanceof Persistable) {
			registerPersistable((Persistable) dataProvider);
		}
	}
	
	
	// Column Width
	
	public void setColumnWidthByPosition(final long columnPosition, final int width) {
		this.columnWidthConfig.setSize(columnPosition, width);
		fireLayerEvent(new DimResizeEvent(getDim(HORIZONTAL), new LRange(columnPosition)));
	}
	
	public void setDefaultColumnWidth(final int width) {
		this.columnWidthConfig.setDefaultSize(width);
	}
	
	public void setDefaultColumnWidthByPosition(final long columnPosition, final int width) {
		this.columnWidthConfig.setDefaultSize(columnPosition, width);
	}
	
	// Column Resize
	
	public void setColumnPositionResizable(final long columnPosition, final boolean resizable) {
		this.columnWidthConfig.setPositionResizable(columnPosition, resizable);
	}
	
	public void setColumnsResizableByDefault(final boolean resizableByDefault) {
		this.columnWidthConfig.setResizableByDefault(resizableByDefault);
	}
	
	// Vertical features
	
	// Row Height
	
	public void setRowHeightByPosition(final long rowPosition, final int height) {
		this.rowHeightConfig.setSize(rowPosition, height);
		fireLayerEvent(new DimResizeEvent(getDim(VERTICAL), new LRange(rowPosition)));
	}
	
	public void setDefaultRowHeight(final int height) {
		this.rowHeightConfig.setDefaultSize(height);
	}
	
	public void setDefaultRowHeightByPosition(final long rowPosition, final int height) {
		this.rowHeightConfig.setDefaultSize(rowPosition, height);
	}
	
	// Row Resize
	
	public void setRowPositionResizable(final long rowPosition, final boolean resizable) {
		this.rowHeightConfig.setPositionResizable(rowPosition, resizable);
	}
	
	public void setRowsResizableByDefault(final boolean resizableByDefault) {
		this.rowHeightConfig.setResizableByDefault(resizableByDefault);
	}
	
	
	// Cell features
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final LayerDim hDim= getDim(HORIZONTAL);
		final LayerDim vDim= getDim(VERTICAL);
		final long columnId= hDim.getPositionId(columnPosition, columnPosition);
		final long rowId= vDim.getPositionId(rowPosition, rowPosition);
		
		return new DataLayerCell(
				new BasicLayerCellDim(HORIZONTAL, columnId, columnPosition),
				new BasicLayerCellDim(VERTICAL, rowId, rowPosition) );
	}
	
	@Override
	public @Nullable Layer getUnderlyingLayerByPosition(final long columnPosition, final long rowPosition) {
		return null;
	}
	
}
