/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.layers;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.layer.config.DefaultRowHeaderLayerConfiguration;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.style.SelectionStyleLabels;


/**
 * Layer for the row headers of the grid layer
 */
@NonNullByDefault
public class RowHeaderLayer extends AbstractPositionHeaderLayer {
	
	
	/**
	 * Creates a row header layer using the default configuration and painter
	 * 
	 * @param baseLayer
	 *            The data provider for this layer
	 * @param verticalLayerDependency
	 *            The layer to link the vertical dimension to, typically the body layer
	 * @param selectionLayer
	 *            The selection layer required to respond to selection events
	 */
	public RowHeaderLayer(final Layer baseLayer, final Layer verticalLayerDependency, final SelectionLayer selectionLayer) {
		this(baseLayer, verticalLayerDependency, selectionLayer, true);
	}
	
	public RowHeaderLayer(final Layer baseLayer, final Layer verticalLayerDependency, final SelectionLayer selectionLayer,
			final boolean useDefaultConfiguration) {
		this(baseLayer, verticalLayerDependency, selectionLayer, useDefaultConfiguration, null);
	}
	
	/**
	 * @param baseLayer
	 *            The data provider for this layer
	 * @param verticalLayerDependency
	 *            The layer to link the vertical dimension to, typically the body layer
	 * @param selectionLayer
	 *            The selection layer required to respond to selection events
	 * @param useDefaultConfiguration
	 *            If default configuration should be applied to this layer
	 * @param layerPainter
	 *            The painter for this layer or <code>null</code> to use the painter of the base layer
	 */
	public RowHeaderLayer(final Layer baseLayer, final Layer verticalLayerDependency,
			final SelectionLayer selectionLayer, final boolean useDefaultConfiguration,
			final LayerPainter layerPainter) {
		super(baseLayer, VERTICAL, verticalLayerDependency,
				selectionLayer, SelectionStyleLabels.ROW_FULLY_SELECTED_STYLE,
				layerPainter );
		
		registerCommandHandlers();
		
		if (useDefaultConfiguration) {
			addConfiguration(new DefaultRowHeaderLayerConfiguration());
		}
	}
	
	
}
