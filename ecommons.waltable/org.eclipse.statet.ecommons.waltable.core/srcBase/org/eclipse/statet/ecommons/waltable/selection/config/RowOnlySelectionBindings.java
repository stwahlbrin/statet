/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.config;

import org.eclipse.swt.SWT;

import org.eclipse.statet.ecommons.waltable.selection.ui.action.RowSelectionDragMode;
import org.eclipse.statet.ecommons.waltable.selection.ui.action.SelectRowAction;
import org.eclipse.statet.ecommons.waltable.ui.action.IDragMode;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;


public class RowOnlySelectionBindings extends DefaultSelectionBindings {

	@Override
	protected void configureBodyMouseClickBindings(final UiBindingRegistry uiBindingRegistry) {
		final IMouseAction action= new SelectRowAction();
		uiBindingRegistry.registerMouseDownBinding(MouseEventMatcher.bodyLeftClick(SWT.NONE), action);
		uiBindingRegistry.registerMouseDownBinding(MouseEventMatcher.bodyLeftClick(SWT.MOD2), action);
		uiBindingRegistry.registerMouseDownBinding(MouseEventMatcher.bodyLeftClick(SWT.MOD1), action);
		uiBindingRegistry.registerMouseDownBinding(MouseEventMatcher.bodyLeftClick(SWT.MOD2 | SWT.MOD1), action);
	}

	@Override
	protected void configureBodyMouseDragMode(final UiBindingRegistry uiBindingRegistry) {
		final IDragMode dragMode= new RowSelectionDragMode();
		uiBindingRegistry.registerFirstMouseDragMode(MouseEventMatcher.bodyLeftClick(SWT.NONE), dragMode);
		uiBindingRegistry.registerFirstMouseDragMode(MouseEventMatcher.bodyLeftClick(SWT.MOD2), dragMode);
		uiBindingRegistry.registerFirstMouseDragMode(MouseEventMatcher.bodyLeftClick(SWT.MOD1), dragMode);
		uiBindingRegistry.registerFirstMouseDragMode(MouseEventMatcher.bodyLeftClick(SWT.MOD2 | SWT.MOD1), dragMode);
	}
}
