/*=============================================================================#
 # Copyright (c) 2012, 2022 Edwin Park and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Edwin Park - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;


@NonNullByDefault
public class ViewportDragCommandHandler implements LayerCommandHandler<ViewportDragCommand> {
	
	
	private final ViewportLayer viewportLayer;
	
	
	public ViewportDragCommandHandler(final ViewportLayer viewportLayer) {
		this.viewportLayer= viewportLayer;
	}
	
	
	@Override
	public Class<ViewportDragCommand> getCommandClass() {
		return ViewportDragCommand.class;
	}
	
	
	@Override
	public boolean executeCommand(final ViewportDragCommand command) {
		this.viewportLayer.drag(command.getX(), command.getY());
		return true;
	}
	
}
