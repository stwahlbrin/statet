/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import java.util.Collection;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.selection.core.AbstractSelectDimPositionsCommand;


/**
 * Command to select column(s)/row(s) in the viewport.
 */
@NonNullByDefault
public class ViewportSelectDimPositionsCommand extends AbstractSelectDimPositionsCommand {
	
	
	public ViewportSelectDimPositionsCommand(final LayerDim layerDim,
			final long position, final int selectionFlags) {
		super(layerDim, position, selectionFlags);
	}
	
	public ViewportSelectDimPositionsCommand(final LayerDim layerDim,
			final long refPosition, final Collection<LRange> positions,
			final long positionToReveal, final int selectionFlags) {
		super(layerDim, refPosition, positions, positionToReveal, selectionFlags);
	}
	
	protected ViewportSelectDimPositionsCommand(final ViewportSelectDimPositionsCommand command) {
		super(command);
	}
	
	@Override
	public ViewportSelectDimPositionsCommand cloneCommand() {
		return new ViewportSelectDimPositionsCommand(this);
	}
	
}
