/*=============================================================================#
 # Copyright (c) 2012, 2022 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.command.AbstractPositionCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public class ShowCellInViewportCommand extends AbstractPositionCommand {
	
	
	public ShowCellInViewportCommand(final Layer layer, final long columnPosition, final long rowPosition) {
		super(layer, columnPosition, rowPosition);
	}
	
	protected ShowCellInViewportCommand(final ShowCellInViewportCommand command) {
		super(command);
	}
	
	@Override
	public ShowCellInViewportCommand cloneCommand() {
		return new ShowCellInViewportCommand(this);
	}

}
