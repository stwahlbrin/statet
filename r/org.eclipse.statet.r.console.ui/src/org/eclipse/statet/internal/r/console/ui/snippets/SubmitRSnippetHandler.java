/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.snippets;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.templates.Template;

import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;


public class SubmitRSnippetHandler extends AbstractHandler {
	
	
	private final RSnippets snippets;
	
	
	public SubmitRSnippetHandler() {
		this.snippets= RConsoleUIPlugin.getInstance().getRSnippets();
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final String name= event.getParameter(RSnippets.SNIPPET_PAR);
		if (name == null) {
			return null;
		}
		final Template template= this.snippets.getTemplateStore().findTemplate(name);
		if (template != null) {
			this.snippets.run(template, event);
		}
		
		return null;
	}
	
	
}
