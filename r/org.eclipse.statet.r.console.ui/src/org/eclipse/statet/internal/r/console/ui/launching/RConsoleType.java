/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class RConsoleType {
	
	
	private final String name;
	private final String id;
	private final boolean requireJRE;
	private final boolean isDebugSupported;
	private final boolean isJDebugSupported;
	
	
	public RConsoleType(final String name, final String id,
			final boolean requireJRE,
			final boolean isDebugSupported, final boolean isJDebugSupported) {
		this.name= name;
		this.id= id;
		this.requireJRE= requireJRE;
		this.isDebugSupported= isDebugSupported;
		this.isJDebugSupported= isJDebugSupported;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public String getId() {
		return this.id;
	}
	
	public boolean requireJRE() {
		return this.requireJRE;
	}
	
	public boolean isDebugSupported() {
		return this.isDebugSupported;
	}
	
	public boolean isJDebugSupported() {
		return this.isJDebugSupported;
	}
	
}
