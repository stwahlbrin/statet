/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.ui;

import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.part.IPageBookViewPage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.internal.r.console.ui.page.RConsolePage;
import org.eclipse.statet.nico.core.NicoCore;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.ui.console.NIConsole;
import org.eclipse.statet.nico.ui.console.NIConsoleColorAdapter;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.RSourceConstants;
import org.eclipse.statet.r.launching.ui.RErrorLineTracker;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.services.RPlatform;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public class RConsole extends NIConsole implements RCoreAccess {
	
	
	private final PreferenceAccess prefs;
	
	private @Nullable RSourceConfig rSourceConfig;
	
	
	public RConsole(final RProcess process, final NIConsoleColorAdapter adapter) {
		super(process, adapter);
		
		final RErrorLineTracker lineMatcher= new RErrorLineTracker(process);
		addPatternMatchListener(lineMatcher);
		this.prefs= NicoCore.getInstanceConsolePreferences();
	}
	
	
	@Override
	public RProcess getProcess() {
		return (RProcess)super.getProcess();
	}
	
	
	@Override
	public IPageBookViewPage createPage(final IConsoleView view) {
		return new RConsolePage(this, view);
	}
	
	@Override
	protected String getSymbolicFontName() {
		return "org.eclipse.statet.workbench.themes.ConsoleFont"; //$NON-NLS-1$
	}
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.prefs;
	}
	
	@Override
	public REnv getREnv() {
		return getProcess().getREnv();
	}
	
	@Override
	public RSourceConfig getRSourceConfig() {
		var rSourceConfig= this.rSourceConfig;
		if (rSourceConfig == null) {
			final ToolController controller= getProcess().getController();
			if (controller instanceof RService) {
				final RPlatform platform= ((RService)controller).getPlatform();
				if (platform != null) {
					rSourceConfig= new RSourceConfig(
							RSourceConstants.getSuitableLangVersion(platform.getRVersion()) );
					this.rSourceConfig= rSourceConfig;
				}
			}
			if (rSourceConfig == null) {
				rSourceConfig= RSourceConfig.DEFAULT_CONFIG;
			}
		}
		return rSourceConfig;
	}
	
	@Override
	public RCodeStyleSettings getRCodeStyle() {
		return RCore.WORKBENCH_ACCESS.getRCodeStyle();
	}
	
}
