/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.SUBSEQUENT;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_FDEF;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ADD_PLUS;
import static org.eclipse.statet.r.core.source.ast.RAstTests.BLOCK;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ERROR_TERM;
import static org.eclipse.statet.r.core.source.ast.RAstTests.FDEF_ARGS;
import static org.eclipse.statet.r.core.source.ast.RAstTests.FDEF_B;
import static org.eclipse.statet.r.core.source.ast.RAstTests.FDEF_STD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.NUM_NUM;
import static org.eclipse.statet.r.core.source.ast.RAstTests.assertExpr0Node;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.RSourceConstants;


@NonNullByDefault
public class RParserFDefTest extends AbstractAstNodeTest {
	
	
	public RParserFDefTest() {
	}
	
	
	@Test
	public void FDefStd_basic() {
		RAstNode expr0;
		
		expr0= assertExpr("function() 1",
						 0, 12, FDEF_STD,	0, null );
		assertExpr0Node( 9,  9, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(11, 12, NUM_NUM,	0, "1",		expr0, new int[] { 1 });
		
		expr0= assertExpr("function() { x; call(x); }",
						 0, 26, FDEF_STD,	0, null );
		assertExpr0Node( 9,  9, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(11, 26, BLOCK,		0, null,	expr0, new int[] { 1 });
		
		expr0= assertExpr("function(x, y= 0) x + y",
						 0, 23, FDEF_STD,	0, null );
		assertExpr0Node( 9, 16, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(18, 23, ADD_PLUS,	0, null,	expr0, new int[] { 1 });
	}
	
	@Test
	public void FDefStd_argsNotOpened() {
		RAstNode expr0;
		
		expr0= assertExpr("function;\n",
						 0,  8, FDEF_STD,	TYPE12_SYNTAX_FDEF_ARGS_MISSING | ERROR_IN_CHILD,
											null );
		assertExpr0Node( 8,  8, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 8,  8, ERROR_TERM,	TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_FDEF | SUBSEQUENT,
											null,		expr0, new int[] { 1 });
	}
	
	@Test
	public void FDefStd_bodyMissing() {
		RAstNode expr0;
		
		expr0= assertExpr("function();\n",
						 0, 10, FDEF_STD,	ERROR_IN_CHILD,
											null );
		assertExpr0Node( 9,  9, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(10, 10, ERROR_TERM,	TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_FDEF,
											null,		expr0, new int[] { 1 });
		
		expr0= assertExpr("function(x, y= 0) ",
						 0, 17, FDEF_STD,	ERROR_IN_CHILD,
											null );
		assertExpr0Node( 9, 16, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(17, 17, ERROR_TERM,	TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_FDEF,
											null,		expr0, new int[] { 1 });
	}
	
	
	@Test
	public void FDefBackslash_basic() {
		RAstNode expr0;
		
		expr0= assertExpr("\\() 1",
						 0,  5, FDEF_B,		0, null );
		assertExpr0Node( 2,  2, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 4,  5, NUM_NUM,	0, "1",		expr0, new int[] { 1 });
		
		expr0= assertExpr("\\() { x; call(x); }",
						 0, 19, FDEF_B,		0, null );
		assertExpr0Node( 2,  2, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 4, 19, BLOCK,		0, null,	expr0, new int[] { 1 });
		
		expr0= assertExpr("\\(x, y= 0) x + y",
						 0, 16, FDEF_B,		0, null );
		assertExpr0Node( 2,  9, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(11, 16, ADD_PLUS,	0, null,	expr0, new int[] { 1 });
	}
	
	@Test
	public void FDefBackslash_argsNotOpened() {
		RAstNode expr0;
		
		expr0= assertExpr("\\;\n",
						 0,  1, FDEF_B,		TYPE12_SYNTAX_FDEF_ARGS_MISSING | ERROR_IN_CHILD,
											null );
		assertExpr0Node( 1,  1, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 1,  1, ERROR_TERM,	TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_FDEF | SUBSEQUENT,
											null,		expr0, new int[] { 1 });
	}
	
	@Test
	public void FDefBackslash_bodyMissing() {
		RAstNode expr0;
		
		expr0= assertExpr("\\();\n",
						 0,  3, FDEF_B,		ERROR_IN_CHILD,
											null );
		assertExpr0Node( 2,  2, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 3,  3, ERROR_TERM,	TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_FDEF,
											null,		expr0, new int[] { 1 });
		
		expr0= assertExpr("\\(x, y= 0) ",
						 0, 10, FDEF_B,		ERROR_IN_CHILD,
											null );
		assertExpr0Node( 2,  9, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(10, 10, ERROR_TERM,	TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_FDEF,
											null,		expr0, new int[] { 1 });
	}
	
	@Test
	public void FDefBackslash_incompatible() {
		this.rParser.setRSourceConfig(new RSourceConfig(RSourceConstants.LANG_VERSION_4_0));
		RAstNode expr0;
		
		expr0= assertExpr("\\() 1",
						 0,  5, FDEF_B,		TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_FDEF,
											null );
		assertExpr0Node( 2,  2, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 4,  5, NUM_NUM,	0, "1",		expr0, new int[] { 1 });
		
		expr0= assertExpr("\\() { x; call(x); }",
						 0, 19, FDEF_B,		TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_FDEF,
											null );
		assertExpr0Node( 2,  2, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 4, 19, BLOCK,		0, null,	expr0, new int[] { 1 });
		
		expr0= assertExpr("\\(x, y= 0) x + y",
						 0, 16, FDEF_B,		TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_FDEF,
											null );
		assertExpr0Node( 2,  9, FDEF_ARGS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node(11, 16, ADD_PLUS,	0, null,	expr0, new int[] { 1 });
	}

	
}
