/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import static org.eclipse.statet.jcommons.collections.ImCollections.newIntList;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.r.core.rmodel.Parameters;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.RAsts.FCallArgMatch;


/**
 * Tests for
 *   {@link RAsts#matchArgs(FCall, Parameters)}
 *   {@link RAsts#matchArgs(FCall.Args, Parameters, ImList)}
 */
@NonNullByDefault
public class RArgumentMatchingTest {
	
	
	/** no match (other) */
	private static final int N= -1;
	/** DEFAULT match */
	private static final int D= 0;
	/** ELLIPSIS match */
	private static final int E= 1;
	
	
	private final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RArgumentMatchingTest() {
	}
	
	
	@Test
	public void Default_match_simple() {
		final Parameters params= new Parameters("a", "b");
		
		assertArgs(params, "1, 2",				newIntList(0, 1));
		assertArgs(params, "a=1, 2",			newIntList(0, 1));
		assertArgs(params, "1, a=2",			newIntList(1, 0));
		assertArgs(params, "b=1, 2",			newIntList(1, 0));
		assertArgs(params, "a=1, b=2",			newIntList(0, 1));
		assertArgs(params, "b=1, a=2",			newIntList(1, 0));
	}
	
	@Test
	public void Default_match_lessThanDef() {
		final Parameters params= new Parameters("a", "b");
		
		assertArgs(params, "1",					newIntList(0));
		assertArgs(params, "a=1",				newIntList(0));
		assertArgs(params, "b=2",				newIntList(1));
	}
	
	@Test
	public void Default_match_emptyArg1() {
		final Parameters params= new Parameters("a", "b");
		
		assertArgs(params, ", 2",				newIntList(0, 1));
		assertArgs(params, "a=, 2",				newIntList(0, 1));
		assertArgs(params, ", a=2",				newIntList(1, 0));
		assertArgs(params, "b=, 2",				newIntList(1, 0));
		assertArgs(params, "a=, b=2",			newIntList(0, 1));
		assertArgs(params, "b=, a=2",			newIntList(1, 0));
	}
	
	@Test
	public void Default_match_emptyArg2() {
		final Parameters params= new Parameters("a", "b");
		
		assertArgs(params, "1, ",				newIntList(0, 1));
		assertArgs(params, "a=1, ",				newIntList(0, 1));
		assertArgs(params, "1, a=",				newIntList(1, 0));
		assertArgs(params, "b=1, ",				newIntList(1, 0));
		assertArgs(params, "a=1, b=",			newIntList(0, 1));
		assertArgs(params, "b=1, a=",			newIntList(1, 0));
	}
	
	@Test
	public void Default_match_partialMatch() {
		final Parameters params= new Parameters("aaa", "bbb", "bb");
		
		assertArgs(params, "bbb=1, b=2, 3",		newIntList(1, 2, 0));
		assertArgs(params, "bbb=1, b=2, b=3",	newIntList(1, 2, -1));
		assertArgs(params, "b=1, b=2, bbb=3",	newIntList(2, -1, 1));
		assertArgs(params, "b=1, 2",			newIntList(-1, 0));
	}
	
	@Test
	public void Default_match_ellipsisAtEnd() {
		final Parameters params= new Parameters("a", "b", "...");
		
		assertArgs(params, "a=1, b=2, 3",		newIntList(0, 1, 2),	newIntList(D, D, E));
		assertArgs(params, "a=1, b=2, 3, 4",	newIntList(0, 1, 2, 2), newIntList(D, D, E, E));
		assertArgs(params, "a=1, 2, 3",			newIntList(0, 1, 2),	newIntList(D, D, E));
		assertArgs(params, "1, b=2, 3",			newIntList(0, 1, 2), 	newIntList(D, D, E));
		assertArgs(params, "",					newIntList(),			newIntList());
		assertArgs(params, "a=1",				newIntList(0),			newIntList(D));
		assertArgs(params, "x=1, y=2",			newIntList(2, 2),		newIntList(E, E));
	}
	
	@Test
	public void Default_match_ellipsisAt0() {
		final Parameters params= new Parameters("...", "b");
		
		assertArgs(params, "a=1, b=2, 3",		newIntList(0, 1, 0),	newIntList(E, D, E));
		assertArgs(params, "1, b=2, 3",			newIntList(0, 1, 0), 	newIntList(E, D, E));
		assertArgs(params, "1, 2, b=2",			newIntList(0, 0, 1), 	newIntList(E, E, D));
		assertArgs(params, "a=1, 2, 3",			newIntList(0, 0, 0),	newIntList(E, E, E));
		assertArgs(params, "",					newIntList(),			newIntList());
		assertArgs(params, "x=1, y=2",			newIntList(0, 0),		newIntList(E, E));
	}
	
	
	@Test
	public void PipeTarget_match_simple() {
		final Parameters params= new Parameters("a", "b", "c");
		
		assertArgs_PipeTarget(params, "1, 2",			newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "b=1, c=2",		newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "a=1, 2",			newIntList(1, 0, 2));
		assertArgs_PipeTarget(params, "1, a=2",			newIntList(1, 2, 0));
		assertArgs_PipeTarget(params, "1, b=2",			newIntList(0, 2, 1));
		assertArgs_PipeTarget(params, "b=1, 2",			newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "c=1, 2",			newIntList(0, 2, 1));
		assertArgs_PipeTarget(params, "a=1, b=2",		newIntList(2, 0, 1));
		assertArgs_PipeTarget(params, "b=1, a=2",		newIntList(2, 1, 0));
		assertArgs_PipeTarget(params, "c=1, b=2",		newIntList(0, 2, 1));
	}
	
	@Test
	public void PipeTarget_match_lessThanDef() {
		final Parameters params= new Parameters("a", "b", "c");
		
		assertArgs_PipeTarget(params, "1",				newIntList(0, 1));
		assertArgs_PipeTarget(params, "b=1",			newIntList(0, 1));
		assertArgs_PipeTarget(params, "a=1",			newIntList(1, 0));
		assertArgs_PipeTarget(params, "c=1",			newIntList(0, 2));
	}
	
	@Test
	public void PipeTarget_match_emptyArg1() {
		final Parameters params= new Parameters("a", "b", "c");
		
		assertArgs_PipeTarget(params, ", 2",			newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "b=, 2",			newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "a=, 2",			newIntList(1, 0, 2));
		assertArgs_PipeTarget(params, ", a=2",			newIntList(1, 2, 0));
		assertArgs_PipeTarget(params, "c=, 2",			newIntList(0, 2, 1));
		assertArgs_PipeTarget(params, "a=, b=2",		newIntList(2, 0, 1));
		assertArgs_PipeTarget(params, "b=, a=2",		newIntList(2, 1, 0));
	}
	
	@Test
	public void PipeTarget_match_emptyArg2() {
		final Parameters params= new Parameters("a", "b", "c");
		
		assertArgs_PipeTarget(params, "1, ",			newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "b=1, ",			newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "a=1, ",			newIntList(1, 0, 2));
		assertArgs_PipeTarget(params, "1, a=",			newIntList(1, 2, 0));
		assertArgs_PipeTarget(params, "b=1, ",			newIntList(0, 1, 2));
		assertArgs_PipeTarget(params, "c=1, ",			newIntList(0, 2, 1));
		assertArgs_PipeTarget(params, "a=1, b=",		newIntList(2, 0, 1));
		assertArgs_PipeTarget(params, "b=1, a=",		newIntList(2, 1, 0));
		assertArgs_PipeTarget(params, "c=1, a=",		newIntList(1, 2, 0));
	}
	
	@Test
	public void PipeTarget_match_partialMatch() {
		final Parameters params= new Parameters("aaa", "bbb", "bb", "c");
		
		assertArgs_PipeTarget(params, "bbb=1, b=2, 3",	newIntList(0, 1, 2, 3));
		assertArgs_PipeTarget(params, "bbb=1, b=2, b=3", newIntList(0, 1, 2, -1));
		assertArgs_PipeTarget(params, "b=1, b=2, bbb=3", newIntList(0, 2, -1, 1));
		assertArgs_PipeTarget(params, "b=1, 2",			newIntList(0, -1, 1));
		assertArgs_PipeTarget(params, "a=1, 2",			newIntList(1, 0, 2));
		assertArgs_PipeTarget(params, "a=1, b=2",		newIntList(1, 0, -1));
	}
	
	@Test
	public void PipeTarget_match_ellipsisAtEnd() {
		final Parameters params= new Parameters("a", "b", "...");
		
		assertArgs_PipeTarget(params, "a=1, b=2, 3",	newIntList(2, 0, 1, 2),	newIntList(E, D, D, E));
		assertArgs_PipeTarget(params, "a=1, b=2, 3, 4", newIntList(2, 0, 1, 2, 2), newIntList(E, D, D, E, E));
		assertArgs_PipeTarget(params, "a=1, 2, 3",		newIntList(1, 0, 2, 2),	newIntList(D, D, E, E));
		assertArgs_PipeTarget(params, "1, b=2, 3",		newIntList(0, 2, 1, 2),	newIntList(D, E, D, E));
		assertArgs_PipeTarget(params, "",				newIntList(0),			newIntList(D));
		assertArgs_PipeTarget(params, "a=1",			newIntList(1, 0),		newIntList(D, D));
		assertArgs_PipeTarget(params, "x=1, y=2",		newIntList(0, 2, 2),	newIntList(D, E, E));
	}
	
	
	@Test
	public void ReplCall_match_simple() {
		final Parameters params= new Parameters("a", "value", "b");
		
		assertArgs_ReplCall(params, "1, 2",				newIntList(0, 2));
		assertArgs_ReplCall(params, "a=1, 2",			newIntList(0, 2));
		assertArgs_ReplCall(params, "1, a=2",			newIntList(2, 0));
		assertArgs_ReplCall(params, "b=1, 2",			newIntList(0, 2));
		assertArgs_ReplCall(params, "a=1, b=2",			newIntList(0, 2));
		assertArgs_ReplCall(params, "b=1, a=2",			newIntList(2, 0));
	}
	
	@Test
	public void ReplCall_match_noValueArg() {
		final Parameters params= new Parameters("a", "b");
		
		assertArgs_ReplCall(params, "1, 2",				newIntList(0, 1), N);
		assertArgs_ReplCall(params, "a=1, 2",			newIntList(0, 1), N);
		assertArgs_ReplCall(params, "1, a=2",			newIntList(1, 0), N);
		assertArgs_ReplCall(params, "b=1, 2",			newIntList(0, 1), N);
		assertArgs_ReplCall(params, "a=1, b=2",			newIntList(0, 1), N);
		assertArgs_ReplCall(params, "b=1, a=2",			newIntList(1, 0), N);
	}
	
	@Test
	public void ReplCall_match_lessThanDef() {
		final Parameters params= new Parameters("a", "b", "value");
		
		assertArgs_ReplCall(params, "1",				newIntList(0));
		assertArgs_ReplCall(params, "a=1",				newIntList(0));
		assertArgs_ReplCall(params, "b=2",				newIntList(0));
	}
	
	@Test
	public void ReplCall_match_moreThanDef() {
		final Parameters params= new Parameters("a", "value");
		
		assertArgs_ReplCall(params, "1, 2",				newIntList(0, -1),		newIntList(D, N));
	}
	
	@Test
	public void ReplCall_match_emptyArg1() {
		final Parameters params= new Parameters("a", "b", "value");
		
		assertArgs_ReplCall(params, ", 2",				newIntList(0, 1));
		assertArgs_ReplCall(params, "a=, 2",			newIntList(0, 1));
		assertArgs_ReplCall(params, ", a=2",			newIntList(1, 0));
		assertArgs_ReplCall(params, "b=, 2",			newIntList(0, 1));
		assertArgs_ReplCall(params, "a=, b=2",			newIntList(0, 1));
		assertArgs_ReplCall(params, "b=, a=2",			newIntList(1, 0));
	}
	
	@Test
	public void ReplCall_match_emptyArg2() {
		final Parameters params= new Parameters("a", "b", "value");
		
		assertArgs_ReplCall(params, "1, ",				newIntList(0, 1));
		assertArgs_ReplCall(params, "a=1, ",			newIntList(0, 1));
		assertArgs_ReplCall(params, "1, a=",			newIntList(1, 0));
		assertArgs_ReplCall(params, "b=1, ",			newIntList(0, 1));
		assertArgs_ReplCall(params, "a=1, b=",			newIntList(0, 1));
		assertArgs_ReplCall(params, "b=1, a=",			newIntList(1, 0));
	}
	
	@Test
	public void ReplCall_match_partialMatch() {
		final Parameters parameters= new Parameters("aaa", "a", "value", "b");
		
		assertArgs_ReplCall(parameters, "a=1, aa=2, 3",	newIntList(1, 0, 3));
	}
	
	@Test
	public void ReplCall_match_ellipsisAtEnd() {
		final Parameters params= new Parameters("a", "value", "b", "...");
		
		assertArgs_ReplCall(params, "a=1, b=2, 3",		newIntList(0, 2, 3),	newIntList(D, D, E));
		assertArgs_ReplCall(params, "a=1, b=2, 3, 4",	newIntList(0, 2, 3, 3), newIntList(D, D, E, E));
		assertArgs_ReplCall(params, "a=1, 2, 3",		newIntList(0, 2, 3),	newIntList(D, D, E));
		assertArgs_ReplCall(params, "1, b=2, 3",		newIntList(0, 2, 3), 	newIntList(D, D, E));
		assertArgs_ReplCall(params, "",					newIntList(),			newIntList());
		assertArgs_ReplCall(params, "a=1",				newIntList(0),			newIntList(D));
		assertArgs_ReplCall(params, "x=1, y=2",			newIntList(0, 3),		newIntList(D, E));
	}
	
	@Test
	public void ReplCall_match_ellipsisAtEnd_noValueArg() {
		final Parameters params= new Parameters("a", "b", "...");
		
		assertArgs_ReplCall(params, "a=1, b=2, 3",		newIntList(0, 1, 2),	newIntList(D, D, E), E);
		assertArgs_ReplCall(params, "a=1, b=2, 3, 4",	newIntList(0, 1, 2, 2), newIntList(D, D, E, E), E);
		assertArgs_ReplCall(params, "a=1, 2, 3",		newIntList(0, 1, 2),	newIntList(D, D, E), E);
		assertArgs_ReplCall(params, "1, b=2, 3",		newIntList(0, 1, 2), 	newIntList(D, D, E), E);
		assertArgs_ReplCall(params, "",					newIntList(),			newIntList(), E);
		assertArgs_ReplCall(params, "a=1",				newIntList(0),			newIntList(D), E);
		assertArgs_ReplCall(params, "x=1, y=2",			newIntList(0, 2),		newIntList(D, E), E);
	}
	
	
	@Test
	public void AutoContext_match() {
		final Parameters params= new Parameters("a", "b", "...");
		final Parameters assignParams = new Parameters("a", "b", "value", "...");
		
		assertArgs_AutoContext(params, (FCall)parseExpr("f(c= 3, 1)"), newIntList(2, 0));
		assertArgs_AutoContext(params, (FCall)parseExpr("x <- f(c= 3, 1)").getChild(1), newIntList(2, 0));
		
		assertArgs_AutoContext(params, (FCall)parseExpr("0 |> f(c= 3, 1)").getChild(1), newIntList(0, 2, 1));
		assertArgs_AutoContext(params, (FCall)parseExpr("0 |> (\\(a, b, ...){})(c= 3, 1)").getChild(1), newIntList(0, 2, 1));
		
		assertArgs_AutoContext(params, (FCall)parseExpr("f(x, c= 3, 1) <- 1").getChild(0), newIntList(0, 2, 1));
		assertArgs_AutoContext(assignParams, (FCall)parseExpr("f(x, c= 3, 1) <- 1").getChild(0), newIntList(0, 3, 1));
		assertArgs_AutoContext(assignParams, (FCall)parseExpr("f(x, c= 3, 1) = 1").getChild(0), newIntList(0, 3, 1));
		assertArgs_AutoContext(assignParams, (FCall)parseExpr("1 -> f(x, c= 3, 1)").getChild(1), newIntList(0, 3, 1));
	}
	
	
	private FCall.Args parseArgs(final String code) {
		return nonNullAssert(this.rParser.parseFCallArgs(this.input.reset(code).init(), true));
	}
	
	private RAstNode parseExpr(final String code) {
		return nonNullAssert(this.rParser.parseExpr(this.input.reset(code).init()));
	}
	
	private ImIntList createMatchTypes(final ImIntList expectedDefIdx) {
		final int[] matchTypes= new int[expectedDefIdx.size()];
		for (int i= 0; i < matchTypes.length; i++) {
			matchTypes[i]= (expectedDefIdx.getAt(i) >= 0) ? D : N;
		}
		return ImCollections.newIntList(matchTypes);
	}
	
	private void assertArgs(final Parameters parameters, final String code,
			final ImIntList expectedDefIdxs, final ImIntList expectedMatchTypes) {
		final FCall.Args callArgs= parseArgs(code);
		final FCallArgMatch matchedArgs= RAsts.matchArgs(callArgs, parameters,
				null, ImCollections.emptyList() );
		
		assertEquals(parameters, matchedArgs.getParameters());
		
		assertArrayEquals(expectedDefIdxs.toArray(), matchedArgs.argsNode2paramIdx());
		
		// methods by callArgIdx
		int ellipsisCount= 0;
		int nonmatchingCount= 0;
		for (int callArgIdx= 0; callArgIdx < expectedDefIdxs.size(); callArgIdx++) {
			final FCall.Arg expectedArg= callArgs.getChild(callArgIdx);
			final int expectedDefArgIdx= expectedDefIdxs.getAt(callArgIdx);
			if (expectedDefArgIdx >= 0) {
				assertEquals(parameters.get(expectedDefArgIdx), matchedArgs.getParameterForFCall(callArgIdx));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					break;
				case E:
					assertEquals(expectedArg, matchedArgs.ellipsisArgs[ellipsisCount++]);
					break;
				default:
					fail();
					break;
				}
			}
			else {
				assertNull(matchedArgs.getParameterForFCall(callArgIdx));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case N:
					assertEquals(expectedArg, matchedArgs.otherArgs[nonmatchingCount++]);
					break;
				default:
					fail();
					break;
				}
			}
		}
		
		assertNull(matchedArgs.getParameterForInject(-1));
		assertNull(matchedArgs.getParameterForFCall(-1));
		
		assertEquals(ellipsisCount, matchedArgs.ellipsisArgs.length);
		assertEquals(nonmatchingCount, matchedArgs.otherArgs.length);
		
		// methods by paramIdx
		for (int paramIdx= 0; paramIdx < parameters.size(); paramIdx++) {
			final int expectedDefArgIdx= expectedDefIdxs.indexOf(paramIdx);
			if (expectedDefArgIdx >= 0){
				final FCall.Arg expectedArg= callArgs.getChild(expectedDefArgIdx);
				switch (expectedMatchTypes.getAt(expectedDefArgIdx)) {
				case D:
					assertEquals(expectedArg, matchedArgs.getArgNode(paramIdx));
					assertEquals(expectedArg.getValueChild(), matchedArgs.getArgValueNode(paramIdx));
					break;
				case E:
					assertNull(matchedArgs.getArgNode(paramIdx));
					assertNull(matchedArgs.getArgValueNode(paramIdx));
					break;
				default:
					fail();
					break;
				}
			}
			else {
				assertNull(matchedArgs.getArgNode(paramIdx));
				assertNull(matchedArgs.getArgValueNode(paramIdx));
			}
		}
		
		assertNull(matchedArgs.getArgNode(-1));
		assertNull(matchedArgs.getArgValueNode(-1));
	}
	
	private void assertArgs(final Parameters parameters, final String code,
			final ImIntList expectedDefIdxs) {
		assertArgs(parameters, code, expectedDefIdxs, createMatchTypes(expectedDefIdxs));
	}
	
	private void assertArgs_PipeTarget(final Parameters parameters, final String code,
			final ImIntList expectedDefIdxs, final ImIntList expectedMatchTypes) {
		final FCall.Args callArgs= parseArgs(code);
		final RAstNode sourceNode= new NumberConst(RTerminal.NUM_INT);
		final FCallArgMatch matchedArgs= RAsts.matchArgs(callArgs, parameters,
				null, ImCollections.newList(sourceNode) );
		
		assertEquals(parameters, matchedArgs.getParameters());
		
		assertArrayEquals(expectedDefIdxs.toArray(), matchedArgs.argsNode2paramIdx());
		
		// methods by callArgIdx
		int ellipsisCount= 0;
		int nonmatchingCount= 0;
		{	final int callArgIdx= 0;
			final int expectedDefArgIdx= expectedDefIdxs.getAt(callArgIdx);
			if (expectedDefArgIdx >= 0) {
				assertEquals(parameters.get(expectedDefArgIdx), matchedArgs.getParameterForInject(0));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					break;
				case E:
					assertEquals(sourceNode, matchedArgs.ellipsisArgs[ellipsisCount++].getValueChild());
					break;
				default:
					fail();
					break;
				}
			}
			else {
				assertNull(matchedArgs.getParameterForInject(0));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case N:
					assertEquals(sourceNode, matchedArgs.otherArgs[nonmatchingCount++].getValueChild());
					break;
				default:
					fail();
					break;
				}
			}
		}
		for (int callArgIdx= 1; callArgIdx < expectedDefIdxs.size(); callArgIdx++) {
			final FCall.Arg expectedArg= callArgs.getChild(callArgIdx - 1);
			final int expectedDefArgIdx= expectedDefIdxs.getAt(callArgIdx);
			if (expectedDefArgIdx >= 0) {
				assertEquals(parameters.get(expectedDefArgIdx), matchedArgs.getParameterForFCall(callArgIdx - 1));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					break;
				case E:
					assertEquals(expectedArg, matchedArgs.ellipsisArgs[ellipsisCount++]);
					break;
				default:
					fail();
					break;
				}
			}
			else {
				assertNull(matchedArgs.getParameterForFCall(callArgIdx - 1));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case N:
					assertEquals(expectedArg, matchedArgs.otherArgs[nonmatchingCount++]);
					break;
				default:
					fail();
					break;
				}
			}
		}
		
		assertNull(matchedArgs.getParameterForInject(-1));
		assertNull(matchedArgs.getParameterForFCall(-1));
		
		assertEquals(ellipsisCount, matchedArgs.ellipsisArgs.length);
		assertEquals(nonmatchingCount, matchedArgs.otherArgs.length);
		
		// methods by paramIdx
		for (int paramIdx= 0; paramIdx < parameters.size(); paramIdx++) {
			final int callArgIdx= expectedDefIdxs.indexOf(paramIdx);
			if (callArgIdx == 0) {
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					assertEquals(sourceNode, matchedArgs.getArgNode(paramIdx).getValueChild());
					assertEquals(sourceNode, matchedArgs.getArgValueNode(paramIdx));
					break;
				case E:
					assertNull(matchedArgs.getArgNode(paramIdx));
					assertNull(matchedArgs.getArgValueNode(paramIdx));
					break;
				default:
					fail();
					break;
				}
			}
			else if (callArgIdx >= 1){
				final FCall.Arg expectedArg= callArgs.getChild(callArgIdx - 1);
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					assertEquals(expectedArg, matchedArgs.getArgNode(paramIdx));
					assertEquals(expectedArg.getValueChild(), matchedArgs.getArgValueNode(paramIdx));
					break;
				case E:
					assertNull(matchedArgs.getArgNode(paramIdx));
					assertNull(matchedArgs.getArgValueNode(paramIdx));
					break;
				default:
					fail();
					break;
				}
			}
			else {
				assertNull(matchedArgs.getArgNode(paramIdx));
				assertNull(matchedArgs.getArgValueNode(paramIdx));
			}
		}
		
		assertNull(matchedArgs.getArgNode(-1));
		assertNull(matchedArgs.getArgValueNode(-1));
	}
	
	private void assertArgs_PipeTarget(final Parameters parameters, final String code,
			final ImIntList expectedDefIdxs) {
		assertArgs_PipeTarget(parameters, code, expectedDefIdxs, createMatchTypes(expectedDefIdxs));
	}
	
	private void assertArgs_ReplCall(final Parameters parameters, final String code,
			final ImIntList expectedDefIdxs, final ImIntList expectedMatchTypes, final int expectedValueType) {
		final FCall.Args callArgs= parseArgs(code);
		final RAstNode replValueNode= new NumberConst(RTerminal.NUM_INT);
		final FCallArgMatch matchedArgs= RAsts.matchArgs(callArgs, parameters,
				replValueNode, ImCollections.emptyList() );
		final int valueParamIdx= parameters.indexOf("value");
		
		assertEquals(parameters, matchedArgs.getParameters());
		
		assertArrayEquals(expectedDefIdxs.toArray(), matchedArgs.argsNode2paramIdx());
		
		// methods by callArgIdx
		int ellipsisCount= 0;
		int nonmatchingCount= 0;
		for (int callArgIdx= 0; callArgIdx < expectedDefIdxs.size(); callArgIdx++) {
			final FCall.Arg expectedArg= callArgs.getChild(callArgIdx);
			final int expectedDefArgIdx= expectedDefIdxs.getAt(callArgIdx);
			if (expectedDefArgIdx >= 0) {
				assertEquals(parameters.get(expectedDefArgIdx), matchedArgs.getParameterForFCall(callArgIdx));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					break;
				case E:
					assertEquals(expectedArg, matchedArgs.ellipsisArgs[ellipsisCount++]);
					break;
				default:
					fail();
					break;
				}
			}
			else {
				assertNull(matchedArgs.getParameterForFCall(callArgIdx));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case N:
					assertEquals(expectedArg, matchedArgs.otherArgs[nonmatchingCount++]);
					break;
				default:
					fail();
					break;
				}
			}
		}
		switch (expectedValueType) {
		case E:
			ellipsisCount++;
			break;
		case N:
			nonmatchingCount++;
			break;
		}
		
		assertNull(matchedArgs.getParameterForInject(-1));
		assertNull(matchedArgs.getParameterForFCall(-1));
		
		assertEquals(ellipsisCount, matchedArgs.ellipsisArgs.length, "ellipsisCount");
		assertEquals(nonmatchingCount, matchedArgs.otherArgs.length, "failCount");
		
		// methods by paramIdx
		for (int paramIdx= 0; paramIdx < parameters.size(); paramIdx++) {
			final int expectedDefArgIdx= expectedDefIdxs.indexOf(paramIdx);
			if (expectedDefArgIdx >= 0){
				final FCall.Arg expectedArg= callArgs.getChild(expectedDefArgIdx);
				switch (expectedMatchTypes.getAt(expectedDefArgIdx)) {
				case D:
					assertEquals(expectedArg, matchedArgs.getArgNode(paramIdx));
					assertEquals(expectedArg.getValueChild(), matchedArgs.getArgValueNode(paramIdx));
					break;
				case E:
					assertNull(matchedArgs.getArgNode(paramIdx));
					assertNull(matchedArgs.getArgValueNode(paramIdx));
					break;
				default:
					fail();
					break;
				}
			}
			else if (paramIdx == valueParamIdx) {
				assertEquals(replValueNode, matchedArgs.getArgValueNode(paramIdx));
			}
			else {
				assertNull(matchedArgs.getArgNode(paramIdx));
				assertNull(matchedArgs.getArgValueNode(paramIdx));
			}
		}
		
		switch (expectedValueType) {
		case E:
			assertEquals(replValueNode, matchedArgs.ellipsisArgs[ellipsisCount - 1].getValueChild());
			break;
		case N:
			assertEquals(replValueNode, matchedArgs.otherArgs[nonmatchingCount - 1].getValueChild());
			break;
		default:
			break;
		}
		
		assertNull(matchedArgs.getArgNode(-1));
		assertNull(matchedArgs.getArgValueNode(-1));
	}
	
	private void assertArgs_ReplCall(final Parameters parameters, final String code,
			final ImIntList expectedDefIdxs, final ImIntList expectedMatchTypes) {
		assertArgs_ReplCall(parameters, code,
				expectedDefIdxs, expectedMatchTypes, D );
	}
	
	private void assertArgs_ReplCall(final Parameters parameters, final String code,
			final ImIntList expectedDefIdxs, final int expectedValueType) {
		assertArgs_ReplCall(parameters, code,
				expectedDefIdxs, createMatchTypes(expectedDefIdxs), expectedValueType );
	}
	
	private void assertArgs_ReplCall(final Parameters parameters, final String code,
			final ImIntList expectedParamIdxs) {
		assertArgs_ReplCall(parameters, code,
				expectedParamIdxs, createMatchTypes(expectedParamIdxs), D );
	}
	
	private void assertArgs_AutoContext(final Parameters parameters, final FCall fCallNode,
			final ImIntList expectedDefIdxs) {
		final FCallArgMatch matchedArgs= RAsts.matchArgs(fCallNode, parameters);
		
		assertArrayEquals(expectedDefIdxs.toArray(), matchedArgs.argsNode2paramIdx());
	}
	
}
