/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.util.Version;


@NonNullByDefault
public class RTerminalTest {
	
	
	public RTerminalTest() {
	}
	
	
	@Test
	public void minRLangVersion_4_1() {
		assertMinRLangVersion(RSourceConstants.LANG_VERSION_4_1, RTerminal.FUNCTION_B);
		assertMinRLangVersion(RSourceConstants.LANG_VERSION_4_1, RTerminal.PIPE_RIGHT);
	}
	
	
	private void assertMinRLangVersion(final Version expected, final RTerminal terminal) {
		assertEquals(expected, terminal.getMinRLangVersion(), "minRLangVersion of " + terminal); //$NON-NLS-1$
	}
	
}
