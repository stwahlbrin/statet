/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.source.StatusDetail;
import org.eclipse.statet.r.core.source.RTerminal;


@NonNullByDefault
public class RAstTests {
	
	
	public static class NodeDescr {
		
		final NodeType type;
		
		final @Nullable RTerminal op0;
		
		public NodeDescr(final NodeType type, final @Nullable RTerminal op) {
			this.type= type;
			this.op0= op;
		}
		
		@Override
		public String toString() {
			return this.type + " » " + ((this.op0 != null) ? this.op0.name() : "");
		}
		
	}
	
	public static final NodeDescr BLOCK= new NodeDescr(NodeType.BLOCK, RTerminal.BLOCK_OPEN);
	
	public static final NodeDescr SYMBOL_STD= new NodeDescr(NodeType.SYMBOL, RTerminal.SYMBOL);
	public static final NodeDescr SYMBOL_G= new NodeDescr(NodeType.SYMBOL, RTerminal.SYMBOL_G);
	
	public static final NodeDescr STRING_D= new NodeDescr(NodeType.STRING_CONST, RTerminal.STRING_D);
	public static final NodeDescr STRING_S= new NodeDescr(NodeType.STRING_CONST, RTerminal.STRING_S);
	public static final NodeDescr STRING_RAW= new NodeDescr(NodeType.STRING_CONST, RTerminal.STRING_R);
	public static final NodeDescr NUM_NUM= new NodeDescr(NodeType.NUM_CONST, RTerminal.NUM_NUM);
	
	public static final NodeDescr SEQ= new NodeDescr(NodeType.SEQ, RTerminal.SEQ);
	
	public static final NodeDescr MULT_MULT= new NodeDescr(NodeType.MULT, RTerminal.MULT);
	public static final NodeDescr MULT_DIV= new NodeDescr(NodeType.MULT, RTerminal.DIV);
	public static final NodeDescr ADD_PLUS= new NodeDescr(NodeType.ADD, RTerminal.PLUS);
	public static final NodeDescr ADD_MINUS= new NodeDescr(NodeType.ADD, RTerminal.MINUS);
	
	public static final NodeDescr FDEF_STD= new NodeDescr(NodeType.F_DEF, RTerminal.FUNCTION);
	public static final NodeDescr FDEF_B= new NodeDescr(NodeType.F_DEF, RTerminal.FUNCTION_B);
	public static final NodeDescr FDEF_ARGS= new NodeDescr(NodeType.F_DEF_ARGS, null);
	public static final NodeDescr FCALL= new NodeDescr(NodeType.F_CALL, null);
	
	public static final NodeDescr PIPE_FORWARD= new NodeDescr(NodeType.PIPE_FORWARD, RTerminal.PIPE_RIGHT);
	
	public static final NodeDescr ASSIGN_LEFT_S= new NodeDescr(NodeType.A_LEFT, RTerminal.ARROW_LEFT_S);
	public static final NodeDescr ASSIGN_LEFT_D= new NodeDescr(NodeType.A_LEFT, RTerminal.ARROW_LEFT_D);
	public static final NodeDescr ASSIGN_RIGHT_S= new NodeDescr(NodeType.A_RIGHT, RTerminal.ARROW_RIGHT_S);
	public static final NodeDescr ASSIGN_RIGHT_D= new NodeDescr(NodeType.A_RIGHT, RTerminal.ARROW_RIGHT_D);
	public static final NodeDescr ASSIGN_EQUALS= new NodeDescr(NodeType.A_EQUALS, RTerminal.EQUAL);
	
	public static final NodeDescr HELP_QUESTIONMARK= new NodeDescr(NodeType.HELP, RTerminal.QUESTIONMARK);
	
	public static final NodeDescr ERROR_TERM= new NodeDescr(NodeType.ERROR_TERM, null);
	
	
	public static void assertNode(final int startOffset, final int endOffset,
			final NodeDescr nodeDescr,
			final RAstNode actual, final String nodeLabel) {
		assertEquals(nodeDescr.type, actual.getNodeType(), nodeLabel + ".nodeType");
		assertEquals(startOffset, actual.getStartOffset(), nodeLabel + ".startOffset");
		assertEquals(endOffset, actual.getEndOffset(), nodeLabel + ".endOffset");
		assertEquals(nodeDescr.op0, actual.getOperator(0), nodeLabel + ".operator[0]");
	}
	
	public static void assertNode(final int startOffset, final int endOffset,
			final NodeDescr nodeDescr, final int statusCode, final @Nullable String text,
			final RAstNode actual, final String nodeLabel) {
		assertNode(startOffset, endOffset, nodeDescr, actual, nodeLabel);
		assertEquals(String.format("0x%1$08X", statusCode), String.format("0x%1$08X", actual.getStatusCode()),
				nodeLabel + ".statusCode");
		assertEquals(text, actual.getText(), nodeLabel + ".text");
	}
	
	
	public static void assertExpr0Node(final int startOffset, final int endOffset,
			final NodeDescr nodeDescr,
			final RAstNode expr0, final int[] nodeIndex) {
		RAstNode node= expr0;
		final StringBuilder sb= new StringBuilder("expr[0]");
		for (int i= 0; i < nodeIndex.length; i++) {
			node= node.getChild(nodeIndex[i]);
			sb.append('[');
			sb.append(nodeIndex[i]);
			sb.append(']');
		}
		assertNode(startOffset, endOffset, nodeDescr, node, sb.toString());
	}
	
	public static void assertExpr0Node(final int startOffset, final int endOffset,
			final NodeDescr nodeDescr, final int statusCode, final @Nullable String text,
			final RAstNode expr0, final int[] nodeIndex) {
		RAstNode node= expr0;
		final StringBuilder sb= new StringBuilder("expr[0]");
		for (int i= 0; i < nodeIndex.length; i++) {
			node= node.getChild(nodeIndex[i]);
			sb.append('[');
			sb.append(nodeIndex[i]);
			sb.append(']');
		}
		assertNode(startOffset, endOffset, nodeDescr, statusCode, text, node, sb.toString());
	}
	
	
	public static void assertTextRegion(final int startOffset, final int endOffset,
			final @Nullable TextRegion actual) {
		assertNotNull(actual);
		assertEquals(startOffset, actual.getStartOffset(), "textRegion.startOffset");
		assertEquals(endOffset, actual.getEndOffset(), "textRegion.endOffset");
	}
	
	public static void assertDetail(final int startOffset, final int length,
			final @Nullable String text,
			final @Nullable StatusDetail actual) {
		assertNotNull(actual);
		assertEquals(startOffset, actual.getStartOffset(), "detail.startOffset");
		assertEquals(startOffset + length, actual.getEndOffset(), "detail.endOffset");
		assertEquals(text, actual.getText(), "detail.text");
	}
	
}
