/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class RValueFormatter06rawTest {
	
	
	private final RValueFormatter formatter= new RValueFormatter();
	
	
	public RValueFormatter06rawTest() {
	}
	
	
	@Test
	public void print() {
		this.formatter.clear();
		this.formatter.appendRaw((byte) 0);
		assertEquals("00", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendRaw((byte) 1);
		assertEquals("01", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendRaw((byte) 0x0F);
		assertEquals("0F", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendRaw((byte) 0xFF);
		assertEquals("FF", this.formatter.getString());
	}
	
}
