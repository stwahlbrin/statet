/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.base.core;

import org.eclipse.core.resources.IProject;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface StatetProject {
	
	
	static final String NATURE_ID= "org.eclipse.statet.ide.resourceProjects.Statet"; //$NON-NLS-1$
	
	
	IProject getProject();
	
}
