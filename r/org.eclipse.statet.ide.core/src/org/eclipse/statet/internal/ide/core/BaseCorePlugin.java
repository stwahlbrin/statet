/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.core;

import java.util.concurrent.CopyOnWriteArraySet;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.osgi.util.tracker.ServiceTracker;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jsch.core.IJSchService;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.base.core.StatetCore;


/**
 * The activator class controls the plug-in life cycle
 */
public final class BaseCorePlugin extends Plugin {
	
	
	private static BaseCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static BaseCorePlugin getInstance() {
		return instance;
	}
	
	
	private boolean started;
	
	private final CopyOnWriteArraySet<Disposable> disposables= new CopyOnWriteArraySet<>();
	
	private ServiceTracker sshTracker;
	private SshSessionManager sshSessions;
	
	
	/**
	 * The constructor
	 */
	public BaseCorePlugin() {
		instance= this;
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		
		this.sshTracker= new ServiceTracker(context, "org.eclipse.jsch.core.IJSchService", null); //$NON-NLS-1$
		this.sshTracker.open();
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				this.sshSessions= null;
			}
			
			try {
				for (final Disposable listener : this.disposables) {
					listener.dispose();
				}
			}
			finally {
				this.disposables.clear();
			}
			
			if (this.sshTracker != null) {
				this.sshTracker.close();
				this.sshTracker= null;
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public void addStopListener(final Disposable listener) {
		this.disposables.add(listener);
	}
	
	public void removeStopListener(final Disposable listener) {
		this.disposables.remove(listener);
	}
	
	
	private static final Version REQ_JSCH_VERSION= new Version(0, 1, 49);
	
	public IJSchService getJSchService() throws CoreException {
		// E-3.5 IJSchService declarative?
		IJSchService.class.getName();
		
		{	// Check if JSch is compatible/up-to-date
			final Bundle bundle= Platform.getBundle("com.jcraft.jsch"); //$NON-NLS-1$
			if (bundle != null && bundle.getVersion().compareTo(REQ_JSCH_VERSION) < 0) {
				throw new CoreException(new Status(IStatus.ERROR, StatetCore.BUNDLE_ID, 0,
						"The installed version of the Java Secure Channel Library 'JSch' by JCraft is outdated.\n" +
						"Please install version " + REQ_JSCH_VERSION + " or newer, " +
						"for example from the update-site of WalWare.de.", null ));
			}
		}
		
		return (IJSchService) this.sshTracker.getService();
	}
	
	public synchronized SshSessionManager getSshSessionManager() {
		if (this.sshSessions == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.sshSessions= new SshSessionManager();
			addStopListener(this.sshSessions);
		}
		return this.sshSessions;
	}
	
}
