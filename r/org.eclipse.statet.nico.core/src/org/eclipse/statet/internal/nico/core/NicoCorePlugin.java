/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.core;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;

import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;

import org.eclipse.statet.nico.core.NicoCore;


/**
 * The activator class controls the plug-in life cycle
 */
public class NicoCorePlugin extends Plugin {
	
	
	public static final int INTERNAL_ERROR = 100;
	public static final int EXTERNAL_ERROR = 105;
	
	
	private static NicoCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static NicoCorePlugin getInstance() {
		return instance;
	}
	
	public static void log(final IStatus status) {
		final NicoCorePlugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	public static void log(final Status status) {
		log(StatusUtils.convert(status));
	}
	
	public static void logError(final String message, final Throwable e) {
		log(new org.eclipse.core.runtime.Status(IStatus.ERROR, NicoCore.BUNDLE_ID, message, e));
	}
	
	public static void logError(final int code, final String message, final Throwable e) {
		log(new org.eclipse.core.runtime.Status(IStatus.ERROR, NicoCore.BUNDLE_ID, code, message, e));
	}
	
	
	/**
	 * The constructor
	 */
	public NicoCorePlugin() {
		instance= this;
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		instance= null;
		super.stop(context);
	}
	
}
