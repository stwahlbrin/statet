/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import javax.security.auth.callback.Callback;

import org.eclipse.statet.jcommons.ts.core.ToolCommandHandler;


/**
 * Can react on tool events.
 * 
 * Tool events allows automation and the separation of UI.
 */
public interface IToolEventHandler extends ToolCommandHandler {
	
	
	/**
	 * Called when a login is required
	 * 
	 * return:  OK = try login, CANCEL = cancel
	 */
	public static final String LOGIN_REQUEST_EVENT_ID = "common/login.request"; //$NON-NLS-1$
	
	/**
	 * Called when a login was successful
	 * 
	 * return:  OK = try login, CANCEL = cancel
	 */
	public static final String LOGIN_OK_EVENT_ID = "common/login.ok"; //$NON-NLS-1$
	
	/** {@link String} = message to show (e.g. previous login error) (optional) */
	public static final String LOGIN_MESSAGE_DATA_KEY = "message"; //$NON-NLS-1$
	/** {@link String} = address to identify login (optional) */
	public static final String LOGIN_ADDRESS_DATA_KEY = "address"; //$NON-NLS-1$
	/** {@link Callback}[] = callbacks to answer (required) */
	public static final String LOGIN_CALLBACKS_DATA_KEY = "callbacks"; //$NON-NLS-1$
	/** {@link String} = username proposal (optional) */
	public static final String LOGIN_USERNAME_DATA_KEY = "username"; //$NON-NLS-1$
	/** {@link String} = flag to force usage of username (optional) */
	public static final String LOGIN_USERNAME_FORCE_DATA_KEY = "username.force"; //$NON-NLS-1$
	/** {@link String} = SSH host (when using SSH) */
	public static final String LOGIN_SSH_HOST_DATA_KEY = "ssh.host"; //$NON-NLS-1$
	/** {@link Integer} = SSH port (when using SSH) */
	public static final String LOGIN_SSH_PORT_DATA_KEY = "ssh.port"; //$NON-NLS-1$
	
	
	/**
	 * 
	 * data:    IStatus status
	 */
	public static final String REPORT_STATUS_EVENT_ID = "common/reportStatus"; //$NON-NLS-1$
	
	/** {@link Status} = status to report (required) */
	public static final String REPORT_STATUS_DATA_KEY = "status"; //$NON-NLS-1$
	
	/**
	 * 
	 * data:    IToolRunnable<IToolRunnableControllerAdapter>[*] schedulesQuitTasks = existing scheduled runnables
	 * return:  OK = schedule, CANCEL = do nothing
	 */
	public static final String SCHEDULE_QUIT_EVENT_ID = "common/scheduleQuit"; //$NON-NLS-1$
	
	/**
	 * Should try to block other actions (e.g. modal dialog)
	 * 
	 * return:  OK, ERROR, CANCEL
	 */
	public static final String RUN_BLOCKING_EVENT_ID = "common/runBlocking"; //$NON-NLS-1$
	
	/** {@link org.eclipse.statet.jcommons.ts.core.ToolRunnable} = runnable to run */
	public static final String RUN_RUNNABLE_DATA_KEY = "runnable"; //$NON-NLS-1$
	
}
