/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import static org.eclipse.statet.jcommons.status.Status.CANCEL_STATUS;
import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.internal.nico.core.Messages;
import org.eclipse.statet.nico.core.NicoCore;
import org.eclipse.statet.nico.core.util.AbstractConsoleCommandHandler;


/**
 * {@link #LOAD_HISTORY_COMMAND_ID}
 * {@link #SAVE_HISTORY_COMMAND_ID}
 * {@link #ADDTO_HISTORY_COMMAND_ID}
 */
@NonNullByDefault
public class HistoryOperationsHandler extends AbstractConsoleCommandHandler {
	
	
	public static final String LOAD_HISTORY_COMMAND_ID= "console/loadHistory"; //$NON-NLS-1$
	
	public static final String SAVE_HISTORY_COMMAND_ID= "console/saveHistory"; //$NON-NLS-1$
	
	public static final String ADDTO_HISTORY_COMMAND_ID= "console/addtoHistory"; //$NON-NLS-1$
	
	
	public HistoryOperationsHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final ConsoleService service, final ToolCommandData data,
			final ProgressMonitor m) {
		switch (id) {
		case LOAD_HISTORY_COMMAND_ID:
			return loadHistory(service, data, m);
		case SAVE_HISTORY_COMMAND_ID:
			return saveHistory(service, data, m);
			
		case ADDTO_HISTORY_COMMAND_ID:
			{	final String item= data.getStringRequired("text"); //$NON-NLS-1$
				service.getTool().getHistory().addCommand(item,
						service.getController().getCurrentSubmitType() );
				return OK_STATUS;
			}
			
		default:
			throw new UnsupportedOperationException();
		}
	}
	
	
	protected Status loadHistory(final ConsoleService tools, final ToolCommandData data,
			final ProgressMonitor m) {
		try {
			CoreException fileException= null;
			IFileStore fileStore= null;
			final String filename= data.getStringRequired("filename"); //$NON-NLS-1$
			final ToolWorkspace workspaceData= tools.getWorkspaceData();
			try {
				fileStore= workspaceData.toFileStore(filename);
			}
			catch (final CoreException e) {
				fileException= e;
			}
			final Status status;
			if (fileStore == null) {
				status= new ErrorStatus(NicoCore.BUNDLE_ID, NLS.bind(
								Messages.ToolController_FileOperation_error_CannotResolve_message,
								filename ),
						fileException );
			}
			else {
				status= tools.getTool().getHistory().load(fileStore,
						workspaceData.getEncoding(), false, m );
			}
			tools.handleStatus(status, m);
			return status;
		}
		catch (final OperationCanceledException e) {
			return CANCEL_STATUS;
		}
	}
	
	protected Status saveHistory(final ConsoleService tools, final ToolCommandData data,
			final ProgressMonitor m) {
		try {
			CoreException fileException= null;
			IFileStore fileStore= null;
			final Status status;
			
			final String filename= data.getStringRequired("filename"); //$NON-NLS-1$
			final ToolWorkspace workspaceData= tools.getWorkspaceData();
			try {
				fileStore= workspaceData.toFileStore(filename);
			}
			catch (final CoreException e) {
				fileException= e;
			}
			if (fileStore == null) {
				status= new ErrorStatus(NicoCore.BUNDLE_ID, NLS.bind(
								Messages.ToolController_FileOperation_error_CannotResolve_message,
								filename ),
						fileException );
			}
			else {
				status= tools.getTool().getHistory().save(fileStore,
						EFS.NONE, workspaceData.getEncoding(), false, m );
			}
			tools.handleStatus(status, m);
			return status;
		}
		catch (final OperationCanceledException e) {
			return CANCEL_STATUS;
		}
	}
	
}
