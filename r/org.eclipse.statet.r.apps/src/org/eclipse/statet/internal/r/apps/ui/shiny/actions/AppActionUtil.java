/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.shiny.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;
import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;

import org.eclipse.statet.internal.r.apps.core.shiny.RShinyResourceTester;
import org.eclipse.statet.internal.r.apps.ui.RAppUIPlugin;
import org.eclipse.statet.internal.r.apps.ui.launching.AppType;


/**
 * 
 * element= app container
 */
@NonNullByDefault
public class AppActionUtil extends ActionUtil<IContainer> {
	
	
	private static final AppType APP_TYPE= new AppType("org.eclipse.statet.r.apps.appTypes.RShiny", //$NON-NLS-1$
			"R Shiny App" );
	
	
	public AppActionUtil(final byte initialMode) {
		super(initialMode);
	}
	
	
	@Override
	public @Nullable IContainer getLaunchElement(final @Nullable IEditorPart editor) {
		IResource resource= null;
		if (editor != null) {
			resource= getSingleResource(editor.getEditorInput());
		}
		return (resource != null) ? RShinyResourceTester.getAppContainer(resource) : null;
	}
	
	@Override
	public @Nullable IContainer getLaunchElement(final @Nullable ISelection selection) {
		IResource resource= null;
		if (selection instanceof IStructuredSelection) {
			resource= getSingleResource((IStructuredSelection) selection);
		}
		return (resource != null) ? RShinyResourceTester.getAppContainer(resource) : null;
	}
	
	@Override
	public @Nullable LaunchConfigManager<IContainer> getManager(final IWorkbenchWindow window,
			final IContainer element) {
		return RAppUIPlugin.getInstance().getRunAppConfigManager(APP_TYPE);
	}
	
}
