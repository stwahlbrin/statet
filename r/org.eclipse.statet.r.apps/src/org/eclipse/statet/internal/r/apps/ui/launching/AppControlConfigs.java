/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.launching;

import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.variables.core.DynamicVariable;

import org.eclipse.statet.internal.r.apps.ui.Messages;


@NonNullByDefault
public class AppControlConfigs {
	
	
	public static final String TYPE_ID= "org.eclipse.statet.r.apps.launchConfigurations.RAppControl"; //$NON-NLS-1$
	
	
	static final String ATTR_ROOT= "org.eclipse.statet.r.apps/"; //$NON-NLS-1$
	
	public static final String SOURCE_PATH_ATTR_NAME= ATTR_ROOT + "AppSource.path"; //$NON-NLS-1$
	
	public static final String APP_HOST_ATTR_NAME= ATTR_ROOT + "App.host"; //$NON-NLS-1$
	public static final String APP_PORT_ATTR_NAME= ATTR_ROOT + "App.port"; //$NON-NLS-1$
	
	public static final String START_STOP_BLOCKING_TASKS_MODE_ATTR_NAME= ATTR_ROOT + "Start.StopBlockingTasks.mode"; //$NON-NLS-1$
	
	public static final String START_R_SNIPPET_CODE_ATTR_NAME= ATTR_ROOT + "Start.RSnippet.code"; //$NON-NLS-1$
	
	public static final String STOP_R_SNIPPET_CODE_ATTR_NAME= ATTR_ROOT + "Stop.RSnippet.code"; //$NON-NLS-1$
	
	public static final String VIEWER_ID_ATTR_NAME= ATTR_ROOT + "Viewer.id"; //$NON-NLS-1$
	
	public static final String VARIABLES_CODE_ATTR_NAME= ATTR_ROOT + "Variables.code"; //$NON-NLS-1$
	public static final String VARIABLES_VIEWER_ACTION_ATTR_NAME= ATTR_ROOT + "Variables.Viewer.action"; //$NON-NLS-1$
	
	
	public static final String APP_HOST_VAR_NAME= "host"; //$NON-NLS-1$
	public static final String APP_PORT_VAR_NAME= "port"; //$NON-NLS-1$
	
	public static final IStringVariable APP_HOST_VAR= new DynamicVariable(
			APP_HOST_VAR_NAME, Messages.Variable_AppHost_description, false );
	public static final IStringVariable APP_PORT_VAR= new DynamicVariable(
			APP_PORT_VAR_NAME, Messages.Variable_AppPort_description, false );
	
	public static final String WORKBENCH_EXTERNAL_BROWSER_ID= "workbench-external"; //$NON-NLS-1$
	public static final String WORKBENCH_VIEW_BROWSER_ID= "workbench-view"; //$NON-NLS-1$
	
	public static final String SHOW_ACTION_ID= "show"; //$NON-NLS-1$
	
	
}
