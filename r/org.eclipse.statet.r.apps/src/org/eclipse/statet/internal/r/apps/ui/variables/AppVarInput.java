/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.variables;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.apps.ui.RApp;
import org.eclipse.statet.r.core.rmodel.RLangElement;
import org.eclipse.statet.r.ui.util.RElementInput;


@NonNullByDefault
public class AppVarInput extends RElementInput<RApp> {
	
	
	public AppVarInput(final RApp app,
			final @Nullable LtkModelElementFilter<? super RLangElement> envFilter,
			final @Nullable LtkModelElementFilter<? super RLangElement> otherFilter) {
		super(app, envFilter, otherFilter);
	}
	
	
}
