/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.text.r;

import java.util.List;

import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.presentation.AbstractRuleBasedScanner;
import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager;


/**
 * Scanner for infix-operators.
 */
@NonNullByDefault
public class RInfixOperatorScanner extends AbstractRuleBasedScanner {
	
	
	public RInfixOperatorScanner(final TextStyleManager<?> textStyles) {
		super(textStyles);
		
		initRules();
	}
	
	
	@Override
	protected void createRules(final List<IRule> rules) {
		final IToken predefinedOpToken= getToken(IRTextTokens.OP_KEY);
		final IToken userdefinedOpToken= getToken(IRTextTokens.OP_SUB_USERDEFINED_KEY);
		final IToken invalidOpToken= getToken(IRTextTokens.UNDEFINED_KEY);
		
		rules.add(new RInfixOperatorRule(userdefinedOpToken, invalidOpToken, predefinedOpToken));
	}
	
}
