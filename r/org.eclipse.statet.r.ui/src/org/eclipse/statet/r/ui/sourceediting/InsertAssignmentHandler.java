/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.rlang.RTokens;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;


/**
 * Inserts the R assignment &lt;-
 */
@NonNullByDefault
public class InsertAssignmentHandler extends AbstractHandler {
	
	
	private final SourceEditor editor;
	
	
	public InsertAssignmentHandler(final SourceEditor editor) {
		this.editor= editor;
	}
	
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		setBaseEnabled(this.editor.isEditable(false));
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		if (this.editor.isEditable(true)) {
			insertSequence();
		}
		return null;
	}
	
	private RCodeStyleSettings getCodeStyle() {
		final SourceUnit sourceUnit= this.editor.getSourceUnit();
		final RCoreAccess coreAccess= (sourceUnit instanceof RSourceUnit) ?
				((RSourceUnit)sourceUnit).getRCoreAccess() : RCore.getWorkbenchAccess();
		return coreAccess.getRCodeStyle();
	}
	
	/**
	 * Inserts the assignment char sequence.
	 */
	private void insertSequence() {
		final ISourceViewer sourceViewer= this.editor.getViewer();
		final IDocument document= sourceViewer.getDocument();
		final ITextSelection selection= (ITextSelection) sourceViewer.getSelectionProvider().getSelection();
		final int offset= selection.getOffset();
		final int selectionLength= selection.getLength();
		
		final StringBuilder sequence= new StringBuilder("<-"); //$NON-NLS-1$
		try {
			final RCodeStyleSettings codeStyle= getCodeStyle();
			if ((offset == 0 || !RTokens.isWhitespace(document.getChar(offset - 1)))
					&& codeStyle.getWhitespaceAssignBefore() ) {
				sequence.insert(0, ' ');
			}
			if (codeStyle.getWhitespaceAssignAfter()) {
				sequence.append(' ');
			}
			document.replace(offset, selectionLength, sequence.toString());
		}
		catch (final BadLocationException e) {
			RUIPlugin.logError(RUIPlugin.INTERNAL_ERROR, "An error occurred while inserting assignment.", e); //$NON-NLS-1$
		}
		final int targetOffset= offset + sequence.length();
		
		sourceViewer.setSelectedRange(targetOffset, 0);
		sourceViewer.revealRange(targetOffset, 0);
	}
	
}
