/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import java.util.Objects;

import org.eclipse.jface.viewers.ISelection;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RDataTableSelection implements ISelection {
	
	
	private final @Nullable DataViewDescription dataViewDescription;
	
	private final @Nullable String anchorRowLabel;
	private final @Nullable String anchorColumnLabel;
	
	private final @Nullable String lastSelectedCellRowLabel;
	private final @Nullable String lastSelectedCellColumnLabel;
	
	
	public RDataTableSelection(
			final @Nullable DataViewDescription dataViewDescription,
			final @Nullable String anchorRowLabel, final @Nullable String anchorColumnLabel,
			final @Nullable String lastSelectedCellRowLabel, final @Nullable String lastSelectedCellColumnLabel) {
		if ((anchorRowLabel != null) != (anchorColumnLabel != null)) {
			throw new IllegalArgumentException();
		}
		if ((lastSelectedCellRowLabel != null) != (lastSelectedCellColumnLabel != null)) {
			throw new IllegalArgumentException();
		}
		this.dataViewDescription= dataViewDescription;
		this.anchorRowLabel= anchorRowLabel;
		this.anchorColumnLabel= anchorColumnLabel;
		this.lastSelectedCellRowLabel= lastSelectedCellRowLabel;
		this.lastSelectedCellColumnLabel= lastSelectedCellColumnLabel;
	}
	
	
	public @Nullable DataViewDescription getDataViewDescription() {
		return this.dataViewDescription;
	}
	
	
	@Override
	public boolean isEmpty() {
		return (this.anchorRowLabel == null);
	}
	
	
	public @Nullable String getAnchorRowLabel() {
		return this.anchorRowLabel;
	}
	
	public @Nullable String getAnchorColumnLabel() {
		return this.anchorColumnLabel;
	}
	
	public @Nullable String getLastSelectedCellRowLabel() {
		return this.lastSelectedCellRowLabel;
	}
	
	public @Nullable String getLastSelectedCellColumnLabel() {
		return this.lastSelectedCellColumnLabel;
	}
	
	
	@Override
	public int hashCode() {
		int h= Objects.hashCode(this.dataViewDescription);
		h= h + Objects.hashCode(this.anchorRowLabel);
		h= h * 7 + Objects.hashCode(this.anchorColumnLabel);
		h= h * 17 + Objects.hashCode(this.lastSelectedCellRowLabel);
		h= h * 99 + Objects.hashCode(this.lastSelectedCellColumnLabel);
		return h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RDataTableSelection) {
			final RDataTableSelection other= (RDataTableSelection) obj;
			return (Objects.equals(this.dataViewDescription, other.dataViewDescription)
					&& Objects.equals(this.anchorRowLabel, other.anchorRowLabel)
					&& Objects.equals(this.anchorColumnLabel, other.anchorColumnLabel)
					&& Objects.equals(this.lastSelectedCellRowLabel, other.lastSelectedCellRowLabel)
					&& Objects.equals(this.lastSelectedCellColumnLabel, other.lastSelectedCellColumnLabel) );
		}
		return false;
	}
	
}
