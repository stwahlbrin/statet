/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public class RDataTableContentDescription implements Immutable {
	
	
	private final RElementName elementName;
	private final RObject struct;
	
	private final String label;
	
	private final RTool rHandle;
	
	private final ImList<RDataTableColumn> columnHeaderRows;
	private final ImList<RDataTableColumn> rowHeaderColumns;
	
	private final ImList<RDataTableColumn> dataColumns;
	
	
	public RDataTableContentDescription(final RElementName elementName, final RObject struct,
			final RTool rHandle,
			final ImList<RDataTableColumn> columnHeaderRows, final ImList<RDataTableColumn> rowHeaderColumns,
			final ImList<RDataTableColumn> dataColumns) {
		this.elementName= nonNullAssert(elementName);
		this.struct= nonNullAssert(struct);
		this.label= elementName.getDisplayName();
		this.rHandle= rHandle;
		
		this.columnHeaderRows= columnHeaderRows;
		this.rowHeaderColumns= rowHeaderColumns;
		this.dataColumns= dataColumns;
	}
	
	
	
	public RElementName getElementName() {
		return this.elementName;
	}
	
	public RObject getRElementStruct() {
		return this.struct;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public RTool getRHandle() {
		return this.rHandle;
	}
	
	
	public ImList<RDataTableColumn> getColumnHeaderRows() {
		return this.columnHeaderRows;
	}
	
	public ImList<RDataTableColumn> getRowHeaderColumns() {
		return this.rowHeaderColumns;
	}
	
	public ImList<RDataTableColumn> getDataColumns() {
		return this.dataColumns;
	}
	
}
