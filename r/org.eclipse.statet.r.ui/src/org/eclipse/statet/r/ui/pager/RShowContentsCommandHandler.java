/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.pager;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.NonNull_StringArray_TYPE;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.Nullable_StringArray_TYPE;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.ide.IDE;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;
import org.eclipse.statet.jcommons.util.StringUtils;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.ui.pager.RPagerEditorInput;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rj.ts.core.AbstractRToolCommandHandler;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class RShowContentsCommandHandler extends AbstractRToolCommandHandler {
	
	
	public static final String SHOW_CONTENTS_COMMAND_ID= "r/showContents"; //$NON-NLS-1$
	
	
	public RShowContentsCommandHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final RToolService r, final ToolCommandData data,
			final ProgressMonitor m) throws StatusException {
		switch (id) {
		case SHOW_CONTENTS_COMMAND_ID:
			{	final var contents= data.getRequired("contents", NonNull_StringArray_TYPE);
				var title= data.getStringRequired("title");
				final var headers= data.getRequired("headers", Nullable_StringArray_TYPE);
				
				title= StringUtils.toSimpleSingleLine(title);
				final var files= new RPagerEditorInput. @NonNull TextFile[contents.length];
				for (int i= 0; i < files.length; i++) {
					String header= headers[i];
					if (header == null) {
						header= "#" + (i + 1);
					}
					files[i]= new RPagerEditorInput.TextFile(header, nonNullAssert(contents[i]));
				}
				
				final var editorInput= new RPagerEditorInput(title, r.getTool(),
						ImCollections.newList(files) );
				try {
					UIAccess.checkedSyncExec(() -> {
						final var workbenchPage= NicoUI.getToolRegistry().findWorkbenchPage(r.getTool());
						IDE.openEditor(workbenchPage, editorInput, "org.eclipse.statet.r.editors.RPager", true);
					});
				}
				catch (final CoreException e) {
					throw new StatusException(new ErrorStatus(RUI.BUNDLE_ID, 0,
							"An error occurred when opening the R Pager view.",
							e ));
				}
				
				return Status.OK_STATUS;
			}
		default:
			throw new UnsupportedOperationException();
		}
	}
	
}
