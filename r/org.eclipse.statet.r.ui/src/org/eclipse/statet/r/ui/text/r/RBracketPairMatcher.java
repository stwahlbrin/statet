/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.text.r;

import org.eclipse.statet.ecommons.text.PairMatcher;

import org.eclipse.statet.r.core.source.RHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;


public class RBracketPairMatcher extends PairMatcher {
	
	
	public static final char[][] BRACKETS = { {'{', '}'}, {'(', ')'}, {'[', ']'} };
	
	private static final String[] CONTENT_TYPES= new String[] {
		RDocumentConstants.R_DEFAULT_CONTENT_TYPE
	};
	
	
	public RBracketPairMatcher(final RHeuristicTokenScanner scanner) {
		this(scanner, CONTENT_TYPES);
	}
	
	public RBracketPairMatcher(final RHeuristicTokenScanner scanner, final String[] partitions) {
		super(BRACKETS,
				scanner.getDocumentPartitioning(),
				partitions,
				scanner,
				(char) 0);
	}
	
}
