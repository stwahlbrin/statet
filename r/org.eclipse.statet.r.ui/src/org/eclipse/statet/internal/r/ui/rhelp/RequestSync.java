/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import org.eclipse.core.runtime.jobs.Job;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RequestSync {
	
	
	public final static class RequestKey {
		
		
		private final boolean isExplicite;
		
		private volatile boolean isCanceled;
		@Nullable Job job;
		
		
		public RequestKey(final boolean explicite) {
			this.isExplicite= explicite;
		}
		
		
		public boolean isExplicite() {
			return this.isExplicite;
		}
		
		public void setAsync(final Job job) {
			this.job= job;
			
			if (this.isCanceled) {
				job.cancel();
			}
		}
		
		public boolean isCanceled() {
			return this.isCanceled;
		}
		
		public void cancel() {
			this.isCanceled= true;
			final Job job= this.job;
			if (job != null) {
				job.cancel();
			}
		}
		
		public boolean isValid() {
			return (!this.isCanceled);
		}
		
	}
	
	
	private @Nullable RequestKey expliciteRequest;
	private @Nullable RequestKey linkRequest;
	
	
	public RequestSync() {
	}
	
	
	public synchronized @Nullable RequestKey newRequest(final boolean explicite) {
		if (explicite) {
			if (this.linkRequest != null) {
				this.linkRequest.cancel();
				this.linkRequest= null;
			}
			if (this.expliciteRequest != null) {
				this.expliciteRequest.cancel();
				this.expliciteRequest= null;
			}
			return this.expliciteRequest= new RequestKey(true);
		}
		else {
			if (this.expliciteRequest == null) {
				return new RequestKey(false);
			}
			return null;
		}
	}
	
	public synchronized boolean startRequest(final RequestKey request) {
		if (request.isExplicite) {
			return request.isValid();
		}
		else {
			if (this.expliciteRequest == null) {
				if (this.linkRequest != null) {
					this.linkRequest.cancel();
					this.linkRequest= null;
				}
				this.linkRequest= request;
				return true;
			}
			return false;
		}
	}
	
	public synchronized void deleteRequest(final RequestKey request) {
		if (request.isExplicite) {
			if (this.expliciteRequest == request) {
				this.expliciteRequest= null;
			}
		}
		else {
			if (this.linkRequest == request) {
				this.linkRequest= null;
			}
		}
	}
	
}
