/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilter;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class TextSearchType {
	
	public static final TextSearchType ECLIPSE= new Eclipse();
	public static final TextSearchType REGEX= new Regex();
	public static final TextSearchType EXACT= new Exact();
	
	public static final ImList<TextSearchType> TYPES= ImCollections.newList(ECLIPSE, REGEX, EXACT);
	
	
	private static class Eclipse extends TextSearchType {
		
		private Eclipse() {
			super(0, Messages.TextSearch_Eclipse_label);
		}
		
		@Override
		public String getRPattern(final String pattern) {
			if (pattern.length() == 0) {
				return pattern;
			}
			final StringBuilder sb= new StringBuilder(pattern.length());
			int index= 0;
			if (pattern.charAt(0) == '*') {
				index++;
			}
			else {
				sb.append('^');
			}
			while (index < pattern.length()) {
				final char c= pattern.charAt(index++);
				switch (c) {
				case '\\':
					if (index < pattern.length()) {
						final char c2= pattern.charAt(index++);
						sb.append('\\');
						sb.append(c2);
					}
					continue;
				case '*':
					sb.append(".*"); //$NON-NLS-1$
					continue;
				case '?':
					sb.append(".?"); //$NON-NLS-1$
					continue;
				case '.':
				case '|':
				case '(':
				case ')':
				case '[':
				case '{':
				case '^':
				case '$':
				case '+':
					sb.append('\\');
					sb.append(c);
					continue;
				default:
					sb.append(c);
					continue;
				}
			}
			return sb.toString();
		}
		
	}
	
	private static class Regex extends TextSearchType {
		
		private Regex() {
			super(1, Messages.TextSearch_Regex_label);
		}
		
		@Override
		public String getRPattern(final String pattern) {
			return pattern;
		}
		
	}
	
	private static class Exact extends TextSearchType {
		
		private Exact() {
			super(2, Messages.TextSearch_Exact_label);
		}
		
		@Override
		public String getRPattern(final String pattern) {
			return pattern;
		}
		
	}
	
	
	private final int id;
	
	private final String label;
	
	
	private TextSearchType(final int id, final String label) {
		this.id= id;
		this.label= label;
	}
	
	
	public int getId() {
		return this.id;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public abstract String getRPattern(String pattern);
	
	
	@Override
	public String toString() {
		return getClass().getName();
	}
	
}
