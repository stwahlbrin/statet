/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import java.util.Collection;

import org.eclipse.statet.r.core.pkgmanager.IRPkgData;
import org.eclipse.statet.rj.renv.core.RPkgCompilation;
import org.eclipse.statet.rj.renv.core.RPkgList;


class Util {
	
	
	public static IRPkgData getPkgByRepo(final RPkgCompilation<? extends IRPkgData> collection,
			final String name, final String repoId) {
		for (final RPkgList<? extends IRPkgData> list : collection.getAll()) {
			final IRPkgData pkg= list.get(name);
			if (pkg != null && repoId.equals(pkg.getRepoId())) {
				return pkg;
			}
		}
		return null;
	}
	
	public static boolean hasPkgPriority(final RPkgCompilation<? extends IRPkgData> collection,
			final String name, final Collection<?> priorities) {
		for (final RPkgList<? extends IRPkgData> list : collection.getAll()) {
			final IRPkgData pkg= list.get(name);
			if (pkg != null && priorities.contains(pkg.getPriority())) {
				return true;
			}
		}
		return false;
	}
	
}
