/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.ui.dataeditor.RDataTableViewer;


@NonNullByDefault
public class FindDialogHandler extends AbstractHandler {
	
	
	private final IWorkbenchPart workbenchPart;
	
	
	public FindDialogHandler(final IWorkbenchPart part) {
		this.workbenchPart= part;
	}
	
	
	protected @Nullable RDataTableViewer getTable() {
		return this.workbenchPart.getAdapter(RDataTableViewer.class);
	}
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		final RDataTableViewer table= getTable();
		setBaseEnabled(table != null);
		
		final IWorkbenchPage page= this.workbenchPart.getSite().getPage();
		if (page.getActivePart() == this.workbenchPart) {
			final FindDataDialog dialog= FindDataDialog.get(page.getWorkbenchWindow());
			if (dialog != null) {
				dialog.update(this.workbenchPart);
			}
		}
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchWindow window= this.workbenchPart.getSite().getWorkbenchWindow();
		
		final FindDataDialog dialog= FindDataDialog.getCreate(window);
		dialog.update(this.workbenchPart);
		dialog.open();
		
		return null;
	}
	
}
