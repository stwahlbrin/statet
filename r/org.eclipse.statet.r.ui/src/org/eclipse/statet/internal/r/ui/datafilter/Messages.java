/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilter;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String IntervalFilter_label;
	public static String LevelFilter_label;
	public static String LevelFilter_TooMuch_message;
	public static String TextFilter_label;
	public static String TextFilter_TooMuch_message;
	
	public static String TextSearch_Eclipse_label;
	public static String TextSearch_Regex_label;
	public static String TextSearch_Exact_label;
	
	public static String UpdateJob_label;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
