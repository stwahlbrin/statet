/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable.css.properties;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.swt.graphics.Color;

import org.w3c.dom.css.CSSValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.style.GridStyling;

import org.eclipse.statet.internal.r.ui.intable.css.dom.TableRegionElement;


@NonNullByDefault
@SuppressWarnings("restriction")
public class TableRegionPropertyHandler extends AbstractDataTablePropertyHandler {
	
	
	public TableRegionPropertyHandler() {
	}
	
	
	@Override
	public @Nullable String retrieveCSSProperty(final Object element,
			final String property, final @Nullable String pseudo,
			final CSSEngine engine) throws Exception {
		if (!(element instanceof TableRegionElement)) {
			return null;
		}
		final var regionElement= (TableRegionElement)element;
		final var displayMode= getDisplayMode(regionElement, pseudo);
		
		if (displayMode != null) {
			switch (property) {
			case "grid-line-color":
				return receiveStyleAttribute(regionElement, displayMode, null,
						GridStyling.GRID_LINE_COLOR, Color.class,
						engine );
			default:
				break;
			}
		}
		return null;
	}
	
	@Override
	public boolean applyCSSProperty(final Object element,
			final String property, final CSSValue value, final @Nullable String pseudo,
			final CSSEngine engine) throws Exception {
		if (!(element instanceof TableRegionElement)) {
			return false;
		}
		if (DEBUG) {
			print(element, pseudo, property, value);
		}
		final var regionElement= (TableRegionElement)element;
		final var displayMode= getDisplayMode(regionElement, pseudo);
		
		if (displayMode != null) {
			switch (property) {
			case "grid-line-color":
				applyStyleAttribute(regionElement, displayMode, null,
						GridStyling.GRID_LINE_COLOR, Color.class, value,
						engine );
				return true;
			default:
				break;
			}
		}
		return true;
	}
	
	
	private static @Nullable DisplayMode getDisplayMode(final TableRegionElement cellElement,
			final @Nullable String pseudo) {
		if (pseudo == null) {
			return DisplayMode.NORMAL;
		}
		return null;
	}
	
	
	private static <T> @Nullable String receiveStyleAttribute(final TableRegionElement regionElement,
			final DisplayMode displayMode, @Nullable String label,
			final ConfigAttribute<T> attribute, final Class<T> valueType,
			final CSSEngine engine) throws Exception {
		final var dataTable= regionElement.getParentNode().getNativeWidget();
		if (label == null) {
			label= regionElement.getCSSClass();
		}
		final var value= dataTable.getConfigRegistry().getSpecificAttribute(
				attribute, displayMode, label );
		return engine.convert(value, valueType, null);
	}
	
	private static <T> void applyStyleAttribute(final TableRegionElement regionElement,
			final DisplayMode displayMode, @Nullable String label,
			final ConfigAttribute<T> attribute, final Class<T> valueType, final CSSValue cssValue,
			final CSSEngine engine) throws Exception {
		final var dataTable= regionElement.getParentNode().getNativeWidget();
		if (label == null) {
			label= regionElement.getCSSClass();
		}
		final var oldValue= dataTable.getConfigRegistry().getSpecificAttribute(
				attribute, displayMode, label );
		if (oldValue != null) {
			@SuppressWarnings("unchecked")
			final var value= (T)engine.convert(cssValue, valueType, dataTable.getDisplay());
			if (value != null && !value.equals(oldValue)) {
				dataTable.getConfigRegistry().registerAttribute(attribute, value);
			}
		}
	}
	
}
