/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.wizard.WizardPageSupport;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.ecommons.databinding.core.util.DirtyTracker;
import org.eclipse.statet.ecommons.resources.core.util.RelativePathValidator;
import org.eclipse.statet.ecommons.resources.core.util.StringToPathConverter;
import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.ltk.core.util.UserInfo;
import org.eclipse.statet.r.core.rlang.RPkgNameValidator;


public class RPkgProjectWizardPage extends WizardPage {
	
	
	private static final String AUTHOR_HISTORY= "RPkgAuthor.history";
	private static final String MAINTAINER_HISTORY= "RPkgMaintainer.history";
	
	
	private final NewRProjectWizardPage projectPage;
	
	private Text pkgFolderControl;
	
	private Text pkgNameControl;
	private Combo pkgAuthorControl;
	private Combo pkgMaintainerControl;
	
	private DataBindingContext dbc;
	
	private IObservableValue<IPath> pkgFolderValue;
	private RelativePathValidator pkgFolderValidator;
	
	private IObservableValue<String> pkgNameValue;
	private IObservableValue<String> pkgAuthorValue;
	private IObservableValue<String> pkgMaintainerValue;
	
	private boolean wasVisible;
	private DirtyTracker pkgUserChanged;
	
	
	public RPkgProjectWizardPage(final NewRProjectWizardPage projectPage) {
		super("RPkgWizardPage"); //$NON-NLS-1$
		
		setTitle(Messages.RPkgWizardPage_title);
		setDescription(Messages.RPkgWizardPage_description);
		
		this.projectPage= projectPage;
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		final Realm realm= Realm.getDefault();
		this.pkgFolderValue= new WritableValue<>(realm, Path.EMPTY, IPath.class);
		this.pkgNameValue= new WritableValue<>(realm, "", String.class); //$NON-NLS-1$
		this.pkgAuthorValue= new WritableValue<>(realm, "", String.class); //$NON-NLS-1$
		this.pkgMaintainerValue= new WritableValue<>(realm, "", String.class); //$NON-NLS-1$
		
		initializeDialogUnits(parent);
		
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newContentGrid());
		
		{	final Composite group= createRPkgGroup(composite);
			group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		{	final Composite group= createFolderGroup(composite);
			group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		
		Dialog.applyDialogFont(composite);
		setControl(composite);
		
		this.dbc= new DataBindingContext(realm);
		addBindings(this.dbc, realm);
		this.projectPage.getProjectValue().addValueChangeListener(this::onProjectChanged);
		WizardPageSupport.create(this, this.dbc);
		
		loadSettings();
	}
	
	protected Composite createFolderGroup(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newGroupGrid(2));
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("Package root folder inside project (optional): ");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			
			this.pkgFolderControl= new Text(composite, SWT.SINGLE | SWT.BORDER);
			this.pkgFolderControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		}
		return composite;
	}
	
	protected Composite createRPkgGroup(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newGroupGrid(2));
		composite.setText("R Package");
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("Package &name:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			
			this.pkgNameControl= new Text(composite, SWT.LEFT | SWT.SINGLE | SWT.BORDER);
			this.pkgNameControl.setFont(JFaceResources.getTextFont());
			this.pkgNameControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		}
		LayoutUtils.addSmallFiller(composite, false);
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("&Author(s):");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			
			this.pkgAuthorControl= new Combo(composite, SWT.DROP_DOWN | SWT.BORDER);
			this.pkgAuthorControl.setFont(JFaceResources.getTextFont());
			this.pkgAuthorControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		}
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("&Maintainer:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			
			this.pkgMaintainerControl= new Combo(composite, SWT.DROP_DOWN | SWT.BORDER);
			this.pkgMaintainerControl.setFont(JFaceResources.getTextFont());
			this.pkgMaintainerControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		}
		
		return composite;
	}
	
	protected void addBindings(final DataBindingContext dbc, final Realm realm) {
		this.pkgUserChanged= new DirtyTracker();
		
		final Binding binding= dbc.bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.pkgNameControl),
				this.pkgNameValue,
				new UpdateValueStrategy<String, String>()
						.setAfterGetValidator(new RPkgNameValidator()),
				null );
		this.pkgUserChanged.add(binding);
		dbc.bindValue(
				WidgetProperties.text()
						.observe(this.pkgAuthorControl),
				this.pkgAuthorValue );
		dbc.bindValue(
				WidgetProperties.text()
						.observe(this.pkgMaintainerControl),
				this.pkgMaintainerValue );
		
		this.pkgFolderValidator= new RelativePathValidator(IResource.FOLDER,
				null, "package root folder" );
		dbc.bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.pkgFolderControl),
				this.pkgFolderValue,
				new UpdateValueStrategy<String, IPath>()
						.setAfterGetValidator(this.pkgFolderValidator)
						.setConverter(new StringToPathConverter()),
				null );
	}
	
	private void onProjectChanged(final ValueChangeEvent<? extends IProject> event) {
		final boolean pkgUserDirty= this.pkgUserChanged.isDirty();
		
		final IProject project= event.diff.getNewValue();
		
		this.pkgFolderValidator.setBaseContainer(project);
		if (!pkgUserDirty) {
			this.pkgNameControl.setText(project.getName());
		}
		
		this.dbc.updateModels();
		
		if (!pkgUserDirty) {
			this.pkgUserChanged.resetDirty();
		}
	}
	
	@Override
	public void setVisible(final boolean visible) {
		super.setVisible(visible);
		if (visible) {
			final boolean firstTime= !this.wasVisible;
			this.wasVisible= true;
			
			if (firstTime) {
				setMessage(null);
				setErrorMessage(null);
			}
		}
	}
	
	protected void loadSettings() {
		final UserInfo authorInfo= UserInfo.getAuthorInfo();
		final IDialogSettings dialogSettings= getDialogSettings();
		initCombo(this.pkgAuthorControl, this.pkgAuthorValue,
				dialogSettings.getArray(AUTHOR_HISTORY),
				authorInfo.getName(), authorInfo.getSource() );
		initCombo(this.pkgMaintainerControl, this.pkgMaintainerValue,
				dialogSettings.getArray(MAINTAINER_HISTORY),
				authorInfo.getName() + " <" + authorInfo.getEmail() + ">", authorInfo.getSource() ); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	private void initCombo(final Combo combo, final IObservableValue<String> value,
			String[] history, final String infoText, final int infoSource) {
		history= DialogUtils.noNull(history);
		String text= ""; //$NON-NLS-1$
		if (history.length > 0) {
			text= history[0];
		}
		if (infoSource <= 2) {
			history= DialogUtils.combineHistoryItems(history, infoText);
		}
		if (text.isEmpty()) {
			text= infoText;
		}
		combo.setItems(history);
		value.setValue(text);
	}
	
	protected void saveSettings() {
		final IDialogSettings dialogSettings= getDialogSettings();
		DialogUtils.saveHistorySettings(dialogSettings, AUTHOR_HISTORY, getPkgAuthor());
		DialogUtils.saveHistorySettings(dialogSettings, MAINTAINER_HISTORY, getPkgMaintainer());
	}
	
	
	public String getPkgName() {
		return this.pkgNameValue.getValue();
	}
	
	public IPath getPkgFolderPath() {
		return this.pkgFolderValue.getValue();
	}
	
	public String getPkgAuthor() {
		return this.pkgAuthorValue.getValue();
	}
	
	public String getPkgMaintainer() {
		return this.pkgMaintainerValue.getValue();
	}
	
}
