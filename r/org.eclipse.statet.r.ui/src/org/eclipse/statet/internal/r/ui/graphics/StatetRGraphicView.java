/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.graphics;

import java.util.List;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolSessionData;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.rj.eclient.graphics.ERGraphic;
import org.eclipse.statet.rj.eclient.graphics.ERGraphicsManager;
import org.eclipse.statet.rj.eclient.graphics.PageBookRGraphicView;
import org.eclipse.statet.rj.eclient.graphics.RGraphicPage;
import org.eclipse.statet.rj.ts.core.RTool;


public class StatetRGraphicView extends PageBookRGraphicView {
	
	
	public StatetRGraphicView() {
	}
	
	
	@Override
	protected ERGraphicsManager loadManager() {
		return RUIPlugin.getInstance().getCommonRGraphicFactory();
	}
	
	@Override
	protected RGraphicPage doCreatePage(final RGraphicSession session) {
		return new StatetRGraphicPage(session.getGraphic());
	}
	
	@Override
	protected IHandler2 createNewPageHandler() {
		return new NewDevHandler() {
			@Override
			protected RTool getTool() throws CoreException {
				final Tool tool= NicoUI.getToolRegistry().getActiveToolSession(
						UIAccess.getActiveWorkbenchPage(false)).getTool();
				NicoUITools.accessTool(RConsoleTool.TYPE, tool);
				return (RTool)tool;
			}
		};
	}
	
	@Override
	protected void collectContextMenuPreferencePages(final List<String> pageIds) {
		pageIds.add("org.eclipse.statet.r.preferencePages.RGraphicsPage"); //$NON-NLS-1$
	}
	
	@Override
	public int canShowGraphic(final ERGraphic graphic) {
		int canShow= super.canShowGraphic(graphic);
		if (canShow > 0) {
			final RTool tool= graphic.getRHandle();
			if (tool != null) {
				final WorkbenchToolSessionData session= NicoUI.getToolRegistry()
						.getActiveToolSession(getViewSite().getPage());
				if (session.getTool() == tool) {
					canShow+= 1;
				}
			}
		}
		return canShow;
	}
	
}
