/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.TextPresentation;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationPresenter;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.input.OffsetStringParserInput;

import org.eclipse.statet.ecommons.text.core.FragmentDocument;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionUtils;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistCompletionInformationProposalWrapper;
import org.eclipse.statet.r.core.rmodel.Parameters;
import org.eclipse.statet.r.core.source.ast.Assignment;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.Pipe;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.r.core.source.doc.RPartitionNodeScanner;
import org.eclipse.statet.r.ui.editors.RSourceEditor;


@NonNullByDefault
public class RContextInformationValidator implements IContextInformationValidator, IContextInformationPresenter {
	
	
	private final RSourceEditor sourceEditor;
	
	private @Nullable RArgumentListContextInformation info;
	
	private long scannedArgsStamp;
	private FCall. @Nullable Args scannedArgs;
	
	private int lastPresentation= -2;
	
	
	public RContextInformationValidator(final RSourceEditor sourceEditor) {
		this.sourceEditor= sourceEditor;
	}
	
	
	@Override
	public void install(IContextInformation info, final ITextViewer viewer, final int offset) {
		if (info instanceof AssistCompletionInformationProposalWrapper) {
			info= ((AssistCompletionInformationProposalWrapper)info).getContextInformation();
		}
		
		this.scannedArgs= null;
		this.lastPresentation= -2;
		
		if (info instanceof RArgumentListContextInformation
				&& viewer == this.sourceEditor.getViewer()) {
			this.info= (RArgumentListContextInformation)info;
		}
		else {
			this.info= null;
			return;
		}
	}
	
	@Override
	public boolean isContextInformationValid(final int offset) {
		final RArgumentListContextInformation info= this.info;
		if (info == null) {
			return false;
		}
		if (offset < info.getContextInformationPosition()
				|| offset > this.sourceEditor.getViewer().getDocument().getLength()) {
			return false;
		}
		final FCall.Args args= getScannedArgs();
		if (args != null) {
			return (offset <= args.getEndOffset());
		}
		return (offset == info.getContextInformationPosition());
	}
	
	@Override
	public boolean updatePresentation(final int offset, final TextPresentation presentation) {
		final RArgumentListContextInformation info= this.info;
		if (info == null) {
			return false;
		}
		final Parameters parameters= info.getParameters();
		if (parameters != null && parameters.size() > 0) {
			final int argParameterIdx= getCurrentArgParameterIdx(offset);
			final var indexes= info.getInformationDisplayStringParameterIndexes();
			if (argParameterIdx >= 0 && argParameterIdx < indexes.size()) {
				if (argParameterIdx == this.lastPresentation) {
					return false;
				}
				final int start= indexes.getAt(argParameterIdx);
				final int stop= (argParameterIdx + 1 < indexes.size()) ?
						indexes.getAt(argParameterIdx + 1) : info.getInformationDisplayString().length();
				presentation.clear();
				presentation.addStyleRange(new StyleRange(start, stop - start, null, null, SWT.BOLD));
				this.lastPresentation= argParameterIdx;
				return true;
			}
		}
		if (this.lastPresentation >= 0) {
			presentation.clear();
			this.lastPresentation= -1;
			return true;
		}
		return false;
	}
	
	
	private FCall. @Nullable Args getScannedArgs() {
		final RArgumentListContextInformation info= nonNullAssert(this.info);
		final int startOffset= info.getFCallArgsOffset();
		AbstractDocument document= (AbstractDocument)this.sourceEditor.getViewer().getDocument();
		int docStartOffset= startOffset;
		if (document instanceof FragmentDocument) {
			final FragmentDocument fragmentDoc= (FragmentDocument)document;
			document= fragmentDoc.getMasterDocument();
			docStartOffset+= fragmentDoc.getOffsetInMasterDocument();
		}
		if (docStartOffset < 0) {
			docStartOffset= 0;
		}
		final long stamp= document.getModificationStamp();
		if (this.scannedArgs == null || this.scannedArgsStamp != stamp) {
			try {
				FCall.Args args= null;
				final TreePartitionNode rRootNode= RPartitionNodeScanner.findRRootNode(
						TreePartitionUtils.getNode(document, this.sourceEditor.getDocumentContentInfo().getPartitioning(),
								docStartOffset, true));
				if (rRootNode != null) {
					final int docEndOffset= Math.min(0x800, rRootNode.getEndOffset() - docStartOffset);
					final var input= new OffsetStringParserInput(
							document.get(docStartOffset, docEndOffset),
							startOffset );
					final var rParser= new RParser(this.sourceEditor.getRCoreAccess().getRSourceConfig(),
							AstInfo.LEVEL_MODEL_DEFAULT, null );
					args= rParser.parseFCallArgs(input.initAtOffset(), true);
				}
				this.scannedArgs= args;
				this.scannedArgsStamp= stamp;
			}
			catch (final Exception e) {
				this.scannedArgs= null;
			}
		}
		
		return this.scannedArgs;
	}
	
	private int getCurrentArgParameterIdx(final int offset) {
		final FCall.Args args= getScannedArgs();
		if (args != null) {
			final int callArgIdx= getCurrentArgInFCall(args, offset);
			if (callArgIdx >= 0) {
				final RAsts.FCallArgMatch match= matchArgs(args);
				final var parameter= match.getParameterForFCall(callArgIdx);
				if (parameter != null) {
					return parameter.index;
				}
			}
		}
		return -1;
	}
	
	private RAsts.FCallArgMatch matchArgs(final FCall.Args argsNode) {
		final RArgumentListContextInformation info= nonNullAssert(this.info);
		// assert info.getParameters() != null
		
		// like org.eclipse.statet.r.core.rsource.ast.RAsts#matchArgs(FCall, ParametersDefinition)
		@Nullable RAstNode replValueArg= null;
		ImList<@Nullable RAstNode> inject0Args= ImCollections.emptyList();
		final FCall fCallNode= info.getFCallNode();
		if (fCallNode != null) {
			final RAstNode parentNode= fCallNode.getRParent();
			if (parentNode != null) {
				switch (parentNode.getNodeType()) {
				case PIPE_FORWARD:
					{	final Pipe pipeNode= (Pipe)parentNode;
						if (pipeNode.getTargetChild() == fCallNode) {
							inject0Args= ImCollections.newList(pipeNode.getSourceChild());
						}
						break;
					}
				case A_LEFT:
				case A_EQUALS:
				case A_RIGHT:
					{	final Assignment assignNode= (Assignment)parentNode;
						if (assignNode.getTargetChild() == fCallNode) {
							replValueArg= assignNode.getSourceChild();
						}
						break;
					}
				default:
					break;
				}
			}
		}
		
		final var parameters= nonNullAssert(info.getParameters());
		return RAsts.matchArgs(argsNode, parameters, replValueArg, inject0Args);
	}
	
	
	private static int getCurrentArgInFCall(final FCall.Args args, final int offset) {
		final int last= args.getChildCount() - 1;
		if (last == -1) {
			return 0;
		}
		for (int i= 0; i < last; i++) {
			if (offset <= args.getSeparatorOffsets().getAt(i)) {
				return i;
			}
		}
		return last;
	}
	
}
