/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.StatusLineContributionItem;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.IShowInTargetList;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import org.eclipse.statet.jcommons.collections.ImLongList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.ContextHandlers;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.base.ui.contentfilter.FilterPage;
import org.eclipse.statet.base.ui.contentfilter.FilterView;
import org.eclipse.statet.internal.r.ui.datafilterview.RDataFilterPage;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.dataeditor.RDataEditorInput;
import org.eclipse.statet.r.ui.dataeditor.RDataTableCallbacks;
import org.eclipse.statet.r.ui.dataeditor.RDataTableInput;
import org.eclipse.statet.r.ui.dataeditor.RDataTableSelection;
import org.eclipse.statet.r.ui.dataeditor.RDataTableViewer;
import org.eclipse.statet.r.ui.dataeditor.RLiveDataEditorInput;


@NonNullByDefault
public class RDataEditor extends EditorPart
		implements IShowInTargetList { // INavigationLocationProvider ?
	
	
	private class ActivationListener implements IPartListener {
		
		@Override
		public void partActivated(final IWorkbenchPart part) {
			if (part == RDataEditor.this) {
				updateInfoStatusLine();
			}
		}
		
		@Override
		public void partBroughtToTop(final IWorkbenchPart part) {
		}
		
		@Override
		public void partClosed(final IWorkbenchPart part) {
		}
		
		@Override
		public void partOpened(final IWorkbenchPart part) {
		}
		
		@Override
		public void partDeactivated(final IWorkbenchPart part) {
		}
		
	}
	
	private class Callbacks implements RDataTableCallbacks {
		
		
		@Override
		public IWorkbenchPart getWorkbenchPart() {
			return RDataEditor.this;
		}
		
		@Override
		public @Nullable IServiceLocator getServiceLocator() {
			return getEditorSite();
		}
		
		@Override
		public boolean isCloseSupported() {
			return true;
		}
		
		@Override
		public void close() {
			RDataEditor.this.close(false);
		}
		
		@Override
		public void show(final IStatus status) {
			StatusManager.getManager().handle(status);
		}
		
	}
	
	
	private RDataTableViewer viewer;
	
	private ContextHandlers handlers;
	
	private final ActivationListener activationListener= new ActivationListener();
	
	private @Nullable RDataEditorOutlinePage outlinePage;
	
	private @Nullable RDataFilterPage filterPage;
	
	private final RDataTableInput.StateListener inputStateListener= new RDataTableInput.StateListener() {
		@Override
		public void tableUnavailable() {
			close(false);
		}
	};
	
	
	public RDataEditor() {
	}
	
	
	@Override
	public void init(final IEditorSite site, final IEditorInput input) throws PartInitException {
		setSite(site);
		
		try {
			doSetInput(input);
		}
		catch (final CoreException e) {
			throw new PartInitException("The R data editor could not be initialized.");
		}
	}
	
	@Override
	public void dispose() {
		getEditorSite().getWorkbenchWindow().getPartService().removePartListener(this.activationListener);
		if (this.handlers != null) {
			this.handlers.dispose();
			this.handlers= null;
		}
		
		disposeTableInput();
		
		super.dispose();
	}
	
	
	@Override
	protected void setInput(final IEditorInput input) {
		setInputWithNotify(input);
	}
	
	@Override
	protected void setInputWithNotify(final IEditorInput input) {
		try {
			doSetInput(input);
			firePropertyChange(PROP_INPUT);
		}
		catch (final CoreException e) {
			StatusManager.getManager().handle(new Status(
					IStatus.ERROR, RUI.BUNDLE_ID, "An error occurred when opening the element.", e));
		}
	}
	
	protected void doSetInput(final IEditorInput input) throws CoreException {
		if (input == null) {
			throw new NullPointerException("input");
		}
		if (!(input instanceof RLiveDataEditorInput)) {
			throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID, -1,
					NLS.bind("The element ''{0}'' is not supported by the R data editor.", input.getName()), null));
		}
		
		super.setInput(input);
		
		setPartName(input.getName());
		setTitleToolTip(input.getToolTipText());
	}
	
	public RDataTableViewer getRDataTable() {
		return this.viewer;
	}
	
	
	@Override
	public boolean isDirty() {
		return false;
	}
	
	@Override
	public void doSave(final IProgressMonitor monitor) {
	}
	
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	@Override
	public void doSaveAs() {
	}
	
	
	@Override
	public void createPartControl(final Composite parent) {
		this.viewer= new RDataTableViewer(parent, new Callbacks());
		getEditorSite().getWorkbenchWindow().getPartService().addPartListener(this.activationListener);
		
		final IWorkbenchPartSite site= getSite();
		site.setSelectionProvider(this.viewer);
		initActions(site,
				this.handlers= new ContextHandlers(site) );
		
		initStatusLine();
		initTableInput();
	}
	
	private void initTableInput() {
		final var editorInput= (RDataEditorInput)getEditorInput();
		if (editorInput != null) {
			final RDataTableInput tableInput= editorInput.getRDataTableInput();
			if (tableInput != null) {
				tableInput.addStateListener(this.inputStateListener);
				if (tableInput.isAvailable()) {
					this.viewer.setInput(tableInput);
				}
				else {
					close(false);
				}
			}
		}
	}
	
	private void disposeTableInput() {
		final var editorInput= (RDataEditorInput)getEditorInput();
		if (editorInput != null) {
			final RDataTableInput tableInput= editorInput.getRDataTableInput();
			if (tableInput != null) {
				tableInput.removeStateListener(this.inputStateListener);
			}
		}
	}
	
	protected void initActions(final IServiceLocator serviceLocator, final ContextHandlers handlers) {
		WorkbenchUIUtils.activateContext(serviceLocator, "org.eclipse.statet.r.contexts.RDataEditor"); //$NON-NLS-1$
		
		{	final IHandler2 handler= new RefreshHandler(this.viewer);
			handlers.addActivate(IWorkbenchCommandConstants.FILE_REFRESH, handler);
		}
		{	final IHandler2 handler= new SelectAllHandler(this.viewer);
			handlers.addActivate(IWorkbenchCommandConstants.EDIT_SELECT_ALL, handler);
		}
		{	final IHandler2 handler= new CopyDataHandler(this.viewer);
			handlers.addActivate(IWorkbenchCommandConstants.EDIT_COPY, handler);
		}
		{	final IHandler2 handler= new FindDialogHandler(this);
			handlers.addActivate(IWorkbenchCommandConstants.EDIT_FIND_AND_REPLACE, handler);
		}
		{	final IHandler2 handler= new GotoCellHandler(this.viewer);
			handlers.addActivate(ITextEditorActionDefinitionIds.LINE_GOTO, handler);
		}
	}
	
	protected void initStatusLine() {
		this.viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				final IStatusLineManager manager= getEditorSite().getActionBars().getStatusLineManager();
				final RDataTableSelection selection= (RDataTableSelection)event.getSelection();
				updateInfoStatusLine();
				final String s= toStatusString(selection);
				if (s != null) {
					manager.setMessage(s);
				}
				else {
					manager.setMessage(null);
				}
			}
		});
	}
	
	@SuppressWarnings("null")
	private @Nullable String toStatusString(final RDataTableSelection selection) {
		String rowLabel= null;
		if (selection == null || (rowLabel= selection.getAnchorRowLabel()) == null) {
			return null;
		}
		final StringBuilder sb= new StringBuilder();
		
		sb.append(rowLabel);
		if (!rowLabel.isEmpty() && !selection.getAnchorColumnLabel().isEmpty()) {
			sb.append(", "); //$NON-NLS-1$
		}
		sb.append(selection.getAnchorColumnLabel());
		
		if ((rowLabel= selection.getLastSelectedCellRowLabel()) != null) {
			sb.append(" ("); //$NON-NLS-1$
			sb.append(rowLabel);
			if (!rowLabel.isEmpty() && !selection.getLastSelectedCellColumnLabel().isEmpty()) {
				sb.append(", "); //$NON-NLS-1$
			}
			sb.append(selection.getLastSelectedCellColumnLabel());
			sb.append(')');
		}
		
		return sb.toString();
	}
	
	private void updateInfoStatusLine() {
		final IStatusLineManager manager= getEditorSite().getActionBars().getStatusLineManager();
		final var viewDescription= this.viewer.getDataView();
		
		final IContributionItem dimItem= manager.find("data.dimension"); //$NON-NLS-1$
		String dimText= ""; //$NON-NLS-1$
		if (viewDescription != null) {
			final StringBuilder sb= new StringBuilder("Dim: "); //$NON-NLS-1$
			appendDim(viewDescription.getDataDimension(), sb);
			if (!viewDescription.getViewDataDimension().equals(viewDescription.getDataDimension())) {
				sb.append(" ("); //$NON-NLS-1$
				appendDim(viewDescription.getViewDataDimension(), sb);
				sb.append(')');
			}
			dimText= sb.toString();
		}
		((StatusLineContributionItem)dimItem).setText(dimText);
	}
	
	private static void appendDim(final ImLongList dim, final StringBuilder sb) {
		if (dim.size() > 0) {
			sb.append(dim.getAt(0));
			for (int i= 1; i < dim.size(); i++) {
				sb.append(" × "); //$NON-NLS-1$
				sb.append(dim.getAt(i));
			}
		}
	}
	
	@Override
	public void setFocus() {
		this.viewer.setFocus();
	}
	
	public void close(final boolean save) {
		final Display display= getSite().getShell().getDisplay();
		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				getSite().getPage().closeEditor(RDataEditor.this, save);
			}
		});
	}
	
	
	protected RDataEditorOutlinePage createOutlinePage() {
		return new RDataEditorOutlinePage(this);
	}
	
	protected RDataFilterPage createFilterPage() {
		return new RDataFilterPage(this);
	}
	
	@Override
	public @NonNull String[] getShowInTargetIds() {
		return new @NonNull String[] { IPageLayout.ID_OUTLINE, FilterView.VIEW_ID };
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RDataTableViewer.class) {
			return (T)this.viewer;
		}
		if (adapterType == IContentOutlinePage.class) {
			if (this.outlinePage == null) {
				this.outlinePage= createOutlinePage();
			}
			return (T)this.outlinePage;
		}
		if (adapterType == FilterPage.class) {
			if (this.filterPage == null) {
				this.filterPage= createFilterPage();
			}
			return (T)this.filterPage;
		}
		return super.getAdapter(adapterType);
	}
	
}
