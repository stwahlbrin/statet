/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable;

import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.data.ControlData;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;
import org.eclipse.statet.ecommons.waltable.data.convert.IDisplayConverter;
import org.eclipse.statet.ecommons.waltable.painter.cell.AbstractTextPainter;
import org.eclipse.statet.ecommons.waltable.style.CellStyleUtil;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public class RTextPainter extends AbstractTextPainter {
	
	
	/**
	 * Convert the data value of the cell using the {@link IDisplayConverter} from the {@link ConfigRegistry}
	 */
	private static Object getData(final LayerCell cell, final ConfigRegistry configRegistry) {
		final IDisplayConverter displayConverter= configRegistry.getAttribute(
				CellConfigAttributes.DISPLAY_CONVERTER, cell.getDisplayMode(),
				cell.getLabels().getLabels() );
		return (displayConverter != null) ?
				displayConverter.canonicalToDisplayValue(cell, configRegistry, cell.getDataValue(0, null)) : EMPTY;
	}
	
	
	private final StringBuilder tempText= new StringBuilder();
	
	private String currentText;
	private int currentTextWidth;
	
	
	public RTextPainter(final int space) {
		super(false, true, space, false, false, SWT.DRAW_TRANSPARENT);
	}
	
	
	@Override
	public long getPreferredWidth(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry){
		final Object data= getData(cell, configRegistry);
		setupGCFromConfig(gc, CellStyleUtil.getCellStyle(cell, configRegistry), data);
		return getWidthFromCache(gc, data.toString()) + (this.spacing * 2);
	}
	
	@Override
	public long getPreferredHeight(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		final Object data= getData(cell, configRegistry);
		setupGCFromConfig(gc, CellStyleUtil.getCellStyle(cell, configRegistry), data);
//		return gc.textExtent(data.toString(), swtDrawStyle).y;
		return gc.getFontMetrics().getHeight();
	}
	
	protected void setupGCFromConfig(final GC gc, final Style cellStyle, final Object data) {
		Color fg= cellStyle.getAttributeValue(CellStyling.FOREGROUND_COLOR);
		if (fg == null) {
			fg= GUIHelper.COLOR_LIST_FOREGROUND;
		}
//		Color bg= cellStyle.getAttributeValue(CellStyling.BACKGROUND_COLOR);
//		if (bg == null) {
//			bg= GUIHelper.COLOR_LIST_BACKGROUND;
//		}
		Font font;
		if ((!(data instanceof ControlData)) || 
				(font= cellStyle.getAttributeValue(CellStyling.CONTROL_FONT)) == null) {
			font= cellStyle.getAttributeValue(CellStyling.FONT);
		}
		
//		gc.setAntialias(SWT.DEFAULT);
		gc.setTextAntialias(SWT.DEFAULT);
		gc.setFont(font);
		gc.setForeground(fg);
//		gc.setBackground(bg);
	}
	
	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle lRectangle,
			final ConfigRegistry configRegistry) {
		if (this.paintBg) {
			super.paintCell(cell, gc, lRectangle, configRegistry);
		}
		
		final org.eclipse.swt.graphics.Rectangle originalClipping= gc.getClipping();
		gc.setClipping(SwtUtils.toSWT(lRectangle).intersection(originalClipping));
		
		final Object data= getData(cell, configRegistry);
		final Style cellStyle= CellStyleUtil.getCellStyle(cell, configRegistry);
		setupGCFromConfig(gc, cellStyle, data);
		
		// Draw Text
		String text= data.toString();
		final long width= lRectangle.width - (this.spacing * 2);
//		if (text.length() > width * 4) {
//			text= text.substring(0, width * 4);
//		}
		if (gc.getFont() == null) {
			gc.setFont(null);
		}
		// first get height because https://bugs.eclipse.org/bugs/show_bug.cgi?id=319125
		final int contentHeight= gc.getFontMetrics().getHeight();
		text= getTextToDisplay(cell, gc, width, text);
		final int contentWidth= getWidthFromCache(gc, text);
		
		gc.drawText(
				text,
				safe(lRectangle.x + this.spacing + CellStyleUtil.getHorizontalAlignmentPadding(
						(data instanceof ControlData) ?
								HorizontalAlignment.LEFT :
								cellStyle.getAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT),
						width, contentWidth )),
				safe(lRectangle.y + CellStyleUtil.getVerticalAlignmentPadding(VerticalAlignment.MIDDLE,
						lRectangle.height, contentHeight )),
				this.swtDrawStyle );
		
		gc.setClipping(originalClipping);
	}
	
}
