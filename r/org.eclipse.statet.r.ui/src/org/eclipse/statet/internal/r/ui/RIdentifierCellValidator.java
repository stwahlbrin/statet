/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.jface.viewers.ICellEditorValidator;

import org.eclipse.statet.r.core.refactoring.RRefactoringAdapter;


public class RIdentifierCellValidator implements ICellEditorValidator {
	
	
	private final RRefactoringAdapter fAdapter = new RRefactoringAdapter();
	
	
	@Override
	public String isValid(final Object value) {
		final String s = (value instanceof String) ? (String) value : null;
		final String message = fAdapter.validateIdentifier(s, null);
		if (message != null) {
			return message;
		}
		return null;
	}
	
}
