/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.correction;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.ui.actions.UIActions.NO_COMMAND_ID;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.LinkedPositionGroup;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.ltk.ui.LtkActions;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.LinkedNamesAssistProposal;
import org.eclipse.statet.r.core.rmodel.RElementAccess;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;


@NonNullByDefault
public class RLinkedNamesAssistProposal extends LinkedNamesAssistProposal<AssistInvocationContext> {
	
	
	public static final int IN_FILE= 1;
	public static final int IN_FILE_PRECEDING= 2;
	public static final int IN_FILE_FOLLOWING= 3;
	public static final int IN_CHUNK= 4;
	
	
	private final int mode;
	private final @Nullable TextRegion region;
	
	private final RElementAccess access;
	private final boolean isReplFName;
	
	
	public RLinkedNamesAssistProposal(final int mode,
			final AssistInvocationContext invocationContext, final RElementAccess access) {
		super(init(invocationContext, mode, false));
		this.mode= mode;
		this.region= null;
		this.access= access;
		this.isReplFName= checkReplFName();
	}
	
	public RLinkedNamesAssistProposal(final int mode,
			final AssistInvocationContext invocationContext, final RElementAccess access,
			final TextRegion region) {
		super(init(invocationContext, mode, true));
		this.mode= mode;
		this.region= nonNullAssert(region);
		this.access= access;
		this.isReplFName= checkReplFName();
	}
	
	private static ProposalParameters<AssistInvocationContext> init(
			final AssistInvocationContext invocationContext,
			final int mode, final boolean withRegion) {
		switch (mode) {
		case IN_FILE:
			return new ProposalParameters<>(invocationContext,
					LtkActions.QUICK_ASSIST_RENAME_IN_FILE,
					RUIMessages.Proposal_RenameInFile_label,
					RUIMessages.Proposal_RenameInFile_description,
					90 );
		case IN_FILE_PRECEDING:
			return new ProposalParameters<>(invocationContext,
					NO_COMMAND_ID,
					RUIMessages.Proposal_RenameInFilePrecending_label,
					RUIMessages.Proposal_RenameInFilePrecending_description,
					85 );
		case IN_FILE_FOLLOWING:
			return new ProposalParameters<>(invocationContext,
					NO_COMMAND_ID,
					RUIMessages.Proposal_RenameInFileFollowing_label,
					RUIMessages.Proposal_RenameInFileFollowing_description,
					84 );
		case IN_CHUNK:
			if (!withRegion) {
				throw new IllegalArgumentException();
			}
			return new ProposalParameters<>(invocationContext,
					NO_COMMAND_ID,
					RUIMessages.Proposal_RenameInChunk_label,
					RUIMessages.Proposal_RenameInChunk_description,
					89 );
		default:
			throw new IllegalArgumentException();
		}
	}
	
	private boolean checkReplFName() {
		if (this.access.getNextSegment() != null) {
			return false;
		}
		final String segmentName= this.access.getSegmentName();
		return (segmentName != null && segmentName.length() > 2
				&& segmentName.endsWith(RTerminal.S_ARROW_LEFT) );
	}
	
	private TextRegion getChunkRegion() {
		if (this.mode != IN_CHUNK) {
			throw new RuntimeException();
		}
		return nonNullAssert(this.region);
	}
	
	
	@Override
	protected void collectPositions(final IDocument document, final LinkedPositionGroup group)
			throws BadLocationException {
		final ImIdentityList<? extends RElementAccess> allAccess= ImCollections.toIdentityList(
				this.access.getAllInUnit(false) );
		final int current= allAccess.indexOf(this.access);
		if (current < 0) {
			return;
		}
		
		int idx= 0;
		{	final var nameNode= nonNullAssert(this.access.getNameNode());
			idx= addPosition(group, document, nameNode, idx);
			if (idx == 0) {
				return;
			}
		}
		if (this.mode == IN_FILE || this.mode == IN_FILE_FOLLOWING) {
			for (int i= current + 1; i < allAccess.size(); i++) {
				final var nameNode= allAccess.get(i).getNameNode();
				if (nameNode != null) {
					idx= addPosition(group, document, nameNode, idx);
				}
			}
		}
		else if (this.mode == IN_CHUNK) {
			final int regionOffset= getChunkRegion().getEndOffset();
			for (int i= current + 1; i < allAccess.size(); i++) {
				final var nameNode= allAccess.get(i).getNameNode();
				if (nameNode != null) {
					if (regionOffset > nameNode.getStartOffset()) {
						idx= addPosition(group, document, nameNode, idx);
					}
					else {
						break;
					}
				}
			}
		}
		if (this.mode == IN_FILE || this.mode == IN_FILE_PRECEDING) {
			for (int i= 0; i < current; i++) {
				final var nameNode= allAccess.get(i).getNameNode();
				if (nameNode != null) {
					idx= addPosition(group, document, nameNode, idx);
				}
			}
		}
		else if (this.mode == IN_CHUNK) {
			final int regionOffset= getChunkRegion().getStartOffset();
			for (int i= 0; i < current; i++) {
				final var nameNode= allAccess.get(i).getNameNode();
				if (nameNode != null) {
					if (regionOffset <= nameNode.getStartOffset()) {
						idx= addPosition(group, document, nameNode, idx);
					}
				}
			}
		}
	}
	
	
	protected int addPosition(final LinkedPositionGroup group, final IDocument document,
			final RAstNode nameNode, final int idx) throws BadLocationException {
		TextRegion position= RAsts.getElementNameRegion(nameNode);
		if (this.isReplFName && position != null
				&& document.get(position.getStartOffset(), position.getLength())
						.endsWith(RTerminal.S_ARROW_LEFT) ) {
			position= new BasicTextRegion(position.getStartOffset(), position.getEndOffset() - 2);
		}
		return super.addPosition(group, document, position, idx);
	}
	
}
