/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class RUIMessages extends NLS {
	
	
	public static String ChooseREnv_None_label;
	public static String ChooseREnv_WorkbenchDefault_label;
	public static String ChooseREnv_Selected_label;
	public static String ChooseREnv_Configure_label;
	public static String ChooseREnv_error_InvalidPreferences_message;
	public static String ChooseREnv_error_IncompleteSelection_message;
	public static String ChooseREnv_error_InvalidSelection_message;
	
	public static String StripComments_task_label;
	public static String CorrectIndent_task_label;
	
	public static String RProject_ConfigureTask_label;
	public static String RPkgProject_ConvertTask_error_message;
	
	public static String RProject_NeedsBuild_title;
	public static String RProject_NeedsBuild_Full_message;
	public static String RProject_NeedsBuild_Project_message;
	
	public static String Proposal_RenameInFile_label;
	public static String Proposal_RenameInFile_description;
	public static String Proposal_RenameInChunk_label;
	public static String Proposal_RenameInChunk_description;
	public static String Proposal_RenameInFilePrecending_label;
	public static String Proposal_RenameInFilePrecending_description;
	public static String Proposal_RenameInFileFollowing_label;
	public static String Proposal_RenameInFileFollowing_description;
	
	public static String Proposal_RenameInRegion_label;
	public static String Proposal_RenameInRegion_description;
	public static String Proposal_RenameInWorkspace_label;
	public static String Proposal_RenameInWorkspace_description;
	public static String Proposal_ConvertFCallToPipeForward_label;
	
	public static String Outline_HideGeneralVariables_name;
	public static String Outline_HideLocalElements_name;
	
	public static String EditorTemplates_RCodeContext_label;
	public static String EditorTemplates_RoxygenContext_label;
	
	public static String GenerateRoxygenElementComment_label;
	public static String GenerateRoxygenElementComment_error_message;
	
	public static String Templates_Variable_ElementName_description;
	public static String Templates_Variable_RoxygenParamTags_description;
	public static String Templates_Variable_RoxygenSlotTags_description;
	public static String Templates_Variable_RoxygenSigList_description;
	public static String Templates_Variable_RPkgName_description;
	
	
	static {
		NLS.initializeMessages(RUIMessages.class.getName(), RUIMessages.class);
	}
	private RUIMessages() {}
	
}
