/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;


public final class FindTask {
	
	
	public final String expression;
	
	public final long rowIdx;
	public final long columnIdx;
	public final boolean firstInRow;
	public final boolean forward;
	
	public final FindFilter filter;
	
	
	public FindTask(final String expression,
			final long rowIdx, final long columnIdx,
			final boolean firstInRow, final boolean forward,
			final FindFilter filter) {
		this.expression= expression;
		
		this.rowIdx= rowIdx;
		this.columnIdx= columnIdx;
		this.firstInRow= firstInRow;
		this.forward= forward;
		
		this.filter= filter;
	}
	
	
	@Override
	public int hashCode() {
		return this.expression.hashCode();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof FindTask)) {
			return false;
		}
		final FindTask other= (FindTask) obj;
		return (this.expression.equals(other.expression)
				&& this.firstInRow == other.firstInRow);
	}
	
}
