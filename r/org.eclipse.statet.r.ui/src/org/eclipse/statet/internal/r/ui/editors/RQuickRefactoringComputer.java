/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.ui.correction.ConvertFCallToPipeForwardAssistProposal;
import org.eclipse.statet.internal.r.ui.correction.RLinkedNamesAssistProposal;
import org.eclipse.statet.internal.r.ui.correction.RenameInRegionAssistProposal;
import org.eclipse.statet.internal.r.ui.correction.RenameInWorkspaceAssistProposal;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistComputer;
import org.eclipse.statet.r.core.refactoring.FCallToPipeForwardRefactoring;
import org.eclipse.statet.r.core.rmodel.RCompositeSourceElement;
import org.eclipse.statet.r.core.rmodel.RElementAccess;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.source.RSourceConstants;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class RQuickRefactoringComputer implements QuickAssistComputer {
	
	
	public RQuickRefactoringComputer() {
	}
	
	
	@Override
	public void computeAssistProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		if (context instanceof RAssistInvocationContext) {
			computeAssistProposals((RAssistInvocationContext)context, proposals, monitor);
		}
	}
	
	public void computeAssistProposals(final RAssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		final RAstNode node= context.getSelectionRAstNode();
		if (node == null) {
			return;
		}
		
		if (node.getNodeType() == NodeType.SYMBOL || node.getNodeType() == NodeType.STRING_CONST) {
			RAstNode candidate= node;
			SEARCH_ACCESS : while (candidate != null) {
				final List<Object> attachments= candidate.getAttachments();
				for (final Object attachment : attachments) {
					if (attachment instanceof RElementAccess) {
						RElementAccess access= (RElementAccess)attachment;
						SUB: while (access != null) {
							if (access.getSegmentName() == null) {
								break SUB;
							}
							if (access.getNameNode() == node) {
								addAccessAssistProposals(context, access, proposals);
								break SEARCH_ACCESS;
							}
							access= access.getNextSegment();
						}
					}
				}
				candidate= candidate.getRParent();
			}
		}
		else if (context.getLength() > 0 && context.getSourceUnit() != null) {
			proposals.add(new RenameInRegionAssistProposal(context));
		}
		
		addFCallProposals(context, node, proposals);
	}
	
	protected void addAccessAssistProposals(final RAssistInvocationContext context,
			final RElementAccess access,
			final AssistProposalCollector proposals) {
		final RAstNode accessNameNode= access.getNameNode();
		if (accessNameNode == null) {
			return;
		}
		final var allAccess= ImCollections.toIdentityList(access.getAllInUnit(false));
		final int current= allAccess.indexOf(access);
		if (current < 0) {
			return;
		}
		
		proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_FILE,
				context, access ));
		
		if (allAccess.size() > 1) {
			proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_FILE_PRECEDING,
					context, access ));
			proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_FILE_FOLLOWING,
					context, access ));
			
			final TextRegion rChunk= getRChunk(context, accessNameNode);
			if (rChunk != null) {
				int chunkBegin= 0;
				for (final int offset= rChunk.getStartOffset();
						chunkBegin < current; chunkBegin++) {
					final RAstNode nameNode= allAccess.get(chunkBegin).getNameNode();
					if (nameNode != null && offset <= nameNode.getStartOffset()) {
						break;
					}
				}
				int chunkEnd= current + 1;
				for (final int offset= rChunk.getEndOffset();
						chunkEnd < allAccess.size(); chunkEnd++) {
					final RAstNode nameNode= allAccess.get(chunkEnd).getNameNode();
					if (nameNode != null && offset <= nameNode.getStartOffset()) {
						break;
					}
				}
				if (chunkEnd - chunkBegin > 1) {
					proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_CHUNK,
							context, access, rChunk ));
				}
			}
		}
		
		if (context.getSourceUnit() instanceof RWorkspaceSourceUnit) {
			proposals.add(new RenameInWorkspaceAssistProposal(context, accessNameNode));
		}
	}
	
	protected @Nullable TextRegion getRChunk(final RAssistInvocationContext context,
			final RAstNode accessNameNode) {
		final var modelInfo= context.getModelInfo();
		if (modelInfo != null) {
			final var sourceElement= modelInfo.getSourceElement();
			if (sourceElement instanceof RCompositeSourceElement) {
				final var elements= ((RCompositeSourceElement)sourceElement).getCompositeElements();
				final RLangSourceElement element= LtkModelUtils.getCoveringSourceElement(
						elements, accessNameNode.getStartOffset() );
				if (element != null) {
					return element.getSourceRange();
				}
			}
		}
		return null;
	}
	
	protected void addFCallProposals(final RAssistInvocationContext context,
			final RAstNode node,
			final AssistProposalCollector proposals) {
		final RSourceUnit sourceUnit= context.getSourceUnit();
		final FCall fCall= findFCall(node);
		if (sourceUnit == null || fCall == null) {
			return;
		}
		final FCall.Args args= fCall.getArgsChild();
		if (context.getRCoreAccess().getRSourceConfig().getLangVersion()
						.compareTo(RSourceConstants.LANG_VERSION_4_1) >= 0
				&& args.getChildCount() > 0 ) {
			
			final var refactoring= new FCallToPipeForwardRefactoring(sourceUnit, fCall);
			final RefactoringStatus status= refactoring.checkInitialConditions(null);
			if (status.hasFatalError()) {
				return;
			}
			proposals.add(new ConvertFCallToPipeForwardAssistProposal(context, refactoring));
		}
	}
	
	private @Nullable FCall findFCall(final @Nullable RAstNode node) {
		if (node == null) {
			return null;
		}
		switch (node.getNodeType()) {
		case F_CALL:
			return (FCall)node;
		case SOURCELINES:
		case COMMENT:
		case ERROR:
		case ERROR_TERM:
		case DUMMY:
		case F_CALL_ARGS:
		case F_CALL_ARG:
		case BLOCK:
			return null;
		default:
			return findFCall(node.getRParent());
		}
	}
	
}
