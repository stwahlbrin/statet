/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.search;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	public static String Search_Query_label;
	public static String Search_error_RunFailed_message;
	public static String Search_Match_sing_label;
	public static String Search_Match_plural_label;
	public static String Search_Occurrence_sing_label;
	public static String Search_Occurrence_plural_label;
	public static String Search_WriteOccurrence_sing_label;
	public static String Search_WriteOccurrence_plural_label;
	
	public static String menus_Scope_Workspace_name;
	public static String menus_Scope_Workspace_mnemonic;
	public static String menus_Scope_Project_name;
	public static String menus_Scope_Project_mnemonic;
	public static String menus_Scope_File_name;
	public static String menus_Scope_File_mnemonic;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
