/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.launching;

import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.ecommons.preferences.core.Preference.StringPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;

import org.eclipse.statet.internal.r.debug.ui.RControllerCodeLaunchConnector;



public class RRunDebugPreferenceConstants {
	
	
	public static final String ROOT_QUALIFIER = "org.eclipse.statet.r.ui"; // 'ui', because at moment in 'ui' plugin //$NON-NLS-1$
	
	public static final String CAT_CODELAUNCH_CONTENTHANDLER_QUALIFIER = ROOT_QUALIFIER + "/CodeLaunchContentHandler"; //$NON-NLS-1$
	
	public static final String CAT_RCONNECTOR_QUALIFIER = ROOT_QUALIFIER + "/RConnector"; //$NON-NLS-1$
	public static final StringPref PREF_R_CONNECTOR = new StringPref(CAT_RCONNECTOR_QUALIFIER, "rconnector.id"); //$NON-NLS-1$
	
	public static final String CAT_RREMOTE_LAUNCHING_QUALIFIER = ROOT_QUALIFIER + "/r.remote.launching"; //$NON-NLS-1$
	
	public static final String CAT_RENV_QUALIFIER = ROOT_QUALIFIER + "/renv"; //$NON-NLS-1$
	public static final StringPref PREF_RENV_CHECK_UPDATE = new StringPref(CAT_RENV_QUALIFIER, "index.console_update.mode"); //$NON-NLS-1$
	
	/** Run update automatically */
	public static final String AUTO = "auto"; //$NON-NLS-1$
	/** Ask if run update */
	public static final String ASK = "ask"; //$NON-NLS-1$
	/** Do not run update */
	public static final String DISABLED = "no"; //$NON-NLS-1$
	
	
	/**
	 * Initializes the default values.
	 */
	public static void initializeDefaultValues(final IScopeContext context) {
		PreferenceUtils.setPrefValue(context, RRunDebugPreferenceConstants.PREF_R_CONNECTOR,
				RControllerCodeLaunchConnector.ID );
		
		PreferenceUtils.setPrefValue(context, PREF_RENV_CHECK_UPDATE, ASK);
	}
	
}
