/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rtool;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.ecommons.resources.core.variables.ResourceVariableResolver;

import org.eclipse.statet.internal.r.ui.rtools.Messages;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.r.ui.RUI;


public class RProjectVariableResolver extends ResourceVariableResolver {
	
	
	public static final String R_PKG_ROOT_PATH_NAME= "r_pkg_root_path"; //$NON-NLS-1$
	
	public static final String R_PKG_NAME_NAME= "r_pkg_name"; //$NON-NLS-1$
	
	
	public RProjectVariableResolver() {
	}
	
	
	@Override
	public String resolveValue(final IDynamicVariable variable, final String argument) throws CoreException {
		final RProject rProject= getRProject(variable, argument);
		if (variable.getName().equals(R_PKG_ROOT_PATH_NAME)) {
			IPath path= rProject.getPkgRootPath();
			if (path == null) {
				path= rProject.getProject().getFullPath();
			}
			return path.toString();
		}
		if (variable.getName().equals(R_PKG_NAME_NAME)) {
			final String name= rProject.getPkgName();
			if (name == null) {
				throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
						NLS.bind(Messages.Variable_error_InvalidProject_NoPkgName_message,
								variable.getName(), rProject.getProject().getName() )));
			}
			return name;
		}
		throw new IllegalArgumentException(variable.toString());
	}
	
	
	protected RProject getRProject(final IDynamicVariable variable, final String argument)
			throws CoreException {
		final IProject project= (IProject) getResource(variable, PROJECT, argument);
		if (!project.exists()) {
			throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
					NLS.bind(Messages.Variable_error_InvalidProject_NotExists_message,
							variable.getName(), project.getName() )));
		}
		final RProject rProject;
		if (!project.hasNature(RProjects.R_NATURE_ID)
				|| (rProject= RProjects.getRProject(project)) == null) {
			throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
					NLS.bind(Messages.Variable_error_InvalidProject_NotExists_message,
							variable.getName(), project.getName() )));
		}
		return rProject;
	}
	
}
