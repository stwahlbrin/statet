/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.objectbrowser;

import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.nico.ui.actions.AbstractToolHandler;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.console.core.RWorkspace;


@NonNullByDefault
class ToggleAutoRefreshHandler extends AbstractToolHandler<RProcess> implements IElementUpdater {
	
	
	private final ObjectBrowserView view;
	
	private boolean currentState;
	
	
	public ToggleAutoRefreshHandler(final ObjectBrowserView view) {
		super(RConsoleTool.TYPE, null, view, view.getSite());
		
		this.view= view;
		init();
	}
	
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		this.currentState= false;
		final RProcess tool= getActiveTool();
		if (tool != null) {
			final RWorkspace workspace= tool.getWorkspaceData();
			if (workspace != null) {
				this.currentState= workspace.isAutoRefreshEnabled();
			}
		}
		
		WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
		try {
			element.setChecked(this.currentState);
		}
		finally {
			WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
		}
	}
	
	@Override
	protected @Nullable Object execute(final RProcess tool, final ExecutionEvent event) {
		final RWorkspace workspace= tool.getWorkspaceData();
		if (workspace != null) {
			this.currentState= !this.currentState;
			workspace.setAutoRefresh(this.currentState);
		}
		return null;
	}
	
}
