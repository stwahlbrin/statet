/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import static org.eclipse.statet.jcommons.status.Status.CANCEL_STATUS;
import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.nico.ui.Messages;
import org.eclipse.statet.nico.core.runtime.ConsoleService;
import org.eclipse.statet.nico.core.runtime.IRemoteEngineController;
import org.eclipse.statet.nico.core.runtime.IToolEventHandler;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.core.util.AbstractConsoleCommandHandler;
import org.eclipse.statet.nico.ui.NicoUI;


/**
 * @see IToolEventHandler#SCHEDULE_QUIT_EVENT_ID
 */
public class QuitHandler extends AbstractConsoleCommandHandler implements IToolEventHandler {
	
	
	private static class UIRunnable implements Runnable {
		
		private ToolController controller;
		private String dialogTitle;
		private String dialogMessage;
		private String[] dialogOptions;
		private volatile int result;
		
		@Override
		public void run() {
			final IWorkbenchWindow window= UIAccess.getActiveWorkbenchWindow(true);
			final MessageDialog dialog= new MessageDialog(window.getShell(), this.dialogTitle, null,
					this.dialogMessage, MessageDialog.QUESTION, this.dialogOptions, 0 );
			this.result= dialog.open();
			
			if (this.result == 1) {
				try {
					window.run(true, true, new IRunnableWithProgress() {
						@Override
						public void run(final IProgressMonitor monitor) throws InvocationTargetException {
							final ProgressMonitor m= EStatusUtils.convert(monitor, 1);
							try {
								UIRunnable.this.controller.kill(m);
							}
							catch (final StatusException e) {
								throw new InvocationTargetException(e);
							}
						}
					});
				}
				catch (final InvocationTargetException e) {
					StatusManager.getManager().handle(new org.eclipse.core.runtime.Status(IStatus.ERROR, NicoUI.BUNDLE_ID,
									Messages.TerminatingMonitor_Force_error_message,
									e.getTargetException() ),
							StatusManager.LOG | StatusManager.SHOW);
				}
				catch (final InterruptedException e) {
				}
			}
		}
		
	}
	
	
	@Override
	@NonNullByDefault
	public Status execute(final String id, final ConsoleService service, final ToolCommandData data,
			final ProgressMonitor m) {
		if (PlatformUI.getWorkbench().isClosing()) {
			final ToolController controller= service.getController();
			if (controller != null) {
				if (service.getTool().isProvidingFeatureSet(IRemoteEngineController.FEATURE_SET_ID)) {
					try {
						((IRemoteEngineController) controller).disconnect(m);
						return CANCEL_STATUS;
					}
					catch (final StatusException e) {}
				}
				try {
					controller.kill(m);
					return CANCEL_STATUS;
				}
				catch (final StatusException e) {}
			}
			return CANCEL_STATUS;
		}
		
		final List<ToolRunnable> quitRunnables= data.getRequired("scheduledQuitRunnables", List.class);
		if (quitRunnables.isEmpty()) {
			return OK_STATUS; // run default= schedule quit
		}
		
		final UIRunnable runner= new UIRunnable();
		runner.controller= service.getController();
		final ToolProcess process= runner.controller.getTool();
		runner.dialogTitle= NLS.bind(Messages.TerminatingMonitor_title, process.getLabel(Tool.DEFAULT_LABEL));
		runner.dialogMessage= NLS.bind(Messages.TerminatingMonitor_message, process.getLabel(Tool.LONG_LABEL));
		runner.dialogOptions= new String[] {
				Messages.TerminatingMonitor_WaitButton_label,
				Messages.TerminatingMonitor_ForceButton_label,
				Messages.TerminatingMonitor_CancelButton_label
		};
		
		UIAccess.getDisplay().syncExec(runner);
		if (runner.result == 2) {
			runner.controller.cancelQuit();
		}
		return CANCEL_STATUS; // do nothing
	}
	
}
