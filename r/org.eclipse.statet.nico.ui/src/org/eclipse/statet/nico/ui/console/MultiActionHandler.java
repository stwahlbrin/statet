/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.console;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.texteditor.IUpdate;

import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Manages activation of actions for different controls
 * (based on FocusIn/FocusOut event) in a view.
 * 
 * For compatibility with old action framework.
 */
public class MultiActionHandler implements Listener, ISelectionChangedListener {
	
	
	private class ActionWrapper extends Action {
		
		private final Map<Widget, IAction> widgetActionMap= new HashMap<>();
		
		ActionWrapper() {
		}
		
		public void update() {
			boolean enabled= false;
			if (MultiActionHandler.this.activeWidget != null) {
				final IAction action= this.widgetActionMap.get(MultiActionHandler.this.activeWidget);
				if (action != null) {
					if (action instanceof IUpdate) {
						((IUpdate) action).update();
					}
					enabled= action.isEnabled();
				}
			}
			setEnabled(enabled);
		}
		
		@Override
		public void runWithEvent(final Event event) {
			if (MultiActionHandler.this.activeWidget != null) {
				final IAction action= this.widgetActionMap.get(MultiActionHandler.this.activeWidget);
				if (action != null) {
					action.runWithEvent(event);
				}
			}
		}
	}
	
	
	private @Nullable Widget activeWidget;
	private final List<Widget> knownWidgets= new ArrayList<>();
	private final Map<String, ActionWrapper> actions= new HashMap<>();
	
	
	MultiActionHandler() {
	}
	
	
	private ActionWrapper getActionWrapper(final String id) {
		ActionWrapper wrapper= this.actions.get(id);
		if (wrapper == null) {
			wrapper= new ActionWrapper();
			this.actions.put(id, wrapper);
		}
		return wrapper;
	}
	
	public void addGlobalAction(final Widget widget, final String globalId, final IAction action) {
		if (action.getId() == null) {
			action.setId(globalId);
		}
		final ActionWrapper wrapper= getActionWrapper(globalId);
		wrapper.widgetActionMap.put(widget, action);
		
		addWidget(widget);
	}
	
	private void addWidget(final Widget widget) {
		if (!this.knownWidgets.contains(widget)) {
			widget.addListener(SWT.FocusIn, this);
			widget.addListener(SWT.FocusOut, this);
			this.knownWidgets.add(widget);
		}
	}
	
	public void registerActions(final IActionBars bars) {
		for (final String id : this.actions.keySet()) {
			bars.setGlobalActionHandler(id, this.actions.get(id));
		}
	}
	
	@Override
	public void handleEvent(final Event event) {
		switch (event.type) {
		case SWT.FocusIn:
			this.activeWidget= event.widget;
			updateEnabledState();
			break;
		case SWT.FocusOut:
			this.activeWidget= null;
			updateEnabledState();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void selectionChanged(final SelectionChangedEvent event) {
		updateEnabledState(); 
	}
	
	void updateEnabledState() {
		for (final ActionWrapper wrapper : this.actions.values()) {
			wrapper.update();
		}
	}
	public void dispose() {
		this.activeWidget= null;
		this.actions.clear();
		this.knownWidgets.clear();
	}
	
}
