/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.internal.ui.launchConfigurations.LaunchConfigurationsMessages;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.ecommons.debug.ui.util.CheckedCommonTab;

import org.eclipse.statet.nico.core.runtime.LogRuntimeProcessFactory;


/**
 * Adapts CommonTab for nico.
 */
public class CommonTabForConsole extends CheckedCommonTab {
	
	
	public CommonTabForConsole() {
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		super.createControl(parent);
		final Button button = searchButton((Composite) getControl());
		if (button != null) {
			button.setText("Allocate additional Error Log Consoles");
		}
	}
	
	private static Button searchButton(final Composite composite) {
		final Control[] children = composite.getChildren();
		for (final Control control : children) {
			if (control instanceof Button) {
				if (((Button) control).getText().equals(LaunchConfigurationsMessages.CommonTab_5)) {
					return (Button) control;
				}
			}
			else if (control instanceof Composite) {
				final Button button = searchButton((Composite) control);
				if (button != null) {
					return button;
				}
			}
		}
		return null;
	}
	
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy config) {
		super.setDefaults(config);
		config.setAttribute(IDebugUIConstants.ATTR_CAPTURE_IN_CONSOLE, false);
		config.setAttribute(DebugPlugin.ATTR_CAPTURE_OUTPUT, (String) null);
		config.setAttribute(DebugPlugin.ATTR_PROCESS_FACTORY_ID, LogRuntimeProcessFactory.FACTORY_ID);
	}
	
	@Override
	public void performApply(final ILaunchConfigurationWorkingCopy config) {
		super.performApply(config);
		config.setAttribute(DebugPlugin.ATTR_CAPTURE_OUTPUT, (String) null);
		config.setAttribute(DebugPlugin.ATTR_PROCESS_FACTORY_ID, LogRuntimeProcessFactory.FACTORY_ID);
	}
	
}
