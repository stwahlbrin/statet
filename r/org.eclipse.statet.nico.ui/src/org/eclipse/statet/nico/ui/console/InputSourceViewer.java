/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.console;

import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.DNDUtils;

import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.swt.EnhStyledText;


@NonNullByDefault
public class InputSourceViewer extends SourceViewer {
	
	
	InputSourceViewer(final Composite parent) {
		super(parent, null, null, false, SWT.SINGLE);
		
		initializeDragAndDrop();
		initTabControl();
	}
	
	
	@Override
	protected StyledText createTextWidget(final Composite parent, final int styles) {
		return EnhStyledText.forSourceEditor(parent, styles);
	}
	
	protected void initializeDragAndDrop() {
		DNDUtils.addDropSupport(getTextWidget(), new DNDUtils.SimpleTextDropAdapter() {
			@Override
			protected StyledText getTextWidget() {
				return InputSourceViewer.this.getTextWidget();
			}
		}, new @NonNull Transfer[] { TextTransfer.getInstance() });
	}
	
	private void initTabControl() {
		// disable traverse on TAB key event, to enable TAB char insertion.
		getTextWidget().addListener(SWT.Traverse, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				if (event.stateMask == SWT.NONE && event.character == SWT.TAB) {
					event.doit= false;
				}
			}
		});
	}
	
	protected void clear() {
		if (this.fContentAssistant instanceof ContentAssist) {
			final ContentAssist assist= (ContentAssist) this.fContentAssistant;
			assist.hidePopups();
		}
		getDocument().set(""); //$NON-NLS-1$
	}
	
}
