/*=============================================================================#
 # Copyright (c) 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.ui.IWorkbenchWindow;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener.ActiveToolEvent;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;

import org.eclipse.statet.nico.ui.NicoUI;


@NonNullByDefault
public abstract class AbstractToolScopeHandler<TTool extends Tool> extends AbstractScopeHandler {
	
	
	private final ActiveToolListener toolListener= this::onToolChanged;
	
	private @Nullable ToolProvider toolProvider;
	
	private @Nullable TTool currentTool;
	
	
	public AbstractToolScopeHandler(final Object scope, final @Nullable String commandId) {
		super();
		init(scope, commandId);
	}
	
	@Override
	protected void init(final Object scope, final @Nullable String commandId) {
		super.init(scope, commandId);
		
		ToolProvider toolProvider= null;
		if (scope instanceof ToolProvider) {
			toolProvider= (ToolProvider)scope;
		}
		else if (scope instanceof IWorkbenchWindow) {
			final var workbenchPage= ((IWorkbenchWindow)scope).getActivePage();
			if (workbenchPage != null) {
				toolProvider= NicoUI.getToolRegistry().getToolProvider(workbenchPage);
			}
		}
		
		if (toolProvider != null) {
			this.toolProvider= toolProvider;
			toolProvider.addToolListener(this.toolListener);
		}
	}
	
	@Override
	public void dispose() {
		final var toolProvider= this.toolProvider;
		if (toolProvider != null) {
			this.toolProvider= null;
			toolProvider.removeToolListener(this.toolListener);
		}
		
		super.dispose();
	}
	
	
	protected @Nullable Tool getTool(final @Nullable IEvaluationContext evalContext) {
		final Object scope= getScope();
		if (scope instanceof ToolProvider) {
			return ((ToolProvider)scope).getTool();
		}
		
		return null;
	}
	
	private void onToolChanged(final ActiveToolEvent event) {
		if (this.toolProvider != null) {
			setEnabled(null);
		}
	}
	
	protected @Nullable TTool getCurrentTool(final @Nullable IEvaluationContext evalContext) {
		final Tool tool= getTool(evalContext);
		@SuppressWarnings("unchecked")
		final var currentTool= (tool == this.currentTool || (tool != null && isSupported(tool))) ?
				(TTool)tool : null;
		this.currentTool= currentTool;
		return currentTool;
	}
	
	protected boolean isSupported(final Tool tool) {
		return true;
	}
	
	protected boolean evaluateIsEnabled(final TTool tool) {
		return true;
	}
	
	
	@Override
	public void setEnabled(final @Nullable IEvaluationContext context) {
		final var currentTool= getCurrentTool(context);
		setBaseEnabled(currentTool != null && evaluateIsEnabled(currentTool));
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event, final IEvaluationContext evalContext)
			throws ExecutionException {
		final var currentTool= getCurrentTool(evalContext);
		if (currentTool != null && evaluateIsEnabled(currentTool)) {
			return execute(event, currentTool, evalContext);
		}
		return null;
	}
	
	public abstract @Nullable Object execute(final ExecutionEvent event,
			final TTool tool, final IEvaluationContext evalContext)
			throws ExecutionException;
	
	
}
