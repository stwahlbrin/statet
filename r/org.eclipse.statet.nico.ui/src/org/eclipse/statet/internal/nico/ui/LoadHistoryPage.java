/*=============================================================================#
 # Copyright (c) 2006, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.ui.workbench.ResourceInputComposite;

import org.eclipse.statet.internal.nico.ui.actions.AbstractHistoryPage;
import org.eclipse.statet.nico.core.runtime.ToolProcess;


public class LoadHistoryPage extends AbstractHistoryPage {
	
	
	public LoadHistoryPage(final ToolProcess tool) {
		super("LoadHistoryPage", Messages.LoadHistoryPage_title, tool); //$NON-NLS-1$
		setDescription(NLS.bind(Messages.LoadHistoryPage_description, fTool.getLabel(Tool.DEFAULT_LABEL)));
	}
	
	
	@Override
	protected ResourceInputComposite createResourceInputComposite(final Composite composite) {
		return new ResourceInputComposite(composite, 
				ResourceInputComposite.STYLE_COMBO,
				ResourceInputComposite.MODE_FILE | ResourceInputComposite.MODE_OPEN, 
				Messages.LoadSaveHistoryPage_File_label);
	}
	
}
