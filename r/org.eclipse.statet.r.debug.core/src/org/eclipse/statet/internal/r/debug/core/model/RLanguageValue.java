/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.data.RValueFormatter;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.FQRObjectRef;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore.Fragment;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class RLanguageValue extends RElementVariableValue<CombinedRElement> {
	
	
	private static final RObjectAdapter<RLanguage> ADAPTER= new RObjectAdapter<>(RObject.TYPE_LANGUAGE);
	
	static void appendValue(final RValueFormatter valueFormatter, final RLanguage rObject) {
		if (rObject.getLanguageType() == RLanguage.NAME) {
			valueFormatter.appendName(rObject.getSource(), false);
		}
		else {
			valueFormatter.appendSourceLine(rObject.getSource(), 200);
		}
	}
	
	
	private @Nullable LazyRStore<RLanguage> detailObjectStore;
	
	
	public RLanguageValue(final BasicRElementVariable variable) {
		super(variable);
	}
	
	
	public final RLanguage getRObject() {
		return (RLanguage) this.element;
	}
	
	
	@Override
	public String getValueString() throws DebugException {
		final LazyRStore.Fragment<RLanguage> fragment;
		synchronized (this) {
			fragment= getDetailObjectFragment();
			if (fragment == null || fragment.getRObject() == null) {
//				throw newRequestLoadDataFailed();
				return ""; //$NON-NLS-1$
			}
		}
		
		final RLanguage rObject= fragment.getRObject();
		if (rObject.getSource() != null) {
			final RValueFormatter valueFormatter= getDebugTarget().getValueFormatter();
			synchronized (valueFormatter) {
				valueFormatter.clear();
				appendValue(valueFormatter, rObject);
				return valueFormatter.getString();
			}
		}
		return ""; //$NON-NLS-1$
	}
	
	@Override
	public String getDetailString() {
		final LazyRStore.Fragment<RLanguage> fragment;
		synchronized (this) {
			fragment= getDetailObjectFragment();
			if (fragment == null || fragment.getRObject() == null) {
				return "<error>"; //$NON-NLS-1$
			}
		}
		
		final RLanguage rObject= fragment.getRObject();
		if (rObject.getSource() != null) {
			return rObject.getSource();
		}
		return ""; //$NON-NLS-1$
	}
	
	
	void setDataObject(final RLanguage rObject) {
		ensureDetailObjectStore().setFragment(0, 0, rObject);
	}
	
	private LazyRStore<RLanguage> ensureDetailObjectStore() {
		if (this.detailObjectStore == null) {
			this.detailObjectStore= new LazyRStore<>(1, 1, 1,
					new RDataLoader<RLanguage>() {
				@Override
				protected RLanguage doLoad(final FQRObjectRef ref,
						final Fragment<RLanguage> fragment,
						final RToolService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
					return ADAPTER.loadObject(ref, getRObject(), fragment, r, m);
				}
			});
		}
		return this.detailObjectStore;
	}
	
	private @Nullable Fragment<RLanguage> getDetailObjectFragment() {
		return ensureDetailObjectStore().getFragment(0, 0, 0, null);
	}
	
}
