/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.core.model.VariablePartition;
import org.eclipse.statet.ecommons.debug.core.model.VariablePartitionFactory;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.debug.core.RDebugTarget;
import org.eclipse.statet.r.debug.core.RValue;
import org.eclipse.statet.r.debug.core.RVariable;
import org.eclipse.statet.rj.data.RObject;


@NonNullByDefault
public abstract class RElementValue<TRElement extends CombinedRElement> extends RDebugElement
		implements RValue {
	
	
	protected static final @NonNull RVariable[] NO_VARIABLES= new @NonNull RVariable[0];
	
	
	protected static class RVariablePartition extends VariablePartition<RIndexElementValue>
			implements RVariable, RValue {
		
		
		public RVariablePartition(final RIndexElementValue value,
				final VariablePartitionFactory<RIndexElementValue>.PartitionHandle partition) {
			super(value, partition);
		}
		
		
		@Override
		public RDebugTarget getDebugTarget() {
			return (RDebugTarget)super.getDebugTarget();
		}
		
		@Override
		public @Nullable RVariable getParent() {
			return null;
		}
		
		@Override
		protected int getNameIndexBase() {
			return 1;
		}
		
		@Override
		public RValue getValue() {
			return this;
		}
		
		@Override
		public RVariable getAssignedVariable() {
			return this;
		}
		
		@Override
		public String getDetailString() {
			return ""; //$NON-NLS-1$
		}
		
	}
	
	protected final static VariablePartitionFactory<RIndexElementValue> PARTITION_FACTORY= new VariablePartitionFactory<>() {
		
		@Override
		protected RVariable createPartition(final RIndexElementValue value,
				final VariablePartitionFactory<RIndexElementValue>.PartitionHandle partition) {
			return new RVariablePartition(value, partition);
		}
		
	};
	
	
	protected final TRElement element;
	
	protected final int stamp;
	
	
	public RElementValue(final RDebugTargetImpl debugTarget,
			final TRElement element, final int stamp) {
		super(debugTarget);
		this.element=  element;
		this.stamp= stamp;
	}
	
	
	public final TRElement getElement() {
		return this.element;
	}
	
	
	@Override
	public String getValueString() throws DebugException {
		switch (this.element.getRObjectType()) {
		case RObject.TYPE_NULL:
			return "NULL"; //$NON-NLS-1$
//		case RObject.TYPE_MISSING:
//			return "<missing>";
		}
		return ""; //$NON-NLS-1$
	}
	
	@Override
	public String getReferenceTypeName() throws DebugException {
		return this.element.getRClassName();
	}
	
	@Override
	public String getDetailString() {
		switch (this.element.getRObjectType()) {
		case RObject.TYPE_NULL:
			return "NULL"; //$NON-NLS-1$
//		case RObject.TYPE_MISSING:
//			return "<missing>";
		}
		return ""; //$NON-NLS-1$
	}
	
	
	@Override
	public boolean isAllocated() throws DebugException {
		return true;
	}
	
	@Override
	public boolean hasVariables() throws DebugException {
		return false;
	}
	
	@Override
	public @NonNull IVariable[] getVariables() throws DebugException {
		return NO_VARIABLES;
	}
	
}
