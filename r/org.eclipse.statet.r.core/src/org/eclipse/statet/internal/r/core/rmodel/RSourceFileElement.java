/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceContainerElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.r.core.rmodel.RElement;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RFrame;
import org.eclipse.statet.r.core.rmodel.RLangSourceElement;
import org.eclipse.statet.r.core.rmodel.RModel;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;


@NonNullByDefault
abstract class RSourceFileElement implements RLangSourceElement,
		SourceContainerElement<RLangSourceElement> {
	
	
	private final RSourceUnit sourceUnit;
	
	private final SourceModelStamp stamp;
	
	protected final BuildSourceFrame envir;
	
	
	public RSourceFileElement(final RSourceUnit sourceUnit,
			final SourceModelStamp stamp,
			final BuildSourceFrame envir) {
		this.sourceUnit= sourceUnit;
		this.stamp= stamp;
		this.envir= envir;
	}
	
	
	@Override
	public final String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public int getElementType() {
		return LtkModelElement.C12_SOURCE_FILE;
	}
	
	@Override
	public RElementName getElementName() {
		final ElementName elementName= this.sourceUnit.getElementName();
		if (elementName instanceof RElementName) {
			return (RElementName) elementName;
		}
		return RElementName.create(RElementName.RESOURCE, elementName.getSegmentName());
	}
	
	@Override
	public String getId() {
		return this.sourceUnit.getId();
	}
	
	@Override
	public boolean exists() {
		final SourceUnitModelInfo modelInfo= getSourceUnit().getModelInfo(RModel.R_TYPE_ID, 0, null);
		return (modelInfo != null && modelInfo.getSourceElement() == this);
	}
	
	@Override
	public boolean isReadOnly() {
		return this.sourceUnit.isReadOnly();
	}
	
	
	@Override
	public @Nullable RElement<?> getModelParent() {
		return null;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return false;
	}
	
	@Override
	public List<? extends RLangSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangSourceElement> filter) {
		return ImCollections.emptyList();
	}
	
	@Override
	public @Nullable SourceStructElement<?, ?> getSourceParent() {
		return null;
	}
	
	@Override
	public RSourceUnit getSourceUnit() {
		return this.sourceUnit;
	}
	
	@Override
	public SourceModelStamp getStamp() {
		return this.stamp;
	}
	
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		return null;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RFrame.class) {
			return (T) this.envir;
		}
		return null;
	}
	
	@Override
	public int hashCode() {
		return this.sourceUnit.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof SourceStructElement) {
			final SourceStructElement<?, ?> other= (SourceStructElement<?, ?>)obj;
			return ((other.getElementType() & LtkModelElement.MASK_C12) == LtkModelElement.C12_SOURCE_FILE)
					&& this.sourceUnit.equals(other.getSourceUnit());
		}
		return false;
	}
	
}
