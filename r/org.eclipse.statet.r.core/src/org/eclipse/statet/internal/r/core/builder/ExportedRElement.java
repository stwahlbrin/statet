/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.Serializable;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.core.rmodel.RLangElement;
import org.eclipse.statet.r.core.rmodel.RModel;


@NonNullByDefault
public class ExportedRElement implements RLangElement<RLangElement<?>>, Serializable {
	
	
	private static final long serialVersionUID= -493469386405499748L;
	
	
	private RLangElement<?> parent;
	private int elementType;
	private RElementName elementName;
	private String elementId;
	
	private int sourceOffset;
	private int sourceLength;
	private int nameOffset;
	private int nameLength;
	
	
	public ExportedRElement(final RLangElement<?> parent, final RLangElement<?> sourceElement) {
		this.parent= parent;
		this.elementType= sourceElement.getElementType();
		this.elementName= RElementName.cloneName(sourceElement.getElementName(), false);
		this.elementId= sourceElement.getId();
		
		{	final TextRegion sourceRange= sourceElement.getSourceRange();
			if (sourceRange != null) {
				this.sourceOffset= sourceRange.getStartOffset();
				this.sourceLength= sourceRange.getLength();
			}
			else {
				this.sourceOffset= -1;
			}
		}
		{
			final TextRegion sourceRange= sourceElement.getNameSourceRange();
			if (sourceRange != null) {
				this.nameOffset= sourceRange.getStartOffset();
				this.nameLength= sourceRange.getLength();
			}
			else {
				this.nameOffset= -1;
			}
		}
	}
	
	public ExportedRElement() {
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public String getId() {
		return this.elementId;
	}
	
	@Override
	public int getElementType() {
		return this.elementType;
	}
	
	@Override
	public RElementName getElementName() {
		return this.elementName;
	}
	
	@Override
	public boolean exists() {
		return true;
	}
	
	@Override
	public boolean isReadOnly() {
		return false;
	}
	
	
	@Override
	public RLangElement<?> getModelParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangElement<?>> filter) {
		return false;
	}
	
	@Override
	public List<? extends RLangElement<?>> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangElement<?>> filter) {
		return ImCollections.emptyList();
	}
	
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.parent.getSourceUnit();
	}
	
	@Override
	public @Nullable TextRegion getSourceRange() {
		if (this.sourceOffset >= 0) {
			return new BasicTextRegion(this.sourceOffset, this.sourceOffset + this.sourceLength);
		}
		return null;
	}
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		if (this.nameOffset >= 0) {
			return new BasicTextRegion(this.nameOffset, this.nameOffset + this.nameLength);
		}
		return null;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
}
