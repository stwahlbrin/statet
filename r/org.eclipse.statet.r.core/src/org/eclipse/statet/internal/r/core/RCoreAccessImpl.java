/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesManageListener;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
final class RCoreAccessImpl implements RCoreAccess {
	
	
	private boolean isDisposed;
	
	private final PreferenceAccess prefs;
	
	private final RSourceConfig rSourceConfig;
	
	private volatile @Nullable RCodeStyleSettings codeStyle;
	private @Nullable PreferencesManageListener codeStyleListener;
	
	private final REnv rEnv;
	
	
	RCoreAccessImpl(final PreferenceAccess prefs, final REnv rEnv,
			final RSourceConfig rSourceConfig) {
		this.prefs= prefs;
		this.rEnv= rEnv;
		this.rSourceConfig= rSourceConfig;
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.prefs;
	}
	
	@Override
	public REnv getREnv() {
		return this.rEnv;
	}
	
	@Override
	public RSourceConfig getRSourceConfig() {
		return this.rSourceConfig;
	}
	
	@Override
	public RCodeStyleSettings getRCodeStyle() {
		RCodeStyleSettings codeStyle= this.codeStyle;
		if (codeStyle == null) {
			synchronized (this) {
				codeStyle= this.codeStyle;
				if (codeStyle == null) {
					codeStyle= new RCodeStyleSettings(1);
					if (!this.isDisposed) {
						this.codeStyleListener= new PreferencesManageListener(codeStyle,
								this.prefs, RCodeStyleSettings.ALL_GROUP_IDS );
					}
					codeStyle.load(this.prefs);
					codeStyle.resetDirty();
					this.codeStyle= codeStyle;
				}
			}
		}
		return codeStyle;
	}
	
	public synchronized void dispose() {
		this.isDisposed= true;
		
		if (this.codeStyleListener != null) {
			this.codeStyleListener.dispose();
			this.codeStyleListener= null;
		}
	}
	
}
