/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementDelta;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;


/**
 * Currently not really a delta
 */
@NonNullByDefault
public class ModelDelta implements LtkModelElementDelta {
	
	
	private final int level;
	private final LtkModelElement element;
	private final @Nullable RSourceUnitModelInfo oldInfo;
	private final @Nullable AstInfo oldAst;
	private final @Nullable RSourceUnitModelInfo newInfo;
	private final @Nullable AstInfo newAst;
	
	
	public ModelDelta(final LtkModelElement element,
			final @Nullable RSourceUnitModelInfo oldInfo, final @Nullable RSourceUnitModelInfo newInfo) {
		this.level= ModelManager.MODEL_FILE;
		this.element= element;
		this.oldInfo= oldInfo;
		this.oldAst= (oldInfo != null) ? oldInfo.getAst() : null;
		this.newInfo= newInfo;
		this.newAst= (newInfo != null) ? newInfo.getAst() : null;
	}
	
	
	@Override
	public LtkModelElement getModelElement() {
		return this.element;
	}
	
	@Override
	public @Nullable AstInfo getOldAst() {
		return this.oldAst;
	}
	
	@Override
	public @Nullable AstInfo getNewAst() {
		return this.newAst;
	}
	
}
