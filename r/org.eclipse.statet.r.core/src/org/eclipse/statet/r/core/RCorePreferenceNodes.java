/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;

import org.eclipse.statet.r.core.renv.IREnvManager;


/**
 * Preference Nodes for the preferences of 'StatET R Core' plug-in
 */
@NonNullByDefault
public class RCorePreferenceNodes {
	
	
	public static final String CAT_R_CODESTYLE_QUALIFIER= RCore.BUNDLE_ID + "/codestyle/r"; //$NON-NLS-1$
	
	public static final BooleanPref PREF_RENV_NETWORK_USE_ECLIPSE= new BooleanPref(IREnvManager.PREF_QUALIFIER, "network.use_eclipse"); //$NON-NLS-1$
	
}
