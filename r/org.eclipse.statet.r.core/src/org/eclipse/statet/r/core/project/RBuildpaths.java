/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.project;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.core.RProjectNature;
import org.eclipse.statet.ltk.buildpath.core.BuildpathAttribute;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElementType;
import org.eclipse.statet.r.core.rmodel.RModel;


@NonNullByDefault
public class RBuildpaths {
	
	
	public static final String R_TYPE_ID= RModel.R_TYPE_ID;
	
	
	public static final String PKG_DESCRIPTION_FILE_NAME= "DESCRIPTION"; //$NON-NLS-1$
	public static final IPath PKG_DESCRIPTION_FILE_PATH= new Path(PKG_DESCRIPTION_FILE_NAME);
	public static final String PKG_NAMESPACE_FILE_NAME= "NAMESPACE"; //$NON-NLS-1$
	public static final IPath PKG_NAMESPACE_FILE_PATH= new Path(PKG_NAMESPACE_FILE_NAME);
	public static final String PKG_R_FOLDER_NAME= "R"; //$NON-NLS-1$
	public static final IPath PKG_R_FOLDER_PATH= new Path(PKG_R_FOLDER_NAME);
	public static final String PKG_DATA_FOLDER_NAME= "data"; //$NON-NLS-1$
	public static final IPath PKG_DATA_FOLDER_PATH= new Path(PKG_DATA_FOLDER_NAME);
	public static final String PKG_DEMO_FOLDER_NAME= "demo"; //$NON-NLS-1$
	public static final IPath PKG_DEMO_FOLDER_PATH= new Path(PKG_DEMO_FOLDER_NAME);
	public static final String PKG_MAN_FOLDER_NAME= "man"; //$NON-NLS-1$
	public static final IPath PKG_MAN_FOLDER_PATH= new Path(PKG_MAN_FOLDER_NAME);
	public static final String PKG_VIGNETTES_FOLDER_NAME= "vignettes"; //$NON-NLS-1$
	public static final IPath PKG_VIGNETTES_FOLDER_PATH= new Path(PKG_VIGNETTES_FOLDER_NAME);
	public static final String PKG_SRC_FOLDER_NAME= "src"; //$NON-NLS-1$
	public static final IPath PKG_SRC_FOLDER_PATH= new Path(PKG_SRC_FOLDER_NAME);
	public static final String PKG_TESTS_FOLDER_NAME= "tests"; //$NON-NLS-1$
	public static final IPath PKG_TESTS_FOLDER_PATH= new Path(PKG_TESTS_FOLDER_NAME);
	public static final String PKG_PO_FOLDER_NAME= "po"; //$NON-NLS-1$
	public static final IPath PKG_PO_FOLDER_PATH= new Path(PKG_PO_FOLDER_NAME);
	public static final String PKG_EXEC_FOLDER_NAME= "exec"; //$NON-NLS-1$
	public static final IPath PKG_EXEC_FOLDER_PATH= new Path(PKG_EXEC_FOLDER_NAME);
	public static final String PKG_INST_FOLDER_NAME= "inst"; //$NON-NLS-1$
	public static final IPath PKG_INST_FOLDER_PATH= new Path(PKG_INST_FOLDER_NAME);
	public static final String PKG_RCHECK_FOLDER_NAME= ".Rcheck"; //$NON-NLS-1$
	public static final IPath PKG_RCHECK_FOLDER_PATH= new Path(PKG_RCHECK_FOLDER_NAME);
	
	
	public static final BuildpathElementType R_SOURCE_TYPE= new BuildpathElementType(R_TYPE_ID,
			BuildpathElement.SOURCE, ImCollections.newList(
					BuildpathAttribute.FILTER_INCLUSIONS, BuildpathAttribute.FILTER_EXCLUSIONS ));
	
	
	public static void set(final IProject project, final ImList<BuildpathElement> rawBuildpath) {
		final RProjectNature rProject= RProjectNature.getRProject(project);
		if (rProject != null) {
			rProject.saveBuildpath(rawBuildpath);
		}
	}
	
}
