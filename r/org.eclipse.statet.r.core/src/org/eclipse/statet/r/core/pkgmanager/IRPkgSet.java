/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.internal.r.core.pkgmanager.FullRPkgSet;
import org.eclipse.statet.rj.renv.core.RPkgCompilation;
import org.eclipse.statet.rj.renv.runtime.RPkgManagerDataset;


public interface IRPkgSet extends RPkgManagerDataset {
	
	
	interface Ext extends IRPkgSet {
		
		ImList<String> DEFAULT_PRIORITIES= ImCollections.newList(
				"base", "recommended", "other"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		
		
		List<String> getNames();
		
		@Override
		RPkgCompilation<? extends IRPkgInfoAndData> getInstalled();
		
		List<String> getPriorities();
		
		RPkgCompilation<? extends IRPkgData> getAvailable();
		
		IRPkgData getReverse(String name);
		
	}
	
	IRPkgSet.Ext DUMMY= FullRPkgSet.DUMMY;
	
	
	@Override
	RPkgCompilation<? extends IRPkgInfo> getInstalled();
	
}
