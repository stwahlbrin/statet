/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.project;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.r.core.rmodel.RModel;


@NonNullByDefault
public final class RIssues {
	
	
	public static final String TASK_MARKER_TYPE= "org.eclipse.statet.r.resourceMarkers.Tasks"; //$NON-NLS-1$
	
	public static final IssueTypeSet.TaskCategory TASK_CATEGORY= new IssueTypeSet.TaskCategory(
			TASK_MARKER_TYPE, null );
	
	
	public static final String BUILDPATH_PROBLEM_MARKER_TYPE= "org.eclipse.statet.r.resourceMarkers.BuildpathProblem"; //$NON-NLS-1$
	
	public static final String R_MODEL_PROBLEM_MARKER_TYPE= "org.eclipse.statet.r.resourceMarkers.RModelProblem"; //$NON-NLS-1$
	
	public static final IssueTypeSet.ProblemTypes R_MODEL_PROBLEM_MARKER_TYPES= new IssueTypeSet.ProblemTypes(
			R_MODEL_PROBLEM_MARKER_TYPE );
	
	public static final IssueTypeSet.ProblemCategory R_MODEL_PROBLEM_CATEGORY= new IssueTypeSet.ProblemCategory(
			RModel.R_TYPE_ID,
			R_MODEL_PROBLEM_MARKER_TYPES, null );
	
	
	private RIssues() {
	}
	
}
