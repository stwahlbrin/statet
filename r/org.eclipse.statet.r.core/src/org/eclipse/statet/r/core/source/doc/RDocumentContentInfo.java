/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.doc;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.BasicDocContentSections;


@NonNullByDefault
public class RDocumentContentInfo extends BasicDocContentSections {
	
	
	public static final String R=                           RDocumentConstants.R_PARTITIONING;
	
	
	public static final RDocumentContentInfo INSTANCE= new RDocumentContentInfo();
	
	
	public RDocumentContentInfo() {
		super(RDocumentConstants.R_PARTITIONING, R);
	}
	
	
	@Override
	public String getTypeByPartition(final String contentType) {
		return R;
	}
	
}
