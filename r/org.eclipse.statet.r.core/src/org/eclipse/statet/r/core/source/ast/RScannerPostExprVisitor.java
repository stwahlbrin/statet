/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.CTX12;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.SUBSEQUENT;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE12;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_PIPE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RSourceConstants;


/**
 * update offsets in expressions
 * not for control statements or function def/calls
 */
@NonNullByDefault
/* package */ class RScannerPostExprVisitor extends RAstVisitor {
	
	static final int SYNTAXERROR_MASK= (ERROR | ERROR_IN_CHILD);
	
	
	private boolean syntaxError;
	
	
	public int check(final RAstNode node) {
		this.syntaxError= false;
		try {
			node.acceptInR(this);
		}
		catch (final InvocationTargetException e) {
			// not used
		}
		if (this.syntaxError) {
			return ERROR_IN_CHILD;
		}
		return 0;
	}
	
	public int checkTerminal(final RAstNode node) {
		if ((node.getStatusCode() & SYNTAXERROR_MASK) != 0) {
			return ERROR_IN_CHILD;
		}
		return 0;
	}
	
	private void doAcceptIn(final RAstNode child) throws InvocationTargetException {
		final boolean savedSyntaxError= this.syntaxError;
		this.syntaxError= false;
		child.acceptInR(this);
		if (this.syntaxError) {
			child.rParent.status |= ERROR_IN_CHILD;
		}
		this.syntaxError |= savedSyntaxError;
	}
	
	private void doAccecptInChildren(final RAstNode node) throws InvocationTargetException {
		final boolean savedSyntaxError= this.syntaxError;
		this.syntaxError= false;
		node.acceptInRChildren(this);
		if (this.syntaxError) {
			node.status |= ERROR_IN_CHILD;
		}
		this.syntaxError |= savedSyntaxError;
	}
	
	private void markSubsequentIfStatus12(final RAstNode node, final int status) {
		if ((node.getStatusCode() & (TYPE12 | CTX12)) == status) {
			node.status|= SUBSEQUENT;
		}
	}
	
	
	@Override
	public void visit(final SourceComponent node) {
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Block node) {
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Group node) {
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final CIfElse node) throws InvocationTargetException {
		if (node.withElse) {
			doAcceptIn(node.elseExpr.node);
		}
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final CForLoop node) throws InvocationTargetException {
		doAcceptIn(node.loopExpr.node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final CRepeatLoop node) throws InvocationTargetException {
		doAcceptIn(node.loopExpr.node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final CWhileLoop node) throws InvocationTargetException {
		doAcceptIn(node.loopExpr.node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final FCall node) throws InvocationTargetException {
		doAcceptIn(node.refExpr.node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final FCall.Args node) {
//		throw new IllegalStateException();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final FCall.Arg node) {
//		throw new IllegalStateException();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final FDef node) throws InvocationTargetException {
		doAcceptIn(node.expr.node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final FDef.Args node) {
//		throw new IllegalStateException();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final FDef.Arg node) {
//		throw new IllegalStateException();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Assignment node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Pipe node) throws InvocationTargetException {
		final RAstNode target= node.getTargetChild();
		if (target.getNodeType() != NodeType.F_CALL
				&& (target.getStatusCode() & (TYPE12 | CTX12)) != (TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | CTX12_PIPE) ) {
			target.status= TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE;
		}
		if ((node.getStatusCode() & (TYPE12 | CTX12)) == (RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE)) {
			markSubsequentIfStatus12(node.getSourceChild(), TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING | CTX12_PIPE);
			markSubsequentIfStatus12(target, TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | CTX12_PIPE);
			markSubsequentIfStatus12(target, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE);
		}
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	
	@Override
	public void visit(final Model node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Relational node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Logical node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Arithmetic node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Power node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Seq node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Special node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Sign node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final SubIndexed node) throws InvocationTargetException {
		doAcceptIn(node.expr.node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final SubIndexed.Args node) {
//		throw new IllegalStateException();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final SubIndexed.Arg node) {
//		throw new IllegalStateException();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final SubNamed node) throws InvocationTargetException {
		doAcceptIn(node.expr.node);
		// name by scanner
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final NSGet node) throws InvocationTargetException {
		// children by scanner
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final StringConst node) {
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final NumberConst node) {
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Symbol node) {
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Help node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
	@Override
	public void visit(final Dummy node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		this.syntaxError |= ((node.getStatusCode() & SYNTAXERROR_MASK) != 0);
	}
	
/*	
	@Override
	public void visit(CLoopCommand node) throws InvocationTargetException {
	}
	
	@Override
	public void visit(NullConst node) throws InvocationTargetException {
	}
*/	
/*	
	public void visit(final Comment node) throws InvocationTargetException {
	}
	
	public void visit(final DocuComment node) throws InvocationTargetException {
		node.acceptInRChildren(this);
	}
	
	public void visit(final DocuTag node) throws InvocationTargetException {
	}
	
	public void visit(final DocuText node) throws InvocationTargetException {
	}
*/
	
}
