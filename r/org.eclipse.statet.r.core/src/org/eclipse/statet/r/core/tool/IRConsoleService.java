/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.tool;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.ts.core.RToolService;


public interface IRConsoleService extends RToolService {
	
	
	int AUTO_CHANGE=                    0b0_0000_0000_0000_0001;
	
//	int DATA_CHANGE=                    0b0_0000_0000_0000_1000;
	
	int PACKAGE_CHANGE=                 0b0_0000_0000_0001_0000;
	
	
	boolean acceptNewConsoleCommand();
	
	/**
	 * Submits the text to the tool console.
	 * 
	 * @param input the text to submit
	 * @param m the progress monitor of the current run (or a child)
	 * @throws StatusException if an error occurred or the operation was canceled
	 */
	void submitToConsole(String input,
			ProgressMonitor m) throws StatusException;
	
	void briefAboutToChange();
	void briefChanged(int flags);
	
}
