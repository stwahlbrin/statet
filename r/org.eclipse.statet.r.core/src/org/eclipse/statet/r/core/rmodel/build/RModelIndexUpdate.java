/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel.build;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.internal.r.core.builder.RPkgData;
import org.eclipse.statet.internal.r.core.rmodel.RModelIndexOrder;
import org.eclipse.statet.internal.r.core.rmodel.RModelManagerImpl;
import org.eclipse.statet.ltk.model.core.impl.GenericResourceSourceUnit;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.rmodel.RSourceUnit;
import org.eclipse.statet.r.core.rmodel.RSourceUnitModelInfo;


@NonNullByDefault
public class RModelIndexUpdate extends RModelIndexOrder {
	
	
	public RModelIndexUpdate(final RProject rProject,
			final List<String> modelTypeIds, final boolean isFullBuild) {
		super(rProject, modelTypeIds, isFullBuild);
	}
	
	
	public void update(final RPkgData pkgData) {
		this.pkgData= pkgData;
	}
	
	public void update(final RSourceUnit sourceUnit, final @Nullable RSourceUnitModelInfo model) {
		final Result result= createResult(sourceUnit, model);
		if (result != null) {
			this.updated.add(result);
		}
	}
	
	public void remove(final RSourceUnit sourceUnit) {
		if (!this.isFullBuild) {
			this.removed.add(sourceUnit.getId());
		}
	}
	
	public void remove(final IFile file) {
		if (!this.isFullBuild) {
			this.removed.add(GenericResourceSourceUnit.createResourceId(file));
		}
	}
	
	public void submit(final IProgressMonitor monitor) throws CoreException {
		final RModelManagerImpl rManager= RCorePlugin.getInstance().getRModelManager();
		rManager.getIndex().update(this, monitor);
	}
	
}
