/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rmodel;

import java.io.Serializable;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Formals of an R function
 */
@NonNullByDefault
public final class Parameters implements Immutable, Serializable {
	
	
	private static final long serialVersionUID= -3536570586490553543L;
	
	
	public static final int UNKNOWN=                        0x0_0000_0000;
	
	private static final int OBJ_SHIFT= 0;
	private static final int NAME_SHIFT= 8;
	private static final int OTHER_SHIFT= 16;
	private static final int FLAGS_SHIFT= 24;
	
	public static final int UNSPECIFIC_OBJ=                 0x01 << OBJ_SHIFT;
	public static final int UNSPECIFIC_NAME=                1 << 0 << NAME_SHIFT;
	
	public static final int PACKAGE_NAME=                   1 << 1 << NAME_SHIFT;
	
	public static final int HELP_TOPIC_NAME=                1 << 4 << NAME_SHIFT;
	
	public static final int CLASS_OBJ=                      1 << 5 << OBJ_SHIFT;
	public static final int CLASS_NAME=                     1 << 5 << NAME_SHIFT;
	
	public static final int METHOD_OBJ=                     1 << 6 << OBJ_SHIFT;
	public static final int METHOD_NAME=                    1 << 6 << NAME_SHIFT;
	
	public static final int FILE_NAME=                      1 << 1 << OTHER_SHIFT;
	
	public static final int NAME_AS_SYMBOL=                 1 << 1 << FLAGS_SHIFT;
	public static final int NAME_AS_STRING=                 1 << 2 << FLAGS_SHIFT;
	
	
	public static final class Parameter implements Serializable {
		
		
		private static final long serialVersionUID= 5880323434513504465L;
		
		
		public final int index;
		
		private final @Nullable String name;
		
		private final int type;
		
		private final @Nullable String className;
//		private final String defaultAsCode;
		
		
		Parameter(final int index, final @Nullable String name, final int type,
				final @Nullable String className) {
			this.index= index;
			this.name= name;
			this.type= type;
			this.className= className;
		}
		
		
		public @Nullable String getName() {
			return this.name;
		}
		
		public int getType() {
			return this.type;
		}
		
		public @Nullable String getClassName() {
			return this.className;
		}
		
	}
	
	
	protected final @NonNull Parameter[] parameters;
	
	
	/**
	 * For more detailed definitions, use an {@link ParametersBuilder}.
	 */
	public Parameters(final String... names) {
		this.parameters= new @NonNull Parameter[names.length];
		for (int i= 0; i < names.length; i++) {
			this.parameters[i]= new Parameter(i, names[i], 0, null);
		}
	}
	
	Parameters(final @NonNull Parameter[] parameters) {
		this.parameters= parameters;
	}
	
	
	public int size() {
		return this.parameters.length;
	}
	
	@SuppressWarnings("null")
	public boolean contains(final @Nullable String name) {
		if (name != null) {
			for (int i= 0; i < this.parameters.length; i++) {
				final Parameter parameter= this.parameters[i];
				if (parameter.name != null && parameter.name.equals(name)) {
					return true;
				}
			}
		}
		return false;
	}
	
	@SuppressWarnings("null")
	public @Nullable Parameter get(final @Nullable String name) {
		if (name != null) {
			for (int i= 0; i < this.parameters.length; i++) {
				final Parameter parameter= this.parameters[i];
				if (parameter.name != null && parameter.name.equals(name)) {
					return parameter;
				}
			}
		}
		return null;
	}
	
	public Parameter get(final int index) {
		return this.parameters[index];
	}
	
	@SuppressWarnings("null")
	public int indexOf(final @Nullable String name) {
		if (name != null) {
			for (int i= 0; i < this.parameters.length; i++) {
				final Parameter parameter= this.parameters[i];
				if (parameter.name != null && parameter.name.equals(name)) {
					return parameter.index;
				}
			}
		}
		return -1;
	}
	
}
