/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.ui.preferences;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String CodeTemplates_title;
	public static String CodeTemplates_label;
	public static String CodeTemplates_EditButton_label;
	public static String CodeTemplates_ImportButton_label;
	public static String CodeTemplates_ExportButton_label;
	public static String CodeTemplates_ExportAllButton_label;
	public static String CodeTemplates_Preview_label;
	
	public static String CodeTemplates_error_title;
	public static String CodeTemplates_error_Read_message;
	public static String CodeTemplates_error_Write_message;
	
	public static String CodeTemplates_Import_title;
	public static String CodeTemplates_Import_extension;
	public static String CodeTemplates_Export_title;
	public static String CodeTemplates_Export_extension;
	public static String CodeTemplates_Export_filename;
	public static String CodeTemplates_Export_Error_title;
	public static String CodeTemplates_Export_Error_Hidden_message;
	public static String CodeTemplates_Export_Error_CanNotWrite_message;
	public static String CodeTemplates_Export_Exists_title;
	public static String CodeTemplates_Export_Exists_message;
	
	public static String TaskTags_title;
	public static String TaskTags_description;
	public static String TaskTags_TaskColumn_name;
	public static String TaskTags_PriorityColumn_name;
	public static String TaskTags_warning_NoTag_message;
	public static String TaskTags_error_DefaultTast_message;
	
	public static String TaskTags_NeedsBuild_title;
	public static String TaskTags_NeedsFullBuild_message;
	public static String TaskTags_NeedsProjectBuild_message;
	
	public static String TaskTags_InputDialog_NewTag_title;
	public static String TaskTags_InputDialog_EditTag_title;
	public static String TaskTags_InputDialog_Name_label;
	public static String TaskTags_InputDialog_Priority_label;
	public static String TaskTags_InputDialog_error_EnterName_message;
	public static String TaskTags_InputDialog_error_Comma_message;
	public static String TaskTags_InputDialog_error_EntryExists_message;
	public static String TaskTags_InputDialog_error_ShouldStartWithLetterOrDigit_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
