/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.base.ext.templates;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.persistence.TemplateStore;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;


/**
 * For extension point
 */
public interface ICodeGenerationTemplateCategory {
	
	public String getProjectNatureId();
	
	public TemplateStore getTemplateStore();
	
	public ContextTypeRegistry getContextTypeRegistry();
	
	public SourceEditorViewerConfigurator getEditTemplateDialogConfiguator(
			TemplateVariableProcessor processor, IProject project);
	
}
