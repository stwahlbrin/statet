/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String LineBreakpoint_name;
	public static String MethodBreakpoint_name;
	
	public static String Breakpoint_Line_label;
	public static String Breakpoint_SubLabel_copula;
	public static String Breakpoint_Function_prefix;
	public static String Breakpoint_S4Method_prefix;
	public static String Breakpoint_ScriptLine_prefix;
	
	public static String Breakpoint_DefaultDetailPane_name;
	public static String Breakpoint_DefaultDetailPane_description;
	
	public static String MethodBreakpoint_Entry_label;
	public static String MethodBreakpoint_Exit_label;
	
	public static String Hyperlink_StepInto_label;
	
	public static String Expression_Context_Missing_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
