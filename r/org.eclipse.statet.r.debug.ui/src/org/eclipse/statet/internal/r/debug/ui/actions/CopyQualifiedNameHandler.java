/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.jcommons.collections.CollectionUtils;

import org.eclipse.statet.ecommons.ui.util.DNDUtils;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.r.debug.core.RElementVariable;
import org.eclipse.statet.r.debug.core.RVariable;


public class CopyQualifiedNameHandler extends AbstractDebugHandler {
	
	
	public CopyQualifiedNameHandler() {
	}
	
	
	@Override
	public void setEnabled(final Object evaluationContext) {
		final IWorkbenchPart part= WorkbenchUIUtils.getActivePart(evaluationContext);
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(evaluationContext);
		if (part != null && selection != null && !selection.isEmpty()) {
			if (selection instanceof IStructuredSelection) {
				final IStructuredSelection structSelection= (IStructuredSelection) selection;
				for (final Object obj : structSelection.toList()) {
					if (obj instanceof RVariable) {
						final RVariable rVariable= (RVariable) obj;
						final RElementVariable elementVariable= getElementVariable(rVariable);
						if (elementVariable != null && elementVariable.getFQElementName() != null) {
							continue;
						}
					}
					setBaseEnabled(false);
					return;
				}
				setBaseEnabled(true);
				return;
			}
		}
		setBaseEnabled(false);
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart part= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		if (part != null && selection != null && !selection.isEmpty()) {
			final List<String> names= new ArrayList<>();
			if (selection instanceof IStructuredSelection) {
				final IStructuredSelection structSelection= (IStructuredSelection) selection;
				for (final Object obj : structSelection.toList()) {
					final String name= getName(obj);
					if (name != null) {
						names.add(name);
					}
				}
			}
			
			if (names.size() == 1) {
				copy(names.get(0), part);
			}
			else if (names.size() > 1) {
				copy(CollectionUtils.toString(names, ", "), part); //$NON-NLS-1$
			}
			else {
				copy("", part); //$NON-NLS-1$
			}
		}
		return null;
	}
	
	private String getName(final Object obj) {
		if (obj instanceof RVariable) {
			final RVariable rVariable= (RVariable) obj;
			final RElementVariable elementVariable= getElementVariable(rVariable);
			if (elementVariable != null) {
				final RElementName elementName= elementVariable.getFQElementName();
				if (elementName != null) {
					return addIndex(
							elementName.getDisplayName(RElementName.DISPLAY_EXACT | RElementName.DISPLAY_FQN),
							getVariableItemIndex(rVariable) );
				}
			}
		}
		return null;
	}
	
	private void copy(final String text, final IWorkbenchPart part) {
		final Clipboard clipboard= new Clipboard(part.getSite().getShell().getDisplay());
		try {
			DNDUtils.setContent(clipboard,
					new String[] { text },
					new Transfer[] { TextTransfer.getInstance() } );
		}
		finally {
			clipboard.dispose();
		}
	}
	
}
