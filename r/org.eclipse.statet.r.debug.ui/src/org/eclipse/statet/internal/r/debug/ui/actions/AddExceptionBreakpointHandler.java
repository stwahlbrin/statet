/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.r.debug.core.RDebugModel;
import org.eclipse.statet.r.debug.core.breakpoints.RExceptionBreakpoint;


public class AddExceptionBreakpointHandler extends AbstractHandler {
	
	
	public AddExceptionBreakpointHandler() {
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final String typeName= "*";
		
		try {
			RExceptionBreakpoint breakpoint= RDebugModel.getExpressionBreakpoint(typeName);
			if (breakpoint == null) {
				breakpoint= RDebugModel.createExceptionBreakpoint(typeName, false);
			}
			else {
				breakpoint.setEnabled(true);
			}
		}
		catch (final CoreException e) {
		}
		return null;
	}
	
}
