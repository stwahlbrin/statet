/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.data.CombinedRList;
import org.eclipse.statet.r.core.rmodel.RElementName;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;


public class RListVar extends BasicCombinedRElement
		implements RList, CombinedRList, ExternalizableRObject {
	
	
	private int length;
	private BasicCombinedRElement[] components;
	
	private final String className1;
	private RCharacter32Store namesAttribute;
	
	
	public RListVar(final RJIO io, final CombinedFactory factory, final int options,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name)
			throws IOException {
		super(parent, name);
		
		//-- options
		//-- special attributes
		this.className1= ((options & RObjectFactory.O_CLASS_NAME) != 0) ?
				io.readString() : ((getRObjectType() == RObject.TYPE_DATAFRAME) ?
						RObject.CLASSNAME_DATAFRAME : RObject.CLASSNAME_LIST);
		final int l= this.length= (int) io.readVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK));
		
		if ((options & RObjectFactory.O_NO_CHILDREN) != 0) {
			this.namesAttribute= null;
			this.components= null;
		}
		else {
			this.namesAttribute= (RCharacter32Store) factory.readNames(io, l);
			//-- data
			this.components= new BasicCombinedRElement[l];
			for (int i= 0; i < l; i++) {
				this.components[i]= factory.readObject(io, this,
						(this.namesAttribute.isNA(i) || this.namesAttribute.getChar(i).isEmpty()) ? 
								RElementName.create(RElementName.SUB_INDEXED_D, Integer.toString(i+1), i+1) :
								RElementName.create(RElementName.SUB_NAMEDPART, this.namesAttribute.getChar(i), i+1) );
			}
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		doWriteExternal(io, factory, 0);
	}
	protected final void doWriteExternal(final RJIO io, final RObjectFactory factory, int options) throws IOException {
		final int l= this.length;
		//-- options
		options |= io.getVULongGrade(l);
		if (!this.className1.equals(getDefaultRClassName())) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		if (this.components == null) {
			options |= RObjectFactory.O_NO_CHILDREN;
		}
		io.writeInt(options);
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			io.writeString(this.className1);
		}
		io.writeVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK), l);
		
		if (this.components != null) {
			factory.writeNames(this.namesAttribute, io);
			//-- data
			for (int i= 0; i < l; i++) {
				factory.writeObject(this.components[i], io);
			}
		}
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_LIST;
	}
	
	protected String getDefaultRClassName() {
		return RObject.CLASSNAME_LIST;
	}
	
	@Override
	public final String getRClassName() {
		return this.className1;
	}
	
	
	protected int length() {
		return this.length;
	}
	
	@Override
	public long getLength() {
		return this.length;
	}
	
	@Override
	public final @Nullable RCharacterStore getNames() {
		return this.namesAttribute;
	}
	
	@Override
	public final @Nullable String getName(final int idx) {
		if (this.namesAttribute != null) {
			return this.namesAttribute.getChar(idx);
		}
		else {
			if (idx < 0 || idx >= this.length) {
				throw new IndexOutOfBoundsException(Integer.toString(idx));
			}
			return null;
		}
	}
	
	@Override
	public final @Nullable String getName(final long idx) {
		if (this.namesAttribute != null) {
			return this.namesAttribute.getChar(idx);
		}
		else {
			if (idx < 0 || idx >= this.length) {
				throw new IndexOutOfBoundsException(Long.toString(idx));
			}
			return null;
		}
	}
	
	@Override
	public final @Nullable CombinedRElement get(final int idx) {
		return this.components[idx];
	}
	
	@Override
	public final @Nullable CombinedRElement get(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		return this.components[(int) idx];
	}
	
	@Override
	public final @Nullable CombinedRElement get(final String name) {
		if (this.namesAttribute != null) {
			final int idx= this.namesAttribute.indexOf(name, 0);
			if (idx >= 0) {
				return this.components[idx];
			}
		}
		return null;
	}
	
	@Override
	public final @Nullable RStore<?> getData() {
		return null;
	}
	
	
	@Override
	public int getElementType() {
		return R_GENERAL_VARIABLE;
	}
	
	@Override
	public final boolean hasModelChildren(final @Nullable LtkModelElementFilter filter) {
		if (this.components == null) {
			return false;
		}
		if (filter == null) {
			return (this.length > 0);
		}
		else {
			for (final BasicCombinedRElement component : this.components) {
				if (filter.include(component)) {
					return true;
				}
			}
			return false;
		}
	}
	
	@Override
	public final List<? extends CombinedRElement> getModelChildren(final @Nullable LtkModelElementFilter filter) {
		if (this.components == null) {
			return Collections.emptyList();
		}
		if (filter == null) {
			return ImCollections.newList(this.components);
		}
		else {
			final List<BasicCombinedRElement> list= new ArrayList<>();
			for (final BasicCombinedRElement component : this.components) {
				if (filter.include(component)) {
					list.add(component);
				}
			}
			return list;
		}
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("RObject type=list, class=").append(getRClassName());
		sb.append("\n\tlength=").append(this.length);
		if (this.components != null) {
			sb.append("\n\tdata: ");
			for (int i= 0; i < this.length; i++) {
				if (i > 100) {
					sb.append("\n... ");
					break;
				}
				if (this.namesAttribute == null || this.namesAttribute.isNA(i)) {
					sb.append("\n[[").append((i + 1)).append("]]\n");
				}
				else {
					sb.append("\n$").append(this.namesAttribute.getChar(i)).append("\n");
				}
				sb.append(this.components[i]);
			}
		}
		else {
			sb.append("\n<NODATA/>");
		}
		return sb.toString();
	}
	
}
